##
## ACMS Absolute Default - Default / Vanilla / Base Mapper - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Base Mapper Imports
from AcecoolCodeMappingSystem.maps.__mapper_imports__ import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##	Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
class ACMS_Default_Default( XCodeMapperBase ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_Default_Default'


	##
	## Callback - these are the symbols from the file we're currently editings symbols( ) from the view - ie all extracted symbols from Sublime Text...
	## In Short: If working this should give us all of the information from the Command Palette when we look at @, :, etc.. for function names and everything... A potentially almost perfect way to create a universal mapper...
	##
	## Task: Allow symbols to be processed through the AddSyntax processor ( Make a syntax processor function outside of Run to do this ) - Then the full-line of code can be grabbed from the symbols SublimeText provides us and we'll have a basic universal mapping system....
	##
	def OnMappingFileSymbols( self, _view, _symbols ):
		## pass
		## print( ' >> Symbols: ' + str( _symbols ) )
		## Symbols also don't give their function prefix...
		## if ( _code.startswith( 'def' ):

		## Symbols appear in a list, in a tuple with a region and the symbol...[((515, 525), 'PANEL:Init'), ((891, 910), 'PANEL:ControlValues'), ((1159, 1178), 'PANEL:PerformLayout')]

		## So for each entry
		for _entry in _symbols:
			## Tuples and lists use numerical indices
			## Grab the region object ( chars from start of file to X, and Total Chars in Region + X )
			_region = _entry[ 0 ]

			## Grab the partial code we're given - we can grab the full line using a helper function...
			_code = _entry[ 1 ]

			## Grab the line number using another helper...
			_line_number = self.GetSublimeViewLineNumberByRegion( _region )
			## _line_number = 1

			## Grab the line code by another helper...
			_line = self.GetFileDataByLine( _line_number )

			_file_view = self.GetMappedFileView( )
			_scope_name = _file_view.scope_name( _region.begin( ) ).strip( )

			## Grab the difference between a left-stripped bit of code and non-stripped to count how many spaces or tabs there are...
			_depth = len( _code ) - len( _code.lstrip( ) )

			## Note - Symbols use regions meaning the starting char of from the entire code blob to the end... not line numbering so this has to be calculated...
			## Since other info is also lost we can really only guess at the data unless we check the file and look at other infomration...
			## self.AddEntry( CATEGORY_FUNCTION, _line_number, _code, _depth, _mode, _pattern, _class_cat )


			##
			## Task: Remove the default system of grabbing functions, etc.. for Python and use Sublime Symbols for 100% of functions and classes... Make the SyntaxRuleFunction for Class ScopeDepthTracker to work with symbols... shouldn't be as difficult but instead of looking at each line we're only looking at each entry - but since they're in order it should be just as straight forward..
			##

			##
			## Process Depths, parents of objects, etc...
			##
			## self.SyntaxRuleFunctionClassScopeDepthTracker( _line_number, _code, _depth, False, False )

			self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ), 0 )

			##
			## if ( _scope_name == 'source.python meta.class.python storage.type.class.python' ):
			## 	## self.AddSimpleEntry( CATEGORY_CLASS, _line_number, _line.strip( ), 0 )
			## 	self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ), 0 )
			## elif ( _scope_name == 'source.python meta.class.python' ):
			## 	## self.AddSimpleEntry( CATEGORY_OBJECT, _line_number, _line.strip( ), 0 )
			## 	self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ), 0 )
			## elif ( _scope_name == 'source.python meta.function.python' ):
			## 	## self.AddSimpleEntry( CATEGORY_CLASS_FUNCTION, _line_number, _line.strip( ), 0 )
			## 	self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ), 0 )
			## elif ( _scope_name == 'source.python meta.function.python storage.type.function.python' ):
			## 	## self.AddSimpleEntry( CATEGORY_FUNCTION, _line_number, _line.strip( ), 0 )
			## 	self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ), 0 )
			## else:
			## 	self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ) + ' - Scope: ' + _scope_name + ' | as: ' + _code, 0 )


	##
	## Callback - Determines the indentation level to use for entries - Categories are at -1 ( not possible to use ), 0 is indented once, nested categories indent further. Default is 4 spaces per level, can be configured to use any character combination.
	##
	def OnCalcDepth( self, _category, _line_number, _code, _depth, _mode, _pattern ):
		## Return natural depth for everything.
		return _depth




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------