##
## Acecool Python / Sublime Text Library - Josh 'Acecool' Moser
##
## Note: This is used in an attempt to simply import Acecool to use Acecool.sublime, Acecool.str or Acecool.string, etc... and keep separate classes instead of from Acecool import * and have all nested in a single class....
##


##
## Task List -
##
## Task: Determine whether or not I should rename AccessorFunc function names to be more intuitive... For instance: The function an Accessor is being added to is called the Instance object, but we add instance helpers which are un-named, and we add named properties, etc.. So maybe if we named it something along the lines of ParentNamedProperty or ParentHelper - maybe it'd be less confusing?? and AccessorSomething for the AccessorFunc Object self function...
## Task:
## Task:
## Task:
## Task:
##


##
## Task List - AccessorFunc Task List
##
## Task: Setup Database for easy output of Key / Name information, functions list, etc..
## Task: Setup Group Aliases... So if I create / Add Group All for Pitch / Yaw / Roll, and the Accessors are created in that order, then self.SetAll( _p, _y, _r ) would work...  I may change the naming to SetAllAccessors or SetAllGroup or something... just to avoid collisions...
## Task: Create class Blah( BlahBase( Pos( PosBase ) ) ): __x = Accessorfunc... in Pos, now in Blah so it can work... __x = AccessorFuncBase.SetupAccessorReference/Link( Pos, 'x' ); or something to allow the redirect to work without causing issues...+- or  self.SetupAccessorGroupByKey( 'All', 'pitch', 'yaw', 'roll' ):
## Task:
## Task:
##


##
## Imports
##


##
##
##
def import_if_exists( _module ):
	## Try to import a module, if it exists - Return False if it doesn't, import if it does and return True.
	try:
		__import__( _module );
	except ImportError:
		return False;

	return True;

## Sublime Text Imports
## System Library
import os, platform, sys, io, errno, shutil;

## Arrays
import array;

## Math functions - This shouldn't be required for floor / ceil... etc... odd..
import math;

## Regular Expressions for Search and Replace...
import re;

##
import time, datetime;

# File I/O
import codecs;

##
import imp;

##
import sqlite3;

##
import logging;

##
import threading;

##
import zipfile;

##
import gc

## from __future__ import unicode_literals
import __future__
import inspect


##
## Declarations for Globals, CONSTants, ENUMeration, etc..
##



##
## Some important declarations
##

## Task: Incorporate recursion for the table processor - so I can define TYPE_NUMBER, etc.. and chain them in a table, if necessary...



## Sublime Text 3 API / Library
if ( import_if_exists( 'sublime' ) and import_if_exists( 'sublime_plugin' ) ):
	## Sublime Text 3 API / Library
	import sublime, sublime_plugin;



VALUE_ANY				= None;
VALUE_ALL				= VALUE_ANY;
VALUE_BOOLEANS			= ( True, False );
VALUE_SINGLE_DIGITS		= ( 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 );

## New-Line Declaration Constants - can use in config if you want..
CONST_NEW_LINE_WINDOWS						= '\r\n';
CONST_NEW_LINE_UNIX							= '\n';
CONST_NEW_LINE_MAC_OS_9						= '\n';

## True / False alternative definitions...
true										= True;
false										= False;
TRUE										= True;
FALSE										= False;

## Data returned when line-number is out of range in certain functions..
CONST_DATA_OUT_OF_RANGE						= 'OUT_OF_RANGE';

## Default used for file-data
CONST_DATA_NONE								= 'DATA_NONE';

## Define the tab jump-points - Pi uses 8, Windows uses 4, etc.. Basically, how many space-chars are used per \t tab char...
CONST_TAB_WIDTH								= 4;

##
## Folder Modes for GetPath...
##

## Default is Suffix Only
PATH_MODE_DEFAULT							= 0;

## Plugin grabs Packages/PluginName/ + suffix
PATH_MODE_PLUGIN							= 1;

## User Plugin grabs Packages/User/PluginName/ + suffix
PATH_MODE_PLUGIN_USER						= 2;

##
PATH_MODE_PLUGIN_RES						= 3;

##
PATH_MODE_PLUGIN_RES_USER					= 4;

##
PATH_MODE_CODEMAP_SYNTAX					= 5;


TYPE_ANY				= None;
TYPE_INTEGER			= int;
TYPE_ENUM				= int;
TYPE_COMPLEX			= complex;
TYPE_FLOAT				= float;
TYPE_BOOLEAN			= type( True );
TYPE_STRING				= type( '' );
TYPE_TYPE				= type;
TYPE_TUPLE				= type( ( ) );
TYPE_LIST				= type( [ ] );
TYPE_SET				= set;
TYPE_DICT				= type( { } );
TYPE_FUNCTION			= type( lambda: print( ) );
TYPE_METHOD				= type( lambda: print( ) );
TYPE_OBJECT				= object;
TYPE_NUMBERS			= ( TYPE_INTEGER, TYPE_FLOAT );

## Sublime Text 3 API / Library

TYPE_SUBLIME_VIEW		= sublime.View if ( import_if_exists( 'sublime' ) and import_if_exists( 'sublime_plugin' ) ) else '<class \'sublime.View\'>';


## TYPE_CLASS				= class
## TYPE_INTEGER			= type( 0 )
## TYPE_LONG				= type( long )
## TYPE_FLOAT				= type( 0.0 )
## TYPE_STRING_BASE		= type( basestring )
## TYPE_TYPE				= type(  )
## TYPE_TYPE				= type(  )
## TYPE_TYPE				= type(  )
## TYPE_TYPE				= type(  )




##
##
##
def tostr( *_args ):
	return String.ToString( *_args );



##
## Data-Type Helpers...
##

##
def IsFunction( _data = None ): return str( type( _data ) ) == "<class 'function'>" or str( type( _data ) ) == "<class 'method'>";
def isfunction( _data ): return IsFunction( _data );

## A Boolean is a 1 bit data-type representing True / False or as 1 / 0 in most languages.
def IsBoolean( _data = None ): return type( _data ) is bool;
def isboolean( _data ): return IsBoolean( _data );

## A List is a list created using [ ]s - It can contain a series of values - the index must be whole-numerical
def IsList( _data = None ): return type( _data ) is list;
def islist( _data ): return IsList( _data );

## A Tuple is a list created using ( )s - It is similar to a List but a Tuple is fixed in size once ccreated.
def IsTuple( _data = None ): return type( _data ) is tuple;
def istuple( _data ): return IsTuple( _data );

## A Dict is a list created using { }s - It is a powerful data-type which allows string / etc.. key data-types, paired with a value which can be any Data-Type.
def IsDict( _data = None ): return type( _data ) is dict;
def isdict( _data ): return IsDict( _data );

## A Dict is a list created using { }s - It is a powerful data-type which allows string / etc.. key data-types, paired with a value which can be any Data-Type.
def IsTableTypeSet( _data = None ): return type( _data ) is set;
def isTableTypeset( _data ): return IsTableTypeSet( _data );

## To help check for multi-type tables allowed - ie List and Tuple are similar
def IsSimpleTable( _data = None ): return IsTuple( _data ) or IsList( _data );
def issimpletable( _data ): return IsSimpleTable( _data );

## To help check for multi-type tables allowed..
def IsTable( _data = None ): return IsSimpleTable( _data ) or IsDict( _data ) or IsTableTypeSet( _data );
def istable( _data ): return IsTable( _data );

## A String is a text object typically contained and / or created within quotes..
def IsString( _data = None ): return type( _data ) is str;			# ( _typeof == "<class 'string'>" )
def isstring( _data ): return IsString( _data );

## A Complex is a traditional Int as a whole number from 0-255
def IsComplex( _data = None ): return type( _data ) is complex;
def iscomplex( _data ): return IsComplex( _data );

## An Integer, traditionally is a whole number not exceeding 255 - otherwise it typically has a range of 2.7 or so million... +/-
def IsInteger( _data = None ): return type( _data ) is int;
def isinteger( _data ): return IsInteger( _data );

## A long is a number as 123L which can represent octal and hexadecimal values
## def IsLong( _data = None ): return type( _data ) is long;
## def islong( _data ): return islong( _data );

## A Float is half-size Double number with decimal support
def IsFloat( _data = None ): return type( _data ) is float;
def isfloat( _data ): return IsFloat( _data );

## A Number is an integer, float, double, etc..
def IsNumber( _data = None ): return IsComplex( _data ) or IsInteger( _data ) or IsFloat( _data ); # or IsLong( _data )
def isnumber( _data ): return IsNumber( _data );


##
## IsSet is just to see whether or not something is None or not...
##
def IsSet( _data = None, _key = None ):
	## List checking...
	if ( _key != None ):
		## Dictionaries are easy to look at...
		if ( IsDict( _data ) ):
			return TernaryFunc( ( _data.get( _key, False ) == False ), False, True )

		## Lists require the key to be an integer... Tuples are like list but immutable
		elif ( IsList( _data ) or IsTuple( _data ) ):
			## Must be integer otherwise there will be an error so there's no point trying...
			if ( IsInteger( _key ) == False ): return False

			## If the key is out of bounds, then there's an issue...
			if ( len( _data ) > _key ): return False

			## We need to use a try because using simple len doesn't seem to be doing it??
			try:
				if ( IsSet( _data[ _key ] ) ):
					return True
				else:
					return False;
			except IndexError:
				return False;
			except:
				return False;
		elif ( IsString( _data ) ):
			return True;


	## Basic data-type...
	return _data != None;


##
## ## Grab the variable value based on a variable name
## ##
## def GetVariableValueByName( _key, _default_key = None, _default = None ):
## 	## return globals( ).get( _key, locals( ).get( _key, _default ) )
## 	if ( _default_key ):
## 		_default = globals( ).get( _default_key, locals( ).get( _default_key, _default ) );

## 	return globals( ).get( _key, locals( ).get( _key, _default ) );


##
## Grab the variable value based on a variable name
##
## def GetVariableValueByName( _key, _default_key = None, _default = None, _alt_tab = None ):

## 	## return globals( ).get( _key, locals( ).get( _key, _default ) )
## 	if ( _default_key ):
## 		_default_override = ( _default, _alt_tab.get( _default_key, _default ) )[ _alt_tab != None ]
## 		_default = globals( ).get( _default_key, locals( ).get( _default_key, _default_override ) );

## 	##
## 	_default_override = ( _default, _alt_tab.get( _default_key, _default ) )[ _alt_tab != None ]
## 	return globals( ).get( _key, locals( ).get( _key, _default_override ) );

def GetVariableValueByName( _key, _default_key = None, _default = None, _alt_tab = { } ):
	## return globals( ).get( _key, locals( ).get( _key, _default ) )
	if ( _default_key ):
		_default_override = ( _default, _alt_tab.get( _default_key, _default ) )[ len( _alt_tab ) > 0 ]
		_default = globals( ).get( _default_key, locals( ).get( _default_key, _default_override ) );

	##
	_default_override = ( _default, _alt_tab.get( _key, _default ) )[ len( _alt_tab ) > 0 ]
	return globals( ).get( _key, locals( ).get( _key, _default_override ) );
## def GetVariableValueByName( self, _key, _default_key = None, _default = None, _alt_tab = None ):
## 	if ( _default_key ):
## 		_default = globals( ).get( _default_key, locals( ).get( _default_key, ( ( _default, _alt_tab.get( _default_key, _default ) )[ _alt_tab != None ] ) ) )

## 	return globals( ).get( _key, locals( ).get( _key, ( ( _default, _alt_tab.get( _default_key, _default ) )[ _alt_tab != None ] ) ) )


##
## Ternary Operation in Python is just weird and has problems with stacking.. This fixes that...
##
def TernaryFunc( _statement = True, _true = None, _false = None ):
	return ( _false, _true )[ _statement ];


##
##
##
def tostr( *_args ):
	return String.ToString( *_args );


##
## Data-Type Aliases
##
## isset			= IsSet
## isboolean		= IsBoolean
## IsBool			= IsBoolean
## isbool			= IsBoolean
## islist			= IsList
## istuple			= IsTuple
## isdict			= IsDict
## isstring		= IsString
## iscomplex		= IsComplex
## isinteger		= IsInteger
## IsInt			= IsInteger
## isint			= IsInteger
## ## islong			= IsLong
## isfloat			= IsFloat
## isnumber		= IsNumber

## isfunction		= IsFunction
## isfunc			= IsFunction
## IsFunc			= IsFunction




##
## Enumerator - set the number of values to generate, and optionally the starting number...
##
## @arg: _count:	The number of ENUMs / Variables you create and prefix to the function - excluding the MAPs
## @arg: _start:	The starting number to use.. ( 4, 0 ) would produce: 0, 1, 2, 3 but ( 4, 0, true ) produces: 0, 1, 2, 4
## @arg: _pow:		Set to true to use base 2 for the ENUMeration values - 2^0 is 1 and 2^1 is 2 - you define the starting value and let it go from there with the second arg..
## @arg: _map:		If you want to map the ENUMeration ( ENUM values to be used as table keys for O( 1 ) access ) to functions, values, language, etc... Add a table here..
## @VarArgs:		Repeat _map for as many different maps as you want to make... Each will be its own table using ENUM values as the table key and the supplied values as values..
##
## Logic is as follows:
##	Call enumerator with relevant data... Start number, mode for pow or +1 increment. the number of elements we want.. OR a table which can be counted... We can also add more _args which basically take those counts and lets you build maps... If count is a table then it becomes a map...
##	Establish the start / end numbers and if there is a map to use then we add that to the maps list...
##	We process / create the enums
##	We then use the same enumerated values to generate the rest o the maps...
##
##
##
def ENUM( _count = None, *_maps ):

	## If count isn't set, and we don't have a map to base it off - we don't do anything...
	if ( _count == None and len( _maps ) < 1 ):
		return;


	## Set up a table for the enum values to be stored...
	_enums = [ ];

	## Create a single list of all of the enums...
	_enum_list				= [ ];

	## We set up the global options table to use _count, if it is set, and it is a Dictionary... If it is a number, we have other plans...
	_options				= ( { }, _count )[ _count != None and IsDict( _count ) ];

	## Determine the starting value - Note: It needs to start at 0 or lower for some reason... We seem to have issues using Lists with any non-concurrent number 1 or higher...
	_start 					= _options.get( 'start', 0 );

	## If _count is set and it is a number, then we use _count to assign _cout, otherwise we leave it at None and generate the number manually..
	_count					= _options.get( 'count', ( None, _count )[ _count != None and IsNumber( _count ) ] );

	## Configuration to see whether or not to generate the enumerator list
	_generate_enum_list		= _options.get( 'generate_enum_list', True );

	## Configuration to see whether or not we generate reverse-maps globally...
	_generate_reverse_maps	= _options.get( 'generate_reverse_maps', True );


	## Make sure start is 0 or less than 0...
	if ( _start > 0 ):
		_start = 0;

	## If we have at least 1 map, and the count isn't set or it is set less than the length of the map, we set it...
	if ( len( _maps ) > 0 and _maps[ 0 ] != None and ( _count == None or _count < len( _maps[ 0 ] ) ) ):
		if ( IsDict( _maps[ 0 ] ) ):
			_count = len( _maps[ 0 ].get( 'map', _maps[ len( _maps ) - 1 ] ) );
		else:
			_count = len( _maps[ 0 ] );


	## Process enumerations...
	for _i in range( _start, _count + _start ):
		_enums.append( _i );
		_enum_list.append( _i );

	## The list of enums is added to the start?
	if ( _generate_enum_list ):
		_enums.append( _enum_list );

	## Process any maps if we have them...
	if ( len( _maps ) > 0 ):
		## The adjusted starting point
		_i = _start;

		## For each map we are to build
		for _map in _maps:
			## If the _map is not a List, Tuple or Dictionary then skip this entry..
			if ( not IsTable( _map ) ):
				print( 'ENUM => Error... Map is not a List, Tuple, Set or Dictionary... Skipping this entry.. -- Type: ' + str( type( _map ) ) + ' -- Data: ' + str( _map ) );
				## print( 'ENUM -> _enums Count [ ERROR ]: ' + str( len( _enums ) ) );
				continue;


			## Setup up some special vars swhich may be changed with a Dictionary configuration entry... Config for the Dictionary, _map becomes _config.get( 'map', [ ] ), and _prefixes becomes _config.get( 'prefixes', _prefixes );
			_prefixes					= [ '' ];
			_value_callback				= None;
			_generate_enum_list			= True;
			_generate_reverse_map		= True;

			## Dictionaries are special...
			if ( IsDict( _map ) ):
				## Grab the prefixes...
				_prefixes				= _map.get( 'prefixes', _prefixes );

				## Grab the prefixes...
				_generate_reverse_map	= _map.get( 'generate_reverse_map', True );

				## Grab the prefixes...
				_value_callback			= _map.get( 'value_callback', lambda **_varargs: _varargs.get( 'value', None ) );

				## Grab the map..
				_map					= _map.get( 'map', [ ] );

			## If we allow generating reverse maps globally, or we have the local one set to True we can generate.. but only if the local one isn't False... If the local is false, then that overrides all..
			_generate_rmap	= ( _generate_reverse_map != False and ( _generate_reverse_maps or _generate_reverse_map ) )

			## For each Prefix we have in our list of prefixes...
			for _prefix in _prefixes:
				## The processed map we will insert into the enums list for export...
				_map_save = [ ];

				## The reversed processed map to save..
				_map_save_reversed = { };

				## For each key / value pairing in the map
				for _key, _value in enumerate( _map ):
					## Generate the appropriate index based on the starting index modified by the current counter...
					_map_i = ( _key + _i );

					## Add _map_save[ ADJUSTED_KEY ] = MAPPED_VALUE
					if ( _value_callback != None and callable( _value_callback ) ):
						_value = _value_callback( prefix = _prefix, value = _value );

					## Process the value futher to ensure it is usable.. Unfortunately, due to a bug, I can't use _prefix + _value in Ternary operators because _value can be None, despite that branch not being used when _value is None... This should really be fixed... So I have to force a string to be a string which is a waste to compensate for times when _value isn't used when it is None...
					_entry = ( _value, _prefix + str( _value ) )[ _prefix != '' and _value != None ];

					## Setup the standard direction mapping table... ENUM -> Word
					_map_save.insert( _map_i, _entry );

					## Setup the reversed direction mapping table... Word -> ENUM - We generate it if we can globally, or if we have the local reverse map set to True... But, we ignore this one, even if global is set, if we have the local one set to false...
					if ( _generate_rmap ):
						_map_save_reversed[ _entry ] = _map_i;


				## As we exiit the loop, before we go to the next loop or exit, we add the table to our enums / return table...
				_enums.append( _map_save );

				## Add the reversed map after each map created...
				if ( _generate_rmap ):
					_enums.append( _map_save_reversed );


	## Return the list of enums and maps...
	return _enums;


##
## Takes a table of keys and a table of values and turns them into a single table of keys = values...
##
def ENUM_MAP( _keys, _values ):
	if ( not ( IsTable( _keys ) and IsTable( _values ) and len( _keys ) == len( _values ) ) ):
		return False;

	##
	_map = { };

	##
	for _key, _value in enumerate( _keys ):
		_map[ _value ] = _values[ _key ];

	return _map;


##
##
##
def ENUM_MAP_PREPARE( _map, _extras = None ):
	##
	_map = _map.copy( );

	##
	if ( _extras != None ):
		_map.update( _extras );


	##
	## print( 'ENUM_MAP: ', str( _map ) );

	return _map;


##
## ENUMeration
##

##
ACCESSORFUNC_TYPE_INSTANCE, ACCESSORFUNC_TYPE_PROPERTY, ENUM_LIST_ACESSORFUNC_TYPES, MAP_ACCESSORFUNC_TYPE_NAMES, MAPR_ACCESSORFUNC_TYPE_NAMES = ENUM( None, [ 'Instance', 'Property' ] );



##
ACCESSORFUNC_ID_SETUP_DEFAULTS, ACCESSORFUNC_ID_GET, ACCESSORFUNC_ID_GET_RAW, ACCESSORFUNC_ID_ISSET, ACCESSORFUNC_ID_GET_STR, ACCESSORFUNC_ID_GET_LEN, ACCESSORFUNC_ID_GET_LEN_STR, ACCESSORFUNC_ID_GET_DEFAULT_VALUE, ACCESSORFUNC_ID_GET_PROPERTY, ACCESSORFUNC_ID_GET_ACCESSOR, ACCESSORFUNC_ID_GET_ALLOWED_TYPES, ACCESSORFUNC_ID_GET_ALLOWED_VALUES, ACCESSORFUNC_ID_GET_BASE_ALLOWED_TYPES, ACCESSORFUNC_ID_GET_BASE_ALLOWED_VALUES, ACCESSORFUNC_ID_HAS_GETTER_PREFIX, ACCESSORFUNC_ID_GET_GETTER_PREFIX, ACCESSORFUNC_ID_GET_NAME, ACCESSORFUNC_ID_GET_KEY, ACCESSORFUNC_ID_GET_ACCESSOR_KEY, ACCESSORFUNC_ID_GET_DATA_KEY, ACCESSORFUNC_ID_SET, ACCESSORFUNC_ID_ADD, ACCESSORFUNC_ID_POP, ACCESSORFUNC_ID_RESET, ACCESSORFUNC_ID_GET_HELPERS, ACCESSORFUNC_ID_GET_OUTPUT, ACCESSORFUNC_ID_GET_GETTER_OUTPUT, ACCESSORFUNC_ID_INSTANCE, ACCESSORFUNC_ID_INSTANCE_RESET_BY_NAME, ACCESSORFUNC_ID_INSTANCE_SET_BY_NAME, ACCESSORFUNC_ID_INSTANCE_GET_BY_NAME, ACCESSORFUNC_ID_INSTANCE_RESET_BY_KEY, ACCESSORFUNC_ID_INSTANCE_SET_BY_KEY, ACCESSORFUNC_ID_INSTANCE_GET_BY_KEY, ACCESSORFUNC_ID_INSTANCE_RESET_GROUPED, ACCESSORFUNC_ID_INSTANCE_SET_GROUPED, ACCESSORFUNC_ID_INSTANCE_GET_GROUPED, ACCESSORFUNC_ID_INSTANCE_ADD_GROUPED, ACCESSORFUNC_ID_INSTANCE_RESET_DATABASE, ACCESSORFUNC_ID_INSTANCE_GET_DATABASE, ACCESSORFUNC_ID_INSTANCE_GET_FUNC_NAMES_DATABASE, ACCESSORFUNC_ID_INSTANCE_GET_GROUPS_DATABASE, ACCESSORFUNC_ID_INSTANCE_GET_KEYS_DATABASE, ACCESSORFUNC_ID_INSTANCE_GET_NAMES_DATABASE, ACCESSORFUNC_ID_INSTANCE_GET_REFERENCES_DATABASE, ACCESSORFUNC_ID_INSTANCE_ADD_DATABASE, ACCESSORFUNC_ID_INSTANCE__STR__, ENUM_LIST_ACESSORFUNC_IDS, MAP_ACCESSORFUNC_IDS, MAPR_ACCESSORFUNC_IDS, MAP_ACCESSORFUNC_ON_IDS, MAPR_ACCESSORFUNC_ON_IDS, MAP_ACCESSORFUNC_PRE_IDS, MAPR_ACCESSORFUNC_PRE_IDS, MAP_ACCESSORFUNC_POST_IDS, MAPR_ACCESSORFUNC_POST_IDS, MAP_ACCESSORFUNC_ID_FUNC_NAMES, MAPR_ACCESSORFUNC_ID_FUNC_NAMES, MAP_ACCESSORFUNC_ID_FUNC_ARGS, MAP_ACCESSORFUNC_ID_FUNC_CALL_INSTANCE = ENUM( None,
	{
		## Value Callback System - if the defined as a lambda using **_varargs, you may define anything to the values..
		## 'value_vallback':	lambda **_varargs: ...,

		## Process this list 4 times with each output map having a different prefix for the existing words
		'prefixes': [ '', 'on_', 'pre_', 'post_' ],

		## Words list...
		'map': [
			## MAP_ACCESSORFUNC_IDS	- for backwards compatibility until these are removed and the enumeration key / values are used as the only thing..
			'setup_defaults', 'get', 'get_raw', 'isset', 'get_str', 'get_len', 'get_len_str', 'get_default_value', 'get_property', 'get_accessor',
			'get_allowed_types', 'get_allowed_values', 'get_base_allowed_types', 'get_base_allowed_values',
			'has_getter_prefix', 'get_getter_prefix', 'get_name', 'get_key', 'get_accessor_key', 'get_data_key',
			'set', 'add', 'pop', 'reset', 'get_helpers', 'get_output', 'get_getter_output',

			## Single - Unique Parent / Instance Functions ( Only added once, and are not bound by 'name' )...
			'instance', 'instance_reset_accessors_by_name', 'instance_set_accessors_by_name', 'instance_get_accessors_by_name',
			'instance_reset_accessors_by_key', 'instance_set_accessors_by_key', 'instance_get_accessors_by_key',
			'instance_reset_grouped_accessors', 'instance_set_grouped_accessors', 'instance_add_grouped_accessors', 'instance_get_grouped_accessors',
			'instance_get_accessors_database', 'instance_get_accessors_func_names_database', 'instance_get_accessors_groups_database', 'instance_get_accessors_keys_database', 'instance_get_accessors_names_database', 'instance_get_accessors_references_database',
			'instance_add_accessors_database', 'instance_reset_accessors_database',

			## __str__
			'instance__str__'
		]
	},
	[
		## MAP_ACCESSORFUNC_ID_FUNC_NAMES - These are the function names without this. / self. or arguments or parens. - Note: word_* are static words,
		None, '{get}{name}', '{get}{name}{word_raw}', '{word_is}{name}{word_set}', '{get}{name}{word_tostring}', '{get}{name}{word_len}', '{get}{name}{word_len}{word_tostring}', '{get}{name}{word_default}{word_value}', '{get}{name}{word_property}', '{get}{name}{word_accessor}',
		'{get}{name}{word_allowed}{word_types}', '{get}{name}{word_allowed}{word_values}', '{get}{name}{word_base}{word_allowed}{word_types}', '{get}{name}{word_base}{word_allowed}{word_values}',
		'{word_has}{name}{word_getter}{word_prefix}', '{get}{name}{word_getter}{word_prefix}', '{get}{name}{word_name}', '{get}{name}{word_key}', '{get}{name}{word_accessor}{word_key}', '{get}{name}{word_data}{word_key}',
		'{word_set}{name}', '{word_add}{name}', '{word_pop}{name}', '{word_reset}{name}', '{get}{name}{word_helpers}', '{get}{name}{word_output}', '{get}{name}{word_getter}{word_output}',

		## Instance
		None, '{word_reset}{word_accessors}', '{word_set}{word_accessors}', '{word_get}{word_accessors}',
		'{word_reset}{word_accessors}{word_by}{word_key}', '{word_set}{word_accessors}{word_by}{word_key}', '{word_get}{word_accessors}{word_by}{word_key}',
		'{word_reset}{word_grouped}{word_accessors}', '{word_set}{word_grouped}{word_accessors}', '{word_add}{word_grouped}{word_accessors}', '{word_get}{word_grouped}{word_accessors}',
		'{word_get}{word_accessors}{word_db}', '{word_get}{word_accessors}{word_func}{word_names}{word_db}', '{word_get}{word_accessors}{word_groups}{word_db}', '{word_get}{word_accessors}{word_keys}{word_db}', '{word_get}{word_accessors}{word_names}{word_db}', '{word_get}{word_accessors}{word_references}{word_db}',
		'{word_add}{word_accessors}{word_db}', '{word_reset}{word_accessors}{word_db}',

		## __str__
		'{word__str__}'
	],
	{
		## These have too many identical values to be useful as a reverse map, so don't generate one..
		'generate_reverse_map': False,

		## Create a map which shows each argument list for each of the functions
		'map': [
			## MAP_ACCESSORFUNC_ID_FUNC_ARGS - These are the function arguments, if any including defaults without parens
			None, '_default_override = None, _ignore_defaults = False', '', '', 'get_str', '_default_override = None, _ignore_defaults = False', '_default_override = None, _ignore_defaults = False', 'get_default_value', '', '',
			'', '', '', '',
			'', '', '', '', '', '',
			'_value', '_value', '_index = len - 1, _count = 1', '', '', '', '',

			## Instance
			None, '_name [ , _name ] ...', '_name, _value [ , _name, _value ] ...', '_name [ , _name ] ...',
			'_key [ , _key ] ...', '_key, _value [ , _key, _value ] ...', '_key [ , _key ] ...',
			'_group [ , _group ] ...', '_group, _name [ , _name ] ...', '_group, _name [ , _name ] ...', '_group [ , _group ] ...',
			'', 'instance_get_accessors_func_names_database', 'instance_get_accessors_groups_database', 'instance_get_accessors_keys_database', 'instance_get_accessors_names_database', 'instance_get_accessors_references_database',
			'_entry', '',

			## __str__
			'',
		],
	},
	{
		## These have too many identical values to be useful as a reverse map, so don't generate one..
		'generate_reverse_map': False,

		## Create a map which shows the type of instance / object the enumeration ids belong to..
		'map': [
			## MAP_ACCESSORFUNC_ID_FUNC_CALL_NAMES - These are the functions as they'd be if they were called... use {map_*} for names and args to add the arguments and generated name so these are short as: '{word_this}.{map_name}( {map_args} );' - The only thing we really need to do is choose this or self as the only difference meaning we can generate these dynamically... Note: Remove this, generate them dynamically and replace this with self vs this choices...
			## MAP_ACCESSORFUNC_ID_FUNC_CALL_INSTANCE
			None, '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}',
			'{word_this}', '{word_this}', '{word_this}', '{word_this}',
			'{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}',
			'{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}',

			## Instance
			None, '{word_this}', '{word_this}', '{word_this}',
			'{word_this}', '{word_this}', '{word_this}',
			'{word_this}', '{word_this}', '{word_this}', '{word_this}',
			'{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}', '{word_this}',

			'{word_this}', '{word_this}',

			## __str__
			'{word_this}',
		],

	},
);



ACCESSORFUNC_WORD_SELF, ACCESSORFUNC_WORD_THIS, ACCESSORFUNC_WORD_FUNC, ACCESSORFUNC_WORD_GROUPS, ACCESSORFUNC_WORD_KEYS, ACCESSORFUNC_WORD_NAMES, ACCESSORFUNC_WORD_REFERENCES, ACCESSORFUNC_WORD_KEY, ACCESSORFUNC_WORD_GET, ACCESSORFUNC_WORD_SET, ACCESSORFUNC_WORD_ADD, ACCESSORFUNC_WORD_POP, ACCESSORFUNC_WORD_RAW, ACCESSORFUNC_WORD_IS, ACCESSORFUNC_WORD_TO_STR, ACCESSORFUNC_WORD_LEN, ACCESSORFUNC_WORD_DEFAULT, ACCESSORFUNC_WORD_VALUE, ACCESSORFUNC_WORD_PROPERTY, ACCESSORFUNC_WORD_ACCESSOR, ACCESSORFUNC_WORD_ACCESSORS, ACCESSORFUNC_WORD_ALLOWED, ACCESSORFUNC_WORD_TYPES, ACCESSORFUNC_WORD_VALUES, ACCESSORFUNC_WORD_BASE, ACCESSORFUNC_WORD_HAS, ACCESSORFUNC_WORD_GETTER, ACCESSORFUNC_WORD_PREFIX, ACCESSORFUNC_WORD_NAME, ACCESSORFUNC_WORD_DATA, ACCESSORFUNC_WORD_RESET, ACCESSORFUNC_WORD_WIKI, ACCESSORFUNC_WORD_OUTPUT, ACCESSORFUNC_WORD_BY, ACCESSORFUNC_WORD_GROUPED, ACCESSORFUNC_WORD_DB, ACCESSORFUNC_WORD_DATABASE, ACCESSORFUNC_WORD_HELPER, ACCESSORFUNC_WORD_HELPERS, ACCESSORFUNC_WORD__STR__, ENUM_LIST_ACCESSOR_FUNC_WORDS, MAP_ACCESSORFUNC_WORDS, MAPR_ACCESSORFUNC_WORDS, MAP_ACCESSORFUNC_WORD_KEYS, MAPR_ACCESSORFUNC_WORD_KEYS = ENUM( None,
	{
		## Change all words to fully lowercase, but only when the prefix is 'word_' so we can generate 2 lists... 1 as is seen, and one where each is lowercase with the word_ prefix...
		'value_callback':	( lambda **_varargs: ( _varargs.get( 'value', None ), str( _varargs.get( 'value', None ) ).lower( ) )[ _varargs.get( 'value', None ) != None and _varargs.get( 'prefix', '' ) != '' ] ),

		## Add word_ prefix to all words
		'prefixes':			( '', 'word_' ),

		## Words List...
		'map': [
			##
			'self',			'this',

			##
			'Func',			'Groups',		'Keys',			'Names',		'References',

			##
			'Key',			'Get',			'Set',			'Add',			'Pop',		'Raw',			'Is',		'ToString',		'Len',			'Default',		'Value',
			'Property',		'Accessor',		'Accessors',	'Allowed',		'Types',	'Values',		'Base',		'Has',			'Getter',		'Prefix',		'Name',
			'Data',			'Reset',		'Wiki',			'Output',		'By',		'Grouped',		'DB',		'Database',		'Helper',		'Helpers',


			##
			'__str__'
		]
	}

	##
	## Note: I'm leaving this here for now to show what the above Dictionary configured map will generate - previously this had to be made by hand... Now it doesn't need to be...
	## [

	## 	##
	## 	'word_self',			'word_this',

	## 	##
	## 	'word_key',				'word_get',				'word_set',				'word_add',			'word_pop',			'word_raw',			'word_is',			'word_tostring',		'word_len',			'word_default',		'word_value',
	## 	'word_property',		'word_accessor',		'word_accessors',		'word_allowed',		'word_types',		'word_values',		'word_base',		'word_has',				'word_getter',		'word_prefix',		'word_name',
	## 	'word_data',			'word_reset',			'word_wiki',			'word_output',		'word_by',			'word_grouped',		'word_db',			'word_database',		'word_helper',		'word_helpers',

	## 	##
	## 	'word__str__'
	## ]
);



##
## Reverse maps...
##

## Create a map which converts word_* as keys in a Dictionary ids to the actual words as values..
MAP_ACCESSORFUNC_KEYS_TO_WORDS = ENUM_MAP( MAP_ACCESSORFUNC_WORD_KEYS, MAP_ACCESSORFUNC_WORDS );

## Create a map which converts enumeration id from all AccessorFunc code-names ( get, get_raw, set, add, etc.. ) and turns them into the words.
## MAP_ACCESSORFUNC_IDS, MAP_ACCESSORFUNC_ID_FUNC_NAMES, MAP_ACCESSORFUNC_ID_FUNC_ARGS, MAP_ACCESSORFUNC_ID_FUNC_CALL_INSTANCE

## Create helper list of all ENUM ACCESSORFUNC_ID_ENUMS
ENUM_LIST_ACCESSORFUNC_ID_ENUMS = ( ACCESSORFUNC_ID_SETUP_DEFAULTS, ACCESSORFUNC_ID_GET, ACCESSORFUNC_ID_GET_RAW, ACCESSORFUNC_ID_ISSET, ACCESSORFUNC_ID_GET_STR, ACCESSORFUNC_ID_GET_LEN, ACCESSORFUNC_ID_GET_LEN_STR, ACCESSORFUNC_ID_GET_DEFAULT_VALUE, ACCESSORFUNC_ID_GET_PROPERTY, ACCESSORFUNC_ID_GET_ACCESSOR, ACCESSORFUNC_ID_GET_ALLOWED_TYPES, ACCESSORFUNC_ID_GET_ALLOWED_VALUES, ACCESSORFUNC_ID_GET_BASE_ALLOWED_TYPES, ACCESSORFUNC_ID_GET_BASE_ALLOWED_VALUES, ACCESSORFUNC_ID_HAS_GETTER_PREFIX, ACCESSORFUNC_ID_GET_GETTER_PREFIX, ACCESSORFUNC_ID_GET_NAME, ACCESSORFUNC_ID_GET_KEY, ACCESSORFUNC_ID_GET_ACCESSOR_KEY, ACCESSORFUNC_ID_GET_DATA_KEY, ACCESSORFUNC_ID_SET, ACCESSORFUNC_ID_ADD, ACCESSORFUNC_ID_POP, ACCESSORFUNC_ID_RESET, ACCESSORFUNC_ID_GET_HELPERS, ACCESSORFUNC_ID_GET_OUTPUT, ACCESSORFUNC_ID_GET_GETTER_OUTPUT, ACCESSORFUNC_ID_INSTANCE, ACCESSORFUNC_ID_INSTANCE_RESET_BY_NAME, ACCESSORFUNC_ID_INSTANCE_SET_BY_NAME, ACCESSORFUNC_ID_INSTANCE_GET_BY_NAME, ACCESSORFUNC_ID_INSTANCE_RESET_BY_KEY, ACCESSORFUNC_ID_INSTANCE_SET_BY_KEY, ACCESSORFUNC_ID_INSTANCE_GET_BY_KEY, ACCESSORFUNC_ID_INSTANCE_RESET_GROUPED, ACCESSORFUNC_ID_INSTANCE_SET_GROUPED, ACCESSORFUNC_ID_INSTANCE_GET_GROUPED, ACCESSORFUNC_ID_INSTANCE_ADD_GROUPED, ACCESSORFUNC_ID_INSTANCE_RESET_DATABASE, ACCESSORFUNC_ID_INSTANCE_GET_DATABASE, ACCESSORFUNC_ID_INSTANCE_ADD_DATABASE, ACCESSORFUNC_ID_INSTANCE__STR__ );

## Create a reverse lookup map from ENUM to ID ( from 1, 2, 3, etc... to get, get_raw, isset, get_str, get_len, get_len_str, etc.. )
MAP_ACCESSORFUNC_ENUMS_TO_IDS = ENUM_MAP( ENUM_LIST_ACCESSORFUNC_ID_ENUMS, MAP_ACCESSORFUNC_IDS );

## Create a reverse lookup map from ENUM to ID ( from 1, 2, 3, etc... to on_get, on_get_raw, on_isset, on_get_str, on_get_len, on_get_len_str, etc.. )
MAP_ACCESSORFUNC_ENUMS_TO_ON_IDS = ENUM_MAP( ENUM_LIST_ACCESSORFUNC_ID_ENUMS, MAP_ACCESSORFUNC_ON_IDS );

## Create a reverse lookup map from ENUM to ID ( from 1, 2, 3, etc... to get, pre_get_raw, pre_isset, pre_get_str, pre_get_len, pre_get_len_str, etc.. )
MAP_ACCESSORFUNC_ENUMS_TO_PRE_IDS = ENUM_MAP( ENUM_LIST_ACCESSORFUNC_ID_ENUMS, MAP_ACCESSORFUNC_PRE_IDS );

## Create a reverse lookup map from ENUM to ID ( from 1, 2, 3, etc... to post_get, post_get_raw, post_isset, post_get_str, post_get_len, post_get_len_str, etc.. )
MAP_ACCESSORFUNC_ENUMS_TO_POST_IDS = ENUM_MAP( ENUM_LIST_ACCESSORFUNC_ID_ENUMS, MAP_ACCESSORFUNC_POST_IDS );


## These are Instance / Property only 2 keys
## ENUM_LIST_ACESSORFUNC_TYPES, MAP_ACCESSORFUNC_TYPE_NAMES, MAPR_ACCESSORFUNC_TYPE_NAMES

## This is for the Word List... self, this, Key, Get, etc... Words is for Key, Get, Set, etc... R is self -> 0, this => 1, etc.. WORD_KEYS is for word_self, word_this, word_key, word_get, etc.. and R is for word_self => 0, word_this => 1, etc...
## ENUM_LIST_ACCESSOR_FUNC_WORDS, MAP_ACCESSORFUNC_WORDS, MAPR_ACCESSORFUNC_WORDS, MAP_ACCESSORFUNC_WORD_KEYS, MAPR_ACCESSORFUNC_WORD_KEYS

## This one is extensive...|| IDs are setup_defaults, get, set, add, etc... && R is get->1, get_raw->2, etc..  || ON_IDs is the same as previous except on_ prefix  && on_get -> 1  || PRE_IDS uses pre_ prefix  && pre_get -> 1 || POST_IDS uses post_ prefix && post_get -> 1 || FUNC_NAMES uses {get}{name} for <Get><Blah>, {word_is}{name}{word_set} for Is<Blah>Set - these need to be formatted &&  Reverse isn't super helpful unelss you use the PRE formatted data to jump to the id.. || FUNC_ARGS is a string list of the arguments for each function && reverse converts back - but a lot of these are empty so this isn't useful either.... || CALL_INSTANCE needs to be formatted but uses {word_this| or {word_self} to show where this is executed... As most of these are identical, conversion won't be very helpful... I'll need to create a Dict setting to prevent reverse maps from being generated ( although maybe I'll revert it back to not generate using normal, and if I want them then use Dict and set it )
## ENUM_LIST_ACESSORFUNC_IDS, MAP_ACCESSORFUNC_IDS, MAPR_ACCESSORFUNC_IDS, MAP_ACCESSORFUNC_ON_IDS, MAPR_ACCESSORFUNC_ON_IDS, MAP_ACCESSORFUNC_PRE_IDS, MAPR_ACCESSORFUNC_PRE_IDS, MAP_ACCESSORFUNC_POST_IDS, MAPR_ACCESSORFUNC_POST_IDS, MAP_ACCESSORFUNC_ID_FUNC_NAMES, MAPR_ACCESSORFUNC_ID_FUNC_NAMES, MAP_ACCESSORFUNC_ID_FUNC_ARGS, MAPR_ACCESSORFUNC_ID_FUNC_ARGS, MAP_ACCESSORFUNC_ID_FUNC_CALL_INSTANCE, MAPR_ACCESSORFUNC_ID_FUNC_CALL_INSTANCE


##
## AccessorFunc Support
##





##
##
##
## class AcecoolBase( ):
## 	pass;
class Acecool: #( AcecoolBase	):
	##
	##
	##
	__name__ = 'Acecool';


	##
	Settings = {
		## Should we output AccessorFunc Registry information such as function name, call name with args, alias or original, etc...
		## 'output_accessorfunc_register_info':				True,

		## Should we output all file examples?
		## 'output_acecool_lib_examples':						True,

		##
		## 'output_accessorfunc_register_info_alias':			True,

		##
		## 'output_accessorfunc_register_info_instance':		True,

		##
		## 'output_accessorfunc_register_info_original':		True,

		##
		## '':			True,

		##
		## '':			True,
	};


	##
	## Task: Create Settings system similar to Sublime, but Stand-Alone and with proper inheritance, etc..
	##
	def GetSetting( _key, _default = None ):
		## Acecool.GetSetting( 'output_accessorfunc_register_info', False )

		##
		_setting = Acecool.Settings.get( _key, False );
		if ( _setting ):
			return _setting;

		return _default;


	##
	##
	##
	def GetExample( _class ):
		##
		_example		= getattr( _class, '__example__', None );
		_name			= getattr( _class, '__name__', 'ClassNameNotDefined' );

		##
		_accessor_db	= getattr( _class, 'accessor_func_db', None );

		##
		if ( callable( _example ) ):
			return Acecool.Header( _name + ': Examples' ) + _example( ) + '\n------- AccessorFuncDB -------\n' + str( _accessor_db );

		##
		return Acecool.Header( _name + ': Examples' ) + '\tExample Not Available\n\n\n';


	##
	## Truncate Example...
	##
	_col_key	= 20;
	_col_eq		= 10;
	_col_res	= 50;
	_row_eq		= '==';
	_text		= 'Testing';

	##
	_col_a_text = '10  10  10';
	_col_b_text = '__|><|';
	_col_c_text = 'T  h  i  r  t y';


	##
	## Header
	##
	def Header( _header = '', _prefix = '', _suffix = '', _depth = 0 ):
		##
		_depth_prefix = _depth * '\t';

		##
		_text = _depth_prefix + '##\n';
		_text += _depth_prefix + '## ' + _header + '\n';
		_text += _depth_prefix + '##\n';
		_text += '' + _depth_prefix + _prefix + String.FormatColumnEx( '-', Acecool._col_key + Acecool._col_eq + Acecool._col_res, '' ) + _suffix + '\n';

		return _text.format( **Acecool.Replacers( _depth_prefix ) );


	##
	## Footer
	##
	def Footer( _prefix = '', _suffix = '\n\n', _depth = 1 ):
		##
		_depth_prefix = _depth * '\t';
		_text = _depth_prefix + _prefix + String.FormatColumnEx( '~', Acecool._col_key + Acecool._col_eq + Acecool._col_res, '' ) + _suffix;

		return _text.format( **Acecool.Replacers( _depth_prefix ) );


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		## _depth_prefix = _depth * '\t';
		## _depth_prefix +;
		## _depth_prefix

		##
		_examples = Examples( )

		##
		print( _examples.GetAll( ) );


	## Replacers for easier additions to comment headers, etc...
	def Replacers( _depth_prefix = '' ):
		##
		_br						= '\n';
		_tab					= '\t';
		_space					= ' ';

		##
		_comment_base			= '##';
		_comment				= _comment_base + _space;
		_comment_tab			= _comment_base + _tab;
		_comment_br				= _comment_base + _br;

		##
		_note_base				= 'Note:';
		_note					= _note_base + _space;
		_note_tab				= _note_base + _tab;

		##
		_hierarchy_base			= 'Hierarchy:';
		_hierarchy				= _hierarchy_base + _space;
		_hierarchy_tab			= _hierarchy_base + _tab;

		##
		_example_base			= 'ie:';
		_example				= _example_base + _space;
		_example_tab			= _example_base + _tab;

		##
		_depth_comment_base		= _br + _depth_prefix + _comment_base;
		_depth_comment			= _depth_comment_base + _space;
		_depth_comment_tab		= _depth_comment_base + _tab;
		_depth_comment_br		= _depth_comment_base + _br;


		##
		_comment_note			= _comment + _note;
		_comment_hierarchy		= _comment + _hierarchy;

		##
		_depth_note_base		= _depth_comment + _note_base;
		_depth_note				= _depth_comment + _note;
		_depth_note_tab			= _depth_comment + _note_tab;

		##
		_depth_example_base		= _depth_comment_tab + _example_base;
		_depth_example			= _depth_comment_tab + _example;
		_depth_example_tab		= _depth_comment_tab + _note_tab;

		##
		_footer					= _depth_comment_br + _depth_prefix;

		##
		_replacers = {
			##
			'br':						_br,
			'soace':					_space,

			##
			'tab':						_tab,
			'tab2':						_tab * 2,
			'tab3':						_tab * 3,
			'tab4':						_tab * 4,
			'tab5':						_tab * 5,

			##
			'depth':					_depth_prefix,

			##
			'note':						_note,
			'note_base':				_note_base,
			'note_tab':					_note_tab,

			##
			'hierarchy':				_hierarchy,
			'hierarchy_base':			_hierarchy_base,
			'hierarchy_tab':			_hierarchy_tab,

			##
			'example':					_example,
			'example_base':				_example_base,
			'example_tab':				_example_tab,

			##
			'depth_example':			_depth_example,
			'depth_example_base':		_depth_example_base,
			'depth_example_tab':		_depth_example_tab,


			##
			'comment':					_comment,
			'comment_base':				_comment_base,
			'comment_tab':				_comment_tab,
			'comment_note':				_comment_note,
			'comment_hierarchy':		_comment_hierarchy,

			##
			'depth_comment':			_depth_comment,
			'depth_comment_base':		_depth_comment_base,
			'depth_comment_tab':		_depth_comment_tab,

			##
			'depth_note':				_depth_note,
			'depth_note_tab':			_depth_note_tab,


			##
			'footer':					_footer,


		};

		##
		return _replacers;


##
##
##
## class ExamplesBase( ):
## 	pass;
class Examples( ):
	##
	##
	##
	def __init__( self ):
		## import_if_exists
		## tostr
		## IsFunction
		## isfunction
		## IsBoolean
		## isboolean
		## IsList
		## islist
		## IsTuple
		## istuple
		## IsDict
		## isdict
		## IsSimpleTable
		## issimpletable
		## def IsTable( _data = None ): return IsSimpleTable( _data ) or IsDict( _data );

		## istable
		## IsString
		## isstring
		## IsComplex
		## iscomplex
		## IsInteger
		## isinteger
		## ## A long is a number as 123L which can represent octal and hexadecimal values
		## ## def IsLong( _data = None ): return type( _data ) is long;
		## ## def islong( _data ): return islong( _data );
		## IsFloat
		## isfloat
		## IsNumber
		## isnumber
		## IsSet
		## GetVariableValueByName
		## TernaryFunc
		## tostr
		pass


	##
	##
	##
	def GetAll( self ):
		##
		_text = ''


		## ## A
		## _text += Acecool.GetExample( AccessorFuncBase );
		## _text += Acecool.GetExample( AccessorFuncException );
		_text += Acecool.GetExample( Angle );

		## ## B
		## ## C

		## ## D
		## _text += Acecool.GetExample( Database );
		## _text += Acecool.GetExample( Dict );

		## ## E
		## _text += self.ENUM( );

		## ## F
		## _text += Acecool.GetExample( File );
		## _text += Acecool.GetExample( Folder );
		## _text += Acecool.GetExample( Function );

		## ## G
		## ## H
		## ## I
		## ## J
		## ## K
		## ## L
		## _text += Acecool.GetExample( List );
		## _text += Acecool.GetExample( Logic );

		## ## M
		## ## N
		## ## O
		## ## P
		_text += Acecool.GetExample( Pos2D );
		_text += Acecool.GetExample( Pos );
		## _text += self.Python( );

		## ## Q
		## ## R
		## ## S
		## _text += Acecool.GetExample( String );

		## ## T
		## _text += Acecool.GetExample( Table );
		## _text += Acecool.GetExample( TimeKeeper );
		## _text += Acecool.GetExample( Tuple );

		## ## U
		## _text += Acecool.GetExample( Util );

		## ## V
		_text += Acecool.GetExample( Vector );
		## _text += Acecool.GetExample( Version );

		## _text += Acecool.GetExample( AccessorFuncExample );
		## ## W
		## ## X
		## ## Y
		## ## Z
		## 0-9



		_text += '\n';
		_text += '\n';






		##
		return _text


	##
	## Python examples which could be confusing to people or provide enlightenment...
	##
	def Python( self ):
		##
		## Interesting - _bar is only initialized ONCE... Solution is to set all optional args as None and initialize them within the function if we want them to be cleared after each call...
		##
		def FunctionScopeIssues( _list = [ ] ):
			_list.append( 'baz' );

			return str( _list );


		##
		_depth	= 1;

		##
		_text	= ''


		_text = '';
		_text += Acecool.Header( 'FunctionScopeIssues', '{comment_note}This shows how an argument to a function is initialized a single time, only.. Each subsequent call, when we append to the, by default empty, list, at the end we have 3 entries instead of 1 each time it is called...{footer}', '', _depth );
		_text += '\n';
		_text += FunctionScopeIssues( ) + '\n';
		_text += FunctionScopeIssues( ) + '\n';
		_text += FunctionScopeIssues( ) + '\n';


		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.Serialize( ): ', 50, str( _vector.Serialize( ) ) ) + '\n';
		## _text += '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.x: ', 50, str( _vector.x ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.y: ', 50, str( _vector.y ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.z: ', 50, str( _vector.z ) ) + '\n';
		## _text += '\n';

		## ## Vector( *_vector.Normalize( ) ).Serialize( )
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> _vector.Normalize( ): ', 85, str( _vector.Normalize( ) ) ) + 'This copies the values from the vector and returns the noramlized data.. It doesn\'t change the vector..\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> _vector.GetNormalized( ): ', 85, str( _vector.GetNormalized( ) ) ) + '\n\n\t\t\t^ This creates a new Vector with the Normalized data.. It doesn\'t change the vector..\n\t\t\tIt could be added as a Node to this Vector object, and when the Vector changes, using on_set callback, the Vector could be Normalized and saved into the Node - This is what the Versioning system does - when the version is updated, it is unpacked and processed ( removes decimals and treats it as a number but allows buffers to be added ).\n\t\t\tAlternatively, to make it more efficient, the Normalized Node could be populated only when Normalize or GetNormalized is called - then that data is micro-cached and a toggled-control would be enabled ( which would be reset when any Vector element [ x, y, z ] is updated ) so, if enabled then it would use the cached data and if disabled it means the data has changed and the normalized data is processed again and re-stored..\n';

		## _text += '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.__serialize: ', 300, str( _vector.__serialize ) ) + '\n';
		## ## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.__x: ', 300, str( _vector.__x ) ) + '\n';
		## ## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.__y: ', 300, str( _vector.__y ) ) + '\n';
		## ## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.__z: ', 300, str( _vector.__z ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.GetXAccessor( ): ', 250, str( _vector.GetXAccessor( ) ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.GetYAccessor( ): ', 250, str( _vector.GetYAccessor( ) ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.GetZAccessor( ): ', 250, str( _vector.GetZAccessor( ) ) ) + '\n';
		## _text += '\n\t\t\tNote: I changed VectorBase Parent to Pos so x, y and z did not need to be duplicated - this means __x, __y, __z are inaccessible but we can still use Get<Name>Accessor( ) for the same result.. It also means I do not need to duplicate __str__ because they all use self.Serialize( )\n';


		##
		return _text + '\n' + Acecool.Footer( ) + '\n\n';

		##
		## return Acecool.Header( _class.__name__ + ': Python Examples' ) + '\tExample Not Available\n\n\n';


	##
	##
	##
	def ENUM( self ):
		##
		##
		##
		def ShowExample( _name, _locals, _maps = [ ], *_args ):
			##
			_i = 0;
			_text = '';
			_title = '\n' + _name + ' - Key / Values:\n\n';
			_header = '\t' + String.FormatColumn( 50, 'ENUM_NAME' );

			## _text += 'globals( ) == _data ?? ' + str( globals( ) == _data ) + ' \n'
			## _text += 'locals( ) == _data ?? ' + str( locals( ) == _data ) + ' \n'
			## _text += 'type( _data ) ?? ' + str( type( _data ) ) + ' \n'
			## print( _text )
			## print( str( _data ) )

			## For each ENUM name,
			for _name in _args:
				##
				_i += 1;

				## Grab the value...
				_value = _locals.get( _name, 'Err' );

				## If we have just switched from normal enumeration to CONST MAP_s then add a space... between each because they can be quite large / long..
				if ( _name.startswith( 'MAP_' ) ):
					_text += '\n';

				## Output the ENUM Name
				_text += '\t' + String.FormatColumn( 50, _name );

				## If we supplied map data, and we're still outputting ENUM data, not the CONST Maps, then lets pull all MAP_*[ ENUM_VALUE ] value... and output it...
				if ( not _name.startswith( 'MAP_' ) and isinteger( _value ) and istable( _maps ) and len( _maps ) > 0 ):
					## For each map we included...
					for _map in _maps:
						if ( _i == 1 ):
							_header += String.FormatColumn( 80, 'MAP_*[ ENUM_VALUE ] ie MAP Value at ENUM_NAME Key' );

						##
						try:
							## Output the value stored at the ENUM Key location...
							## _text += String.FormatColumn( 80, _map.get( _value, 'err' ) )
							_text += String.FormatColumn( 80, _map[ _value ] );
						except IndexError as _error:
							_text += String.FormatColumn( 80, 'IndexError: {0} - Tried: {1} with a Max: {2}'.format( _error, _value, len( _map ) ) );


				## Output the ENUM or CONST Value ( Map data )
				_text += str( _locals.get( _name, 'Err' ) );

				_text += '';

				##
				_text += '\n';

			## Add final Header Text
			_header += 'ENUM_VALUE';


			## _text_list = [ ];
			## _text_list.insert( 0, 'Zero' );
			## _text_list.insert( 1, 'One' );
			## _text_list.insert( 2, 'Two' );
			## _text_list.insert( 4, 'Four' );
			## _text_list.insert( 8, 'Eight' );
			## _text_list.insert( 16, 'Sixteen' );
			## _text_list.insert( 32, 'Thirty Two' );
			## _text_list.insert( 64, 'Sixty Four' );
			## _text_list.insert( 128, 'One Hundred Twenty Eight' );
			## _text += '\n';
			## _text += '\n' + str( _text_list );
			## _text += '\n' + str( _text_list[ 5 ] );

			##
			return ( '~' * ( len( _header ) + 4 ) ) + '\n' + _title + _header + '\n' + ( '~' * ( len( _header ) + 4 ) ) + '\n' + _text + '\n\n';




		##
		_text = '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n';
		_text += '\n';
		_text += 'ENUM Example - Examples based on actual Garry\'s Mod AcecoolDev_Framework ENUMerations Generated by a near identical function...\n';
		_text += '\n';
		_text += 'Please Note: This ENUM system is very simple - you can map ENUM_ values through CONST_ MAP_ tables which makes it useful. You can not use the power feature for bitwise enumeration operations yet... I plan on looking into this soon.\n';
		_text += '\t\t\tFor the time being, you can set the start value from negative whatever to positive whatever and if they are consecutive it will work, but for some reason if you do power of 2, it won\'t work...\n';
		_text += '\t\t\tI could resolve this by adding a new mapping layer, or by making a new ENUM class - but before I do that I\'ll look into how Python handles their internal system before reinventing the wheel... \n';
		_text += '\t\t\tI ported this system over because of its simplicity and how quickly I cans etup maps, etc...\n';
		_text += '\t\t\t\n';
		_text += '\t\t\t\n';
		_text += 'Important: ?You may use a starting enum value of 0 or less than 0.. If you use 1 or a higher value than 1, the enumeration will not work with any of the maps you include. I will be working on a solution - but for now, 2 ** _start + _i can\'t be used, and _start > 0 can\'t be used...\n';
		_text += '\t\t\tOn a side note: Enumeration values really do not matter unless you need them for bitwise operations - so, for now, I\'m removing the ability to set the starting value and I\'ll leave it at -1 for now.. or 0...\n';
		_text += '\t\t\t\n';
		_text += '\n';
		_text += '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n';


		TYPE_INVALID, TYPE_NIL, TYPE_BOOL, TYPE_LIGHTUSERDATA, TYPE_NUMBER, TYPE_STRING, TYPE_TABLE, TYPE_FUNCTION, TYPE_USERDATA, TYPE_THREAD, TYPE_ENTITY, TYPE_VECTOR, TYPE_ANGLE, TYPE_PHYSOBJ, TYPE_SAVE, TYPE_RESTORE, TYPE_DAMAGEINFO, TYPE_EFFECTDATA, TYPE_MOVEDATA, TYPE_RECIPIENTFILTER, TYPE_USERCMD, TYPE_SCRIPTEDVEHICLE, TYPE_MATERIAL, TYPE_PANEL, TYPE_PARTICLE, TYPE_PARTICLEEMITTER, TYPE_TEXTURE, TYPE_USERMSG, TYPE_CONVAR, TYPE_IMESH, TYPE_MATRIX, TYPE_SOUND, TYPE_PIXELVISHANDLE, TYPE_DLIGHT, TYPE_VIDEO, TYPE_FILE, TYPE_LOCOMOTION, TYPE_PATH, TYPE_NAVAREA, TYPE_SOUNDHANDLE, TYPE_NAVLADDER, TYPE_PARTICLESYSTEM, TYPE_PROJECTEDTEXTURE, TYPE_COLOR, TYPE_NPC, TYPE_PLAYER, TYPE_WEAPON, TYPE_VEHICLE, TYPE_CSENT, TYPE_NEXTBOT, TYPE_ACCESSORS, ENUM_LIST_DATA_TYPES, MAP_TYPE_IDS, MAPR_TYPE_IDS, MAP_TYPE_NAMES, MAPR_TYPE_NAMES = ENUM( None, [ '#typeid_invalid', '#typeid_nil', '#typeid_bool', '#typeid_lightuserdata', '#typeid_number', '#typeid_string', '#typeid_table', '#typeid_function', '#typeid_userdata', '#typeid_thread', '#typeid_entity', '#typeid_vector', '#typeid_angle', '#typeid_physobj', '#typeid_save', '#typeid_restore', '#typeid_damageinfo', '#typeid_effectdata', '#typeid_movedata', '#typeid_recipientfilter', '#typeid_usercmd', '#typeid_scriptedvehicle', '#typeid_material', '#typeid_panel', '#typeid_particle', '#typeid_particleemitter', '#typeid_texture', '#typeid_usermsg', '#typeid_convar', '#typeid_imesh', '#typeid_matrix', '#typeid_sound', '#typeid_pixelvishandle', '#typeid_dlight', '#typeid_video', '#typeid_file', '#typeid_locomotion', '#typeid_path', '#typeid_navarea', '#typeid_soundhandle', '#typeid_navladder', '#typeid_particlesystem', '#typeid_projectedtexutre', '#typeid_color', '#typeid_npc', '#typeid_player', '#typeid_weapon', '#typeid_vehicle', '#typeid_csent', '#typeid_nextbot', '#typeid_accessor_structure' ], [ 'Invalid type', 'nil', 'boolean', 'TYPE_LIGHTUSERDATA', 'number', 'string', 'table', 'function', 'userdata', 'thread', 'Entity', 'Vector', 'Angle', 'PhysObj', 'ISave', 'IRestore', 'CTakeDamageInfo', 'CEffectData', 'CMoveData', 'CRecipientFilter', 'CUserCmd', 'TYPE_SCRIPTEDVEHICLE', 'IMaterial', 'Panel', 'CLuaParticle', 'CLuaEmitter', 'ITexture', 'bf_read', 'ConVar', 'IMesh', 'VMatrix', 'CSoundPatch', 'pixelvis_handle_t', 'dlight_t', 'IVideoWriter', 'File', 'CLuaLocomotion', 'PathFollower', 'CNavArea', 'IGModAudioChannel', 'CNavLadder', 'CNewParticleEffect', 'ProjectedTexture', 'Color', 'NPC', 'Player', 'Weapon', 'Vehicle', 'CSEnt', 'NextBot', 'AccessorStructure' ] );

		## ENUM_LIST_DATA_TYPES					MAP_TYPE_IDS					MAPR_TYPE_IDS					MAP_TYPE_NAMES					MAPR_TYPE_NAMES
		_text += ShowExample( 'Data-Types', locals( ), [ MAP_TYPE_IDS, MAP_TYPE_NAMES ], 'TYPE_INVALID', 'TYPE_NIL', 'TYPE_BOOL', 'TYPE_LIGHTUSERDATA', 'TYPE_NUMBER', 'TYPE_STRING', 'TYPE_TABLE', 'TYPE_FUNCTION', 'TYPE_USERDATA', 'TYPE_THREAD', 'TYPE_ENTITY', 'TYPE_VECTOR', 'TYPE_ANGLE', 'TYPE_PHYSOBJ', 'TYPE_SAVE', 'TYPE_RESTORE', 'TYPE_DAMAGEINFO', 'TYPE_EFFECTDATA', 'TYPE_MOVEDATA', 'TYPE_RECIPIENTFILTER', 'TYPE_USERCMD', 'TYPE_SCRIPTEDVEHICLE', 'TYPE_MATERIAL', 'TYPE_PANEL', 'TYPE_PARTICLE', 'TYPE_PARTICLEEMITTER', 'TYPE_TEXTURE', 'TYPE_USERMSG', 'TYPE_CONVAR', 'TYPE_IMESH', 'TYPE_MATRIX', 'TYPE_SOUND', 'TYPE_PIXELVISHANDLE', 'TYPE_DLIGHT', 'TYPE_VIDEO', 'TYPE_FILE', 'TYPE_LOCOMOTION', 'TYPE_PATH', 'TYPE_NAVAREA', 'TYPE_SOUNDHANDLE', 'TYPE_NAVLADDER', 'TYPE_PARTICLESYSTEM', 'TYPE_PROJECTEDTEXTURE', 'TYPE_COLOR', 'TYPE_NPC', 'TYPE_PLAYER', 'TYPE_WEAPON', 'TYPE_VEHICLE', 'TYPE_CSENT', 'TYPE_NEXTBOT', 'TYPE_ACCESSORS', 'MAP_TYPE_IDS', 'MAP_TYPE_NAMES' );

		REALM_NOLOAD, REALM_UNKNOWN, REALM_CLIENT, REALM_SERVER, REALM_SHARED, ENUM_LIST_REALMS, MAP_REALMS, MAPR_REALMS, MAP_REALM_NAMES, MAPR_REALM_NAMES = ENUM( None, [ '#realm_noload', '#realm_unknown', '#realm_client', '#realm_server', '#realm_shared' ], [ 'NoLoad Realm', 'Ignored Realm', 'Client Realm', 'Server Realm', 'Shared Realm' ] );
		_text += ShowExample( 'Realm', locals( ), [ MAP_REALMS, MAP_REALM_NAMES ], 'REALM_NOLOAD', 'REALM_UNKNOWN', 'REALM_CLIENT', 'REALM_SERVER', 'REALM_SHARED', 'MAP_REALMS', 'MAP_REALM_NAMES' );

		FILE_TYPE_UNKNOWN, FILE_TYPE_ENTITY, FILE_TYPE_SWEP, FILE_TYPE_GM, FILE_TYPE_FRAMEWORK, FILE_TYPE_BASE, FILE_TYPE_ADDON, ENUM_LIST_FILE_TYPES, MAP_FILE_TYPES, MAPR_FILE_TYPES = ENUM( None, [ '#file_type_unknown', '#file_type_entity', '#file_type_swep', '#file_type_gm', '#file_type_framework', '#file_type_base', '#file_type_addon' ] );
		_text += ShowExample( 'File-Type', locals( ), [ MAP_FILE_TYPES ], 'FILE_TYPE_UNKNOWN', 'FILE_TYPE_ENTITY', 'FILE_TYPE_SWEP', 'FILE_TYPE_GM', 'FILE_TYPE_FRAMEWORK', 'FILE_TYPE_BASE', 'FILE_TYPE_ADDON', 'MAP_FILE_TYPES' );

		GAMEMODE_PATH_UNKNOWN, GAMEMODE_PATH_GMOD, GAMEMODE_PATH_C, GAMEMODE_PATH_BASE, GAMEMODE_PATH_FRAMEWORK, GAMEMODE_PATH_ACTIVE_GM, GAMEMODE_PATH_ADDONS, ENUM_LIST_GAMEMODE_PATHS, MAP_GAMEMODE_PATHS, MAPR_GAMEMODE_PATHS = ENUM( None, [ '#gamemode_path_unknown', '#gamemode_path_gmod', '#gamemode_path_c', '#gamemode_path_base', '#gamemode_path_framework', '#gamemode_path_active_gm', '#gamemode_path_addons' ] );
		_text += ShowExample( 'Game-Mode Path', locals( ), [ MAP_GAMEMODE_PATHS ], 'GAMEMODE_PATH_UNKNOWN', 'GAMEMODE_PATH_GMOD', 'GAMEMODE_PATH_C', 'GAMEMODE_PATH_BASE', 'GAMEMODE_PATH_FRAMEWORK', 'GAMEMODE_PATH_ACTIVE_GM', 'GAMEMODE_PATH_ADDONS', 'MAP_GAMEMODE_PATHS' );

		AUTOLOADER_AS_UNKNOWN, AUTOLOADER_AS_GAMEMODE, AUTOLOADER_AS_ADDON, ENUM_LIST_AUTOLOADER_AS, MAP_AUTOLOADER_LOADING_METHOD, MAPR_AUTOLOADER_LOADING_METHOD = ENUM( None, [ '#autoloader_loading_method_unknown', '#autoloader_loading_method_gamemode', '#autoloader_loading_method_addon' ] );
		_text += ShowExample( 'AutoLoader', locals( ), [ MAP_AUTOLOADER_LOADING_METHOD ], 'AUTOLOADER_AS_UNKNOWN', 'AUTOLOADER_AS_GAMEMODE', 'AUTOLOADER_AS_ADDON', 'MAP_AUTOLOADER_LOADING_METHOD' );

		AUTOLOADER_INSTRUCTION_FOLDER_REALM, AUTOLOADER_INSTRUCTION_PROCESS_LATER, AUTOLOADER_INSTRUCTION_FOLDER_REDIRECT, AUTOLOADER_INSTRUCTION_ALTERNATE_FILE_READ, ENUM_LIST_AUTOLOADER_INSTRUCTIONS = ENUM( 4 );
		_text += ShowExample( 'AutoLoader Instruction', locals( ), [ ], 'AUTOLOADER_INSTRUCTION_FOLDER_REALM', 'AUTOLOADER_INSTRUCTION_PROCESS_LATER', 'AUTOLOADER_INSTRUCTION_FOLDER_REDIRECT', 'AUTOLOADER_INSTRUCTION_ALTERNATE_FILE_READ' );

		LOADER_STATUS_INIT, LOADER_STATUS_REFRESH, LOADER_STATUS_SHUTDOWN, ENUM_LIST_LOADER_STATUS, MAP_LOADER_STATUS, MAPR_LOADER_STATUS, MAP_LOADER_STATUS_FUNC_NAME, MAPR_LOADER_STATUS_FUNC_NAME = ENUM( None, [ '#loader_status_init', '#loader_status_refresh', '#loader_status_shutdown' ], [ 'OnInit', 'OnRefresh', 'OnShutdown' ] );
		_text += ShowExample( 'Loader Status', locals( ), [ MAP_LOADER_STATUS, MAP_LOADER_STATUS_FUNC_NAME ], 'LOADER_STATUS_INIT', 'LOADER_STATUS_REFRESH', 'LOADER_STATUS_SHUTDOWN', 'MAP_LOADER_STATUS', 'MAP_LOADER_STATUS_FUNC_NAME' );

		TIMEKEEPER_LERP_MODE_FADE_OUT, TIMEKEEPER_LERP_MODE_DEFAULT, TIMEKEEPER_LERP_MODE_FADE_IN, ENUM_LIST_TIMEKEEPER_LERP, MAP_TIMEKEEPER_LERP_MODE, MAPR_TIMEKEEPER_LERP_MODE = ENUM( None, [ '#timekeeper_lerp_mode_fade_out', '#timekeeper_lerp_mode_default', '#timekeeper_lerp_mode_fade_in' ] );
		_text += ShowExample( 'TimeKeeper Lerp Mode', locals( ), [ MAP_TIMEKEEPER_LERP_MODE ], 'TIMEKEEPER_LERP_MODE_FADE_OUT', 'TIMEKEEPER_LERP_MODE_DEFAULT', 'TIMEKEEPER_LERP_MODE_FADE_IN', 'MAP_TIMEKEEPER_LERP_MODE' );

		_text += '\n';
		_text += '\n';
		_text += '\n';
		_text += '\n';


		return _text;


##
## A Stack is exactly as it sounds. Imagine a stack of papers on your desk and if you add a piece of paper, you can't grab any other piece of paper until the top one is removed. This is a stack.
##
class Stack( ):
	##
	##
	##
	stack_data = None;


	##
	##
	##
	def __init__( self, _stack = None ):
		##
		self.stack_data = [ ];

		##
		if ( _stack != None and IsList( _stack ) ):
			self.stack_data = _stack;


	##
	##
	##
	def Add( self, _value = None ):
		if ( _value != None ):
			_stack.append( );


	##
	##
	##
	def Pop( self ):
		return _stack.pop( len( self.stack_data ) - 1 );


##
## A Queue is like a stack, but instead of only being able to remove the last element added which is ( First in Last out or Last In First Out ) we take items out in first come first serve or ( First In First Out ) order as you'd expect with a Queue such as a line at a Store checkout lane...
##
class Queue( Stack ):
	##
	##
	##
	def Pop( self ):
		return super( ).Pop( 0 );


##
##
##
class DatabaseBase( ):
	pass;
class Database( DatabaseBase ):
	##
	##
	##
	__name__ = 'Database';


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		_text					= '';

		##


		## _text += Acecool.Header( 'Database.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += Acecool.Footer( ) + '\n';


		_text += Acecool.Header( 'Database.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += Acecool.Footer( ) + '\n';


		## _text += Acecool.Header( 'Database.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += Acecool.Footer( ) + '\n';

		return _text;


	##
	##
	##
	def __init__( self, _name = 'Acecool', _path = '' ):
		self.connection		= sqlite3.connect( _path + _name + '.db' );
		self.cursor			= self.connection.cursor( );



	##
	##
	##
	def Query( self, _query, _values, _no_commit = False ):
		self.cursor.execute( _query, _values );

		if ( not _no_commit ):
			self.connection.commit( );


	##
	##
	##
	def Fetch( self, _table, _rows, _values = None ):
		## self.Query( 'CREATE TABLE IF NOT EXISTS ' + _table + ' ( ' + _rows + ' )' );
		## _values = ('RHAT',);
		self.cursor.execute( 'SELECT * FROM ' + _table + ' WHERE ' + _rows, _values );

		return self.cursor.fetchone( );


	##
	##
	##
	## def Update( self, _table, _where, _where_data, _values, _columns = 1 ):
	def Update( self, _table, _rows, _values = None ):
		self.cursor.execute( 'UPDATE ' + _table + ' SET ' + _rows + '', _values );
		self.connection.commit( );

		## c.executemany('INSERT INTO stocks VALUES (?,?,?,?,?)', purchases);
		## _column_qs = '?,' * _columns;
		## _column_qs = _column_qs[ : len( _column_qs ) - 1 ];
		## self.cursor.execute( 'UPDATE ' + _table + ' VALUES ( ' + _column_qs + ' ) WHERE ' + _where + ' = ' + _where_data + ' LIMIT 1', _values );


	##
	##
	##
	def CreateTableIfNotExists( self, _table, _rows, _values = None ):
		self.cursor.execute( 'CREATE TABLE IF NOT EXISTS ' + _table + ' ( ' + _rows + ' )' );
		self.connection.commit( );




	##
	##
	##
	def Close( self ):
		self.cursor.close( );


##
## Logic / Operation Helpers
##


##
## Misc / Generic Based Helpers
##




##
##
##
## class LogicBase( ):
## 	pass;
class Logic:
	##
	##
	##
	__name__ = 'Logic';


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		_text					= '';

		##


		## _text += Acecool.Header( 'Logic.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += Acecool.Footer( ) + '\n';


		_text += Acecool.Header( 'Logic.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += Acecool.Footer( ) + '\n';


		## _text += Acecool.Header( 'Logic.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += Acecool.Footer( ) + '\n';

		return _text;


	##
	##
	##
	def TernaryFunc( _statement = True, _true = None, _false = None ):
		return ( _false, _true )[ _statement ];



##
## Data-Type Based Helpers
##


##
## Math Class
##
## class MathBase( ):
## 	pass;
class Math:
	##
	##
	##
	__name__ = 'Math';


	##
	##
	##
	def Clamp( _number, _min, _max ):
		if ( _number > _max ): return _max;
		if ( _number < _max ): return _min;
		return _number;


	##
	##
	##
	def min( *_numbers ):
		return min( *_numbers );


	##
	##
	##
	def max( *_numbers ):
		return max( *_numbers );


##
##
##
class System( ):
	##
	## Returns the Computer Operating System as Windows, Linux or Darwin for MAC
	##
	def GetOS( ):
		return platform.system( );


	##
	## Returns the Computer Name
	##
	def GetName( ):
		return platform.node( );


	##
	## Returns the Distribution information...
	##
	def GetDistribution( ):
		return platform.linux_distribution( );


	##
	## Returns the Processor Name
	##
	def GetProcessorName( ):
		return platform.processor( );


	##
	## Returns the Machine Type
	##
	def GetMachineType( ):
		return platform.machine( );


	##
	## Returns the Version
	##
	def GetVersion( ):
		return platform.version( );


	##
	## Returns the Release
	##
	def GetVersion( ):
		return platform.release( );


	##
	## Helper - Returns the path delimeter for the operating system... \\ for Windows, / for everything else...
	##
	def GetPathDelimiter( ):
		return ( '/', '\\' )[ System.GetOS( ) == 'Windows' ];



##
## String Class
##
## class StringBase( ):
## 	pass;
class String:
	##
	##
	##
	__name__ = 'String';


	##
	## Shows Example Usage and Output for all Class Functions...
	##
	def __example__( _depth = 1 ):
		##
		## Truncate Example...
		##
		_col_key					= 20;
		_col_eq						= 10;
		_col_res					= 50;
		_row_eq						= '==';
		_example_text				= 'Testing';

		##;
		_col_a_text					= '10  10  10';
		_col_b_text					= '__|><|';
		_col_c_text					= 'T  h  i  r  t y';

		##;
		_depth_header				= _depth;
		_depth_header_prefix		= _depth * '\t';

		_depth						= _depth + 1;
		_depth_prefix				= _depth * '\t';
		## _depth_prefix +;


		## ##
		## ## Header
		## ##
		## def Header( _header = '', _prefix = '', _suffix = '' ):
		## 	_text = _depth_prefix + '##\n';
		## 	_text += _depth_prefix + '## ' + _header + '\n';
		## 	_text += _depth_prefix + '##\n';
		## 	_text += _depth_prefix + _prefix + String.FormatColumnEx( '-', _col_key + _col_eq + _col_res, '' ) + _suffix + '';

		## 	return _text;


		## ##
		## ## Footer
		## ##
		## def Footer( _prefix = '', _suffix = '\n\n' ):
		## 	_text = _depth_prefix + _prefix + String.FormatColumnEx( '~', _col_key + _col_eq + _col_res, '' ) + _suffix;

		## 	return _text;

		_text = '';
		_text += '';

		_text += Acecool.Header( 'Index for Examples', '', '', _depth_header );
		_text += _depth_prefix + '## String.FormatColumn( [ _width, _text ]+ ... )' + '\n';
		_text += _depth_prefix + '## String.FormatSimpleColumn( _width, [ _text ] ... ) ' + '\n';
		_text += _depth_prefix + '## String.FormatColumnEx( _blank_chars, [ _width, _text ]+ ... ) ' + '\n';
		_text += _depth_prefix + '## String.Truncate( _text, _max_len, _suffix = "..", _absolute_max_len = True )' + '\n';
		_text += _depth_prefix + '## DATA_TYPE Output' + '\n';
		_text += _depth_prefix + '## String.reverse( _text )' + '\n';
		_text += _depth_prefix + '## String.repeat( _text, _count = 1 )' + '\n';
		_text += _depth_prefix + '## String.ToString( [ _text ]+ )' + '\n';
		_text += _depth_prefix + '## String.ToStringKeyedVarArgs( [ _text ]+ )' + '\n';
		_text += _depth_prefix + '## String.Safe( _text = '' )' + '\n';
		_text += _depth_prefix + '## String.IndexOf( _needle, _haystack )' + '\n';
		_text += _depth_prefix + '## String.IndexOfExists( _needle, _haystack, _case_sensitive )' + '\n';
		_text += _depth_prefix + '## String.SubStr( _text, _start, _end )' + '\n';
		_text += _depth_prefix + '## String.SubStrEx( _text, _start, _end )' + '\n';
		_text += _depth_prefix + '## String.SubStrNextChars( _text = '', _start = 0, _chars = 1 )' + '\n';
		_text += _depth_prefix + '## String.Strip( _text, _index, _len )' + '\n';
		_text += _depth_prefix + '## String.StripAll( _needle, _haystack, _case_sensitive = True )' + '\n';
		_text += _depth_prefix + '## String.FindAll( _needle, _haystack )' + '\n';
		_text += _depth_prefix + '## String.StripQuotes( _text )' + '\n';
		_text += _depth_prefix + '## String.GetQuotesType( _text )' + '\n';
		_text += _depth_prefix + '## String.HasQuotes( _text )' + '\n';
		_text += _depth_prefix + '## String.StartChars( _text, _count )' + '\n';
		_text += _depth_prefix + '## String.EndChars( _text, _count )' + '\n';
		_text += _depth_prefix + '## String.NotEndChars( _text, _count )' + '\n';
		_text += _depth_prefix + '## String.StartsWith( _needle, _haystack )' + '\n';
		_text += _depth_prefix + '## String.EndsWith( _needle, _haystack )' + '\n';
		_text += _depth_prefix + '## String.FormatColumnStripR( _width = 25, _text ='', *_varargs )' + '\n';
		_text += _depth_prefix + '## String.FormatSimpleColumnStripR( _width = 25, *_varargs )' + '\n';
		_text += _depth_prefix + '## String.GetFileName( _file = '' )' + '\n';
		_text += _depth_prefix + '## String.GetFileExt( _file = '' )' + '\n';
		_text += _depth_prefix + '## String.String.__example__( )' + '\n\n\n';
		## print( '## ' );
		## print( '## ' );
		_text += '';
		_text += '';


		_text += Acecool.Header( 'Note: All, or most, of the examples below are formatted into columns using:', '', '', _depth_header );
		_text += _depth_prefix + '## Please Also Note that I alternate blank character patterns to show the text is truncated if it reaches the max size... '+ '\n';
		_text += _depth_prefix + '## I am considering adding in a border option, so there ccan always be 1 char space, or you decide, between areas ( meaning subtracting from the allotted length, OR adding an extra char...'+ '\n';
		_text += _depth_prefix + '## I am also going to work on a Layout System so those old Dos Characters ( which made up the nice looking tables in the BIOS, etc... ) can be used as a buffer / organizer with header...'+ '\n';
		_text += _depth_prefix + '## Note: The blank char in the Ex format column function can be used to make interesting patterns as you can use 1 to many chars... Experiment to find what looks best to you for your debugging needs!'+ '\n';
		_text += _depth_prefix + '## '+ '\n';
		_text += _depth_prefix + 'String.FormatColumn( [ _width, _text ] ... )'+ '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + 'ie: String.FormatColumn( 10,	"Test 10 Width",		20,	"Test 20 Width",		30,	"Test 30 Width" )'+ '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + String.FormatColumnEx( _col_a_text, 10,	'' ) + String.FormatColumnEx( _col_b_text, 20,	'' ) + String.FormatColumnEx( _col_c_text, 30,	'' )+ '\n';
		_text += _depth_prefix + String.FormatColumn( 10,	"Test 10 Width",		20, "Test 20 Width",		30,	"Test 30 Width" ) + '<Whitespace OR Substitute Chars Fill Space>'+ '\n';
		_text += _depth_prefix + String.FormatColumnEx( _col_a_text, 10,	'' ) + String.FormatColumnEx( _col_b_text, 20,	'' ) + String.FormatColumnEx( _col_c_text, 30,	'' )+ '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + 'String.FormatSimpleColumn( _width, [ _text ] ... )'+ '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + 'ie: String.FormatSimpleColumn( 10, "Test 10", "Test 20", "Test 30" )'+ '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + String.FormatColumnEx( _col_a_text, 10,	'' ) + String.FormatColumnEx( _col_b_text, 10,	'' ) + String.FormatColumnEx( _col_c_text, 10,	'' )+ '\n';
		_text += _depth_prefix + String.FormatSimpleColumn( 10, "Test 10 Width", "Test 20 Width", "Test 30 Width" ) + '<Whitespace OR Substitute Chars Fill Space>'+ '\n';
		_text += _depth_prefix + String.FormatColumnEx( _col_a_text, 10,	'' ) + String.FormatColumnEx( _col_b_text, 10,	'' ) + String.FormatColumnEx( _col_c_text, 10,	'' )+ '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + 'String.FormatColumnEx( _blank_chars, [ _width, _text ] ... )'+ '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + 'ie: String.FormatColumnEx( "-",		60,	"" )'+ '\n';
		_text += _depth_prefix + 'ie: String.FormatColumnEx( " ",		10,	"Test 10 Width",		20,	"Test 20 Width",		30, "Test 30 Width" )'+ '\n';
		_text += _depth_prefix + 'ie: String.FormatColumnEx( "-",		10,	"Test 10 Width",		20,	"Test 20 Width",		30, "Test 30 Width" )'+ '\n';
		_text += _depth_prefix + 'ie: String.FormatColumnEx( " -",		10,	"Test 10 Width",		20,	"Test 20 Width",		30, "Test 30 Width" )'+ '\n';
		_text += _depth_prefix + 'ie: String.FormatColumnEx( "- ",		10,	"Test 10 Width",		20,	"Test 20 Width",		30, "Test 30 Width" )'+ '\n';
		_text += _depth_prefix + 'ie: String.FormatColumnEx( " - ",		10,	"Test 10 Width",		20,	"Test 20 Width",		30, "Test 30 Width" )'+ '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + String.FormatColumnEx( '-',		60,	'' )+ '\n';
		_text += _depth_prefix + String.FormatColumnEx( '-', 		10,	'' ) + String.FormatColumnEx( '.', 20,	'' ) + String.FormatColumnEx( '-', 30,	'' )+ '\n';
		_text += _depth_prefix + String.FormatColumnEx( ' ',		10,	'Test 10 Width',		20, 'Test 20 Width',		30, 'Test 30 Width' ) + '<Whitespace OR Substitute Chars Fill Space>'+ '\n';
		_text += _depth_prefix + String.FormatColumnEx( '-',		10,	'Test 10 Width',		20, 'Test 20 Width',		30, 'Test 30 Width' ) + '<Whitespace OR Substitute Chars Fill Space>'+ '\n';
		_text += _depth_prefix + String.FormatColumnEx( ' -',		10,	'Test 10 Width',		20, 'Test 20 Width',		30, 'Test 30 Width' ) + '<Whitespace OR Substitute Chars Fill Space>'+ '\n';
		_text += _depth_prefix + String.FormatColumnEx( '- ',		10,	'Test 10 Width',		20, 'Test 20 Width',		30, 'Test 30 Width' ) + '<Whitespace OR Substitute Chars Fill Space>'+ '\n';
		_text += _depth_prefix + String.FormatColumnEx( ' - ',	10,	'Test 10 Width',		20, 'Test 20 Width',		30, 'Test 30 Width' ) + '<Whitespace OR Substitute Chars Fill Space>'+ '\n';
		_text += _depth_prefix + String.FormatColumnEx( '-', 10,	'' ) + String.FormatColumnEx( '.', 20,	'' ) + String.FormatColumnEx( '-', 30,	'' )+ '\n';
		## print(  );
		_text += Acecool.Footer( ) + '\n';

		_width = 8;
		_text += Acecool.Header( 'String.Truncate( <str> _text, <int> _max_len, <str> || <False> _suffix = "..", <bool> _absolute_max_len = True )', '', ' ' + str( _width ), _depth_header );
		_text += _depth_prefix + String.FormatColumn( _col_key, 'Testing',				_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'Testing',				_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width, '..', True ) ) + '\n';

		_width = 7;
		_text += Acecool.Footer( '', ' ' + str( _width ) );
		_text += _depth_prefix + String.FormatColumn( _col_key, 'Testing',				_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'Testing',				_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width, '..', True ) ) + '\n';

		_width = 6;
		_text += Acecool.Footer( '', ' ' + str( _width ) );
		_text += _depth_prefix + String.FormatColumn( _col_key, 'Testin',				_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'Test..',				_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width, '..', True ) ) + '\n';

		_width = 5;
		_text += Acecool.Footer( '', ' ' + str( _width ) );
		_text += _depth_prefix + String.FormatColumn( _col_key, 'Testi',				_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'Tes..',				_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width, '..', True ) ) + '\n';

		_width = 4;
		_text += Acecool.Footer( '', ' ' + str( _width ) );
		_text += _depth_prefix + String.FormatColumn( _col_key, 'Test',					_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'Te..',					_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width, '..', True ) ) + '\n';

		_width = 3;
		_text += Acecool.Footer( '', ' ' + str( _width ) );
		_text += _depth_prefix + String.FormatColumn( _col_key, 'Tes',					_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'T..',					_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width, '..', True ) ) + '\n';

		_width = 2;
		_text += Acecool.Footer( '', ' ' + str( _width ) );
		_text += _depth_prefix + String.FormatColumn( _col_key, 'Te',					_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '..',					_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width, '..', True ) ) + '\n';

		_width = 1;
		_text += Acecool.Footer( '', ' ' + str( _width ) );
		_text += _depth_prefix + String.FormatColumn( _col_key, 'T',					_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '.',					_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width, '..', True ) ) + '\n';

		_width = 0;
		_text += Acecool.Footer( '', ' ' + str( _width ) );
		_text += _depth_prefix + String.FormatColumn( _col_key, '',						_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '',						_col_eq, _row_eq,			_col_res,	String.Truncate( _example_text, _width, '..', True ) ) + '\n';
		_text += Acecool.Footer( '', ' ^ Finished\n\n' ) + '\n';


		_text += Acecool.Header( 'String.reverse( <str> _text )', '', '', _depth_header );
		_text += _depth_prefix + String.FormatColumn( _col_key, 'gnitseT',				_col_eq, _row_eq,			_col_res, String.reverse( _example_text ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'Testing',				_col_eq, _row_eq,			_col_res, String.reverse( String.reverse( _example_text ) ) ) + '\n';
		_text += Acecool.Footer( ) + '\n';


		_text += Acecool.Header( 'String.repeat( <str> _text, <int> _count = 1 )', '', '', _depth_header );
		_text += _depth_prefix + String.FormatColumn( _col_key, 'Testing',				_col_eq, _row_eq,			_col_res, String.repeat( _example_text ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'TestingTesting',		_col_eq, _row_eq,			_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		_text += Acecool.Footer( ) + '\n';

		_text += Acecool.Header( 'String.FormatColumn( 20, \'TYPE_*\', 50, str( TYPE_* ) ) - Used to display TYPE_ output...', '', '', _depth_header );
		_text += _depth_prefix + String.FormatColumn( _col_key, 'TYPE_ANY',				_col_eq, _row_eq,			_col_res,	str( TYPE_ANY ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'TYPE_INTEGER',			_col_eq, _row_eq,			_col_res,	str( TYPE_INTEGER ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'TYPE_ENUM',			_col_eq, _row_eq,			_col_res,	str( TYPE_ENUM ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'TYPE_COMPLEX',			_col_eq, _row_eq,			_col_res,	str( TYPE_COMPLEX ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'TYPE_FLOAT',			_col_eq, _row_eq,			_col_res,	str( TYPE_FLOAT ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'TYPE_BOOLEAN',			_col_eq, _row_eq,			_col_res,	str( TYPE_BOOLEAN ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'TYPE_STRING',			_col_eq, _row_eq,			_col_res,	str( TYPE_STRING ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'TYPE_TYPE',			_col_eq, _row_eq,			_col_res,	str( TYPE_TYPE ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'TYPE_TUPLE',			_col_eq, _row_eq,			_col_res,	str( TYPE_TUPLE ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'TYPE_LIST',			_col_eq, _row_eq,			_col_res,	str( TYPE_LIST ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'TYPE_SET',				_col_eq, _row_eq,			_col_res,	str( TYPE_SET ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'TYPE_DICT',			_col_eq, _row_eq,			_col_res,	str( TYPE_DICT ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'TYPE_FUNCTION',		_col_eq, _row_eq,			_col_res,	str( TYPE_FUNCTION ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'TYPE_METHOD',			_col_eq, _row_eq,			_col_res,	str( TYPE_METHOD ) ) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, 'TYPE_OBJECT',			_col_eq, _row_eq,			_col_res,	str( TYPE_OBJECT ) ) + '\n';
		_text += Acecool.Footer( ) + '\n';



		_text += Acecool.Header( 'String.ToString( _text, [ _text ]* ) - This is used to quickly convert a wide range of elements to string without calling str( ) on all of them..', '', '', _depth_header );
		_text += _depth_prefix + String.ToString( _col_key, 'Testing',					_col_res, String.repeat( _example_text ) ) + '\n';
		_text += _depth_prefix + String.ToString( _col_key, 'TestingTesting',				_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		_text += Acecool.Footer( ) + '\n';


		_text += Acecool.Header( 'String.ToStringKeyedVarArgs( _text, [ _text ]* ) - This is used to create copy / paste code for Dicts, etc.. Or to organize debugging data of a list...', '', '', _depth_header );
		_text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		_text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		_text += Acecool.Footer( ) + '\n';



		_dict = {
			"one":		1,
			"two":		2,
			"three":	3,
			"four":		4,
		};

		_tuple = (
			"one",
			"two",
			"three",
			"four",
		);

		_list = [
			"one",
			"two",
			"three",
			"four",
		];

		_col_key = 50;
		_col_func = 100;
		_col_res = 150;

		_text += Acecool.Header( 'Dict.SetupQuickLookupTable[ Ex ]( _table [ , _default_value, _use_key_instead_of_value ]* ) - This is used to create an O( 1 ) map!\n' + _depth_header_prefix + '## Note: Non Ex will automatically use the appropriate _use_key_instead_of_value option for _dicts vs _tuples and _lists', '', '', _depth_header );
		_text += _depth_prefix + str( _dict ) + '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + str( _dict.fromkeys( _dict, True ) ) + '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + str( _dict.fromkeys( _dict, True ) ) + '\n';
		_text += _depth_prefix + str( _dict.fromkeys( _dict, None ) ) + '\n';
		_text += _depth_prefix + str( _dict.fromkeys( _dict, False ) ) + '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + 'Dict.FromValues - Setting each value to be True - First uses Value, second uses Key as identifier... Note: Keys are one / two / three / four -- Values are 1 / 2 / 3 / 4' + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '1: KEY = True',							_col_res, str( Dict.FromValues( _dict, True ) )														) + '\n';
		_text += _depth_prefix + '\n\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '2: VALUE = True',							_col_res, str( Dict.FromValues( _dict, True, False ) )												) + '\n';
		_text += _depth_prefix + '\n\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '3: VALUE = KEY',							_col_res, str( Dict.FromValues( _dict, lambda _dict, _key, _value: _key, False ) )					) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '4: VALUE = VALUE',							_col_res, str( Dict.FromValues( _dict, lambda _dict, _key, _value: _value, False ) )					) + '\n';
		_text += _depth_prefix + '\n\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '5: KEY = KEY',								_col_res, str( Dict.FromValues( _dict, lambda _dict, _key, _value: _key ) )							) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '6: KEY = VALUE',							_col_res, str( Dict.FromValues( _dict, lambda _dict, _key, _value: _value ) )						) + '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + 'Dict.SetupQuickLookupTable - 1: Default = Value = type( Value ) ... 2: Returns _key for value ... 3: Returns _value for value' + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '1.1: KEY = str( type( KEY ) )',			_col_func,	'Dict.SetupQuickLookupTable( _dict )',																			_col_res, str( Dict.SetupQuickLookupTable( _dict ) )													) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '1.2: VALUE = True',						_col_func,	'Dict.SetupQuickLookupTableEx( _dict )',																		_col_res, str( Dict.SetupQuickLookupTableEx( _dict ) )													) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '1.3: VALUE = str( type( VALUE ) )',		_col_func,	'Dict.SetupQuickLookupTable( _tuple )',																			_col_res, str( Dict.SetupQuickLookupTable( _tuple ) )													) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '1.4: KEY = str( type( KEY ) )',			_col_func,	'Dict.SetupQuickLookupTable( _list )',																			_col_res, str( Dict.SetupQuickLookupTable( _list ) )													) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '1.5: KEY = True )',						_col_func,	'Dict.SetupQuickLookupTableEx( _tuple, True, True )',															_col_res, str( Dict.SetupQuickLookupTableEx( _tuple, True, True ) )													) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '1.6: KEY = True )',						_col_func,	'Dict.SetupQuickLookupTableEx( _list, True, True )',															_col_res, str( Dict.SetupQuickLookupTableEx( _list, True, True ) )													) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '1.7: VALUE = True )',						_col_func,	'Dict.SetupQuickLookupTableEx( _tuple, True, False )',															_col_res, str( Dict.SetupQuickLookupTableEx( _tuple, True, False ) )													) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '1.8: VALUE = True )',						_col_func,	'Dict.SetupQuickLookupTableEx( _list, True, False )',															_col_res, str( Dict.SetupQuickLookupTableEx( _list, True, False ) )													) + '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '2.1: KEY = None',							_col_func,	'Dict.SetupQuickLookupTableEx( _dict, None, True )',															_col_res, str( Dict.SetupQuickLookupTableEx( _dict, None, True ) )													) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '2.2: KEY = KEY',							_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: _key ), True )',							_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: _key ), True ) )						) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '2.3: KEY = type( KEY )',					_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: type( _key ) ), True )',					_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: type( _key ) ), True ) )				) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '2.4: KEY = str( type( KEY ) )',			_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: str( type( _key ) ) ), True )',				_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: str( type( _key ) ) ), True ) )				) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '2.5: KEY = VALUE',							_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: _value ), True )',							_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: _value ), True ) )						) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '2.6: KEY = type( VALUE )',					_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: type( _value ) ), True )',					_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: type( _value ) ), True ) )				) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '2.7: KEY = str( type( VALUE ) )',			_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: str( type( _value ) ) ), True )',			_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: str( type( _value ) ) ), True ) )				) + '\n';
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '3.1: VALUE = None',						_col_func,	'Dict.SetupQuickLookupTableEx( _dict, None, False )',															_col_res, str( Dict.SetupQuickLookupTableEx( _dict, None, False ) )													) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '3.2: VALUE = KEY',							_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: _key ) )',									_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: _key ) ) )						) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '3.3: VALUE = type( KEY )',					_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: type( _key ) ) )',							_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: type( _key ) ) ) )				) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '3.4: VALUE = str( type( KEY ) )',			_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: str( type( _key ) ) ) )',					_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: str( type( _key ) ) ) ) )				) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '3.5: VALUE = VALUE',						_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: _value ) )',								_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: _value ) ) )						) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '3.6: VALUE = type( VALUE )',				_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: type( _value ) ) )',						_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: type( _value ) ) ) )				) + '\n';
		_text += _depth_prefix + String.FormatColumn( _col_key, '3.7: VALUE = str( type( VALUE ) )',		_col_func,	'Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: str( type( _value ) ) ) )',					_col_res, str( Dict.SetupQuickLookupTableEx( _dict, ( lambda _dict, _key, _value: str( type( _value ) ) ) ) )				) + '\n';

		_text += Acecool.Footer( ) + '\n';


		## print( String.FormatColumn( 30, '3: VALUE = type( VALUE )',				150, str( Dict.SetupQuickLookupTable( _dict, lambda _dict, _key, _value: _value ) )				) );
		## print( );
		## print( String.FormatColumn( 30, '4: KEY = type( KEY )',					150, str( Dict.SetupQuickLookupTable( _dict, lambda _dict, _key, _value: _key, True ) ) 		) );
		## print( String.FormatColumn( 30, '5: VALUE = type( VALUE )',				150, str( Dict.SetupQuickLookupTable( _dict, lambda _dict, _key, _value: _value, True ) )		) );
		## print( String.FormatColumn( 30, '6: KEY = type( KEY )',					150, str( Dict.SetupQuickLookupTable( _dict, True, True ) )										) );
		## print( String.FormatColumn( 30, '7: VALUE = type( VALUE )',				150, str( Dict.SetupQuickLookupTable( _dict, True, False ) )									) );
		## print( );
		## print( String.FormatColumn( 30, '8: KEY = type( KEY )',					150, str( Dict.SetupQuickLookupTable( _dict, lambda _dict, _key, _value: _key, False ) )		) );
		## print( String.FormatColumn( 30, '9: VALUE = type( VALUE )',				150, str( Dict.SetupQuickLookupTable( _dict, lambda _dict, _key, _value: _value, False ) ) 		) );


		return _text;


	##
	## Converts VarArgs into a comma delimited string ( could also use join )
	##
	def ToString( *_args ):
		_text = '';

		if ( len( _args ) > 0 ):
			for _arg in _args:
				_text += str( _arg ) + ', ';

		return _text[ : -2 ];


	##
	## Converts VarArgs into X = Y, bi-delimited string
	##
	def ToStringKeyedVarArgs( *_args ):
		_text = '';

		if ( len( _args ) > 0 ):
			for _i, _arg in enumerate( _args ):
				_text += ( '"', '' )[ _i % 2 != 0 ] + str( _arg ) + ( '" = ', ', ' )[ _i % 2 != 0 ];

		return _text[ : -2 ];



	##
	## Expand Vars System
	##
	def ExpandVariables( _text, _map, _extras = None ):
		## Copy the map if we have any dynamic entries or extras to add...
		_map = Table.MapKeysValuesToExtra( _map, _extras );  # _map.copy( );

		## ##
		## if ( _extras != None ):
		## 	_map.update( _extras );

		##
		return _text.format( **_map );


	##
	## Expand Vars System - Takes separated / unprepared keys table and values table, combines them into keys = values single map table, and allows adding extras for dynamic options..
	##
	def ExpandKeyMapVariables( _text, _keys, _values, _extras = None ):
		##
		_map = Table.MapKeysToValues( _keys, _values ); #ENUM_MAP( _keys, _values );

		##
		return String.ExpandVariables( _text, _map, _extras );


	##
	## Returns the string in reverse
	##
	def reverse( _text = '' ):
		return _text[ : : - 1 ];


	##
	## String.repeat
	##
	def repeat( _chars, _count = 1 ):
		return _chars * _count;


	##
	## Returns a safe file-name...
	##
	def Safe( _text = '' ):
		_text = _text.replace( ':', '_' );
		_text = _text.replace( '\\', '_' );
		_text = _text.replace( '/', '_' );
		_text = _text.replace( ' ', '_' );
		_text = _text.replace( '__', '_' );
		_text = _text.replace( '*', '' );
		_text = _text.replace( '?', '' );
		_text = _text.replace( '|', '' );
		_text = _text.replace( '<', '' );
		_text = _text.replace( '>', '' );

		return _text;


	##
	## Alias
	##
	## def contains( )


	##
	## Returns the index of the _needle word first-char if discovered - otherwise it returns -1
	##
	def IndexOf( _needle, _haystack ):
		return _haystack.find( _needle ) + 1;


	##
	## Returns True or False depending if _needle exists in _haystack or not..
	##
	def IndexOfExists( _needle, _haystack, _case_sensitive = False ):
		if not _case_sensitive:
			return ( String.IndexOf( _needle, _haystack ) > 0 );
		else:
			return ( String.IndexOf( _needle.lower( ), _haystack.lower( ) ) > 0 );


	##
	## Returns a string with only numbers, or blank if no numbers found in supplied string...
	##
	def Numbers( _text = '' ):
		return ''.join( filter( str.isdigit, _text ) );


	##
	## Returns a string with only numbers
	##
	def ExtractNumbers( _text = '' ):
		return re.subn( '\D', '', _text )


	##
	## Returns a string without any numbers
	##
	def ExtractNonNumbers( _text = '' ):
		return re.subn( '\d', '', _text )


	##
	## SubString replacement
	##
	## Usage:
	##		_data = String.SubStr( _text, 0, 10 )
	##
	def SubStr( _text, _start, _end ):
		return _text[ _start : _end ];


	##
	## SubString replacement
	##
	def SubStrEx( _text, _start, _end ):
		return _text[ _start : _end - 1 ];


	##
	## Returns n chars after the len supplied ( useful for determining next char or chars from a pos, if number of chars is blank then 1 is used... )
	##
	def SubStrNextChars( _text = '', _start = 0, _chars = 1 ):
		return _text[ _start : _start + _chars ];


	##
	## Strips out text found at _index
	##
	def Strip( _text, _index, _len ):
		if ( _index > 0 ):
			return String.SubStr( _text, 0, _index ) + String.SubStr( _text, _index + _len, len( _text ) );
		else:
			return String.SubStr( _text, _len, len( _text ) );


	##
	## Strips all instances of _needle from _haystack
	##
	def StripAll( _needle, _haystack, _case_sensitive = True ):
		## Do while... ie run at least once...
		while( True ):
			## Make sure the data exists before running strip and repeating..
			_exists = String.IndexOfExists( _needle, _haystack, _case_sensitive );

			## It exists, lets do it...
			if ( _exists ):
				## It exists.. Grab the first index of where it is located...
				_index = String.IndexOf( _needle, _haystack ) - 1;

				## Strip our needle from the haystack
				_haystack = String.Strip( _haystack, _index, len( _needle ) );
			else:
				## We're done... exit loop...
				break;

		return _haystack;


	##
	## Truncate characters of a string after _len'nth char, if necessary... If _len is less than 0, don't truncate anything... Note: If you attach a suffix, and you enable absolute max length then the suffix length is subtracted from max length... Note: If the suffix length is longer than the output then no suffix is used...
	##
	## Usage: Where _text = 'Testing', _width = 4
	##		_data = String.Truncate( _text, _width )						== Test
	##		_data = String.Truncate( _text, _width, '..', True )			== Te..
	##
	## Equivalent Alternates: Where _text = 'Testing', _width = 4
	##		_data = String.SubStr( _text, 0, _width )						== Test
	##		_data = _text[  : _width ]										== Test
	## 		_data = ( _text )[  : _width ]									== Test
	##
	def Truncate( _text, _max_len = -1, _suffix = False, _absolute_max_len = True ):
		## Length of the string we are considering for truncation
		_len			= len( _text );

		## Whether or not we have to truncate
		_truncate		= ( False, True )[ _len > _max_len ];

		## Note: If we don't need to truncate, there's no point in proceeding...
		if ( not _truncate ):
			return _text;

		## The suffix in string form
		_suffix_str		= ( '',  str( _suffix ) )[ _truncate and _suffix != False ];

		## The suffix length
		_len_suffix		= len( _suffix_str );

		## Whether or not we add the suffix
		_add_suffix		= ( False, True )[ _truncate and _suffix != False and _max_len > _len_suffix ];

		## Suffix Offset
		_suffix_offset = _max_len - _len_suffix;
		_suffix_offset	= ( _max_len, _suffix_offset )[ _add_suffix and _absolute_max_len != False and _suffix_offset > 0 ];
		## _suffix_offset	= ( _suffix_offset, 0 )[ _suffix_offset < 0 ];

		## The truncate point.... If not necessary, then length of string.. If necessary then the max length with or without subtracting the suffix length... Note: It may be easier ( less logic cost ) to simply add the suffix to the calculated point, then truncate - if point is negative then the suffix will be destroyed anyway.
		## If we don't need to truncate, then the length is the length of the string.. If we do need to truncate, then the length depends on whether we add the suffix and offset the length of the suffix or not...
		_len_truncate	= ( _len, _max_len )[ _truncate ];
		_len_truncate	= ( _len_truncate, _max_len )[ _len_truncate <= _max_len ];
		## _suffix_offset	= 0;

		## If we add the suffix, add it... Suffix won't be added if the suffix is the same length as the text being output...
		if ( _add_suffix ):
			_text = _text[ 0 : _suffix_offset ] + _suffix_str + _text[ _suffix_offset: ];

		## Return the text after truncating...
		return _text[ : _len_truncate ];


	##
	## Finds all instances of _needle from _haystack
	##
	def FindAll( _needle, _haystack ):
		##
		_start = 0;
		_found = [ ];
		_exclude_found = { };
		_regex = re.compile( _needle );

		_exclude = Table.KeySet( 'end', 'for', 'in', 'do', 'then', 'else', 'elseif', 'elif', 'if', 'local', 'return', 'break', 'true', 'false', 'nil', 'function' );

		## Do while... ie run at least once...
		while( True ):
			## Make sure the data exists before running strip and repeating.., len( _haystack ) + 1
			_result = _regex.search( _haystack, _start );

			## It exists, lets do it...
			if ( _result != None ): #and _result.start( ) > 0 and _result.end( ) > 0 ):
				## Update start pos
				_start = _result.end( ) + 1;
				_data = str( _result.group( 1 ).strip( ) );

				##
				if ( _data.find( ':' ) > 0 ):
					_data = _data.split( ':' )[ 0 ];

				## (?!end|then|else|elif|if|local|return|break|true|false)
				## _data.find( ':' ) > 0 or
				## if not ( _data == 'end' or _data == 'then' or _data == 'else' or _data == 'elif' or _data == 'if' or _data == 'local' or _data == 'return' or _data == 'break' or _data == 'true' or _data == 'false' ):
				##
				if not ( _data.find( '..' ) > 0 or _data.endswith( '"' ) or _data.endswith( "'" ) or _data.startswith( '"' ) or _data.startswith( "'" ) or _data.endswith( "(" ) or String.HasQuotes( _data ) or Table.HasKeySet( _exclude_found, _data ) or Ttable.HasKeySet( _exclude, _data ) ):
					## Add the result...
					_found.append( _data );
					_exclude_found[ _data ] = True;

					## print( str( _data ) )
			else:
				## We're done... exit loop...
				break;

		if ( len( _found ) > 0 ):
			return _found;
		else:
			return None;


	##
	## Removes Quotes from a string...
	##
	def StripQuotes( _text ):
		if ( _text.startswith( "'" ) and _text.endswith( "'" ) or _text.startswith( '"' ) and _text.endswith( '"' ) ):
			_text = String.SubStr( _text, 1, len( _text ) - 1 );

		return _text;


	##
	## Returns the type of quotes used by a string..
	##
	def GetQuotesType( _text ):
		if ( not _text or _text == '' ):
			return '';

		if ( _text.startswith( "'" ) and _text.endswith( "'" ) ):
			return "'";

		if ( _text.startswith( '"' ) and _text.endswith( '"' ) ):
			return '"';

		return '';


	##
	## Returns whether or not a particular string has quotes within it.
	##
	def HasQuotes( _text ):
		_type = String.GetQuotesType( _text )

		return Logic.TernaryFunc( _type == '"' or _type == "'", True, False );


	##
	## Returns the n starting chars from the string
	##
	def StartChars( _text, _count ):
		return _text[ : _count ];


	##
	## Returns the n ending chars from the string
	##
	def EndChars( _text, _count ):
		## One way to do it
		## _len = len( _text );
		## return _text[ _len - _count : ];

		## And another way...
		return _text[ - _count : ];


	##
	## Returns everything except the last _count chars in the string...
	##
	def NotEndChars( _text, _count ):
		return _text[ : - _count ];


	##
	## BUILT-IN - Returns whether or not our string starts with certain text
	##
	def StartsWith( _needle, _haystack ):
		return _haystack.startswith( _needle );


	##
	## BUILT-IN - Returns whether or not our string ends with certain text
	##
	def EndsWith( _needle, _haystack ):
		return _haystack.endswith( _needle );


	##
	## Helper function which lets you define width for each text, and it repeats it for you so you can repeat text and all columns will be the same width
	##
	## Purpose:
	## 			The purpose of the Format Column Helpers is to improve data output, primarily for debugging so output is easier to follow..
	## Usage:
	##		String.FormatColumn( 25, 'Text / Key', 15, 'Some Value', 15, 'Another Key', 50, 'Another Value' )
	##		String.FormatColumn( 25, 'Text / Key', 15, 'Some Value', 15, 'Another Key', 50, 'Another Value' )
	##		String.FormatColumn( 25, 'Key', 15, 'Some', 15, 'Another', 50, 'Value' )
	##		String.FormatColumn( -5, 'Key', -10, 'Some', -15, 'Another', -20, 'Value' )
	##
	## Output:
	##		Text / Key               Some Value     Another Key    Another Value                                     <LINE END>
	##		Text / Key               Some Value     Another Key    Another Value                                     <LINE END>
	##		Key                      Some           Another        Value                                             <LINE END>
	##
	## Using Negatives
	##		Key     Some          Another               Value                    <LINE END>
	##
	def FormatColumn( _width = 25, _text = '', *_varargs ):
		return String.FormatColumnEx( ' ', _width, _text, *_varargs );


	##
	## Helper function which lets you define width for each text, and it repeats it for you so you can repeat text and all columns will be the same width
	##
	## Task: Possibly optimize by adding if _width < 1 then simply append option and add abs( _width ) * _empty to the end... this means a lot less logic to process..
	##
	## Purpose:
	## 			The purpose of the Format Column Helpers is to improve data output, primarily for debugging so output is easier to follow..
	## Usage:
	##		String.FormatColumnEx( '.', 25, 'Text / Key', 15, 'Some Value', 15, 'Another Key', 50, 'Another Value' )
	##		String.FormatColumnEx( ' ', 25, 'Text / Key', 15, 'Some Value', 15, 'Another Key', 50, 'Another Value' )
	##		String.FormatColumnEx( ' ', 25, 'Key', 15, 'Some', 15, 'Another', 50, 'Value' )
	##		String.FormatColumn( ' ', -5, 'Key', -10, 'Some', -15, 'Another', -20, 'Value' )
	##
	## Output:
	##		Text / Key...............Some Value.....Another Key....Another Value.....................................<LINE END>
	##		Text / Key               Some Value     Another Key    Another Value                                     <LINE END>
	##		Key                      Some           Another        Value                                             <LINE END>
	##
	## Using Negatives
	##		Key     Some          Another               Value                    <LINE END>
	##
	def FormatColumnEx( _empty_char = ' ', _width = 25, _text = '', *_varargs ):
		## Make sure our text is a string .. and for each tab used, calculate how many spaces should be used with minimum of 1 and maximum of 4 being the range depending which snap-point is used.. Then strip that tab and add spaces in its place..
		_text	= str( _text ).expandtabs( CONST_TAB_WIDTH );

		## Since our Ex function, this, must use a paired-system, we make sure the rounded division is > 0
		_more	= ( round( len( _varargs ) / 2, 0 ) > 0 );

		## If _width is 0, we append the string without doing anything ( but we still process logic - I may make this shorter later by doing that though )... if _width is negative, we fit to the length of text + abs( _width ) spaces added to the end...
		_width = ( len( _text ) + abs( _width ), _width )[ _width >= 0 ];

		## How many repeating chars do we need to create?
		_reps	= ( _width - len( _text ) );

		## Build a string to fill the empty column space with spaces so everything lines up - as long as the right font is used ( where all chars are the same size )
		_empty	= _empty_char * _reps;

		## Now we ensure our text is limited to the _width size - data going over is truncated...TernaryFunc( _reps > 0, _empty, _empty )
		## _data = String.SubStr( _text + ( _empty ), 0, _width );
		## _data = ( _text + ( _empty ) )[  : _width ];
		_data	= String.Truncate( _text + ( _empty ), _width );

		## If we have more cars
		if ( _more ):
			## Recursive call by shifting our VarArgs left so they populate _width and _text - then add the result to the data var... This only stops when no more paired options are left...
			_data = _data + String.FormatColumnEx( _empty_char, *_varargs );

		## Return the data..
		return _data;


	##
	## Helper function which lets you define width once, and it repeats it for you so you can repeat text and all columns will be the same width
	##
	## Purpose:
	## 			The purpose of the Format Column Helpers is to improve data output, primarily for debugging so output is easier to follow..
	##
	## Usage:
	##		String.FormatSimpleColumn( 15, 'Text / Key', 'Some Value', 'Another Key', 'Another Value' )
	##		String.FormatSimpleColumn( 15, 'Key', 'Some', 'Another', 'Value' )
	##		String.FormatSimpleColumn( -5, 'Key', 'Some', 'Another', 'Value' )
	##
	## Output:
	##		Text / Key     Some Value     Another Key    Another Value  <LINE END>
	##		Key            Some           Another        Value          <LINE END>
	##
	## Using Negatives
	##		Key     Some     Another     Value     <LINE END>
	##
	def FormatSimpleColumn( _width = 25, *_varargs ):
		## Count how many text elements we have...
		_count = len( _varargs );

		## Set up our return var
		_data = '';

		## If we have at least 1 text element to set-up into a column
		if ( _count > 0 ):
			## Then we loop through each vararg
			for _text in _varargs:
				## Accumulate data by appending each additional _text argument using a call to FormatColumn
				_data = _data + String.FormatColumn( _width, str( _text ) );

		## Return the data..
		return _data;


	##
	##
	##
	def FormatColumnStripR( _width = 25, _text ='', *_varargs ):
		return String.FormatColumn( _width, _text, *_varargs ).rstrip( );


	##
	##
	##
	def FormatSimpleColumnStripR( _width = 25, *_varargs ):
		return String.FormatSimpleColumn( _width = 25, *_varargs ).rstrip( );


	##
	## Returns the filename with file extension from path/to/file.ext
	## Task: Deprecated - use Acecool.File.* or Folder.*
	##
	def GetFileName( _file = '' ):
		return File.GetName( _file );


	##
	## Returns the file extension from path/to/file.ext
	## Task: Deprecated - use Acecool.File.* or Folder.*
	##
	def GetFileExt( _file = '' ):
		return File.GetExt( _file );


##
## Text Formatting Class - All formatting functions will be moved here - all string operations will be kept in String...
##
class Text( ):
	##																◙						◙
	## Create a 3 line piped text which looks something like this:	◙	Text Appears Here	◙
	##																◙						◙
	def FormatPipedHeader( _text, _pipes = '◙', _buffer = 4, _buffer_chars = ' ' ):
		_text = _pipes + ( _buffer_chars * _buffer ) + _text + ( _buffer_chars * _buffer ) + _pipes;
		_text = String.FormatColumn( len( _text ) - len( _pipes	), _pipes ) + _pipes +'\n' + _text + '\n' + String.FormatColumn( len( _text ) - len( _pipes	), _pipes ) + _pipes + '\n';

		## This creates a sstaggered - because _text already has the spaces, by adding them again, you end up with normal top and bottom, but the center is offset to the right by x chars... so it looks like an arrow > _text > - could be useful for something...
		## _text = String.FormatColumn( len( _text ) - len( _pipes	), _pipes ) + _pipes +'\n' + ( _buffer_chars * _buffer ) + _text + ( _buffer_chars * _buffer ) + '\n' + String.FormatColumn( len( _text ) - len( _pipes	), _pipes ) + _pipes + '\n';

		return _text


	##
	##
	##
	def FormatColumn( _width = 25, _text = '', *_varargs ):
		return String.FormatColumnEx( ' ', _width, _text, *_varargs );


	##
	##
	##
	def FormatColumnEx( _empty_char = ' ', _width = 25, _text = '', *_varargs ):
		return String.FormatColumnEx( _empty_char, _width, _text, *_varargs );

	##
	##
	##
	def FormatSimpleColumn( _width = 25, *_varargs ):
		return String.FormatSimpleColumn( _width, *_varargs );


	##
	##
	##
	def FormatColumnStripR( _width = 25, _text ='', *_varargs ):
		return String.FormatColumnStripR( _width, _text, *_varargs );


	##
	##
	##
	def FormatSimpleColumnStripR( _width = 25, *_varargs ):
		return String.FormatSimpleColumnStripR( _width = 25, *_varargs );




##
## Generic Table Helpers
##
## class TableBase( ):
## 	pass;
class Table:
	##
	##
	##
	__name__ = 'Table';


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		_text					= '';

		##


		## _text += Acecool.Header( 'Table.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += Acecool.Footer( ) + '\n';


		_text += Acecool.Header( 'Table.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += Acecool.Footer( ) + '\n';


		## _text += Acecool.Header( 'Table.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += Acecool.Footer( ) + '\n';

		return _text;


	##
	## Takes a table of keys and a table of values and turns them into a single table of keys = values...
	##
	def MapKeysToValues( _keys, _values ):
		if ( not ( IsTable( _keys ) and IsTable( _values ) and len( _keys ) == len( _values ) ) ):
			return False;

		##
		_map = { };

		##
		for _key, _value in enumerate( _keys ):
			_map[ _value ] = _values[ _key ];


		return _map;


	##
	## Takes a mapped table of keys to values and adds additional elements to it - uses a copy of it to prevent changing the original table...
	##
	def MapKeysValuesToExtra( _map, _extras = None ):
		##
		_map = _map.copy( );

		##
		if ( _extras != None ):
			_map.update( _extras );

		##
		return _map;


	##
	##
	##
	def Merge( _a, _b ):
		##
		if ( type( _a ) != type( b ) ):
			raise Exception( 'MergeTypeException: Both Tables need to be the same type: Tuple, List, or Dict - They are: ' + str( type( _a ) ) + ' and ' + str( type( _b ) ) );

		##
		if ( isinstance( _a, { } ) ):
			return Dict.Merge( _a, _b );

		##
		if ( isinstance( _a, [ ] ) ):
			return List.Merge( _a, _b );

		##
		if ( isinstance( _a, ( ) ) ):
			return Tuple.Merge( _a, _b );

		##
		raise Exception( 'MergeException: The data is not a table type Tuple, List or Dict... Can not merge!' );


	##
	##
	##
	def GetValue( _args, _args_count, _index, _default ):
		_arg = ''; ## _index += 1;

		if ( _default != '' or str( _default ) != 'nil' ) and _arg == '':
			_arg = _default;
		if ( _args_count > _index and str( _args[ _index ] ) != 'nil' ):
			_arg = str( _args[ _index ] );

		return _arg;


	##
	## Takes a Dictionary ( uses { } ) and returns a tuple ( uses [ ] ) map which is sorted - basic index values convert to string key so
	##
	## TODO: Create this...
	##
	## ## Usage:
	##
	## _dict = { "My":{ "z":True, "a":False }, "ABC":{ "z":False, "a":True } }
	##
	## ## This becomes [ 1: "ABC", 2: "My" ]
	## _tuple_map = Table.DictToTupleMap( _dict, SORT_METHOD_A_Z )
	##
	## ## So, in our loop we can use range which does 1, then 2 - it means we need a lengthier call.. Or we can use a for-each method...
	## for _i in range( len( _tuple_map ) ):
	##	## _tuple_map for 1 is ABC so we convert it... - Returns ABC as { "z":False, "a":True } then My as { "z":True, "a":False }
	##	_dict_data = _dict[ _tuple_map[ _i ] ]
	##
	##
	def DictToTupleMap( _dict ):
		## Create our Tuple
		_tuple = [ ];

		## Go through the dictionary one at a time...
		for _key, _value in enumerate( _dict ):
			print( 'Tuple > Dict[ ' + str( _key ) + ' ] = ' + str( _value ) );



	##
	## Redesign of the code below...
	##
	def FromCodeDefinitionToComponents( _code, _args_delimeter = ',', _start_paren_char = '(', _finish_paren_char = ')' ):
		## Determine how many parens are there...
		_start_char_count = _code.count( _start_paren_char );
		_end_char_count = _code.count( _finish_paren_char );

		##
		## Notes: If function CALL then preserve args ( quotes, etc.. except white-space between args ) if definition then allow skip chars...
		##

		## If we have the same number of each...
		if ( _start_char_count == _end_char_count ):
			## If we have 1 of each then
			if ( _start_char_count == 1 ):
				## Because there is 1 of each we can break it down into component form by means of <FUNC/CLASS-COMBO> <NAME><START><ARGS><END> or <NAME> = <FUNC/CLASS-COMBO><START><ARGS><END> by simply splitting the string up...

				## Just in case we're not starting from 0...
				_begin = String.indexOf( _start_paren_char, _text );
				_finish = String.indexOf( _finish_paren_char, _text );


			## More than one of each with the same count - we need to determine scope...
			else:
				## We have to process each character... Depth 1 means function.. Depth > 1 means part of an arg so preserve everything... It can be split using another call... Everything outside of <START> and <END> is left to be the name / id... we only break it into component form, nothing else..
				print( );
		else:
			## We have an odd number of parens - ie
			##
			if ( _start_char_count > 1 or _end_char_count > 1 ):
				print( );


	##
	## Table.FromFunctionArgsString - Function to take a string of function arguments and turn it into a table
	##
	## TODO: Change so depth 0 chars aren't used, then primary_depth 1 args are taken, depth 2 is returned in a different table, 3 and so on... but 0 is ignored... { }s and [ ]s count as a different type of depth ( table, or so on )..
	## Note: Spaces should be allowed in after the first char and before the last to allow for x = y default arg value systems...
	##
	def FromFunctionArgsString( _text ):
		if not Util.isset( _text ):
			return '', 0;

		##
		## Config
		##

		## Chars and Cap ( Starting and Ending ) Chars we want to skip...
		_cfg_skip_chars = { };
		_cfg_skip_cap_chars = { };

		## Chars to skip at ALL times regardless of location within the string...
		## _cfg_skip_chars[ "{" ] = True;
		## _cfg_skip_chars[ "}" ] = True;

		## Chars we want to skip regardless of where they are in the string...
		_cfg_skip_cap_chars[ ' ' ] = True;
		_cfg_skip_cap_chars[ "'" ] = True;
		_cfg_skip_cap_chars[ '"' ] = True;

		## Tracks the current "Depth" based on brackets / braces
		_depth			= 0;

		## Tracks the number of words we've accumulated in our table
		_args_count		= 0;

		## Tracks the number of chars in the current word
		_char_count		= 0;

		## Holds the current word until it is ready to be imploded as a string and inserted into our words table..
		_arg			= [ ];
		_arg_raw		= [ ];

		## Holds all of the arguments we've accumulated
		_args			= [ ];
		_args_raw		= [ ];

		## Current Arg Flags
		_arg_isstring	= False;

		## Just in case we're not starting from 0...
		_begin = String.IndexOf( '(', _text );
		_finish = String.IndexOf( ')', _text );

		## Function declarations can be an argument - make sure these aren't included...
		_begin_func = String.IndexOf( 'function(', _text );
		## _finish = Logic.TernaryFunc( _finish <= len( _text ), _finish, len( _text ) );

		## print( "Debugging: _text = '" + _text + "' / _begin = " + str( _begin ) + " / _finish = " + str( _finish ) );

		## Clean
		## if ( _begin > 0 ):
			## if ( _begin_func > 0 ):
			##	_text = String.SubStrEx( _text, _begin, _begin_func );
			##	_text = _text + 'FUNC_REF';
			## else:
		_text = String.SubStrEx( _text, _begin, _finish );

		## if ( ):
		##	_text = String.EndChars( _text, _finish );

		## trick to remove special-case of trailing chars
		for _key, _char in enumerate( _text + "," ):
			## Increment the key..
			## _key += 1;

			## Manage Brace Depth
			if _char == "{" or _char == "(" or _char == "[":
				_depth += 1;
			elif _char == "}" or _char == ")" or _char == "]":
				_depth -= 1;

			## If we've encountered a char we should skip, then skip it..
			if _cfg_skip_chars.get( _char, False ) == True:
				## _arg_raw.append( _char );
				_arg_raw.append( '*' );
				continue;

			## If we've reached a comma.. we turn out current work collected chars into an actual word for our table and reset the collector for the next word...
			if _char == "," and _depth < 1:
				## If We're not in a bracket
				if _depth == 0:
					## Assemble the word and strip left / right spaces...
					_arg = "" . join( _arg ).strip( );
					_arg_raw = "" . join( _arg_raw ).strip( );

					## Add the current word to our table of words
					_args.append( _arg );
					_args_raw.append( _arg_raw );

					## Increment our counter
					_args_count += 1;

					## Reset the word so we can begin assembling a new word
					_arg = [ ];
					_arg_raw = [ ];

					## Reset Arg Flags
					_char_count = 0;
					_arg_isstring = False;

			## else - ie If the char isn't a comma or the depth is >= 1 - meaning commas will only be added if depth is 1 or more and other data will be added all the time
			else:
				## Helpers... _head_char == first char of arg, _tail_char == last char of arg, _nil_depth == No Depth, _skip_char == Bad Character ( Determine whether or not to skip )
				_head_char = _char_count == 0;
				_tail_char = _text[_key:_key + 1] == ',';
				_nil_depth = _depth == 0;
				_skip_char = _cfg_skip_cap_chars.get( _char, False ) == False;

				## Helpers so we don't need to repeat _arg append... In short, if we're on a cap-character which is bad skip it...
				## If head_char or nil depth or tail char encountered... or not
				## if ( ( _head_char or _nil_depth and _tail_char ) or not ( _head_char or _nil_depth and _tail_char ) ) and _skip_char or not _nil_depth:
				if _skip_char or not _nil_depth:
					## Add the current character to our word string
					_arg.append( _char );
					_char_count += 1;

				## if _char == '"' or _char == "'" or _skip_char or not _nil_depth:
				## if not _nil_depth:
				_arg_raw.append( _char );

		return _args, _args_count, _args_raw;


	##
	##
	##
	def KeySet( *_varargs ):
		##
		_tab = { };

		##
		for _value in _varargs:
			## print( ' >>> ' + str( _value ) );
			_tab[ str( _value ) ] = True;

		##
		return _tab;


	##
	##
	##
	def HasKeySet( _tab, _key ):
		return _tab.get( str( _key ), False );

		##
		## if ( _key != None and _tab[ str( _key ) ] == True ):
			## return True;

		## return False;


	##
	## Helper: Converts 1 object, or a table of objects to a Dict O( 1 ) getter style table...
	##
	def SetupQuickLookupTable( _data = None ):
		if ( IsDict( _data ) ):
			return Dict.SetupQuickLookupTable( _data );
		if ( IsList( _data ) or IsTuple( _data ) or IsTableTypeSet( _data ) ):
			return List.SetupQuickLookupTable( _data );


##
## Dictionary Specific Data-Type Helpers
##
## class DictBase( ):
## 	pass;
class Dict: #( DictBase ):
	##
	##
	##
	__name__ = 'Dict';


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		_text					= '';

		##


		## _text += Acecool.Header( 'Dict.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += Acecool.Footer( ) + '\n';


		_text += Acecool.Header( 'Dict.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += Acecool.Footer( ) + '\n';


		## _text += Acecool.Header( 'Dict.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += Acecool.Footer( ) + '\n';

		return _text;


	##
	## Merges 2 Dicts into 1
	##
	def Merge( _a, _b ):
		_dict = _a.copy( );
		_dict.update( _b );
		return _dict;


	##
	## Adds an entry to a Dict
	##
	def Add( _dict = { }, _key = 'Default', _entry = None ):
		##
		setattr( _dict, _key, _entry );

		##
		return _dict;


	##
	## Returns a keyed entry / value from a Dict
	##
	def Get( _dict = { }, _key = 'Default', _default = None ):
		##
		return getattr( _dict, _key, _default );


	##
	## Returns whether or not a key exists with a valid value from a Dict exists
	##
	def HasKey( _dict = { }, _key = 'Default' ):
		_has = True if Dict.Get( _dict, _key, None ) != None else False;
		return _has;


	##
	## Returns whether or not an value from a Dict exists
	##
	def Len( _dict = { } ):
		return len( _dict );


	##
	## Returns whether or not an value from a Dict exists
	##
	def Reset( _dict = { } ):
		return Dict.Clear( _dict );


	##
	## Returns whether or not an value from a Dict exists
	##
	def Clear( _dict = { } ):
		return _dict.clear( );


	##
	## Removes, and Returns, the last entry added to the dict!
	##
	def Pop( _dict ):
		return _dict.popitem( );


	##
	## Removes, and Returns, the entry corresponding to the key within the dict, if not found, the _default value is returned!
	##
	def GetPop( _dict, _key, _default = None ):
		return _dict.pop( _key, _default );


	##
	## Inserts an entry to a Dict without a key, uses numerical keys...
	##
	def Insert( _dict = { }, _entry = None ):
		##
		setattr( _dict, len( _dict ), _entry );

		##
		return _dict;


	##
	## This simply creates a new Dict from a list of keys, setting the value to what we specify for ALL of them...
	##
	def FromKeys( _dict = { }, _value = True ):
		return _dict.fromkeys( _dict, _value );


	##
	## Same as SetupQuickLookupTable, except instead of setting the value to the data-type, it sets it to True...
	## Note: This uses the key = True instead of value = True... In a lot of cases, with other lists to convert, we need the value and not the key...
	##
	def FromValues( _dict = { }, _default_value = None, _use_keys = True ):
		##
		_dict_new = { };

		##
		for _key, _value in _dict.items( ):
			##
			if ( _use_keys ):
				_type_key = str( type( _key ) ) if _default_value == None else _default_value;
				if ( callable( _type_key ) ):
					_type_key = _type_key( _dict, _key, _value );

				_dict_new[ str( _key ) ] = _type_key;
			else:
				_type_val = str( type( _value ) ) if _default_value == None else _default_value;
				if ( callable( _type_val ) ):
					_type_val = _type_val( _dict, _key, _value );

				_dict_new[ str( _value ) ] = _type_val;

		##
		return _dict_new;


	##
	## Helper: Converts 1 object, or a table of objects to a Dict O( 1 ) getter style table...
	##
	def SetupQuickLookupTable( _data = None ):
		return Dict.SetupQuickLookupTableEx( _data, ( lambda _dict, _key, _value: str( type( _key ) ) ) )


	##
	## Helper: Converts 1 object, or a table of objects to a Dict O( 1 ) getter style table...
	##
	def SetupQuickLookupTableEx( _data = None, _default_value = True, _use_keys = None ):
		## If we have no data, return None...
		if ( _data == None ):
			return None;

		## Helper
		_type = type( _data );

		## If the data isn't None, we need to determine what it is - single element or table... So if not Dict, Tuple, List then we only add 1 object to Dict and return - else we go through each element..
		if ( _type != type( { } ) and _type != type( ( ) ) and _type != type( [ ] ) ):
			return { str( _data ): str( _type ) };

		## Now, we have established it is a Dict, Tuple or List, process all of the values into keys = True...
		_dict = { };
		_iter = None;

		##;
		_is_dict = ( _type == type( { } ) )
		_is_list = ( _type == type( [ ] ) or _type == type( ( ) ) );

		## Data-Table Iteration Method..
		if ( _is_dict ):
			_iter = _data.items( );

		##
		if ( _is_list ):
			_iter = enumerate( _data );

		## For each element, process it...
		for _key, _value in _iter:
			## If List / Tuple, we focus on the value, not the numerical key, as the identifer - ie ( 'x', 'y', 'z' ) would be more useful as x y z instead of 0, 1, 2...
			_value_result = _value;

			## If Dict, the default quick-search method we want won't be the value, it'll be the key...
			if ( _use_keys == True or ( _is_dict and _use_keys == None ) ):
				_value_result = _key;

			_default_value_result = _default_value;
			if ( callable( _default_value ) ):
				_default_value_result = _default_value( _data, _key, _value );

			_dict[ str( _value_result ) ] = _default_value_result;

		## return the Dict..
		return _dict;




##
## List Specific Data-Type Helpers
##
## class ListBase( ):
## 	pass;
class List:
	##
	##
	##
	__name__ = 'List';


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		_text					= '';

		##


		## _text += Acecool.Header( 'List.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += Acecool.Footer( ) + '\n';


		_text += Acecool.Header( 'List.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += Acecool.Footer( ) + '\n';


		## _text += Acecool.Header( 'List.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += Acecool.Footer( ) + '\n';

		return _text;


	##
	##
	##
	def Merge( _a, _b ):
		## _dict = _a.copy( );
		## _dict.update( _b );
		## return _dict;
		return _a + _b;


	##
	## Helper: Converts 1 object, or a table of objects to a Dict O( 1 ) getter style table...
	##
	def SetupQuickLookupTable( _data = None ):
		return Dict.SetupQuickLookupTableEx( _data, ( lambda _dict, _key, _value: str( type( _value ) ) ) );


##
## Tuple Specific Data-Type Helpers
##
## class TupleBase( ):
## 	pass;
class Tuple:
	##
	##
	##
	__name__ = 'Tuple';


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		_text					= '';

		##


		## _text += Acecool.Header( 'Tuple.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += Acecool.Footer( ) + '\n';


		_text += Acecool.Header( 'Tuple.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += Acecool.Footer( ) + '\n';


		## _text += Acecool.Header( 'Tuple.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += Acecool.Footer( ) + '\n';

		return _text;


	##
	##
	##
	def Merge( _a, _b ):
		## _dict = _a.copy( );
		## _dict.update( _b );
		## return _dict;
		return _a + _b;


	##
	## Helper: Converts 1 object, or a table of objects to a Dict O( 1 ) getter style table...
	##
	def SetupQuickLookupTable( _data = None ):
		return List.SetupQuickLookupTable( _data );




##
## File / Folder Based Helpers
##




##
## Folder Functions
##
## class FolderBase( ):
## 	pass;
class Folder:
	##
	##
	##
	__name__ = 'Folder';


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		_text					= '';

		##


		## _text += Acecool.Header( 'Folder.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += Acecool.Footer( ) + '\n';


		_text += Acecool.Header( 'Folder.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += Acecool.Footer( ) + '\n';


		## _text += Acecool.Header( 'Folder.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += Acecool.Footer( ) + '\n';


		return _text;


	##
	## Returns the proper path delimiter for the operating system...
	##
	def GetPathDelimiter( ):
		return System.GetPathDelimiter( );


	##
	## Returns the parent path - Note: If there is a filename attached, then that will be removed..
	##
	def GetParent( _path ):
		return os.path.basename( _path );


	##
	## Get Path Components - Returns full-path ( which is what is supplied ), the path without the file, the filename, the base filename, the file extension.
	##
	def GetComponents( _path, _snip_packages = False ):
		## The full path ( 'C:/Program Files/Sublime Text 3/sublime_text.exe' )
		## _path

		## Returns ( 'C:/Program Files/Sublime Text 3' )
		_path_base, _filename				= os.path.split( _path );

		## Returns ( 'sublime_text', '.exe' )
		_file_base, _file_ext				= os.path.splitext( _filename );

		## Returns ( 'exe' )
		_file_ext							= _file_ext[ 1 :  ] if ( _file_ext[ 0 : 1 ] == '.' ) else _file_ext;

		## If path starts with Packages/ then remove it. Otherwise return the original path.
		if ( _snip_packages ):
			_path[ len( 'Packages' + System.GetPathDelimiter( ) ) : ] if ( _path.startswith( 'Packages' + System.GetPathDelimiter( ) ) ) else _path

		##
		return _path, _path_base, _filename, _file_base, _file_ext;


	##
	## Helper - Returns whether or not the file / folder exists by checking the timestamp...
	##
	def Exists( _path ):
		return File.GetTimeStamp( _path ) > -1;


	##
	## Creates all of the important folders in the User folder and either generates files, copies them, etc... Then the plugin can be 1 folder and install outwards to what is needed.. including replacing CodeMap files...
	##
	def Create( *_paths ):
		##
		if ( _paths == None or not len( _paths ) > 0 ):
			print( 'AcecoolLib -> Folder -> Create -> Can not create folder - _paths is None or has no entries.' );
			return False;

		##
		_errors = '';

		##
		for _path in _paths:
			## print( 'AcecoolLib -> Folder -> Create -> Trying to create file: ' + _path );
			if not len( _path ) > 0:
				continue;

			## Make sure the folder structure exists..
			try:
				## print( 'AcecoolLib -> Folder -> Create -> Trying to create file...' );
				## Attempt to create the folder
				os.makedirs( _path );

				## print( 'AcecoolLib -> Folder -> Create -> File created successfully!!!' );
			except OSError as _error:
				_errors += str( _error ) + '\n';
			except:
				_errors += 'Other Exception!\n';
				## if e.errno != errno.EEXIST:
			##	raise;

		if ( _errors == '' ):
			return True;
		else:
			print( 'AcecoolLib -> Folder -> Create -> Errors: ' + _errors );
			return False;


	##
	##
	##
	def ListFiles( _path, _limit_endswith = None ):
		##
		_list = os.listdir( _path );

		## If we want to limit the list then we need to
		if ( _limit_endswith != None ):
			## Create a new list
			_new = [ ];

			## Process every file in the original list
			for _file in _list:
				## And if the file ( case insensitive ) ends with our limiting string then
				if ( _file.lower( ).endswith( _limit_endswith.lower( ) ) ):
					## We add it to the new list
					_new.append( _file );

			## and then we return the new list...
			return _new;

		## Return the list of all files
		return _list;



	##
	##
	##
	def Rename( _path, _new ):
		pass;


	##
	##
	##
	def Remove( _path ):
		return File.Remove( _path );


	##
	##
	##
	def SafeName( _name = '' ):
		return File.GetSafeName( _name );


	##
	##
	##
	def GetSafeName( _name = '' ):
		return File.GetSafeName( _name );


	##
	##
	##
	def GetExt( _name = '' ):
		return File.GetExt( _name );


##
## File Functions
##
## class FileBase( ):
## 	pass;
class File:
	##
	##
	##
	__name__ = 'File';


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		_text					= '';

		##


		## _text += Acecool.Header( 'File.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += Acecool.Footer( ) + '\n';


		_text += Acecool.Header( 'File.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += Acecool.Footer( ) + '\n';


		## _text += Acecool.Header( 'File.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += Acecool.Footer( ) + '\n';

		return _text;


	##
	## Returns the proper path delimiter for the operating system...
	##
	def GetPathDelimiter( ):
		return System.GetPathDelimiter( );


	##
	##
	##
	def GetName( _path = '', _extended = False ):
		_file_base, _ext = os.path.splitext( os.path.basename( _path ) );

		return _file_base + _ext;

		## if ( not _extended ):
		## 	return _path.split( '\\' )[ - 1 ];
		## else:
		## 	return _path;


	##
	##
	##
	def SafeName( _name = '' ):
		return File.GetSafeName( _name );


	##
	## Helper - Converts the full-path to and including the file-name to a single-filename which can be used as a filename saved...
	##
	def GetSafeName( _name, _replace_dir = '_-_', _replace_blank = '_', _replace_banned = '' ):
		## Convert the filename to a safe-filename - will be using RegEx later..
		## _file_safe = String.Safe( _file ) + '.cache';

		## Same as above except not using RegEx...

		## Replace Directory Specific chars
		_name = _name.replace( '\\',	_replace_dir );
		_name = _name.replace( '/',		_replace_dir );

		## Replace bad chars - anythin
		_name = _name.replace( ' ',		_replace_blank );
		_name = _name.replace( '__',	_replace_blank );

		## Replace Banned ( Any char that can not be used in the name )..
		_name = _name.replace( ':',		_replace_banned );
		_name = _name.replace( '*',		_replace_banned );
		_name = _name.replace( '?',		_replace_banned );
		_name = _name.replace( '|',		_replace_banned );
		_name = _name.replace( '<',		_replace_banned );
		_name = _name.replace( '>',		_replace_banned );

		## Replace Excess
		_name = _name.replace( _replace_dir + _replace_dir,		_replace_dir );
		_name = _name.replace( _replace_blank + _replace_blank,	_replace_blank );

		## Return the finally "safe for saving" filename..
		return _name;


	##
	##
	##
	def GetExt( _file = '' ):
		## _data = _file.split( '.' );
		## return _data[ len( _data ) - 1 ] or '';
		_raw_ext, _ext = os.path.splitext( _file );
		if ( _ext[ 0 : 1 ] == '.' ):
			_ext = _ext[ 1 :  ];

		## ## _data = _file.split( '.' );
		## ## return _data[ len( _data ) - 1 ] or '';
		## _raw_ext, _ext = os.path.splitext( _file );
		## if ( _ext[ 0 : 1 ] == '.' ):
		## 	_ext = _ext[ 1 :  ];

		return _ext;
		## ## return String.GetFileExt( _name )


	##
	## Helper - Returns whether or not the file / folder exists by checking the timestamp...
	##
	def Exists( _path ):
		return File.GetTimeStamp( _path ) > -1;


	##
	##
	##
	def Create( _path, _data = '', _flags = 'w+' ):
		if File.Exists( _path ):
			return False;

		File.Write( _path, _data, _flags );


	## ##
	## ##
	## ##
	## def Read( _path, _flags = 'r' ):
	## 	## Try to read the file data... This function should only be called if the file exists...
	## 	try:
	## 		## Open the file with readonly-permissions and proper encoding
	## 		with io.open( _path, _flags, encoding = 'utf8' ) as _file:
	## 			## Read the file and output it..
	## 			return _file.read( );
	## 	except:
	## 		## Output the error to the output window...
	## 		return 'ExceptionThrown: ' + str( sys.exc_info( )[ 0 ] ), -1;


	##
	##
	##
	def Read( _file, _char_new_line = CONST_NEW_LINE_UNIX ):
		_lines = None;

		try:
			with codecs.open( _file, "r", encoding = 'utf8' ) as _f:
				_lines = _f.read( ).split( _char_new_line );
		except Exception as _error:
			print( '[ Acecool > file > Read ] Error:', _error );
			_lines = ''.split( _char_new_line );

		return _lines, len( _lines );


	##
	##
	##
	def Write( _path, _data = '', _flags = 'w+' ):
		## Try, try, try again...
		try:
			## Open the file with write-permissions and proper encoding
			with io.open( _path, _flags, encoding = 'utf8' ) as _file:
				## Write the data...
				_file.write( _data );

			## Once we're done, return None to ensure no error output is output to the output panel..
			return None
		except NameError as _error:
			## Naming error? Too long maybe? Invalid chars? -- actually because global unicode didn't exist..
			## return 'NameError( {0} ): {1} ' . format( _error.errno, _error.strerror );
			return 'Acecool - Code Mapping System > SaveFileCache > NameError: ' + str( sys.exc_info( )[ 0 ] ) + ' / ' + str( _error );
		except UnicodeError as _error:
			## Data Formatting Issue
			return 'Acecool - Code Mapping System > SaveFileCache > UnicodeError( {0} ): {1} ' . format( str( _error.errno ), str( _error.strerror ) );
		except IOError as _error:
			## General Input / Output Error
			return 'Acecool - Code Mapping System > SaveFileCache > I / O Error( {0} ): {1} ' . format( str( _error.errno ), str( _error.strerror ) );
		except:
			# Handle all other errors
			return 'Acecool - Code Mapping System > SaveFileCache > Unexpected error: ' + str( sys.exc_info( )[ 0 ] );


	##
	## Adds content to the beginning of a file
	##
	def Prepend( _path, _data = '', _flags = 'w+' ):
		return File.Write( _path, _data +  File.Read( _path ) );


	##
	## Adds content to the end of a file
	##
	def Append( _path, _data = '', _flags = 'a+' ):
		return File.Write( _path,  File.Read( _path ) + _data );


	##
	## Aliases
	##
	def Prefix( _path, _data = '', _flags = 'w+' ): return self.Prepend( _path, _data, _flags );
	def Suffix( _path, _data = '', _flags = 'a+' ): return self.Append( _path, _data, _flags );


	##
	##
	##
	def Copy( _file, _path, _new_path ):
		##
		_file_col = 135;
		_file_old = os.path.join( _path, _file );
		_file_new = os.path.join( _new_path, _file );

		##
		if ( File.Exists( _file_old ) ):
			try:
				shutil.copyfile( _file_old, _file_new );
			except Exception as _error:
				print( 'AcecoolLib.File.Copy -> File: ' + Text.FormatColumn( 25, _file ) + ' -> From: ' + Text.FormatColumn( _file_col - 25, _path ) + ' -> To: ' + Text.FormatColumn( _file_col, _file_new ) + ' -> Error: ' + _error );
		else:
			print( 'AcecoolLib.File.Copy -> File Not Found: ' + Text.FormatColumn( _file_col, _file_old ) + ' -> Trying to Move To: ' + Text.FormatColumn( _file_col - 25, _new_path ) + ' -> As New File: ' + Text.FormatColumn( _file_col, _file_new ) );


	##
	##
	##
	def Rename( _path, _file, _new ):
		pass;


	##
	##
	##
	def Remove( _path ):
		try:
			os.remove( _path );
		except Exception as _error:
			print( 'AcecoolLib.File.Remove -> Exception on Path: ' + _path + ' -> ' + _error );



	##
	## Returns the timestamp from the actual file to help determine the last time it was modified...
	##
	def GetTimeStamp( _file ):
		## Default timestamp set at 0
		_timestamp = 0;

		## Attempt to read the file timestamp...
		try:
			## We try because numerous issues can arise from file I / O
			_timestamp = os.path.getmtime( _file );
		except ( OSError ):
			## File not found, inaccessible or other error - Set the timestamp to -1 so we know an error was thrown..
			_timestamp = -1;

		## Output the data..
		## print( '>> Timestamp for file: ' + _file + ' is ' + str( _timestamp ) );

		## Grab the real-time File TimeStamp if the file exists, otherwise -1 or 0...
		return round( _timestamp, 2 );


	##
	## Alias
	##
	def TimeStamp( _file ):
		return File.GetTimeStamp( _file );


	##
	##
	##
	def Size( _file ):
		_info = os.stat( _file );
		return _info.st_size;

	##
	##
	##
	def CRC( _file ):
		return str( File.GetTimeStamp( _file ) );


##
## Function Utlities
##
## class FunctionBase( ):
## 	pass;
class Function:
	##
	##
	##
	__name__ = 'Function';


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		_text					= '';

		##


		## _text += Acecool.Header( 'Function.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += Acecool.Footer( ) + '\n';


		_text += Acecool.Header( 'Function.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += Acecool.Footer( ) + '\n';


		## _text += Acecool.Header( 'Function.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += Acecool.Footer( ) + '\n';

		return _text;


	##
	## Helper Function used to process VarArgs...
	##
	def ProcessVarArgs( _args, _callback, _print_category, _print_text, *_text_args ):
		## Set up out output / return value to a blank-string...
		_output = '';

		## Make sure we have something to process
		if ( _args != None and len( _args ) > 0 ):
			## Process the data...
			for _i, _arg in enumerate( _args ):
				_output = _output + _callback( _i, _arg );

		## Convert the _text_args to print-data... _output is always {0}
		_print = [ ];
		_print.append( str( _output ) );
		if ( len( _text_args ) > 0 ):
			for _, _value in enumerate( _text_args ):
				_print.append( str( _value ) );

		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( _print_category, ( _print_text.format( *_print ) ) );

		## Always return something
		return _output;


	##
	## Similar to ProcessVarArgs without text... Simple processor of keyed varargs...
	##
	def ProcessSimpleVarArgs( _callback, *_args ):
		## Define the amount of iterations required...
		_varargs = len( _args );

		## If callback is provided and arguments are provided and we have a valid number of arguments...
		if ( isfunction( _callback ) and _args != None and _varargs > 0 ):

			## Loop through the varargs...
			for _index in range( _varargs ):
				## Grab the key / value for our index..
				_key = _args[ _index ];

				## Execute the callback with the key / value...
				_callback( _key );
		else:
			## Return false if we can't do anything..
			return False;

		## Return true when done...
		return True;


	##
	## Similar to ProcessVarArgs without text... Simple processor of keyed varargs...
	##
	def ProcessKeyVarArgs( _callback, *_args ):
		## Define the amount of iterations required...
		_vararg_pairs = math.floor( len( _args ) / 2 );

		## If callback is provided and arguments are provided and we have a valid number of arguments...
		if ( isfunction( _callback ) and _args != None and _vararg_pairs > 0 ):

			## Loop through the varargs...
			for _i in range( _vararg_pairs ):
				## Grab the appropriate index for the first element, ie the key...
				_index = ( _i * 2 );

				## Grab the key / value for our index..
				_key = _args[ _index ];
				_value = _args[ _index + 1 ];

				## Execute the callback with the key / value...
				_callback( _key, _value );
		else:
			## Return false if we can't do anything..
			return False;

		## Return true when done...
		return True;



##
##
##
class Python( ):
	##
	## Helper - Returns a list of the magic function names ready for use in tmLanguage or sublime-syntax files...
	##
	def GetMagicFunctionNames( _max_line_len = 100, _add_prefix_suffix_tabs = True ):
		_prefix	= '''	magic-function-names:
			- match: |-
					(?x)\b(__(?:
						'''
		_suffix	= '''
					)__)\b
				comment: these methods have magic interpretation by python and are generally called indirectly through syntactic constructs
				scope: support.function.magic.python'''

		## Accumulate the data...
		_data	= sorted( { ( _attr[ 2: -2 ] ) for _object in gc.get_objects( ) for _i, _attr in enumerate( dir( _object ) ) if _attr.startswith( '__' ) } );

		## Set up helpers.
		_words	= '';
		_len	= 0;

		## For each word
		for _word in _data: #.split( '|' ):
			## Add a pipe to the word
			_word = _word + '|';

			## Add the word
			_words = _words + _word;

			## Count the length of the word and add it to a counter
			_len = _len + len( _word );

			## If we've accurumulated more than _max_line_len characters since the last pip
			if ( _len > _max_line_len ):
				## Add a new line
				_words = _words + '\n' + ( '', '					' )[ _add_prefix_suffix_tabs ];

				## Reset the counter
				_len = 0;

		## Return the words without the last pipe...
		return ( '', _prefix )[ _add_prefix_suffix_tabs ] + _words[ : - 1 ] + ( '', _suffix )[ _add_prefix_suffix_tabs ];



##
##
##
## class UtilBase( ):
## 	pass;
class Util:
	##
	##
	##
	__name__ = 'Util'


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		_text					= '';

		##


		## _text += Acecool.Header( 'Util.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += Acecool.Footer( ) + '\n';


		_text += Acecool.Header( 'Util.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += Acecool.Footer( ) + '\n';


		## _text += Acecool.Header( 'Util.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += Acecool.Footer( ) + '\n';

		return _text;




	##
	## Helper Function used to process VarArgs...
	##
	def ProcessVarArgs( self, _args, _callback, _print_category, _print_text, *_text_args ):
		## Set up out output / return value to a blank-string...
		_output = '';

		## Make sure we have something to process
		if ( _args != None and len( _args ) > 0 ):
			## Process the data...
			for _i, _arg in enumerate( _args ):
				_output = _output + _callback( self, _i, _arg );

		## Convert the _text_args to print-data... _output is always {0}
		_print = [ ];
		_print.append( str( _output ) );
		if ( len( _text_args ) > 0 ):
			for _, _value in enumerate( _text_args ):
				_print.append( str( _value ) );

		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		print( _print_category, ( _print_text.format( *_print ) ) );

		## Always return something
		return _output;


	##
	## Similar to ProcessVarArgs without text... Simple processor of keyed varargs...
	##
	def ProcessSimpleVarArgs( self, _callback, *_args ):
		## Define the amount of iterations required...
		_varargs = len( _args );

		## If callback is provided and arguments are provided and we have a valid number of arguments...
		if ( callable( _callback ) and _args != None and _varargs > 0 ):

			## Loop through the varargs...
			for _index in range( _varargs ):
				## Grab the key / value for our index..
				_key = _args[ _index ];

				## Execute the callback with the key / value...
				_callback( self, _key );
		else:
			## Return false if we can't do anything..
			return False;

		## Return true when done...
		return True;


	##
	## Similar to ProcessVarArgs without text... Simple processor of keyed varargs...
	##
	def ProcessKeyVarArgs( self, _callback, *_args ):
		## Define the amount of iterations required...
		_vararg_pairs = math.floor( len( _args ) / 2 );

		## If callback is provided and arguments are provided and we have a valid number of arguments...
		if ( callable( _callback ) and _args != None and _vararg_pairs > 0 ):

			## Loop through the varargs...
			for _i in range( _vararg_pairs ):
				## Grab the appropriate index for the first element, ie the key...
				_index = ( _i * 2 );

				## Grab the key / value for our index..
				_key = _args[ _index ];
				_value = _args[ _index + 1 ];

				## Execute the callback with the key / value...
				_callback( self, _key, _value );
		else:
			## Return false if we can't do anything..
			return False;

		## Return true when done...
		return True;


	##
	## Similar to ProcessVarArgs without text... Simple processor of keyed varargs...
	##
	def ProcessAccumulateVarArgs( self, _callback, _data = [ ], *_args ):
		## Define the amount of iterations required...
		_varargs = len( _args );

		## If callback is provided and arguments are provided and we have a valid number of arguments...
		if ( callable( _callback ) and _args != None and _varargs > 0 ):

			## Loop through the varargs...
			for _index in range( _varargs ):
				## Grab the key / value for our index..
				_key = _args[ _index ];

				## Execute the callback with the key / value...
				_data.append( _callback( self, _key, _data ) );

		## Return true when done...
		return _data;


	##
	##
	##
	def ImportInvalidlyNamed( _base, _name_invalid, _name ):
		with open( _base + _name_invalid + '.py', 'rb' ) as _data:
			_name = imp.load_module( _name, _data, _name_invalid, ( '.py', 'rb', imp.PY_SOURCE ) );

		return _name;


	##
	## return Acecool.logic.ternary( not _data and _data != False, False, True )
	##
	def isset( _data = None ):
		if ( _data == None ):
			return False;

		_isset = True;
		try:
			_data;
		except NameError:
			_isset = False;

		return _isset;






##
## Objects
##



##
## Stop Watch - Timing System
##
class TimeKeeperBase( ):
	pass;
class TimeKeeper( TimeKeeperBase ):
	##
	##
	##
	__name__ = 'TimeKeeper';


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		_text					= '';

		##


		## _text += Acecool.Header( 'TimeKeeper.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += Acecool.Footer( ) + '\n';


		_text += Acecool.Header( 'TimeKeeper.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += Acecool.Footer( ) + '\n';


		## _text += Acecool.Header( 'TimeKeeper.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += Acecool.Footer( ) + '\n';

		return _text;


	##
	## One-Line Helpers
	##

	## Grab actual epoch timestamp from time... Should be in seconds since...
	def TimeStamp( self = None ):	return time.time( );

	## Grab actual epoch timestamp from time... Should be in Milli-Seconds since...
	## def TimeStampMS( self ):		return time.time( );

	## Grab actual epoch timestamp from time... Should be in Micro-Seconds since...
	## def TimeStampMicro( self ):		return time.time( );

	## Returns the length of time which has passed since the time keeper has started... if it hasn't then it'll use now as start and end time == 0
	def Len( self ):				return self.TimeEnded( ) - self.TimeStarted( );

	## Returns the Delta as a string
	def LenStr( self ):				return str( self.Len( ) );

	## Returns whether or not a Duration exists
	def HasDuration( self ):		return ( self.__duration__ == None or not IsNumber( self.__duration__ ) or IsNumber( self.__duration__ ) and self.__duration__ <= 0 );

	## Returns the TimeKeeper Duration - if one was given... otherwise returns the length of time which has passed since time has started...
	def Duration( self ):			return TernaryFunc( self.HasDuration( ), self.__duration__, self.Len( ) );

	## Returns the Delta as a string
	def DurationStr( self ):		return str( self.Duration( ) );

	## Returns the Delta as a string
	def DeltaStr( self ):			return str( self.Delta( ) );

	## Returns the timestamp as a string
	def TimeStampStr( self ):		return str( self.TimeStamp( ) );

	## Returns whether or not the timekeeper tracking has started...
	def HasStarted( self ):			return ( IsNumber( self.__timestamp_start__ ) );

	## Returns the Time Started or NOW
	def TimeStarted( self ):
		if ( self.HasStarted( ) ):
			return self.__timestamp_start__;

		return self.TimeStamp( );

	## Returns whether or not the timekeeper tracking has end...
	def HasEnded( self ):			return ( IsNumber( self.__timestamp_end__ ) );

	## Returns the Time Ended or NOW
	def TimeEnded( self ):
		if ( self.HasEnded( ) ):
			return self.__timestamp_end__;

		return self.TimeStamp( );

	##
	## def DeltaStr( self ):			return str( self.Delta( ) );

	##
	## def DeltaStr( self ):			return str( self.Delta( ) );


	##
	## On Initialization - if duration is given it is very useful for progress-bars, etc...
	## Task: Add Duration update system where I can time how long something has taken, and return how many more somethings it has to do to live-calculate how long the total somethings should take to process for a live-progressbar instead of timed...
	##
	def __init__( self, _duration = None, _name = None ):
		##
		self.__timekeeper_name__	= _name;
		## Sets up the duration var
		self.__duration__			= TernaryFunc( ( _duration != None and IsNumber( _duration ) and _duration > 0 ), _duration, None );

		## Set up when the timekeeper was created
		self.__timestamp_created__	= self.TimeStamp( );

		## Set up when the timekeeper started
		self.__timestamp_start__	= None;

		## End is set when Stop is called
		self.__timestamp_end__		= None;

		## Delta = last time GetDelta was called...
		self.__timestamp_delta__	= self.__timestamp_start__;

		## Save is used for XCM - Deprecated with Delta added...
		## self.__timestamp_save__		= None;


	##
	##
	##
	def __str__( self ):
		## String.FormatColumn( _width = 25, _text = '', *_varargs );
		## String.FormatSimpleColumn( _width = 25, *_varargs );

		##
		_text = ''

		##
		if ( self.Name( 50 - 17 ).strip( ) != 'N/A' ):
			_text += String.FormatSimpleColumn( 50, '' + self.Name( 50 - 17 ) + ' - ' );
		else:
			_text += 'N/A - ';

		##
		if ( self.DeltaStr( ) != '0.0' ):
			_text += String.FormatSimpleColumn( 20, 'Delta: ' + self.DeltaStr( ) + ' ' );

			##
		if ( self.HasStarted( ) ):
			_text += String.FormatSimpleColumn( 20, 'Elapsed: ' + self.LenStr( ) + ' ' );

		##
		return '[ TimeKeeper -> ' + _text + ' ]; ';


	##
	## Clearing of values for shutdown
	##
	def __unload__( ):
		self.__duration__			= None;
		self.__timestamp_start__	= None;
		self.__timestamp_end__		= None;
		self.__timestamp_delta__	= None;
		## self						= None;


	##
	## Returns the name of the TimeKeeper so we can keep track of differently named timekeepers...
	##
	def Name( self, _max = -1 ):
		##
		_text = TernaryFunc( IsString( self.__timekeeper_name__ ), self.__timekeeper_name__, 'N/A' );

		##
		if ( _max >= 0 ):
			return _text[ : _max ];

		##
		return _text;


	##
	## Starts the timer by setting a start-time - Returns self so I can create the timestamp, start it all in the same line, or chain events...
	##
	def Start( self ):
		##
		self.__timestamp_start__	= self.TimeStamp( );

		##
		return self;


	##
	## Stops the timer by setting an end time - Returns self so I can chain events...
	##
	def Stop( self, _update_delta = True ):
		##
		self.__timestamp_stop__		= self.TimeStamp( );

		##
		if ( _update_delta ):
			self.Delta( True );

		##
		return self;


	##
	## Returns the Delta Time since the last time this function was called
	##
	def Delta( self, _update = True ):
		_delta = TernaryFunc( self.__timestamp_delta__ == None, self.TimeStamp( ), self.__timestamp_delta__ )
		_time = self.TimeStamp( );

		if ( _update ):
			self.__timestamp_delta__ = self.TimeStamp( );

		## Return the ( NOW - LAST ) time for ▲
		return _time - _delta;


	##
	## Helper - Returns how long it took to execute / parse, etc.. the output generation so I can time caching system vs parsing..
	##
	def GetTimeDelta( self, _start = None, _end = None ):
		## If Start wasn't provided as an argument..
		if ( _start == None ):
			_start = self.__timestamp_start__;

		## If _end wasn't provided as an argument...
		if ( _end == None ):
			## If __timestamp_end__ isn't set, we grab the current timestamp... or _start is identical to _end... then we must be grabbing a STEP timestamp... recapture the timestamp for this time but don't update end...
			if ( self.__timestamp_end__ == None or _start == self.__timestamp_end__ ):
				## For loading the cached file..
				_end = self.GetTimeStamp( );
			else:
				## Otherwise we use it..
				_end = self.__timestamp_end__;

		## Return the DeltaTime...
		return _end - _start;










































































































































































































































































































































































##
## AccessorFunc Exception - Used when we want to throw an error / exception in case the developer isn't using the AccessorFunc system correctly...
##
class AccessorFuncExceptionBase( Exception ):
	pass;
class AccessorFuncException( AccessorFuncExceptionBase ):
	##
	##
	##
	__name__ = 'AccessorFuncException';


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		_text					= '';

		##


		## _text += Acecool.Header( 'AccessorFuncException.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += Acecool.Footer( ) + '\n';


		_text += Acecool.Header( 'AccessorFuncException.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += Acecool.Footer( ) + '\n';


		## _text += Acecool.Header( 'AccessorFuncException.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += Acecool.Footer( ) + '\n';

		return _text


	##
	##
	##
	def __init__( self, _key = 'Unknown Key', _error = 'General Exception' ):
		##
		print( '[ AccessorFuncException ' + str( _key ) + ' ] ' + _error )

		##
		return super( ).__init__( )






## ##
## ## AccessorFunc Class - Works similar to GMod implementation... Lots of features, easy to add / create new helpers...
## ##
## ## class AccessorFuncBase( ):
## ## 	pass

## class AccessorFunc( AccessorFuncBase ):
## 	##
## 	## AccessorFuncs for the AccessorFunc class - to simplify a few things...
## 	##


## 	##
## 	## Create a new AccessorFunc Object for a class....
## 	##
## 	def __init__( self, _parent ):
## 		## Setup the AccessorFunc class table..
## 		self.data = { }

## 		## Set the parent class - this is where the functions are added while the data remains within this object...
## 		self.parent = _parent

## ACCESSORFUNC_ID_GET								= 'get';
## ACCESSORFUNC_ID_GET_RAW							= 'get_raw';
## ACCESSORFUNC_ID_ISSET							= 'isset';
## ACCESSORFUNC_ID_GET_STR							= 'get_str';
## ACCESSORFUNC_ID_GET_LEN							= 'get_len';
## ACCESSORFUNC_ID_GET_LEN_STR						= 'get_len_str';
## ACCESSORFUNC_ID_GET_DEFAULT_VALUE				= 'get_default_value';
## ACCESSORFUNC_ID_GET_ACCESSOR						= 'get_accessor';
## ACCESSORFUNC_ID_GET_ALLOWED_TYPES				= 'get_allowed_types';
## ACCESSORFUNC_ID_GET_ALLOWED_VALUES				= 'get_allowed_values';
## ACCESSORFUNC_ID_GET_BASE_ALLOWED_TYPES			= 'get_base_allowed_types';
## ACCESSORFUNC_ID_GET_BASE_ALLOWED_VALUES			= 'get_base_allowed_values';
## ACCESSORFUNC_ID_HAS_GETTER_PREFIX				= 'has_getter_prefix';
## ACCESSORFUNC_ID_GET_GETTER_PREFIX				= 'get_getter_prefix';
## ACCESSORFUNC_ID_GET_NAME							= 'get_name';
## ACCESSORFUNC_ID_GET_KEY							= 'get_key';
## ACCESSORFUNC_ID_GET_ACCESSOR_KEY					= 'get_accessor_key';
## ACCESSORFUNC_ID_GET_DATA_KEY						= 'get_data_key';
## ACCESSORFUNC_ID_SET								= 'set';
## ACCESSORFUNC_ID_ADD								= 'add';
## ACCESSORFUNC_ID_RESET							= 'reset';
## ACCESSORFUNC_ID_GET_HELPERS						= 'get_helpers';
## ACCESSORFUNC_ID_GET_OUTPUT						= 'get_output';
## ACCESSORFUNC_ID_GET_GETTER_OUTPUT				= 'get_getter_output';


## ACCESSORFUNC_ID_GET								= 'get';
## ACCESSORFUNC_ID_GET_RAW							= 'get_raw';
## ACCESSORFUNC_ID_ISSET							= 'isset';
## ACCESSORFUNC_ID_GET_STR							= 'get_str';
## ACCESSORFUNC_ID_GET_LEN							= 'get_len';
## ACCESSORFUNC_ID_GET_LEN_STR						= 'get_len_str';
## ACCESSORFUNC_ID_GET_DEFAULT_VALUE				= 'get_default_value';
## ACCESSORFUNC_ID_GET_ACCESSOR						= 'get_accessor';
## ACCESSORFUNC_ID_GET_ALLOWED_TYPES				= 'get_allowed_types';
## ACCESSORFUNC_ID_GET_ALLOWED_VALUES				= 'get_allowed_values';
## ACCESSORFUNC_ID_GET_BASE_ALLOWED_TYPES			= 'get_base_allowed_types';
## ACCESSORFUNC_ID_GET_BASE_ALLOWED_VALUES			= 'get_base_allowed_values';
## ACCESSORFUNC_ID_HAS_GETTER_PREFIX				= 'has_getter_prefix';
## ACCESSORFUNC_ID_GET_GETTER_PREFIX				= 'get_getter_prefix';
## ACCESSORFUNC_ID_GET_NAME							= 'get_name';
## ACCESSORFUNC_ID_GET_KEY							= 'get_key';
## ACCESSORFUNC_ID_GET_ACCESSOR_KEY					= 'get_accessor_key';
## ACCESSORFUNC_ID_GET_DATA_KEY						= 'get_data_key';
## ACCESSORFUNC_ID_SET								= 'set';
## ACCESSORFUNC_ID_RESET							= 'reset';
## ACCESSORFUNC_ID_GET_HELPERS						= 'get_helpers';
## ACCESSORFUNC_ID_GET_OUTPUT						= 'get_output';
## ACCESSORFUNC_ID_GET_GETTER_OUTPUT				= 'get_getter_output';

##
## AccessorFuncBase: An easy way to add Dynamic Properties - Simply define the __<key> = AccessorFuncBase( TheClassYouAddItTo, '<key>', 'DefaultValue - You may need to create a base-class with only pass in it, and extend your primary class from that then add it in..' )
##
## Usage:
## -------------------------------------------------------------------
## 	class MyClassBase:
## 		pass
## 	class MyClass( MyClassBase ):
## 		# Note: Arg 4 is optional to set Allowed Data-Types and can be None, a type( x ) or a Dict, List or Tuple of type( x )'s which is converted to an O( 1 ) Dict of allowed data-types used to restrict the setter..
## 		# Note: Arg 5 is optional to set Allowed Stored Values and can be None, a single value or a Dict, List or Tuple of values which is converted to an O( 1 ) Dict of allowed values used to restrict the setter..
## 		# Note: I am working on adding min / max restrictions for number-based getters / setters, and callbacks used to alter the data and help in many ways...
## 		__Height = AccessorFuncBase( MyClassBase, 'Height', 0 )
## 		__Width = AccessorFuncBase( MyClassBase, 'Width', 1, ( type( 0 ), type( 0.0 ) ) )
## 		__Depth = AccessorFuncBase( MyClassBase, 'Depth', 2, None, ( 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ) )
##
## 		def __str__( self ):
## 			return 'Height: ' + str( self.Height ) + '\t\tWidth: ' + str( self.Width ) + '\t\tDepth: ' + str( self.Depth )
##
## 		## Or
##
## 		def __str__( self ):
## 			_text = 'Height: ' + str( self.Height )
## 			_text += '\t\tWidth: ' + str( self.Width )
## 			_text += '\t\tDepth: ' + str( self.Depth )
## 			return _text
##
##
## 	_class = MyClass( )
## 	print( _class )
## 	_class.Height = 10
## 	_class.Width = 20
## 	_class.Depth = 30
## 	print( _class )
##	_class.Depth = 9
##	print( _class )
## -------------------------------------------------------------------
## Output:
## Height: 0		Width: 1		Depth: 2
## Height: 10		Width: 20		Depth: 2
## Height: 10		Width: 20		Depth: 9
##
##
## AccessorFuncBase FlowChart
##	-> __init__ is called with key information letting us know which functions we want to assign, whether we use the defaults or custom replacements, and more...
##	-> All of the functions are defined in local memory in preparation for the registry phase - I may be able to improve this efficiency / overhead to reduce it to the amount necessary and no more... It'd mean slightly more logic, but constant, but I'd still prefer an alternative..
##	-> Each local function is passed through to the Registry System with an identifier such as get, set, etc.. If the setup table ahsn't been changed, all of them are allowed... If the setup_override is setup, then only the ones which have been altered or exist in that table will hav eany bearing on what is to come.. Both tables can be used - ie Setup to prevent all, then setup
## _override to set, or change, but this isn't necessary... If you want to remove all, simply use setup.. If you simply want to fchange 1 or more, use setup_override.
##
##
##
##
## Task: Add an IsUsing*ToString style TernaryFunc return for Boolean Type information to return Enabled / Disabled, On / Off, True / False, etc... helper when requested...
## def IsUsingCacheSystemToString( self ):
## 	return ( 'Disabled', 'Enabled' )[ self.GetSetting( 'use_caching_system', False ) ]
##
## Task: Add SetMin, SetMax helpers for number data-types ( Float is set at 0.0 and 1.0 by default )
## Task: Convert current system of tons of functions, etc.. for function names, function call examples, and so on into the setup table so they can all be referencced using a simple ENUMerator...
## Task: Setup several defaults - AcecoolFuncBase as ->.CreateGetter( 'Name', 'key', _func_override, _on_callback, _post_callback ), ->.SetupFloat( min / max at 0.0, 1.0, etc.. ), ->.SetupString( ), ->.SetupUltraLight( Get, Set, Add, and a few others - but not much more )
## Task: Setup Instance Table containing a list of all accessors, and link the helper table to that list and link it into the Setup Tables to provide EXACT results of what is enabled...
## Task: Setup File Output / wiki.txt or wiki.md style file generation which lists all functions created instead of output to text - or in wiki/filename.md ( so AcecoolCodeMappingSystem.py, Acecool.py, etc.. )..
## Task:
##
##
##
##
##
##
class AccessorFuncCoreBase:

	## The key to target in parent object
	__key				= None;
	__name				= None;
	__prefix_get		= None;

	## Default Data, if provided
	__default			= None;

	## Allowed types / values
	__allowed_types		= None;
	__allowed_values	= None;

	##
	__documentation		= '[ this.{key} / this._{key} / this.__{key} Documentation ] {documentation} - Default Value: {default_value} - Allowed Data-Types: {allowed_types} - Allowed Values: {allowed_values}';


	##
	## Returns the data from this object in a nice ouput
	##
	def __str__( self ):
		##
		_text = '[ AccessorFuncBase ] Key: ' + str( self.__key ) + ' :::: Class ID: ' + str( id( self.__class__ ) ) + ' :::: self ID: ' + str( id( self ) );
		_text += '\t\tDefault: ' + str( self.DefaultValue( ) );
		_text += '\t\tAllowed Types: ' + str( self.GetAllowedTypes( ) );
		_text += '\t\tAllowed Values: ' + str( self.GetAllowedValues( ) );
		return _text;


	##
	## AccessorFunc Node...
	##
	@classmethod
	def AddProperty( _class, _parent, _name, _default = None, _allowed_types = None, _allowed_values = None, _documentation = '', _prefix_get = 'Get', _key = None, _allow_erroneous_default = False, _options = { } ):
		return _class(
			parent						= _parent,
			name						= _name,
			default						= _default,
			allowed_types				= _allowed_types,
			allowed_values				= _allowed_values,
			documentation				= _documentation,
			getter_prefix				= _prefix_get,
			key							= _key,
			allow_erroneous_default		= _allow_erroneous_default,
			options						= _options
		);


	##
	## AccessorFunc Node...
	##
	@classmethod
	def AddGetter( _class, _parent, _key, _name, _func ):
		return _class(
			parent						= _parent,
			name						= _name,
			documentation				= 'Getter: ' + _name,
			key							= _key,
			setup						= { 'get': _func }
		);


	##
	## Uses keyed variable arguments instead of forced a, c, b arguments...
	##
	## Task: Create Get / Set Min / Max Helper Accessors for Data-Types restricted to Numbers..
	## Task: Create 0 - 1 Fraction Restrictions Automatic if Type Restricted to TYPE_FLOAT_MOD or something... ie, set min / max value restrictions to 0, 1, allowed types FLOAT..
	## Task: Create other Data-Type Specific Helpers to make data-type and value restrictions easier to setup or manage...
	## Task: Create Setup ids as enumerated values
	## Task: Update setup to include more than 1 value... ie it will contain fail message, on setup callback, pre on set callback, the function name, the function argument helper output, and more... When I override setup from the init in the future it will work like stup override... so the base table is all, if I cchange something and only want that, the default info is read from the table and stuff I change  or add si used otherwise default... so override lets me change one of the list or many.. setup lets me start almost from scratch but re-use elements from the ones I want...
	##
	## Note: The following Callbacks have been implemented...
	## 	on_set, on_set_fail
	##
	##
	def __init__( self, **_varargs ):
		## Advanced Setup - By default, all helpers are defined... However, if Setup is defined, ALL must be set to True, or only the ones set to True will be created, none others...]
		_setup_defaults = {
			## All Instance IDs are renamed to this, for now... Do not replace this function...
			'instance':		True,

			## ## All Instance Accessor IDs
			## 'instance_reset_accessors_by_name':			True,
			## 'on_instance_reset_accessors_by_name':		False,
			## 'pre_instance_reset_accessors_by_name':		False,

			## ##  - Calls ( this, ) - Returns
			## 'instance_set_accessors_by_name':			True,
			## 'on_instance_set_accessors_by_name':		False,
			## 'pre_instance_set_accessors_by_name':		False,

			## ##  - Calls ( this, ) - Returns
			## 'instance_get_accessors_by_name':			True,
			## 'on_instance_get_accessors_by_name':		False,
			## 'pre_instance_get_accessors_by_name':		False,

			## ##  - Calls ( this, ) - Returns
			## 'instance_reset_accessors_by_key':			True,
			## 'on_instance_reset_accessors_by_key':		False,
			## 'pre_instance_reset_accessors_by_key':		False,

			## ##  - Calls ( this, ) - Returns
			## 'instance_set_accessors_by_key':			True,
			## 'on_instance_set_accessors_by_key':			False,
			## 'pre_instance_set_accessors_by_key':		False,

			## ##  - Calls ( this, ) - Returns
			## 'instance_get_accessors_by_key':			True,
			## 'on_instance_get_accessors_by_key':			False,
			## 'pre_instance_get_accessors_by_key':		False,

			## ##  - Calls ( this, ) - Returns
			## 'instance_reset_grouped_accessors':			True,
			## 'on_instance_reset_grouped_accessors':		False,
			## 'pre_instance_reset_grouped_accessors':		False,

			## ##  - Calls ( this, ) - Returns
			## 'instance_set_grouped_accessors':			True,
			## 'on_instance_set_grouped_accessors':		False,
			## 'pre_instance_set_grouped_accessors':		False,

			## ##  - Calls ( this, ) - Returns
			## 'instance_add_grouped_accessors':			True,
			## 'on_instance_add_grouped_accessors':		False,
			## 'pre_instance_add_grouped_accessors':		False,

			## ##  - Calls ( this, ) - Returns
			## 'instance_get_grouped_accessors':			True,
			## 'on_instance_get_grouped_accessors':		False,
			## 'pre_instance_get_grouped_accessors':		False,

			## ##  - Calls ( this, ) - Returns
			## 'instance_get_accessors_database':			True,
			## 'on_instance_get_accessors_database':		False,
			## 'pre_instance_get_accessors_database':		False,

			## ##  - Calls ( this, ) - Returns
			## 'instance_add_accessors_database':			True,
			## 'on_instance_add_accessors_database':		False,
			## 'pre_instance_add_accessors_database':		False,

			## ##  - Calls ( this, ) - Returns
			## 'instance_reset_accessors_database':		True,
			## 'on_instance_reset_accessors_database':		False,
			## 'pre_instance_reset_accessors_database':	False,


			##
			## All of the others Accessor IDs...
			##

			## Get - Calls Get( this, ) - Returns
			'get':										True,

			## Get - Calls Get( this, ) - Returns
			'get_raw':									True,

			## Get - Calls Get( this, ) - Returns
			'isset':									True,

			## Get - Calls Get( this, ) - Returns
			'get_str':									True,

			## Get - Calls Get( this, ) - Returns
			'get_len':									True,

			## Get - Calls Get( this, ) - Returns
			'get_len_str':								True,

			## Get - Calls Get( this, ) - Returns
			'get_default_value':						True,

			## Get - Calls Get( this, ) - Returns
			'get_property':								True,

			## Get - Calls Get( this, ) - Returns
			'get_accessor':								True,

			## Get - Calls Get( this, ) - Returns
			'get_allowed_types':						True,

			## Get - Calls Get( this, ) - Returns
			'get_allowed_values':						True,

			## Get - Calls Get( this, ) - Returns
			'get_base_allowed_types':					True,

			## Get - Calls Get( this, ) - Returns
			'get_base_allowed_values':					True,

			## Get - Calls Get( this, ) - Returns
			'has_getter_prefix':						True,

			## Get - Calls Get( this, ) - Returns
			'get_getter_prefix':						True,

			## Get - Calls Get( this, ) - Returns
			'get_name':									True,

			## Get - Calls Get( this, ) - Returns
			'get_key':									True,

			## Get - Calls Get( this, ) - Returns
			'get_accessor_key':							True,

			## Get - Calls Get( this, ) - Returns
			'get_data_key':								True,

			## Set - Calls Get( this, ) - Returns
			'set':										True,

			## Add - Calls Get( this, ) - Returns
			'add':										True,

			## Pop - Calls Pop( this, ) - Returns
			'pop':										False,

			## Get - Calls Get( this, ) - Returns
			'reset':									True,

			## Get - Calls Get( this, ) - Returns
			'get_helpers':								True,

			## Get - Calls Get( this, ) - Returns
			'get_output':								True,

			## Get - Calls Get( this, ) - Returns
			'get_getter_output':						True,


			##
			## All of the others Accessor IDs...
			##


			##
			## New System for handling function additions, when they are added, and more...
			##
			## ID					Included?		Description
			## name:								This is the function name - A simple string with formatted key replacers are added.. All of the keys are passed through and only the ones the string provides will be replaced. This name shouldn't include this., arguments, etc.. Just the raw name.
			## name_call:			X				This is the callable function name - It will have a new key {name} which is the previous value after being formatted. So it'd only need: 'this/self.{name}( args );' as the string. and this / self can be set as a key too. I may simply just set up an args key which will include the arguments instead of this, as the full function callable name can be built - and I'd simply assign a new tag to determine if it is an instance function ( which is added to the class which adds the property / accessor which uses this. as an identifier in debugging output ) or a property / an accessor function ( which is a function residing in this property / accessor object, identified as self. in debugging output )
			## args:								This is the arguments string - ie for pop: '_index = len - 1, _count = 1', for get: '_default_override = None, _no_default_return = False', etc...
			## type:				X				This is the type of function - ie if it is an instance function ( which generates this. in the name ) or a property function ( which uses self. ) - Note: I may not use this as all instance functions use the instance_ prefix and in the registry, as they are all added or not, they are identified as 'instance'
			## active:								This sets the default enabled status of a particular function
			## active_on:							This sets a condition when the default enabled status can be overriden automatically ( without the dev adding it to setup_override, or setup in the initialization phase )
			## callback_on:							This sets a callback function to be executed during a function - for get, this would be called with the currently assigned value and the potential new value along with whether or not that new value passes allowed-types / values checks. The return would be whether or not to override, and a new value if override. It can either be the current value, unchanged, or a new value ( this value will be subjected to the same allowed-types / values checks )
			## callback_pre:						This sets a callback function to be executed prior to a function being called.. similar to _on, this receives the same information however the only choices are to allow or prevent the function from continuing.. Maybe more later..
			## callback_post:						This sets a callback function to be executed after a function has been called.. similar to _on, this receives the old value, and the new value - but the new value is what has already been assigned.
			##
			##

			## Default Table values - Only add or change what needs changing...
			ACCESSORFUNC_ID_SETUP_DEFAULTS: {
				'name_call_property':				'{self}.{name}( {args} );',
				'name_call_instance':				'{this}.{name}( {args} );',
				'name':								'{get}{PropertyName}',
				'key':								'{PropertyKey}',
				## 'name_call':						'{name_call_instance}',
				'args':								'_default_override = None, _hide_default_return = False',
				## 'type':								ACCESSORFUNC_TYPE_INSTANCE,
				'type':								ACCESSORFUNC_TYPE_PROPERTY,
				'active':							True,
				'active_on':						None,
				'callback_on':						None,
				'callback_pre':						None,
				'callback_post':					None,

			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_RAW: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_ISSET: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_STR: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_LEN: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_LEN_STR: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_DEFAULT_VALUE: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_ACCESSOR: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_ALLOWED_TYPES: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_ALLOWED_VALUES: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_BASE_ALLOWED_TYPES: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_BASE_ALLOWED_VALUES: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_HAS_GETTER_PREFIX: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_GETTER_PREFIX: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_NAME: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_KEY: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_ACCESSOR_KEY: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_DATA_KEY: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_SET: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_ADD: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_POP: {
				'active':							False,
				'active_on':						( lambda self: self.IsListProperty( ) ),
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_RESET: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_HELPERS: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_OUTPUT: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},

			## Get - Calls Get( this, ) - Returns
			ACCESSORFUNC_ID_GET_GETTER_OUTPUT: {
				'active':							True,
				'callback_on':						False,
				'callback_pre':						False,
				'callback_post':					False,
			},


			##
			## All of the Accessor IDs which can be called after the main function is called... For Setters, it can be used to make something else happen. For getters, it can trigger events, or change the returns - But this, because it happens after, will have the result provided to the callback.....
			##

			##  OnGet - Calls OnGet( this, _current, _raw ) - Return anything other than None, if authorized type or value, will return that value - otherwise the normal function will run its return ... )
			'on_get':									False,
			'on_get_fail':								False,
			'on_get_fail_msg':							'OnGetFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_raw':								False,
			'on_get_raw_fail':							False,
			'on_get_raw_fail_msg':						'OnGetRawFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_isset':									False,
			'on_isset_fail':							False,
			'on_isset_fail_msg':						'OnIssetFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_str':								False,
			'on_get_str_fail':							False,
			'on_get_str_fail_msg':						'OnGetStrFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_len':								False,
			'on_get_len_fail':							False,
			'on_get_len_fail_msg':						'OnGetLenFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_len_str':							False,
			'on_get_len_str_fail':						False,
			'on_get_len_str_fail_msg':					'OnGetLenStrFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_default_value':						False,
			'on_get_default_value_fail':				False,
			'on_get_default_value_fail_msg':			'OnGetDefaultValueFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_accessor':							False,
			'on_get_accessor_fail':						False,
			'on_get_accessor_fail_msg':					'OnGetAccessorFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_allowed_types':						False,
			'on_get_allowed_types_fail':				False,
			'on_get_allowed_types_fail_msg':			'OnGetAllowedTypesFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_allowed_values':					False,
			'on_get_allowed_values_fail':				False,
			'on_get_allowed_values_fail_msg':			'OnGetAllowedValuesFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_base_allowed_types':				False,
			'on_get_base_allowed_types_fail':			False,
			'on_get_base_allowed_types_fail_msg':		'OnGetBaseAllowedTypesFail:Default Message',

			## On - Calls On( this, ) - Return
			'on_get_base_allowed_values':				False,
			'on_get_base_allowed_values_fail':			False,
			'on_get_base_allowed_values_fail_msg':		'OnGetBaseAllowedValuesFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_has_getter_prefix':						False,
			'on_has_getter_prefix_fail':				False,
			'on_has_getter_prefix_fail_msg':			'OnHasGetterPrefixFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_getter_prefix':						False,
			'on_get_getter_prefix_fail':				False,
			'on_get_getter_prefix_fail_msg':			'OnGetGetterPrefixFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_name':								False,
			'on_get_name_fail':							False,
			'on_get_name_fail_msg':						'OnGetNameFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_key':								False,
			'on_get_key_fail':							False,
			'on_get_key_fail_msg':						'OnGetNameFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_accessor_key':						False,
			'on_get_accessor_key_fail':					False,
			'on_get_accessor_key_fail_msg':				'OnGetAccessorKeyFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_data_key':							False,
			'on_get_data_key_fail':						False,
			'on_get_data_key_fail_msg':					'OnGetDataKeyFail: Default Message',

			## OnSet - Calls OnSet( this, _current, _new, _is_authorized ) - Used to trigger a call-chained from the Setter, if you return any other value than None, that value will be set if authorized..
			'on_set':									False,
			'on_set_fail':								False,
			'on_set_fail_msg':							'OnSetFail: Default Message',

			## OnAdd - Calls OnAdd( this, _current, _new, _is_authorized ) - Used to trigger a call-chained from the Setter, if you return any other value than None, that value will be set if authorized..
			'on_add':									False,
			'on_add_fail':								False,
			'on_add_fail_msg':							'OnAddFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_reset':									False,
			'on_reset_fail':							False,
			'on_reset_fail_msg':						'OnResetFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_helpers':							False,
			'on_get_helpers_fail':						False,
			'on_get_helpers_fail_msg':					'OnGetHelpersFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_output':							False,
			'on_get_output_fail':						False,
			'on_get_output_fail_msg':					'OnGetOutputFail: Default Message',

			## On - Calls On( this, ) - Return
			'on_get_getter_output':						False,
			'on_get_getter_output_fail':				False,
			'on_get_getter_output_fail_msg':			'OnGetGetterOutputFail: Default Message',


			##
			## All of the Accessor IDs which can be called BEFORE the main function is called... For Setters, it can be used to prevent a value from being set, or alter it.... For Getters, it can return a different value or influence wht is returned.. - This will not have the result provided to the callback - the default value, etc.. will be.. but the current value may not be... or the current may be for set,, and the value we intend to set, etc... but for getters the grabbing the raw data may be more work..
			##

			## Pre-Get will call PreGet( this ) - If I add the raw or current value, then it is called after the function call... Returning anything other than Nne will return that value ( if it is authorized by restricted types / values.. if not, None is used )..
			'pre_get':									False,
			'pre_get_fail':								False,
			'pre_get_fail_msg':							'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_raw':								False,
			'pre_get_raw_fail':							False,
			'pre_get_raw_fail_msg':						'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_isset':								False,
			'pre_isset_fail':							False,
			'pre_isset_fail_msg':						'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_str':								False,
			'pre_get_str_fail':							False,
			'pre_get_str_fail_msg':						'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_len':								False,
			'pre_get_len_fail':							False,
			'pre_get_len_fail_msg':						'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_len_str':							False,
			'pre_get_len_str_fail':						False,
			'pre_get_len_str_fail_msg':					'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_default_value':					False,
			'pre_get_default_value_fail':				False,
			'pre_get_default_value_fail_msg':			'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_accessor':							False,
			'pre_get_accessor_fail':					False,
			'pre_get_accessor_fail_msg':				'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_allowed_types':					False,
			'pre_get_allowed_types_fail':				False,
			'pre_get_allowed_types_fail_msg':			'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_allowed_values':					False,
			'pre_get_allowed_values_fail':				False,
			'pre_get_allowed_values_fail_msg':			'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_base_allowed_types':				False,
			'pre_get_base_allowed_types_fail':			False,
			'pre_get_base_allowed_types_fail_msg':		'OnFail: Default Message',



			## Pre - Calls Pre( this, ) - Return
			'pre_get_base_allowed_values':				False,
			'pre_get_base_allowed_values_fail':			False,
			'pre_get_base_allowed_values_fail_msg':		'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_has_getter_prefix':					False,
			'pre_has_getter_prefix_fail':				False,
			'pre_has_getter_prefix_fail_msg':			'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_getter_prefix':					False,
			'pre_get_getter_prefix_fail':				False,
			'pre_get_getter_prefix_fail_msg':			'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_name':								False,
			'pre_get_name_fail':						False,
			'pre_get_name_fail_msg':					'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_key':								False,
			'pre_get_key_fail':							False,
			'pre_get_key_fail_msg':						'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_accessor_key':						False,
			'pre_get_accessor_key_fail':				False,
			'pre_get_accessor_key_fail_msg':			'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_data_key':							False,
			'pre_get_data_key_fail':					False,
			'pre_get_data_key_fail_msg':				'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_set':									False,
			'pre_set_fail':								False,
			'pre_set_fail_msg':							'OnSetFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_add':									False,
			'pre_add_fail':								False,
			'pre_add_fail_msg':							'OnAddFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_reset':								False,
			'pre_reset_fail':							False,
			'pre_reset_fail_msg':						'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_helpers':							False,
			'pre_get_helpers_fail':						False,
			'pre_get_helpers_fail_msg':					'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_output':							False,
			'pre_get_output_fail':						False,
			'pre_get_output_fail_msg':					'OnFail: Default Message',


			## Pre - Calls Pre( this, ) - Return
			'pre_get_getter_output':					False,
			'pre_get_getter_output_fail':				False,
			'pre_get_getter_output_fail_msg':			'OnFail: Default Message',

		}


		## Helpers...
		## Task: Integrate keys and names system - remove singular name / key system... Set it up so that all keys are implemented, with 'key' or the first in keys being the primary, and all others link to that reference... For names, the same thing, generate the functions..
		_parent								= _varargs.get( 'parent',						None );
		_key								= _varargs.get( 'key',							'Key' );
		_keys								= _varargs.get( 'keys',							None );
		_name								= _varargs.get( 'name',							'Name' );
		_names								= _varargs.get( 'names',						None );
		_default							= _varargs.get( 'default',						None );
		_allowed_types						= _varargs.get( 'allowed_types',				TYPE_ANY );
		_allowed_values						= _varargs.get( 'allowed_values',				VALUE_ANY );
		_documentation						= _varargs.get( 'documentation',				_name + ' Documentation' );
		_getter_prefix						= _varargs.get( 'getter_prefix',				'Get' );
		_allow_erroneous_default			= _varargs.get( 'allow_erroneous_default',		False );
		_display_created_accessors			= _varargs.get( 'show_accessors',				False );
		_options							= _varargs.get( 'options',						{ } );
		_setup								= _varargs.get( 'setup',						_setup_defaults );

		## ## It this Accessor able to use super( ).__init__( ... ) to update the values?
		## _keys								= _varargs.get( 'keys',							None );

		## Setup Override works differently to Setup - If the user defines Setup and only adds 1 function to True, then only that func is fcreated...
		## For override, if one is set to True or False, then that key in Setup is altered to the new value so it lets you make changes to the default list...
		_setup_override						= _varargs.get( 'setup_override',				{ } );


		##
		## print( '[ Acecool Library -> AccessorFuncBase -> Init ] Creating: ' + _name );
		## print( '[ Acecool Library -> AccessorFuncBase -> Init ] Using Key: ' + _key );
		## print( '[ Acecool Library -> AccessorFuncBase -> Init ] With Key Aliases: ' + str( _keys ) );
		## print( '[ Acecool Library -> AccessorFuncBase -> Init ] With Name Aliases: ' + str( _names ) );

		## If we have anything to alter, we do it...
		if ( len( _setup_override ) > 0 ):
			for _k, _v in _setup_override.items( ):
				## print( '[ Acecool Library -> AccessorFuncBase -> Init ] SetupOverride Is altering _setup( ' + _key + ': ' + str( _setup.get( _key, False ) ) + ' ) to: ' + str( _value ) );
				_setup[ _k ] = _v;

		## setup = { get = True }

		## ##
		## _text								= '\n' + String.FormatColumnEx( '-', 130, ' ' )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'Parent: ',						100, str( _parent ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'name: ',							100, str( _name ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'default: ',						100, str( _default ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'allowed_types: ',					100, str( _allowed_types ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'allowed_values: ',				100, str( _allowed_values ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'documentation: ',					100, str( _documentation ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'getter_prefix: ',					100, str( _getter_prefix ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'key: ',							100, str( _key ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'allow_erroneous_default: ',		100, str( _allow_erroneous_default ) )
		## _text								+= '\n\t' + String.FormatColumn( 30, 'options: ',						100, str( _options ) )
		## _text								+= '\n' + String.FormatColumnEx( '-', 130, ' ' )
		## ## _text								+= String.FormatColumn(  )
		## ## _text								+= String.FormatColumn(  )
		## ## _text								+= String.FormatColumn(  )
		## ## _text								+= String.FormatColumn(  )

		## ##
		## print( 'AccessorFuncBase.AddProperty: ' + _text )

		##
		self.__class_name			= _parent.__name__;

		## Setup name / Key system... Name is for functions, key is for data storage / property ie self.x, self.y, self.z instead of self.X, self.Y, self.Z which is annoying to write... but self.Height works...
		self.__name					= _name;
		self.__names				= _names;
		self.__key					= ( _key, _name )[ _key == None ];
		self.__keys					= ( _keys, _names )[ _keys == None ];
		self.__prefix_get			= _getter_prefix;
		self.__setup				= _setup;

		##
		self.__display_accessors	= _display_created_accessors;

		## These values will never change for this property - they are static...
		self.__default				= _default;

		## Setup the allowed types and values table...
		self.__base_allowed_types	= _allowed_types;
		self.__allowed_types		= Table.SetupQuickLookupTable( _allowed_types );
		self.__base_allowed_values	= _allowed_values;
		self.__allowed_values		= Table.SetupQuickLookupTable( _allowed_values );


		##
		## self.SetKeyAliases( _keys );
		## self.SetNameAliases( _names );

		##
		_allowed_types_desc, _allowed_values_desc = self.GetAllowedDesc( );
		self.__documentation		= self.__documentation.format( key = self.Key( ), documentation = _documentation, default_value = _default, allowed_types = _allowed_types_desc, allowed_values = _allowed_values_desc );

		## Setup the property using this class functions for the Getter, Setter, and Deleter functions
		## _property = property( lambda _parent: self.Get( _parent ), lambda _parent, _value: self.Set( _parent, _value ), lambda _parent: self.Del( _parent ), self.__documentation )
		## _property = property( lambda  this: self.Get( this ), lambda this, _value: self.Set( this, _value ), lambda this: self.Del( ), self.__documentation )
		## _property = property( lambda self: self.Get( _key ), lambda self, _value: self.Set( _key, _value ), lambda self: self.Del( _key ), self.__documentation )

		## Setup the property using this class functions for the Getter, Setter, and Deleter functions
		_property					= property( self.Get, self.Set, self.Del, self.__documentation );

		## We have to re-set the _parent.__<key> for the parent class for some reason... We do this so we can call self.__x.Get( ), etc... Without it, you see this: "AttributeError: 'MyClass' object has no attribute '__x'"
		setattr( _parent, self.AccessorKey( ), self );

		## We are storing per class unique value assigned in _parent._<key>
		setattr( _parent, self.DataKey( ), None );

		## We are setting _parent.<key> as the property so the Getter / Setter functions will work for self.<key>
		setattr( _parent, self.Key( ), _property );


		## For each key alias, we simply set up the self.<key_alias> = property... - the data will only ever be stored in the default key location, not at all otherwise everything would need a loop to process all keys and I'd prefer not to do that... the aliases let you easily set up extra ways to get the data.. For instance, an Angle class uses p / y / r for pitch / yaw / roll... So if I set up p / y / r as the defaults and allow pitch / yaw / roll, when I use self.p = 123 or self.pitch = 123, they both store at self._p ... and if I retrieve data, they both return from self._p...
		if ( _keys != None and IsTable( _keys ) ):
			## Set up a raw property to allow 'cloning' so that the default will behave normally, but for added aliases, this can make _<key_alias> behave the same as _<key> where the raw value is directly controlled... I'd have control of it all, so this just needs to point to _<key> - then I can also make one for __<key_alias>, maybe.. - Note: Set and SetRaw would be identical - they're both meant to set a value to the raw data storage area with proper callback support leaving SetRaw unnecessary... - Note: DelRaw may have merit if we decide to populate Raw to remove the Accessor completely and DelRaw to simply nullify the stored raw value to None..
			## _property_raw				= property( self.GetRaw, self.SetRaw, self.DelRaw, self.__documentation );
			_property_raw				= property( self.GetRaw, self.Set, self.DelRaw, self.__documentation );

			## For each alias, set up the raw / alias property for _<alias> so it is redirected to _<key>, also set up the standard property for the <alias> - Determine how to set up __<alias> without having the private system kick in...
			for _k in _keys:
				## We are setting _parent.<key> as the property so the Getter / Setter functions will work for self.<key>
				## setattr( _parent, '__' + _k, _property );
				## setattr( _parent, '__' + _k, lambda: getattr( _parent, '__' + _k ) );
				## We are setting _parent.<key> as the property so the Getter / Setter functions will work for self.<key>
				## setattr( _parent, '_' + _k, lambda: getattr( _parent, '_' + _k ) );
				## setattr( _parent, '_' + _k, getattr( _parent, self.GetDataKey( ) ) );

				## We are setting the _<key_alias> to a raw property which redirects actions to _<key> while keeping natural integrity..
				setattr( _parent, self.AccessorKey( _k ), _property_raw );

				## We are setting the _<key_alias> to a raw property which redirects actions to _<key> while keeping natural integrity..
				setattr( _parent, self.DataKey( _k ), _property_raw );

				## We are setting _parent.<key> as the property so the Getter / Setter functions will work for self.<key>
				setattr( _parent, self.Key( _k ), _property );

				##
				## print( 'AccessorFuncBase: Creating Extra Key References... ' + _k + '\t\tNote: This does not mean data is stored in more than 1 location... Data will only be stored in the default data key location or "self._<key>", but this system allows you a new way to access that data and assign it.. IE for Angle class, pitch / yaw / roll.. If I set the default key as p / y / and r and add aliases for pitch / yaw / roll then using self.p = 123 is the same as self.pitch = 123 - the data is stored at self._p... The data can also be retrieved with self._pitch via the Get Raw Proeprty which redirects the raw data alias to the raw data key... I may add _pitch as another property reference, but as it would not work the same as other _<key>s, it seems counter intuitive to implement....' );


		##
		if ( not getattr( _parent, 'accessor_func_db', None ) ):
			##
			## I'm considering... Maybe I should make it so everything redirects to a single table? Instead of having to manage multiple tables... So keys / alias keys link to acccessors.. Names, call names, etc... not sure.. maybe.. And the Accessors table should have all of the data or?
			##
			setattr( _parent, 'accessor_func_db', {
				## Note: Groups don't need to store everything... Store by group name... When I add something, I add by name, key, or whatever... That adds a single reference instead of by name / key whatever so they're all the same...
				## Note: Figure out if I want to create alias functions or properties such as self.All = 1234.56; to assign all in All to 1234.56...
				'groups': {
					## Default group - this is for EVERYTHING...
					'*': { }

					## 4 28 44 85
					## 'All': {
					##		'Pitch': True,
					##		'Yaw': True,
					##		'Roll': True,
					## },
				},

				## a list of keys which also link to the accessor func, property, names?
				'keys': {
					## 'roll': { },
					## 'r': { },
				},

				## a list of names which also link to the accessor func, property, keys? These should be pre-names and aliases..
				'names': {
					## 'Roll': { },
					## 'R': { },
				},

				## a list of names which also link to the accessor func, property, keys? These should be the complete function name without args... args can be added so they can be printed... but... yeah..
				'call_names': {
					## 'GetRollAccessor': '',
					## 'GetRollAllowedTypes': '',
					## 'GetRoll': ''
				},

				## Accessors list is a simple list of all accessor objects
				## 'accessors': [

				## ]

				##
				## '': [],
				## '': [],
				## '': []
			}
		);

		## Setup all of the Accessor Functions...
		self.SetupAccessorFuncs( _parent );

		## If this is the first AccessorFunc being added, we need to add hidden / internal data / keys for certain bits of data.... Along with Helper functions... ResetKeys( .. )
		self.SetupInstanceHelpers( _parent );



		##
		## print( str( self ) )

		## Verify the default value if Allowed Types or Values is set...
		if ( not _allow_erroneous_default and _default != None and not callable( _default ) and ( _allowed_types != None or _allowed_values != None ) ):
			##
			if ( not self.IsValidData( _default ) ):
				##
				self.__default = None;

				##
				raise AccessorFuncException( _key, '***** Default Value does not meet Allowed Data-Types and / or Allowed Value Specifications and the Override / Ignore Option was not activated... *****' );



	##
	##
	##
	def GetAccessorDB( self, _parent ):
		return getattr( _parent, '_accessorfunc_database', None );

	##
	##
	## Task: Setup words and enumerators for each of these special words - also determine ways to implement these into the properties to simplify everything...
	##
	'''
		__init__
		__name__

		__len__
		__str__

		__getattr__
		__setattr__
		__delattr__
		__getattribute__


		__eq__
		__ne__
		__lt__
		__gt__
		__le__
		__ge__


		__abs__


		__add__
		__sub__
		__mul__
		__floordiv__
		__div__
		__truediv__
		__mod__
		__pow__
		__divmod__

		__iadd__
		__isub__
		__imul__
		__ifloordiv__
		__idiv__
		__itruediv__
		__imod__
		__ipow__

		__radd__
		__rsub__
		__rmul__
		__rfloordiv__
		__rdiv__
		__rtruediv__
		__rmod__
		__rdivmod__
		__rpow__

		__lshift__
		__rshift__
		__and__
		__or__
		__xor__

		__ilshift__
		__irshift__
		__iand__
		__ior__
		__ixor__

		__rlshift__
		__rrshift__
		__rand__
		__ror__
		__rxor__

	'''


	'''
		__enter__( self )															Defines what the context manager should do at the beginning of the block created by the with statement. Note that the return value of __enter__ is bound to the target of the with statement, or the name after the as.
		__exit__( self, _exception_type, _exception_value, _traceback )				Defines what the context manager should do after its block has been executed (or terminates). It can be used to handle exceptions, perform cleanup, or do something always done immediately after the action in the block. If the block executes successfully, exception_type, exception_value, and traceback will be None. Otherwise, you can choose to handle the exception or let the user handle it; if you want to handle it, make sure __exit__ returns True after all is said and done. If you don't want the exception to be handled by the context manager, just let it happen.
		__call__( self, *_varargs )													Allows an instance of a class to be called as a function. Essentially, this means that x() is the same as x.__call__(). Note that __call__ takes a variable number of arguments; this means that you define __call__ as you would any other function, taking however many arguments you'd like it to.
		__instancecheck__( self, _instance )										Checks if an instance is an instance of the class you defined (e.g. isinstance(instance, class).
		__subclasscheck__( self, _subclass )										Checks if a class subclasses the class you defined (e.g. issubclass(subclass, class)).
		__getitem__(self, key)														Defines behavior for when an item is accessed, using the notation self[key]. This is also part of both the mutable and immutable container protocols. It should also raise appropriate exceptions: TypeError if the type of the key is wrong and KeyError if there is no corresponding value for the key.
		__setitem__(self, key, value)												Defines behavior for when an item is assigned to, using the notation self[nkey] = value. This is part of the mutable container protocol. Again, you should raise KeyError and TypeError where appropriate.
		__delitem__(self, key)														Defines behavior for when an item is deleted (e.g. del self[key]). This is only part of the mutable container protocol. You must raise the appropriate exceptions when an invalid key is used.
		__iter__(self)																Should return an iterator for the container. Iterators are returned in a number of contexts, most notably by the iter() built in function and when a container is looped over using the form for x in container:. Iterators are their own objects, and they also must define an __iter__ method that returns self.
		__reversed__(self)															Called to implement behavior for the reversed() built in function. Should return a reversed version of the sequence. Implement this only if the sequence class is ordered, like list or tuple.
		__contains__(self, item)													__contains__ defines behavior for membership tests using in and not in. Why isn't this part of a sequence protocol, you ask? Because when __contains__ isn't defined, Python just iterates over the sequence and returns True if it comes across the item it's looking for.
		__missing__(self, key)														__missing__ is used in subclasses of dict. It defines behavior for whenever a key is accessed that does not exist in a dictionary (so, for instance, if I had a dictionary d and said d["george"] when "george" is not a key in the dict, d.__missing__("george") would be called).

		__repr__(self)																Defines behavior for when repr() is called on an instance of your class. The major difference between str() and repr() is intended audience. repr() is intended to produce output that is mostly machine-readable (in many cases, it could be valid Python code even), whereas str() is intended to be human-readable.
		__unicode__(self)															Defines behavior for when unicode() is called on an instance of your class. unicode() is like str(), but it returns a unicode string. Be wary: if a client calls str() on an instance of your class and you've only defined __unicode__(), it won't work. You should always try to define __str__() as well in case someone doesn't have the luxury of using unicode.
		__format__(self, formatstr)													Defines behavior for when an instance of your class is used in new-style string formatting. For instance, "Hello, {0:abc}!".format(a) would lead to the call a.__format__("abc"). This can be useful for defining your own numerical or string types that you might like to give special formatting options.
		__hash__(self)																Defines behavior for when hash() is called on an instance of your class. It has to return an integer, and its result is used for quick key comparison in dictionaries. Note that this usually entails implementing __eq__ as well. Live by the following rule: a == b implies hash(a) == hash(b).
		__nonzero__(self)															Defines behavior for when bool() is called on an instance of your class. Should return True or False, depending on whether you would want to consider the instance to be True or False.
		__dir__(self)																Defines behavior for when dir() is called on an instance of your class. This method should return a list of attributes for the user. Typically, implementing __dir__ is unnecessary, but it can be vitally important for interactive use of your classes if you redefine __getattr__ or __getattribute__ (which you will see in the next section) or are otherwise dynamically generating attributes.
		__sizeof__(self)															Defines behavior for when sys.getsizeof() is called on an instance of your class. This should return the size of your object, in bytes. This is generally more useful for Python classes implemented in C extensions, but it helps to be aware of it.
		__int__(self)																Implements type conversion to int.
		__long__(self)																Implements type conversion to long.
		__float__(self)																Implements type conversion to float.
		__complex__(self)															Implements type conversion to complex.
		__oct__(self)																Implements type conversion to octal.
		__hex__(self)																Implements type conversion to hexadecimal.
		__index__(self)																Implements type conversion to an int when the object is used in a slice expression. If you define a custom numeric type that might be used in slicing, you should define __index__.
		__trunc__(self)																Called when math.trunc(self) is called. __trunc__ should return the value of `self truncated to an integral type (usually a long).
		__coerce__(self, other)														Method to implement mixed mode arithmetic. __coerce__ should return None if type conversion is impossible. Otherwise, it should return a pair (2-tuple) of self and other, manipulated to have the same type.


		__pos__(self)																Implements behavior for unary positive (e.g. +some_object)
		__neg__(self)																Implements behavior for negation (e.g. -some_object)
		__invert__(self)															Implements behavior for inversion using the ~ operator. For an explanation on what this does, see the Wikipedia article on bitwise operations.
		__round__(self, n)															Implements behavior for the built in round() function. n is the number of decimal places to round to.
		__floor__(self)																Implements behavior for math.floor(), i.e., rounding down to the nearest integer.
		__ceil__(self)																Implements behavior for math.ceil(), i.e., rounding up to the nearest integer.
		__trunc__(self)																Implements behavior for math.trunc(), i.e., truncating to an integral.

		__cmp__(self, other)														__cmp__ is the most basic of the comparison magic methods. It actually implements behavior for all of the comparison operators (<, ==, !=, etc.), but it might not do it the way you want (for example, if whether one instance was equal to another were determined by one criterion and and whether an instance is greater than another were determined by something else). __cmp__ should return a negative integer if self < other, zero if self == other, and positive if self > other. It's usually best to define each comparison you need rather than define them all at once, but __cmp__ can be a good way to save repetition and improve clarity when you need all comparisons implemented with similar criteria.

		__new__(cls, [...)															__new__ is the first method to get called in an object's instantiation. It takes the class, then any other arguments that it will pass along to __init__. __new__ is used fairly rarely, but it does have its purposes, particularly when subclassing an immutable type like a tuple or a string. I don't want to go in to too much detail on __new__ because it's not too useful, but it is covered in great detail in the Python docs.
		__del__(self)																If __new__ and __init__ formed the constructor of the object, __del__ is the destructor. It doesn't implement behavior for the statement del x (so that code would not translate to x.__del__()). Rather, it defines behavior for when an object is garbage collected. It can be quite useful for objects that might require extra cleanup upon deletion, like sockets or file objects. Be careful, however, as there is no guarantee that __del__ will be executed if the object is still alive when the interpreter exits, so __del__ can't serve as a replacement for good coding practices (like always closing a connection when you're done with it. In fact, __del__ should almost never be used because of the precarious circumstances under which it is called; use it with caution!
	'''


	'''
		__init__(self, [...)														The initializer for the class. It gets passed whatever the primary constructor was called with (so, for example, if we called x = SomeClass(10, 'foo'), __init__ would get passed 10 and 'foo' as arguments. __init__ is almost universally used in Python class definitions.

		__len__(self)																Returns the length of the container. Part of the protocol for both immutable and mutable containers.
		__str__(self)																Defines behavior for when str() is called on an instance of your class.

		__getattr__(self, name)														You can define behavior for when a user attempts to access an attribute that doesn't exist (either at all or yet). This can be useful for catching and redirecting common misspellings, giving warnings about using deprecated attributes (you can still choose to compute and return that attribute, if you wish), or deftly handing an AttributeError. It only gets called when a nonexistent attribute is accessed, however, so it isn't a true encapsulation solution.
		__setattr__(self, name, value)												Unlike __getattr__, __setattr__ is an encapsulation solution. It allows you to define behavior for assignment to an attribute regardless of whether or not that attribute exists, meaning you can define custom rules for any changes in the values of attributes. However, you have to be careful with how you use __setattr__, as the example at the end of the list will show.
		__delattr__(self, name)														This is the exact same as __setattr__, but for deleting attributes instead of setting them. The same precautions need to be taken as with __setattr__ as well in order to prevent infinite recursion (calling del self.name in the implementation of __delattr__ would cause infinite recursion).
		__getattribute__(self, name)												After all this, __getattribute__ fits in pretty well with its companions __setattr__ and __delattr__. However, I don't recommend you use it. __getattribute__ can only be used with new-style classes (all classes are new-style in the newest versions of Python, and in older versions you can make a class new-style by subclassing object. It allows you to define rules for whenever an attribute's value is accessed. It suffers from some similar infinite recursion problems as its partners-in-crime (this time you call the base class's __getattribute__ method to prevent this). It also mainly obviates the need for __getattr__, which, when __getattribute__ is implemented, only gets called if it is called explicitly or an AttributeError is raised. This method can be used (after all, it's your choice), but I don't recommend it because it has a small use case (it's far more rare that we need special behavior to retrieve a value than to assign to it) and because it can be really difficult to implement bug-free.


		__eq__(self, other)															Defines behavior for the equality operator, ==.
		__ne__(self, other)															Defines behavior for the inequality operator, !=.
		__lt__(self, other)															Defines behavior for the less-than operator, <.
		__gt__(self, other)															Defines behavior for the greater-than operator, >.
		__le__(self, other)															Defines behavior for the less-than-or-equal-to operator, <=.
		__ge__(self, other)															Defines behavior for the greater-than-or-equal-to operator, >=.

		__abs__(self)																Implements behavior for the built in abs() function.

		__add__(self, other)														Implements addition.
		__sub__(self, other)														Implements subtraction.
		__mul__(self, other)														Implements multiplication.
		__floordiv__(self, other)													Implements integer division using the // operator.
		__div__(self, other)														Implements division using the / operator.
		__truediv__(self, other)													Implements true division. Note that this only works when from __future__ import division is in effect.
		__mod__(self, other)														Implements modulo using the % operator.
		__divmod__(self, other)														Implements behavior for long division using the divmod() built in function.
		__pow__																		Implements behavior for exponents using the ** operator.

		__iadd__(self, other)														Implements addition with assignment.
		__isub__(self, other)														Implements subtraction with assignment.
		__imul__(self, other)														Implements multiplication with assignment.
		__ifloordiv__(self, other)													Implements integer division with assignment using the //= operator.
		__idiv__(self, other)														Implements division with assignment using the /= operator.
		__itruediv__(self, other)													Implements true division with assignment. Note that this only works when from __future__ import division is in effect.
		__imod__(self, other)														Implements modulo with assignment using the %= operator.
		__ipow__																	Implements behavior for exponents with assignment using the **= operator.

		__radd__(self, other)														Implements reflected addition.
		__rsub__(self, other)														Implements reflected subtraction.
		__rmul__(self, other)														Implements reflected multiplication.
		__rfloordiv__(self, other)													Implements reflected integer division using the // operator.
		__rdiv__(self, other)														Implements reflected division using the / operator.
		__rtruediv__(self, other)													Implements reflected true division. Note that this only works when from __future__ import division is in effect.
		__rmod__(self, other)														Implements reflected modulo using the % operator.
		__rdivmod__(self, other)													Implements behavior for long division using the divmod() built in function, when divmod(other, self) is called.
		__rpow__																	Implements behavior for reflected exponents using the ** operator.

		__lshift__(self, other)														Implements left bitwise shift using the << operator.
		__rshift__(self, other)														Implements right bitwise shift using the >> operator.
		__and__(self, other)														Implements bitwise and using the & operator.
		__or__(self, other)															Implements bitwise or using the | operator.
		__xor__(self, other )														Implements bitwise xor using the ^ operator.

		__ilshift__(self, other)													Implements left bitwise shift with assignment using the <<= operator.
		__irshift__(self, other)													Implements right bitwise shift with assignment using the >>= operator.
		__iand__(self, other)														Implements bitwise and with assignment using the &= operator.
		__ior__(self, other)														Implements bitwise or with assignment using the |= operator.
		__ixor__(self, other)														Implements bitwise xor with assignment using the ^= operator.

		__rlshift__(self, other)													Implements reflected left bitwise shift using the << operator.
		__rrshift__(self, other)													Implements reflected right bitwise shift using the >> operator.
		__rand__(self, other)														Implements reflected bitwise and using the & operator.
		__ror__(self, other)														Implements reflected bitwise or using the | operator.
		__rxor__(self, other)														Implements reflected bitwise xor using the ^ operator.
	'''


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';

		_text					= '';

		##


		## _text += Acecool.Header( 'AccessorFuncBase.(  ) - ...', '', '', _depth_header );
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'Testing',		_col_res, String.repeat( _example_text ) ) + '\n';
		## ## _text += _depth_prefix + String.ToStringKeyedVarArgs( _col_key, 'TestingTesting',	_col_res, String.repeat( _example_text, 2 ) ) + '\n';
		## _text += Acecool.Footer( ) + '\n';


		_text += Acecool.Header( 'AccessorFuncBase.(  ) - ...', '', '', _depth_header );
		_text += _depth_prefix + '\n';
		_text += _depth_prefix + '\n';
		_text += Acecool.Footer( ) + '\n';


		## _text += Acecool.Header( 'AccessorFuncBase.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += Acecool.Footer( ) + '\n';

		return _text;



	##
	## This creates, if not exists, helper functions for the class using AccessorFunc systems such as ResetAccessorValues, etc...
	##
	def SetupInstanceHelpers( self, _parent = None ):
		## If these already exist, then don't set them up again...
		if ( _parent == None ):
			return False;


		##
		if ( getattr( _parent, 'accessorfunc_setup', None ) ):
			##
			_key			= self.Key( );
			_name			= self.Name( );

			##
			## print( '[ AccessorFuncBase ] Added Name DB:		' + _name );
			## print( '[ AccessorFuncBase ] Added Key DB:		' + _key );


			return True;
		else:
			setattr( _parent, 'accessorfunc_setup', True );

		##
		if ( self.ShowCreatedAccessors( ) ):
			## print( '[ AccessorFuncBase ]\t##' );
			## print( '[ AccessorFuncBase ]\t##' );
			print( '[ AccessorFuncBase ] Please note: several helper functions have been added to the parent class, ' + str( _parent ) + ', for the first-time setup of AccessorFunc Support for this class...' );
			print( '[ AccessorFuncBase ]\t##' );
			print( '[ AccessorFuncBase ]\t## These Helpers are meant to simplify common actions and improve efficiency of writing code...' );
			print( '[ AccessorFuncBase ]\t##' );
			print( '[ AccessorFuncBase ]\t## Note: Default restrictions mean Width and Depth will not be set as they only allow 0 to 9 values..' );
			print( '[ AccessorFuncBase ]\t## Note: Resetting a key / name sets the stored value to None - The getter will still return the default value unless overwritten or default returns are disabled...' );
			print( '[ AccessorFuncBase ]\t##' );
			print( );
			print( );
			print( '[ AccessorFuncBase ]\t##' );
			print( '[ AccessorFuncBase ]\t## This will let you Set 1 to many Accessor Name Values at a time - Note: This is identical to calling _class.SetName( _value ) or _class.Name = _value -- Data-Type / Value Setting Restrictions will still function...' );
			print( '[ AccessorFuncBase ]\t##' );
			print( '\t\tHelper:\t\t\t_class.SetAccessors( _name, _value [ , ... ] )' );
			print( );
			print( "\t\tUsage:\t\t\t_foo.SetAccessors( 'Name', 'Foo Fighters', 'X', 100, 'Y', 200, 'Z', 300, 'Blah', 'Blahing all the way', 'Height', 400, 'Width', 500, 'Depth', 600 )" );
			print( "\t\tIdentical To:\n\t\t\t\t\t\t_foo.Name = 'Foo Fighters'\n\t\t\t\t\t\t_foo.x = 100\n\t\t\t\t\t\t_foo.y = 200\n\t\t\t\t\t\t_foo.SetZ( 300 )\n\t\t\t\t\t\t_foo.SetBlah( 'Blahing all the way' )\n\t\t\t\t\t\t_foo.Height = 400\n\t\t\t\t\t\t_foo.SetWidth( 500 )\n\t\t\t\t\t\t_foo.SetDepth( 600 )" );
			print( );
			print( );
			print( '[ AccessorFuncBase ]\t##' );
			print( '[ AccessorFuncBase ]\t## This will let you Reset 1 to many Accessor Names to default value ( None ) - Note: Even if you reset the stored value to None, there is a Default Value system which will still function unless you explicitly disable it on the Getter call...' );
			print( '[ AccessorFuncBase ]\t##' );
			print( '\t\tHelper:\t\t\t_class.ResetAccessors( _name [ , ... ] )' );
			print( );
			print( "\t\tUsage:\t\t\t_foo.ResetAccessors( 'Name', 'X', 'Y', 'Z', 'Blah', 'Height', 'Width', 'Depth' )" );
			print( "\t\tIdentical To:\n\t\t\t\t\t\t_foo.Name = None\n\t\t\t\t\t\t_foo.x = None\n\t\t\t\t\t\t_foo.y = None\n\t\t\t\t\t\t_foo.SetZ( None )\n\t\t\t\t\t\t_foo.ResetBlah( )\n\t\t\t\t\t\t_foo.Height = None\n\t\t\t\t\t\t_foo.SetWidth( None )\n\t\t\t\t\t\t_foo.ResetDepth( )" );
			print( );
			print( );
			print( );
			print( '##' );
			print( '## Note: This is the first one added... A complete list of the functions created is shown below for the first - for the rest, a list of names / keys is provided instead of outputting the same information over and over..' );
			print( '##' );
			print( '[ AccessorFuncBase ] Added Name DB:		' + self.Name( ) );
			print( '[ AccessorFuncBase ] Added Key DB:		' + self.Key( ) );
			print( );
			## print( getattr( _parent, self.AccessorNameGetHelpers( ) )( _parent ) );
			print( self.AccessorCallGetHelpers( _parent, True ) );
			print( );


		## ##
		## ## Accessor Func DB...
		## ##
		## ## _accessor_func_db = {
		## ## 	'names': { },
		## ## 	'keys': { },

		## ## };
		## ## setattr( _parent, '_accessor_func_db', _accessor_func_db );
		## ##
		## if ( not getattr( _parent, 'x_accessor_func_db', None ) ):
		## 	##
		## 	## I'm considering... Maybe I should make it so everything redirects to a single table? Instead of having to manage multiple tables... So keys / alias keys link to acccessors.. Names, call names, etc... not sure.. maybe.. And the Accessors table should have all of the data or?
		## 	##
		## 	setattr( _parent, 'x_accessor_func_db', {
		## 		## Note: Groups don't need to store everything... Store by group name... When I add something, I add by name, key, or whatever... That adds a single reference instead of by name / key whatever so they're all the same...
		## 		## Note: Figure out if I want to create alias functions or properties such as self.All = 1234.56; to assign all in All to 1234.56...
		## 		'groups': {
		## 			## Default group - this is for EVERYTHING...
		## 			'*': [ ]

		## 			## 4 28 44 85
		## 			## 'All': {
		## 			##		'Pitch': True,
		## 			##		'Yaw': True,
		## 			##		'Roll': True,
		## 			## },
		## 		},

		## 		## a list of keys which also link to the accessor func, property, names?
		## 		'keys': {
		## 			## 'roll': { },
		## 			## 'r': { },
		## 		},

		## 		## a list of names which also link to the accessor func, property, keys? These should be pre-names and aliases..
		## 		'names': {
		## 			## 'Roll': { },
		## 			## 'R': { },
		## 		},

		## 		## a list of names which also link to the accessor func, property, keys? These should be the complete function name without args... args can be added so they can be printed... but... yeah..
		## 		'call_names': {
		## 			## 'GetRollAccessor': '',
		## 			## 'GetRollAllowedTypes': '',
		## 			## 'GetRoll': ''
		## 		},

		## 		## ## Accessors list is a simple list of all accessor objects
		## 		## 'accessors': [

		## 		## ]

		## 		##
		## 		## '': [],
		## 		## '': [],
		## 		## '': []
		## 	}
		## );


		## ## function this.ResetAccessors( [ _name, ]+ ) - Resets the value for each key / name submitted...
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameReset( ), lambda this, _name, *_names: self.ResetAccessorFuncValues( this, True, _name, *_names ) )

		## ## function this.SetAccessors( [ _name, _value, ]+ ) - Sets the value for each Accessor name supplied... SetAccessorFuncValues( self, this = None, _by_name = True, *_args )
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameSet( ), lambda this, _name, _value, *_args: self.SetAccessorFuncValues( this, True, _name, _value, *_args ) )

		## ## function this.GetAccessors( [ _name, ]+ ) - Returns the value for each Accessor name supplied... Can be returned into a single var as a list, or the list can be split simply by using _value, _value2, _value3 = this.GetAccessors( _name, _name2, _name3 )
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameGet( ), lambda this, _name, *_names: self.GetAccessorFuncValues( this, True, _name, *_names ) )




		## ## function this.ResetAccessorsByKey( [ _key ]+ ) - Resets the value for each Accessor key submitted...
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameResetByKey( ), lambda this, _key, *_keys: self.ResetAccessorFuncValues( this, False, _key, *_keys ) )

		## ## function this.SetAccessorsByKey( [ _key, _value, ]+ ) - Assigns a value to each Accessor for each key / value pair provided.
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameSetByKey( ), lambda this, _key, _value, *_args: self.SetAccessorFuncValues( this, False, _key, _value, *_args ) )

		## ## function this.GetAccessorsByKey( [ _key ]+ ) - Resets ( Assigns None Value ) to each Accessor bound to each key provided.
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameGetByKey( ), lambda this, _key, *_keys: self.GetAccessorFuncValues( this, False, _key, *_keys ) )




		## ## function this.GetAccessorsDB( this ) - Returns the Accessor DB for the instance object - a list of all accessors, references, by name / key, and more information...
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameGetDB( ), lambda this: getattr( this, 'accessor_func_db', [ ] ) )

		## ## function this.AddAccessorsDB( this ) - Adds a new entry to the database
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameAddDB( ), lambda this: None )

		## ## function this.ResetAccessorsDB( this ) - Resets the entire Accessor Func Database - This shouldn't be used unless all elements have been removed - a lot of work...
		## self.CreateInstanceAccessor( _parent, self.AccessorsNameResetDB( ), lambda this: None )






		##
		## Setup the Parent Class __str__ function, if one doesn't already exist - output all of the accessors in the db, and more...
		##
		if ( getattr( _parent, 'AccessorOverrideFunc__str__', False ) ): #not callable( getattr( _parent, '__str__' ) ) ):
			##
			## Parent __str__
			##
			def InstanceToString( this ):
				return 'Testing Override ToString Function...';

			##
			## Register Special SetupInstanceHelpers Accessors for the Instance Class Calls...
			##
			##

			##
			if ( Acecool.GetSetting( 'output_accessorfunc_register_info', False ) ):
				##
				print( '\nRegisterAccessor -> RegisterNamedAccessor id: ' + Text.FormatColumn( 30, '__str__', 10, ' -> ', 20, self.Name( ), 10, ' -> ', 6, 'class ', 20, self.ClassName( ) + '( ... ):' ) + ' -> Getter Prefix: ' + self.GetterPrefix( ) );

			## function self.GetAccessorsDB( )																, '__str__'
			self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE__STR__, InstanceToString );


		## MAP_ACCESSORFUNC_IDS
		## MAP_ACCESSORFUNC_ID_FUNC_NAMES
		## MAP_ACCESSORFUNC_ID_FUNC_ARGS
		## MAP_ACCESSORFUNC_ID_FUNC_CALL_NAMES


		## ACCESSORFUNC_ID_INSTANCE,
		## ACCESSORFUNC_ID_INSTANCE_RESET_BY_NAME,
		## ACCESSORFUNC_ID_INSTANCE_SET_BY_NAME,
		## ACCESSORFUNC_ID_INSTANCE_GET_BY_NAME,
		## ACCESSORFUNC_ID_INSTANCE_RESET_BY_KEY,
		## ACCESSORFUNC_ID_INSTANCE_SET_BY_KEY,
		## ACCESSORFUNC_ID_INSTANCE_GET_BY_KEY,
		## ACCESSORFUNC_ID_INSTANCE_RESET_GROUPED,
		## ACCESSORFUNC_ID_INSTANCE_SET_GROUPED,
		## ACCESSORFUNC_ID_INSTANCE_GET_GROUPED,
		## ACCESSORFUNC_ID_INSTANCE_ADD_GROUPED,
		## ACCESSORFUNC_ID_INSTANCE_RESET_DATABASE,
		## ACCESSORFUNC_ID_INSTANCE_GET_DATABASE,
		## ACCESSORFUNC_ID_INSTANCE_ADD_DATABASE

		##
		## Helpers used to Update the values, or retrieve the values, by Accessor Group ID
		##





		##
		## Helpers used to perform actions on Accessor References ( used for the group functions which assign data, etc.. which use the Get/Add/Set/ResetGroupAccessors functions to retrieve accessor references, set, add, reset.. ) such as updating an entire group of Accessor Values to a single value, etc..
		##





		##
		## Helpers used to Update the values, or retrieve the values, by Accessor name, of Accessor Funcs
		##


		##
		## function this.ResetAccessors( [ _name, ]+ ) - Resets the value for each key / name submitted...
		##
		def ResetAccessors( this, _name, *_names ):
			return self.ResetAccessorFuncValues( this, True, _name, *_names );


		##
		## function this.SetAccessors( [ _name, _value, ]+ ) - Sets the value for each Accessor name supplied...
		##
		def SetAccessors( this, _name, _value, *_args ):
			return self.SetAccessorFuncValues( this, True, _name, _value, *_args );


		##
		## function this.GetAccessors( [ _name, ]+ ) - Returns the value for each Accessor name supplied... Can be returned into a single var as a list, or the list can be split simply by using _value, _value2, _value3 = this.GetAccessors( _name, _name2, _name3 )
		##
		def GetAccessors( this, _name, *_names ):
			return self.GetAccessorFuncValues( this, True, _name, *_names );


		##
		## Helpers used to Update the values, or retrieve the values, by Accessor key, of Accessor Funcs
		##


		##
		## function this.ResetAccessorsByKey( [ _key ]+ ) - Resets the value for each Accessor key submitted...
		##
		def ResetAccessorsByKey( this, _key, *_keys ):
			return self.ResetAccessorFuncValues( this, False, _key, *_keys );


		##
		## function this.SetAccessorsByKey( [ _key, _value, ]+ ) - Assigns a value to each Accessor for each key / value pair provided.
		##
		def SetAccessorsByKey( this, _key, _value, *_args ):
			return self.SetAccessorFuncValues( this, False, _key, _value, *_args );


		##
		## function this.GetAccessorsByKey( [ _key ]+ ) - Resets ( Assigns None Value ) to each Accessor bound to each key provided.
		##
		def GetAccessorsByKey( this, _key, *_keys ):
			return self.GetAccessorFuncValues( this, False, _key, *_keys );

		##
		## Helpers used to manage Accessor Groupings which can contain 1 to many ( 0 will not exist ) Accessor References by Name and / or Key - Reset Accessors in group, Assign Accessors to Group, Add Accessors To Group without resetting the group as Set does, and to Retrieve / Get the list of Accessor References in a group...
		##


		##
		## function this.ResetGroupAccessors( [ _group ]+ ) - Resets the Group Accessor List, doesn't change the value of each Accessor ( For that, use the ByGroup designated function calls )
		##
		def ResetGroupAccessors( this, _group, *_groups ):
			return None; #self.ResetAccessorFuncValues( this, False, _key, *_groups )


		##
		## function this.SetGroupAccessors( _group, [ _name, ]+ ) - Resets the Group Accessor List to only include the Accessors provided.
		##
		def SetGroupAccessors( this, _group, _name, *_names ):
			return None; #self.SetAccessorFuncValues( this, False, _key, _name, *_names )


		##
		## function this.AddGroupAccessors( _group, [ _name, ]+ ) - Adds the Accessors provided to the Group Accessor List without Resetting it..
		##
		def AddGroupAccessors( this, _group, _name, *_names ):
			return None; #self.SetAccessorFuncValues( this, False, _group, _name, *_names )


		##
		## function this.GetGroupAccessors( [ _group, ]+ ) - Returns a list of Accessors assigned to the group provided..
		##
		def GetGroupAccessors( this, _group, *_groups ):
			return None; #getattr( this.accessor_func_db.groups, _group, [ ] )

		##
		## Helpers pertaining to the instance object AcccessorFunc DB ( Note: * - by default - value for group name refers to ALL elements which is used to print out functions created, etc.. when that option is enabled, or in an __str__ function in the instance class )
		##


		##
		## function this.GetAccessorsDB( this ) - Returns the Accessor DB for the instance object - a list of all accessors, references, by name / key, and more information...
		##
		def GetAccessorsDB( this ):
			return getattr( this, 'accessor_func_db', { } );


		##
		##
		##
		def GetAccessorsGroupsDB( this ):
			return this.GetAccessorsDB( ).get( 'groups', None );


		##
		##
		##
		def GetAccessorsKeysDB( this ):
			return this.GetAccessorsDB( ).get( 'keys', None );


		##
		##
		##
		def GetAccessorsNamesDB( this ):
			return this.GetAccessorsDB( ).get( 'names', None );


		##
		##
		##
		def GetAccessorsFuncNamesDB( this ):
			return this.GetAccessorsDB( ).get( 'call_names', None );


		##
		##
		##
		def GetAccessorsReferenceDB( this ):
			return this.GetAccessorsDB( ).get( 'accessors', None );


		##
		## function this.AddAccessorsDB( this ) - Adds a new entry to the database
		##
		def AddAccessorsDB( this ):
			## Grab the database
			_db = GetAccessorsDB( this );

			## Add the data ( key and name quick entries, as well as references, etc... so we can search by both - maybe even add function names to the searchable table )


			return True;


		##
		## function this.ResetAccessorsDB( this ) - Resets the entire Accessor Func Database - This shouldn't be used unless all elements have been removed - a lot of work...
		##
		def ResetAccessorsDB( this ):
			## Task: Todo: Right now, we don't need it - unless a dynamic class is made where the accessors aren't static - right now it won't be necessary but it may become necessary later which is why a blank function exists..
			return False;


		##
		## Register all Remaining SetupInstanceHelpers Accessors for the Instance Class Calls... Note: These instance functions are different because they are only added once and aren't Name specific. These are helpers added to the parent class to help perform accessor actions using less lines, etc... ie: ResetAccessors( 'ThreadJobs' ,'Thread', 'Blah' ), ResetAccessorsByKey( 'thread_jobs', 'thread', 'blah' ), ResetAccessorsByGroup( 'threading' ), etc..
		##

		##
		## Note: These functions are added to the PARENT class, the class which is receiving a property but only the first property adds these, so it has helper functions to manage Accessors easier...
		##

		##
		if ( Acecool.GetSetting( 'output_accessorfunc_register_info', False ) ):
			##
			print( '\nRegisterAccessor -> RegisterInstanceAccessors ' + Text.FormatColumn( 10, ' -> ', 6, 'class ', 20, self.ClassName( ) + '( ... ):' ) );

			##
			if ( not self.Setup( 'instance' ) ):
				print( '\tNote: This list appears empty - this means setup = { } was redefined without "instance": true, or setup_override = { "instance": false, } was defubed... This means the instance helpers are not added... If just one other AccessorFunc allows them, they are added... They are only ever added once too..' );

		## function self.ResetAccessors( [ _name, ]+ )														, self.AccessorsNameReset( _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_RESET_BY_NAME, ResetAccessors );

		## function self.SetAccessors( [ _name, _value ]+ )													, self.AccessorsNameSet( _name, _value, ...)
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_SET_BY_NAME, SetAccessors );

		## function self.GetAccessors( [ _name, ]+ )														, self.AccessorsNameGet( _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_BY_NAME, GetAccessors );

		## function self.ResetAccessorsByKey( [ _key ]+ )													, self.AccessorsNameResetByKey( _key, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_RESET_BY_KEY, ResetAccessorsByKey );

		## function self.SetAccessorsByKey( [ _key, _value, ]+ )											, self.AccessorsNameSetByKey( _key, _value, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_SET_BY_KEY, SetAccessorsByKey );

		## function self.GetAccessorsByKey( [ _key ]+ )														, self.AccessorsNameGetByKey( _key, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_BY_KEY, GetAccessorsByKey );

		## function self.ResetGroupAccessors( [ _group ]+ )													, self.AccessorsNameResetGroup( _group, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_RESET_GROUPED, ResetGroupAccessors );

		## function self.SetGroupAccessors( _group, [ _name, ]+ )											, self.AccessorsNameSetGroup( _group, _dict? )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_SET_GROUPED, SetGroupAccessors );

		## function self.GetGroupAccessors( [ _group, ]+ )													, self.AccessorsNameGetGroup( _group, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_GROUPED, GetGroupAccessors );

		## function self.AddGroupAccessors( _group, [ _name, ]+ )											, self.AccessorsNameAddGroup( _group, _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_ADD_GROUPED, AddGroupAccessors );

		## function self.ResetAccessorsDB( )																, self.AccessorsNameResetDB( _name, ... )
		## self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_RESET_DATABASE, ResetAccessorsDB );

		## function self.GetAccessorsDB( )																	, self.AccessorsNameGetDB( _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_DATABASE, GetAccessorsDB );

		## function self.GetAccessorsDB( )																	, self.AccessorsNameGetDB( _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_FUNC_NAMES_DATABASE, GetAccessorsFuncNamesDB );

		## function self.GetAccessorsDB( )																	, self.AccessorsNameGetDB( _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_GROUPS_DATABASE, GetAccessorsGroupsDB );

		## function self.GetAccessorsDB( )																	, self.AccessorsNameGetDB( _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_KEYS_DATABASE, GetAccessorsKeysDB );

		## function self.GetAccessorsDB( )																	, self.AccessorsNameGetDB( _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_NAMES_DATABASE, GetAccessorsNamesDB );

		## function self.GetAccessorsDB( )																	, self.AccessorsNameGetDB( _name, ... )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_REFERENCES_DATABASE, GetAccessorsReferenceDB );

		## function self.GetAccessorsDB( )																	, self.AccessorsNameGetDB( _name, ... )
		## self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_GET_DATABASE, GetAccessorsDB );

		## function self.AddAccessorsDB( )																	, self.AccessorsNameAddDB( _name, ... )
		## self.RegisterAccessor( _parent, ACCESSORFUNC_ID_INSTANCE_ADD_DATABASE, AddAccessorsDB );

		## function self.GetAccessorsWiki( )																, self.AccessorsNameGetWIKI( )
		## self.RegisterAccessor( _parent, 'instance_wiki', GetAccessorsWIKI );

		## function self.CreateWikiCSV( )																	, self.AccessorsNameGetWiki( )
		## self.RegisterAccessor( _parent, 'instance_wiki', GetAccessorsWiki );

		## function self.GetInstanceWiki( )																	, self.AccessorsNameGetInstanceWiki( )
		## self.RegisterAccessor( _parent, 'instance_helpers_wiki', GetAccessorsInstanceWiki );

		## print( );


	##
	## Helper to setup Accessor Functions...
	##
	def SetupAccessorFuncs( self, _parent = None ):
		##
		_key			= self.Key( );
		_key_data		= self.GetDataKey( );
		_key_accessor	= self.GetAccessorKey( );
		_name			= self.Name( );


		##
		## The following are Getters / Setters created for the class requesting them... I use this instead of self for this functions so it is clear that self represents AccessorFuncBase, this represents the class instance requesting the functions, _parent represents the non-instanced class requesting these functions.
		##


		##
		## function self.Get<Name>( _default_override = None, _ignore_defaults = False )
		##
		## Args: Override Default as _default_override - This overrides the default value in favor of an on a per-call default setting basis..
		## Args: Ignore ALL Defaults as _no_default - This overrides all default values in favor of returning None if a value isn't set...
		##
		def AccessorFuncGet( this, _default_override = None, _ignore_defaults = False ):
			## print( 'AccessorFuncGet args: ' + str( this ) + ' // ' + str( _default_override ) + ' // ' + str( _ignore_defaults ) + ' // ' + str( _x ) )
			## Debugging
			## print( '[ AccessorFunc ] ' + _name_get + ' is being called!' ) ( Logic.TernaryFunc( _default_override != None, _default_override, _default ) )

			##
			_value = getattr( this, _key );
			_isset = AccessorFuncIsSet( this );

			## If we have a value to return, return it... Or, if we don't have a value to return and we ignore defaults, we return the stored value ( None )...
			if ( ( _isset ) or ( not _isset and _ignore_defaults ) ):
				## Return the value assigned as it is set...
				return AccessorFuncGetRaw( this );

			## If there is no override value, we return the value retrieved from the getter which returns the original default value... Otherwise we return the override default value...
			if ( _default_override == None ):
				return _value;

			## Return the default override...
			return _default_override;


		##
		## function self.Get<Key>Raw( _default ) - Returns the value stored under _<key>
		##
		def AccessorFuncGetRaw( this ):
			return getattr( this, self.GetDataKey( ) );


		##
		## function self.Is<Key>Set( _default ) - Returns whether or not there is a value stored in _<key> ( By default, there isn't... )
		##
		def AccessorFuncIsSet( this ):
			return AccessorFuncGetRaw( this ) != None;



		##
		## function self.Get<Key>ToString( _default ) - Returns the data in string-format..
		##
		def AccessorFuncGetToString( this, _default_override = None, _ignore_defaults = False ):
			return str( AccessorFuncGet( this, _default_override, _ignore_defaults ) );


		##
		## function self.Get<Key>Len( _default ) - Returns the Length of characters of the data
		##
		def AccessorFuncGetLen( this, _default_override = '', _ignore_defaults = False ):
			##
			return len( AccessorFuncGet( this, _default_override, _ignore_defaults ) );
			## return len( AccessorFuncGetToString( this, _default_override, _ignore_defaults ) );


		##
		## function self.Get<Key>LenToString( _default ) - Returns the Length of characters of the data
		##
		def AccessorFuncGetLenToString( this, _default_override = '', _ignore_defaults = False ):
			##
			return str( AccessorFuncGetLen( this, _default_override, _ignore_defaults ) );


		##
		## function self.Get<Key>DefaultValue( ) -
		##
		def AccessorFuncGetDefaultValue( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameDefaultValue( ) );


		##
		## function self.Get<Key>Property( _default ) - Returns the property
		##
		def AccessorFuncGetProperty( this ):
			##
			return getattr( this, self.Key( ) );



		##
		## function self.Get<Key>Accessor( _default ) - Returns the accessor, or it calls the accessor with args but includes self without the client needing to...
		##
		## Helper used as: return GetKeyAccessor( this, 'FunctionName', *_args ) - typical use: return GetKeyAccessor( this, 'Set', this, _value )
		## Note: *_args are left without automatic this because not all functions are formulated def Set( self, _parent, _value = None ):... Some may only need self ( the first this represents that automatically via getattr ), the other args after self are represented by *_args and the function name by 'Set' or 'FunctionName'...
		##
		def AccessorFuncGetAccessor( this, _func_name = None, *_args ):
			## ## Grab the accessor reference		_accessor = getattr( this, self.GetAccessorKey( ) )
			## _accessor = getattr( this, '__' + _key )

			## ## Determine, if a function name is given, if that function is callable... If it is, we call it with supplied arguments..
			## if ( _func_name and callable( getattr( _accessor, _func_name ) ) ):
			## 	return getattr( _accessor, _func_name )( *_args )

			## ## If no function name is provided, or the function isn't callable ( read as: doesn't exist, or some other issue ) then we return the accessor reference to simplify things....
			## return _accessor

			## Determine, if a function name is given, if that function is callable... If it is, we call it with supplied arguments..
			if ( _func_name and callable( getattr( self, _func_name ) ) ):
				return getattr( self, _func_name )( *_args );

			## If no function name is provided, or the function isn't callable ( read as: doesn't exist, or some other issue ) then we return the accessor reference to simplify things....
			return self;


		##
		## function self.Get<Key>AllowedTypes( ) - Returns a Dict of allowed types in O( 1 ) format...
		##
		def AccessorFuncGetAllowedTypes( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameGetAllowedTypes( ) );


		##
		## function self.Get<Key>AllowedValues( ) - Returns a Dict of allowed values in O( 1 ) format...
		##
		def AccessorFuncGetAllowedValues( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameGetAllowedValues( ) );


		##
		## function self.Get<Key>BaseAllowedTypes( ) - Returns a Dict of allowed types in O( 1 ) format...
		##
		def AccessorFuncGetBaseAllowedTypes( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameGetBaseAllowedTypes( ) );


		##
		## function self.Get<Key>BaseAllowedValues( ) - Returns a Dict of allowed values in O( 1 ) format...
		##
		def AccessorFuncGetBaseAllowedValues( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameGetBaseAllowedValues( ) );


		##
		## function self.Has<Key>GetterPrefix( ) - Returns whether or not a Getter Prefix exists ( Must be String data-type, non '' )
		##
		def AccessorFuncHasGetterPrefix( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameHasGetterPrefix( ) );


		##
		## function self.Get<Key>GetterPrefix( ) - Returns the Getter PRefix ( Returns '' if doesn't have a prefix )
		##
		def AccessorFuncGetGetterPrefix( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameGetterPrefix( ) );


		##
		## function self.Get<Key>Name( ) - Returns the Formal ( Title-Case, typically ) Name for this Property
		##
		def AccessorFuncGetName( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameName( ) );



		##
		## function self.Get<Key>Key( ) - Returns the ID ( typically lowercase ) Key for this Property
		##
		def AccessorFuncGetKey( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameKey( ) );


		##
		## function self.Get<Key>AccessorKey( ) - Returns the Internal Accessor Key for this Property ( Typically __<key> )
		##
		def AccessorFuncGetAccessorKey( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameGetAccessorKey( ) );


		##
		## function self.Get<Key>DataKey( ) - Returns the Internal Data Storage Key for this Property ( Typically _<key> )
		##
		def AccessorFuncGetDataKey( this ):
			return AccessorFuncGetAccessor( this, self.StaticNameGetDataKey( ) );


		##
		## function self.Set<Key>( _value ) -
		##
		def AccessorFuncSet( this, _value = None ):
			return AccessorFuncGetAccessor( this, 'Set', this, _value );


		##
		## function self.Add<Key>( _value ) -
		##
		def AccessorFuncAdd( this, _value = None ):
			return AccessorFuncGetAccessor( this, 'Add', this, _value );


		##
		## function self.Pop<Key>( _index = len - 1, _count = 1 ) -
		##
		def AccessorFuncPop( this, _index = None, _count = 1 ):
			return AccessorFuncGetAccessor( this, 'Pop', this, _index, _count );


		##
		## function self.Reset<Key>( _value ) -
		##
		def AccessorFuncReset( this ):
			## return GetKeyAccessor( this, 'Set', this, None )
			return AccessorFuncGetAccessor( this, 'Reset', this );


		##
		## Output all Instance Functions added to the parent class which initialized the Accessor Properties
		##
		def GetInstanceWiki( this ):
			##
			_key			= getattr( this, self.GetAccessorKey( ), None );
			_key_data		= getattr( this, self.GetAccessorDataKey( ), None );
			_key_property	= getattr( this, self.GetAccessorFuncKey( ), None );

			##
			_text = '';

			## List all functions added - include the instance functions...


			## Task: Output layout of wiki should be either | list of names vertical | next one along side | next | next - but word-wrap is bad, so it'd need to be output to the output panel, or I'll open it like I open the expanded vars example... I should also update the plugin output info the same way...
			## Task: Output layout of wiki: Either before or after the functions list, show the Accessor stats such as the name before outputting functions, getter prefix, etc... Since this is the instance object un-named, it covers all accessors added to the instance so we need to update the database in the actual instance / parent... and read from it...

			##
			## Parent Object Information
			## Number of Accessors, the names of all of them in a simple list, etc..
			##
			## Maybe more info...
			## Maybe more info...
			##
			## Function List Header - Name above each, list of functions used... Format String...
			## function list | function list | function list |
			## function list | function list | function list |
			## function list | function list | function list |
			##
			## Maybe more info...
			## Maybe more info...
			##
			##



			## For each Name, grab named wikis...







			##
			return _text;


		##
		## Output all Single Name / Accessor / Property-Specific Functions added
		##
		def GetNameWiki( this ):
			##
			_key			= getattr( this, self.GetAccessorKey( ), None );
			_key_data		= getattr( this, self.GetAccessorDataKey( ), None );
			_key_property	= getattr( this, self.GetAccessorFuncKey( ), None );

			##
			_text = '';

			## List all functions added - include the instance functions...


			##
			## Task: This is the name wiki... Basically, it gets the longest name and gives the length, adds a buffer and that's how wide the column will be for all of the functions...
			## Task: May need more... So, GetNameWiki should be for each Named function, so ... hmm.....
			##
			##




			##
			return _text;


		##
		## Output all Names / Accessors / Properties added to the parent class... This should be complete ( calls made to GetNameWiki for each Accessor set added, and one time call to GetInstanceWiki to output those functions and details )
		##
		def GetAccessorWiki( this ):
			##
			_key			= getattr( this, self.GetAccessorKey( ), None );
			_key_data		= getattr( this, self.GetAccessorDataKey( ), None );
			_key_property	= getattr( this, self.GetAccessorFuncKey( ), None );

			##
			_text = '';

			## List all functions added - include the instance functions...
			##
			## Get Accessor Wiki - This should be the wiki entry for everything this accessor related... This accessor is name based, the functions created, and more... Or, am I formatting it differently? GetNameWiki basically does the same thing... not sure... InstanceWiki pulls it all together...
			##






			##
			return _text;


		##
		##
		##
		def GetGetterOutput( this, _name, _default_override = None, _ignore_defaults = False, _getter_prefix = 'Get' ):
			_col_key = 35;
			_col_value = 20;
			_show_args = False;

			##
			_default_override_text = 'D: ';
			_ignore_defaults_text = 'I: ';

			## Task: Setup Accessor DB to log all keys / names, etc.... and some helper accessors, instances, or other info, in order to easily grab data...
			## Exit self ( stuck on 1 object because called by __str__ self - trying to call on all keys / names... ), re-enter using provided name as key or modify name to fit key, if it works... to get proper data...
			_base = self.GenerateAccessorKey( this );

			##
			_args = '' #_default_override_text + ': ' + str( _default_override ) + ', ' + _ignore_defaults_text + ': ' + str( _ignore_defaults )

			## ##
			## _text = ''
			## _text += String.FormatColumn( _col_key - 13,		'self.Is' + _name + 'Set( ): ',														_col_value - 10,	str( getattr( this, 'Is' + _name + 'Set' )( ) ) )
			## ## _text += String.FormatColumn( _col_key - 14,	'self.' + _getter_prefix + _name + '( ' + _args + ' ): ',							_col_value,			str( getattr( this, _getter_prefix + _name + '' )( _default_override, _ignore_defaults ) ) )
			## _text += String.FormatColumn( _col_key - 6,		'self.' + _getter_prefix + _name + 'ToString( ' + _args + ' ): ',					_col_value,			getattr( this, _getter_prefix + _name + 'ToString' )( _default_override, _ignore_defaults ) )
			## ## _text += String.FormatColumn( _col_key,		'self.' + _getter_prefix + _name + 'Len( ' + _args + ' ): ',						_col_value,			str( getattr( this, _getter_prefix + _name + 'Len' )( _default_override, _ignore_defaults ) ) )
			## _text += String.FormatColumn( _col_key - 3,		'self.' + _getter_prefix + _name + 'LenToString( ' + _args + ' ): ',				_col_value,			getattr( this, _getter_prefix + _name + 'LenToString' )( _default_override, _ignore_defaults ) )
			## _text += String.FormatColumn( _col_key - 12,		'self.' + _getter_prefix + _name + 'Raw( ): ',										_col_value,			str( getattr( this, _getter_prefix + _name + 'Raw' )( ) ) )
			## _text += String.FormatColumn( _col_key - 3,		'self.' + _getter_prefix + _name + 'DefaultValue( ): ',								_col_value,			str( getattr( this, _getter_prefix + _name + 'DefaultValue' )( ) ) )

			##
			_text = '';
			_text += String.FormatColumn( _col_key - 13,	self.AccessorCallNameIsSet( _show_args ) + ': ',				_col_value - 10,	str( self.AccessorCallIsSet( this ) ) );
			_text += String.FormatColumn( _col_key - 14,	self.AccessorCallNameGet( _show_args ) + ': ',					_col_value + 5,		str( self.AccessorCallGet( this, _default_override, _ignore_defaults ) ) );
			_text += String.FormatColumn( _col_key - 12,	self.AccessorCallNameGetRaw( _show_args ) + ': ',				_col_value + 5,		str( self.AccessorCallGetRaw( this ) ) );
			_text += String.FormatColumn( _col_key - 3,		self.AccessorCallNameGetDefaultValue( _show_args ) + ': ',		_col_value + 5,		str( self.AccessorCallGetDefaultValue( this ) ) );
			_text += String.FormatColumn( _col_key - 12,	self.AccessorCallNameGetLen( _show_args ) + ': ',				_col_value - 15,	str( self.AccessorCallGetLen( this, _default_override, _ignore_defaults ) ) );

			## _text += String.FormatColumn( _col_key - 3,		self.AccessorCallNameHasGetterPrefix( _name, _getter_prefix, _show_args ) + ': ',		_col_value - 10,	str( getattr( getattr( this, '__' + _name, None )self.AccessorCallHasGetterPrefix( this, _name, _getter_prefix ) ) )
			## _text += String.FormatColumn( _col_key - 3,		self.AccessorCallNameGetGetterPrefix( _name, _getter_prefix, _show_args ) + ': ',		_col_value - 10,	str( getattr( getattr( this, '__' + _name, None )self.AccessorCallGetGetterPrefix( this, _name, _getter_prefix ) ) )

			_text += String.FormatColumn( _col_key - 3,		self.AccessorCallNameHasGetterPrefix( _show_args ) + ': ',		_col_value + 25,	str( getattr( _base, 'GetBaseAllowedTypes' )( ) ) );
			_text += String.FormatColumn( _col_key - 3,		self.AccessorCallNameGetGetterPrefix( _show_args ) + ': ',		_col_value + 25,	str( getattr( _base, 'GetBaseAllowedValues' )( ) ) );

			## Need to link allowed types through to instance?? Since this is a
			## _text += String.FormatColumn( _col_key - 3,		self.StaticNameGetBaseAllowedTypes( ) + ': ',											_col_value + 20,	str( self.GetBaseAllowedTypes( ) ) )
			## _text += String.FormatColumn( _col_key - 2,		self.StaticNameGetBaseAllowedValues( ) + ': ',											_col_value + 20,	str( self.GetBaseAllowedValues( ) ) )


			## _text += String.FormatColumn( _col_key - 15,		self.AccessorCallNameGetToString( _name, _getter_prefix, _show_args ) + ': ',				col_value + 5,		self.AccessorCallGetToString( this, _default_override, _ignore_defaults, _name, _getter_prefix ) )
			## _text += String.FormatColumn( _col_key - 12,		self.AccessorCallNameGetLenToString( _name, _getter_prefix, _show_args ) + ': ',			_col_value - 10,	self.AccessorCallGetLenToString( this, _default_override, _ignore_defaults, _name, _getter_prefix ) )


			return _text # + '\n'


		##
		##
		##
		def GetKeyOutput( this, _key = None, _header = False, _footer = False ):
			##
			if ( _key == None and not _header and not _footer):
				return '\n\t\t[ No Key Provided ]';

			##
			def GetHorizontalBar( ):
				## Max Width: 467 + tab = 471 + left = 581
				_text = String.FormatColumnEx( '-',		_col_key_tab,		''	)
				_text += String.FormatColumnEx( '-',	_col_key,			'',		_col_val,			''	) + _text_divider;
				_text += String.FormatColumnEx( '-',	_col_key_raw,		'',		_col_val_raw,		''	) + _text_divider;
				_text += String.FormatColumnEx( '-',	_col_key_default,	'',		_col_val_default,	''	) + _text_divider;
				_text += String.FormatColumnEx( '-',	_col_key_types,		'',		_col_val_types,		''	) + _text_divider;
				_text += String.FormatColumnEx( '-',	_col_key_values,	'',		_col_val_values,	''	) + _text_divider + _text_divider + '\n';
				_text += '';

				return _text;

			##
			def GetHeader( ):
				_text = '';
				_text += GetHorizontalBar( );
				_text += this.Name + '\t\t --- MyClass: ---- id( this.__class__ ): ' + str( id( this.__class__ ) ) + ' :::: id( this ): ' + str( id( this ) )+ '\n';
				_text += GetHorizontalBar( );
				_text += String.FormatColumnEx( ' ',	_col_key_tab,		'' )
				_text += String.FormatColumnEx( ' ',	_col_key,			'Key',								_col_val,			'Getter Value',						_col_key_divider,	_char_divider );
				_text += String.FormatColumnEx( ' ',	_col_key_raw,		'Raw Key',							_col_val_raw,		'Raw / Stored Value',				_col_key_divider,	_char_divider );
				_text += String.FormatColumnEx( ' ',	_col_key_default,	'Get Default Value',				_col_val_default,	'Default Value',					_col_key_divider,	_char_divider );
				_text += String.FormatColumnEx( ' ',	_col_key_types,		'Get Allowed Types',				_col_val_types,		'Allowed Types',					_col_key_divider,	_char_divider );
				_text += String.FormatColumnEx( ' ',	_col_key_values,	'Get Allowed Values',				_col_val_values,	'Allowed Values',					_col_key_divider,	_char_divider )+ '\n';
				_text += GetHorizontalBar( );

				return _text;

			##
			def GetFooter( ):
				return String.FormatColumnEx( '~',	( _col_key_divider * 5 - 1 + _col_key_tab + _col_key + _col_val + _col_key_raw + _col_val_raw + _col_key_default + _col_val_default + _col_key_types + _col_val_types + _col_key_values + _col_val_values ),		'' ) + '\n';


			## Max Width: 467 + tab = 471 + left = 581

			##
			_col_key_tab			= 4;
			_col_key_divider		= 2;
			_col_key				= 10;
			_col_key_raw			= 10;
			_col_key_default		= 30;
			_col_key_types			= 30;
			_col_key_values			= 30;

			##
			_col_val				= 20;
			_col_val_raw			= 25;
			_col_val_default		= 25;
			_col_val_types			= 75;
			_col_val_values			= 185;

			_char_divider_bar		= '|-';
			_char_divider			= '| ';

			##
			_text_divider			= String.FormatColumnEx( '-',	_col_key_divider,	_char_divider_bar	);

			_text					= '';

			##
			if ( _header ):
				_text += GetHeader( );

			##
			if ( _key ):
				##
				_default									= getattr( this, _key );
				_raw										= getattr( this, '_' + _key );
				_accessor									= getattr( this, '__' + _key );
				## _getter_prefix							= _accessor.GetterPrefix( );
				## _name									= _accessor.Name( );
				_internal									= _accessor.DefaultValue( );
				_allowed_types_desc, _allowed_values_desc	= _accessor.GetAllowedDesc( );


				_text += '\t'
				_text += String.FormatColumn(			_col_key,			_key + ':',										_col_val,			str( _default ),						_col_key_divider,	_char_divider );
				_text += String.FormatColumn(			_col_key_raw,		'_' + _key + ':',								_col_val_raw,		str( _raw ),							_col_key_divider,	_char_divider );
				_text += String.FormatColumn(			_col_key_default,	'__' + _key + '.DefaultValue( ):',				_col_val_default,	str( _internal ),						_col_key_divider,	_char_divider );
				_text += String.FormatColumn(			_col_key_types,		'__' + _key + '.GetAllowedTypes( )',			_col_val_types,		str( _allowed_types_desc ),				_col_key_divider,	_char_divider );
				_text += String.FormatColumn(			_col_key_values,	'__' + _key + '.GetAllowedValues( )',			_col_val_values,	str( _allowed_values_desc ),			_col_key_divider,	_char_divider );
				_text += '\n';


			##
			if ( _footer ):
				_text += GetFooter( );

			##
			return _text;


		##
		## function self.Get<Key>Helpers( ) - Returns the complete Helper Function List for this property ( May or may not include internal functions )...
		##
		def AccessorFuncGetHelpers( this, _header = False ):
			## Buffer bound to give the name 20 chars max - all names should line up as long as the name is 20 chars or less... otherwise it'll be cut off..
			_buffer = 20 - len( _name );

			_col_func = 80;
			_col_desc = 140;
			_col_value = 250;

			## Text Output...
			_text = '';

			## Text output for the row of functions...
			_text += '\n------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n';
			if ( _header ):
				_text += String.FormatColumnEx( ' ',		_col_value, 'Proper usage of this property ranges ( Please Note: The key vs name is CaSe-SeNsItIvE - the key is typically lowercase and the Name, used in Function Calls, is Title or UpperCamelCase ):' ) + '\n';
				_text += '\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\tUsing the property itself to Set / Get - When setting a value, if you defined authorized data-types / values, then the value will only be assigned if it passes those checks, regardless of method you use to set the data ( unless you set it to the internal storage key )' ) + '\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\t\t_class.' + self.Key( ) + ' = VALUE' ) + '\n';
				## _text += '\n';
				## _text += String.FormatColumnEx( ' ',		_col_value, '\t\tif ( _class.' + self.Key( ) + ' == VALUE ):\n\t\t\treturn' ) + '\n\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\t\t_var = _class.' + self.Key( ) + '' ) + '\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '' ) + '\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\tTo using the Accessor Functions Provided to make coding in Python feel more like standard programming languages' ) + '\n';
				_text += '\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\t\t_class.Set' + self.Name( ) + '( VALUE )' ) + '\n';
				_text += '\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\t\tif ( _class.Get' + self.Name( ) + '( ) == VALUE ):\n\t\t\treturn' ) + '\n\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\t\t_var = _class.Get' + self.Name( ) + '( )' ) + '\n';
				_text += '\n\t\t## Note: The Functions have useful OPTIONAL arguments to overwrite default behavior - The Getter allows you to override the default default value if no value is set... The second arg also allows you to ignore default values, so if a value isn\'t set, nothing will be returned...\n';
				## _text += String.FormatColumnEx( ' ',		_col_value, '\t\tif ( _class.Get' + self.Name( ) + '( DEFAULT_OVERRIDE, IGNORE_DEFAULTS ) == VALUE ):\n\t\t\treturn' ) + '\n\n';
				_text += '\t\t## This example would overwrite the default value of: ' + str( self.DefaultValue( ) ) + ' to None, if a value was never assigned / set. Ignore defaults is False by default, and it is optional so omitting it is perfectly fine..\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\t\tif ( _class.Get' + self.Name( ) + '( None ) == VALUE ):\n\t\t\treturn' ) + '\n';
				_text += '\n\t\t## This example would be the same thing as above, but None from the first argument is not used.. ie.. if a value was never set, None would be the value stored.. Since ignore defaults is true, a default value will not be substituted for None - so None would be returned...\n\t\t## I could change the first arg None to "Blah" and None would still be returned, unless I change True to False, or remove the second arg as ignore defaults is optional, and allow Defaults... Then "Blah" would only be returned if a value was never set..\n';
				_text += String.FormatColumnEx( ' ',		_col_value, '\t\tif ( _class.Get' + self.Name( ) + '( None, True ) == VALUE ):\n\t\t\treturn' ) + '\n';
				_text += '\n\t\tPlease Note: It doesn\'t matter if you use the Property index method or the Accessor Function method to Get or Set values... They are both processed identically - it is 100% your personal preference. ie you can not set a banned data-type or value using one method over the other if you set authorized data-types / values when you initialize the Accessor Function System.';
				_text += '\n';
			_text += '\n';1

			_text += '\n';
			_text += 'Documentation for this Accessor:\n';
			_text += self.Documentation( );

			_text += '\n';
			_text += '\n';

			## Base Accessor Helpers in Instance Object
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\tthis.ResetAccessors( _name [ , _name ] ... )',						_col_desc, 'Instance Class Helper Function: Allows resetting many key stored values to None on a single line.',												_col_value, 'In short: Calls this.Reset<Name>() for each _name provided...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\tthis.SetAccessors( _name, _value [ , _name, _value ] ... )',			_col_desc, 'Instance Class Helper Function: Allows assigning many keys / values on a single line - useful for initial setup, or to minimize lines...',		_col_value, 'In short: Calls this.Set<Name>( _value ) for each _name / _value pairing...'											 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t_data  =\tthis.GetAccessors( _name [ , _name ] ... )',						_col_desc, 'Instance Class Helper Function: Allows getting the stored values on a single line.',															_col_value, 'In short: Returns this.Get<Name>() for each _name provided... Note: You can use the _data = format to return the [ ] returned...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t_a, _b, _c =\tthis.GetAccessors( _name [ , _name ] ... )',					_col_desc, 'Instance Class Helper Function: Allows getting the stored values on a single line.',															_col_value, 'In short: Returns this.Get<Name>() for each _name provided... Note: You can split the list by adding variable assignments in the same order you get each value...'	 ) + '\n';
			_text += '\n';

			## Base Accessor Helpers in Instance Object
			## this.SetAccessorsByKey( _key, _value [ , _key, _value ] ... )
			## this.ResetAccessorsByKey( _key [ , _key ] ... )
			## this.GetAccessorsByKey( _key [ , _key ] ... )
			##
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsCallNameResetByKey( ),								_col_desc, self.AccessorsDescResetByKey( ),					_col_value, 'In short: Calls this.<Key> = None; for each _key provided...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsCallNameSetByKey( ),								_col_desc, self.AccessorsDescSetByKey( ),					_col_value, 'In short: Calls this.<Key> = _value; for each _key / _value pairing...'											 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t_data  =\t' + self.AccessorsCallNameGetByKey( ),							_col_desc, self.AccessorsDescGetByKey( ),					_col_value, 'In short: Returns this.<Key> for each _key provided... Note: You can use the _data = format to return the [ ] returned...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t_a, _b, _c =\t' + self.AccessorsCallNameGetByKey( ),							_col_desc, self.AccessorsDescGetByKey( ),					_col_value, 'In short: Returns this.<Key> for each _key provided... Note: You can split the list by adding variable assignments in the same order you get each value...'	 ) + '\n';
			_text += '\n';

			## Group Accessor Helpers in Instance Object ( Each ID can be in more than 1 group )...
			## this.ResetAccessorsGroup( _name [ , _name ] ... )
			## this.SetAccessorsGroup( _name [ , _name ] ... )
			## this.GetAccessorsInGroup( _name [ , _name ] ... )
			## this.GetAccessorsInGroup( _name [ , _name ] ... )
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsNameResetGroup( ),									_col_desc, self.AccessorsDescResetGroup( ),					_col_value, 'In short: Calls this.Reset<Name>() for all IDs in provided groups...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsNameSetGroup( ),									_col_desc, self.AccessorsDescSetGroup( ),					_col_value, 'In short: Calls this.Set<Name>() for all IDs in provided groups...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsNameAddGroup( ),									_col_desc, self.AccessorsDescGetGroup( ),					_col_value, 'In short: Calls this.Get<Name>() for all IDs in provided groups...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsNameAddGroup( ),									_col_desc, self.AccessorsDescGetGroup( ),					_col_value, 'In short: Calls this.Get<Name>() for all IDs in provided groups...'	 ) + '\n';
			_text += '\n';

			## Group Accessor Helpers in Instance Object ( Each ID can be in more than 1 group )...
			## this.ResetAccessorsGroup( _name [ , _name ] ... )
			## this.SetAccessorsGroup( _name [ , _name ] ... )
			## this.GetAccessorsInGroup( _name [ , _name ] ... )
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsNameResetGrouped( ),								_col_desc, self.AccessorsDescResetGrouped( ),				_col_value, 'In short: Calls this.Reset<Name>() for all IDs in provided groups...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsNameSetGrouped( ),									_col_desc, self.AccessorsDescSetGrouped( ),					_col_value, 'In short: Calls this.Set<Name>() for all IDs in provided groups...'	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsNameGetGrouped( ),									_col_desc, self.AccessorsDescGetGrouped( ),					_col_value, 'In short: Calls this.Get<Name>() for all IDs in provided groups...'	 ) + '\n';
			_text += '\n';


			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsCallNameResetDB( ),								_col_desc, self.AccessorsDescResetDB( ),					_col_value, ''	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsCallNameSetDB( ),									_col_desc, self.AccessorsDescSetDB( ),						_col_value, ''	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsCallNameGetDB( ),									_col_desc, self.AccessorsDescGetDB( ),						_col_value, ''	 ) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	'\t\t\t\t\t' + self.AccessorsCallNameAddDB( ),									_col_desc, self.AccessorsDescAddDB( ),						_col_value, ''	 ) + '\n';
			_text += '\n';




			_text += '\n';
			_text += '\nNote: Functions below may list self.Get / Set / Name( _args ) - self is meant as the class instance reference in the cases below - coded as this in AccessorFuncBase Class..\n';
			_text += '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGet( ),					_col_desc, self.AccessorDescGet( ),						_col_value,		str( self.AccessorCallGet( this ) )						) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetRaw( ),					_col_desc, self.AccessorDescGetRaw( ),					_col_value,		str( self.AccessorCallGetRaw( this ) )					) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameIsSet( ),					_col_desc, self.AccessorDescIsSet( ),					_col_value,		str( self.AccessorCallIsSet( this ) )					) + '\n\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetToString( ),			_col_desc, self.AccessorDescGetToString( ),				_col_value,		str( self.AccessorCallGetToString( this ) )				) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetLen( ),					_col_desc, self.AccessorDescGetLen( ),					_col_value,		str( self.AccessorCallGetLen( this ) )					) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetLenToString( ),			_col_desc, self.AccessorDescGetLenToString( ),			_col_value,		str( self.AccessorCallGetLenToString( this ) )			) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetDefaultValue( ),		_col_desc, self.AccessorDescGetDefaultValue( ),			_col_value,		str( self.AccessorCallGetDefaultValue( this ) )			) + '\n\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetAccessor( ),			_col_desc, self.AccessorDescGetAccessor( ),				_col_value,		str( self.AccessorCallGetAccessor( this ) )				) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetAllowedTypes( ),		_col_desc, self.AccessorDescGetAllowedTypes( ),			_col_value,		str( self.AccessorCallGetBaseAllowedTypes( this ) )		) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetAllowedValues( ),		_col_desc, self.AccessorDescGetAllowedValues( ),		_col_value,		str( self.AccessorCallGetBaseAllowedValues( this ) )	) + '\n\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetHelpers( ),				_col_desc, self.AccessorDescGetHelpers( ),				_col_value,		'THESE ROWS OF TEXT'				 					) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetOutput( ),				_col_desc, self.AccessorDescGetOutput( ),				_col_value,		'ROWS OF TEXT'				 							) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetGetterOutput( ),		_col_desc, self.AccessorDescGetGetterOutput( ),			_col_value,		'ROWS OF TEXT'				 							) + '\n\n';


			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameSet( ),					_col_desc, self.AccessorDescSet( ),						_col_value,		'N / A'													) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameReset( ),					_col_desc, self.AccessorDescReset( ),					_col_value,		'N / A'													) + '\n';


			## _data = _foo.GetAccessors( 'Name', 'X', 'Y', 'Z', 'Blah', 'Height', 'Width', 'Depth' )
			## print( '''_data = _foo.GetAccessors( 'Name', 'X', 'Y', 'Z', 'Blah', 'Height', 'Width', 'Depth' )''' )
			## print( '-- _data as a List being output has ' + str( len( _data ) ) + ' entries...' )

			## _name, _x, _y, _z, _blah, _h, _w, _d = _foo.GetAccessors( 'Name', 'X', 'Y', 'Z', 'Blah', 'Height', 'Width', 'Depth' )
			## print( '-- Now, instead of as a list, we use each individual var...' )
			## print( '''_name, _x, _y, _z, _blah, _h, _w, _d = _foo.GetAccessors( 'Name', 'X', 'Y', 'Z', 'Blah', 'Height', 'Width', 'Depth' )''' )


			_text += '\n'
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameHasGetterPrefix( ),		_col_desc, self.AccessorDescHasGetterPrefix( ),			_col_value,		str( self.AccessorCallHasGetterPrefix( this ) )			) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetGetterPrefix( ),		_col_desc, self.AccessorDescGetGetterPrefix( ),			_col_value,		str( self.AccessorCallGetGetterPrefix( this ) )			) + '\n';

			_text += '\n'
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetName( ),				_col_desc, self.AccessorDescGetName( ),					_col_value,		str( self.AccessorCallGetName( this ) )					) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetKey( ),					_col_desc, self.AccessorDescGetKey( ),					_col_value,		str( self.AccessorCallGetKey( this ) )					) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetAccessorKey( ),			_col_desc, self.AccessorDescGetAccessorKey( ),			_col_value,		str( self.AccessorCallGetAccessorKey( this ) )			) + '\n';
			_text += String.FormatColumnEx( ' ',		_col_func,	self.AccessorCallNameGetDataKey( ),				_col_desc, self.AccessorDescGetDataKey( ),				_col_value,		str( self.AccessorCallGetDataKey( this ) )				) + '\n';

			return _text




		## ##
		## ##
		## ##
		## def AccessorFuncGetOutput( self, this = None ):
		## 	return ''

		## function self.Get<Key>DefaultValue( )
		## AccessorFuncGetOutput.__name__ = self.AccessorNameGetOutput( );
		## setattr( _parent, AccessorFuncGetOutput.__name__, AccessorFuncGetOutput )


		## ##
		## ##
		## ##
		## def AccessorFuncGetGetterOutput( self, this = None ):
		## 	return ''

		## function self.Get<Key>DefaultValue( )
		## AccessorFuncGetGetterOutput.__name__ = self.AccessorNameGetGetterOutput( );
		## setattr( _parent, AccessorFuncGetGetterOutput.__name__, AccessorFuncGetGetterOutput )





		##
		## Register all SetupAccessorFuncs Accessors for the Instance Class Calls... - Note: These instance functions are added to the parent class too, but these are name specific.. They are added for each name added. so GetThreadJobs, GetThread, GetBlah, etc...
		##

		##
		if ( Acecool.GetSetting( 'output_accessorfunc_register_info', False ) ):
			##
			print( '\nRegisterAccessor -> RegisterNamedAccessors ' + Text.FormatColumn( 10, ' -> ', 20, self.Name( ), 10, ' -> ', 6, 'class ', 20, self.ClassName( ) + '( ... ):' ) + ' -> Getter Prefix: ' + self.GetterPrefix( ) );

		## function self.Get<Name>( _default_override, _ignore_defaults )														, self.AccessorNameGet( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET, AccessorFuncGet );

		## function self.Get<Key>Raw( )																							, self.AccessorNameGetRaw( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_RAW, AccessorFuncGetRaw );

		## function self.Is<Key>Set( )																							, self.AccessorNameIsSet( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_ISSET, AccessorFuncIsSet );

		## function self.Get<Key>ToString( )																					, self.AccessorNameGetToString( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_STR, AccessorFuncGetToString );

		## function self.Get<Key>Len( )																							, self.AccessorNameGetLen( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_LEN, AccessorFuncGetLen );

		## function self.Get<Key>LenToString( )																					, self.AccessorNameGetLenToString( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_LEN_STR, AccessorFuncGetLenToString );

		## function self.Get<Key>DefaultValue( )																				, self.AccessorNameGetDefaultValue( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_DEFAULT_VALUE, AccessorFuncGetDefaultValue );

		## function self.Get<Key>Property( ) - Like Get<Key>Accessor - instead of grabbing self / AccessorFuncBase object, we grab the property defined at <key>...
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_PROPERTY, AccessorFuncGetProperty );

		## function self.Get<Key>Accessor( ) - Uses Accessor ( self ) stored at this.__<Key> and then grabs FuncName and passes args :: ie self.AccessorFuncGetAccessor( 'FuncName', _Args ) which returns this.__<Key>.FuncName( *_args )																																					, self.AccessorNameGetAccessor( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_ACCESSOR, AccessorFuncGetAccessor );

		## function self.Get<Key>AllowedTypes( )																				, self.AccessorNameGetAllowedTypes( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_ALLOWED_TYPES, AccessorFuncGetAllowedTypes );

		## function self.Get<Key>AllowedValues( )																				, self.AccessorNameGetAllowedValues( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_ALLOWED_VALUES, AccessorFuncGetAllowedValues );

		## function self.Get<Key>BaseAllowedTypes( )																			, self.AccessorNameGetBaseAllowedTypes( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_BASE_ALLOWED_TYPES, AccessorFuncGetBaseAllowedTypes );

		## function self.Get<Key>BaseAllowedValues( )																			, self.AccessorNameGetBaseAllowedValues( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_BASE_ALLOWED_VALUES, AccessorFuncGetBaseAllowedValues );

		## function self.<Key>HasGetterPrefix( )																				, self.AccessorNameHasGetterPrefix( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_HAS_GETTER_PREFIX, AccessorFuncHasGetterPrefix );

		## function self.Get<Key>GetterPrefix( )																				, self.AccessorNameGetGetterPrefix( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_GETTER_PREFIX, AccessorFuncGetGetterPrefix );

		## function self.Get<Key>GetName( )																						, self.AccessorNameGetName( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_NAME, AccessorFuncGetName );

		## function self.Get<Key>GetKey( )																						, self.AccessorNameGetKey( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_KEY, AccessorFuncGetKey );

		## function self.Get<Key>AccessorKey( )																					, self.AccessorNameGetAccessorKey( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_ACCESSOR_KEY, AccessorFuncGetAccessorKey );

		## function self.Get<Key>DataKey( )																						, self.AccessorNameGetDataKey( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_DATA_KEY, AccessorFuncGetDataKey );

		## function self.Set<Key>( )																							, self.AccessorNameSet( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_SET, AccessorFuncSet );

		## function self.Add<Key>( _value )																						, self.AccessorNameAdd( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_ADD, AccessorFuncAdd );

		## function self.Pop<Key>( _index = len - 1, _count = 1 )																, self.AccessorNamePop( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_POP, AccessorFuncPop );



		## function self.Reset<Key>( )																							, self.AccessorNameReset( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_RESET, AccessorFuncReset );

		## function self.Get<Key>Helpers( )																						, self.AccessorNameGetHelpers( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_HELPERS, AccessorFuncGetHelpers );

		## function self.GetKeyOutput( *_keys ... )																				, self.AccessorNameGetOutput( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_OUTPUT, GetKeyOutput );

		## function self.GetGetterOutput( *_keys ... )																			, self.AccessorNameGetGetterOutput( )
		self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_GETTER_OUTPUT, GetGetterOutput );

		## function self.Get<Key>Wiki( )																						, self.AccessorNameGetWiki( )
		## self.RegisterAccessor( _parent, ACCESSORFUNC_ID_GET_WIKI, GetWiki );

		## print( );


	##
	##
	##
	def ProcessCallback( self, _parent, _id, *_args ):
		## print( 'AccessorFuncBase -> ProcessCallback for id: ' + str( _id ) + ' using: \n\tself:\t\t' + str( self ) + '\n\t_parent:\t'  + str( _parent ) + '\n\t_id:\t\t"'  + str( _id ) + '"\n\t_args:\t\t'  + str( _args ) + '\n' )
		if ( callable( self.Setup( _id ) ) ):
			## print( 'AccessorFuncBase -> ProcessCallback for id: ' + str( _id ) + ' -> callback' )
			self.Setup( _id )( _parent, *_args );
			## { 'on_set': ( lambda self, this, _current, _new, _has_updated: this.Unpack( ) ) }


	##
	## Returns the AccessorFuncc Object Setup Instructions - if _key is defined, pull the record for that instruction..
	##
	def Setup( self, _key = None ):
		if ( _key == None ):
			return self.__setup;

		return self.__setup.get( _key, False );


	##
	## Returns whether or not this Property / Accessor is a Table type ( Enables Pop functionality, and others...
	##
	def IsTableProperty( self ):
		##
		_allowed_types = self.GetAllowedTypes( );

		## If we allow all data-types, only lists, dicts or tuples then enable pop and other table-specific helpers...
		return ( _allowed_types == None or _allowed_types.get( [ ], False ) or _allowed_types.get( { }, False ) or _allowed_types.get( ( ), False ) );


	##
	##
	##
	def Pop( self, _parent = None, _index = None, _count = 1 ):
		## Use the GetRaw new Property system for aliases to shorten the code here...
		_get = self.Get( _parent )
		_isset = self.GetRaw( _parent ) != None;
		if ( _isset and IsDict( _get ) or IsTableTypeSet( _get ) or IsList( _get ) or IsTuple( _get ) ):
			return _get.pop( ( _index, len( _get ) - 1 )[ _index == None ] )



	##
	## Helper - Returns whether or not a value can be set based on data-type and / or value restrictions, and checks to see if the parent class has a CanSet method for this key to act as an override which also gets a say, if it exists...
	##
	def CanSet( self, _parent = None, _value = None ):
		## Determine if the value being assigned is valid to use - if not we assign the current value, otherwise we assign the new value...
		_valid_data = self.IsValidData( _value );

		## Determine if the Parent Class Instance has a CanSet Callback / Override function - If it does, call it...
		if ( hasattr( _parent, 'CanSet' ) and callable( getattr( _parent, 'CanSet' ) ) ):
			pass;
			## If the CanSet returns a non-boolean, use _valid_data... If boolean, then use that - also make sure the callback has the valid data arg so they can see if the data fails or passes the valid data-type / value system...

		## Alternative - use CanSet<Key>
		if ( hasattr( _parent, 'CanSet' + self.Key( ) ) and callable( getattr( _parent, 'CanSet' + self.Key( ) ) ) ):
			pass;

		return _valid_data;


	##
	## Resets the stored value to None...
	##
	def Reset( self, _parent = None ):
		return setattr( _parent, self.GetDataKey( ), None );
		## return self.Set( _parent, None ); # , self.Key( );


	##
	## Called to Set the value
	##
	## def Set( self, _parent = None, _value = None ):
	def Set( self, _parent = None, _value = None ):
		## If parent isn't provided, we can't do anything other tham providing static information..
		if ( _parent == None ):
			## On Set Fail, if we have an on_fail_set key in our setup table and it is callable, then call it...
			self.ProcessCallback( _parent, 'on_set_fail', '_parent is undefined; can\'t retrieve current value!', None, _value, False );

			##
			## print( 'AccessorFunc -> Set -> Value NOT UPDATED because of _parent being not set for key: ' + self.Key( ) + ' to ' + str( _value ) + ' and it remains ' + str( _current ) );


			## print( 'AccessorFuncBase -> Set -> _parent == None' );
			return self;

		##Grab the current value, in case we don't update the value - we need to assign the current value otherwise None will be assigned...
		_current = getattr( _parent, self.GetDataKey( ) );

		## The value and data-type checks determine if _value is an authorized data-type or value - If is is not a valid data-type, and it is not a valid value, then we prevent the value from being set...
		if ( not self.CanSet( _parent, _value ) ):
			## Update the value or define it so that it exists...
			setattr( _parent, self.GetDataKey( ), _current );

			##
			## print( 'AccessorFuncBase -> Set -> !CanSet' );
			## print( 'AccessorFuncBase -> Set -> CanSet -> ProcessCallback using "on_set", ' + str( _parent ) + ', '  + str( _current ) + ', '  + str( _value ) + ', '  + str( True ) + '!' );

			## On Set Fail, if we have an on_fail_set key in our setup table and it is callable, then call it...
			self.ProcessCallback( _parent, 'on_set_fail', 'CanSet Callback prevented the assignment!', _current, _value, False );


			##
			## print( 'AccessorFunc -> Set -> Value NOT UPDATED for key: ' + self.Key( ) + ' to ' + str( _value ) + ' and it remains ' + str( _current ) );

			## if ( callable( self.Setup( 'on_set_fail' ) ) ):
			## 	self.Setup( 'on_set_fail' )( _current, _current, False );

			## Return the current value so we don't reset the variable - we only want it to be set to something else if the developer says so - setting it to an incorrect value doesn't mean reset it..
			return _current;

		## Update the value or define it so that it exists...
		setattr( _parent, self.GetDataKey( ), _value );

		##
		## print( 'AccessorFunc -> Set -> Updated Value for key: ' + self.Key( ) + ' to ' + str( _value ) );

		##
		## print( 'AccessorFuncBase -> Set -> CanSet' );
		## print( 'AccessorFuncBase -> Set -> CanSet -> ProcessCallback using "on_set", ' + str( _parent ) + ', '  + str( _current ) + ', '  + str( _value ) + ', '  + str( True ) + '!' );

		## On Set, if we have an on_set key in our setup table and it is callable, then call it...
		self.ProcessCallback( _parent, 'on_set', _current, _value, True );

		## if ( callable( self.Setup( 'on_set' ) ) ):
		## 	self.Setup( 'on_set' )( _current, _value, True );

		## Return the value - we are a setter afterall...
		return _value;


	##
	## Retrieves the value from the parent class._<key>, or, if the data is None, then it'll return the Default value
	##
	## Fix: For some reason, when _default is callable, it returns the function object instead of the return value which isn't wanted behavior and it isn't coded that way unless something else is taking affect or I missed something... Review...
	## def Get( self, _parent = None ):
	def Get( self, _parent = None ):
		##
		_default = self.DefaultValue( );

		## Use the GetRaw new Property system for aliases to shorten the code here...
		_base = self.GetRaw( _parent );

		##
		## print( 'AccessorFunc -> Get -> Trying to retrieve data for key: ' + self.Key( ) + ' in ' + str( self.Name( ) ) );

		## If _base isn't None, it means we have a viable value we can use
		if ( _base != None ):
			## print( 'Return Getter _base for ' + self.Name( ) );
			return _base;

		## If parent is or isn't provided and / or the raw value isn't defined - we need to return a default value. If the default value is set as a function, we call it... otherwise we return it..
		if ( _default != None and callable( _default ) ):
			## _data = _default( self, _parent );
			_data = _default( _parent );

			## print( 'Return Callable Getter for ' + self.Name( ) );

			## if ( self.CanSet( _parent, _data ) ):
			return _data;

		## print( 'Return Getter _default for ' + self.Name( ) );
		return _default;

		## return _base if ( _base != None ) else self.DefaultValue( );


	##
	## Called to Add the value
	##
	def Add( self, _parent = None, *_values ): #, _value = None
		## if ( _value != None ):
		## 	self.Set( _parent, self.Get( _parent ) + _value );

		##
		_data = self.Get( _parent );
		_type = type( self.Get( _parent ) );

		##
		if ( len( _values ) > 0 ):
			## print( 'Determining if AccessorFuncBase.Add is adding to a list or tuple: ' + str( type( _data ) is list or type( _data ) is tuple ) + ' or ' + str( isinstance( _data, list ) or isinstance( _data, tuple ) ) );
			for _value in _values:
				##
				if ( isinstance( _data, list ) or isinstance( _data, tuple ) ):
					_data = _data.append( _value );
				else:
					_data = _data + _value;

			##
			self.Set( _parent, _data );


	##
	## Called on deletion..
	##
	def Del( self, _parent = None ):
		pass;


	##
	## Retrieves the value from the parent class._<key>
	##
	def GetRaw( self, _parent = None ):
		## If paren't is set, then we check to see if a value is defined... in the Raw data location...
		if ( _parent != None ):
			_base = getattr( _parent, self.GetDataKey( ) );

			## Task: Future: If we allow raw value pass-through, then allow a raw stored function reference to be called - need to add *_args ( variable arguments list ) or **_varargs ( keys variable arguments list ) - add a controlled toggle...
			## if ( callable( _base ) ):
			## 	return _base( self, _parent );

			## Return the value, if set..
			if ( _base != None ):
				return _base;

		##
		return None;


	##
	## Called on deletion..
	##
	def DelRaw( self, _parent = None ):
		pass;


	##
	## Returns the Getter Prefix
	##
	def GetterPrefix( self, _prefix_override = None ):
		return ( ( '', self.__prefix_get )[ self.HasGetterPrefix( ) ], _prefix_override )[ _prefix_override != None ];


	##
	## Returns the key
	##
	def HasGetterPrefix( self ):
		return type( self.__prefix_get ) is str # self.GetterPrefix( ) != None and self.GetterPrefix( ) != '';


	##
	## Returns the Class Name this Accessor is registered to...
	##
	def ClassName( self, _name_override = None ):
		return ( self.__class_name, _name_override )[ _name_override != None ];


	##
	## Returns the key
	##
	def Name( self, _name_override = None ):
		return ( self.__name, _name_override )[ _name_override != None ];


	##
	## Returns the key
	##
	def Key( self, _key = None ):
		return ( ( _key, self.__key )[ _key == None ] );


	##
	## Returns the key
	##
	def DataKey( self, _key = None ):## Return the value - we are a getter afterall...
		return ( '_' + ( _key, self.__key )[ _key == None ] );


	##
	## Returns the key
	##
	def AccessorKey( self, _key = None ):## Return the value - we are a getter afterall...
		return ( '__' + ( _key, self.__key )[ _key == None ] );


	##
	## Alias of Key - Returns the __<key> or, for alias support allow using this as a builder if a _key is provided
	##
	def GetKey( self, _key = None ):
		return self.Key( _key );


	##
	## Alias of DataKey - Returns the __<key> or, for alias support allow using this as a builder if a _key is provided
	##
	def GetDataKey( self, _key = None ):
		return self.DataKey( _key );


	##
	## Alias of AccessorKey - Returns the __<key> or, for alias support allow using this as a builder if a _key is provided
	##
	def GetAccessorKey( self, _key = None ):
		return self.AccessorKey( _key );


	##
	## Attempt to find an existent key through the use of the AccessorFunc Key, Name and LowerCase Name...
	##
	def GenerateKey( self, _parent = None, _prefix = '' ):
		## Try the default key
		_key = getattr( this, _parent + self.Key( ), None );

		## If the default key isn't there, try using the AccessorFunc Name as a key...
		_key = getattr( this, _parent + self.Name( ) ) if _key == None else _key;

		## If the AccessorFunc Name isn't assigned, try using the AccessorFunc Name as a lower-case string...
		_key = getattr( this, _parent + self.Name( ).lower( ) ) if _key == None else _key;

		## Returns either None, or the _key which exists...
		return _key;


	##
	## Attempt to find an existent Data Key
	##
	def GenerateDataKey( self, _parent = None ):
		return self.GenerateKey( _parent, '_' );


	##
	## Attempt to find an existent Accessor / Property Key
	##
	def GenerateAccessorKey( self, _parent = None ):
		return self.GenerateKey( _parent, '__' );


	##
	## Returns the default value
	##
	def DefaultValue( self ):
		return self.__default;


	##
	## Returns the documentation for this Accessor
	##
	def Documentation( self ):
		return self.__documentation;


	##
	## Determines, and Returns both Human-Friendly / Readable Outputs for Allowed Data-Types and Values...
	##
	def GetAllowedDesc( self ):
		_allowed_types			= self.GetBaseAllowedTypes( );
		_allowed_values			= self.GetBaseAllowedValues( );

		##
		_status_allowed_types	= ( _allowed_types, 'Unrestricted' )[ _allowed_types == None or _allowed_types == (None,) ];
		_status_allowed_values	= ( _allowed_values, 'Unrestricted' )[ _allowed_values == None or _allowed_values == (None,) ];

		##
		_output_allowed_types	= ( _status_allowed_types, 'Saved Value Restricted to Authorized Values ONLY' )[ _status_allowed_types == 'Unrestricted' and _status_allowed_values != 'Unrestricted' ];
		_output_allowed_values	= ( _status_allowed_values, 'Saved Value Restrictions Levied by Data-Type' )[ _status_allowed_types != 'Unrestricted' and _status_allowed_values == 'Unrestricted' ];

		return _output_allowed_types, _output_allowed_values;


	##
	## Returns the Allowed Types which the user / dev supplied on creation for this Accessor...
	##
	def GetBaseAllowedTypes( self ):
		return self.__base_allowed_types;


	##
	## Returns the Human-Friendly Version of Allowed Types for this Accessor...
	##
	def GetDescAllowedTypes( self ):
		_allowed_types, _allowed_values = self.GetAllowedDesc( );
		return _allowed_types;


	##
	## Returns None if there are no restrictions, or a Dict using O( 1 ) _dict.get( _x, False ) == True to determine whether or not the _type entered is allowed..
	##
	def GetAllowedTypes( self ):
		return self.__allowed_types;


	##
	## Returns the Allowed Values which the user / dev supplied on creation for this Accessor...
	##
	def GetBaseAllowedValues( self ):
		return self.__base_allowed_values;


	##
	## Returns the Human-Friendly Version of Allowed Values for this Accessor...
	##
	def GetDescAllowedValues( self ):
		_allowed_types, _allowed_values = self.GetAllowedDesc( );
		return _allowed_values;


	##
	## Returns None if there are no restrictions, or a Dict using O( 1 ) _dict.get( _x, False ) == True to determine whether or not the _value entered is allowed..
	##
	def GetAllowedValues( self ):
		return self.__allowed_values;


	##
	## Whether or not to display created accessors in log...
	##
	def ShowCreatedAccessors( self ):
		return self.__display_accessors;


	##
	## Returns whether or not the Data Type provided is authorized to be used
	##
	def IsValidDataType( self, _type = None ):
		## Helper
		_types = self.GetAllowedTypes( )

		## If we don't need to check, return true..
		if ( _types == None ):
			return True;

		##
		return ( _types == _type or _types.get( str( _type ), False ) != False );


	##
	## Returns whether or not the Data Value provided is authorized to be used
	##
	def IsValidDataValue( self, _value = None ):
		## Helper
		_values = self.GetAllowedValues( );

		## If we don't need to check, return true..
		if ( _values == None ):
			return True;

		##
		return ( _values == _value or _values.get( str( _value ), False ) == str( type( _value ) ) );


	##
	## Returns whether
	##
	def IsValidData( self, _value = None ):
		## Make sure if we only accept certain values without defining types, then IsValid can only return true if the value is amonst the allowed...
		## If we allow types, and no values, we need to make sure that only those types are valid...
		_type			= type( _value );

		## Helpers
		_types			= self.GetAllowedTypes( );
		_values			= self.GetAllowedValues( );

		## None is always True
		if ( _value == None ):
			return True;

		## If both types and values are None, return true..
		if ( _types == None and _values == None ):
			return True;

		##
		_valid_type		= self.IsValidDataType( _type );
		_valid_value	= self.IsValidDataValue( _value );

		## If only one or the other exists - make sure it is valid...
		if ( _values == None and _types != None ):
			## print( 'Comparing using: Data-Type' )
			return ( False, True )[ _valid_type ];
		elif ( _values != None and _types == None ):
			## print( 'Comparing using: Value' )
			return ( False, True )[ _valid_value ];

		## print( 'Comparing using: Data-Type and Value -- However, since we have both, we can allow Value to be a definitive ALLOW ALL OF THESE... For Type, we may want to restrict so it is either value or value and type' )
		## print( '_valid_type: ', _valid_type );
		## print( '_types: ', _types );
		## print( '_valid_value: ', _valid_value );
		## print( '_values: ', _values );
		## print( 'Value Type: ', str( type( _value ) ) );


		## If both are defined, make sure they both exist...
		return ( False, True )[ _valid_type and _valid_value or _valid_value ];


	##
	## Helper: Converts 1 object, or a table of objects to a Dict O( 1 ) getter style table...
	##
	def SetupAllowedDict( self, _data = None ):
		## If we have no data, return None...
		if ( _data == None ):
			return None;

		## Helper
		_type = type( _data );

		## If the data isn't None, we need to determine what it is - single element or table... So if not Dict, Tuple, List then we only add 1 object to Dict and return - else we go through each element..
		if ( _type != type( { } ) and _type != type( ( ) ) and _type != type( [ ] ) ):
			return { str( _data ): str( _type ) };

		## Now, we have established it is a Dict, Tuple or List, process all of the values into keys = True...
		_dict = { }

		## For each element, process it...
		for _value in _data:
			_dict[ str( _value ) ] = str( type( _value ) );

		## return the Dict..
		return _dict;


	##
	## Helper - Extra Functionality for setting all AccessorFunc Names provided to the Values provided..
	##
	def SetAccessorFuncValues( self, this = None, _by_name = True, *_args ):
		## Callback - Called for each _key as function name and _value pairing - used to run functions named, provided, with the input provided - single-arg mainly to set AccessorFunc values to their assigned names..
		def Callback( self, _name, _value ):
			## Debugging..
			if ( _by_name ):
				## print( 'SetAccessorFuncValues', 'Executing: Set' + str( _name ) + '( ' + str( _value ) + ' )' )

				## Execute the Set function for the AccessorFunc Names provided with their associated values...setattr( this, self.GetKey( ), _value ) #
				return getattr( this, 'Set' + _name )( _value );
			else:
				## print( 'SetAccessorFuncValues', 'Executing: this.' + str( _name ) + '( ' + str( _value ) + ' )' )

				## Execute the Set function for the AccessorFunc Names provided with their associated values...
				return setattr( this, _name, _value );

		## Debugging..self: ' + str( id( self ) ) + ', this: ' + str( id( this ) ) + ', *_args:
		## print( 'SetAccessorFuncValues', 'Executing: SetAccessorFuncValues( ' + String.ToStringKeyedVarArgs( *_args ) + ' ) :: By Name: ' + str( _by_name ) + ' :: By Key: ' + str( not _by_name ) )

		## Execute...
		return Util.ProcessKeyVarArgs( self, Callback, *_args );


	##
	## Helper - Extra Functionality for setting all AccessorFunc Names provided to the Values provided..
	##
	def ResetAccessorFuncValues( self, this = None, _by_name = True, *_args ):
		## Callback - Called for each _key as function name and _value pairing - used to run functions named, provided, with the input provided - single-arg mainly to set AccessorFunc values to their assigned names..
		def Callback( self, _name ):
			## Debugging..
			if ( _by_name ):
				## print( 'ResetAccessorFuncValues', 'Executing: Reset' + _name + '( )' )

				## Execute the Set function for the AccessorFunc Names provided with their associated values... setattr( this, self.GetDataKey( ), None ) #
				return getattr( this, 'Reset' + _name )( );
			else:
				## print( 'ResetAccessorFuncValues', 'Executing: this.' + _name + ' = None' )

				## Execute the Set function for the AccessorFunc Names provided with their associated values...
				return setattr( this, _name, None )( );

		## Debugging.. self: ' + str( id( self ) ) + ', this: ' + str( id( this ) ) + ', *_args:
		## print( 'ResetAccessorFuncValues', 'Executing: Reset( ' + String.ToString( *_args ) + ' ) :: By Name: ' + str( _by_name ) )

		## Execute...
		return Util.ProcessSimpleVarArgs( self, Callback, *_args );


	##
	## Helper - Extra Functionality for getting all AccessorFunc Names provided to the Values provided..
	##
	def GetAccessorFuncValues( self, this = None, _by_name = True, *_args ):
		##
		_data = [ ];

		## Callback - Called for each _key as function name and _value pairing - used to run functions named, provided, with the input provided - single-arg mainly to set AccessorFunc values to their assigned names..
		def Callback( self, _name, _data ):
				## Debugging..
			if ( _by_name ):
				## print( 'GetAccessorFuncValues', 'Executing: return Get' + _name + '( )' )

				## Execute the Set function for the AccessorFunc Names provided with their associated values...getattr( this, self.Key( ) ) #
				return getattr( this, 'Get' + _name )( );
			else:
				## print( 'GetAccessorFuncValues', 'Executing: return this.' + _name + '' )

				## Execute the Set function for the AccessorFunc Names provided with their associated values...
				return getattr( this, _name );

		## Debugging.. self: ' + str( id( self ) ) + ', this: ' + str( id( this ) ) + ', *_args:
		## print( 'GetAccessorFuncValues', 'Executing: Get( ' + String.ToString( *_args ) + ' ) :: By Name: ' + str( _by_name ) )

		## Execute...
		return Util.ProcessAccumulateVarArgs( self, Callback, _data, *_args );


	##
	## Returns the Ca
	##




	##
	## Registers the Accessor Function with a class..
	## Task: Add the Key / Name check along with Setup instructions to allow bypassing registry if setup is instructed to ignore... or if setup defines a callable, then that is used as the function instead of the one supplied...
	##
	## Task: Read the new setup table system which includes the active_on label to determine whether or not to activate... Also, allow named tags, as they currently are, or enumerated ids for the setup table... Also, read the callbacks from the setup table this way.. Maybe setup enumeration values for them? OR simply multiply by 10000 for post, -10000 for pre, and 1000 for on? But that limits us to 1000 functions or so.. A bit high ( functions PER accessor, not total functions ) but I'd rather not limit us that way... It's likely be better to associate each tag with a pre, post, on, active, active_on tags... Also, since the rest is defined using the mapping system very easily, there should be no issue using the mapping system to pre-define defaults and active_on states and others... then we can focus on callback info and the like for setup systems?
	##
	##
	##
	## self is this Accessor
	## _parent is the class the function is assigned to, this can also be self...
	## _id is the quick / simple identifier for the function... Something simple such as get, getstr, getlen, getlenstr, etc... Simle designations... This will be converted to Enumeration Soon.
	## _name is the function name which will be used...
	## _func is the default function to provide - if the _id is recognized as callable under Setup, then we use that instead of creating more args...
	##
	def RegisterAccessor( self, _parent, _id, _func, _is_callback = False ):
		## The numerical ID in string form of the enumerator
		_enum_id		= _id;

		## print( '\tenum_value:\t\t' + str( _enum_id ) );
		## print( '\tid:\t\t\t\t' + str( _id ) );
		## print( '\tMAP_ACCESSORFUNC_IDS:\t\t\t\t' + str( MAP_ACCESSORFUNC_IDS ) );

		## The text identifier such as get, get_raw, etc... backwards compatibility until replaced...
		_id				= MAP_ACCESSORFUNC_IDS[ _enum_id ];

		## ID Override - All Instance Functions are lopped in as a single item...
		_id				= ( _id, 'instance' )[ _id.startswith( 'instance_' ) ];

		##
		_setup			= self.Setup( _id );
		_is_callback	= ( _is_callback or _id.startswith( 'on_' ) or _id.startswith( 'pre_' ) );

		##
		if ( _setup == False ):
			## Only show the text if this isn't a callback
			## if ( not _is_callback ):
			## print( '[ Acecool Library -> AccessorFuncBase -> RegisterAccessor ] Setup Instructions are preventing this Accessor from being added.. ' + String.FormatColumn( 5, 'id:', 50, _id, 10, 'Name:', 100, _func_call ) );

			return False;

		##
		if ( callable( _setup ) ):
			## print( '[ Acecool Library -> AccessorFuncBase -> RegisterAccessor ] Setup Instructions are overriding this Accessor function entirely.. ' + String.FormatColumn( 5, 'id:', 50, _id, 10, 'Name:', 100, _name ) );
			_func = _setup;


		##
		## Preconfigure the namine system so we fill out all of the blanks, but we keep name set to something like {alias_name_option} or something that won't be replaced, until we get into the function...
		##

		## The dynamic values of the function name, etc.. such as Get and Name, etc..
		_func_vars = {
			'get':				self.GetterPrefix( ),
			'name':				'{name_alias_system}',
			'key':				self.Key( ),
			'key_data':			self.DataKey( ),
			'key_accessor':		self.AccessorKey( ),
		};


		## print( 'RegisterAccessor _func_vars: ', _func_vars );

		## The Arguments String
		_func_args		= MAP_ACCESSORFUNC_ID_FUNC_ARGS[ _enum_id ];

		## The function name without this/self. or parens and args
		_func_name		= String.ExpandVariables( MAP_ACCESSORFUNC_ID_FUNC_NAMES[ _enum_id ], MAP_ACCESSORFUNC_KEYS_TO_WORDS, _func_vars );

		## The function parent usage... this for instance, self for the property / accessor, ie AccessorFuncBase object...
		_func_this		= String.ExpandVariables( MAP_ACCESSORFUNC_ID_FUNC_CALL_INSTANCE[ _enum_id ], MAP_ACCESSORFUNC_KEYS_TO_WORDS, _func_vars );

		## These vars are to assemble the final product - the callable function name for the current function being registered...
		_func_vars = {
			## Word Parent is self or this to reflect part of the property or the instance / class the property is added to
			'word_parent':	_func_this,

			## Map Name is the raw name without self/this. and args / parens.
			'map_name':		_func_name,
			'map_args':		_func_args,

			## Argument / Parenthesis Spaces - left always has 1, and if there are arguments then the right has one but if there are no args then there is no extra space which complies with my coding standard.
			'arg_spacer_l':	' ',
			'arg_spacer_r':	( '', ' ' )[ len( _func_args ) > 0 ],
		};

		## The function name as it would be used to be called...
		_func_call		= String.ExpandVariables( '{word_parent}.{map_name}({arg_spacer_l}{map_args}{arg_spacer_r});', MAP_ACCESSORFUNC_KEYS_TO_WORDS, _func_vars );


		## ## _func.__name__ = _name;
		## _func.__name__ =_func_name;
		## ## setattr( _func, '__name__', _name );
		## setattr( _parent, _func.__name__, _func );

		## ## Task: Create replacement system for this - I'd prefer to not manually add this, although I shouldn't need to.. I could be able to register it using the setup table, and defaults, using the new system...
		## ## ## Automatically add the callbacks...
		## if ( not _id == 'instance' and not _is_callback ):
		## 	_setup = self.Setup( 'on_' + _id );
		## 	if ( _setup != False ):
		## 		_func.__name__ = 'On' + _func_name;
		## 		setattr( _parent, _func.__name__, _setup );

		## 	_setup = self.Setup( 'pre_' + _id );
		## 	if ( _setup != False ):
		## 		_func.__name__ = 'Pre' + _func_name;
		## 		setattr( _parent, _func.__name__, _setup );

		## ##
		## if ( Acecool.GetSetting( 'output_accessorfunc_register_info', False ) ):
		## 	##
		## 	print( '\nRegisterAccessor -> RegisterNamedAccessor id: ' + Text.FormatColumn( 30, _id, 10, ' -> ', 20, self.Name( ), 10, ' -> ', 6, 'class ', 20, self.ClassName( ) + '( ... ):' ) + ' -> Getter Prefix: ' + self.GetterPrefix( ) );

		##
		## Register AccessorFunc Names
		##
		## @arg: _alias: This is the name, such as Blah
		## @arg: _name: This is the Is{name_alias_system}Set or so - everything is filled out except for the name...
		## @arg: _call: This is basically this.{name_alias_system}( _args_list ); - everything is filled out except for the name...
		##
		def RegisterNamedAccessor( _enum_id, _id, _alias, _prep_name, _prep_call ):
			##
			_this_name			= self.Name( );
			_usable_name		= String.ExpandVariables( _prep_name, { 'name_alias_system': _alias } );
			_usable_call		= String.ExpandVariables( _prep_call, { 'name_alias_system': _alias } );

			##
			## if ( Acecool.GetSetting( 'output_accessorfunc_register_info', False ) ):
			##
			_is_name_original	= ( _this_name == _alias or _usable_name == _this_name );
			_is_name_instance	= ( _id.startswith( 'instance' ) );
			_is_name_alias		= ( not _is_name_instance and not _is_name_original );
			_func_type			= ( ( 'Alias', 'Instance' )[ _is_name_instance ], 'Original' )[ _is_name_original ];

				## ##
				## ## ( '\n' if _is_name_original else '' ) +

				## ##
				## ## print( 'RegisterAccessor -> RegisterNamedAccessor for class: [ ' + Text.FormatColumn( 20, self.ClassName( ) ) + ' for: ' + Text.FormatColumn( 20, _this_name ) + ' ] -> ' + Text.FormatColumn( 20, _func_type, 5, '  |', 50, _usable_name, 5, '  |', 100, _usable_call ) );

				## ##
				## if ( _func_type == 'Alias' and Acecool.GetSetting( 'output_accessorfunc_register_info_alias', False ) or _func_type == 'Instance' and Acecool.GetSetting( 'output_accessorfunc_register_info_instance', False ) or _func_type == 'Original' and Acecool.GetSetting( 'output_accessorfunc_register_info_original', False ) ):
				## 	print( '\t' + Text.FormatColumn( 20, 'Name: ' + _func_type, 5, '  |', 50, _usable_name, 5, '  |', 100, _usable_call ) );


			## Old
			## _func.__name__ = _name;
			## setattr( _func, '__name__', _name );

			## Register the function...
			self.RegisterFunctionNotify( _parent, _func_type, _usable_name, _usable_call );
			self.RegisterFunction( _parent, _usable_name, _func );

			## ##
			## if ( not getattr( _parent, 'accessor_func_db', None ) ):
			## 	##
			## 	## I'm considering... Maybe I should make it so everything redirects to a single table? Instead of having to manage multiple tables... So keys / alias keys link to acccessors.. Names, call names, etc... not sure.. maybe.. And the Accessors table should have all of the data or?
			## 	##
			## 	setattr( _parent, 'accessor_func_db', {
			## 		## Note: Groups don't need to store everything... Store by group name... When I add something, I add by name, key, or whatever... That adds a single reference instead of by name / key whatever so they're all the same...
			## 		## Note: Figure out if I want to create alias functions or properties such as self.All = 1234.56; to assign all in All to 1234.56...
			## 		'groups': {
			## 			## Default group - this is for EVERYTHING...
			## 			'*': [ ]

			## 			## 4 28 44 85
			## 			## 'All': {
			## 			##		'Pitch': True,
			## 			##		'Yaw': True,
			## 			##		'Roll': True,
			## 			## },
			## 		},

			## 		## a list of keys which also link to the accessor func, property, names?
			## 		'keys': {
			## 			## 'roll': { },
			## 			## 'r': { },
			## 		},

			## 		## a list of names which also link to the accessor func, property, keys? These should be pre-names and aliases..
			## 		'names': {
			## 			## 'Roll': { },
			## 			## 'R': { },
			## 		},

			## 		## a list of names which also link to the accessor func, property, keys? These should be the complete function name without args... args can be added so they can be printed... but... yeah..
			## 		'call_names': {
			## 			## 'GetRollAccessor': '',
			## 			## 'GetRollAllowedTypes': '',
			## 			## 'GetRoll': ''
			## 		},

			## 		## Accessors list is a simple list of all accessor objects
			## 		'accessors': [

			## 		]

			## 		##
			## 		## '': [],
			## 		## '': [],
			## 		## '': []
			## 	}
			## );


			##
			_accessor_db		= _parent.accessor_func_db;
			_db_groups			= _parent.accessor_func_db.get( 'groups', { } );
			_db_names			= _parent.accessor_func_db.get( 'names', { } );
			_db_func_names		= _parent.accessor_func_db.get( 'call_names', { } );
			_db_keys			= _parent.accessor_func_db.get( 'keys', { } );
			## _db_references		= _parent.accessor_func_db.get( 'references', { } );
			_db					= _parent.accessor_func_db.get( 'groups', { } );
			_db_groups_all		= _db_groups.get( '*', { } );

			## _ident = {
			## 	'accessor':		self,
			## 	'args':			_func_args,
			## 	'call_name':	_usable_call
			## };

			## _db_names[ self.Name( ) ] = self;
			## _db_func_names[ _usable_name ] = self.Name( );
			if ( not _db_func_names.get( self.Name( ), None ) ):
				_db_func_names[ self.Name( ) ] = { };

			_db_func_names[ self.Name( ) ][ _usable_name ] = {
				'is_original':	_is_name_original,
				'is_instance':	_is_name_instance,
				'is_alias':		_is_name_alias,

				'args':			_func_args,

			};
			_db_groups_all[ self.Name( ) ] = True;
			## _db_groups_all.append( self );



			## _db_names[ self.Key( ) ] = self;
			## _db_func_names[ _usable_name ] = self.Key( );
			if ( not _db_keys.get( self.Key( ), None ) ):
				_db_keys[ self.Key( ) ] = { };

			_db_keys[ self.Key( ) ][ self.Key( ) ] = {
				'is_original':	_is_name_original,
				'is_instance':	_is_name_instance,
				'is_alias':		_is_name_alias,

				## 'args':			_func_args,

			};
			## _db_keys[ self.Key( ) ] = [ ];
			## _db_keys[ self.Key( ) ] = self;
			if ( self.__keys ):
				for _key in self.__keys:
					## _db_keys[ _key ] = self;
					## _db_keys[ self.Key( ) ].append( _key );
					_db_keys[ self.Key( ) ][_key ] = {
						'is_original':	_is_name_original,
						'is_instance':	_is_name_instance,
						'is_alias':		_is_name_alias,

						## 'args':			_func_args,

					};


			## _db_groups_all[ self.Name( ) ] = True;
			## _db_groups_all.append( self );

			## _db_keys[ self.Key( ) ] = [ ];
			## ## _db_keys[ self.Key( ) ] = self;
			## if ( self.__keys ):
			## 	for _key in self.__keys:
			## 		## _db_keys[ _key ] = self;
			## 		_db_keys[ self.Key( ) ].append( _key );

			## _db_names[ self.Name( ) ] = [ ];
			## ## _db_names[ self.Name( ) ] = self;
			## if ( self.__names ):
			## 	for _name in self.__names:
			## 		## _db_names[ _name ] = self;
			## 		_db_names[ self.Name( ) ].append( _name );


			## Newest old version
			## _func.__name__ =_usable_name;
			## setattr( _parent, _func.__name__, _func );

			## Task: Create replacement system for this - I'd prefer to not manually add this, although I shouldn't need to.. I could be able to register it using the setup table, and defaults, using the new system...
			## ## Automatically add the callbacks...
			if ( not _id == 'instance' and not _is_callback ):
				_setup = self.Setup( 'on_' + _id );
				if ( _setup != False ):
					self.RegisterFunctionNotify( _parent, _func_type, _usable_name, _usable_call );
					self.RegisterFunction( _parent, 'On' + _usable_name, _setup );
					_db_groups_all[ 'On' + _usable_name ] = self;
					## _func.__name__ = 'On' + _usable_name;
					## setattr( _parent, _func.__name__, _setup );



				_setup = self.Setup( 'pre_' + _id );
				if ( _setup != False ):
					self.RegisterFunctionNotify( _parent, _func_type, _usable_name, _usable_call );
					self.RegisterFunction( _parent, 'Pre' + _usable_name, _setup );
					_db_groups_all[ 'Pre' + _usable_name ] = self;
					## _func.__name__ = 'Pre' + _usable_name;
					## setattr( _parent, _func.__name__, _setup );




		##
		_is_instance_helper = ( _func_name.find( '{' ) > -1 );
		_actual_name 		= ( _func_name, self.Name( ) )[ _is_instance_helper ];

		## Register the primary function name - For the output, if we have unprocessed {x} tags, then we use self.Name( ) as the replacer.. Otherwise, if no {x} tags are left, then it is a nameless / instance helper which doesn't have a unique name for each registered name...
		## print( 'RegisterAccessor -> Registering Primary Name: ' + _get_actual_name );
		RegisterNamedAccessor( _enum_id, _id, _actual_name, _func_name, _func_call );

		## Grab the list of Alias Names
		## _names = self.GetNameAliases( );
		_names = self.__names;
		## _names.append( self.Name( ) );
		##
		if ( _names != None and len( _names ) > 0 ):
			for _name in _names:
				RegisterNamedAccessor( _enum_id, _id, _name, _func_name, _func_call );










		## {
		##    'groups':{
		##       '*':{
		##          '':True,
		##          'Serialize':True,
		##          'X':True,
		##          'Y':True,
		##          'Z':True,
		##          'Pitch':True,
		##          'Yaw':True,
		##          'Roll':True,
		##          'Normalize':True,
		##          'Normalized':True
		##       }
		##    },
		##    'keys':{
		##       'this_object':{
		##          'this_object':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False
		##          }
		##       },
		##       'serialize':{
		##          'serialize':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False
		##          }
		##       },
		##       'x':{
		##          'x':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False
		##          }
		##       },
		##       'y':{
		##          'y':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True
		##          },
		##          'yaw':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True
		##          }
		##       },
		##       'z':{
		##          'z':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False
		##          }
		##       },
		##       'p':{
		##          'p':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True
		##          },
		##          'pitch':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True
		##          }
		##       },
		##       'r':{
		##          'r':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True
		##          },
		##          'roll':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True
		##          }
		##       },
		##       'normalize':{
		##          'normalize':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False
		##          }
		##       },
		##       'normalized':{
		##          'normalized':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False
		##          }
		##       }
		##    },
		##    'names':{

		##    },
		##    'call_names':{
		##       '':{
		##          'Get':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_str'
		##          },
		##          'Set':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          }
		##       },
		##       'Serialize':{
		##          'Serialize':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'SerializeToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_str'
		##          },
		##          'SerializeAccessor':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          }
		##       },
		##       'X':{
		##          'GetX':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetXToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_str'
		##          },
		##          'GetXAccessor':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'SetX':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          }
		##       },
		##       'Y':{
		##          'GetY':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetYToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_str'
		##          },
		##          'GetYAccessor':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'SetY':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          }
		##       },
		##       'Z':{
		##          'GetZ':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetZRaw':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'IsZSet':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_str'
		##          },
		##          'GetZLen':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetZLenToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetZDefaultValue':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_default_value'
		##          },
		##          'GetZProperty':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZAccessor':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZAllowedTypes':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZAllowedValues':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZBaseAllowedTypes':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZBaseAllowedValues':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'HasZGetterPrefix':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZGetterPrefix':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZName':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZAccessorKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZDataKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'SetZ':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          },
		##          'AddZ':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          },
		##          'ResetZ':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZHelpers':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZOutput':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetZGetterOutput':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          }
		##       },
		##       'Pitch':{
		##          'GetPitch':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetP':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetPitchRaw':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPRaw':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'IsPitchSet':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'IsPSet':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_str'
		##          },
		##          'GetPToString':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'get_str'
		##          },
		##          'GetPitchLen':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetPLen':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetPitchLenToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetPLenToString':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetPitchDefaultValue':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_default_value'
		##          },
		##          'GetPDefaultValue':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'get_default_value'
		##          },
		##          'GetPitchProperty':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPProperty':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchAccessor':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPAccessor':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchAllowedTypes':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPAllowedTypes':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchAllowedValues':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPAllowedValues':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchBaseAllowedTypes':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPBaseAllowedTypes':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchBaseAllowedValues':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPBaseAllowedValues':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'HasPitchGetterPrefix':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'HasPGetterPrefix':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchGetterPrefix':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPGetterPrefix':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchName':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPName':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchAccessorKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPAccessorKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchDataKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPDataKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'SetPitch':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          },
		##          'SetP':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_value'
		##          },
		##          'AddPitch':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          },
		##          'AddP':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_value'
		##          },
		##          'ResetPitch':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'ResetP':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchHelpers':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPHelpers':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchOutput':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPOutput':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetPitchGetterOutput':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetPGetterOutput':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          }
		##       },
		##       'Yaw':{
		##          'GetYaw':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetY':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetYawRaw':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYRaw':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'IsYawSet':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'IsYSet':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_str'
		##          },
		##          'GetYToString':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'get_str'
		##          },
		##          'GetYawLen':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetYLen':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetYawLenToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetYLenToString':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetYawDefaultValue':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_default_value'
		##          },
		##          'GetYDefaultValue':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'get_default_value'
		##          },
		##          'GetYawProperty':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYProperty':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawAccessor':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYAccessor':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawAllowedTypes':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYAllowedTypes':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawAllowedValues':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYAllowedValues':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawBaseAllowedTypes':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYBaseAllowedTypes':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawBaseAllowedValues':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYBaseAllowedValues':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'HasYawGetterPrefix':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'HasYGetterPrefix':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawGetterPrefix':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYGetterPrefix':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawName':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYName':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawAccessorKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYAccessorKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawDataKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYDataKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'SetYaw':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          },
		##          'SetY':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_value'
		##          },
		##          'AddYaw':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          },
		##          'AddY':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_value'
		##          },
		##          'ResetYaw':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'ResetY':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawHelpers':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYHelpers':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawOutput':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYOutput':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetYawGetterOutput':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetYGetterOutput':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          }
		##       },
		##       'Roll':{
		##          'GetRoll':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetR':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetRollRaw':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRRaw':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'IsRollSet':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'IsRSet':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_str'
		##          },
		##          'GetRToString':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'get_str'
		##          },
		##          'GetRollLen':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetRLen':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetRollLenToString':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetRLenToString':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          },
		##          'GetRollDefaultValue':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'get_default_value'
		##          },
		##          'GetRDefaultValue':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'get_default_value'
		##          },
		##          'GetRollProperty':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRProperty':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollAccessor':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRAccessor':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollAllowedTypes':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRAllowedTypes':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollAllowedValues':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRAllowedValues':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollBaseAllowedTypes':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRBaseAllowedTypes':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollBaseAllowedValues':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRBaseAllowedValues':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'HasRollGetterPrefix':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'HasRGetterPrefix':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollGetterPrefix':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRGetterPrefix':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollName':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRName':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollAccessorKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRAccessorKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollDataKey':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRDataKey':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'SetRoll':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          },
		##          'SetR':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_value'
		##          },
		##          'AddRoll':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_value'
		##          },
		##          'AddR':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':'_value'
		##          },
		##          'ResetRoll':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'ResetR':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollHelpers':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRHelpers':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollOutput':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetROutput':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          },
		##          'GetRollGetterOutput':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':''
		##          },
		##          'GetRGetterOutput':{
		##             'is_original':False,
		##             'is_instance':False,
		##             'is_alias':True,
		##             'args':''
		##          }
		##       },
		##       'Normalize':{
		##          'Normalize':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          }
		##       },
		##       'Normalized':{
		##          'GetNormalized':{
		##             'is_original':True,
		##             'is_instance':False,
		##             'is_alias':False,
		##             'args':'_default_override = None, _ignore_defaults = False'
		##          }
		##       }
		##    }
		## }
























	##
	##
	##
	def RegisterFunction( self, _parent, _name, _func ):
		_func.__name__ =_name;
		setattr( _parent, _func.__name__, _func );


	##
	##
	##
	def RegisterFunctionNotify( self, _parent, _func_type, _usable_name, _usable_call ):
		if ( not getattr( _parent, _usable_name, None ) ):
			##
			if ( Acecool.GetSetting( 'output_accessorfunc_register_info', False ) ):
				##
				if ( _func_type == 'Alias' and Acecool.GetSetting( 'output_accessorfunc_register_info_alias', False ) or _func_type == 'Instance' and Acecool.GetSetting( 'output_accessorfunc_register_info_instance', False ) or _func_type == 'Original' and Acecool.GetSetting( 'output_accessorfunc_register_info_original', False ) ):
					print( '\t' + Text.FormatColumn( 20, 'Name: ' + _func_type, 5, '  |', 50, _usable_name, 5, '  |', 100, _usable_call ) );
		## else:
		## 	print( '\tError trying to register function - ALREADY REGISTERED: ' + Text.FormatColumn( 20, 'Name: ' + _func_type, 5, '  |', 50, _usable_name, 5, '  |', 100, _usable_call ) );


		## ##
		## if ( Acecool.GetSetting( 'output_accessorfunc_register_info', False ) ):
		## 	##
		## 	print( 'RegisterAccessor -> RegisterXAccessor for class: [ ' + Text.FormatColumn( 20, self.ClassName( ) ) );
		## 	## print( '\t' + Text.FormatColumn( 20, _func_type, 5, '  |', 50, _usable_name, 5, '  |', 100, _usable_call ) );


		## ##
		## for _i in range( 1 + len( _names ) ):
		## 	## We use index - 1 as the index so it'll be -1 through len( _names ) - 1;
		## 	## _index = _i - 1;

		## 	## This grabs the name, but it won't allow it to go below 0 or out of bounds...
		## 	## _name = _names[ Math.Clamp( _index, 0, len( _names ) - 1 ) ];
		## 	_name = _names[ _i ];

		## 	## ## If we're at the first, we add the primary name... If we aren't, name is already set...
		## 	## if ( _i == 0 ):
		## 	## 	_name = self.Name( );

		## _usable_name
		## 	##
		## 	print( 'RegisterNamedAccessor -> Alias Solutions - Pre: ' + _func_name + ' || ' + _func_call );

		## 	##
		## 	_func_name = String.ExpandVariables( _func_name, { 'name_alias_system': _name } );
		## 	_func_call = String.ExpandVariables( _func_call, { 'name_alias_system': _name } );

		## 	##
		## 	print( 'RegisterNamedAccessor -> Alias Solutions: ' + _func_name + ' || ' + _func_call );

		## 	## _func.__name__ = _name;
		## 	_func.__name__ =_func_name;
		## 	## setattr( _func, '__name__', _name );
		## 	setattr( _parent, _func.__name__, _func );

		## 	## Task: Create replacement system for this - I'd prefer to not manually add this, although I shouldn't need to.. I could be able to register it using the setup table, and defaults, using the new system...
		## 	## ## Automatically add the callbacks...
		## 	if ( not _id == 'instance' and not _is_callback ):
		## 		_setup = self.Setup( 'on_' + _id );
		## 		if ( _setup != False ):
		## 			_func.__name__ = 'On' + _func_name;
		## 			setattr( _parent, _func.__name__, _setup );

		## 		_setup = self.Setup( 'pre_' + _id );
		## 		if ( _setup != False ):
		## 			_func.__name__ = 'Pre' + _func_name;
		## 			setattr( _parent, _func.__name__, _setup );


	## ##
	## ##
	## ##
	## class AccessorFuncBase( AccessorFuncCore ):
	## 	##
	## 	## Helpers
	## 	##
	## 	__name__ = 'AccessorFuncBase';


	## 	##
	## 	__name_aliases				= AccessorFuncCoreBase( parent = AccessorFuncCoreBase,		key = 'name_aliases',		keys = None,	name = 'NameAliases',		names = ( ),	default = '0.0.0',						getter_prefix = 'Get',		documentation = 'Version Value',		allowed_types = ( TYPE_ANY ),		allowed_values = ( VALUE_ANY ),					setup = {
	## 		'get': lambda this: this.__names,
	## 		'set': lambda this, _value: setattr( this, '__names', _value )
	## 	} );
	## 	__key_aliases				= AccessorFuncCoreBase( parent = AccessorFuncCoreBase,		key = 'key_aliases',		keys = None,	name = 'KeyAliases',		names = ( ),	default = '0.0.0',						getter_prefix = 'Get',		documentation = 'Version Value',		allowed_types = ( TYPE_ANY ),		allowed_values = ( VALUE_ANY ),					setup = {
	## 		'get': lambda this: this.__keys,
	## 		'set': lambda this, _value: setattr( this, '__keys', _value )
	## 	} );


	## 	##
	## 	##
	## 	##
	## 	def __init__( self, **_varargs ):
	## 		super( ).__init__( **_varargs );

	## 	## 																																																																																								( lambda this, _version, _minor, _revision: this.__init__( _version, _minor, _revision ) )
	## 	## __name_aliases				= AccessorFuncCoreBase( parent = AccessorFuncCoreBase,		key = 'name_aliases',						name = 'NameAliases',						default = [ ],			group = '',		getter_prefix = 'Get',		documentation = 'Name Aliases',			allowed_types = TYPE_ANY,			allowed_values = VALUE_ANY,		setup = { 'get': ( lambda this: this.__name_aliases ), 'set': ( lambda this, _value: this.__name_aliases = _value ) }		);
	## 	## __key_aliases				= AccessorFuncCoreBase( parent = AccessorFuncCoreBase,		key = 'key_aliases',						name = 'KeyAliases',						default = [ ],			group = '',		getter_prefix = 'Get',		documentation = 'Key Aliases',			allowed_types = TYPE_ANY,			allowed_values = VALUE_ANY,		setup = { 'get': ( lambda this: this.__key_aliases ), 'set': ( lambda this, _value: this.__key_aliases = _value ) }		);
	## 	## __OutputViewID			= AccessorFuncCoreBase( parent = AccessorFuncCoreBase,		key = 'OutputViewID',						name = 'OutputViewID',			default = None,				group = '',			getter_prefix = 'Get',		documentation = 'OutputViewID',			allowed_types = ( TYPE_INTEGER ),	allowed_values = VALUE_ANY													);



	## 		## ## For each key alias, we simply
	## 		## if ( _names != None and IsTable( _names ) ):
	## 		## 	## For each alias, set up the raw / alias property for _<alias> so it is redirected to _<key>, also set up the standard property for the <alias> - Determine how to set up __<alias> without having the private system kick in...
	## 		## 	for _name in _names:
	## 		## 		RegisterNamedAccessor( _name, _func_name, _func_call );

	## 		##
	## 		## print( 'RegisterAccessor:\t\t\t' + _func_call )




	## 			## self.RegisterAccessor( _parent, 'on_' + _id, _func, True );
	## 			## self.RegisterAccessor( _parent, 'pre_' + _id, _func, True );



	## 		## print( 'RegisterAccessor called' );
	## 		## print( '\tenum_value:\t\t' + str( _enum_id ) );
	## 		## print( '\tid:\t\t\t\t' + _id );
	## 		## print( '\tfunc_raw:\t\t' + MAP_ACCESSORFUNC_ID_FUNC_NAMES[ _enum_id ] );

	## 		## print( '\tself/this:\t\t' + str( _func_this ) );
	## 		## print( '\tname:\t\t\t' + _func_name );
	## 		## print( '\targs:\t\t\t' + _func_args );
	## 		## print( '\tcall:\t\t\t' + _func_call );
	## 		## print( );


	## 		## def Setup( _id, _func = None ):
	## 		## 	print( 'Setup called' );
	## 		## 	print( '\tenum_value:\t\t' + str( _id ) );
	## 		## 	print( '\tid:\t\t\t\t' + str( MAP_ACCESSORFUNC_IDS[ _id ] ) );
	## 		## 	print( '\tfunc_raw:\t\t\t' + str( MAP_ACCESSORFUNC_ID_FUNC_NAMES[ _id ] ) );


	## 		## 	_extras = {
	## 		## 		'get':		'Get',
	## 		## 		'name':		'Blah',
	## 		## 	};

	## 		## 	_func_args = MAP_ACCESSORFUNC_ID_FUNC_ARGS[ _id ];
	## 		## 	_func_name = String.ExpandVariables( MAP_ACCESSORFUNC_ID_FUNC_NAMES[ _id ], MAP_ACCESSORFUNC_KEYS_TO_WORDS, _extras );
	## 		## 	_func_this = String.ExpandVariables( MAP_ACCESSORFUNC_ID_FUNC_CALL_INSTANCE[ _id ], MAP_ACCESSORFUNC_KEYS_TO_WORDS, _extras );

	## 		## 	_extras = {
	## 		## 		## Word Parent is self or this to reflect part of the property or the instance / class the property is added to
	## 		## 		'word_parent':	_func_this,

	## 		## 		## Map Name is the raw name without self/this. and args / parens.
	## 		## 		'map_name':		_func_name,
	## 		## 		'map_args':		_func_args,

	## 		## 		## Argument / Parenthesis Spaces - left always has 1, and if there are arguments then the right has one but if there are no args then there is no extra space which complies with my coding standard.
	## 		## 		'arg_spacer_l':	' ',
	## 		## 		'arg_spacer_r':	( '', ' ' )[ len( _func_args ) > 0 ],
	## 		## 	};
	## 		## 	_func_call = String.ExpandVariables( '{word_parent}.{map_name}({arg_spacer_l}{map_args}{arg_spacer_r});', MAP_ACCESSORFUNC_KEYS_TO_WORDS, _extras );

	## 		## 	print( '\tself/this:\t\t' + str( _func_this ) );
	## 		## 	print( '\tname:\t\t\t' + _func_name );
	## 		## 	print( '\targs:\t\t\t' + _func_args );
	## 		## 	print( '\tcall:\t\t\t' + _func_call );

	## 		## 	print( );







class AccessorFuncBase( AccessorFuncCoreBase ):
	##
	## Helpers
	##
	__name__ = 'AccessorFuncBase';



	##  Jote froii7hnAs part of
	__name_alases				= AccessorFuncCoreBase( parent = AccessorFuncCoreBase,		key = 'name_aliases',		keys = None,	name = 'NameAliases',		names = ( ),	default = '0.0.0',						getter_prefix = 'Get',		documentation = 'Version Value',		allowed_types = ( TYPE_ANY ),		allowed_values = ( VALUE_ANY ),					setup = {
		## 'instance': True,
		'get': lambda this: this.__names,
		'set': lambda this, _value: setattr( this, '__names', _value )
	} );
	__key_aliases				= AccessorFuncCoreBase( parent = AccessorFuncCoreBase,		key = 'key_aliases',		keys = None,	name = 'KeyAliases',		names = ( ),	default = '0.0.0',						getter_prefix = 'Get',		documentation = 'Version Value',		allowed_types = ( TYPE_ANY ),		allowed_values = ( VALUE_ANY ),					setup = {
		'get': lambda this: this.__keys,
		'set': lambda this, _value: setattr( this, '__keys', _value )
	} );













##
## This example is to show off all, or most of the features of the AccessorFunc system which aren't necessarily able to jne
##
##
## Tasl: AcecoolFuncExample: Things to exmplore: Data-Type specialization, auto functions based on data-type ro value typing...
##
##
##
##
##
##
##
##
##
##
##
class AccessorFuncExample( ):
	##
	##
	##

	##
	__name__ = 'AccessorFuncExample';


	##
	##
	##
	def __example__( ):
		##
		_text = '';

		##
		_text += 'AccessorFuncExample - Pending...';


		##
		return _text;



































































































































































































































































































































































































## Generate ENUMeration for Version Octet Modes
VERSION_OCTET_RESET_NEVER, VERSION_OCTET_RESET_ON_ANY_CHANGE, VERSION_OCTET_RESET_ON_LESS_THAN_CHANGE, VERSION_OCTET_RESET_ON_GREATER_THAN_CHANGE, ENUM_LIST_VERSION_OCTET_RESET = ENUM( 4 );

## Generate ENUMeration for Version Octents - Order them so they can be compared numerically with MAJOR the highest value, etc..
VERSION_OCTET_PATCH, VERSION_OCTET_MINOR, VERSION_OCTET_MAJOR, ENUM_LIST_VERSION_OCTES = ENUM( 3 );


##
## Versioning System - Stores a version in Major.Minor.Revision format and to compare version each octet is converted into 10 ** len_of_previous_octets + buffer ( essentially removing the decimals ) so the values can be compared and this works quite well...
## Note: I updated this to change the way _version is used... Instead of the old method which uses SetVersion( String, None, None ) or String, String, String or Number, Number, Number and setting an actual version value and updating the major / minor / revision numbers, I instead only maintain the major / minor / revision values and use Getters for the Getversion to output a string / serialized piece of info... I also use __init__ to handle updating the data so technically there is less code involved in the newer method. I have left the old method to show.. so on_set called UnPack which converted the string GetVersion into numbers so any time SetVersion was called, it woul dbe called to update the 3 other functions.. -- I left both to show some of the ways the AccessorFunc system can be used with its callbacks..
##
## Rules: If X.0.0 is set, 0.X.X are assigned 0. If 0.X.0 is set, 0.0.X is assigned 0 - As for multiplying versions together, adding, etc... I will likely drastically change the behavior...
##
class VersionBase:
	pass
class Version( VersionBase ):
	##
	##
	##
	__name__ = 'Version';


	##
	##
	##

	## The version we use for text, can use self.GetVersion( ) or simply self.Get( ) fr Version.Get( ), Version.Set( ), Version.IsSet( ), etc.. which is a lot more intuitive...
	__version					= AccessorFuncBase( parent = VersionBase,	key = 'version',		keys = [ ],																									name = 'Version',			names = [ '' ],																	default = '0.0.0',									getter_prefix = 'Get',			documentation = 'Versioning Blank / Base',		allowed_types = ( VALUE_ANY ),					allowed_values = ( VALUE_ANY ),					setup_override = { 'get': ( lambda this: this.GetToString( ) ), 'get_str': ( lambda this: this.GetMajorToString( ) + '.' + this.GetMinorToString( ) + '.' + this.GetPatchToString( ) ), 'set': ( lambda this, _version, _minor, _revision = 0: this.__init__( _version, _minor, _revision ) ) }															);

	## Actual stored values... Major.Minor.Patch
	#( lambda this, _value, _new, _is_valid: this.ResetAccessors( 'Minor', 'Patch' ) if ( this.GetOctetReset( ) != VERSION_OCTET_RESET_NEVER and ( _is_valid and _new != None and _value != None and ( ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_ANY_CHANGE and _new != _value ) or ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_LESS_THAN_CHANGE and _new < _value ) or ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_GREATER_THAN_CHANGE and _new > _value ) ) ) ) else '' ) } );
	__major						= AccessorFuncBase( parent = VersionBase,	key = 'major',			keys = [ 'version_major',			'main',		'primary' ],												name = 'Major',				names = [ 'VersionMajor',		'Primary',		'Main' ],						default = 0,										getter_prefix = 'Get',			documentation = 'X.0.0',						allowed_types = ( TYPE_INTEGER ),				allowed_values = ( VALUE_ANY ),					setup_override = { 'on_set': ( lambda this, _value, _new, _is_valid: this.HandleResetOctet( VERSION_OCTET_MAJOR, _value, _new, _is_valid ) ) } );
	#( lambda this, _value, _new, _is_valid: this.ResetAccessors( 'Patch' ) if ( this.GetOctetReset( ) != VERSION_OCTET_RESET_NEVER and ( _is_valid and _new != None and _value != None and ( ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_ANY_CHANGE and _new != _value ) or ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_LESS_THAN_CHANGE and _new < _value ) or ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_GREATER_THAN_CHANGE and _new > _value ) ) ) ) else '' ) } );'' ) } );
	__minor						= AccessorFuncBase( parent = VersionBase,	key = 'minor',			keys = [ 'version_minor',			'sub',		'secondary' ],												name = 'Minor',				names = [ 'VersionMinor',		'Secondary',	'Sub' ],						default = 0,										getter_prefix = 'Get',			documentation = '0.X.0',						allowed_types = ( TYPE_INTEGER ),				allowed_values = ( VALUE_ANY ),					setup_override = { 'on_set': ( lambda this, _value, _new, _is_valid: this.HandleResetOctet( VERSION_OCTET_MINOR, _value, _new, _is_valid ) ) } );
	__patch						= AccessorFuncBase( parent = VersionBase,	key = 'patch',			keys = [ 'version_revision',		'build',	'revision' ],												name = 'Patch',				names = [ 'VersionPatch',		'Revision',		'VersionRevision' ],			default = 0,										getter_prefix = 'Get',			documentation = '0.0.X',						allowed_types = ( TYPE_INTEGER ),				allowed_values = ( VALUE_ANY )		);

	## When do we revert lower level octets? Ie, do we reset ever, do we reset only if the value is larger than the previous, do we reset if it is different, do we only reset if lower?
	__octet_mode				= AccessorFuncBase( parent = VersionBase,	key = 'octet_mode',		keys = [ 'version_octet_reset_mode',	'octet_reset_mode', 'version_octet_mode',	'octet_mode' ],			name = 'OctetResetMode',	names = [ 'VersionOctetMode', 'OctetMode', 'VersionOctetResetMode' ],			default = VERSION_OCTET_RESET_ON_ANY_CHANGE,		getter_prefix = 'Get',			documentation = 'Define when lower level octets are reverted to 0... On any value change from a higher octet, if the value is changed to something greater than, if it is changed to something less, or never',		allowed_types = ( TYPE_INTEGER ),				allowed_values = ( VERSION_OCTET_RESET_NEVER, VERSION_OCTET_RESET_ON_ANY_CHANGE, VERSION_OCTET_RESET_ON_LESS_THAN_CHANGE, VERSION_OCTET_RESET_ON_GREATER_THAN_CHANGE ) 		); #,					setup_override = { 'set_callback': ( lambda this, _value: this.Unpack( ) ) }																		);

	## When the version is converted to a string and / or number without decimals, this determines how many 0s are used in place of the defcijmp
	__buffer					= AccessorFuncBase( parent = VersionBase,	key = 'buffer',			keys = [ 'version_octet_buffer',	'octet_buffer' ],														name = 'OctetBuffer',		names = [ 'VersionOctetBuffer',	'Buffer' ],										default = 1 ,										getter_prefix = 'Get',			documentation = 'Version Octet Buffer Value',	allowed_types = ( TYPE_INTEGER ),				allowed_values = ( VALUE_ANY ), 		); #,					setup_override = { 'set_callback': ( lambda this, _value: this.Unpack( ) ) }																		);

	## To extend the system at some point...
	## __lang								= AccessorFuncBase( parent = VersionBase,	key = 'lang',																											name = 'InLanguage',		names = None,																	default = 'ACMS_LANG_UNKNOWN',						getter_prefix = '',				documentation = '',								allowed_types = ( TYPE_ANY ),					allowed_values = ( VALUE_ANY ),					setup = { 'get': ( lambda this: this.GetSetting( 'language_name', 'Default' ) ) }																		);


	##
	##
	##
	def HandleResetOctet( this, _octet, _value, _new, _is_valid ):
		##
		return
		## _can_reset = this.CanHandleResetOctet( _octet, _value, _new, _is_valid );

		##
		## if ( _octet == VERSION_OCTET_MAJOR and _can_reset ):
		## 	this.ResetAccessors( 'Minor', 'Patch' );

		## if ( _octet == VERSION_OCTET_MINOR and _can_reset ):
		## 	this.ResetAccessors( 'Patch' );


	##
	## Determine whether or not we can reset one or more lower octets based on OctetReset Mode, the current value compared to the new vaslue, if the value is actually able to be set ( is valid only ), etc.. This is easier in a larger function form as we can split it up, use helpers so less repeated code,
	##
	## def CanRunOctetResetMode( self, _value, _new, _is_valid ):
	def CanHandleResetOctet( this, _octet, _value, _new, _is_valid ):
		## Fetch the current mode...
		_mode = this.GetOctetResetMode( );

		## If we never reset the octets, then don't... If the value isn't value, then the data hasn't actually changed so we can ignore it here too...
		if ( _mode == VERSION_OCTET_ ): # ); #_NEVER or not _is_valid ):
			return False;

		## Grab the default values in case we need them...
		_default_major, _default_minor, _default_patch = this.GetMajorDefaultValue( ), this.GetMinorDefaultValue( ), this.GetPatchDefaultValue( );

		## If the new value is valid, and it is None - we check the octet to determine which default value we compare it to, then we compare it to the _value, if not None, to see whether or not it meets the requirements of the Octet Mode.. This may be easier to process by calling return self.CanResetOctet( self, _octet, _value, _default, True );
		## Note: We don't even need to check for _is_valid as all of these calls will be value change _is_valid because of our first statement kicking the call out if not valid... or if we never reset octets..
		## if ( _is_valid and _new == None ):
			## pass;

		## For None values, we look up the default value to see what it changes to...
		## or _new != None and _value != None

		## If we only allow the reset if a value is higher than, or less than, then we need to verify the values to make sure they aren't None...
		## If _new is None, then we need to determine how we handle this case... _value, if None, uses the default because that's the actual value it gets... So, if _new is None, then _new needs to become the default value and then we can compare the change based on their config..

		## If the Octet is Major, use the default major value...
		if ( _octet == VERSION_OCTET_MAJOR ):
			_value	= ( _value, _default_major )[ _value == None ];
			_new	= ( _new, _default_major )[ _new == None ];

		## If the Octet is Minor, use the default minor value...
		if ( _octet == VERSION_OCTET_MINOR ):
			_value	= ( _value, _default_minor )[ _value == None ];
			_new	= ( _new, _default_minor )[ _new == None ];

		## If the Octet is Patch, use the default patch value...
		if ( _octet == VERSION_OCTET_PATCH ):
			_value	= ( _value, _default_patch )[ _value == None ];
			_new	= ( _new, _default_patch )[ _new == None ];


		## If we're in any-change mode, then allow it, even if the values are reset to None...
		## Task: Compare the default values returned when None is used to the previous value to determine if we actually need to reset - because if they're the same, then technically there is no change either....
		if ( _mode == VERSION_OCTET_RESET_ON_ANY_CHANGE and _new != _value ):
			return True;

		## If we only allow the reset if a value is greater than, and the new value is greater than, then allow it
		if ( _mode == VERSION_OCTET_RESET_ON_GREATER_THAN_CHANGE and _new > _value ):
			return True;

		## If we only allow the reset if a value is less than, and the new value is less than, then allow it
		if ( _mode == VERSION_OCTET_RESET_ON_LESS_THAN_CHANGE and _new < _value ):
			return True;

		##
		print( 'Version -> CanResetOctet -> Returning False - Default Return ... ' );

		## All situations should be covered, but just in case we'll return False here - maybe True later and add a print statement to see when, if at all, this is happening.. If it isn't, we'll get rid of one of our clauses and use it as the default.
		return False
		## if ( this.GetOctetReset( ) != VERSION_OCTET_RESET_NEVER and ( _is_valid and _new != None and _value != None and ( ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_ANY_CHANGE and _new != _value ) or ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_LESS_THAN_CHANGE and _new < _value ) or ( this.GetOctetReset( ) == VERSION_OCTET_RESET_ON_GREATER_THAN_CHANGE and _new > _value ) ) ) )

	##
	## Version Initializer - This updates all octet values based on the data being passed through meaning it can also be used as a Setter...
	##
	def __init__( self, _version = '0.0.0', _minor = None, _revision = None ):
		## Pre-Declare the version values as their defaults... We'll change them below..
		_ver_major, _ver_minor, _ver_build = 0, 0, 0;

		## Update the version values in order to update them in a single place below...
		if ( isstring( _version ) and isstring( _minor ) and isstring( _revision ) ):
			_ver_major, _ver_minor, _ver_build = int( _version ), int( _minor ), int( _revision );
		elif ( isnumber( _version ) and isnumber( _minor ) and isnumber( _revision ) ):
			_ver_major, _ver_minor, _ver_build = _version, _minor, _revision;
		elif ( isstring( _version ) ):
			_data = _version.split( '.' );
			_ver_major, _ver_minor, _ver_build = int( _data[ 0 ] ), int( _data[ 1 ] ), int( _data[ 2 ] );
		else:
			raise Exception( 'AcecoolLib.Version->__init__ Error: Supplied arguments aren\'t all numerical, or the first isn\'t a string containing all version octets. Types are: ( ' + str( type( _version ) ) + ', ' + str( type( _minor ) ) + ', ' + str( type( _revision ) ) + ' )' );

		## Update the 3 version Octets
		self.SetVersionMajor( _ver_major );
		self.SetVersionMinor( _ver_minor );
		self.SetVersionRevision( _ver_build );

	##
	## ToString Method - Printing data using print / str( ... ) is for debugging so we want to include the important information which defines it as a Version so it can't be confused as something else which may have a similar format..
	##
	def __str__( self ):
		return 'Version( ' + self.GetVersion( ) + ' )';


	##
	## Compares the string, rather than numbers, for equality... If not equal, it tries number ( in case the file was edited or a phantom character exists ) Compares the version to see if self == _compare
	## Note: This should be a base comparer - We only need 2... gt and eq or lt and eq.. Because the others can be derived from these..
	## @Return: <TYPE_BOOL> If the current version is equal to the argued version...
	##
	def __eq__( self, _compare = '0.0.0' ):
		return ( _compare != None and self.GetValue( _compare ) == _compare.GetValue( self ) );


	##
	## Compares the version to see if self > _compare
	## Note: This should be a base comparer - We only need 2... gt and eq or lt and eq.. Because the others can be derived from these..
	## @Return: <TYPE_BOOL> If current version is less than argued version
	##
	def __gt__( self, _compare = '0.0.0' ):
		return ( _compare != None and self.GetValue( _compare ) > _compare.GetValue( self ) );


	##
	## Compares the version to see if self >= _compare
	## Note: This should actually call ( with self and _compare being a number object ) self.__gt__( _compare ) or self.__eq__( _compare )
	## @Return: <TYPE_BOOL> If current version is less than argued version
	##
	def __ge__( self, _compare = '0.0.0' ):
		return ( _compare != None and self.GetValue( _compare ) >= _compare.GetValue( self ) );


	##
	## Returns true if the current version is less than the argued version... Compares the version to see if self < _compare
	## Note: This should actually call ( with self and _compare being a number object ) ( Note: We use previously created helpers to shorten solving this ) not self.__ge__( _compare )
	## @Return: <TYPE_BOOL> If current version is less than argued version
	##
	def __lt__( self, _compare = '0.0.0' ):
		return ( _compare != None and self.GetValue( _compare ) < _compare.GetValue( self ) );



	##
	## Returns true if the current version is less than or equal to the argued version... Compares the version to see if self <= _compare
	## Note: This should actually call ( with self and _compare being a number object ) ( Note: We use previously created helpers to shorten solving this ) not self.__gt__( _compare )
	## @Return: <TYPE_BOOL> If current version is less than or equal to argued version
	##
	def __le__( self, _compare = '0.0.0' ):
		return ( _compare != None and self.GetValue( _compare ) <= _compare.GetValue( self ) );


	##
	## Adds two versions together - this simply performs the math from each octet on each other octet - I may change how this operates later... - Note: Since the revisions number is reset each major / minor version, I may just reset it to on adding..
	##
	def __add__( self, _version = None ):
		##
		_version_b = _version;

		##
		if ( IsString( _version_b ) ):
			_version_b = Version( _version_b );

		##
		_version_a = Version( *self.GetAllOctets( ) );
		## _version_major, _version_minor, _version_build, _major, _minor, _build		= *_version_b.GetAllOctets( ), *self.GetAllOctets( );
		_version_major, _version_minor, _version_build		= _version_b.GetAllOctets( );
		_major, _minor, _build								= _version_a.GetAllOctets( );
		## _version_a = Version( _major, _minor, _build );
		## _version_major, _version_minor, _version_build		= _version_b.GetAllOctets( );
		## _major, _minor, _build								= _version_a.GetAllOctets( );

		##
		_version_a.major, _version_a.minor, _version_a.build = _major + _version_major, _minor + _version_minor, _build + _version_build;
		## _version_a.SetVersion( str( _major + _version_major ) + '.' + str( _minor + _version_minor ) + '.' + str( _build + _version_build ) );
		_version_a.SetVersion( _major + _version_major, _minor + _version_minor, _build + _version_build );
		## _major = _major + _version_major;
		## _minor = _minor + _version_minor;
		## _build = _build + _version_build;

		##
		## _version_a.SetVersion( str( _major ) + '.' + str( _minor ) + '.' + str( _build ) );

		##
		return _version_a;


	##
	## Subtracts a version from the other - this simply performs the math from each octet on each other octet - I may change how this operates later...
	##
	def __sub__( self, _version = None ):
		##
		_version_b = _version;

		##
		if ( IsString( _version_b ) ):
			_version_b = Version( _version_b );

		##
		_version_a = Version( *self.GetAllOctets( ) );
		## _version_major, _version_minor, _version_build, _major, _minor, _build		= *_version_b.GetAllOctets( ), *_version_a.GetAllOctets( );
		_version_major, _version_minor, _version_build		= _version_b.GetAllOctets( );
		_major, _minor, _build								= _version_a.GetAllOctets( );
		## _version_a = Version( _major, _minor, _build );

		##
		_version_a.major, _version_a.minor, _version_a.build = _major - _version_major, _minor - _version_minor, _build - _version_build;
		## _version_a.SetVersion( str( _major - _version_major ) + '.' + str( _minor - _version_minor ) + '.' + str( _build - _version_build ) );
		_version_a.SetVersion( _major - _version_major, _minor - _version_minor, _build - _version_build );

		##
		## _version_a.SetVersion( str( _major ) + '.' + str( _minor ) + '.' + str( _build ) );

		##
		return _version_a;


	##
	## Multiplies a version by the other - this simply performs the math from each octet on each other octet - I may change how this operates later...
	##
	def __mul__( self, _version = None ):
		##
		_version_b = _version;

		##
		if ( IsString( _version_b ) ):
			_version_b = Version( _version_b );

		##
		_version_a = Version( *self.GetAllOctets( ) );
		## _version_major, _version_minor, _version_build, _major, _minor, _build		= *_version_b.GetAllOctets( ), *_version_a.GetAllOctets( );
		_version_major, _version_minor, _version_build		= _version_b.GetAllOctets( );
		_major, _minor, _build								= _version_a.GetAllOctets( );
		## _version_a = Version( _major, _minor, _build );

		##
		_version_a.major, _version_a.minor, _version_a.build = _major * _version_major, _minor * _version_minor, _build * _version_build;
		## _version_a.SetVersion( str( _major * _version_major ) + '.' + str( _minor * _version_minor ) + '.' + str( _build * _version_build ) );
		_version_a.SetVersion( _major * _version_major, _minor * _version_minor, _build * _version_build );

		##
		## _version_a.SetVersion( str( _major ) + '.' + str( _minor ) + '.' + str( _build ) );

		##
		return _version_a;


	##
	## Divides a version by the other - this simply performs the math from each octet on each other octet - I may change how this operates later...
	##
	def __div__( self, _version = None ):
		##
		_version_b = _version;

		##
		if ( IsString( _version_b ) ):
			_version_b = Version( _version_b );

		##
		_version_a = Version( *self.GetAllOctets( ) );
		## _version_major, _version_minor, _version_build, _major, _minor, _build		= *_version_b.GetAllOctets( ), *_version_a.GetAllOctets( );
		_version_major, _version_minor, _version_build		= _version_b.GetAllOctets( );
		_major, _minor, _build								= _version_a.GetAllOctets( );
		## _version_a = Version( _major, _minor, _build );

		##
		_version_a.major, _version_a.minor, _version_a.build = _major / _version_major, _minor / _version_minor, _build / _version_build;
		## _version_a.SetVersion( str( _major / _version_major ) + '.' + str( _minor / _version_minor ) + '.' + str( _build / _version_build ) );
		_version_a.SetVersion( _major / _version_major, _minor / _version_minor, _build / _version_build );

		##
		## _version_a.SetVersion( str( _major ) + '.' + str( _minor ) + '.' + str( _build ) );

		##
		return _version_a;


	##
	## Raises a version to the power of the other - this simply performs the math from each octet on each other octet - I may change how this operates later...
	##
	def __pow__( self, _version = None ):
		##
		_version_b = _version;

		##
		if ( IsString( _version_b ) ):
			_version_b = Version( _version_b );

		##
		## _version_a = Version( str( *self.GetAllOctets( ).join( '.' ) ) );
		_version_a = Version( *self.GetAllOctets( ) );
		## _version_major, _version_minor, _version_build, _major, _minor, _build		= *_version_b.GetAllOctets( ), *_version_a.GetAllOctets( );
		_version_major, _version_minor, _version_build		= _version_b.GetAllOctets( );
		_major, _minor, _build								= _version_a.GetAllOctets( );
		## _version_a.SetVersion( str( _major ** _version_major ) + '.' + str( _minor ** _version_minor ) + '.' + str( _build ** _version_build ) );
		_version_a.SetVersion( _major ** _version_major, _minor ** _version_minor, _build ** _version_build );
		## _version_a = Version( _major, _minor, _build );

		##
		## _version_a.major, _version_a.minor, _version_a.build = _major ** _version_major, _minor ** _version_minor, _build ** _version_build;

		##

		##
		return _version_a;


	##
	##
	##
	def __example__( _depth = 1 ):
		##
		_depth_header			= _depth;
		_depth					= _depth + 1;
		_depth_prefix			= _depth * '\t';
		_depth_header_prefix	= _depth_header * '\t';


		## Version Objects...
		_version_zero			= Version( '0.0.0' );
		_version_001			= Version( '0.0.1' );
		_version_010			= Version( '0.1.0' );
		_version_100			= Version( '1.0.0' );
		_version_011			= Version( '0.1.1' );
		_version_111			= Version( '1.1.1' );
		_version_002			= Version( '0.0.2' );
		_version_021			= Version( '0.2.1' );
		_version_212			= Version( '2.1.2' );
		_version_222			= Version( '2.2.2' );
		_version_123			= Version( '1.2.3' );
		_version_321			= Version( '3.2.1' );
		_version_33321456		= Version( '333.21.456' );
		_version_33322456		= Version( '333.22.456' );
		_version_10000000000	= Version( '10000.000.000' );

		_text					= '';

		##
		_text += Acecool.Header( 'Version.IsOlderThan( 0.0.0, 0.0.0 )', '', '', _depth_header );

		##
		def CompareExample( _version_a, _version_b, _gt = False, _ge = False, _lt = False, _le = False, _eq = False, _show_expected = False ):
			## Grab the Version Multipliers... - If all are single-digit then it'll be 100, 10 and 1... If double then 1000, 100, 1
			_a_mult_x, _a_mult_y, _a_mult_z = _version_a.GetMultipliers( );
			_b_mult_x, _b_mult_y, _b_mult_z = _version_b.GetMultipliers( );
			_w_mult_x, _w_mult_y, _w_mult_z = _version_a.GetMultipliers( _version_b );

			_text = ''
			_text += _depth_prefix + String.FormatColumn( 15, 'A Version:',			45, str( _version_a ),								15, 'B Version:',		35, str( _version_b ),									25, 'Using Octet Buffer:',		35, str( _version_a.GetVersionOctetBuffer( ) ) ) + '\n';
			## _text += _depth_prefix + String.FormatColumn( 15, 'A Version:',		45, str( _version_a.version ),						15, 'B Version:',		35, str( _version_b.version ),							25, 'Using Octet Buffer:',		35, str( _version_a.GetVersionOctetBuffer( ) ) ) + '\n';
			_text += _depth_prefix + String.FormatColumn( 15, 'A Value:',			45, str( _version_a.GetValue( _version_b ) ),		15, 'B Value:',			35, str( _version_b.GetValue( _version_a ) ) ) + '\n';
			_text += _depth_prefix + String.FormatColumn( 15, 'A Multipliers:',		15, str( _a_mult_x ),	15, str( _a_mult_y ),		15, str( _a_mult_z ),	15, 'B Multipliers:',									15, str( _b_mult_x ),			15, str( _b_mult_y ),			15, str( _b_mult_z ) ) + '\n';
			## _text += _depth_prefix + String.FormatColumn( 15, 'B Multipliers:',	15, str( _b_mult_x ),	15, str( _b_mult_y ),		15, str( _b_mult_z ) ) + '\n';
			_text += _depth_prefix + String.FormatColumn( 15, 'W Multipliers:',		15, str( _w_mult_x ),	15, str( _w_mult_y ),		15, str( _w_mult_z ) ) + '\n';
			## _text += _depth_prefix + 'A Multipliers: ' + str( _a_mult_x ) + ', ' + str( _a_mult_y ) + ', ' + str( _a_mult_z ) + '\n';
			## _text += _depth_prefix + 'B Multipliers: ' + str( _b_mult_x ) + ', ' + str( _b_mult_y ) + ', ' + str( _b_mult_z ) + '\n';
			## _text += _depth_prefix + 'W Multipliers: ' + str( _w_mult_x ) + ', ' + str( _w_mult_y ) + ', ' + str( _w_mult_z ) + '\n';
			_text += _depth_prefix + '' + str( _version_a ) + ' IsOlderThan ' + str( _version_b ) + ' == ' + str( _version_a.IsOlderThan( _version_b ) ) + '\n';
			_text += _depth_prefix + '' + str( _version_a ) + ' IsNewerThan ' + str( _version_b ) + ' == ' + str( _version_a.IsNewerThan( _version_b ) ) + '\n';
			_text += _depth_prefix + '' + str( _version_a ) + '  > ' + str( _version_b ) + ' == ' + str( _version_a > _version_b ) + ( '', '\t\tExpected: ' + str( _gt ) )[ _show_expected ] + '\t\t' + ( 'Wrong', '' )[ ( _gt == ( _version_a > _version_b ) ) ] + '\n';
			_text += _depth_prefix + '' + str( _version_a ) + ' >= ' + str( _version_b ) + ' == ' + str( _version_a >= _version_b ) + ( '', '\t\tExpected: ' + str( _ge ) )[ _show_expected ] + '\t\t' + ( 'Wrong', '' )[ ( _ge == ( _version_a >= _version_b ) ) ] + '\n';
			_text += _depth_prefix + '' + str( _version_a ) + '  < ' + str( _version_b ) + ' == ' + str( _version_a < _version_b ) + ( '', '\t\tExpected: ' + str( _lt ) )[ _show_expected ] + '\t\t' + ( 'Wrong', '' )[ ( _lt == ( _version_a < _version_b ) ) ] + '\n';
			_text += _depth_prefix + '' + str( _version_a ) + ' <= ' + str( _version_b ) + ' == ' + str( _version_a <= _version_b ) + ( '', '\t\tExpected: ' + str( _le ) )[ _show_expected ] + '\t\t' + ( 'Wrong', '' )[ ( _le == ( _version_a <= _version_b ) ) ] + '\n';
			_text += _depth_prefix + '' + str( _version_a ) + ' == ' + str( _version_b ) + ' == ' + str( _version_a == _version_b ) + ( '', '\t\tExpected: ' + str( _eq ) )[ _show_expected ] + '\t\t' + ( 'Wrong', '' )[ ( _eq == ( _version_a == _version_b ) ) ] + '\n';
			_text += _depth_prefix + '\n';
			return _text;

		##																	>		>=		<		<=		==
		_text += CompareExample( _version_zero,	_version_zero,				False,	True,	False,	True,	True );
		_text += CompareExample( _version_zero,	_version_001,				False,	False,	True,	True,	False );
		_text += CompareExample( _version_001,	_version_001,				False,	True,	False,	True,	True );
		_text += CompareExample( _version_010,	_version_100,				False,	False,	True,	True,	False );
		_text += CompareExample( _version_111,	_version_100,				True,	True,	False,	False,	False );
		_text += CompareExample( _version_111,	_version_212,				False,	False,	True,	True,	False );
		_text += CompareExample( _version_123,	_version_212,				False,	False,	True,	True,	False );
		_text += CompareExample( _version_123,	_version_321,				False,	False,	True,	True,	False );
		_text += CompareExample( _version_321,	_version_123,				True,	True,	False,	False,	False );
		_text += CompareExample( _version_123,	_version_33322456,			False,	False,	True,	True,	False );
		_text += CompareExample( _version_33321456,	_version_33322456,		False,	False,	True,	True,	False );
		_text += CompareExample( _version_33322456,	_version_33321456,		True,	True,	False,	False,	False );
		_text += CompareExample( _version_33322456,	_version_10000000000,	False,	False,	True,	True,	False );

		_text += '\n\n';

		_version = _version_33321456;

		## version
		## version_major			main			primary			major
		## version_minor			minor			sub				secondary
		## version_revision			revision		build
		## version_octet_buffer		buffer


		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version: ', 50, str( _version ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.version: ', 50, str( _version.version ) ) + '\n';

		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.version_major: ', 50, str( _version.version_major ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.version_minor: ', 50, str( _version.version_minor ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.version_revision: ', 50, str( _version.version_revision ) ) + '\n';

		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.major: ', 50, str( _version.major ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.minor: ', 50, str( _version.minor ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.revision: ', 50, str( _version.revision ) ) + '\n';

		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.main: ', 50, str( _version.main ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.sub: ', 50, str( _version.sub ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.patch: ', 50, str( _version.build ) ) + '\n';

		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.primary: ', 50, str( _version.primary ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.secondary: ', 50, str( _version.secondary ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Version.patch: ', 50, str( _version.patch ) ) + '\n';

		_text += '\n';
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> + ', 30, _version_33321456, 10, '+', 30, _version_321, 10, '==', 50, _version + _version_321 ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> - ', 30, _version_33321456, 10, '-', 30, _version_321, 10, '==', 50, _version - _version_321 ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> * ', 30, _version_33321456, 10, '*', 30, _version_321, 10, '==', 50, _version * _version_321 ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> / ', 30, _version_33321456, 10, '/', 50, _version_321, 10, '==', 50, _version / _version_321 ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> ** ', 30, _version_33321456, 10, '**', 30, _version_321, 10, '==', 50, _version ** _version_321 ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> + ', 30, _version_33321456, 10, '+', 50, _version_321, 10, '==', 50, _version + _version_321 ) + '\n';
		_text += '\n';
		_text += '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Version.__p: ', 250, str( _version.__p ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Version._pitch: ', 250, str( _version._pitch ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Version._p: ', 250, str( _version._p ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Version._yaw: ', 250, str( _version._yaw ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Version._y: ', 250, str( _version._y ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Version._roll: ', 250, str( __versionang._roll ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Version._r: ', 250, str( _version._r ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Version.GetYAccessor( ): ', 250, str( _version.GetYAccessor( ) ) ) + '\n';

		_text += '\t\t##\n'
		_text += '\t\t## Note: I may alter the behavior to always reset smaller octets regardless of the change - except if equal to... So if I change up or down, the lower ones will be reset to 0.. If I set to the same value, then nothing changes..\n';
		_text += '\t\t## Note: I made that change.. We will see how it goes - I may revert it... Actually, I will go ahead and create a controlled toggle so that the behavior can be defined as you see fit - by default, it will reset lower octets if the value of a higher octet is changed to something different..\n';
		_text += '\t\t##\n'
		_text += _depth_prefix + Text.FormatColumn( 50, '-> Before changes ', 30, _version_321 ) + '\n';
		_version_321.minor = 10;
		_text += _depth_prefix + Text.FormatColumn( 50, '-> Altered Minor to 10, expect 3.10.0', 30, _version_321 ) + '\n';

		_version_321.major = 11;
		_text += _depth_prefix + Text.FormatColumn( 50, '-> Altered Major to 11, expect 11.0.0 ', 30, _version_321 ) + '\n';
		_version_321.SetMinor( 12 );
		_text += _depth_prefix + Text.FormatColumn( 50, '-> Altered Minor to 12, expect 11.12.0 ', 30, _version_321 ) + '\n';

		_version_321.SetMajor( 9 );
		_text += _depth_prefix + Text.FormatColumn( 50, '-> Altered Major to 9 ( < ), expect 9.12.0 ', 30, _version_321 ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 50, '-> Altered Major to 9 ( < ), expect 9.0.0 ', 30, _version_321 ) + '\n';



		_text += _depth_prefix + '\n';
		_text += Acecool.Footer( ) + '\n';



		## Unload all examples...
		_version_zero	= None;
		_version_001	= None;
		_version_010	= None;
		_version_100	= None;
		_version_011	= None;
		_version_111	= None;
		_version_002	= None;
		_version_021	= None;
		_version_212	= None;
		_version_222	= None;


		## _text += Acecool.Header( 'Util.(  ) - ...', '', '', _depth_header );
		## _text += _depth_prefix + '';
		## _text += _depth_prefix + '';
		## _text += Acecool.Footer( ) + '\n';

		return _text;


	##
	## Returns the values from each octet ( I could use major / primary / minor / secondary / build / revision / and more or the Get*( ) helpers...
	##
	def GetAllOctets( self ):
		return self.major, self.minor, self.patch;


	##
	## Returns the base multiplier for this version...
	##
	def GetMultipliersEx( self ):
		## Grab all values
		_major			= self.GetVersionMajor( );
		_minor			= self.GetVersionMinor( );
		_revision		= self.GetVersionRevision( );

		## Grab the length of all values...
		_major_len		= len( str( _major ) );
		_minor_len		= len( str( _minor ) );
		_revision_len	= len( str( _revision ) );

		## Return values to ensure there can be no collisions
		return ( 10 ** ( _revision_len + _minor_len + ( self.GetVersionOctetBuffer( ) * 2 ) ) ), ( 10 ** ( _revision_len + self.GetVersionOctetBuffer( ) ) ), ( 1 );


	##
	## Returns the multiplier either by itself, or when being compared with another version...
	##
	def GetMultipliers( self, _compare = None ):
		## Grab the base multipliers...
		_mult_x, _mult_y, _mult_z = self.GetMultipliersEx( )
		if ( _compare == None ):
			return _mult_x, _mult_y, _mult_z;

		## If compare is assigned, we need the max of all...
		_mult_b_x, _mult_b_y, _mult_b_z = _compare.GetMultipliersEx( );

		## Return the maximum multiplier to use... This is so compares can be on the same page...
		return max( _mult_x, _mult_b_x ), max( _mult_y, _mult_b_y ), max( _mult_z, _mult_b_z );


	##
	## Returns the Version Value after being multiplied...
	##
	def GetValue( self, _compare = None ):
		## Grab the multipliers
		_mult_x, _mult_y, _mult_z = self.GetMultipliers( _compare );

		## Rerturn the single-numerical value for the version..
		return int( ( self.GetVersionMajor( ) * _mult_x ) + ( self.GetVersionMinor( ) * _mult_y ) + ( self.GetVersionRevision( ) * _mult_z ) );


	##
	## Returns whether or not _version is newer than _compare
	##
	def IsNewerThan( self, _compare = '0.0.0' ):
		return self.__gt__( _compare );
		## return ( not _version == _compare and not _version.IsOlderThan( _compare ) )


	##
	## Returns whther or not _version is older than _compare
	##
	def IsOlderThan( self, _compare = '0.0.0' ):
		return self.__lt__( _compare );




	## ## Old and new version.. Old uses callback so whe SetVersion is called, the Major / Minor / Revision values would be updated through the callback...
	## ##	With the new, GetVersion uses Major / Minor / Revision Getters to return the value, and Set uses __init__ which had to process the data anyway, so instead of repeating code, there is actually less...
	## ##	And, now I'm not storing Version value so there is less overhead - and the data isn't being stored twice..
	## __version								= AccessorFuncBase( parent = VersionBase,	key = 'version',				keys = ( 'x' ),												name = 'Version',					names = ( ),											default = '0.0.0',							getter_prefix = 'Get',			documentation = 'Version Value',				allowed_types = ( TYPE_STRING ),				allowed_values = ( VALUE_ANY ),					setup_override = { 'on_set': ( lambda this, _current, _new, _has_updated: this.Unpack( ) ) }															);

	## ##
	## ## Version Initializer - This updates all octet values based on the data being passed through meaning it can also be used as a Setter...
	## ##
	## def __init__( self, _version = '0.0.0', _minor = None, _revision = None ):

	## 	## Old Init Code - The old one used SetVersion which, when called, called a callback on_set which then updated all 3 Major / Minor / Revision stored data...
	## 	## 	However, Version was also stored ( I could've used a Getter the same way I am now to ensure I used the 3 octets, but version would still be a string of the data with increased overhead - by  )

	## 	self.SetVersion( _version );
	## 	self.version = _version;

	## 	##
	## 	if ( isstring( _version ) ):
	## 		print( 'Version -> SetVersion: ' + str( _version ) );
	## 		self.SetVersion( _version );
	## 		## self.version = _version
	## 		## self.Unpack( );
	## 	elif( isnumber( _version ) and isnumber( _minor ) and isnumber( _revision ) ):
	## 		self.SetVersion( str( _version ) + '.' + str( _minor ) + '.' + str( _revision ) );
	## 	elif( isstring( _version ) and isstring( _minor ) and isstring( _revision ) ):
	## 		self.SetVersion( _version + '.' + _minor + '.' + _revision );
	## 		## self.version = str( _version ) + '.' + str( _minor ) + '.' + str( _revision )

	## 		## Task: Maybe add an alternative on_set callback for this case.. If numeric then avoid - or an alternative setter to allow multiple args, joined on a delimeter or something...
	## 		## 	self.SetVersionMajor( _version );
	## 		## 	self.SetVersionMinor( _minor );
	## 		## 	self.SetVersionRevision( _revision );
	## 		## 	## self.version_major		= _version;
	## 		## 	## self.version_minor		= _minor;
	## 		## 	## self.version_revision	= _revisionw;
	## 	else:
	## 		raise Exception( 'AcecoolLib.Version->__init__ Error: Supplied arguments aren\'t all numerical, or the first isn\'t a string containing all version octets. Types are: ( ' + str( type( _version ) ) + ', ' + str( type( _minor ) ) + ', ' + str( type( _revision ) ) + ' )' );


	## ##
	## ##
	## ##
	## def GetAll( _compare = '0.0.0' ):
	## 	##
	## 	_version			= '0.0.0';
	## 	_version_major, _version_minor, _version_revision		= 0, 0, 0;

	## 	##
	## 	if ( isstring( _compare ) ):
	## 		_version = _compare;
	## 		_version_major, _version_minor, _version_revision = Version.UnpackEx( _compare );
	## 	elif ( isinstance( _compare, Version ) ):
	## 		_version = _compare.version
	## 		_version_major, _version_minor, _version_revision = _compare.Unpack( );

	## 	##
	## 	return _version, _version_major, _version_minor, _version_revision;


	## ##
	## ## Old Version System
	## ##
	## def UnpackEx( _ver = '0.0.0' ):
	## 	##
	## 	_version = '0.0.0';

	## 	##
	## 	if ( isstring( _ver ) ):
	## 		_version = _ver;
	## 	elif ( isinstance( _ver, Version ) ):
	## 		_version = _ver.version;

	## 	##
	## 	_versions 	= _version.split( '.' );

	## 	##
	## 	_version_major		= 0 + int( ( 0, _versions[ 0 ] )[ _versions[ 0 ] != None and isstring( _versions[ 0 ] ) ] );
	## 	_version_minor		= 0 + int( ( 0, _versions[ 1 ] )[ _versions[ 1 ] != None and isstring( _versions[ 1 ] ) ] );
	## 	_version_revision	= 0 + int( ( 0, _versions[ 2 ] )[ _versions[ 2 ] != None and isstring( _versions[ 2 ] ) ] );

	## 	##
	## 	return _version_major, _version_minor, _version_revision;


	## ##
	## ## Old Version System
	## ##
	## def Unpack( self ):
	## 	##
	## 	_major, _minor, _revision = Version.UnpackEx( self.GetVersion( ) );
	## 	self.SetVersionMajor( _major );
	## 	self.SetVersionMinor( _minor );
	## 	self.SetVersionRevision( _revision );

	## 	##
	## 	print( 'Version -> Unpack -> ' + str( self ) )

	## 	##
	## 	return _major, _minor, _revision;





##
## Position 2D Base Class - This is an example of how quickly a container object can be created using AccessorFunc System.....
##
class Pos2DBase( ):
	pass;
class Pos2D( Pos2DBase ):
	##
	__name__					= 'Pos2D';

	## This is blank - so there is this.Get, this.IsSet, etc... ( getter_prefix must be set otherwise empty function-names may cause an issue before I block them )
	__this_object				= AccessorFuncBase( parent = Pos2DBase,		key = 'this_object',	name = '',				default = 0.0,						getter_prefix = 'Get',		documentation = 'Returns this object octets',	allowed_types = ( TYPE_ANY ),								allowed_values = ( VALUE_ANY ),				setup = {
		'get':		( lambda this: [ this.x, this.y ] ),
		'set':		( lambda this, _x = 0.0, _y = 0.0: this.__init__( _x, _y ) ),
		'get_str': 	( lambda this: 'Pos2D( ' + str( this.x ) + ', ' + str( this.y ) + ' );' )
	} );

	## Serialize turns this object into a human readable format - Note: There will be a serialize function in AccessorFunc in the future where you can easily define separators, prefix, suffix, etc..
	__serialize					= AccessorFuncBase( parent = Pos2DBase,		key = 'serialize',		name = 'Serialize',		default = 'Pos2D( 0.0, 0.0 );',		getter_prefix = '',			documentation = 'Serialize Data',				allowed_types = ( TYPE_STRING ),							allowed_values = ( VALUE_ANY ),				setup = { 'get_accessor': True, 'get': ( lambda this: this.GetToString( ) ), 'get_str': ( lambda this: this.GetToString( ) ) }															);

	## This is the actual data for this object - a 2D position needs X and Y axis.
	__x							= AccessorFuncBase( parent = Pos2DBase,		key = 'x',				name = 'X',				default = 0.0,						getter_prefix = 'Get',		documentation = 'X',							allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),				allowed_values = ( VALUE_ANY ),		setup = { 'get': True, 'get_str': True, 'get_accessor': True, 'set': True }		);
	__y							= AccessorFuncBase( parent = Pos2DBase,		key = 'y',				name = 'Y',				default = 0.0,						getter_prefix = 'Get',		documentation = 'Y',							allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),				allowed_values = ( VALUE_ANY ),		setup = { 'get': True, 'get_str': True, 'get_accessor': True, 'set': True }		);


	##
	##
	##
	def __init__( self, _x = 0.0, _y = 0.0 ):
		## Update all of the properties - Note: I could use self.SetX( _x ), self._x = _x, and a few other options. I'm using short-hand here for sake of efficiency... But self.SetX( ... ) is actually called when self.x is reassigned with _x...
		self.x = _x;
		self.y = _y;


	##
	##
	##
	def __str__( self ):
		return self.Serialize( );


	##
	## Shows Example Usage and Output for all Class Functions...
	##
	def __example__( _depth = 1 ):
		##;
		_depth_prefix				= ( _depth + 1 ) * '\t';

		_pos = Pos2D( );
		_pos.x = 1.01;
		_pos.y = 2.02345;

		_text = '';
		_text += Acecool.Header( 'Data for _pos2d = Pos2D( 1.01, 2.02345 )', '{comment_hierarchy}Pos2DBase -> Pos2D{footer}', '', _depth );
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos2D.Serialize( ): ', 50, str( _pos.Serialize( ) ) ) + '\n';
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos2D.x: ', 50, str( _pos.x ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos2D.y: ', 50, str( _pos.y ) ) + '\n';
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos2D.__serialize: ', 250, str( _pos.__serialize ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos2D.__x: ', 250, str( _pos.__x ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos2D.__y: ', 250, str( _pos.__y ) ) + '\n';


		return _text + '\n' + Acecool.Footer( ) + '\n\n';


##
## Position 3D Base Class - This is an example of how quickly a container object can be created using AccessorFunc System.....
##
class PosBase( Pos2D ):
	pass;
class Pos( PosBase ):
	##
	__name__					= 'Pos';

	## Note: This may not work properly because this extends Pos2D - __ prefixed methods can't be overrided by child-classes, so it may not work with other data so I'll need to verify... This is the behavior I want for adding these accessors so the accessors can't be overwritten, but for child-classes I think it wouldn't be a bad idea to allow updated functions...
	__serialize					= AccessorFuncBase( parent = PosBase,		key = 'serialize',		name = 'Serialize',			default = 'Pos2D( 0.0, 0.0, 0.0 );',		getter_prefix = '',				documentation = 'Serialize Data',				allowed_types = ( TYPE_STRING ),							allowed_values = ( VALUE_ANY ),				setup = { 'get_accessor': True, 'get': ( lambda this: 'Pos( ' + str( this.x ) + ', ' + str( this.y ) + ', ' + str( this.z ) + ' );' ) }							);
	__z							= AccessorFuncBase( parent = PosBase,		key = 'z',				name = 'Z',					default = 0.0,								getter_prefix = 'Get',			documentation = 'Z',							allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),				allowed_values = ( VALUE_ANY )		);
	## __x = Pos2Base.__x
	## __y = Pos2Base.__y


	##
	##
	##
	def __init__( self, _x = 0.0, _y = 0.0, _z = 0.0 ):
		## Update all of the properties - Note: I could use self.SetX( _x ), self._x = _x, and a few other options. I'm using short-hand here for sake of efficiency... But self.SetX( ... ) is actually called when self.x is reassigned with _x...
		self.x = _x;
		self.y = _y;
		self.z = _z;


	##
	## Shows Example Usage and Output for all Class Functions...
	##
	def __example__( _depth = 1 ):
		##;
		_depth_prefix				= ( _depth + 1 ) * '\t';

		_pos = Pos( );
		_pos.x = 12.2478;
		_pos.y = 4321.12;
		_pos.z = 9867.393;

		_text = '';
		_text += Acecool.Header( 'Data for _pos = Pos( 12.2478, 4321.12, 9867.393 )', '{comment_hierarchy}Pos2DBase -> Pos2D -> PosBase -> Pos{footer}', '', _depth );
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.Serialize( ): ', 50, str( _pos.Serialize( ) ) ) + '\n';
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.x: ', 50, str( _pos.x ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.y: ', 50, str( _pos.y ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.z: ', 50, str( _pos.z ) ) + '\n';
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.__serialize: ', 250, str( _pos.__serialize ) ) + '\n';

		## Note: This will also return an error because I created Serialize with a setup table override - meaning the only function which is created is the getter... If I want to allow this, then I need to unlock it... which I did by adding 'get_accessor': True to the setup table... Soon this table will use the enumerators, although I'll probably still allow the text elements... Note: Get prefix isn't use, this affects all helpers - I may change it back to where it only alters the main Get/Set etc.. functions and keep the others so Get<Name>Accessor( ) instead of <Name>Accessor( ), etc... Or I'll change a few things to remov ethe Get prefix for certain systems.
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.Serialize( ): ', 250, str( _pos.SerializeAccessor( ) ) ) + '\n';

		## Note: because __ is effectively 'internal' designation in Python, and since we haven't devided it in Pos - even though we extend to a class that has it - we don't have access to it through these means... I may need to look into a way to reinitialize it when a class is initialized or something... We can still use Get<Name>Accessor( ) which is effectively identical to __<key> and, if you look at the identifier provided by the output, they are identical to x / y for Pos@D with x / y for Pos because those are re-used.... This is intended. But not being able to use __<key> is a drawback I will look into.
		## Task: Look into way to unlock __<key> usage for child-classes which parent a class which makes use of them... Or trigger a way to add the __<key> to the current class without having ro re-add the same line as the previous class...
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.__x: ', 300, str( _pos.__x ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.__y: ', 300, str( _pos.__y ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.GetXAccessor( ): ', 250, str( _pos.GetXAccessor( ) ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.GetYAccessor( ): ', 250, str( _pos.GetYAccessor( ) ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.__x: ', 300, str( _pos.GetXProperty( ) ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.__y: ', 300, str( _pos.GetYProperty( ) ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.__z: ', 250, str( _pos.__z ) ) + '\n';


		return _text + '\n' + Acecool.Footer( ) + '\n\n';


##
## Angle Base Class - Note: I could parent the same way Vector does and change the getattr magic function to alter x / y / z to p / y / r for pitch, yaw and roll without re-declaring anything...
##
class AngleBase( Pos ):
	pass;
class Angle( AngleBase ):
	##
	__name__					= 'Angle';

	## Note, I'm using self.p / y / r for to string data instead of functions because if I rename the property from x, y, z dynamically without re-declaring then it means I'd either need to rename all of the functions too, or just re-declare, or simply use self.p / y / r instead, everywhere...
	## Task: Add system to rename functions in this regard to allow prevention of adding so much duplicate code...
	__serialize					= AccessorFuncBase( parent = AngleBase,	key = 'serialize',									name = 'Serialize',											default = 'Angle( 0.0, 0.0, 0.0 );',		getter_prefix = '',				documentation = 'Serialize Data',			allowed_types = ( TYPE_STRING ),					allowed_values = ( VALUE_ANY ),					setup = { 'get': ( lambda this: 'Angle( ' + str( this.p ) + ', ' + str( this.y ) + ', ' + str( this.r ) + ' );' ) }				);

	## Note: I could set up pitch, yaw, roll with Get / Set redirecting to p / y / r.... This would make __pitch, __yaw, and __roll available... But I don't want to override pitch / yaw / roll / _pitch / _yaw / _roll created by these 3 aliases... So I'll likely just need to add the alias system for names too.. Honestly, I should change the defaults to Pitch / Yaw / Roll and add P / Y / R as the aliases..
	__p							= AccessorFuncBase( parent = AngleBase,	key = 'p',				keys = [ 'pitch' ],			name = 'Pitch',				names = [ 'P' ],				default = 0.0,								getter_prefix = 'Get',			documentation = 'Pitch',					allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),		allowed_values = ( VALUE_ANY )		);
	__y							= AccessorFuncBase( parent = AngleBase,	key = 'y',				keys = [ 'yaw' ],			name = 'Yaw',				names = [ 'Y' ],				default = 0.0,								getter_prefix = 'Get',			documentation = 'Yaw',						allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),		allowed_values = ( VALUE_ANY )		);
	__r							= AccessorFuncBase( parent = AngleBase,	key = 'r',				keys = [ 'roll' ],			name = 'Roll',				names = [ 'R' ],				default = 0.0,								getter_prefix = 'Get',			documentation = 'Roll',						allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),		allowed_values = ( VALUE_ANY )		);

	## ##
	## ##
	## ##
	## def __getattr__( self, _key, _default = None ):
	## 	if ( _key == 'x' ): _key = 'p';
	## 	if ( _key == 'z' ): _key = 'r';

	## 	##
	## 	return self.__dict__[ _key ];


	##
	## This isn't necessary... As the defaults are already 0.0, using the getter or the property will return that value... This is a convenience function to allow assigning all values at once...
	##
	def __init__( self, _pitch = 0.0, _yaw = 0.0, _roll = 0.0 ):
		## Update all of the properties - Note: I could use self.SetPitch( _pitch ), self._p = _pitch, and a few other options. I'm using short-hand here for sake of efficiency... But self.SetPitch( ... ) is actually called when self.p / self.pitch is reassigned with _pitch...
		self.p = _pitch;
		self.y = _yaw;
		self.r = _roll;


	##
	## Shows Example Usage and Output for all Class Functions...
	##
	def __example__( _depth = 1 ):
		##;
		_depth_prefix				= ( _depth + 1 ) * '\t';

		_ang = Angle( );
		_ang.x = 12.2478;
		_ang.y = 4321.12;
		_ang.z = 9867.393;

		_text = '';
		_text += Acecool.Header( 'Data for _ang = Angle( 12.2478, 4321.12, 9867.393 )', '{comment_hierarchy}Pos2DBase -> Pos2D -> PosBase -> Pos -> AngleBase -> Angle{depth_comment}{depth_note}This example is interesting because Angle is a class where p / y / r is the primary key, but the aliases pitch / yaw / roll have been added and reference the primary keys meaning you can use self.p = 123.45 or self.pitch = 123.45{depth_comment_tab}When attempting to reassign data for the Primary Key, or using the Primary Setter ( Set<Name> ), the Primary Property Setter assigns the data via the Raw Property Setter, if the data-type / value system doesn\'t block it.\n\t##\tWhen attempting to reassign using an alias, only the Raw Property Setter is used which also has protection against bad data, but it redirects to _<key>..{depth_comment}{depth_note}Each alias key added has 2 properties used - the primary property for self.<alias> and the raw property for self._<alias> whereas with the Primary Key, we only need to set a property to self.<key>{depth_comment}{depth_example}self.p{tab3}=={tab2}Property{depth_example}self.pitch{tab2}=={tab2}Property{depth_comment}{depth_example}self._p{tab3}=={tab2}Value{depth_example}self._pitch{tab2}=={tab2}RawProperty{footer}', '', _depth );
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.Serialize( ): ', 50, str( _ang.Serialize( ) ) ) + '\n';
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.p: ', 50, str( _ang.p ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.y: ', 50, str( _ang.y ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.r: ', 50, str( _ang.r ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.pitch: ', 50, str( _ang.pitch ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.yaw: ', 50, str( _ang.yaw ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.roll: ', 50, str( _ang.roll ) ) + '\n';
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.__serialize: ', 250, str( _ang.__serialize ) ) + '\n';

		## Note: This will also return an error because I created Serialize with a setup table override - meaning the only function which is created is the getter... If I want to allow this, then I need to unlock it... which I did by adding 'get_accessor': True to the setup table... Soon this table will use the enumerators, although I'll probably still allow the text elements... Note: Get prefix isn't use, this affects all helpers - I may change it back to where it only alters the main Get/Set etc.. functions and keep the others so Get<Name>Accessor( ) instead of <Name>Accessor( ), etc... Or I'll change a few things to remov ethe Get prefix for certain systems.
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.Serialize( ): ', 250, str( _ang.SerializeAccessor( ) ) ) + '\n';

		## Note: because __ is effectively 'internal' designation in Python, and since we haven't devided it in Pos - even though we extend to a class that has it - we don't have access to it through these means... I may need to look into a way to reinitialize it when a class is initialized or something... We can still use Get<Name>Accessor( ) which is effectively identical to __<key> and, if you look at the identifier provided by the output, they are identical to x / y for Pos@D with x / y for Pos because those are re-used.... This is intended. But not being able to use __<key> is a drawback I will look into.
		## Task: Look into way to unlock __<key> usage for child-classes which parent a class which makes use of them... Or trigger a way to add the __<key> to the current class without having ro re-add the same line as the previous class...
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.__x: ', 300, str( _ang.__x ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Pos.__y: ', 300, str( _ang.__y ) ) + '\n';
		_ang.roll = 123;
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.__p: ', 250, str( _ang.__p ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle._pitch: ', 250, str( _ang._pitch ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle._p: ', 250, str( _ang._p ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle._yaw: ', 250, str( _ang._yaw ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle._y: ', 250, str( _ang._y ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle._roll: ', 250, str( _ang._roll ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle._r: ', 250, str( _ang._r ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.GetYAccessor( ): ', 250, str( _ang.GetYAccessor( ) ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.__x: ', 300, str( _ang.GetXProperty( ) ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.__y: ', 300, str( _ang.GetYProperty( ) ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Angle.__r: ', 250, str( _ang.__r ) ) + '\n';


		return _text + '\n' + Acecool.Footer( ) + '\n\n';


##
## Vector Base Class - This is an example of how quickly a container object can be created using AccessorFunc System.....
##
class VectorBase( Pos ):
	pass;
class Vector( VectorBase ):
	##
	__name__					= 'Vector';

	## One method to serialize the data - Note: Here I use this.Get<Name>ToString( ) instead of str( this.<key> to show that you can implement it how you want - you can use the built in helpers or not.... it is up to you.. If you don't want to use them, you can set the key to None in the setup_override panel to remove only specific functions, or override the setup dict and only include the functions you specifically want )
	__serialize					= AccessorFuncBase( parent = VectorBase,	key = 'serialize',		name = 'Serialize',			default = 'Vector( 0.0, 0.0, 0.0 );',		getter_prefix = '',				documentation = 'Serialize Data',												allowed_types = ( TYPE_STRING ),					allowed_values = ( VALUE_ANY ),					setup = { 'get': ( lambda this: 'Vector( ' + this.GetXToString( ) + ', ' + this.GetYToString( ) + ', ' + this.GetZToString( ) + ' );' ) }				);
	__normalize					= AccessorFuncBase( parent = VectorBase,	key = 'normalize',		name = 'Normalize',			default = None,								getter_prefix = '',				documentation = 'Normalizes the Vector Values and returns them as a copy',		allowed_types = ( TYPE_STRING ),					allowed_values = ( VALUE_ANY ),					setup = { 'get': ( lambda this: this.CalcNormalize( ) ) }															);
	__normalized				= AccessorFuncBase( parent = VectorBase,	key = 'normalized',		name = 'Normalized',		default = None,								getter_prefix = 'Get',			documentation = 'Returns a Normalized Vector',									allowed_types = ( TYPE_STRING ),					allowed_values = ( VALUE_ANY ),					setup = { 'get': ( lambda this: Vector( *this.Normalize( ) ) ) }															);

	## Not necessary because of parenting - it means instead of using self.__x/y/z I have to use self.GetX/Y/ZAccessor( ) when I want to access the AccessorFuncBase Object... This adds Vector Functions but still uses the 3 previous locations for storage of information created by Pos2D ( x and y ), and Pos ( 3D which uses x and y from Pos2D and creates z )
	## __x = Pos.__x;
	## __y = Pos.__y;
	## __z = Pos.__z;
	## __x							= AccessorFuncBase( parent = VectorBase,	key = 'x',				name = 'X',					default = 0.0,								getter_prefix = 'Get',			documentation = 'X',															allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),		allowed_values = ( VALUE_ANY )		);
	## __y							= AccessorFuncBase( parent = VectorBase,	key = 'y',				name = 'Y',					default = 0.0,								getter_prefix = 'Get',			documentation = 'Y',															allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),		allowed_values = ( VALUE_ANY )		);
	## __z							= AccessorFuncBase( parent = VectorBase,	key = 'z',				name = 'Z',					default = 0.0,								getter_prefix = 'Get',			documentation = 'Z',															allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),		allowed_values = ( VALUE_ANY )		);


	## ##
	## ## Note: This isn't necessary because of Pos parent class...
	## ##
	## def __init__( self, _x = 0.0, _y = 0.0, _z = 0.0 ):
	## 	## Update all of the properties - Note: I could use self.SetX( _x ), self._x = _x, and a few other options. I'm using short-hand here for sake of efficiency... But self.SetX( ... ) is actually called when self.x is reassigned with _x...
	## 	self.x = _x;
	## 	self.y = _y;
	## 	self.z = _z;


	##
	## Calculates the magnitude of the vector... A vector is a direction in 3D space, when we calculate the magnitude we can use it to travel in that direction 1 unit at a time instead of n units... This means if the Vector is pointing forward in relation to a player and we SetPlayerPos( GetPlayerPos( ) + NormalizedVector( ) * _units ) we move the player n units forward...
	##
	def CalcNormalize( self ):
		## Create local copies of the variables so we don't change the data...
		_x, _y, _z = self.x, self.y, self.z

		## Calculate the length
		_len = math.sqrt( ( _x * _x ) + ( _y * _y ) + ( _z * _z ) );

		## Now, normalize the values
		_x /= _len;
		_y /= _len;
		_z /= _len;

		## Return those values...
		return _x, _y, _z;


	##
	## Shows Example Usage and Output for all Class Functions...
	##
	def __example__( _depth = 1 ):
		##;
		_depth_prefix				= ( _depth + 1 ) * '\t';

		_vector = Vector( );
		_vector.x = -1.0486;
		_vector.y = 0.00134785;
		_vector.z = 0.9768;

		_text = '';
		_text += Acecool.Header( 'Data for _vector = Vector( -1.0486, 0.00134785, 0.9768 )', '{comment_note}Pos2DBase -> Pos2D -> PosBase -> Pos -> VectorBase -> Vector{footer}', '', _depth );
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.Serialize( ): ', 50, str( _vector.Serialize( ) ) ) + '\n';
		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.x: ', 50, str( _vector.x ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.y: ', 50, str( _vector.y ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.z: ', 50, str( _vector.z ) ) + '\n';
		_text += '\n';

		## Vector( *_vector.Normalize( ) ).Serialize( )
		_text += _depth_prefix + Text.FormatColumn( 30, '-> _vector.Normalize( ): ', 85, str( _vector.Normalize( ) ) ) + 'This copies the values from the vector and returns the noramlized data.. It doesn\'t change the vector..\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> _vector.GetNormalized( ): ', 85, str( _vector.GetNormalized( ) ) ) + '\n\n\t\t\t^ This creates a new Vector with the Normalized data.. It doesn\'t change the vector..\n\t\t\tIt could be added as a Node to this Vector object, and when the Vector changes, using on_set callback, the Vector could be Normalized and saved into the Node - This is what the Versioning system does - when the version is updated, it is unpacked and processed ( removes decimals and treats it as a number but allows buffers to be added ).\n\t\t\tAlternatively, to make it more efficient, the Normalized Node could be populated only when Normalize or GetNormalized is called - then that data is micro-cached and a toggled-control would be enabled ( which would be reset when any Vector element [ x, y, z ] is updated ) so, if enabled then it would use the cached data and if disabled it means the data has changed and the normalized data is processed again and re-stored..\n';

		_text += '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.__serialize: ', 300, str( _vector.__serialize ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.__x: ', 300, str( _vector.__x ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.__y: ', 300, str( _vector.__y ) ) + '\n';
		## _text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.__z: ', 300, str( _vector.__z ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.GetXAccessor( ): ', 250, str( _vector.GetXAccessor( ) ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.GetYAccessor( ): ', 250, str( _vector.GetYAccessor( ) ) ) + '\n';
		_text += _depth_prefix + Text.FormatColumn( 30, '-> Vector.GetZAccessor( ): ', 250, str( _vector.GetZAccessor( ) ) ) + '\n';
		_text += '\n\t\t\tNote: I changed VectorBase Parent to Pos so x, y and z did not need to be duplicated - this means __x, __y, __z are inaccessible but we can still use Get<Name>Accessor( ) for the same result.. It also means I do not need to duplicate __str__ because they all use self.Serialize( )\n';


		return _text + '\n' + Acecool.Footer( ) + '\n\n';



##
##
##
class SteamException( Exception ):
	##
	##
	##
	def __init__( self ):
		super( ).__init__( );


##
## Steam API - Acecool's Edition...
##
class Steam( ):
	##
	## Library Helpers / Defaults / ENUMs, etc...
	##
	DEFAULT_STEAMID32		= 'STEAM_0:0:0';
	DEFAULT_STEAMID64		= 0;
	DEFAULT_STEAMID3		= '[U:0:0]';
	DEFAULT_BOT_STEAMID32	= 'BOT';
	DEFAULT_BOT_STEAMID64	= 0;
	DEFAULT_BOT_STEAMID3	= 'BOT';


	##
	##
	##
	def TranslateSteamIDDefaults( _bot = None ):
		## If we're supposed to return the bot defaults - return them...
		if ( _bot ):
			return Steam.DEFAULT_BOT_STEAMID32, Steam.DEFAULT_BOT_STEAMID64, Steam.DEFAULT_BOT_STEAMID3;

		## Return standard defaults
		return Steam.DEFAULT_STEAMID32, Steam.DEFAULT_STEAMID64, Steam.DEFAULT_STEAMID3;


	##
	## Converts a SteamID64 from a SteamID32
	##
	def SteamIDTo64( _id = None ):
		## If the ID isn't set, raise an exception?
		if ( not _id ):
			raise SteamException( 'SteamIDTo64 ->  Issue, _id has not been set!' );



	##
	## Converts a SteamID32 from a SteamID64
	##
	def SteamIDFrom64( _id = None ):
		## If the ID isn't set, raise an exception?
		if ( not _id ):
			raise SteamException( 'SteamIDFrom64 -> Issue, _id has not been set!' );



	##
	## Takes any type of SteamID ( 32, 64, or 3 / U ) Variant, and outputs in this order ( 32, 64, 3 / U ) ie: STEAM_0:1:4173055, 76561197968611839, [U:1:8346111]
	## Translate input SteamID, SteamID64 or SteamU into SteamID, SteamID64 AND SteamU. Return in same order 32, 64, U :: 'STEAM_0:1:4173055', 76561197968611839, '[U:1:8346111]'
	##
	## steamID			STEAM_0:1:4173055
	## steamID3			[U:1:8346111]
	## steamID64		76561197968611839
	## http://steamcommunity.com/id/Acecool
	## http://steamcommunity.com/profiles/76561197968611839
	##
	def TranslateSteamID( _id = None ):
		## SteamID Scope Trackers
		_steamid32				= _id;
		_steamid64				= -1;
		_steamid3U				= '';

		##
		_id_isstring			= isstring( _id );
		_id_isnumber			= isnumber( _id );

		## If the ID isn't set, raise an exception?
		if ( not _id ):
			raise SteamException( 'TranslateSteamID -> Any type of SteamID was not provided... id == None...' );
			return Steam.TranslateSteamIDDefault;

		## If we've used an entity...
		if ( not _id_isstring and not isnumber( _id ) ):
			raise SteamException( 'TranslateSteamID -> Entities ( Player or otherwise ) sre not supported in this version of Steam.TranslateSteamID...' );
			return Steam.TranslateSteamIDDefaults( True );


		## If we're looking at a bot via either using BOT, 0 or any non-player entity...
		## Note: Determine whether or not I should create the Player Object so it could be imported... Likely unnecessary.. -- or ( not _id_isstring and Steam.IsValidEntity( _id ) and not Steam.IsValidPlayer( _id ) )
		if ( ( _id_isstring and _id.upper( ) == 'BOT' ) or ( _id == 0 ) ):
			return Steam.TranslateSteamIDDefaults( True );

		## Helpers - Extract all numbers from the _id and determine if they are the same.. If they are then _id == SteamID64
		_steam_digits			= String.ExtractNumbers( _id );
		_is_len_same			= len( str( _id ) ) == len( str( _steam_digits ) );

		## If _id Input is SteamID3/U, ensure it is moved to correct var and calculate Steam32.
		if ( _id[ 1 : 2 ].upper( ) == 'U' ):
			## If UID was input, ensure it is moved to correct var and calculate Steam32.
			_steamid3U			= _id;

			## Remove first number from 1x after strip, before: [U:1:x] and convert to number.
			_id					= tonumber( string.sub( _steam_digits, 2 ) );

			## Even or odd?
			_is_even			= ( _id % 2 == 0 );
			_id					= math.floor( _id / 2 );

			## Set _steamid32 since we calculated it, and because we have the steamU we only need 64. Do it, and return.
			_steamid32 = 'STEAM_0:' + ( '0' if ( _is_even	) else '1' ) + ':' + _id;
			_steamid64 = Steam.SteamIDTo64( _steamid32 );


		else:
			## We're left with either 32 or 64 as options for input... A simple solution to verify 64bit input is by using the length of the string after removing all non-digits - if they remain the same, then the input is SteamID64..
			## If they are not the same length, generate 64 from 32 as it isn't 32... If they are the same len, then it is 64 so assign _id to 64... For 32, it's the opposite..
			## Because we are in else... It isn't U... So, because it isn't u, and len is dif, it must be 32 so generate 64 from 32... For 32, same... generate from 64 if len is same, otherwise keep 32 the same... which is _id from when I set at the start...
			_steamid64 = Steam.SteamIDTo64( _steamid32 ) if ( not _is_len_same ) else _id;
			_steamid32 = Steam.SteamIDFrom64( _steamid64 ) if ( _is_len_same ) else _steamid32;

			## Only convert if not input.. Conversion is [U:1:( ( x * 2 ) + y )] where x and y is STEAM_0:y:x
			if ( _steamid3U == '' ):
				_uidX = _steamid32[ 8 : 9 ]; #tonumber( string.sub( _steamid32, 9, 9 ) );
				_uidX = _steamid32[ 10 : ]; #tonumber( string.sub( _steamid32, 11 ) );
				if ( _uidY and _uidX ):
					_uidY = ( _uidY * 2 ) + _uidX;
					_steamid3U = '[U:1:' + _uidY + ']';



		## We're done..
		return _steamid32, _steamid64, _steamuid;


##
## Sublime Text Based Helpers
##
if ( import_if_exists( 'sublime' ) and import_if_exists( 'sublime_plugin' ) ):
	##
	## If Sublime and Sublime Plugin Packages exist, add the helper classes for it...
	##
	## if ( import_if_exists( 'sublime' ) and import_if_exists( 'sublime_package' ) ):
	##
	## Is Types...
	##

	## IsSublimeView
	## def IsSublimeView( _data ):		return _data != None and str( type( _data ) ) == '<class \'sublime.View\'>'

	## IsSublimeView
	def IsSublimeView( _view ):			return _view != None and isinstance( _view, sublime.View );


	##
	## Determines whether or not we're looking at a VALID Sublime View - typically there's an error when using file_name...
	##
	## return _view != None and _view.file_name( ) and IsSublimeView( _view )
	##
	def IsValidSublimeView( _view ):
		##
		return IsSublimeView( _view ) and _view.is_valid( ) and _view.file_name( ) != None;

		## ## Because of the issue with .file_name, we're using a try...
		## try:
		## 	if ( _view != None and IsSublimeView( _view ) ):
		## 		if ( _view.file_name( ) != None ):
		## 			return True;

		## 	return False;
		## except _error:
		## 	return False;

		## return False;




	##
	## class SublimeSettingsBase( ):
	## 	pass;
	##
	class SublimeSettings:
		##
		def Load( ):
			pass;

		##
		def Save( ):
			pass;

		##
		def DecodeJSON( ):
			pass;

		##
		def EncodeJSON( ):
			pass;


	## ##
	## ## Creates custom helper-functions for the Sublime Region Class
	## ##
	## ## Note: Are regions defined by character / columns? If so then each line needs to be tallied then when going from one line to another - the start for that line is all of the previous lines, then to end is the line on top of that plus all lines to end...
	## ##
	## class SublimeRegion( sublime.Region ):
	## 	##
	## 	## Creates a region based on line-number instead of character number...
	## 	##
	## 	def DefineByLineNumber( self, _text, _start, _end ):
	## 		pass;


	## ##
	## ##
	## ##
	## class SublimeView( sublime.View ):
	## 	pass



	##
	## Acecool Sublime Text Library - This is not an object - self is not used...
	##
	## class SublimeBase( ):
	## 	pass;
	class Sublime:
		##
		## Important File-Names
		##

		## File-Names
		PLUGIN_FILE_NAME_CORE_DEFS					= 'AcecoolCodeMappingSystemDefinitions.py';
		PLUGIN_FILE_NAME_CORE_LIB					= 'AcecoolLib_SublimeText3.py';
		PLUGIN_FILE_NAME_CORE						= 'AcecoolCodeMappingSystem.py';

		## DEFAULT Core Key Binding Files
		PLUGIN_FILE_NAME_KEYS_ALL					= 'Default.sublime-keymap';
		PLUGIN_FILE_NAME_KEYS_WIN					= 'Default (Windows).sublime-keymap';
		PLUGIN_FILE_NAME_KEYS_LINUX					= 'Default (Linux).sublime-keymap';
		PLUGIN_FILE_NAME_KEYS_OSX					= 'Default (OSX).sublime-keymap';

		## DEFAULT Core Config Files
		PLUGIN_FILE_NAME_CFG_DEFS					= 'Default_PluginDefinitions.sublime-settings';
		PLUGIN_FILE_NAME_CFG_CORE					= 'Default_PluginSettings.sublime-settings';
		PLUGIN_FILE_NAME_CFG_MAP					= 'Default_MapSettings.sublime-settings';

		## DEFAULT Mapping Class Config Files - This will be reduced to dynamic system MappSettings_<EXT>.sublime-settings...
		PLUGIN_FILE_NAME_CFG_MAP_AHK				= 'MapSettings_ahk.sublime-settings';
		PLUGIN_FILE_NAME_CFG_MAP_BAT				= 'MapSettings_bat.sublime-settings';
		PLUGIN_FILE_NAME_CFG_MAP_CPP				= 'MapSettings_cpp.sublime-settings';
		PLUGIN_FILE_NAME_CFG_MAP_JS					= 'MapSettings_js.sublime-settings';
		PLUGIN_FILE_NAME_CFG_MAP_LUA				= 'MapSettings_lua.sublime-settings';
		PLUGIN_FILE_NAME_CFG_MAP_MD					= 'MapSettings_md.sublime-settings';
		PLUGIN_FILE_NAME_CFG_MAP_PHP				= 'MapSettings_php.sublime-settings';
		PLUGIN_FILE_NAME_CFG_MAP_PY					= 'MapSettings_py.sublime-settings';
		PLUGIN_FILE_NAME_CFG_MAP_SUBLIME_SETTINGS	= 'MapSettings_sublime-settings.sublime-settings';
		PLUGIN_FILE_NAME_CFG_MAP_TS					= 'MapSettings_ts.sublime-settings';
		PLUGIN_FILE_NAME_CFG_MAP_TXT				= 'MapSettings_txt.sublime-settings';


		##
		## Important folder-Names, and other important information...
		##

		## The main Package name... This is %AppData%\Sublime Text 3\Packages\<PLUGIN_FOLDER_NAME> and %AppData%\Sublime Text 3\Packages\<SUBLIME_FOLDER_NAME_USER>\<PLUGIN_FOLDER_NAME_USER>
		PLUGIN_FOLDER_NAME							= 'AcecoolCodeMappingSystem';
		## PLUGIN_FOLDER_NAME_USER						= 'AcecoolCodeMappingSystem';

		## Sublime Text Folder Names
		SUBLIME_FOLDER_NAME_RES						= 'res://';
		SUBLIME_FOLDER_NAME_PACKAGES				= 'Packages';
		SUBLIME_FOLDER_NAME_DEFAULT					= 'Default';
		SUBLIME_FOLDER_NAME_USER					= 'User';

		## The folder where settings files are kept...
		PLUGIN_FOLDER_NAME_SYNTAX_HIGHLIGHTERS		= 'syntax_files';
		PLUGIN_FOLDER_NAME_SETTINGS					= 'settings';
		PLUGIN_FOLDER_NAME_MAPPING_CLASSES			= 'classes';
		PLUGIN_FOLDER_NAME_CACHE					= 'cache';
		PLUGIN_FOLDER_NAME_BACKUP					= 'backup';


		##
		##
		##
		def IsView( _view = None ):												return _view != None and isinstance( _view, sublime.View );


		##
		##
		##
		def IsValidView( _view = None ):										return Sublime.IsView( _view ) and _view.is_valid( ) and _view.file_name( ) != None;


		##
		## Returns a path to an actual file ( checks plugin extract status, if a file exists vs if in res:// etc.. )
		##
		def GetFilePath( ):
			pass

		## Plugin Core File Names - unbuilt
		def GetFileNamePluginCoreDefs( ):										return Sublime.PLUGIN_FILE_NAME_CORE_DEFS;				# XCodeMapper_Definitions.py
		def GetFileNamePluginCoreLib( ):										return Sublime.PLUGIN_FILE_NAME_CORE_LIB;				# AcecoolST3_Library.py
		def GetFileNamePluginCore( ):											return Sublime.PLUGIN_FILE_NAME_CORE;					# XCodeMapper.py

		## Plugin Core Config File Names - unbuilt
		def GetFileNamePluginCfgDefs( ):										return Sublime.PLUGIN_FILE_NAME_CFG_DEFS;				# Default_PluginDefinitions.sublime-settings
		def GetFileNamePluginCfgCore( ):										return Sublime.PLUGIN_FILE_NAME_CFG_CORE;				# Default_PluginSettings.sublime-settings
		def GetFileNamePluginCfgMap( ):											return Sublime.PLUGIN_FILE_NAME_CFG_MAP;					# Default_MapSettings.sublime-settings
		def GetFileNamePluginMapSettings( _ext = 'py' ):						return 'MapSettings_' + str( _ext ) + '.sublime-settings';		# MapSettings_abc.sublime-settings

		## Plugin Core Config Key Binding File Names - unbuilt
		def GetFileNamePluginCfgKeysWin( ):										return Sublime.PLUGIN_FILE_NAME_KEYS_WIN;				# Default (Windows).sublime-keymap
		def GetFileNamePluginCfgKeysLin( ):										return Sublime.PLUGIN_FILE_NAME_KEYS_LINUX;				# Default (Linux).sublime-keymap
		def GetFileNamePluginCfgKeysOSX( ):										return Sublime.PLUGIN_FILE_NAME_KEYS_OSX;				# Default (OSX).sublime-keymap


		## Sublime Core Folder Names - unbuilt
		def GetFolderNameRes( *_paths ):										return Sublime.GetBasicPath( Sublime.SUBLIME_FOLDER_NAME_RES, *_paths );					# res://
		def GetFolderNamePackages( *_paths ):									return Sublime.GetBasicPath( Sublime.SUBLIME_FOLDER_NAME_PACKAGES, *_paths );				# Packages
		def GetFolderNameDefault( *_paths ):									return Sublime.GetBasicPath( Sublime.SUBLIME_FOLDER_NAME_DEFAULT, *_paths );				# Default
		def GetFolderNameUser( *_paths ):										return Sublime.GetBasicPath( Sublime.SUBLIME_FOLDER_NAME_USER, *_paths );					# User

		## Plugin Core Folder Names - unbuilt
		def GetFolderNamePlugin( *_paths ):										return Sublime.GetBasicPath( Sublime.PLUGIN_FOLDER_NAME, *_paths );							# Acecool_CMS
		## def GetFolderNamePluginUser( *_paths ):								return Sublime.GetBasicPath( Sublime.PLUGIN_FOLDER_NAME_USER, *_paths );					# Acecool_CMS
		def GetFolderNamePluginSettings( *_paths ):								return Sublime.GetBasicPath( Sublime.PLUGIN_FOLDER_NAME_SETTINGS, *_paths );				# settings
		def GetFolderNamePluginSyntax( *_paths ):								return Sublime.GetBasicPath( Sublime.PLUGIN_FOLDER_NAME_SYNTAX_HIGHLIGHTERS, *_paths );		# syntax_files
		def GetFolderNamePluginMapClasses( *_paths ):							return Sublime.GetBasicPath( Sublime.PLUGIN_FOLDER_NAME_MAPPING_CLASSES, *_paths );			# classes
		def GetFolderNamePluginCache( *_paths ):								return Sublime.GetBasicPath( Sublime.PLUGIN_FOLDER_NAME_CACHE, *_paths );					# cache
		def GetFolderNamePluginBackup( *_paths ):								return Sublime.GetBasicPath( Sublime.PLUGIN_FOLDER_NAME_BACKUP, *_paths );					# backup

		## Helpers
		def GetCleanPath( _path ):												return _path.replace( '\\', '/' )
		def GetBasicPath( *_paths ):											return Sublime.GetCleanPath( os.path.join( *_paths ) );



		## Base Sublime Paths - For res:// || Packages/ || Packages/ || Packages/User/
		def GetPathPackages( *_paths ):											return Sublime.GetBasicPath( sublime.packages_path( ), *_paths );
		def GetPathPackagesUser( *_paths ):										return Sublime.GetPathPackages( Sublime.GetFolderNameUser( *_paths ) );
		def GetPathRes( *_paths ):												return Sublime.GetFolderNameRes( *_paths );
		def GetPathResPackages( *_paths ):										return Sublime.GetPathRes( Sublime.GetFolderNamePackages( *_paths ) );
		def GetPathResPackagesUser( *_paths ):									return Sublime.GetPathResPackages( Sublime.GetFolderNameUser( *_paths ) );

		## Base Sublime Paths with a direct link to the root directory of this plugin...
		def GetPathPackagesPlugin( *_paths ):									return Sublime.GetPathPackages( Sublime.GetFolderNamePlugin( *_paths ) );
		def GetPathPackagesUserPlugin( *_paths ):								return Sublime.GetPathPackagesUser( Sublime.GetFolderNamePlugin( *_paths ) );
		def GetPathResPackagesPlugin( *_paths ):								return Sublime.GetPathResPackages( Sublime.GetFolderNamePlugin( *_paths ) );
		def GetPathResPackagesUserPlugin( *_paths ):							return Sublime.GetPathResPackagesUser( Sublime.GetFolderNamePlugin( *_paths ) );

		## This combines the path suffix with the path prefix ( if any )
		def GetPath( _mode = PATH_MODE_DEFAULT, *_paths ):
			##
			_suffix = Sublime.GetBasicPath( *_paths );

			print( ' >>>> Suffix from GetPath is: ' + _suffix );

			##
			if ( _mode > PATH_MODE_DEFAULT ):
				##
				if( _mode == PATH_MODE_PLUGIN ):
					return Sublime.GetPathPackagesPlugin( _suffix );

				##
				if ( _mode == PATH_MODE_PLUGIN_USER ):
					return Sublime.GetPathPackagesUserPlugin( _suffix );

				##
				if ( _mode == PATH_MODE_PLUGIN_RES ):
					return Sublime.GetPathResPackagesPlugin( _suffix );

				##
				if ( _mode == PATH_MODE_PLUGIN_RES_USER ):
					return Sublime.GetPathResPackagesUserPlugin( _suffix );

				##
				## if ( _mode == PATH_MODE_CODEMAP_SYNTAX ):
				##	return Sublime.GetFolderNamePackages( _suffix );

			##
			return _suffix;

		## Partial paths without prefix - so these can be used for Packages/<Name>/* or Packages/User/<Name>/*
		## def GetPathPluginSettings( _mode = PATH_MODE_DEFAULT, *_paths ):		return Sublime.GetFolderNamePlugin( Sublime.GetFolderNamePluginSettings( *_paths ) );
		def GetPluginSyntaxPath( _mode = PATH_MODE_DEFAULT, *_paths ):			return Sublime.GetFolderNamePlugin( Sublime.GetFolderNamePluginSyntax( *_paths ) );
		## def GetPathPluginMapClasses( _mode = PATH_MODE_DEFAULT, *_paths ):		return Sublime.GetFolderNamePlugin( Sublime.GetFolderNamePluginMapClasses( *_paths ) );
		## def GetPathPluginCache( _mode = PATH_MODE_DEFAULT, *_paths ):			return Sublime.GetFolderNamePlugin( Sublime.GetFolderNamePluginCache( *_paths ) );
		## def GetPathPluginBackup( _mode = PATH_MODE_DEFAULT, *_paths ):			return Sublime.GetFolderNamePlugin( Sublime.GetFolderNamePluginBackup( *_paths ) );

		##
		def FileExists( _path ):												return os.path.exists( Sublime.GetPathPackages( _path ) ) or os.path.exists( _path );
		## def GetDefaultSyntaxFilePath( ):										return os.path.join( Sublime.SUBLIME_FOLDER_NAME_PACKAGES, Sublime.GetViewSetSyntaxFile( ) );
		## def GetFileTimestamp( _path ):											pass;

		## The way set_syntax needs the file is Packages/PackageName/FileName.sublime-syntax or tmLanguage ...
		def GetViewSetSyntaxPath( _package, *_paths ):							return Sublime.GetBasicPath( _package, *_paths );
		## def GetPathCodeMapXPanelSyntax( *_paths ):								return Sublime.GetFolderNamePackages( *_paths );

		## Partially built paths... ( Without base - so )
		def GetViewSetSyntaxFile( ):									return Sublime.GetBasicPath( 'Python', 'Python.sublime-syntax' );

		## Determines which Syntax Highlighter to use... tmLanguage or sublime-syntax...
		def GetSyntaxFilePath( _package, _file = '' ):
			##
			## print( ' >> GetSyntaxFilePath > Attempting to locate Syntax Highlighter File...' )

			##
			if ( _file == '' ):
				_file = _package;

			## Keep repetitions down to define it once up here and use them twice each below... more if additional folders are added...
			_file_tm					= _file + '.tmLanguage';
			_file_sy					= _file + '.sublime-syntax';

			## Table containing possible locations and file-names...
			_syntax_files				= [ ];

			##  - Pre version 3092 allows only tmLanguage... So we only add tmLanguage if the version is less than 3092... Otherwise we allow both...
			_allow_sublime_syntax		= ( int( sublime.version( ) ) >= 3084 );

			## Acecool/syntax_files/<FileName>.<tmLanguage / sublime-syntax>
			_syntax_files.append( Sublime.GetPluginSyntaxPath( PATH_MODE_DEFAULT, _file_tm ) );

			## If version is 3092 or higher, we allow sublime-syntax files..
			if ( _allow_sublime_syntax ):
				_syntax_files.append( Sublime.GetPluginSyntaxPath( PATH_MODE_DEFAULT, _file_sy ) );

			## <PackageName>/<FileName>.<tmLanguage / sublime-syntax>
			_syntax_files.append( Sublime.GetViewSetSyntaxPath( _package, _file_sy ) ) #os.path.join( _package, _file_sy ) );

			## If version is 3092 or higher, we allow sublime-syntax files..
			if ( _allow_sublime_syntax ):
				_syntax_files.append( Sublime.GetViewSetSyntaxPath( _package, _file_tm ) ) #os.path.join( _package, _file_tm ) );

			## Allow tmLanguage always, only allow sublime-syntax if over 3092...
			if ( Sublime.GetViewSetSyntaxFile( ).endswith( 'tmLanguage' ) or ( _allow_sublime_syntax and Sublime.GetViewSetSyntaxFile( ).endswith( 'sublime-syntax' ) ) ):
				_syntax_files.append( Sublime.GetViewSetSyntaxFile( ) );

			## Process the locations above in search of the right one...
			for _path in _syntax_files:
				if ( _path == None ):
					raise Exception( 'AcecoolLib -> Sublime.GetSyntaxFilePath -> Error with loop, _path is None which should never happen...' );


				##
				## print( '\t\t>> Trying: ' + _path );

				##
				if ( Sublime.FileExists( _path ) ):
				## if ( File.Exists( _path ) ):
					##
					print( '\t\t\t>> Setting Syntax Highlighter File: ' + _path );
					## print( '\t\t\t>> From folder: ' + SUBLIME_PATH_PACKAGES );
					## print( '\t>> Returned as: ' + Sublime.GetPathCodeMapXPanelSyntax( _path ) );
					## print( '\t>> Returned as: ' + Sublime.GetPluginSyntaxPath( PATH_MODE_CODEMAP_SYNTAX, _path ) );
					## print( '\t>> Returned as: Packages/' + _path );
					## print( '\t\t>> Testing: ' + os.path.join( 'Python' ) );

					##
					return 'Packages/' + _path;
					## return _path;


			## _syntax_files.append( Sublime.GetCleanPath( os.path.join( Sublime.GetPluginSyntaxPath( ), _file_tm ) ) );
			## _syntax_files.append( Sublime.GetCleanPath( os.path.join( Sublime.GetPluginSyntaxPath( ), _file_sy ) ) );
			## Base Path through AppData\Sublime Text 3\Packages\ - Unfortunately CodeMap doesn't support using the full-path.... which is why I can't use it in the return statement...
			## _base_path = SUBLIME_PATH_PACKAGES;

		##
		## Specifics...
		##
		## ## Path Builders - These are the prefixes for all of the above GetPathPlugin* functions...
		## def GetPathPluginCache( ):												return Sublime.GetPathPackagesUserPlugin( Sublime.PLUGIN_FOLDER_NAME_CACHE );
		## def GetFullPathPackagesX( *_paths ):									return Sublime.GetPathPackages( *_paths );
		## def GetFullPathPackagesUserX( *_paths ):								return Sublime.GetPathPackagesUser( *_paths );
		## def GetUserFilesPath( ):												return Sublime.GetPathPackages( 'User' );
		## Path Builders
		## def GetPathBuildRes( *_paths ):											return Sublime.GetPathRes( *_paths );
		## def GetPathBuildResPackages( *_paths ):									return Sublime.GetPathRes( Sublime.GetFolderNamePackages( *_paths ) );
		## def GetPathBuildResPackagesPlugin( *_paths ):							return Sublime.GetPathRes( Sublime.GetFolderNamePackages( Sublime.GetFolderNamePlugin( *_paths ) );
		## def GetPathPackagesPluginX( *_paths ):									return Sublime.GetPathPackages( Sublime.GetFolderNamePlugin( *_paths ) );
		## def GetPathPackagesUserPluginX( *_paths ):								return Sublime.GetPathPackagesUser( Sublime.GetFolderNamePlugin( *_paths ) );



		## ##
		## ## Creates all of the important folders in the User folder and either generates files, copies them, etc... Then the plugin can be 1 folder and install outwards to what is needed.. including replacing CodeMap files...
		## ##
		## def CreateFolders( *_varargs ):
		## 	## As long as VarArgs are entered
		## 	if ( _varargs != None and len( _varargs ) > 0 ):
		## 		## For each path added
		## 		for _path in _varargs:
		## 			## If it doesn't already exist
		## 			if ( not os.path.exists( _path ) ):
		## 				## Try to create it...
		## 				try:
		## 					pass;
		## 				except Exception:
		## 					pass;
		## 	pass;















		##
		##
		##

		## Deprecated / Helper - The Package Name for the package and package_user variable replacers...
		## Task: Automate this to use the package which is CALLING this function...
		__package_name__																		= 'AcecoolCodeMappingSystem';

		## Deprecated / Helper - The Package Repo for issues link, download, and more links...
		__package_repo__																		= 'https://bitbucket.org/Acecool/acecooldev_sublimetext3';

		##
		__settings_file_sublime__																= 'Preferences.sublime-settings';
		__settings_file_platform__																= 'Preferences (${platform}).sublime-settings';
		__settings_file_syntax__																= '${syntax}.sublime-settings';


		##
		## Helper - Returns the Package Folder Name Dynamically
		##
		def GetPackageName( ):																	return GetThisPackageName( );


		## Sublime Text Preferences - Specifically for the Operating System in Use...
		def GetSyntaxSettingsFileName( _view = None ):											return sublime.expand_variables( __settings_file_syntax__, { 'syntax': Sublime.GetSyntaxName( _view ) } );
		def GetSyntaxSettingsObject( ):															return Sublime.GetSettingsObject( Sublime.GetPlatformSettingsFileName( ) );
		def SaveSyntaxSettings( ):																return Sublime.SaveSettings( Sublime.GetPlatformSettingsFileName( ) );
		def GetSyntaxSetting( _key, _default = None ):											return Sublime.GetSyntaxSettingsObject( ).get( _key, _default );
		def SetSyntaxSetting( _key, _value = None ):											return Sublime.GetSyntaxSettingsObject( ).set( _key, _value );

		## Sublime Text Preferences
		def GetSublimeSettingsFileName( _view = None ):											return __settings_file_sublime__
		def GetSublimeSettingsObject( ):														return Sublime.GetSettingsObject( Sublime.GetPlatformSettingsFileName( ) );
		def SaveSublimeSettings( ):																return Sublime.SaveSettings( Sublime.GetPlatformSettingsFileName( ) );
		def GetSublimeSetting(_key, _default = None ):											return Sublime.GetSublimeSettingsObject( ).get( _key, _default );
		def SetSublimeSetting(_key, _value = None ):											return Sublime.GetSublimeSettingsObject( ).set( _key, _value );

			## Sublime Text Preferences - Specifically for the Operating System in Use...
		def GetPlatformSettingsFileName( ):														return sublime.expand_variables( __settings_file_platform__, { 'platform': Sublime.GetPlatformName( ) } );
		def GetPlatformSettingsObject( ):														return Sublime.GetSettingsObject( Sublime.GetPlatformSettingsFileName( ) );
		def SavePlatformSettings( ):															return Sublime.SaveSettings( Sublime.GetPlatformSettingsFileName( ) );
		def GetPlatformSetting( _key, _default = None ):										return Sublime.GetDefinitionsSettingsObject( ).get( _key, _default );
		def SetPlatformSetting( _key, _value = None ):											return Sublime.GetDefinitionsSettingsObject( ).set( _key, _value );


		##
		def ToggleConsole( ):
			Sublime.TogglePanel( 'console' );

		##
		def ToggleBuildPanel( ):
			Sublime.TogglePanel( 'build' );


		##
		def ToggleOutputPanel( ):
			Sublime.TogglePanel( 'output' );


		##
		def TogglePanel( _name = 'ACMS' ):
			sublime.run_command( 'show_panel',  { 'panel': _name, 'toggle': True } );

		##
		def Settings( _name ):
			return sublime.load_setting( _name );

		##
		def SetSetting( _name, _key, _value ):
			return Sublime.Settings( _name ).set( _key, _value );

		##
		def SaveSettings( _name ):
			return sublime.save_setting( _name );

		## Convert the Platform name into the Correct Case / Variant so the file can be found on *nix systems...
		def GetPlatformName( ):
			return {
				'osx':			'OSX',
				'windows':		'Windows',
				'linux':		'Linux',
			}[ sublime.platform( ) ];


		##
		##
		##
		__default_syntax_name__ = 'Python';
		__default_syntax_ext__ = 'tmLanguage';
		__default_syntax_name__ = 'Python/';

		##
		def GetSyntaxName( _view = None ):
			_view = Sublime.GetView( _view );

			if ( Sublime.IsValidView( _view ) ):
				_syntax, _syntax_ext = os.path.splitext( os.path.basename( _view.settings( ).get( 'syntax' ) ) );
				return _syntax;

			return Sublime.__default_syntax_name__;

		##
		def GetSyntaxExt( _view = None ):
			_view = Sublime.GetView( _view );

			if ( Sublime.IsValidView( _view ) ):
				_syntax, _syntax_ext = os.path.splitext( os.path.basename( _view.settings( ).get( 'syntax' ) ) );
				return _syntax_ext;

			return Sublime.__default_syntax_ext__;

		##
		def GetSyntaxPath( _view = None ):
			_view = Sublime.GetView( _view );

			if ( Sublime.IsValidView( _view ) ):
				return _view.settings( ).get( 'syntax' );

			return Sublime.__default_syntax_name__;


		##
		def DelayCall( _delay, _callback, *_args ):											sublime.set_timeout( _callback( *_args ), _delay );

		## Clear Console by spamming new lines...
		def ClearConsole( _count = 100 ):													print( '\n' * _count );


		##
		## Window -> Project Related
		##

		## Returns the name of the currently open project file, if applicable
		def GetProjectFileName( _window = None ):											return Sublime.GetWindow( _window ).project_file_name( );

		## Returns the project data associated with the current window. The data is in the same format as the contents of a .sublime-project file.
		def GetProjectData( _window = None ):												return Sublime.GetWindow( _window ).project_data( );

		## Updates the project data associated with the current window. If the window is associated with a .sublime-project file, the project file will be updated on disk, otherwise the window will store the data internally.
		def SetProjectData( _data, _window = None ):										return Sublime.GetWindow( _window ).set_project_data( _data );

		##
		def SetWindowLayout( _window = None ):
			## This should set a left window and a right window - the right one with 3 stacked panels...
			Sublime.GetWindow( _window ).set_layout( {
				## Column Sizes
				"cols":		[
					## This is the starting position - this needs to exist for all... so the minimum is 2 entries for this list...
					0.0,

					## Column 1 starts at the 0% mark and proceeds through 70% of the window width
					0.70,

					## Column 2 starts at the 70% mark and proceeds through 100% of the window width
					1.0,
				],

				## Row Sizes
				"rows":		[
					## This is the starting position - this needs to exist for all... so the minimum is 2 entries for this list...
					0.0,

					## Row 1 is 0 to 33% height
					0.33,

					## Row 2 is 33% to 66%
					0.66,

					## Row 3 is 66% to 100%
					1.0,
				],

				## Cells Structure is x1, y1, x2, y2 for column == x, row = y - Unlike cols and rows, the cells list contains as many entries are there are panels...
				"cells":	[
								## Column 0 to 1, row 0 to 3 - this is the main panel which extends all 3 rows
								[ 0, 0, 1, 3 ],

								## column 1 to 2, row 0 to 1 - ie the right column, top row
								[ 1, 0, 2, 1 ],

								## Column 1 to 2, row 1 to 2 - ie the right column, middle row
								[ 1, 1, 2, 2 ],

								## Column 1 to 2, row 2 to 3 - ie the right column, bottom row...
								[ 1, 2, 2, 3 ],
							],
			} );



		##
		## Window -> Symbol Related
		##

		## Returns all locations where the symbol is defined across files in the current project.
		def GetAllSymbolLocationsByProject( _symbol, _window = None ):					return Sublime.GetWindow( _window ).lookup_symbol_in_index( _symbol );

		## Returns all locations where the symbol is defined across open files.
		def GetAllSymbolLocationsByOpenFiles( _symbol ):								return Sublime.GetWindow( _window ).lookup_symbol_in_open_files( _symbol );

		## Returns a dictionary of strings populated with contextual keys: packages, platform, file, file_path, file_name, file_base_name, file_extension, folder, project, project_path, project_name, project_base_name, project_extension. This dict is suitable for passing to sublime.expand_variables().
		def GetAllVariables( ):															return Sublime.GetWindow( _window ).extract_variables( );


		##
		## Window -> Group Related
		##

		## Returns the number of groups in the window
		def GetGroupCount( _window = None ):											return Sublime.GetWindow( _window ).num_groups( );

		## Returns the number of groups in the window
		def GetActiveGroup( _window = None ):											return Sublime.GetWindow( _window ).active_group( );

		## Sets the group provided as active
		def SetActiveGroup( _group, _window = None ):									return Sublime.GetWindow( _window ).focus_group( _group );


		##
		## Window -> Panel Related
		##

		## Shows a quick panel, to select an item in a list. on_done( selected_item_index or -1 on canceled ) callback, items = [ string ] each entry row or [ [ stirngs ], .. ] entry shows multi-rows, flags = bitwise sublime.MONOSPACE_FONT and KEEP_OPEN_ON_FOCUS_LOST, on_selected( ) callback each time
		def ShowPanel( _items, _on_done, _flags, _index, _on_selected = None ):			return Sublime.GetWindow( _window ).show_quick_panel( _items, _on_done, _flags, _index, _on_selected );

		## Shows the input panel, to collect a line of input from the user. on_done and on_change, if not None, should both be functions that expect a single string argument. on_cancel should be a function that expects no arguments. The view used for the input widget is returned.
		def ShowInputPanel( _title, _text, _on_done, _on_change, _on_cancel ):			return Sublime.GetWindow( _window ).show_input_panel( _title, _text, _on_done, _on_change, _on_cancel );

		##Returns the view associated with the named output panel, creating it if required. The output panel can be shown by running the show_panel window command, with the panel argument set to the name with an "output." prefix.
		def CreateOutputPanel( _name, _show_in_switcher = False ):						return Sublime.GetWindow( _window ).create_output_panel( _name, _show_in_switcher );

		## Destroys the named output panel, hiding it if currently open.
		def RemoveOutputPanel( _name ):													return Sublime.GetWindow( _window ).destroy_output_panel( _name );
		## Returns the name of the currently open panel, or None if no panel is open. Will return built-in panel names (e.g. "console", "find", etc) in addition to output panels.
		def GetActivePanel( ):															return Sublime.GetWindow( _window ).active_panel( );

		## Returns a list of the names of all panels that have not been marked as unlisted. Includes certain built-in panels in addition to output panels.
		def GetAllPanels( ):															return Sublime.GetWindow( _window ).panels( );


		##
		## Window -> Window Related
		##

		## Returns the Active Window or the Window provided via reference
		def GetWindow( _window = None ):												return ( _window, Sublime.GetActiveWindow( ) )[ _window == None ];

		## Returns the Window ID - If no window is provided, it uses the Active Window
		def GetwindowID( _window = None ):												return Sublime.GetWindow( _window ).id( );

		## Returns the Sublime Text Window which has focus
		def GetActiveWindow( ):															return sublime.active_window( );

		## Returns the group and index within the group of the sheet, returns -1 if not found
		def GetWindowBySheet( _sheet = None, _window = None ):							return Sublime.GetSheet( _sheet, _window ).window( );


		##
		## Window -> View Related
		##

		## Returns the Active View or the View provided via reference
		def GetView( _view = None, _window = None ):									return ( _view, Sublime.GetActiveView( _window ) )[ _view == None ];

		## Returns the View ID - If no view is provided, it uses the Active View
		def GetViewID( _view = None ):													return Sublime.GetView( _view ).id( );

		## Returns the group and index within the group of the view, returns -1 if not found
		def GetViewIndex( _view, _window = None ):										return Sublime.GetWindow( _window ).get_view_index( _view );

		## Moves the view to the given group and index
		def SetViewIndex( _view, _group, _index, _window = None ):						return Sublime.GetWindow( _window ).set_view_index( _view, _group, _index );

		## Returns the File, which is a View object, in focus
		def GetActiveView( _window = None ):											return Sublime.GetWindow( _window ).active_view( );

		## Sets the View provided as active - ie switches to the given view
		def SetActiveView( _view, _window = None ):										return Sublime.GetWindow( _window ).focus_view( _view );

		## Returns all open sheets in window
		def GetAllViews( _window = None ):												return Sublime.GetWindow( _window ).views( );

		## Returns the group and index within the group of the sheet, returns -1 if not found
		def GetViewBySheet( _sheet = None, _window = None ):							return Sublime.GetSheet( _sheet, _window ).view( );

		## Returns the View based on a file-name
		def GetViewByFileName( _file, _window = None ):									return Sublime.GetWindow( _window ).find_open_file( _file );

		## Returns the view associated with the named output panel, or None if the output panel does not exist.
		def GetViewByOutputPanel( _name ):												return Sublime.GetWindow( _window ).find_output_panel( _name );

		## Returns all open sheets in window
		def GetAllGroupedViews( _group, _window = None ):								return Sublime.GetWindow( _window ).views_in_group( _group );

		## Returns the actively focused sheet in the actively focused window, or window provided
		def GetActiveGroupedView( _group, _window = None ):								return Sublime.GetWindow( _window ).active_view_in_group( _group );

		## Sets the syntax highlighter file to be used for the given view...
		def SetViewSyntaxFile( _view, _syntax_file = 'Python\\Python.sublime-syntax' ): return _view.set_syntax_file( _syntax_file );

		## Returns a region containing the entire file contents
		def GetViewContentsRegion( _view ):												return sublime.Region( 0, TernaryFunc( _view != None, _view.size( ), 0 ) );

		##
		def ViewInsertSnippet( _view = None ):
			_view.run_command( 'insert', { 'characters': '\n' } );
			_view.run_command( 'insert_snippet', { 'contents': "\n\t$0\n"});

		## Returns whether or not a view is read only - an invalid view doesn't exist ergo technically it is read only... should I return false or none?
		def IsViewReadOnly( _view ):
			if ( IsValidSublimeView( _view ) ):
				return _view.is_read_only( );

			return True;

		## Sets the syntax highlighter file to be used for the given view...
		def SetViewReadOnly( _view, _value = False ):
			if ( IsValidSublimeView( _view ) ):
				return _view.set_read_only( _value );

			return False;

		## Sets the Scratch Value for the given view...
		def SetViewScratch( _view, _value = False ):
			if ( IsValidSublimeView( _view ) ):
				return _view.set_scratch( _value );

			return False;

		##
		def GetViewFileExt( _view = None ):
			return Sublime.GetViewFileName( _view ).split( '.' )[ - 1 ];

		##
		def GetViewFileName( _view = None, _extended = False ):
			if ( IsValidSublimeView( _view ) ):
				_filename = _view.file_name( );
				if ( not _extended ):
					return _filename.split( '\\' )[ - 1 ];
				else:
					return _filename;

			return '';


		##
		def ResetViewHorizontalScroll( _view = None ):
			## Grab the Y position so we only scroll left without going up or down
			_pos_y = _view.text_to_layout( _view.visible_region( ).a )[ 1 ];

			## Scroll left...
			_view.set_viewport_position( ( 0, _pos_y ), False );

		##
		def ScrollViewToLine( _view = None, _line = 1 ):
			## Scroll...
			_point = Sublime.GetLineCharsOffset( _view, _line );
			_region = sublime.Region( _point, _point );
			_line = Sublime.GetViewLineNumberByRegion( _view, _region );

			_point = _view.text_point( _line - 1, 0 );
			_selection = _view.line( _point );

			##
			_view.sel( ).clear( );
			_view.sel( ).add( _selection );
			_view.show_at_center( _selection );
			_view.show( _point );



		##
		## Window -> Sheet Related
		##

		## Returns the Active Sheet or the Sheet using the sheet reference or by using the Window class to grab the active sheet
		def GetSheet( _sheet = None, _window = None ):									return ( _sheet, Sublime.GetActiveSheet( _window ) )[ _sheet == None ];

		## Returns the group and index within the group of the sheet, returns -1 if not found
		def GetSheetId( _sheet = None, _window = None ):								return Sublime.GetSheet( _sheet, _window ).id( );

		## Returns all open sheets in window
		def GetAllSheets( _window = None ):												return Sublime.GetWindow( _window ).sheets( );

		## Returns all open sheets in window
		def GetAllGroupedSheets( _group, _window = None ):								return Sublime.GetWindow( _window ).sheets_in_group( _group );

		## Returns the actively focused sheet in the actively focused window, or window provided
		def GetActiveGroupedSheet( _group, _window = None ):							return Sublime.GetWindow( _window ).active_sheet_in_group( _group );

		## Returns the group and index within the group of the sheet, returns -1 if not found
		def GetSheetIndex( _sheet, _window = None ):									return Sublime.GetWindow( _window ).get_sheet_index( _sheet );

		## Moves the sheet to the given group and index
		def SetSheetIndex( _sheet, _group, _index, _window = None ):					return Sublime.GetWindow( _window ).set_sheet_index( _sheet, _group, _index );

		## Returns the actively focused sheet in the actively focused window, or window provided
		def GetActiveSheet( _window = None ):											return Sublime.GetWindow( _window ).active_sheet( );

		## Sets the sheet provided as active - ie switches to the given sheet
		def SetActiveSheet( _sheet, _window = None ):									return Sublime.GetWindow( _window ).focus_sheet( _sheet );


		##
		## Window -> Line Related
		##

		## Returns the Character Offset from the start of the file to the beginning of the row + num chars to the chosen column
		def GetLineCharsOffset( _view, _row = 0, _col = 0 ):							return _view.text_point( _row - 1, _col );

		## Return the line number from a provided Region Tuple, or from the char value...
		def GetViewLineNumberByRegion( _view, _region ):								return _view.rowcol( _region.begin( ) )[ 0 ] + 1;


		##
		## Window -> Operations
		##

		## Opens a file in the provided window or the active window
		def OpenFile( _file, _flags, _window = None ):									return Sublime.GetWindow( _window ).open_file( _file, _flags );

		## Opens a file in the provided window or the active window
		def CloseView( _file, _flags, _view = None, _return_focus_view = None ):
			##
			_window = Sublime.GetWindow( _view );

			## Make sure we're focused on the mapping panel view before we execute close...
			Sublime.RunCommand( 'focus_view', _window, _view );

			## Close the view...
			Sublime.RunCommand( 'close', _window );

			## If we have to refocus a different view, then set it here..
			if ( _return_focus_view != None ):
				Sublime.RunCommand( 'focus_view', Sublime.GetWindow( _return_focus_view ), _return_focus_view );

			##
			return True;


		def CloseFile( _file, _flags, _window = None ):									return Sublime.CloseView( _file, _flags, _Window );

		## Shows a message in the status bar
		def NotifyStatusBar( _text = 'Example Message', _window = None ):				return Sublime.GetWindow( _window ).status_message( _text );

		## Returns True if the menu is visible
		def IsMenuVisible( _window = None ):											return Sublime.GetWindow( _window ).is_menu_visible( );

		## Sets the menu visibility
		def SetMenuVisible( _flag, _window = None ):									return Sublime.GetWindow( _window ).set_menu_visible( _flag );

		## Returns True if the sidebar is visible
		def IsSideBarVisible( _window = None ):											return Sublime.GetWindow( _window ).is_sidebar_visible( );

		## Sets the sidebar visibility - ie if it'll be displayed or not when contents are available
		def SetSideBarVisible( _flag, _window = None ):									return Sublime.GetWindow( _window ).set_sidebar_visible( _flag );

		## Returns True if the status_bar is visible
		def IsStatusBarVisible( _window = None ):										return Sublime.GetWindow( _window ).is_status_bar_visible( );

		## Sets the status_bar visibility
		def SetStatusBarVisible( _flag, _window = None ):								return Sublime.GetWindow( _window ).set_status_bar_visible( _flag );

		## Returns True if the tabs is visible
		def AreTabsVisible( _window = None ):											return Sublime.GetWindow( _window ).get_tabs_visible( );

		## Sets the tabs visibility - ie if they'll be displayed for open files
		def SetTabsVisible( _flag = True ):												return Sublime.GetWindow( _window ).set_tabs_visible( _flag );

		## Returns True if the minimap is visible
		def IsMiniMapVisible( _window = None ):											return Sublime.GetWindow( _window ).is_minimap_visible( );

		## Sets the minimap visibility
		def SetMiniMapVisible( _flag, _window = None ):									return Sublime.GetWindow( _window ).set_minimap_visible( _flag );

		## Returns List: [ ] of all open folders
		def GetAllOpenFolders( _window = None ):										return Sublime.GetWindow( _window ).folders( );

		## Runs the named WindowCommand with the (optional) given args. This method is able to run any sort of command, dispatching the command via input focus.
		def RunCommand( _cmd, _window = None, *_varargs ):								return Sublime.GetWindow( _window ).run_command( _cmd, *_varargs );

		## Runs the named WindowCommand with the (optional) given args. This method is able to run any sort of command, dispatching the command via input focus.
		def RunCmdSetLayout( _layout, _window = None ):									return Sublime.RunCommand( 'set_layout', _window, _layout );

		##
		## def RunCmdSetSyntax( _syntax, _window = None ):									return Sublime.RunCommand( 'set_file_type', _window, 'syntax', _syntax );



		##
		## Useful Helpers....
		##


		##
		## Cell are created as [ col_index_left, row_index_top, col_index_right, row_index_bottom ]
		##
		def CreateLayoutCell( _col_left, _col_right, _row_top, _row_bottom ):
			return [ _col_left, _row_top, _col_right, _row_bottom ];


		##
		## Returns a region from the lines provided
		##
		def GetViewRegionByLines( _view, _line_number = 0, _line_number_2 = 0 ):
			##
			_region = sublime.Region( Sublime.GetLineCharsOffset( _view, _line_number ), Sublime.GetLineCharsOffset( _view, TernaryFunc( _line_number_2 > _line_number, _line_number_2, _line_number ) ) );

			return _view.line( _region );


		##
		## Returns the region of a single entire-line
		##
		def GetViewLineRegion( _view, _line_number ):
			## Grabs the starting point of the line
			_start = Sublime.GetLineCharsOffset( _view, _line_number );

			## Grabs the starting point of the line after it - then subtracts 1 char which should put us on the previous line at the end...
			_end = _start; # Sublime.GetLineCharsOffset( _view, _line_number + 1 ) - 1;

			return sublime.Region( _start, _end );


		##
		## Helper - Get the view we're looking for...
		##
		def FindView( _name = 'ACMS - Panel' ):
			## Grab all of the active views for the current window...
			_views = sublime.active_window( ).views( );

			## The view we're targeting
			_view = None;

			## Test the first view views

			## Code - Map is typically the LAST view, always...
			_view_last = _views[ len( _views ) - 1 ];
			if ( _view == None and IsSublimeView( _view_last ) and _view_last.file_name( ) !=	None and _view_last.file_name( ).strip( ).endswith( _name ) ):
				return _view_last;

			## First view..
			_view_first = _views[ 0 ];
			if ( _view == None and IsSublimeView( _view_first ) and _view_first.file_name( ) != None and _view_first.file_name( ).strip( ).endswith( _name ) ):
				return _view_first;

			## Active View
			_view_active = sublime.active_window( ).active_view( );
			if ( _view == None and IsSublimeView( _view_active ) and _view_active.file_name( ) !=	None and _view_active.file_name( ).strip( ).endswith( _name ) ):
				return _view_active;

			## if the last view is what we're looking for, don't continue..
			if ( _view == None ):
				## Let the user know
				## print( 'Suspected Code - Map Panel is incorrent.. loop needed!' )

				## Loop through up to all views - 1 to find the Code - Map Panel if the above one is incorrect... We don't need to look at the last one because we tried that up above...
				for _i in range( len( _views ) - 1 ):
					## Reference the view
					_view = _views[ _i ];

					## Skip the first, last and active views...
					if ( _view == _view_active or _view == _view_first or _view == _view_last ): continue;

					## If the view is what we we're looking for then we exit the loop so we can return it ( we could return it here too... )
					if ( Sublime.GetViewFileName( _view ).strip( ).endswith( _name ) ):
						## Notify
						## print( '>> Code - Map Panel Identified: ID: "' + str( _view.id( ) ) + '" / Name: "' + _view.name( ) + '" / File-Name: "' + _view.file_name( ) + '"' );

						## Exit the loop
						## break;

						## Return from here..
						return _view;

			##
			return _view;


	##
	## Return C:/Program Files/Sublime Text 3/sublime_text.exe
	##
	def GetExecutablePath( ):
		return sublime.executable_path( );


	##
	## Return sublime_text
	##
	def GetExecutableName( ):
		_install_path, _exe_name				= os.path.split( sublime.executable_path( ) );
		_base_name, _ext 						= os.path.splitext( _exe_name );

		return _base_name;

	##
	## return exe or dmg or ...
	##
	def GetExecutableExt( ):
		_install_path, _exe_name				= os.path.split( sublime.executable_path( ) );
		_base_name, _ext 						= os.path.splitext( _exe_name );

		return _ext[ 1 : ];


	##
	## Return C:/Program Files/Sublime Text 3/
	##
	def GetInstallPath( ):
		##
		_path_executable						= sublime.executable_path( );

		## 	str	Returns the path where Sublime Text Executable is with the executable attached...
		_install_path, _exe_name				= os.path.split( sublime.executable_path( ) ); #.replace( '\\', '/' );

		return _install_path;


	##
	## Return C:/Program Files/Sublime Text 3/changelog.txt
	##
	def GetChangelogPath( ):
		return os.path.join( Sublime.GetInstallPath( ), 'changelog.txt' );




	## ##
	## ## Adds additional Replacement Variables to the names, etc... such as syntax file selected, the file extension of the current file, selected language, and more...
	## ##
	## ## Build System Variables
	## ## Build systems expand the following variables in .sublime-build files:
	## ##
	## ## $file_path	The directory of the current file, e.g., C:\Files.
	## ## $file	The full path to the current file, e.g., C:\Files\Chapter1.txt.
	## ## $file_name	The name portion of the current file, e.g., Chapter1.txt.
	## ## $file_extension	The extension portion of the current file, e.g., txt.
	## ## $file_base_name	The name-only portion of the current file, e.g., Document.
	## ## $folder	The path to the first folder opened in the current project.
	## ## $project	The full path to the current project file.
	## ## $project_path	The directory of the current project file.
	## ## $project_name	The name portion of the current project file.
	## ## $project_extension	The extension portion of the current project file.
	## ## $project_base_name	The name-only portion of the current project file.
	## ## $packages	The full path to the Packages folder.
	## ## def GetExpandableVariables( _text = '', _package = None, _repo = None, _debugging = False, _caller_view = None, _caller_window = None ):
	## ##
	## ##
	## ##
	## ## WARNING: This is outdated and doesn't contain all of the features that edit_settings_plus uses - please wait until everything is properly ported over before using this as the args list is going to change...
	## ##
	## ##
	## def GetExpandableVariables( _text = '', _view_name = 'ACMS - Panel', _window = None, _view = None, _package = None, _repo = None ):

	## 	## The current Window and Active View...
	## 	_window	= Sublime.GetWindow( _window ); # sublime.active_window( );
	## 	_view	= Sublime.GetView( _view ); # _window.active_view( );

	## 	## If package is unset, use the package name above until it is automated to use the package name of the package script which is calling this function... I'll add an arg for it too which is why it exists up there..
	## 	if ( _package == None ):
	## 		_package = Sublime.GetPackageName( );

	## 	##
	## 	if ( _package == None ):
	## 		_package = Sublime.__package_name__;

	## 		##
	## 		_text = 'ERROR: AcecoolSublime.GetPackageName did not provide accurate results! Result: "' + Sublime.GetPackageName( ) + '"\n';
	## 		print( _text * 100 );

	## 	## If package repo is unset, use the package repo above until it is automated to use the package repo of the package script which is calling this function... I'll add an arg for it too which is why it exists up there..
	## 	if ( _repo == None ):
	## 		_repo = Sublime.__package_repo__;

	## 	## Grab the extension of the active file..
	## 	_ext = 'xxx'
	## 	if ( IsValidSublimeView( _view ) ):
	## 		_ext = _view.file_name( ).split( '.' )[ - 1 ];

	## 	## Selected Syntax...
	## 	_syntax_path = _view.settings( ).get( 'syntax' );
	## 	_syntax, _syntax_ext = os.path.splitext( os.path.basename( _syntax_path ) );

	## 	## Scope Pre-Definitions :: If the ACMS Panel is open - grab the syntax used for it...
	## 	_map_view							= Sublime.FindView( _view_name );
	## 	_map_syntax_path					= '';
	## 	_map_syntax							= '';
	## 	_map_syntax_ext						= 'sublime-syntax';

	## 	## Make sure the view works...
	## 	if ( IsValidSublimeView( _map_view ) ):
	## 		_map_syntax_path				= _map_view.settings( ).get( 'syntax' );
	## 		_map_syntax, _map_syntax_ext	= os.path.splitext( os.path.basename( _syntax_path ) );

	## 	## Selected Language
	## 	_language = 'SelectedLanguage';

	## 	## ${id} to data replacements Dictionary...
	## 	_vars = {
	## 		##
	## 		'platform':					Sublime.GetPlatformName( ),

	## 		## Packages/
	## 		'packages':					sublime.packages_path( ),

	## 		## Packages/User/
	## 		'user':						os.path.join( sublime.packages_path( ), 'User' ),

	## 		## Packages/Default/
	## 		'default':					os.path.join( sublime.packages_path( ), 'Default' ),

	## 		## Active Syntax Highlighter File such as JSON, JavaScript, etc...
	## 		'syntax':					_syntax,
	## 		'syntax_file':				_syntax + '.' + _syntax_ext,
	## 		'syntax_path':				_syntax_path,

	## 		## ACMS Mapper yntax
	## 		'nap_syntax':				_map_syntax,
	## 		'nap_syntax_file':			_map_syntax + '.' + _map_syntax_ext,
	## 		'nap_syntax_path':			_map_syntax_path,

	## 		## The File Extension of the active file...
	## 		'ext':						_ext,
	## 		'extension':				_ext,

	## 		## Active Language such as JSON, JavaScript, GMod Lua, Lua, etc..
	## 		## 'lang':						_language,
	## 		## 'language':					_language,

	## 		## Full path to current page
	## 		## Task: Find way to programmatically get the package name so it doesn't need to be hardcoded...
	## 		'package':					os.path.join( sublime.packages_path( ), _package ),
	## 		'user_package':				os.path.join( sublime.packages_path( ), 'User', _package ),

	## 		## Package Repo
	## 		'repo': _repo,

	## 		##
	## 		## '': '',
	## 		## '': '',
	## 	};

	## 	## Debugging...
	## 	print( ' >> Expanding Vars of _text:\t\t' + _text );
	## 	print( ' >> Expanded Vars of _text:\t\t' + sublime.expand_variables( _text, _vars ) );


	## 	## value	Expands any variables in the string value using the variables defined in the dictionary variables.
	## 	## value may also be a list or dict, in which case the structure will be recursively expanded. Strings should use snippet syntax,
	## 	## for example: expand_variables("Hello, ${name}", {"name": "Foo"})
	## 	return sublime.expand_variables( _text, _vars );


	##
	## Adds additional Replacement Variables to the names, etc... such as syntax file selected, the file extension of the current file, selected language, and more...
	##
	##
	## Build System Variables
	## Build systems expand the following variables in .sublime-build files:
	##
	## $file_path	The directory of the current file, e.g., C:\Files.
	## $file	The full path to the current file, e.g., C:\Files\Chapter1.txt.
	## $file_name	The name portion of the current file, e.g., Chapter1.txt.
	## $file_extension	The extension portion of the current file, e.g., txt.
	## $file_base_name	The name-only portion of the current file, e.g., Document.
	## $folder	The path to the first folder opened in the current project.
	## $project	The full path to the current project file.
	## $project_path	The directory of the current project file.
	## $project_name	The name portion of the current project file.
	## $project_extension	The extension portion of the current project file.
	## $project_base_name	The name-only portion of the current project file.
	## $packages	The full path to the Packages folder.
	##
	##
	##
	## 	//
	## 	// Acecool Library / Extract Vars
	## 	// sublime.active_window( ).extract_variables( )
	## 	//
	## 	${version_sublime}		3143 || ####
	## 	${platform}				Windows || Linux || OSX
	## 	${architecture}			x64 || x32
	## 	${arch}					x64 || x32
	##
	## 	// Important Paths
	## 	${cache}				C:\Users\%UserName%\AppData\Local\Sublime Text 3\Cache																			|| PLATFORM_SPECIFIC_PATH || res://
	## 	${packages_installed}	C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Installed Packages															|| PLATFORM_SPECIFIC_PATH || res://
	## 	${packages}				C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Packages																		|| PLATFORM_SPECIFIC_PATH || res://
	## 	${user}					C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Packages\User																|| PLATFORM_SPECIFIC_PATH || res://
	## 	${default}				C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Packages\Default																|| PLATFORM_SPECIFIC_PATH || res://
	## 	${package}				C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Packages\AcecoolCodeMappingSystem											|| PLATFORM_SPECIFIC_PATH || res://
	## 	${user_package}			C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Packages\User\AcecoolCodeMappingSystem										|| PLATFORM_SPECIFIC_PATH || res://
	##
	## 	// Active File when edit_settings_plus was called Data:
	## 	${file_path}			C:\AcecoolGit\acecooldev_sublimetext3\AppData\Sublime Text 3\Packages\AcecoolCodeMappingSystem\commands							|| PLATFORM_SPECIFIC_PATH_TO_FILE || res://	+ PATH
	## 	${file}					C:\AcecoolGit\acecooldev_sublimetext3\AppData\Sublime Text 3\Packages\AcecoolCodeMappingSystem\commands\edit_settings_plus.py	|| PLATFORM_SPECIFIC_PATH_TO_FILE || res://	+ PATH
	## 	${file_name}			edit_settings_plus.py
	## 	${file_base_name}		edit_settings_plus
	## 	${file_extension}		py
	## 	${extension}			py
	## 	${ext}					py
	##
	## 	// Active File Syntax Information
	## 	${syntax}				Python
	## 	${language}				Python
	## 	${lang}					Python
	## 	${user_syntax}			C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User\Python.sublime-settings
	## 	${syntax_ext}			sublime-syntax
	## 	${syntax_file}			Python.sublime-syntax
	## 	${syntax_path_base}		Python/Python.sublime-syntax
	## 	${syntax_path}			res://Packages/Python/Python.sublime-syntax || %AppData%/Sublime Text 3/Packages/Python/Python.sublime-settings
	##
	## 	// Menu > Project / Sublime Text Window Session Data
	## 	${folder}				C:\AcecoolGit\acecooldev_sublimetext3\AppData\Sublime Text 3\Packages\AcecoolCodeMappingSystem
	## 	${project}				C:\AcecoolGit\AcecoolCodeMappingSystem.sublime-project
	## 	${project_path}			C:\AcecoolGit
	## 	${project_name}			AcecoolCodeMappingSystem.sublime-project
	## 	${project_base_name}	AcecoolCodeMappingSystem
	## 	${project_extension}	sublime-project
	##
	## 	// Misc
	## 	${repo}					https://bitbucket.org/Acecool/acecooldev_sublimetext3
	## }
	##
	## Sublime Text - Active Window Variables using: sublime.active_window( ).extract_variables( )
	## {
	## 	'project_name':			'AcecoolCodeMappingSystem.sublime-project',
	## 	'project_base_name':	'AcecoolCodeMappingSystem',
	## 	'project_path':			'C:\\AcecoolGit',
	## 	'file_extension':		'py',
	## 	'file_name':			'edit_settings_plus.py',
	## 	'project_extension':	'sublime-project',
	## 	'platform':				'Windows',
	## 	'project':				'C:\\AcecoolGit\\AcecoolCodeMappingSystem.sublime-project',
	## 	'file':					'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem\\commands\\edit_settings_plus.py',
	## 	'packages':				'C:\\Users\\Acecool\\AppData\\Roaming\\Sublime Text 3\\Packages',
	## 	'folder':				'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem',
	## 	'file_base_name':		'edit_settings_plus',
	## 	'file_path':			'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem\\commands'
	## }
	##
	##
	##
	##
	##
	##
	##
	##
	##
	##
	##
	##
	##
	##
	##
	## _view != None and isinstance( _view, sublime.View ) and _view.file_name( ) != None
	##
	##
	##
	##
	##
	##
	##
	##
	def GetExpandableVariables( self, _text = '', _caller_view = None, _caller_window = None, _package = None, _repo = None ):
		## Grab the window / view which was used when calling the command - or the active window / view...
		_window								= ( _caller_window, sublime.active_window( ) )[ _caller_window == None ];
		_view								= ( _caller_view, _window.active_view( ) )[ _caller_view == None ];

		## Scope declaration - if a valid view is defined, we'll need this.
		_ext								= '';

		## If we've supplied a valid view then we can grab some additional helpful information to process through the extracted / expanded variable system
		if ( Sublime.IsValidView( _view ) ):
			_, _, _, _raw_base_from_ext, _ext = Folder.GetComponents( _view.file_name( ) );

		## Selected Syntax...
		_syntax_path, _, _, _syntax, _syntax_ext = Folder.GetComponents( _view.settings( ).get( 'syntax' ), True );

		## str	Returns the path where Sublime Text Executable is with the executable attached...
		_path_executable, _path_install, _exe_name, _exe_name_base, _exe_name_ext = Folder.GetComponents( sublime.executable_path( ) );

		## Magic File Path...
		_magic_file_path, _magic_file_path_base, _magic_filename, _magic_file_base_name, _magic_file_ext = Folder.GetComponents( __file__ );

		## Extracts the following information from the window: platform, packages, project, project_path, project_name, project_base_name, project_extension, file, folder, file_path, file_name, file_base_name, file_extension
		_vars_window						= _window.extract_variables( );

		## ${id} to data replacements Dictionary...
		_vars								= { };

		##
		## sublime.active_window( ).extract_variables( )
		##
		## 	'platform':				'Windows',
		## 	'packages':				'C:\\Users\\Acecool\\AppData\\Roaming\\Sublime Text 3\\Packages',
		## 	'project_path':			'C:\\AcecoolGit',
		## 	'project':				'C:\\AcecoolGit\\AcecoolCodeMappingSystem.sublime-project',
		## 	'project_name':			'AcecoolCodeMappingSystem.sublime-project',
		## 	'project_base_name':	'AcecoolCodeMappingSystem',
		## 	'project_extension':	'sublime-project',
		## 	'folder':				'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem',
		## 	'file':					'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem\\commands\\edit_settings_plus.py',
		## 	'file_path':			'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem\\commands'
		## 	'file_name':			'edit_settings_plus.py',
		## 	'file_base_name':		'edit_settings_plus',
		## 	'file_extension':		'py',
		##
		_vars[ 'platform' ]					= _vars_window.get( 'platform',				None );
		_vars[ 'packages' ]					= _vars_window.get( 'packages',				None );
		_vars[ 'project_path' ]				= _vars_window.get( 'project_path',			None );
		_vars[ 'project' ]					= _vars_window.get( 'project',				None );
		_vars[ 'project_name' ]				= _vars_window.get( 'project_name',			None );
		_vars[ 'project_base_name' ]		= _vars_window.get( 'project_base_name',	None );
		_vars[ 'project_extension' ]		= _vars_window.get( 'project_extension',	None );
		_vars[ 'folder' ]					= _vars_window.get( 'folder',				None );
		_vars[ 'file' ]						= _vars_window.get( 'file',					None );
		_vars[ 'file_path' ]				= _vars_window.get( 'file_path',			None );
		_vars[ 'file_name' ]				= _vars_window.get( 'file_name',			None );
		_vars[ 'file_base_name' ]			= _vars_window.get( 'file_base_name',		None );
		_vars[ 'file_extension' ]			= _vars_window.get( 'file_extension',		None );

		## %AppData%\Sublime Text 3\Packages\<PackageName\
		_vars[ 'packages_path' ]			= sublime.packages_path( );

		## Returns the Sublime Text version, such as '3176'
		_vars[ 'version_sublime' ]			= sublime.version( );

		## Returns the path where Sublime Text stores cache files.
		_vars[ 'cache' ]					= sublime.cache_path( );

		## Returns the path where all the user's .sublime-package files are located.
		_vars[ 'packages_installed' ]		= sublime.installed_packages_path( );

		## '%AppData%/Sublime Text 3/Packages/User'
		_vars[ 'user' ]						= os.path.join( _vars[ 'packages_path' ], 'User' );

		## %AppData%\Sublime Text 3\Packages\Default\
		_vars[ 'default' ]					= os.path.join( _vars[ 'packages_path' ], 'Default' );

		## Returns the filename
		_vars[ 'magic_file_path' ]			= _magic_file_path;
		_vars[ 'magic_file_path_base' ]		= _magic_file_path_base;
		_vars[ 'magic_filename' ]			= _magic_filename;
		_vars[ 'magic_file_base_name' ]		= _magic_file_base_name;
		_vars[ 'magic_file_ext' ]			= _magic_file_ext;

		## If package is unset, use the package name above until it is automated to use the package name of the package script which is calling this function... I'll add an arg for it too which is why it exists up there..
		_vars[ 'magic_package' ]			= __package__;

		## If package is unset, use the package name above until it is automated to use the package name of the package script which is calling this function... I'll add an arg for it too which is why it exists up there..
		_vars[ 'package_name' ]				= ( _package, _vars[ 'magic_package' ] )[ ( _package == None ) ];

		## %AppData%\Sublime Text 3\Packages\<PackageName\
		_vars[ 'package' ]					= os.path.join( _vars[ 'packages_path' ], _vars[ 'package_name' ] );

		## %AppData%\Sublime Text 3\Packages\User\<PackageName\
		_vars[ 'user_package' ]				= os.path.join( _vars[ 'user' ], _vars[ 'package_name' ] );

		## Package Repo - If package repo is unset, use the package repo above until it is automated to use the package repo of the package script which is calling this function... I'll add an arg for it too which is why it exists up there..
		_vars[ 'repo' ]						= ( _repo, 'https://www.bitbucket.com/UserName/PackageNameUndefined' )[ ( _repo == None ) ];

		## Active Syntax Highlighter File such as JSON, JavaScript, etc...
		_vars[ 'syntax' ]					= _syntax;

		## Active Language such as JSON, JavaScript, GMod Lua, Lua, etc..
		_vars[ 'language' ]					= _vars[ 'syntax' ];
		_vars[ 'lang' ]						= _vars[ 'syntax' ];

		## ${user}/${syntax}.sublime-settings' == Works just fine..., #'${user_syntax}' == ERROR ie not found... - Issue or only dif would maybe be \ vs /? mixxed may cause issues and have it report as not existing which replaces the data?
		## %AppData%\Sublime Text 3\Packages\User\JSON.sublime-settings / JavaScript.sublime-settings, Lua.sublime-settings, etc..
		_vars[ 'user_syntax' ]				= os.path.join( _vars[ 'user' ], _vars[ 'syntax' ] + '.sublime-settings' ); #.replace( '/', '\\' );

		## Raw file name - JSON.sublime-syntax or .tmLanguage - filename only..
		_vars[ 'syntax_ext' ]				= _syntax_ext;

		## Raw file name - JSON.sublime-syntax or .tmLanguage - filename only..
		_vars[ 'syntax_file' ]				= _vars[ 'syntax' ] + '.' + _vars[ 'syntax_ext' ];

		## Core file path - So starting from Packages/ .. ie: JavaScript/JSON.sublime-syntax, Lua/Lua.tmLanguage or sublime-syntax, etc...
		_vars[ 'syntax_path_base' ]			= _syntax_path;

		## The full path to the syntax file...
		_vars[ 'syntax_path' ]				= os.path.join( _vars[ 'packages_path' ], _vars[ 'syntax_path_base' ] );

		## ACMS Mapper yntax
		## _vars[ 'map_syntax' ]				= _map_syntax;
		## _vars[ 'map_syntax_file' ]			= _vars[ 'map_syntax' ] + '.' + _map_syntax_ext;
		## _vars[ 'map_syntax_path' ]			= _map_syntax_path;

		## The File Extension of the active file...
		_vars[ 'extension' ]				= _ext;
		_vars[ 'ext' ]						= _vars[ 'extension' ];

		## The File Extension of the active file...
		_vars[ 'architecture' ]				= sublime.arch( );
		_vars[ 'arch' ]						= _vars[ 'architecture' ];

		## // Full path to the Sublime Text Installation Directory ( Directory where the executable is ) -- C:/Program Files/Sublime Text 3/
		_vars[ 'sublime_install_path' ]		= _path_install;

		## // Full path to the Sublime Changelog file -- C:/Program Files/Sublime Text 3/changelog.txt
		_vars[ 'sublime_changelog_path' ]	= os.path.join( _vars[ 'sublime_install_path' ], 'changelog.txt' );

		## // Full path to the Sublime Executable Path -- C:/Program Files/Sublime Text 3/sublime_text.exe
		_vars[ 'sublime_exe_path' ]			= _path_executable;

		## // The base filename for the Sublime Text Executable -- sublime_text
		_vars[ 'sublime_exe_filename' ]		= _exe_name;

		## // The base filename for the Sublime Text Executable -- sublime_text
		_vars[ 'sublime_exe_name' ]			= _exe_name_base;

		## // Full path to the Sublime Executable Extension (.exe, .dmg, etc.. ) -- exe
		_vars[ 'sublime_exe_extension' ]	= _exe_name_ext;

		## // Full path to the Sublime Text Installation Directory ( Directory where the executable is ), then the Packages Sub-Directory.
		_vars[ 'sublime_packages_path' ]	= os.path.join( _vars[ 'sublime_install_path' ], 'Packages' );

		## Debugging Values
		_vars[ 'VAR_PREFIX' ]				= '\$\{';
		_vars[ 'VAR_SUFFIX' ]				= '\}';
		_vars[ 'VAR' ]						= _vars[ 'VAR_PREFIX' ];
		_vars[ 'VARB' ]						= _vars[ 'VAR_SUFFIX' ];
		_vars[ '<?' ]						= _vars[ 'VAR_PREFIX' ];
		_vars[ ' ?>' ]						= _vars[ 'VAR_SUFFIX' ];


		## Merge the tables... - Note: In this configuration, _vars_window will overwrite any duplicates in _vars... this is future compatible.. but if SublimeText rewrites how something works, it won't be backwards compatible unless the rest of Sublime Text updates itself along with it, which they should..
		## _vars.update( _vars_window );

		## res://Packages\
		_path_res_packages					= 'res://Packages';

		## Helpers
		_res_package						= os.path.join( _path_res_packages, _vars[ 'package_name' ] );
		_res_default						= os.path.join( _path_res_packages, 'Default' );
		_res_user							= os.path.join( _path_res_packages, 'User' );
		_res_user_package					= os.path.join( _res_user, _vars[ 'package_name' ] );
		_res_user_syntax					= os.path.join( _res_user, _syntax + '.sublime-settings' );
		_res_syntax_path					= os.path.join( _path_res_packages, _syntax_path );

		## Copy of the _vars table replacing a few key vars with res:// for resting...
		_vars_res = _vars.copy( );

		## Replace a few vars with res://Packages links in order to test location data...
		_vars_res[ 'packages' ]				= _path_res_packages;
		_vars_res[ 'package' ]				= _res_package;
		_vars_res[ 'default' ]				= _res_default;
		_vars_res[ 'user' ]					= _res_user;
		_vars_res[ 'user_package' ]			= _res_user_package;
		_vars_res[ 'user_syntax' ]			= _res_user_syntax;
		_vars_res[ 'syntax_path' ]			= _res_user_syntax;

		## value	Expands any variables in the string value using the variables defined in the dictionary variables.
		_text_reg	= sublime_api.expand_variables( _text, _vars );

		## And replace a few key pieces using res:// Packages links....
		_text_res	= sublime_api.expand_variables( _text, _vars_res );

		## Return the output...
		return _text_reg, _text_res;


##
## Sublime Resource Class - This has everything to do with packages and files within them... Including extraction, reading, determing if they exist... etc..
##
class SublimeResource:
	##
	## Helper: Returns whether or not the package exists, and if it does, the complete path including the filename is returned... otherwise None...
	##
	def PackageExists( _package = 'AcecoolCodeMappingSystem', _path_alt = None ):
		## Grab the full package path...
		_package_path = SublimeResource.GetPackagePath( _package, _path_alt );

		## Determine if the file at the path found, if not None, is a zipfile... Otherwise, we'll generate the same path via GetPackage and have the archive returned to test that way...
		## Task: Check the api and determine if it needs the archive or a file path...
		if ( _package_path != None ):
			## Grab the archive - this is for usability...
			_archive = SublimeResource.GetPackageObject( _package );

			## If it needs the path, then try that... Return the path if viable... The Archive if needed...
			if ( zipfile.is_zipfile( _package_path ) ):
				return _package_path, _archive;

			## If it needs an archive, then lets try that...
			if ( zipfile.is_zipfile( _archive ) ):
				return _package_path, _archive;

		## Not found... or some other issue..
		return None, None;


	##
	## Helper: Returns whether a file exists inside of a sublime resource / package
	## Note: Here I'm calling a Resource a file within a Package / Archive.. , so SublimeResource == File... a Package / Archive is a Package...
	##
	def Exists( _file, _package = 'AcecoolCodeMappingSystem' ):
		## Determine whether the package exists - the path will be returned....
		_package_path, _archive = SublimeResource.PackageExists( _package );

		## If it was found, return True along with the data...
		if ( _package_path != None and _archive != None ):
			return True, _package_path, _archive;

		## If it wasn't found, return False along with any potential data...
		return False, _package_path, _archive;


	##
	## Helper:
	##
	def Extract( _file, _extract_path, _package = 'AcecoolCodeMappingSystem' ):
		pass


	##
	## Returns whether a file is a resource file, and is extracted - it will not return True if the file is a non-resource file but the file exists... so you'll need an or file exists on the same path... This stops if the file doesn't exist in resource form...
	##
	def ExistsExtracted( _file, _package = 'AcecoolCodeMappingSystem' ):
		return SublimeResource.Exists( _package, _file ) and File.Exists( os.path.join( sublime.packages_path( ), _file ) );



	##
	## Helper: Returns file contents from within an archive / package object...
	##
	def Read( _file, _encoding = None, _package = 'AcecoolCodeMappingSystem' ):
		## Grab the archive...
		_archive = SublimeResource.GetPackageObject( _package, _file );

		## If the archive is defined, then read a file from it...
		if ( _archive != None ):
			try:
				_content = _archive.read( _file );

				if ( _encoding != None ):
					_content = _content.decode( _encoding );

				return _content;
			except Exception as _error:
				print( 'AcecoolLib.SublimeResource.Read -> Error reading - Note: Use the file path from the Package Directory, ie: Use "maps/Python_User.py" instead of "PackageName/maps/Python_User.py"... ' + str( _error ) );

		return '';



	##
	## Helper: Returns file contents from within an archive / package object...
	##
	def ReadText( _file, _encoding = 'utf8', _package = 'AcecoolCodeMappingSystem' ):
		return SublimeResource.Read( _file, _encoding, _package );



	##
	## Helper: Returns whether a file is extracted, or not..
	##
	def GetPackagePath( _package = 'AcecoolCodeMappingSystem', _path_alt = None ):
		## Make sure we don't need the extension, add it if needed or don't if already there - simpler to do this than have multiple ifs or ternaries for the clause..
		if ( not _package.endswith( '.sublime-package' ) ):
			_package = _package + '.sublime-package';

		## If the alt path is set, we use it first...
		if ( _path_alt != None ):
			_path_alt				= os.path.join( _path_alt, _package );

			## If the package doesn't exist, try in another location... And if this doesn't exist, use the _package string by itself.. If it doesn't exist then nope...
			if ( File.Exists( _path_alt ) ):
				return _path_alt;

		## Get the Installed Packages Path - this is where default packages are placed... - The installed packages path should either be: %AppData%/Sublime Text 3/Installed Packages/ or C:/Program Files//Sublime Text 3/Installed Packages/ or something similar
		_installed_packages_path	= sublime.installed_packages_path( );

		## Grab the standard packages path too, and a few helpers.. This is where extracted packages are executed from ( User/ is in this folder too ) - packages installed by Package Control are moved here: %AppData%/Sublime Text 3/Packages/
		_packages_extracted			= sublime.packages_path( );

		## Join the package name / file with the installed packages folder for the package path
		_package_path				= os.path.join( _installed_packages_path, _package );

		## If the package exists, return it
		if ( File.Exists( _package_path ) ):
			return _package_path;

		## Try Extracted Packages Location: %AppData%/Sublime Text 3/Packages/
		_package_path				= os.path.join( _packages_extracted, _package );

		## If the package exists, return it
		if ( File.Exists( _package_path ) ):
			return _package_path;


		## Sublime Install Path Setup...

		## 	str	Returns the path where Sublime Text Executable is with the executable attached...
		_install_path, _exe_name				= os.path.split( sublime.executable_path( ) ); #.replace( '\\', '/' );

		## install_dir/Packages/
		_path_sublime_packages		= os.path.join( _install_path, 'Packages' );

		## Try the install dir.../Packages/PackageName.sublime-package
		_install_path_package_path				= os.path.join( _path_sublime_packages, _package );

		## Try the package inside of the install directory..
		if ( File.Exists( _install_path_package_path ) ):
			return _install_path_package_path;


		## Try Package by itself, in case someone set the entire path to it...
		if ( File.Exists( _package ) ):
			return _package;

		## We can't find it...
		return None;




	##
	## Extracts internal _file from _package to _path
	##
	def GetPackageObject( _package = 'AcecoolCodeMappingSystem', _path_alt = None ):
		## Get the Package Path, complete...
		_package_path				= SublimeResource.GetPackagePath( _package, _path_alt );

		## Scope Initialiation - Make sure the reference exists so when we assign it in the try, if possible, then we can return it below..
		_archive					= None;

		## Reference the package as an archive we can manipulate...Convert the sublime-package path to an archive object so we can extract files...
		try:
			_archive				= zipfile.ZipFile( _package_path ); # Sublime.GetPackageObject( _package_path, _package );
		except Exception as _error:
			print( 'AcecoolLib.SublimeResource.GetPackageObject -> Error: ' + str( _error ) );

		## Return whatever it is... None if it is unassigned, or the package...
		return _archive;


	##
	## Extracts internal folder from _package to _path
	##
	def ExtractFiles( _extract_to, _extract = '', _overwrite_extracted_files = False, _package = 'AcecoolCodeMappingSystem' ):
		## Reference the package as an archive we can manipulate...Convert the sublime-package path to an archive object so we can extract files...
		_archive					= SublimeResource.GetPackageObject( _package, _extract_to );

		## If the archive isn't set, then return None...
		if ( not _archive ):
			return None;

		## Scope Init: Pre-Define the Extraction List...
		_extract_list				= None;

		## Make sure we don't process the list or tuple - these should be exactly what we want, nothing more or less.. If it is a Dict, we compare those names - and if string, we use that as startswith..
		if ( IsList( _extract ) or IsTuple( _extract ) ):
			##
			_extract_list			= _extract;

			##
			for _file in _extract:
				## Put together file file path
				_file_path			= os.path.join( _extract_to, _file );

				## If the file exists, we can't extract it so an exception will be thrown... So, if overwrite extracted files is set to True, we delete it first... to ensure the latest is extracted - this can cause issues - so I may change this to move files elsewhere but I'll add a warning for the time being... so users need to move files to the User directory.. and I'll only extract maps...
				if ( File.Exists( _file_path ) and _overwrite_extracted_files ):
					File.Remove( _file_path );

		elif ( _extract != None ):
			_extract_list = ( [ ], _extract )[ IsList( _extract ) ];

			for _file in _archive.namelist( ):
				##
				_rule = None;

				if ( IsDict( _extract ) ):
					for _rule in _extract:
						if ( not _file.startswith( _rule ) ):
							_rule	= None;

				if ( ( IsDict( _extract ) and _extract.get( _file, False ) or _rule != None ) or ( not IsDict( _extract ) and _file.startswith( _extract ) ) ):
					_extract_list.append( _file );

					## Put together file file path
					_file_path		= os.path.join( _extract_to, _file );

					## If the file exists, we can't extract it so an exception will be thrown... So, if overwrite extracted files is set to True, we delete it first... to ensure the latest is extracted - this can cause issues - so I may change this to move files elsewhere but I'll add a warning for the time being... so users need to move files to the User directory.. and I'll only extract maps...
					if ( File.Exists( _file_path ) and _overwrite_extracted_files and _archive.getinfo( _file ).file_size != File.Size( _file_path ) ):
						File.Remove( _file_path );

		##
		try:
			_archive.extractall( _extract_to, _extract_list );
		except Exception as _error:
			print( 'Error Extracting Files: ' + _error );

		##
		if ( _archive != None ):
			_archive.close( );





##
## Acecool Library Examples - This outputs all function information, and proper usage on each function in an easy-to-read format...
## If the function isn't shown, how it is called, simply go to the ClassName.__example__ function, and find the Header( 'ClassName.FuncName' ) line, and the lines under it call the function you're trying to use..
##
if ( Acecool.GetSetting( 'output_acecool_lib_examples', False ) ):
	print( Acecool.__example__( ) );





		## print( 'AcecoolLib -> ExtractFiles -> File-Size -> ' + str( File.Size( _file_path )	) + ' -> Archive File Size -> RemoveFile -> ' + str( _archive.getinfo( _file ).file_size ) + ' -> ' + _file_path );
		## else:
		## print( 'AcecoolLib -> ExtractFiles -> File-Size -> ' + str( File.Size( _file_path )	) + ' -> Archive File Size -> Keeping File -> ' + str( _archive.getinfo( _file ).file_size ) + ' -> ' + _file_path );
		## print( 'Archive File Info: ' + str( _archive.getinfo( _file ) ) );

		## ##
		## if ( _package_path != None ):
		## 	## Reference the package as an archive we can manipulate...Convert the sublime-package path to an archive object so we can extract files...
		## 	try:
		## 		_archive = zipfile.ZipFile( _package_path );
		## 	except Exception as _error:
		## 		print( 'AcecoolLib.SublimeResource.GetPackageObject -> Error: ' + str( _error ) );


		##
		## print( 'Extract List: ' + str( _extract_list ) );


	## ##
	## ## Extracts internal _file from _package to _path
	## ##
	## def GetPackageObject( _package = 'AcecoolCodeMappingSystem', _path_alt = None ):
		## ## ## Grab the users installed packages folder
		## ## _packages_installed		= sublime.installed_packages_path( );

		## ## ## ## Grab the standard packages path too, and a few helpers.. This is where extracted packages are executed from ( User/ is in this folder too )
		## ## ## _packages_extracted		= sublime.packages_path( );

		## ## ## Join the package name / file with the installed packages folder for the package path
		## ## _package_path			= os.path.join( _packages_installed, _package );

		## ## ## If the file / package exists, then convert it and return it..
		## ## if ( File.Exists( _packages_path ) ):
		## ## 	## Reference the package as an archive we can manipulate...Convert the sublime-package path to an archive object so we can extract files...
		## ## 	_archive				= zipfile.ZipFile( _package );

		## ## 	## Return it...
		## ## 	return _archive;

		## ## ## The archive doesn't exist... We could throw an exception - maybe later..
		## ## return None

		## ## Make sure we don't need the extension, add it if needed or don't if already there - simpler to do this than have multiple ifs or ternaries for the clause..
		## if ( not _package.endswith( '.sublime-package' ) ):
		## 	_package = _package + '.sublime-package';

		## ##
		## _installed_packages_path	= sublime.installed_packages_path( );

		## ## Grab the standard packages path too, and a few helpers.. This is where extracted packages are executed from ( User/ is in this folder too )
		## _packages_extracted			= sublime.packages_path( );

		## ## Join the package name / file with the installed packages folder for the package path
		## _package_path				= os.path.join( _installed_packages_path, _package );

		## ## If the package doesn't exist, try in another location... And if this doesn't exist, use the _package string by itself.. If it doesn't exist then nope...
		## if ( not File.Exists( _package_path ) ):
		## 	_package_path			= os.path.join( _packages_extracted, _package );

		## 	## If the package doesn't exist, try in another location... And if this doesn't exist, use the _package string by itself.. If it doesn't exist then nope...
		## 	if ( not File.Exists( _package_path ) ):
		## 		_package_path		= _package;

		## 		## If the package doesn't exist, try in another location... And if this doesn't exist, use the _package string by itself.. If it doesn't exist then nope...
		## 		if ( not File.Exists( _package_path ) ):
		## 			_package_path = _path_alt + _package;

		## 			## If the package doesn't exist, try in another location... And if this doesn't exist, use the _package string by itself.. If it doesn't exist then nope...
		## 			if ( not File.Exists( _package_path ) ):
		## 				return None;






## print( '\n-------\n' );
## print( 'Testing for word replacers without dynamic to make thing slightly easier...' );
## print( );
## print( );


## ENUM_LIST_ACESSORFUNC_IDS
## MAP_ACCESSORFUNC_ID_FUNC_NAMES
## MAPR_ACCESSORFUNC_ID_FUNC_NAMES
## MAP_ACCESSORFUNC_WORDS
## MAPR_ACCESSORFUNC_WORDS
## MAP_ACCESSORFUNC_WORD_KEYS
## MAPR_ACCESSORFUNC_WORD_KEYS




## ## Expected:		get				get_raw				isset			get_str				get_len				get_len_str
## print( MAP_ACCESSORFUNC_ENUMS_TO_IDS[ ACCESSORFUNC_ID_GET ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_IDS[ ACCESSORFUNC_ID_GET_RAW ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_IDS[ ACCESSORFUNC_ID_ISSET ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_IDS[ ACCESSORFUNC_ID_GET_STR ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_IDS[ ACCESSORFUNC_ID_GET_LEN ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_IDS[ ACCESSORFUNC_ID_GET_LEN_STR ] );
## print( );
## print( MAP_ACCESSORFUNC_ENUMS_TO_IDS[ ACCESSORFUNC_ID_GET ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_IDS[ ACCESSORFUNC_ID_GET_RAW ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_IDS[ ACCESSORFUNC_ID_ISSET ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_IDS[ ACCESSORFUNC_ID_GET_STR ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_IDS[ ACCESSORFUNC_ID_GET_LEN ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_IDS[ ACCESSORFUNC_ID_GET_LEN_STR ] );
## print( );
## print( );

## ## Expected:		on_get			on_get_raw			on_isset		on_get_str			on_get_len			on_get_len_str
## print( MAP_ACCESSORFUNC_ENUMS_TO_ON_IDS[ ACCESSORFUNC_ID_GET ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_ON_IDS[ ACCESSORFUNC_ID_GET_RAW ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_ON_IDS[ ACCESSORFUNC_ID_ISSET ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_ON_IDS[ ACCESSORFUNC_ID_GET_STR ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_ON_IDS[ ACCESSORFUNC_ID_GET_LEN ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_ON_IDS[ ACCESSORFUNC_ID_GET_LEN_STR ] );
## print( );

## print( );
## print( );

## ## Expected:		pre_get			pre_get_raw			pre_isset		pre_get_str			pre_get_len			pre_get_len_str
## print( MAP_ACCESSORFUNC_ENUMS_TO_PRE_IDS[ ACCESSORFUNC_ID_GET ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_PRE_IDS[ ACCESSORFUNC_ID_GET_RAW ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_PRE_IDS[ ACCESSORFUNC_ID_ISSET ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_PRE_IDS[ ACCESSORFUNC_ID_GET_STR ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_PRE_IDS[ ACCESSORFUNC_ID_GET_LEN ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_PRE_IDS[ ACCESSORFUNC_ID_GET_LEN_STR ] );
## print( );

## print( );
## print( );

## ## Expected:		post_get		post_get_raw		post_isset		post_get_str		post_get_len		post_get_len_str
## print( MAP_ACCESSORFUNC_ENUMS_TO_POST_IDS[ ACCESSORFUNC_ID_GET ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_POST_IDS[ ACCESSORFUNC_ID_GET_RAW ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_POST_IDS[ ACCESSORFUNC_ID_ISSET ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_POST_IDS[ ACCESSORFUNC_ID_GET_STR ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_POST_IDS[ ACCESSORFUNC_ID_GET_LEN ] );
## print( MAP_ACCESSORFUNC_ENUMS_TO_POST_IDS[ ACCESSORFUNC_ID_GET_LEN_STR ] );
## print( );

## print( );

## print( );
## print( );
## print( );
## print( );

## print( );
## print( );
## print( );
## print( );

## print( );
## print( );
## print( );
## print( );

## print( 'ENUMs: ', ACCESSORFUNC_TYPE_INSTANCE );
## print( 'ENUMs: ', ACCESSORFUNC_TYPE_PROPERTY );
## print( 'ENUMs List: ', ENUM_LIST_ACESSORFUNC_TYPES );
## print( 'Map ENUM -> Type', MAP_ACCESSORFUNC_TYPE_NAMES[ ACCESSORFUNC_TYPE_INSTANCE ] );
## print( 'Map ENUM -> Type', MAP_ACCESSORFUNC_TYPE_NAMES[ ACCESSORFUNC_TYPE_PROPERTY ] );
## print( 'MapR Type -> ENUM', MAPR_ACCESSORFUNC_TYPE_NAMES.get( MAP_ACCESSORFUNC_TYPE_NAMES[ ACCESSORFUNC_TYPE_INSTANCE ], None ) );
## print( 'MapR Type -> ENUM', MAPR_ACCESSORFUNC_TYPE_NAMES.get( MAP_ACCESSORFUNC_TYPE_NAMES[ ACCESSORFUNC_TYPE_PROPERTY ], None ) );


## print( );
## print( );
## print( ENUM_LIST_ACCESSOR_FUNC_WORDS );
## print( );
## print( MAP_ACCESSORFUNC_WORDS );
## print( );
## print( MAPR_ACCESSORFUNC_WORDS );
## print( );
## print( MAP_ACCESSORFUNC_WORD_KEYS );
## print( );
## print( MAPR_ACCESSORFUNC_WORD_KEYS );

## ACCESSORFUNC_ID_SETUP_DEFAULTS					= 'setup_defaults';
## ACCESSORFUNC_ID_GET								= 'get';
## ACCESSORFUNC_ID_GET_RAW							= 'get_raw';
## ACCESSORFUNC_ID_ISSET							= 'isset';
## ACCESSORFUNC_ID_GET_STR							= 'get_str';
## ACCESSORFUNC_ID_GET_LEN							= 'get_len';
## ACCESSORFUNC_ID_GET_LEN_STR						= 'get_len_str';
## ACCESSORFUNC_ID_GET_DEFAULT_VALUE				= 'get_default_value';
## ACCESSORFUNC_ID_GET_ACCESSOR					= 'get_accessor';
## ACCESSORFUNC_ID_GET_ALLOWED_TYPES				= 'get_allowed_types';
## ACCESSORFUNC_ID_GET_ALLOWED_VALUES				= 'get_allowed_values';
## ACCESSORFUNC_ID_GET_BASE_ALLOWED_TYPES			= 'get_base_allowed_types';
## ACCESSORFUNC_ID_GET_BASE_ALLOWED_VALUES			= 'get_base_allowed_values';
## ACCESSORFUNC_ID_HAS_GETTER_PREFIX				= 'has_getter_prefix';
## ACCESSORFUNC_ID_GET_GETTER_PREFIX				= 'get_getter_prefix';
## ACCESSORFUNC_ID_GET_NAME						= 'get_name';
## ACCESSORFUNC_ID_GET_KEY							= 'get_key';
## ACCESSORFUNC_ID_GET_ACCESSOR_KEY				= 'get_accessor_key';
## ACCESSORFUNC_ID_GET_DATA_KEY					= 'get_data_key';
## ACCESSORFUNC_ID_SET								= 'set';
## ACCESSORFUNC_ID_ADD								= 'add';
## ACCESSORFUNC_ID_POP								= 'pop';
## ACCESSORFUNC_ID_RESET							= 'reset';
## ACCESSORFUNC_ID_GET_HELPERS						= 'get_helpers';
## ACCESSORFUNC_ID_GET_OUTPUT						= 'get_output';
## ACCESSORFUNC_ID_GET_GETTER_OUTPUT				= 'get_getter_output';