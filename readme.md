//
// Acecool's Source Code Navigator / Code Mapping System - Previously CodeMap + XCodeMapper now Stand-Alone and BUG FREE - Josh 'Acecool' Moser
//

//
---
// Read Me - Acecool Code Mapping System
---
//
---

- Content
  - Read Me - Acecool Code Mapping System
  - Index
  - About this plugin
  - About Me
  - Support this plugin




//
---
// About the Plugin - Acecool Code Mapping System
---
//
---

Acecool Code Mapping System was inspired by a common element most programming tools or IDEs contain - a sidebar with a map or tree of the file or project listing all classes, functions, etc... with a dropdown at the top to filter or mask the results below to include / exclude data from within the dropdown of classnames, function names, and more... and a search box to quickly find what you're looking for. This didn't exist in Sublime Text.

This addon was inspired by standard utility, and it was needed for efficiency in Sublime Text as no other had met the requirement I needed. By this, I mean that because of my car accident on May 7, 2011 which broke my neck, back and left me with severe nerve damage. I have been greatly hindered in the amount of work I am able to do. I am lucky to get a 40 hour work-week done in 3 to 6 months but it typically takes 6 monmths to a year. By creating this, which has taken a long time because of this limitation and because a lot of the time is required for physical therapy, care, doctors, managing depression, eating, constantly changing position because of pain, and more... By improving efficiency while coding, I can get more done in a shorter amount of time, and this can apply to anyone. But, for me this helps me fit more work into a shorter amount of time. I know I won't get anywhere close to where I want to be, and the hell I endure is something I'd never wish upon anyone - not even the person who did this to me - because it is hell. A state of living death, never knowing whether or not your legs will give out while on the stairs, or when I stop breathing while driving if I'll start breathing again, or if a specific moment where my bladder stops working is the last time to where it'll either burst because it never gives me control back or if it just releases constantly. I don't know if my left arm will start seizing, and if it does how long it'll be until it stops - it can go for days or a week - and during that time it is impossible to get any sleep. I don't know when I'm able to sleep because any small noise wakes me up and it can take hours to get to bed. I used to be a deep sleeper, but because of the entire issue with the broken neck and any motion caused severe pain like I was being electrocuted, along mith severe migraines and nausea, my body has stopped moving during sleep which isn't healthy. Our bodies need to move during sleep to relieve pressure on pressure points and promote blood-flow - this doesn't happen for me. I don't know if today is the day a doctor will accuse me, again, of selling my life-saving medication because it doesn't show up in urine ( it only shows up in blood ) tests despite all of the evidence that I do use the medication as directed for severe chronic pain stemming from the accident on May 7, 2011. A doctor did this recently despite showing the proof ( My arms are discolored where I alternate the patches, on top of that, because I can't bathe I only clean the arm before I place a patch on so as a patch is on for days there forms a ring as the patch migrates over the sklin leaving a trail of adhesive which picks up dirt, grime, sweat, etc...  On top of that, as I no longer go to a hospital to see my doctor in North Carolina, as I did in South Carolina, I have no place to safely dispose of my medication so I store it. I offered to provde over 3 years worth of patches with my dna on them to prove even further and beyond any doubt that I do take my medication, and that I do not sell them. The reason I don't have the full 5 tyears is because I disposed of them in SC in the MUSC hospital in those containers which are incinerated for biohazardous waste / needles, etc.... But, because I couldn't do that anymore, I had them and offered to provide them.. Te doctor inflicted so much pain and deepened my depression so much I was content on dying that day and I would've been fine taking a few people along for the ride.. Do no harm, that is the oath they take, but because of this politicized opiate epidemic, all legitimate users of the medication are persecuted in witch-hunts by villagers with pitch-forks and torches. By doctors with pitch forks and torches.. It's a disgusting practice.


The closest thing which did was a plugin called CodeMap which is a very simple system which takes a file extension and loads the file in the custom mappers folder to process each line of the source file and build a glob of text to return in order to be displayed. The text output was simplistic with a line or partial line output followed by a semi-colon and a line number.. When you double-clicked on a line with a line number suffix, it'd jump you to that line. The major problem with this is you end up duplicating categories in the output blob as there was no category system, meaning you end up with a lot of wasted space. I created a category system and a class system to get rid of these limitations but there were many more and a language barrier which prevented us from merging. I decided to take my system stand-alone to get away from circular import issues, and other issues.


I am reworking the limitations out of my system imposed by CodeMap to introduce a brand new Region based output system so every single character on a line can be clickable... Classnames can jump you to their point of origin. Function names can point you to theirs. Snippets can be added at the touch of a word, macros can be called, bookmarks can be navigated to, added or removed and maybe even named, and much much more... On top of the toolbox functionality to add a live search to filter / mask the output panel data... This is where XCodeMapper ( Dev Name for my addon to CodeMap ) became Acecool's Code Mapping System.. Something completely unique devoted to efficiency, cleanliness, feature-rich and ease of use with concise output showing you what you need to know about your code and hiding what isn't relevant on top of giving you the choice and power to display or hide even more based on the project and language you're working with and even adding rule systems to the file extensions themselves.

What makes Acecool's Code Mapping System so unique is the ability to get multiple language mappers to work with a single file extension..  .php files, in actuallity have around 6 languages which can be used in its extension. I personally like using templating systems with PHP but just because I prefer it one way doesn't mean everyone does - and because it is possible people will. So with my system you'll be able to set up the languages and define which file extensions they're active in and also set up rules to specify exactly when they take over and another yields to be more efficient.

For example - Instead of having to recode 6 languages for a .php file to properly map the 6 languages which can be contained within - you now only need to map the 6 languages, then set up rules so the language mappers know when you be active within the .php file... For instance - HTML is enabled outside of PHP tags <?(php)? ?>, CSS is active when HTML is active inside of STYLE tags. JavaScript is active when HTML is active inside of SCRIPT tags. RegEx is active ( using JavaScript OR PHP Implementation ) in various function calls when JavaScript OR PHP is active. SQL is mapped when PHP is active in various function calls or references.. So: PHP, HTML, JavaScript, RegEx, CSS, and SQL.... Instead of recoding all of those features every single file extension - the languages can be coded once ( with the option to extend a language and use that extension inside of a project, file extension, etc... or globally when that language is used ) and you define when the language is active meaning there's no need to waste time recoding the same thing over and over and over again - no repeated code... efficiency... cleanliness.. feature-rich..



Note: Some of the new features will include:


	- Command to change the order of code in a file to sort classes, functions, etc... to be alphabetically sorted... Will also be able to be set to automatically do this..

	- Command to add Wiki Style Args to all functions and objects -- ie @arg: _arg <DataType> Description, @return <DataType> Desc, more either on new rows or whatever... Can also be automatically added..

	- Command to generate a Wiki for the entire project for all objects, functions, arguments, data-types, etc... a fully automated, or by command, system to create usable data for developers to use for
		your package... This gets rid of the tedious work, although some will remain for populating the function args data-types and descriptions ( although you'll be able to register arg names you use -
		for instance for GMod Lua I always use _p for Player Entity, _w for Weapon Entity, _npc for Non-Player Character Entities, _vm for View Model Entity, _ent for Generic Entity / Prop, and so many
		more which would mean when detected, they'd be auto populated and handled ( This is why I always advocate designing and using a coding standard which involves everything from the way you style
		comments to argument names and more.... -- Also, this will be used for the automatic on_hover live-wiki style description boxes which open when you hover over an entry in the output panel...

	- Every Character in the output panel is mappable to more than 1 action... on_click, on_double_click, on_hover, and more... from executing macros, inserting snippets, jumping to lines of code,
		bookmarks, or more... You'll be able to map all characters to anything and the mapping evidence is hidden so you no longer see :<line_number> at the end of the line ( unless you want to with the
		template system ) which is exploded and retrieved in order to know which line something is related to... etc... all hidden, makes things cleaner, more concise, can add the start line for a func
		and the end line, and click on line for start and the end line number in line to jump to that, can map a class reference at the start of a call, the function reference, and so much more....

	- Threaded Performance - no more UI lockups during mapping ( This is one of the primary focuses right now )

	- Restructuring all of the Callbacks - instead of many args there'll be the basic self and the entry object which contains a ton of information about what is currently being looked at...

	- String-Blocks to prevent mapping stuff inside giant string blocks, similar to comment-blocks

	- Threaded Preprocessing for all open files, for all project files, etc... When a core file is changed, it can trigger this - or if you don't edit core files, it will trigger if enabled in config to
		ensure accessing code is so much faster..

	- Caching Job prioritization - Based on how many times you save a file, look at a file, data changed, etc... will determine the order / weight of a particular file in terms of mapping it so you can
		immediately map all of the most common / important files ( based on session and global ) to ensure you can continue your work ASAP...

	- Database Extension - Track how many times a file is loaded, how many times it is cached, how much data in bytes or KB has been added, how much is removed to get a balance...

	- Backup System - This is based on session, and more... there is a document somewhere which covers it, I could write 10 pages and barely touch the tip of the iceberg, it's that complex, but it is also
		as simple as ensuring the developer always has a backup to restore from ( Also using TKInter or some type of GUI to easily see the file preview, the data which has changed since the previous, and
		more )

	- Caching System Updates - Adding in flushing features so cached files won't be saved forever... For instance, if a file is deleted, there isn't any  point to keep the cached map ( a backup, yes, but
		the cached output and map, not really ).. A maximum number of cached files ( keeping the x most common, then the y latest )... and so many more features.s such as clearing the database on new session ( a session is when you start Sublime Text, andd then Close it... If it crashes, if you reload it within 1 hour [ configurable ] the session hasn't ended )...

	- More configuration options

	- Current configuration options which aren't linked, will be linked to code and be functional...



//
---
// About Me
---
//
---

My name is Josh Moser, I figured out my philsophy and dream of being early on in life which is to learn as much as possible and help those in need through teaching hence my nickname was born: Acecool or A School - to learn more than enough and learn how to properly educate anyone in order to be considered a walking school of knowledge, professionalism, courtesy, honor, and more.

I'll add more about myself and the hell I endure soon.




//
---
// Support Me / Plugin Development - Acecool Code Mapping System
---
//
---




//
---
// ACMS Major Features
---
//
---

	The features list is quite extensive, especially for version 1.0.0. If there is an asterisk next to a feature, it is planned for the near future. I'll list some of the larger functions as larger descriptions, and have an advancced features list which goes into line-item detail.


	- Mapping Source-Code into easily navigable output layout which creates hotlinks to jump to those lines in the source code
	- User configurable mappers - you decide what should be mapped and when those mapping files are loaded vs a default based on a map-loader rules-system which can define loading a specific set of mappers when a source-file is loaded from a specific directory, if the directory has a folder name in the path, if a certain project is loaded, if a certain file extension is used, and many more options.
	- User configurable actions bound up to each and every character in the output panel - You can use the default which links an output line to a source-code line, or you can add other links so a ClassName.NestedClass.FuncName( args ) can point to each class definition line, function definition line, and arguments can open an on_hover live-wiki-like documentation panel detailing the input arguments, data-types, and so on. The function can have this in addition to the clickable link to the source-code line quick-jump-to and it can show the argument information, return information, or anything you want.. You have full control over the output panel region mapping for each and every character in the output panel.
	- edit_settings_plus - edit_settings with more features such as: File Linking so if you click one file in the user or base panel, the other file will open in the other panel. The ability to load more than a single pair of configuration files into a settings window - you can open an unlimited number of views. The ability to add default text to a user or base view without showing the file as edited.
	-
	-
	-
	-
	-
	-
	-
	-




//
---
// ACMS Line-Item Features
---
//
---
	- Output Mapping Panel Configuration
	- Complete Configuration System
	- edit_settings_plus
	-
	-
	-
	-
	-
	-
	-
	-
	-
	-
	-
	-
	-
	-
	-







//
---
// Read Me - Acecool Code Mapping System
---
//
---




//
---
// Read Me - Acecool Code Mapping System
---
//
---



