##
## BatchFile - Default / Vanilla / Base Mapper - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent ACMS Mapper - As this is the User mapper, I've set up all possible includes for this mapper out!
from AcecoolCodeMappingSystem.maps.__mapper_imports__ import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##  Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
class ACMS_BatchFile_Default( XCodeMapperBase ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_BatchFile_Default'


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' )

		# class CSScriptHoverProvider implements HoverProvider {

		## Functions
		self.AddSyntax( CATEGORY_COMMENT, SEARCH_TYPE_STARTSWITH, 'rem ', ':: ', ': ' )


		## Functions / GoTo's / Anchors
		self.AddSyntax( CATEGORY_GOTO, SEARCH_TYPE_STARTSWITH, 'goto ', 'call ', 'exit' )

		## Look for Anchors where the line ( trimmed of spaces ) begins with 1 :, then has at least 1 UpperCase, LowerCase, Underscore or Hyphen Character and where the line ends with the same character set
		self.AddSyntax( CATEGORY_ANCHOR, SEARCH_TYPE_REGEX, '^:[A-Za-z_-]+$' )


		## Configuration based..
		self.AddSyntax( CATEGORY_CONFIG, SEARCH_TYPE_STARTSWITH, '@echo ' )

		## File based commands..
		self.AddSyntax( CATEGORY_CMD_FILE, SEARCH_TYPE_STARTSWITH, 'rename ', 'mklink ' )

		## Registry
		self.AddSyntax( CATEGORY_CMD_REGISTRY, SEARCH_TYPE_STARTSWITH, '`reg query' )

		## Iteration
		self.AddSyntax( CATEGORY_CMD_ITERATION, SEARCH_TYPE_STARTSWITH, 'FOR /F ' )

		## Definitions / Variables
		self.AddSyntax( CATEGORY_DEFINITION, SEARCH_TYPE_STARTSWITH, 'set ' )

		## Debugging / Optimization
		self.AddSyntax( CATEGORY_OPTIMIZATION, SEARCH_TYPE_STARTSWITH, 'echo ', 'echo.', 'cls' )


		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )


	##
	## Callback - these are the symbols from the file we're currently editings symbols( ) from the view - ie all extracted symbols from Sublime Text...
	## In Short: If working this should give us all of the information from the Command Palette when we look at @, :, etc.. for function names and everything... A potentially almost perfect way to create a universal mapper...
	##
	## Task: Allow symbols to be processed through the AddSyntax processor ( Make a syntax processor function outside of Run to do this ) - Then the full-line of code can be grabbed from the symbols SublimeText provides us and we'll have a basic universal mapping system....
	##
	def OnMappingFileSymbols( self, _view, _symbols ):
		## pass
		print( ' >> Symbols: ' + str( _symbols ) )
		## Symbols also don't give their function prefix...
		## if ( _code.startswith( 'def' ):

		## Symbols appear in a list, in a tuple with a region and the symbol...[((515, 525), 'PANEL:Init'), ((891, 910), 'PANEL:ControlValues'), ((1159, 1178), 'PANEL:PerformLayout')]

		## So for each entry
		for _entry in _symbols:
			## Tuples and lists use numerical indices
			## Grab the region object ( chars from start of file to X, and Total Chars in Region + X )
			_region = _entry[ 0 ]

			## Grab the partial code we're given - we can grab the full line using a helper function...
			_code = _entry[ 1 ]

			## Grab the line number using another helper...
			_line_number = self.GetSublimeViewLineNumberByRegion( _region )

			## Grab the line code by another helper...
			_line = self.GetFileDataByLine( _line_number )

			## Note - Symbols use regions meaning the starting char of from the entire code blob to the end... not line numbering so this has to be calculated...
			## Since other info is also lost we can really only guess at the data unless we check the file and look at other infomration...
			## self.AddEntry( CATEGORY_FUNCTION, _line_number, _code, _depth, _mode, _pattern, _class_cat )
			self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ), 0 )


	##
	## Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original ):
		## If the line starts with 'public ' then remove it from the code...
		_text = 'public '
		if _pattern == _text:
			_code = AcecoolLib.String.SubStr( _code, len( _text ), len( _code ) )

			## TODO: Return True or False to SKIP adding this entry - we do this so this line can be reprocessed after removing the data from the line...
			## return True


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------