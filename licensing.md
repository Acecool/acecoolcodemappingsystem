= Acecool's Code Mapping System - Licensing

	This is under the 'buy' option as there is no license key to purchase but there is an
	associated license - The Acecool License - I have posted it here in incredibly to lay
	out the primary functions of the license.

== Acecool License, Acecool Company License ( "ACL" )

All of Acecool's Code Mapping System is licensed under the Acecool License, also known as the Acecool Company License ( "ACL" ).

	TradeMark 'Acecool', '_Acecool', '__Acecool', ..., 'Acecool Company', ..., 'Acecool Medical', ..., 'Acecool Education', ..., and more.. ( TM ) 1989 - All Rights Reserved Josh 'Accecool' Moser <webmaster@acecoolco.com>
	Copyright (c) 2017 - All Rights Reserved - Josh 'Acecool' Moser <webmaster@acecoolco.com>

	Permission is hereby granted, free of charge, to any person ( the "People" ) obtaining a
	copy of this software and associated files ( the "Software" ), to deal in the Software
	with restrictions ( the "Restrictions" ) and to deal in the Software with certain liberties ( the "Liberties" ).

	Permission is hereby granted to students, teaching aides, tutors, doctors, or professors
	of Computer Science ( "CS Scholars" or "CS Scholar" ) to deal in the Software for the
	purpose of education with fewer Restrictions. The areas of lessened restrictions are
	focused in the areas of the distribution and profit to allow educators to receive
	compensation for teaching a class of students without granting the ability to charge for
	the Software in addition to authorizing a CS Scholor to transfer an	unmodified or
	modified copy from  personal or school computer to another CS Scholar in the
	participating class without being able to charge for the Software or the transfer.

	All transfers are required to come with a copy of this license and any recipient must
	agree to the License, Terms of Use, and End User License Agreement prior to initiating
	the transfer of the Software.

	The ACL does not grant you access to the use of any named or un-named
	protected Intellectual Property Rights in relation to the Software, the Author, the Company or with any other relation to any of the aforementioned elements.

	The ACL does not grant you access to the use of any registered or unregistered TradeMark.

	The ACL does not grant you any right to make the software public domain or available to the public in any shape or form.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED. IN NO EVENT SHALL THE AUTHORS, COPYRIGHT HOLDERS, TRADEMARK HOLDER,
	OR ANY MEMBER, EMPLOYEE, OR STAKE HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR
	OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

	Notice: This license may be altered at any time without notice and remain binding; If
	you do not agree with the latest changes from the time they are made public, you have 24
	hours to remove all copies of the Software. The rights granted and / or affirmed by this
	license are not unlimited. This clause / Notice has been a part of the ACL since its inception.