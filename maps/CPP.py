##
## C++ - Default / Vanilla / Base Mapper - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task: Set up Sublime Text Symbols Mapper as alternative to file-parsing... OR precursor so the default can use symbols and other mappers can add on to that - or I'll add an additional layer...
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Base Mapper Imports
from AcecoolCodeMappingSystem.maps.__mapper_imports__ import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Regular Expression Variables - these are the BASE expressions used for detecting functions, classes, definitions and more...
##
identifier									= '\b[[:alpha:]_][[:alnum:]_]*\b'
path_lookahead								= '(::\s*)?(' + identifier + '\s*::\s*)*' + identifier + ''
macro_identifier							= '\b[[:upper:]_][[:upper:][:digit:]_]*\b'
operator_method_name						= '\boperator\s*(?:[-+*/%^&|~!=<>]|[-+*/%^&|=!<>]=|<<=?|>>=?|&&|\|\||\+\+|--|,|->\*?|\(\)|\[\]|""\s*' + identifier + ')'
casts										= 'const_cast|dynamic_cast|reinterpret_cast|static_cast'
operator_keywords							= 'and|and_eq|bitand|bitor|compl|not|not_eq|or|or_eq|xor|xor_eq|noexcept'
control_keywords							= 'break|case|catch|continue|default|do|else|for|goto|if|_Pragma|return|switch|throw|try|while'
memory_operators							= 'new|delete'
basic_types									= 'asm|__asm__|auto|bool|_Bool|char|_Complex|double|float|_Imaginary|int|long|short|signed|unsigned|void'
before_tag									= 'struct|union|enum\s+class|enum\s+struct|enum|class'
declspec									= '__declspec\(\s*\w+(?:\([^)]+\))?\s*\)'
storage_classes								= 'static|export|extern|friend|explicit|virtual|register|thread_local'
type_qualifier								= 'const|constexpr|mutable|typename|volatile'
compiler_directive							= 'inline|restrict|__restrict__|__restrict'
visibility_modifiers						= 'private|protected|public'
other_keywords								= 'typedef|decltype|nullptr|' + visibility_modifiers + '|static_assert|sizeof|using|typeid|alignof|alignas|namespace|template'
modifiers									= storage_classes + '|' + type_qualifier + '|' + compiler_directive
non_angle_brackets							= '(?=<<|<=)'
generic_lookahead							= '<(?:\s*(?:' + path_lookahead + '|\d+)(?:' + path_lookahead + '|&(?!&)|\*|,|\.|<.*?>|\s|\d)*)?>'
data_structures_forward_decl_lookahead		= '(\s+' + macro_identifier + ')*\s*(:\s*(' + path_lookahead + '|' + visibility_modifiers + '|,|\s|<[^;]*>)+)?;'
non_func_keywords							= 'if|for|switch|while|decltype|sizeof|__declspec|__attribute__|typeid|alignof|alignas|static_assert'


generic_type_match							= '(?=(?!template)' + path_lookahead + '\s*' + generic_lookahead + '\s*\()'
func_call									= '(?:(::)\s*)?' + identifier + '\s*(::)\s*'
variable_func								= '(?:(::)\s*)?' + identifier + ''
section_generic_begin_end					= '(?=(?!template)' + path_lookahead + '\s*' + generic_lookahead + ')' # needs match < and >



##
##  Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
class ACMS_CPP_Default( XCodeMapperBase ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_CPP_Default'


	##
	## Override / Helper to allow for depth to be all uniform or based on indentation ( if you return _depth )
	##
	def OnCalcDepth( self, _category, _line_number, _code, _depth, _mode, _pattern ):
		return 0


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' )

		## Comments
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_BEGIN, SEARCH_TYPE_CONTAINS, '/*' )
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_FINISH, SEARCH_TYPE_CONTAINS, '*/' )
		self.AddSyntax( CATEGORY_COMMENT, SEARCH_TYPE_STARTSWITH, '//' )


		self.AddSyntax( CATEGORY_CLASS, SEARCH_TYPE_STARTSWITH, 'struct ', 'class ', 'union', 'friend' )

		## Functions
		## self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_REGEX, '^\b(const|final|noexcept|override)(\w*)(int|void|bool)(\w+)([a-zA-Z_-0-9]+)(\w*)\((.*)\)' )
		## self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_REGEX, r'^\s*(\w+)(\()(.*)(\))' )
		## self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_REGEX, r'^([a-zA-Z_][a-zA-Z0-9_-]+)\s*(\w+)(\()(.*)(\))' )
		self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_REGEX, r'^(const|final|noexcept|override|int|void|bool|\w+)\s*(\w+)(\()(.*)(\))' )

		## ## Look for Anchors where the line ( trimmed of spaces ) begins with 1 :, then has at least 1 UpperCase, LowerCase, Underscore or Hyphen Character and where the line ends with the same character set
		## self.AddSyntax( CATEGORY_ANCHOR, SEARCH_TYPE_REGEX, '^:[A-Za-z_-]+$' )


		## Configuration based..
		## self.AddSyntax( CATEGORY_CONFIG, SEARCH_TYPE_STARTSWITH, '@echo ' )

		## ## File based commands..
		## self.AddSyntax( CATEGORY_CMD_FILE, SEARCH_TYPE_STARTSWITH, 'rename ', 'mklink ' )

		## ## Registry
		## self.AddSyntax( CATEGORY_CMD_REGISTRY, SEARCH_TYPE_STARTSWITH, '`reg query' )

		## ## Iteration
		## self.AddSyntax( CATEGORY_CMD_ITERATION, SEARCH_TYPE_STARTSWITH, 'FOR /F ' )

		## ## Definitions / Variables
		self.AddSyntax( CATEGORY_DEFINITION, SEARCH_TYPE_STARTSWITH, '#define ' )

		## String Literals: https://stackoverflow.com/questions/36597386/match-c-strings-and-string-literals-using-regex-in-python
		self.AddSyntax( CATEGORY_DEFINITION, SEARCH_TYPE_REGEX, r'(?P<prefix>(?:\bu8|\b[LuU])?)(?:"(?P<dbl>[^"\\]*(?:\\.[^"\\]*)*)"|\'(?P<sngl>[^\'\\]*(?:\\.[^\'\\]*)*)\')|R"([^"(]*)\((?P<raw>.*?)\)\4"' )

		## ## Debugging / Optimization
		## self.AddSyntax( CATEGORY_OPTIMIZATION, SEARCH_TYPE_STARTSWITH, 'echo ', 'echo.', 'cls' )


		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )


	##
	## Callback - these are the symbols from the file we're currently editings symbols( ) from the view - ie all extracted symbols from Sublime Text...
	## In Short: If working this should give us all of the information from the Command Palette when we look at @, :, etc.. for function names and everything... A potentially almost perfect way to create a universal mapper...
	##
	## Task: Allow symbols to be processed through the AddSyntax processor ( Make a syntax processor function outside of Run to do this ) - Then the full-line of code can be grabbed from the symbols SublimeText provides us and we'll have a basic universal mapping system....
	##
	def OnMappingFileSymbols( self, _view, _symbols ):
		## pass
		print( ' >> Symbols: ' + str( _symbols ) )
		## Symbols also don't give their function prefix...
		## if ( _code.startswith( 'def' ):

		## Symbols appear in a list, in a tuple with a region and the symbol...[((515, 525), 'PANEL:Init'), ((891, 910), 'PANEL:ControlValues'), ((1159, 1178), 'PANEL:PerformLayout')]

		## So for each entry
		for _entry in _symbols:
			## Tuples and lists use numerical indices
			## Grab the region object ( chars from start of file to X, and Total Chars in Region + X )
			_region = _entry[ 0 ]

			## Grab the partial code we're given - we can grab the full line using a helper function...
			_code = _entry[ 1 ]

			## Grab the line number using another helper...
			_line_number = self.GetSublimeViewLineNumberByRegion( _region )

			## Grab the line code by another helper...
			_line = self.GetFileDataByLine( _line_number )

			##
			_file_view = self.GetMappedFileView( )
			_scope_name = _file_view.scope_name( _region.begin( ) )

			## Note - Symbols use regions meaning the starting char of from the entire code blob to the end... not line numbering so this has to be calculated...
			## Since other info is also lost we can really only guess at the data unless we check the file and look at other infomration...
			## self.AddEntry( CATEGORY_FUNCTION, _line_number, _code, _depth, _mode, _pattern, _class_cat )
			self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ) + ' - Scope: ' + _scope_name, 0 )


	##
	## Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original ):

			## TODO: Return True or False to SKIP adding this entry - we do this so this line can be reprocessed after removing the data from the line...
			## return True


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )


	##
	## Similar to PreOnEntry except instead of modifying Code, it modifies the Category...
	##
	def PreOnAddEntryCategory( self, _category, _line_number, _code, _depth, _search_mode, _pattern ):


		## If our class or struct ends with ; then it is likely a header / definition... move it to definitions.
		if _category == CATEGORY_CLASS and _code.endswith( ';' ):
			_category = CATEGORY_DEFINITION



		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).PreOnAddEntryCategory( _category, _line_number, _code, _depth, _search_mode, _pattern )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------