##
## Sublime Settings - User Mapper - <Author Name>
##


##
## Note: All Sublime* Files, at this time, use the JSON language - these User files only exist as placeholders for when I add special language mappings designed to look for special rules in the files and bring them to the attention of the user.
## 		 In order to unlock using these files, by default, you will need to change the ACMS_Definitions file to change the language mapping table active_language from JSON to Sublime*
##


##
## Task List - Proper User Mapper Creation and / or Modification Procedure
##
## Task: Copy this file to Packages\User\AcecoolCodeMappingSystem\maps\LanguageName_MapperName.py
## Task: Make a plan or design of what you want to accomplish with this mapper...
## Task: Modify this mapper...
## Task: If you need more tools at your disposal or find an issue: Submit bug reports, feature requests or constructive criticism!
## Task: When you've finished with this mapper for your project, or for a language - Release it?
## Note: If you want to, you may submit it on my repo and if I like it, I will add it and it will retain your author name, site info, other info you want to include, etc... I may ask you to make changes before accepting it, or I may make the changes myself if I find any bugs, etc..
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent ACMS Mapper - As this is the User mapper, I've set up all possible includes for this mapper out!
from AcecoolCodeMappingSystem.maps.SublimeSettings import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Everything you need to create your own - The base class which very limited structure, the CFG_ options above and the ENUM value to use to set to it... Simply rename them...
##
## class ACMS_SublimeSettings_User( ACMS_JSON_Default ):
## class ACMS_SublimeSettings_User( XCodeMapperBase ):
class ACMS_SublimeSettings_User( ACMS_SublimeSettings_Default ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_SublimeSettings_User'




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------