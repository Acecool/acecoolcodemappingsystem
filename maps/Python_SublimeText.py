##
## Python - User Mapper - <Author Name>
##


##
## Task List - Proper User Mapper Creation and / or Modification Procedure
##
## Task: Copy this file to Packages\User\AcecoolCodeMappingSystem\maps\LanguageName_MapperName.py
## Task: Make a plan or design of what you want to accomplish with this mapper...
## Task: Modify this mapper...
## Task: If you need more tools at your disposal or find an issue: Submit bug reports, feature requests or constructive criticism!
## Task: When you've finished with this mapper for your project, or for a language - Release it?
## Note: If you want to, you may submit it on my repo and if I like it, I will add it and it will retain your author name, site info, other info you want to include, etc... I may ask you to make changes before accepting it, or I may make the changes myself if I find any bugs, etc..
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent ACMS Mapper - As this is the User mapper, I've set up all possible includes for this mapper out!
from AcecoolCodeMappingSystem.maps.Python import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##	Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
## class ACMS_Python_User( ACMS_Python_Dev ):
## class ACMS_Python_User( ACMS_Python_ACMS ):
## class ACMS_Python_User( XCodeMapperBase ):
class ACMS_Python_SublimeText( ACMS_Python_Default ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_Python_SublimeText'


	##
	## Callback - Used to setup rules to track code, expand AccessorFuncs, etc.. at to display the output in the Code - Map Panel. Priority is handled using standard English Reading direction ie using self.AddSyntax( COMMENT, STARTSWITH, '#', '##' ) means ## would never be handled.
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' )

		##
		##

		##
		##


		##
		## Mapper Features
		##
		## self.AddFeature( 'User Class - This is what you are to copy / paste when updates come around - Soon you will not need to! I am going to separate the classes from each other and move them so the user folder is for you..' )
		## self.AddFeature( '' )
		## self.AddFeature( '' )
		## self.AddFeature( '' )


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )		## self.CallParent( 'OnSetupSyntax' )


	##
	## Callback - Used to either prevent an entry from being added by returning True / False, to alter the code being added by returning a String, to look for Syntax or other errors and add an Error Entry or Warning Entry, etc..
	##
	def PostOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original ):
		## Get the category type
		_category_type = self.GetCategoryType( _category );


		##
		## If we are looking at a class line, then lets make some changes if we extend sublime text objects - basically like AccessorFunc except we're adding inherited functions to it..
		##
		if ( _category_type == CATEGORY_TYPE_CLASS ):
			## Grab our class category...
			_class_cat = self.GetActiveClass( )

			##
			if ( _code.find( 'sublime_plugin.EventListener' ) > -1 ):
				pass;
				## self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).is_applicable( self, _settings )', _depth, _mode, _pattern, _class_cat );
				## self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).applies_to_primary_view_only( self )', _depth, _mode, _pattern, _class_cat );
				## self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).__init__( self, _view )', _depth, _mode, _pattern, _class_cat );


			##
			if ( _code.find( 'sublime_plugin.WindowEventListener' ) > -1 ):
				pass;
				## self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).is_applicable( self, _settings )', _depth, _mode, _pattern, _class_cat );
				## self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).applies_to_primary_view_only( self )', _depth, _mode, _pattern, _class_cat );
				## self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).__init__( self, _view )', _depth, _mode, _pattern, _class_cat );


			##
			if ( _code.find( 'sublime_plugin.ViewEventListener' ) > -1 ):
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).is_applicable( self, _settings )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).applies_to_primary_view_only( self )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).__init__( self, _view )', _depth, _mode, _pattern, _class_cat );


			##
			if ( _code.find( 'sublime_plugin.CommandInputHandler' ) > -1 ):
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).name( self )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).next_input( self, **_args )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).placeholder( self )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).initial_text( self )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).preview( self, _arg )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).validate( self, _arg )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).cancel( self )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).confirm( self, _arg )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).create_input_handler_( self, **_args )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).preview_( self, _value )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).validate_( self, _value )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).cancel_( self )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).confirm_( self, _value )', _depth, _mode, _pattern, _class_cat );


			##
			if ( _code.find( 'sublime_plugin.ApplicationCommand' ) > -1 ):
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).__init__( self, _view )', _depth, _mode, _pattern, _class_cat );
				## self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).run_( self, _edit, **_args )', _depth, _mode, _pattern, _class_cat );
				## self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).run( self, _edit )', _depth, _mode, _pattern, _class_cat );


			##
			if ( _code.find( 'sublime_plugin.TextCommand' ) > -1 ):
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).__init__( self, _view )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).run_( self, _edit, **_args )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).run( self, _edit )', _depth, _mode, _pattern, _class_cat );
				## self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).( self )', _depth, _mode, _pattern, _class_cat );
				## self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).( self )', _depth, _mode, _pattern, _class_cat );
				## self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).( self )', _depth, _mode, _pattern, _class_cat );
				## self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).( self )', _depth, _mode, _pattern, _class_cat );
				## self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).( self )', _depth, _mode, _pattern, _class_cat );


			##
			if ( _code.find( 'sublime_plugin.WindowCommand' ) > -1 ):
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).__init__( self, _view )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).run_( self, _edit, **_args )', _depth, _mode, _pattern, _class_cat );
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, 'def super( ).run( self, _edit )', _depth, _mode, _pattern, _class_cat );




			##
			## Fallthrough to thep parent so their syntax can be added to our own...
			##
			super( ).PostOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original );


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------