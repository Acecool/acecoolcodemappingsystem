//
---
// Acecool Code Mapping System - maps/ folder ReadMe
---
//
---




//
---
// Intro
---
//
---

This readme was created to show you how ACMS chooses which files to interact with within. It will walk you through creating a language id which can be used to assign or alter
existing configuration keys to values for when that language id is active. It will also explain exactly how the file naming system looks for files based on the configuration
values.




//
---
// In short...
---
//
---

- Create a language id by adding a new key to ACMS_Definitions configuration file named ( Which looks for file.FileExtension to activate MyNewLanguageID ):

	// Some information - helpful to group like languages together or your own method of organization for these entries...
	"acms_language_map->FileExtension":					"MyNewLanguageID",


- That language ID will be used to find the language configuration table created using ( which will use JavaScript_ACMS.py in the maps\ folder and use any
	additional configuration keys / values you add from any of the configuration files prefixed with ACMS_ - this is the only table which precedes the
	ACMS_RunTime configuration file although RunTime configuration isn't meant to store normal configuration values - only those relevant to the Users
	session with ACMS ):

	// MyProjectName Configuration Data
	"acms_language_config->MyNewLanguageID":
	{
		// The language name...
		"language_name":									"JavaScript - Acecool UserScript Framework Project Configuration",

		// The Language Identifier ( Used in file-names )
		"active_language":									"JavaScript",

		// The Default language mapper ( IE AutoHotkey_Default.py in maps/ - Case Sensitive )
		"active_mapper":									"ACMS",

		// Anything else you want...
		// ...

		// Anything else you want...
		// ...

		// Anything else you want...
		// ...
	},


- active_language and active_mapper values have large impacts on the file which will be looked for in maps\ - note: active_mapper is an optional value.

	- IF active_language is "Default" and active_mapper is "Default", "", or null
		- The resulting filename will be: __default__.py

	- If active_mapper is "Default", "", or null
		- The resulting filename will be: <active_language ONLY>.py - excluding <>s - this is to allow primary mappers to show alphabetically above all same language instead of being lost in the middle.



- language_name isn't used for anything other than letting you, the user, know which language is active... It is the human-friendly / readable name..



//
---
// Creating a new language id
---
//
---

When you create a new extension mapping in the ACMS_Definitions configuration file, you can assign any Language ID to the extension. In the future instead of 'key': 'LanguageID',
a table will be used to allow more flexibility such as being able to assign different language IDs to an extension based on the folder the source file is residing in, or which
project the window has loaded, and much more..

Creating a "Language ID" is very easy and it gives you a place to configure everything when that Language ID is active from the mapper being used to changing any non-language
configuration data and more...

To create an ID simply create a new entry in the ACMS_Definitions configuration file... For example, by adding the following line in the User configuration file:


	// Blah Blah Blah
	"acms_language_map->abc":								"MyProjectName",


... I will have created a map so that any file bearing the extension 'abc' such as 'readme.abc' will be instructed via ACMS to use configuration from the MyProjectName Language
ID configuration table before trying elsewhere... If I do not create the following below:


	// MyProjectName Configuration Data
	"acms_language_config->MyProjectName":
	{
		// The language name...
		"language_name":									"Python",

		// The Language Identifier ( Used in file-names )
		"active_language":									"Default",

		// The Default language mapper ( IE AutoHotkey_Default.py in maps/ - Case Sensitive )
		"active_mapper":									"ACMS",

		// Anything else you want...
		// ...

		// Anything else you want...
		// ...

		// Anything else you want...
		// ...
	},


... then any file with abc as its extension will use data from the ACMS_Default language configuration table instead... However, by creating that table I have set it to use the
Default Language / Mapper with a mapper using the name ACMS. This means Default_ACMS.py will need to exist within the maps/ folder - if it does not then it will look for abc.py
and if that doesn't exist, then it will look for __default__.py

ACMS Always looks for files in the Packages\User\AcecoolCodeMappingSystem\maps\ folder first before looking in the standard / default packages folder or as it is known:
Packages\AcecoolCodeMappingSystem\maps\

The active_mapper field does not need to contain any data - you can set it to null or '' - if it is set as "", null, or "Default" then the _ will be omitted from the name... ie:
using Example language and Default mapper then ACMS will look for Example.py instead of Example_Default.py. This is to allow the Default / Primary mapper to be displayed, when
ordered alphabetically, above the others.

There is an additional condition - if the mapper name is "Default", '', or null and the language is also "Default" then instead of Default.py which could be in a LARGE list of
files - I have chosen to use the name __default__.py which will show at the top of the list if you're using Windows and at the bottom if using *Nix systems when the list is
ordered alphabetically allowing you to easily find it and modify it in the User\ ACMS maps\ folder.