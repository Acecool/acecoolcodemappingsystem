##
## C - User Mapper - <Author Name>
##


##
## Task List - In order to modify
##
## Task: Copy this file to Packages\User\AcecoolCodeMappingSystem\maps\LanguageName_User.py
## Task: Make a plan or design of what you want to accomplish..
## Task: Begin Modifying...
## Task: Submit bug reports, feature requests or constructive criticism!
## Task: Release?
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent ACMS Mapper - As this is the User mapper, I've set up all possible includes for this mapper out!
from AcecoolCodeMappingSystem.maps.C import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Everything you need to create your own - The base class which very limited structure, the CFG_ options above and the ENUM value to use to set to it... Simply rename them...
##
## class ACMS_C_User( ACMS_C_Default ):
## class ACMS_C_User( XCodeMapperBase ):
class ACMS_C_User( ACMS_C_Default ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_C_User'




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------