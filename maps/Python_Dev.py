##
## Python - Developer Mapper - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent Language
from AcecoolCodeMappingSystem.maps.Python import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla - In Development Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## I'm using the Python class for development of new functionality because XCodeMapper uses Python... These functions are place-holders for new functionality planned...
##
class ACMS_Python_Dev( ACMS_Python_Default ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_Python_Dev'



	##
	## The following are implemented - they're in here for testing..
	##


	##
	## Callback - these are the symbols from the file we're currently editings symbols( ) from the view - ie all extracted symbols from Sublime Text...
	##
	def OnMappingFileSymbols( self, _view, _symbols ):
		## Note: Symbols should be all of the functions, vars, etc... We'll see... This would be HUGE in creating an automated mapper though..
		pass


	##
	## Callback - Determines the indentation level to use for entries - Categories are at -1 ( not possible to use ), 0 is indented once, nested categories indent further. Default is 4 spaces per level, can be configured to use any character combination.
	##
	def OnCalcDepth( self, _category, _line_number, _code, _depth, _mode, _pattern ):
		##
		## OnCalcCodeAlignment Development Test -
		##
		## If we use OnCalcDepth and we Add		++ depth relative to _depth ie ( return _depth + 1 or return X when _depth	<	X ), ex: we indent more on the LEFT than normal, then we DELETE the whitespace between VAR_END<HERE> all the way through <HERE>=... in the amount of how much was added on the left.. ie we simply offset the Delta to ensure the code aligns properly...
		## If we use OnCalcDepth and we Subtract-- depth relative to _depth ie ( return _depth - 1 or return 0 when _depth isn't 0 ), ex: we indent less on the LEFT than normal, then we	ADD the whitespace between the var<HERE> all the way through <HERE>=... in the amount of how much was removed on the left.. ie we simply offset the Delta to ensure the code aligns properly...
		##
		## This is so, no matter how it is written - as long as it is aligned in your code, it will be aligned in the mapper...
		##
		if ( _category == CATEGORY_CONFIG ):
			return _depth - 1

		## Return 0 for everything else..
		return 0


	##
	## The following ARE NOT IMPLEMENTED!!!!
	##


	##
	## OVerride / Helper to modify gutter icons before they're added or to prevent them from being added at all...
	## Task: Create functionality
	##
	def OnCalcGutterIcon( self ):
		## Basically - If function discovered in PreOnAddEntry we can do self.AddGutterIcon( line_number, self.GetGutterIcon( 'function', 'ƒ' ) ) and this function will let you capture it before it is added - so if we want to modify behavior ( for the _User class - to alter to a different icon return string id / or number enum of gutter icon ) or prevent it from being displayed ( Return boolean )
		## if ( _category == CATEGORY_CLASS and _icon_id == ICON_CLASS ):
		##		## something along these lines...
		##		return ICON_CLASS_HEADER
		##
		##		## or.. to stop it from showing up...
		##		return False
		pass


	##
	## Override / Helper - Callback used to override the indent characters used... IE Lets say you use tabs for indenting... Now lets say you want to create a TREE style view.. Use this function... Inject code - ie injecting into tabs is easy, add at the beginning and it is absorbed, up to 3 chars without the tab jumping ( or 7 on computers which have it set to 8 spaces per tab )... Or use spaces and use my FormatColumn code to absorb your changes...
	##
	## Used to create output similar to this ( Callback is currently a WORK IN PROGRESS and is considered non-functional ):
	##
	##	┌─☼ Class Functions Table - Category...
	##	│ ├──☼ Vector Class
	##	│ │ ├─☼ ƒ X( )
	##	│ │ ├─☼ ƒ Y( )
	##	│ │ └─☼ ƒ Z( )
	##	│ │
	##	│ └──☼ Angle Class
	##	│	├─☼ ƒ P( )
	##	│	├─☼ ƒ Y( )
	##	│	└─☼ ƒ R( )
	##	│
	##	└─☼ Functions Category
	##	├──☼ ƒ isstring( _data )
	##	├──☼ ƒ isnumber( _data )
	##	├──☼ ƒ isfunction( _data )
	##	└──☼ ƒ isbool( _data )
	##
	def OnCalcDepthChars( self, _category, _line_number, _code, _depth, _search_mode, _pattern, _default_chars ):
		pass






##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla - In Development Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------