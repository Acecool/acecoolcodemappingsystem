##
## Python - User Mapper - <Author Name>
##


##
## Task List - Proper User Mapper Creation and / or Modification Procedure
##
## Task: Copy this file to Packages\User\AcecoolCodeMappingSystem\maps\LanguageName_MapperName.py
## Task: Make a plan or design of what you want to accomplish with this mapper...
## Task: Modify this mapper...
## Task: If you need more tools at your disposal or find an issue: Submit bug reports, feature requests or constructive criticism!
## Task: When you've finished with this mapper for your project, or for a language - Release it?
## Note: If you want to, you may submit it on my repo and if I like it, I will add it and it will retain your author name, site info, other info you want to include, etc... I may ask you to make changes before accepting it, or I may make the changes myself if I find any bugs, etc..
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent ACMS Mapper - As this is the User mapper, I've set up all possible includes for this mapper out!
from AcecoolCodeMappingSystem.maps.Python import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##	Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
## class ACMS_Python_User( ACMS_Python_Dev ):
## class ACMS_Python_User( ACMS_Python_ACMS ):
## class ACMS_Python_User( XCodeMapperBase ):
class ACMS_Python_User( ACMS_Python_Default ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_Python_User'


	##
	## Callback - Used to setup rules to track code, expand AccessorFuncs, etc.. at to display the output in the Code - Map Panel. Priority is handled using standard English Reading direction ie using self.AddSyntax( COMMENT, STARTSWITH, '#', '##' ) means ## would never be handled.
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' )

		##
		##

		##
		##


		##
		## Mapper Features
		##
		## self.AddFeature( 'User Class - This is what you are to copy / paste when updates come around - Soon you will not need to! I am going to separate the classes from each other and move them so the user folder is for you..' )
		## self.AddFeature( '' )
		## self.AddFeature( '' )
		## self.AddFeature( '' )


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )		## self.CallParent( 'OnSetupSyntax' )


	##
	## Callback - Used to convert found AccessorFunc Calls into Multiple Entries in XCodeMapper so the Getter / Setter and other functions created by the AccessorFunc call are added to the Map rather than the call.
	##
	def OnAccessorFuncExpansion( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Base Args - _tab will be the first arg in all cases...
		_arg_tab = AcecoolLib.Table.GetValue( _args, _args_count, 0, self.__name__ )

		## This is the first arg without any decimals - and we remove everything before last decimal in TitleCase - ClassName ie Timekeeper or Data...
		_arg_class = AcecoolLib.String.SubStr( _arg_tab, 10, len( _arg_tab ) ).title( )

		##
		##

		##
		##


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnAccessorFuncExpansion( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )		## self.CallParent( 'OnAccessorFuncExpansion', _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


	##
	## Create Custom AccessorFuncs and ConfigAccessorFuncs here...
	##
	def OnSetupAccessorFuncs( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupAccessorFuncs', 'Creating custom AccessorFuncs for the current mapper ( ' + self.GetSetting( 'active_language', 'None' ) + ' )!' )

		## Example AccessorFunc Addition
		## self.AccessorFunc( 'Name', CFG_OR_OTHER_DEFAULT_VALUE_ )

		## Example Config AccessorFunc Addition
		## self.ConfigAccessorFunc( 'FuncNameDefinitionSearch', CFG_FUNC_NAME_SEARCH )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupAccessorFuncs( )		## self.CallParent( 'OnSetupAccessorFuncs' )


	##
	## Setup this mapper configuration values here...
	##
	def OnSetupConfig( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupConfig', 'Applying custom settings for the current mapper ( ' + self.GetSetting( 'active_language', 'None' ) + ' )!' )


		##
		## Set the values of the config additions the mapper...
		##

		## Custom A
		## self.SetCfgNameXXXXXXXName( CFG_XLUA_XXXXXXX )

		## Custom B
		## self.SetNonCfgNameXXXXXXXName( CONST_XLUA_XXXXXXX )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupConfig( )		## self.CallParent( 'OnSetupConfig' )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------