##
## Garry's Mod Lua - DarkRP Mapper - <Author Name>
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent ACMS Mapper - As this is the User mapper, I've set up all possible includes for this mapper out!
## from AcecoolCodeMappingSystem.maps.GMod_Lua import *
from AcecoolCodeMappingSystem.maps.GMod_Lua_User import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Everything you need to create your own - The base class which very limited structure, the CFG_ options above and the ENUM value to use to set to it... Simply rename them...
##
## class ACMS_GMod_Lua_DarkRP( ACMS_GMod_Lua_Default ):
## class ACMS_GMod_Lua_DarkRP( XCodeMapperBase ):
class ACMS_GMod_Lua_DarkRP( ACMS_GMod_Lua_User ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_GMod_Lua_DarkRP'




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------