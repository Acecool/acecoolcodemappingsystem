##
## Acecool's Source Code Navigator - Mapping Class Linker - Josh 'Acecool' Moser
##
## This file exists to ensure all of the files in Packages/User/CodeMap/custom_mappers/ can be IDENTICAL and small.
## They are redirected to Packages/AcecoolSourceCodeNavigator/ along with linking the settings files, more than 1 mapper per file, and more...
## 	ie PHP uses approximately 6 languages in a .php extension file: PHP, SQL, RegEx ( PHP Implementation ), HTML, CSS, JavaScript, RegEx ( JavaScript Implementation ).
##	and with CodeMap it isn't easily possible to do this... and oleg-shiro won't implement it...
##


##
## Imports
##

## Import Sublime Library
## import sublime
## import sublime_plugin

## import os

## import errno

## import io

## import datetime

## import time

## Import the definitions ( Categories, enumerators, etc... everything definition based )
## from AcecoolCodeMappingSystem.AcecoolCodeMappingSystemDefinitions import *

## ## Import Acecool's Library of useful functions and globals / constants...
## from AcecoolCodeMappingSystem.AcecoolLib_Python import *

## ## Import Acecool's Library of useful functions and globals / constants...
## from AcecoolCodeMappingSystem.AcecoolLib_Sublime import *

## ## Import Acecool's Library of useful functions and globals / constants...
## from AcecoolCodeMappingSystem.AcecoolLib_SublimeText3 import *


##
##
##
class AcecoolCodeMappingSystemClassLinker:
	##
	## Important Object Values!
	##

	## Name / ID of the XCodeMapperBase
	__name__ = 'AcecoolCodeMappingSystemClassLinker'


	##
	##
	##
	def __init__( self, _ext ):
		pass

		##


	##
	##
	##
