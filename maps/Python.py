##
## Python - Default / Vanilla / Base Mapper - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task: Set up Sublime Text Symbols Mapper as alternative to file-parsing... OR precursor so the default can use symbols and other mappers can add on to that - or I'll add an additional layer...
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Base Mapper Imports
from AcecoolCodeMappingSystem.maps.__mapper_imports__ import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##	Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
class ACMS_Python_Default( XCodeMapperBase ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_Python_Default'


	##
	##
	##
	def DeveloperFunc( self ):
		pass


	##
	##
	##
	def OnRunDeveloperFunc( self ):
		pass


	##
	## Callback used to alter Code - Map Panel configuration per mapper...
	##
	def OnSetupCodeMapPanel( self, _panel, _settings ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_SETUP, 'OnSetupCodeMapPanel' )

		## Disable Scroll Past End... Note: By default I set this to false - if you want it re-enabled, set it here!
		## _panel.settings( ).set( 'scroll_past_end', False )


	##
	## Callback - Used to setup rules to track code, expand AccessorFuncs, etc.. at to display the output in the Code - Map Panel. Priority is handled using standard English Reading direction ie using self.AddSyntax( COMMENT, STARTSWITH, '#', '##' ) means ## would never be handled.
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' )

		## Comments
		self.AddSyntax( CATEGORY_COMMENT, SEARCH_TYPE_STARTSWITH, '##', '#' )
		self.AddSyntax( CATEGORY_STRING_BLOCK_BEGIN, SEARCH_TYPE_CONTAINS, "'''", '"""' )
		self.AddSyntax( CATEGORY_STRING_BLOCK_FINISH, SEARCH_TYPE_CONTAINS, "'''", '"""' )

		## Functions and Class
		self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_STARTSWITH, 'def ' )
		self.AddSyntax( CATEGORY_CLASS, SEARCH_TYPE_STARTSWITH, 'class ' )
		self.AddSyntax( CATEGORY_IMPORT, SEARCH_TYPE_STARTSWITH, 'import ', 'from ' )

		## Debugging / Optimization Earmarks
		self.AddSyntax( CATEGORY_OPTIMIZATION, SEARCH_TYPE_STARTSWITH, 'print(' )
		self.AddSyntax( CATEGORY_ERROR, SEARCH_TYPE_STARTSWITH, 'self:' )
		self.AddSyntax( CATEGORY_ERROR, SEARCH_TYPE_REGEX, '^[a-zA-Z0-9_-]:[a-zA-Z0-9_-]' )
		self.AddSyntax( CATEGORY_ERROR, SEARCH_TYPE_CONTAINS, '.endswidth', 'startswidth' )

		## Variables / Declarations
		if ( self.GetSetting( 'output_const_definitions', False ) ):
			self.AddSyntax( CATEGORY_CONFIG, SEARCH_TYPE_REGEX, '^CFG_\w+\s+\=' )
			self.AddSyntax( CATEGORY_ENUMERATION, SEARCH_TYPE_REGEX, '^ENUM_\w+\s+\=' )
			self.AddSyntax( CATEGORY_CONSTANT, SEARCH_TYPE_REGEX, '^CONST_\w+\s+\=', '^[A-Z_]+\s+\=' )


		##
		## Mapper Features
		##
		self.AddFeature( '- Language: Python using ' + self.__name__ )
		self.AddFeature( 'Detection for comments, functions, classes and print statements for debugging / optimization...' )
		self.AddFeature( 'Class Headers. Child Functions sorted into nested categories and global / local to the scope functions in their own category..' )
		self.AddFeature( 'Basic Syntax-Error Detection for Class and Function Definitions - if : doesn\'t exist then [ ERROR ] is added to the output line, if applicable, and a Syntax Error Entry is added!' )
		self.AddFeature( 'Added new feature: Jan 11 2018 - CFG_DISPLAY_FUNCTIONS_ALTERNATIVELY / CFG_DISPLAY_FUNCTIONS_ALTERNATIVE_FULL_DEPTH - These lets you choose to show functions within classes in the nested-category layout, or similar to LuaTable.FunctionName( _args ) style with the second Cfg allowing you to show more than just the direct-parent ( which.will.follow.the.shallowest.class.to.our.FunctionName( _args ) - This can save space, it can cost space... It is a unique way to display the data and it sorts... For those worried about too little vertical room - you may want to try this! )' )
		## self.AddFeature( '' )
		## self.AddFeature( '' )



		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )


	##
	## Callback - these are the symbols from the file we're currently editings symbols( ) from the view - ie all extracted symbols from Sublime Text...
	## In Short: If working this should give us all of the information from the Command Palette when we look at @, :, etc.. for function names and everything... A potentially almost perfect way to create a universal mapper...
	##
	## Task: Allow symbols to be processed through the AddSyntax processor ( Make a syntax processor function outside of Run to do this ) - Then the full-line of code can be grabbed from the symbols SublimeText provides us and we'll have a basic universal mapping system....
	##
	def OnMappingFileSymbols( self, _view, _symbols ):
		## Symbols appear in a list, in a tuple with a region and the symbol...[((515, 525), 'PANEL:Init'), ((891, 910), 'PANEL:ControlValues'), ((1159, 1178), 'PANEL:PerformLayout')]

		## So for each entry
		for _entry in _symbols:
			## Tuples and lists use numerical indices
			## Grab the region object ( chars from start of file to X, and Total Chars in Region + X )
			_region = _entry[ 0 ]

			## Grab the partial code we're given - we can grab the full line using a helper function...
			_code = _entry[ 1 ]

			## Grab the line number using another helper...
			_line_number = self.GetSublimeViewLineNumberByRegion( _region )
			## _line_number = 1

			## Grab the line code by another helper...
			_line = self.GetFileDataByLine( _line_number )

			_file_view = self.GetMappedFileView( )
			_scope_name = _file_view.scope_name( _region.begin( ) ).strip( )

			## Grab the difference between a left-stripped bit of code and non-stripped to count how many spaces or tabs there are...
			_depth = len( _code ) - len( _code.lstrip( ) )



	##
	## Callback - Called for each line for additional error-checking and for processing rules for class recognition, stack popping, etc...
	##
	def OnProcessLine( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		##
		## Inverted to get rid of duplicate return code... - Make sure there is code on this line... that it isn't a comment and we're not in a comment block...
		## Adding the not in front of the ( ... ) is identical to applying that negation manually - meaning it would become the following ( as we need to make sure none of those exist in order to run within ):
		##		if ( _Code.strip( ) != '' and not _in_comment and not _in_comment_block )
		##
		if not ( _code.strip( ) == '' or _in_comment or _in_comment_block ):
			##
			## Process potential syntax errors
			##
			self.ProcessSyntaxErrors( _line_number, _code, _depth, _in_comment, _in_comment_block )


			##
			## Process Depths, parents of objects, etc...
			##
			self.SyntaxRuleFunctionClassScopeDepthTracker( _line_number, _code, _depth, _in_comment, _in_comment_block )


		##
		## _in_string = self.LineContainsStringBlock( None );
		if ( not _in_comment and not self.IsInString( ) ):

			##
			## Check for tuple creation - since I use ;s at the end of a line.. if I find ), that will create it as a tuple..
			##
			## Task: Add checking to see if the user is in a list - if the user is in a list then make sure a , is used and not ;...
			##
			if ( not _code.strip( ).endswith( ');' ) and not _code.strip( ).endswith( '( ),' ) and not _code.strip( ).find( 'lambda' ) > -1 and not _code.strip( ).find( ', ' ) > -1 and not _code.strip( ).find( ',	' ) > -1 and ( _code.strip( ).endswith( '),' ) or ( _code.strip( ).rfind( '),' ) > -1 and _code.strip( ).rfind( '),' ) < _code.rfind( '#' ) ) ) ):
				self.AddWarning( 'Tuple Warning: Possible issue - a tuple may be created on this line... Please be sure this is what you want. Sometimes using ), at the end of a line is something you do not intend to become a tuple, but Python generates a tuple in these cases...', _line_number );


			##
			## Ternary Alternate Error Checking..
			##
			if ( ( ( _code.strip( ).find( 'if ' ) > -1 or _code.strip( ).find( 'if:' ) > -1 ) and ( _code.strip( ).find( 'else ' ) > -1 or _code.strip( ).find( 'else:' ) > -1 ) and _code.strip( ).endswith( ':' ) ) ):
				self.AddError( 'Ternary Alternate Error: "_x = TRUE_VALUE if ( Condition ) else FALSE_VALUE" - Is the correct usage.. Be sure if does not read: if:, be sure else does not have else: and be sure there is no : at the end of the line after FALSE_VALUE', _line_number );


		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		return super( ).OnProcessLine( _line_number, _code, _depth, _in_comment, _in_comment_block )


	##
	## Process Syntax Errors - Set in its own function to simplify default callbacks and to reinforce building-block style of coding I typically use...
	##
	def ProcessSyntaxErrors( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		## Create Helpers to determine whether or not we've detected a line containing a class or function definition...
		_bClass		= _code.startswith( 'class ' )
		_bFunc		= _code.startswith( 'def ' )
		_bIfElIf	= _code.startswith( 'if' ) or _code.startswith( 'elif' )
		_bElse		= _code.startswith( 'else' )
		_bFor		= _code.startswith( 'for' )
		_bTry		= _code.startswith( 'try' )
		_bWith		= _code.startswith( 'with' )
		_bExcept	= _code.startswith( 'except' )
		_bWhile		= _code.startswith( 'while' )


		##
		_in_string = self.LineContainsStringBlock( None );

		##
		## Check for syntax error - on CATEGORY_CLASS or CATEGORY_FUNCTION - if either doesn't end with : then report syntax error... - Note: This is done here instead of OnProcessLine because we have access to category and patterns, etc.. here and because we are looking for class and function definitions..
		##
		if ( not self.IsInComment( ) and not self.IsInString( ) ):
			if ( _bClass or _bFunc or _bIfElIf or _bElse or _bFor or _bTry or _bWith or _bExcept or _bWhile ):
				## Make sure the line is a valid code-based line and check to see if the line ends with a semi-colon..
				if ( _line_number != '' and not _code.endswith( ':' ) and not _code.find( ':' ) > 0 ):
					## If it is a class, asume the worst.. If it is a function, it may be a single-line definition so lets check for a return statement before jumping the gun.. Note: I used Category Original, because we modify functions to class in this, to accurately process this..
					## if ( ( _category_original != CATEGORY_FUNCTION and _category == CATEGORY_CLASS ) or ( _category_original == CATEGORY_FUNCTION and not AcecoolLib.String.IndexOfExists( ': return', _code, True ) and not AcecoolLib.String.IndexOfExists( ':return', _code, True ) and not AcecoolLib.String.IndexOfExists( ': pass', _code, True ) and not AcecoolLib.String.IndexOfExists( ':pass', _code, True ) and not AcecoolLib.String.IndexOfExists( ': pas s', _code, True ) and not AcecoolLib.String.IndexOfExists( ':pa ss', _code, True ) ) ):
					if ( _bClass or ( _bFunc and not AcecoolLib.String.IndexOfExists( 'return', _code, True ) and not AcecoolLib.String.IndexOfExists( 'pass', _code, True ) ) ):
						## Mark the error so the developer can fix it..
						self.AddError( 'Syntax Error: Missing Colon ":" at end of line for a ' + AcecoolLib.Logic.TernaryFunc( ( _bFunc ), 'Function', 'Class' ) + ' declaration!', _line_number );

						## Add an error tag to the code in question to make it noticeable as the error only describes the type and line number to keep it concise..
						_code = _code + ' [ ERROR ]'
			else:
				## Make sure the line doesn't end with a : because outside of the above cases, it shouldn't be there...
				if ( _line_number != '' and _code.endswith( ':' ) and ( not _code.startswith( '"' ) and not _code.startswith( '\'' ) ) ):
					## Mark the error so the developer can fix it..
					self.AddError( 'Possible Syntax Error: Colon ":" found at the end of a line which does not require it...', _line_number );

					## Add an error tag to the code in question to make it noticeable as the error only describes the type and line number to keep it concise..
					_code = _code + ' [ ERROR ]'


			##
			## Detect Syntax Error with if / elseif / else if / else statements...
			##
			if ( _bIfElIf ):
				## If ! is used instead of not, alert the user..
				## Task: Create self.AddSyntaxRule( SYNTAX_ERROR, IF_STATEMENTS, CONTAINS, "!", WHEN_NOT "!=" ) - ie extend search detection methods
				if ( AcecoolLib.String.IndexOfExists( '!', _code, True ) and not AcecoolLib.String.IndexOfExists( '!=', _code, True ) ):
					self.AddError( 'Syntax Error: Incorrect usage of "!". Python requires that "not" is used except when using "!=".. if ( not X ): is correct, if ( !X ): does not work!', _line_number );
					_code = _code + ' [ ERROR ]'

				## Detect && and || usage in if statements...
				## Task: Create self.AddSyntaxRule( SYNTAX_ERROR, IF_STATEMENTS, CONTAINS, "&&", "WHEN_NOT_IN_QUOTES, "||", "WHEN_NOT_IN_QUOTES ) - ie extend search detection methods
				if ( ( not AcecoolLib.String.IndexOfExists( '"&&"', _code, True ) and not AcecoolLib.String.IndexOfExists( '"||"', _code, True ) and not AcecoolLib.String.IndexOfExists( "'&&'", _code, True ) and not AcecoolLib.String.IndexOfExists( "'||'", _code, True ) ) and ( AcecoolLib.String.IndexOfExists( '&&', _code, True ) or AcecoolLib.String.IndexOfExists( '||', _code, True ) ) ):
					self.AddError( 'Syntax Error: Incorrect usage of "&&" or "||" - Python requires "and" or "or" as "&&" and "||" does not work!', _line_number );
					_code = _code + ' [ ERROR ]'



	##
	## Note: This has been added temporarily as a stand-alone function until it is converted into a Syntax Rule... or Rule Set... I will be renaming some of the helper functions to better fit what it is I'm actually doing... Expect that update soon.. Converting things, and coding, is easier when you design your code to be a series of building blocks..
	##
	def SyntaxRuleFunctionClassScopeDepthTracker( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		## Create Helpers to determine whether or not we've detected a line containing a class or function definition...
		_bClass = _code.startswith( 'class ' )
		_bFunc = _code.startswith( 'def ' )

		## If we haven't detected a line containing either, simply prevent the rest of the code from running...
		if ( not _bClass and not _bFunc ):
			return


		##
		## TODO: Add basic rule system so different languages can take advantage of this ( for languages that don't use a prefix - Lua uses function x.func( ) or x.func = function where x acts as the object, library of functions without object or cloning properties, meta-table, etc.. -- PHP for example could benefit from this output but it requires optional { }s to look for scope and it requires looking at more info so a simple rule system to plug into this system would be fantastic - depth isn't always defined by indentation so depth has to be defined other ways )
		## TODO: Create Depth system - as depth isn't defined by indent-level in all languages ( python being the exception ), depth needs to be defined using other methods... But depth doesn't always mean what we have it mean here so we need to target specifically what we want to trigger depth and add a new IndentLevel accessor-func for visual output and let Depth be used for logical / scope depth..
		##


		##
		## Local - Helper Function - Resets the Active Claass data-set / AccessorFuncs to their default values... Used for depth == 0 cases when code isn't a class...
		##
		def ResetActiveClass( self ):
			## Set the Class Depth Value to 0
			self.SetActiveClassDepth( 0 )

			## Set the Active Class Name to None - ie UNSET.. Logic looks for this, if None then don't categorize...
			self.SetActiveClass( None )

			## Reset the List to an empty list... Quickest way to unset the list...
			## Task: Look into Sublime text and their GC for Python. If these aren't properly collected that this can cause issues after days or weeks running non-stop. Unlikely as it may be, if it is an issue that can arise, I don't want it in my code... I prefer to release quality instead of hacks.
			self.SetClassStack( [ ] )


		##
		## Class Depth Handler...
		##
		if ( _bClass ):
			## Depth is 0 - we reset all values ( and then set active class as well as setting depth to 1 )
			if ( _depth == 0 ):
				## If depth is 0 we need to reset - since a class was found it'll update below...
				ResetActiveClass( self )
			elif ( _depth >= self.GetActiveClassDepth( ) ):
				## We're going deeper into the rabbit hole - make sure we leave breadcrumbs to find our way out...
				self.GetClassStack( ).append( self.GetActiveClass( ) )
			## Apparently not needed, but it may be... this should be called for severe nesting when there are a lot of classes on the same level or going up... need to find a code example to test this, or I'll remove it..
			else:
				## _depth is less than ActiveClassDepth without being 0... Because we're less than, we need to pop as many as levels we're going up... How many times do we pop?
				_difference = self.GetActiveClassDepth( ) - _depth

				## For every element we are supposed to pop
				for _i in range( _difference - 1 ):
					## Make sure we can pop - if we can't, we have an issue... else: print( ' >> INVALID LOGIC DETECTED!!!' ) - Add this if looking for broken logic - this shouldn't happen..
					if ( len( self.GetClassStack( ) ) > 0 ):
						self.SetActiveClass( self.GetClassStack( ).pop( ) )
					else:
						self.AddSimpleEntry( CATEGORY_XCM_ERROR, _line_number, 'Logic Error: Attempted to self.GetClassStack( ).pop( ) from an empty list! This should never happen! Code( "' + str( _code ) + '" )', 0 )
						break

			## Now that the old active class is added, if applicable, we need to update the current active class, and the active depth...
			## Added the Line Number - we use Pop, etc.. so line number will be handled properly because we use GetActiveClass to nest the entry under the parent class... Line number allows the category to act as a class header without needing to scroll up...
			self.SetActiveClass( _code + ':' + str( _line_number ) )
			self.SetActiveClassDepth( _depth + 1 )


		##
		## Function Depth Handler
		##
		if ( _bFunc ):
			## We've found a function at depth 0 - it is a global / local-to-file-scope function - Reset depth status..
			## We use our helper - note: update it to make sure entries are in - debugging technique so we can print when it is called and isn't needed... this is to track faulty logic...
			if ( _depth == 0 ):
				ResetActiveClass( self )
			else:
				## Otherwise - note: Depth should never be < 0... So we're deeper...
				if ( _depth < self.GetActiveClassDepth( ) ):
					if ( len( self.GetClassStack( ) ) > 0 ):
						## If the depth == active we need to set active depth to current depth ( as it is a function, and according to the class it'll be depth + 1 by perspective..
						self.SetActiveClass( self.GetClassStack( ).pop( ) )
						self.SetActiveClassDepth( _depth )


	##
	## Callback - Used to either prevent an entry from being added by returning True / False, to alter the code being added by returning a String, to look for Syntax or other errors and add an Error Entry or Warning Entry, etc..
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original ):
		##
		## Make sure commented out print statements are removed, if decided...
		##
		if ( _pattern == 'print(' ):
			if ( _code.strip( ).startswith( '#' ) ):
				## _code = '--' + _code
				return False

				## print( str( XCM_MAPPER_SETTINGS.get( 'DeveloperMode', False ) ) )


		## ##
		## ## System to alter the Category system -- I
		## ##
		## if ( _category == CATEGORY_CLASS ):
		## 	if ( _code.find( '(' ) <= 0 and _code.find( ':' ) <= 0 or _code.find( '..' ) > 0 ):
		## 		print( 'REMOVING CLASS FOR MISSING (: ' + _code )
		## 		return False


		##
		## Helpers
		##

		## Get the category type
		_category_type = self.GetCategoryType( _category );


		##
		## System to scan multiple lines to read the full function declaration if a function or object is declared over multiple lines...
		##
		if ( not _category == CATEGORY_FUNCTION_ACCESSOR and not self.InString( ) and not self.InStringBlock( ) and ( _category_type == CATEGORY_TYPE_FUNCTION or _category_type == CATEGORY_TYPE_CLASS or _category_type == CATEGORY_TYPE_CLASS_FUNCTION ) ):
			##
			_code_line = self.GetFileDataByLine( _line_number ).strip( );

			## If the line doesn't contain a : or end with : and doesn't have a ) then check the following lines as long as there are ,s until ): or x lines have been processed..
			if ( ( not _code.endswith( '):' ) and not _code.endswith( ':' ) and not _code.find( '):' ) > -1 ) and ( not _code_line.endswith( '):' ) and not _code_line.endswith( ':' ) and not _code_line.find( '):' ) > -1 ) ):
				##
				_error_finding_func_end = False;

				##
				for _i in range( 1, self.GetSetting( 'scan_lines_function_end_max', 10 ) ):
					_line = self.GetFileDataByLine( _line_number + _i ).strip( );

					## If a comma is found, then it
					## if ( _line.find( ',' ) > -1 ):

					##
					_code = _code + ' ' + _line;

					## If we found the function end, then break...
					if ( _line.endswith( '):' ) or _line.endswith( ':' ) or _line.find( '):' ) > -1 ):
						_error_finding_func_end = True;
						break;

				## if we didn't find the end and left the loop before finding the end, add an error...
				if ( _error_finding_func_end ):
					self.AddError( 'Function / Class Declaration Error: Can not find the end of a function / class declaration after scanning multiple lines... ', _line_number );


		##
		## System to alter the Category system -- Insert Function into Class Nested Category... OR prefix the parent-class to the function similar to Lua... Optionally include all the way to the base class ( useful tool to show that stack system works now )!
		##
		if ( _category == CATEGORY_FUNCTION ):
			## Grab our class category...
			_class_cat = self.GetActiveClass( )

			## If we want single line functions to be displayed then allow it - otherwise snip...
			if ( not self.GetSetting( 'output_single_line_functions_code', False ) ):
				## Find the : and ensure single line function definitions only return the first part...
				_index_colon = _code.find( ':' )

				## If we found it - snip... ie code = code up until : including : and all before...
				if ( _index_colon > 0 ):
					_code = _code[ : _index_colon + 1 ]

			## If the class has a category of its own - ie the class was set... move our function into it...
			if ( _class_cat != None ):
				if ( self.GetSetting( 'output_library_functions_in_new_category', False ) == False ):
					## Prevent Stack Overflow because we call AddSimpleEntry to allow editing further without pattern or mode...
					if ( _mode != None and _pattern != None ):
						## Set up helper var to track progress...
						_output = _code

						## Remove def prefix...
						_output = _output.replace( 'def ', '' )

						## Clean up the class-code so we're only left with the name... ( Maybe I'll add into the rule system a system to ensure only the name is inserted because re-adding the other data is easy ( although extensions can be useful in some cases )
						_class_cat = self.StripAllExceptClassName( _class_cat )

						## This is the var which tracks out progress when we are to build it ( when stack exists > 0 )
						_class_builder = ''

						## Should we build.the.full.depth.to.function( ) or should we simply show to.function( )? - The value needs to be set to true, and the stack has to have data for this to execute - otherwise only the direct-parent is used...
						if ( self.GetSetting( 'output_class_functions_in_new_category', False ) == False and len( self.GetClassStack( ) ) > 0 ):
							## _class_build = ''
							for _i in range( len( self.GetClassStack( ) ) ):
								## Grab the stack value and clean the code so we're only left with the Class Name. We're moving from shallowest to deepest, we don't need to reverse the data or anything special..
								_value = self.StripAllExceptClassName( self.GetClassStack( )[ _i ] )

								## We're moving from SHALLOW >> to >> DEEP so we don't need to do anything special... Simply <String> + <NextClass> + .
								_class_builder = _class_builder + _value + '.'

						## Finalized output: def Class.FuncName( args ) - To keep things short, only the top-most class is output.. Later I may add support to go deeper or all the way...
						_output = 'def ' + _class_builder + _class_cat + '.' + _output

						## AddSimpleEntry to ensure the callbacks are called on the new monster we've build ( if full-length is set )...
						self.AddSimpleEntry( CATEGORY_CLASS_FUNCTION, _line_number, _output, _depth )

						return False
				else:
					## Our entry is redirected from CATEGORY_FUNCTION to CATEGORY_CLASS_FUNCTION and nested into _class_cat or ActiveClass...
					self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, _code, _depth, _mode, _pattern, _class_cat )

					## Prevent the function from being added to the standard functions table ( although it can be and it can be nested there - although a separate primary category is sometimes cleaner )
					return False


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )


	##
	## Callback - Determines the indentation level to use for entries - Categories are at -1 ( not possible to use ), 0 is indented once, nested categories indent further. Default is 4 spaces per level, can be configured to use any character combination.
	##
	def OnCalcDepth( self, _category, _line_number, _code, _depth, _mode, _pattern ):
		## Helper...
		_type = self.GetCategoryType( _category )

		## Updated to use category types so child / nested functions should add the depth properly because right now it adds in the back-end but it returns 0 below so it ends up being the same depth as the nested category...
		if ( _type == CATEGORY_TYPE_CLASS or _type == CATEGORY_TYPE_CLASS_FUNCTION or _type == CATEGORY_TYPE_FUNCTION ):
			return _depth

		## Return 0 for everything else..
		return 0


	##
	## Configuration - Converts Class / Function Arguments based on config ( AddArgDataTypeEntry( FUNC_OR_CLASS, 'DATA_TYPE', 'str', 'blah' ) would appear as function Name( <DATA_TYPE> str, <DATA_TYPE> blah ),	function Name( <DATA_TYPE> <DATA_TYPE> ), or function Name( str, blah )
	##
	def OnSetupArgDataType( self ):
		## Adds a replacement for 'str' argument in CLASS categories - Functions will not receive this modification... This is to show that class TestingString( str ): will be modified properly..
		self.AddArgDataTypeEntry( CATEGORY_TYPE_CLASS, 'Extends Default String Object', 'str' )

		## Adds a replacement for 'self' argument in FUNCTION categories - Classes will not receive this modification... This is to show that all functions with self as an argument will be properly modified!
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, 'This', 'self' )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupArgDataType( )


	##
	## Python Mapper Helper Functions / Custom Functions... These are usable in ALL classes which except XCodeMapper_Python!
	##


	##
	## Helper - specifically for Python mapper...
	##
	def StripAllExceptClassName( self, _code ):
		## Remove class prefix...
		_code = _code.replace( 'class ', '' )

		## If we've found (, then ) likely exists - strip it out... all the way through : - otherwise we only need to remove :
		if ( AcecoolLib.String.IndexOfExists( '(', _code, True ) ):
			## If ( is included ( for a class extending another ) Then remove ( all the way to ): - otherwise only remove :
			_code = AcecoolLib.String.Strip( _code, AcecoolLib.String.IndexOf( '(', _code ) - 1, AcecoolLib.String.IndexOf( '):', _code ) )
		else:
			## Remove the :
			_code = _code.replace( ':', '' )

		## Returned the freshly cleaned Class Entry / Code
		return _code




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		## self.AddSyntax( CATEGORY_COMMENT_BLOCK_BEGIN, SEARCH_TYPE_STARTSWITH, '"""' )
		## self.AddSyntax( CATEGORY_COMMENT_BLOCK_FINISH, SEARCH_TYPE_STARTSWITH, '"""' )



		## self.AddSyntax( CATEGORY_OPTIMIZATION, SEARCH_TYPE_STARTSWITH, 'print(' )



		##
		## Add common typos to look for along with the approximate code to look for and what it should look like...
		##
		## self.AddTypoFixEntry( 'startswidth', '_haystack.startswidth( _needle )', '_haystack.startswith( _needle )' )
		## self.AddTypoFixEntry( 'startwidth', '_haystack.startwidth( _needle )', '_haystack.startswith( _needle )' )
		## self.AddTypoFixEntry( 'startwith', '_haystack.startwith( _needle )', '_haystack.startswith( _needle )' )
		## self.AddTypoFixEntry( 'endswidth', '_haystack.endswidth( _needle )', '_haystack.endswith( _needle )' )
		## self.AddTypoFixEntry( 'endwidth', '_haystack.endwidth( _needle )', '_haystack.endswith( _needle )' )
		## self.AddTypoFixEntry( 'endwith', '_haystack.endwith( _needle )', '_haystack.endswith( _needle )' )









		## For functions and objects / classes.. allow depth to be based on indentation - this is primarily for Python until I add the nested categories system...
		## Note: I've left functions and categories to use depth so they'll show you their level of indentation ( useful to know in Python ) while the rest is at 0 so you don't end up with a ton of tabs for other bits of info... If you don't like the depths being shown all over the place for categories and normal functions simply comment it out and the return 0 will be used...
		## if ( _category == CATEGORY_FUNCTION or _category == CATEGORY_CLASS ):

		## For the new nested category system - Because I modify the depth elsewhere, I subtract it here in order to re-add depth... Modding depth elsewhere is necessary in order to make the output readable because of category nesting - I subtract it here because the depth is being used instead of 0..
		## Note: Commented out for now so all nested line up... Now that we don't put classes and functions in the same category ( they're category and nested entries meaning they can be sorted without the headers being moved ) the depth-mod already modifies the output depth so 0 is category_depth + 1
		## if ( _category == CATEGORY_CLASS_FUNCTION ):
		##	return _depth - 1







		## pass
		## print( ' >> Symbols: ' + str( _symbols ) )
		## Symbols also don't give their function prefix...
		## if ( _code.startswith( 'def' ):

			## Note - Symbols use regions meaning the starting char of from the entire code blob to the end... not line numbering so this has to be calculated...
			## Since other info is also lost we can really only guess at the data unless we check the file and look at other infomration...
			## self.AddEntry( CATEGORY_FUNCTION, _line_number, _code, _depth, _mode, _pattern, _class_cat )


			##
			## Task: Remove the default system of grabbing functions, etc.. for Python and use Sublime Symbols for 100% of functions and classes... Make the SyntaxRuleFunction for Class ScopeDepthTracker to work with symbols... shouldn't be as difficult but instead of looking at each line we're only looking at each entry - but since they're in order it should be just as straight forward..
			##

			##
			## Process Depths, parents of objects, etc...
			##
			## self.SyntaxRuleFunctionClassScopeDepthTracker( _line_number, _code, _depth, False, False )

			##
			## if ( _scope_name == 'source.python meta.class.python storage.type.class.python' ):
			## 	## self.AddSimpleEntry( CATEGORY_CLASS, _line_number, _line.strip( ), 0 )
			## 	self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ), 0 )
			## elif ( _scope_name == 'source.python meta.class.python' ):
			## 	## self.AddSimpleEntry( CATEGORY_OBJECT, _line_number, _line.strip( ), 0 )
			## 	self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ), 0 )
			## elif ( _scope_name == 'source.python meta.function.python' ):
			## 	## self.AddSimpleEntry( CATEGORY_CLASS_FUNCTION, _line_number, _line.strip( ), 0 )
			## 	self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ), 0 )
			## elif ( _scope_name == 'source.python meta.function.python storage.type.function.python' ):
			## 	## self.AddSimpleEntry( CATEGORY_FUNCTION, _line_number, _line.strip( ), 0 )
			## 	self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ), 0 )
			## else:
			## 	self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ) + ' - Scope: ' + _scope_name + ' | as: ' + _code, 0 )
		##
		## Gutter Icon System
		##
		## if ( _category == CATEGORY_FUNCTION ):
			## import sublime
			## import sublime_plugin


			## class SetMarkCommand(sublime_plugin.TextCommand):
			##		def run(self, edit):
			##			mark = [s for s in self.view.sel()]
			##			self.view.add_regions("mark", mark, "mark", "dot", sublime.HIDDEN | sublime.PERSISTENT)


			## class SwapWithMarkCommand(sublime_plugin.TextCommand):
			##		def run(self, edit):
			##			old_mark = self.view.get_regions("mark")

			##			mark = [s for s in self.view.sel()]
			##			self.view.add_regions("mark", mark, "mark", "dot", sublime.HIDDEN | sublime.PERSISTENT)

			##			if len(old_mark):
			##				self.view.sel().clear()
			##				for r in old_mark:
			##					self.view.sel().add(r)


			## class SelectToMarkCommand(sublime_plugin.TextCommand):
			##		def run(self, edit):
			##			mark = self.view.get_regions("mark")

			##			num = min(len(mark), len(self.view.sel()))

			##			regions = []
			##			for i in range(num):
			##				regions.append(self.view.sel()[i].cover(mark[i]))

			##			for i in range(num, len(self.view.sel())):
			##				regions.append(self.view.sel()[i])

			##			self.view.sel().clear()
			##			for r in regions:
			##				self.view.sel().add(r)


			## class DeleteToMark(sublime_plugin.TextCommand):
			##		def run(self, edit):
			##			self.view.run_command("select_to_mark")
			##			self.view.run_command("add_to_kill_ring", {"forward": False})
			##			self.view.run_command("left_delete")

			## self.GetSublimeActiveView( ).add_regions(key, [regions], scope, <icon>, <flags>)
			## self.GetSublimeActiveView( ).add_regions( "gutter", "gutter", "function", "../User/CodeMap/Assets/Icons/char_function_48x48", sublime.PERSISTENT )
			## self.GetSublimeActiveView( ).add_regions("mark", mark, "mark", "dot", sublime.HIDDEN | sublime.PERSISTENT)













		## No need running this code / output when not actively developing or working on it..
		## if ( True ):
		## 	return

		## print( '***** <<<<< >>>>> *****' )
		## print( '***** <<<<< >>>>> *****' )

		## Load the mapper configuration - dynamically would be to read the loaded file and get the extension then use ext_config.sublime-settings...
		## XCM_MAPPER_SETTINGS = sublime.load_settings( 'Acecool\'s_XCodeMapper.sublime-settings' )

		## For some reason the file isn't detected... This hould return True, True, "Python", "Alphabetically" - Instead the defaults are being returned and this is a problem!!!
		## print( str( XCM_MAPPER_SETTINGS.get( 'DeveloperMode', False ) ) )
		## print( str( XCM_MAPPER_SETTINGS.get( 'DebugMode', False ) ) )
		## print( str( XCM_MAPPER_SETTINGS.get( 'LanguageName', False ) ) )
		## print( str( XCM_MAPPER_SETTINGS.get( 'OutputSortMethod', 'A-Z' ) ) )

		## _lang = 'ASP'
		## _key = 'language_name'
		## _default = 'XXX'

		## _a = self.GetPluginObject( ).GetSetting( 'language_name' )
		## print( str( _a ) )

		## _a = self.GetPluginObject( ).GetSetting( 'language_xxx' )
		## print( str( _a ) )

		## _a = self.GetPluginObject( ).GetLanguageSettingEx( _lang, _key, self.GetPluginObject( ).GetLanguageSettings( _lang ).get( _key, _default ) )
		## print( str( _a ) )

		## _a = self.GetPluginObject( ).GetLanguageSettingEx( _lang, _key, None )
		## print( str( _a ) )

		## _a = self.GetPluginObject( ).GetLanguageSettings( _lang ).get( _key, _default )
		## print( str( _a ) )

		## print( '***** <<<<< >>>>> *****' )
		## print( '***** <<<<< >>>>> *****' )