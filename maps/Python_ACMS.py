##
## Python - Acecool Code Mapping System Mapper - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent - ie Python_Default ... We could also use Python_User to give an option to use Python_User or Default as it follows the inclusion chain..
## from AcecoolCodeMappingSystem.maps.Python import *
from AcecoolCodeMappingSystem.maps.Python_SublimeText import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin XCodeMapper_Python_Project_AC_XCM
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##	Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
class ACMS_Python_ACMS( ACMS_Python_SublimeText ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_Python_ACMS'


	##
	## Callback - Used to setup rules to track code, expand AccessorFuncs, etc.. at to display the output in the Code - Map Panel. Priority is handled using standard English Reading direction ie using self.AddSyntax( COMMENT, STARTSWITH, '#', '##' ) means ## would never be handled.
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' )

		## When we're adding manual entries...
		self.AddSyntax( CATEGORY_XCM_OUTPUT_ENTRY, SEARCH_TYPE_STARTSWITH, 'self.AddEntry', 'self.AddSimpleEntry', 'self.AddWarning', 'self.AddError', 'self.AddFeature', 'self.AddHelp' )
		self.AddSyntax( CATEGORY_XCM_SYNTAX_ENTRY, SEARCH_TYPE_STARTSWITH, 'self.AddSyntax' )
		self.AddSyntax( CATEGORY_XCM_SYNTAX_RULE_ENTRY, SEARCH_TYPE_STARTSWITH, 'self.AddSyntaxRule' )

		## Accessor Funcs - These are expanded...
		self.AddSyntax( CATEGORY_FUNCTION_ACCESSOR, SEARCH_TYPE_STARTSWITH, 'Acecool.util.AccessorFunc' )
		self.AddSyntax( CATEGORY_FUNCTION_ACCESSOR, SEARCH_TYPE_CONTAINS, 'AcecoolLib.AccessorFuncBase(' )
		## self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_REGEX, '__[A-Z_a-z0-9]\s+=\s+AcecoolLib.AccessorFuncBase' )

		self.AddSyntax( CATEGORY_INFO, SEARCH_TYPE_CONTAINS, '.Notify(' )
		self.AddSyntax( CATEGORY_INFO, SEARCH_TYPE_CONTAINS, '.GetSetting(' )
		self.AddSyntax( CATEGORY_OPTIMIZATION, SEARCH_TYPE_STARTSWITH, 'print(', 'self.print(' )

		## Make sure I don't ship with these...
		self.AddSyntax( CATEGORY_WARNING, SEARCH_TYPE_CONTAINS, 'Acecool.__example__( )' )


		##
		## Mapper Features
		##
		self.AddFeature( 'Detection for configuration, enumeration, constant and all upper-case variables as definitions into config, enum, and constant variable categories..' )
		self.AddFeature( 'AccessorFunc Expanders for: Acecool.util.AccessorFunc' )


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )


	##
	## Callback - Used to convert found AccessorFunc Calls into Multiple Entries in XCodeMapper so the Getter / Setter and other functions created by the AccessorFunc call are added to the Map rather than the call.
	##
	def OnAccessorFuncExpansion( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Base Args - _tab will be the first arg in all cases...
		_arg_tab = AcecoolLib.Table.GetValue( _args, _args_count, 0, '<_arg_tab>' )

		## This is the first arg without any decimals - and we remove everything before last decimal in TitleCase - ClassName ie Timekeeper or Data...
		_arg_class = AcecoolLib.String.SubStr( _arg_tab, 10, len( _arg_tab ) ).title( )


		##
		## Creates several very useful functions...
		##
		## <Class>.Get<Name>( _default, _skip_defaults ), <Class>.Get<Name>ToString( _default ), <Class>.Get<Name>Len( _default )
		## <Class>.Set<Name>( _value ), <Class>.Reset<Name>( _default ), <Class>.Is<Name>Set( _true_on_default )
		##
		if ( _pattern == 'Acecool.util.AccessorFunc' ):
			##
			_arg_name = AcecoolLib.Table.GetValue( _args, _args_count, 1, 'NAME_NONE' )
			_arg_default = AcecoolLib.Table.GetValue( _args, _args_count, 2, 'DEFAULT_NONE' )

			_name_get		= _arg_tab + '.Get' + _arg_name + '( _default, _skip_defaults )'
			_name_str		= _arg_tab + '.Get' + _arg_name + 'ToString( _default )'
			_name_len		= _arg_tab + '.Get' + _arg_name + 'Len( _default )'
			_name_lenstr	= _arg_tab + '.Get' + _arg_name + 'LenToString( _default )'
			_name_set		= _arg_tab + '.Set' + _arg_name + '( _value )'
			_name_reset		= _arg_tab + '.Reset' + _arg_name + '( _default )'
			_name_isset		= _arg_tab + '.Is' + _arg_name + 'Set( _true_on_default )'

			## Add the Get<Name> and Set<Name> functions to CATEGORY_FUNCTION category - note, you can use CATEGORY_FUNCTION_ACCESSOR if you want it more out of the way...
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'def ' + _name_get, _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'def ' + _name_str, _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'def ' + _name_len, _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'def ' + _name_lenstr, _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'def ' + _name_set, _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'def ' + _name_reset, _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'def ' + _name_isset, _depth, _mode, _pattern )

			return True


		##
		##

		if ( _pattern == 'AcecoolLib.AccessorFuncBase(' ):
			## print( 'ACMS.FoundAccessor -> ' + _code )
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, _code.expandtabs( self.GetSetting( 'file_tab_size', 4 ) ), _depth, _mode, _pattern )

			return True


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnAccessorFuncExpansion( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )		## self.CallParent( 'OnAccessorFuncExpansion', _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End XCodeMapper_Python_Project_AC_XCM
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------





		## XCodeMapper Features we'd like to see.... - Category Order Entries for output order... Syntax, and more...
		## self.AddSyntax( CATEGORY_INFO, SEARCH_TYPE_STARTSWITH, 'self.AddCategoryOrderEntry' )
		## self.AddSyntax( CATEGORY_XCM_OUTPUT_ORDER_ENTRY, SEARCH_TYPE_STARTSWITH, 'self.AddCategoryOrderEntry' )

			## self.AddSyntax( CATEGORY_CONSTANT, SEARCH_TYPE_REGEX, '^[A-Z_]+\s+\=' )

		## self.CallParent( 'OnSetupSyntax' )

		## self.AddSyntax( CATEGORY_OPTIMIZATION, SEARCH_TYPE_CONTAINS, '.print(' )


			## _name_get		= 'self.Get' + _name_prefix + _name + '( _default, _skip_defaults )'
			## _name_str		= 'self.Get' + _name_prefix + _name + 'ToString( _default )'
			## _name_len		= 'self.Get' + _name_prefix + _name + 'Len( _default )'
			## _name_lenstr		= 'self.Get' + _name_prefix + _name + 'LenToString( _default )'
			## _name_set		= 'self.Set' + _name_prefix + _name + '( _value )'
			## _name_reset		= 'self.Reset' + _name_prefix + _name + '( _default )'
			## _name_isset		= 'self.Is' + _name_prefix + _name + 'Set( _true_on_default )'




		## self.AddFeature( '' )
		## self.AddFeature( '' )
		## self.AddFeature( '' )
		## self.AddFeature( '' )