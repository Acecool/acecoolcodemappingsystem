##
## Sublime tmLanguage - Default / Vanilla / Base Mapper - Josh 'Acecool' Moser
##


##
## Note: All Sublime* Files, at this time, use the JSON language - these User files only exist as placeholders for when I add special language mappings designed to look for special rules in the files and bring them to the attention of the user.
## 		 In order to unlock using these files, by default, you will need to change the ACMS_Definitions file to change the language mapping table active_language from JSON to Sublime*
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent ACMS Mapper - As this is the User mapper, I've set up all possible includes for this mapper out!
from AcecoolCodeMappingSystem.maps.JSON import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Everything you need to create your own - The base class which very limited structure, the CFG_ options above and the ENUM value to use to set to it... Simply rename them...
##
## class ACMS_SublimeLanguage_Default( XCodeMapperBase ):
class ACMS_SublimeLanguage_Default( ACMS_JSON_Default ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_SublimeLanguage_Default'




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------