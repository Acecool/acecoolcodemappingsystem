##
## MarkDown - Default / Vanilla / Base Mapper - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent ACMS Mapper - As this is the User mapper, I've set up all possible includes for this mapper out!
from AcecoolCodeMappingSystem.maps.__mapper_imports__ import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##  Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
class ACMS_MarkDown_Default( XCodeMapperBase ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_MarkDown_Default'


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' )

		## Comments - Note: Only block-comment exists in MarkDown...
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_BEGIN, SEARCH_TYPE_STARTSWITH, '<!--' )
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_FINISH, SEARCH_TYPE_STARTSWITH, '-->' )

		## Image / Link
		self.AddSyntax( CATEGORY_FORMATTING_IMAGE, SEARCH_TYPE_REGEX, '\!\[(.+)\]\((.+)\)' )
		self.AddSyntax( CATEGORY_FORMATTING_LINK, SEARCH_TYPE_REGEX, '\[(.+)\]\((.+)\)' )

		## Code and Quote Blocks / Snippets
		self.AddSyntax( CATEGORY_FORMATTING_QUOTE, SEARCH_TYPE_STARTSWITH, '> ' )
		self.AddSyntax( CATEGORY_FORMATTING_CODE, SEARCH_TYPE_STARTSWITH, '```', '~~~' )
		self.AddSyntax( CATEGORY_FORMATTING_CODE_SNIPPET, SEARCH_TYPE_STARTSWITH, '`' )

		## Lists
		self.AddSyntax( CATEGORY_FORMATTING_LIST_BUTTONED, SEARCH_TYPE_STARTSWITH, '- ', '* ', '+ ' )
		self.AddSyntax( CATEGORY_FORMATTING_LIST_DEFINITION, SEARCH_TYPE_STARTSWITH, ': ' )
		self.AddSyntax( CATEGORY_FORMATTING_LIST_NUMBERED, SEARCH_TYPE_REGEX, '^\d+\.' )

		## Headers - If you want all headers to be in a single-category named 'Headers H1 - H6', simply comment the 6 lines directly below this comment.. '#' has already been added to the tag below..
		self.AddSyntax( CATEGORY_FORMATTING_HEADER_6, SEARCH_TYPE_STARTSWITH, '######' )
		self.AddSyntax( CATEGORY_FORMATTING_HEADER_5, SEARCH_TYPE_STARTSWITH, '#####' )
		self.AddSyntax( CATEGORY_FORMATTING_HEADER_4, SEARCH_TYPE_STARTSWITH, '####' )
		self.AddSyntax( CATEGORY_FORMATTING_HEADER_3, SEARCH_TYPE_STARTSWITH, '###' )
		self.AddSyntax( CATEGORY_FORMATTING_HEADER_2, SEARCH_TYPE_STARTSWITH, '##' )
		self.AddSyntax( CATEGORY_FORMATTING_HEADER_1, SEARCH_TYPE_STARTSWITH, '#' )

		## Headers - H1 = '======', H2 = '------' underline so line - 1 needs to be used... ( and I hope to read the data )...
		self.AddSyntax( CATEGORY_FORMATTING_HEADER, SEARCH_TYPE_STARTSWITH, '======', '------', '#' )

		##
		self.AddSyntax( CATEGORY_FORMATTING_RULE_HORIZONTAL, SEARCH_TYPE_STARTSWITH, '***', '---', '___' )

		## Bold, Italics, Strikethrough - These RegEx checks look for **<text>** or __<text>__ for bold, *<text>* or _<text>_ for italics, and ~~<text>~~ for Strike-Through
		self.AddSyntax( CATEGORY_FORMATTING_BOLD, SEARCH_TYPE_REGEX, '^(\*\*|__)(.+)(\*\*|__)' )
		self.AddSyntax( CATEGORY_FORMATTING_ITALICS, SEARCH_TYPE_REGEX, '^(\*|_)(.+)(\*|_)' )
		self.AddSyntax( CATEGORY_FORMATTING_STRIKETHROUGH, SEARCH_TYPE_REGEX, '^(~~)(.+)(~~)' )

		## Organization...
		self.AddSyntax( CATEGORY_FORMATTING_TABLE, SEARCH_TYPE_REGEX, '\|(.+)\|' )
		self.AddSyntax( CATEGORY_FORMATTING_FOOTNOTE, SEARCH_TYPE_STARTSWITH, '/(\[\d+\]: )/ig' )
		self.AddSyntax( CATEGORY_FORMATTING_LINE_BLOCK, SEARCH_TYPE_STARTSWITH, '\|' )


		## Tags - I could use RegEx to match tags, but some can extend further than 1 line and I haven't added the feature for multiple-line scanning yet...
		self.AddSyntax( CATEGORY_FORMATTING_TAG, SEARCH_TYPE_STARTSWITH, '<' )

		## ## Other
		self.AddSyntax( CATEGORY_FORMATTING_EXAMPLE, SEARCH_TYPE_STARTSWITH, 'Lorem ipsum' )


		## self.AddSyntax( CATEGORY_FORMATTING_CODE, SEARCH_TYPE_STARTSWITH, '~~~' )
		## self.AddSyntax( CATEGORY_FORMATTING_LIST_BUTTONED, SEARCH_TYPE_STARTSWITH, '* ' )
		## self.AddSyntax( CATEGORY_FORMATTING_LIST_BUTTONED, SEARCH_TYPE_STARTSWITH, '+ ' )
		## self.AddSyntax( CATEGORY_FORMATTING_RULE_HORIZONTAL, SEARCH_TYPE_STARTSWITH, '---' )
		## self.AddSyntax( CATEGORY_FORMATTING_RULE_HORIZONTAL, SEARCH_TYPE_STARTSWITH, '___' )
		## self.AddSyntax( CATEGORY_FORMATTING_HEADER, SEARCH_TYPE_STARTSWITH, '------' )


		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )


	##
	## Callback - these are the symbols from the file we're currently editings symbols( ) from the view - ie all extracted symbols from Sublime Text...
	## In Short: If working this should give us all of the information from the Command Palette when we look at @, :, etc.. for function names and everything... A potentially almost perfect way to create a universal mapper...
	##
	## Task: Allow symbols to be processed through the AddSyntax processor ( Make a syntax processor function outside of Run to do this ) - Then the full-line of code can be grabbed from the symbols SublimeText provides us and we'll have a basic universal mapping system....
	##
	def OnMappingFileSymbols( self, _view, _symbols ):
		## pass
		print( ' >> Symbols: ' + str( _symbols ) )
		## Symbols also don't give their function prefix...
		## if ( _code.startswith( 'def' ):

		## Symbols appear in a list, in a tuple with a region and the symbol...[((515, 525), 'PANEL:Init'), ((891, 910), 'PANEL:ControlValues'), ((1159, 1178), 'PANEL:PerformLayout')]

		## So for each entry
		for _entry in _symbols:
			## Tuples and lists use numerical indices
			## Grab the region object ( chars from start of file to X, and Total Chars in Region + X )
			_region = _entry[ 0 ]

			## Grab the partial code we're given - we can grab the full line using a helper function...
			_code = _entry[ 1 ]

			## Grab the line number using another helper...
			_line_number = self.GetSublimeViewLineNumberByRegion( _region )

			## Grab the line code by another helper...
			_line = self.GetFileDataByLine( _line_number )

			## Note - Symbols use regions meaning the starting char of from the entire code blob to the end... not line numbering so this has to be calculated...
			## Since other info is also lost we can really only guess at the data unless we check the file and look at other infomration...
			## self.AddEntry( CATEGORY_FUNCTION, _line_number, _code, _depth, _mode, _pattern, _class_cat )
			self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ), 0 )


	##
	## Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original ):
		## If we see a horizontal rule, but the line above it isn't blank then it isn't a horizontal rule - it is a header option - basically makes the above text huge..
		if ( _category == CATEGORY_FORMATTING_RULE_HORIZONTAL and _line_number > 0 ):
			## Grab the data from the line above..
			_data = self.GetFileDataByLine( _line_number - 1 )

			## If there is data, then we modify the category...
			if ( _data.strip( ) != '' ):
				## Add the line above the current line as our entry instead of adding the underlined portion...
				self.AddSimpleEntry( CATEGORY_FORMATTING_HEADER, _line_number - 1, _data, _depth )

				## Prevent this entry from being added
				return False


		## Don't run in the definitions category so we can see what they are in raw form...
		if ( _category == CATEGORY_FORMATTING_HEADER ):
			## H1 using ====== or H2 using ------ meaning we need to get the line ABOVE the underline...
			if ( _pattern == '======' or _pattern == '------' ):
				## Grab the data from line - 1
				_data = self.GetFileDataByLine( _line_number - 1 )

				## Add the line above the current line as our entry instead of adding the underlined portion...
				self.AddSimpleEntry( _category, _line_number - 1, _data, _depth )

				## Don't add the equals or hyphens... Return True or False to skip adding this line
				return False


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------