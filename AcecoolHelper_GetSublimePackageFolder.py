##
## Helper File - Gets the package name from this file - Josh 'Acecool' Moser
##
## Use:
##	Place in your package
##	Import it to your plugin file
##	Create Plugin Object Functions Linking to these so it is available throughout your entire plugin
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Important Declarations
##

##
global THIS_PACKAGE_FILE, THIS_PACKAGE_NAME
THIS_PACKAGE_FILE = __file__
THIS_PACKAGE_NAME = ''


##
## Helper - Returns the filename...
##
def GetThisFileName( ):
	global THIS_PACKAGE_FILE
	return THIS_PACKAGE_FILE


##
## Helper - Returns the Package this file is in...
##
def GetThisPackageName( ):
	global THIS_PACKAGE_FILE, THIS_PACKAGE_NAME

	if ( THIS_PACKAGE_NAME == '' ):
		## Grab the path into an easier to use
		_path = THIS_PACKAGE_FILE

		## Grab the index of Packages, then grab all of the text from the path from Packages/* to the end.. Then split by all directory slashes, and return the FIRST one.. This will be the Package Folder ( unless User/ )
		_path = _path[ _path.find( 'Packages' ) + 9 : ].split( '\\' )

		## Grab the first directory which makes sense to be the package name - ie if not User or Default then it is 0, otherwise it is the one after User or Default...
		if ( _path[ 0 ] == 'User' or _path[ 0 ] == 'Default' or _path[ 0 ] == 'Packages' ):
			THIS_PACKAGE_NAME = _path[ 1 ]
		else:
			THIS_PACKAGE_NAME = _path[ 0 ]

	## Return the package name..
	return THIS_PACKAGE_NAME