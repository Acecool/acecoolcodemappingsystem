##
## JavaScript - AcecoolWeb_Framework Mapper - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent ACMS Mapper - As this is the User mapper, I've set up all possible includes for this mapper out!
from AcecoolCodeMappingSystem.maps.JavaScript import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin JavaScript using additional Acecool Mappings for Ultimate Site Modding Script, and also AcecoolST3_Library - Maybe turn that into it's own lib...
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##
##
class ACMS_JavaScript_Acecool( ACMS_JavaScript_Default ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_JavaScript_Acecool'


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' )

		## Accessor Funcs - These are expanded, typically by adding Getter and Setter functions
		## The following 3 are for - https://bitbucket.org/Acecool/acecooldev_userscripts/src/master/__everything_one_script_to_run_everything_through_modules_and_rules__/AcecoolDev_Ultimate_UserScript.user.js?at=master&fileviewer=file-view-default
		## I use the AccessorFunc callback to ensure _tag is replaced with the appropriate previously memorized value etc..
		## These two instead of adding functions, adds a comment for each.. The content of the comments are based on the arguments to these functions...
		self.AddSyntax( CATEGORY_FUNCTION_ACCESSOR, SEARCH_TYPE_STARTSWITH, 'Acecool.util.AccessorFunc', 'AccessorFunc', 'AddFeature', 'SetupTasks', 'SetupProcesses' )


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )		## self.CallParent( 'OnSetupSyntax' )


	##
	## When an AccessorFunc type is found, this is the callback executed which allows you to expand the AccessorFunc into multiple entries...
	##
	## example: When encountering a GMod Lua / GLua util function, it is expanded into 2 entries representative of the 2 functions AccessorFunc creates..
	##
	## Code:
	## 	AccessorFunc( perp.lighting, "somevar", "LightsEnabled", FORCE_BOOL )
	##
	## XCodeMapper Entries - I set it to add into CATEGORY_FUNCTION_ACCESSOR categories:
	##	function perp.lighting:GetLightsEnabled( );:102
	##	function perp.lighting:SetLightsEnabled( _value :: Note: _value forced tobool( ) on set! );:102
	##
	##
	## Because each AccessorFunc creation method is different for each function we look for... they're all routed through here to be expanded upon...
	## If pattern x then args[ 1 ] = classname, args[ 2 ] = name ( so S/Get<Name> etc.. ), and more... so if args[ n ] is "" then we don't add Get prefix to the Getter.. and so on..
	## Also, we can force the data to show up in different categories.. for example __ functions can be put into CATEGORY_FUNCTION_INTERNAL which appears last in list of functions..
	## or we can determine whether or not x function was already added which has an optional creation function ( ie on first time call of an accessor func, it
	##  creates __GetDataTable, __GetData, __SetData internal function but the next time we call it on the same class, it shouldn't output these ).. etc...
	##
	## The benefit of this function is that you can do ANYTHING you want with the data of AccessorFuncs ( I may add callbacks for each category type or search later on, but right now AccessorFunc is what needs it )
	##
	def OnAccessorFuncExpansion( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Base Args - _tab will be the first arg in all cases...
		_arg_tab = AcecoolLib.Table.GetValue( _args, _args_count, 0, self.__name__ )

		## This is the first arg without any decimals - and we remove everything before last decimal in TitleCase - ClassName ie Timekeeper or Data...
		_arg_class = AcecoolLib.String.SubStr( _arg_tab, 10, len( _arg_tab ) ).title( )


		##
		## Acecool.util.AccessorFunc = function( _tab, _name, _default, _getter_prefix = 'Get', _key = _name )
		##
		## Make sure the code isn't the definition - it must be a call
		## TODO: Create a rule system to detect function calls VS function definitions and add it to the arguments of all...
		## TODO: Create an object used for the return instead of a lengthy arguments list which can be altered...
		if ( _pattern == 'AccessorFunc' or _pattern == 'Acecool.util.AccessorFunc' and not _code.startswith( 'function' ) and not _code.startswith( 'Acecool.util.AccessorFunc = function' ) ):## IF _arg_tab is self, then assume XCodeMapper
			if ( _arg_tab == 'self' ):
				_arg_tab = self.__name__

			## Function name so if this is Blah then SetBlah is one function and _getter_prefix + Blah is the second.
			_arg_name	= AcecoolLib.Table.GetValue( _args, _args_count, 1, 'NAME_NONE' )

			## Default value for the Getter - however it can be overwritten by using GetBlah( xxx )
			_arg_default = AcecoolLib.Table.GetValue( _args, _args_count, 2, 'null' )

			## Getter Function Prefix - Get is the default value for the function definition so we use Get here too as the default...
			_arg_getter_prefix = AcecoolLib.Table.GetValue( _args, _args_count, 3, 'Get' )

			## Key is the index / key of a table where data is stored.. as _name arg is used as default if none is provided, we use the same here...
			_arg_key	= AcecoolLib.Table.GetValue( _args, _args_count, 4, _arg_name )

			## If _arg_default is set, show the default value in our output.. otherwise simply show that an argument can be used with this getter...
			if ( _arg_default != 'DEFAULT_NONE' ):
				_arg_default = '_default = ' + _arg_default
			else:
				_arg_default = '_default'

			## function <Class>.<GetPrefix><Name>( _default ), function <Class>.Set<Name>( _value )
			## Add the Setter and Getter functions to the primary function category...
			## self.AddEntry( CATEGORY_FUNCTION, _line_number, 'function ' + _arg_class + 'Set' + _arg_name + '( _value )', _depth, _mode, _pattern )
			## self.AddEntry( CATEGORY_FUNCTION, _line_number, 'function ' + _arg_class + _arg_getter_prefix + _arg_name + '( ' + _arg_default + ' )', _depth, _mode, _pattern )
			self.AddSimpleEntry( CATEGORY_FUNCTION, _line_number, 'function ' + _arg_class + 'Set' + _arg_name + '( _value )', _depth )
			self.AddSimpleEntry( CATEGORY_FUNCTION, _line_number, 'function ' + _arg_class + _arg_getter_prefix + _arg_name + '( ' + _arg_default + ' )', _depth )


		##
		## function SetupTasks( _tag, _preload, _doc_ready, _on_load, _on_iframe_load, _setup_observer )
		##
		if ( _pattern == 'SetupTasks' ):
			## Arguments for the function along with the defaults ( False ) for each ( in which case we output nothing to the info comment ).. 0 - 5 == 6 total args.
			_arg_tag				= AcecoolLib.Table.GetValue( _args, _args_count, 0, False )
			_arg_task_on_preload	= AcecoolLib.Table.GetValue( _args, _args_count, 1, False )
			_arg_task_on_ready		= AcecoolLib.Table.GetValue( _args, _args_count, 2, False )
			_arg_task_on_load		= AcecoolLib.Table.GetValue( _args, _args_count, 3, False )
			_arg_task_on_load_frame	= AcecoolLib.Table.GetValue( _args, _args_count, 4, False )
			_arg_task_on_setup_obs	= AcecoolLib.Table.GetValue( _args, _args_count, 5, False )

			## Base Data
			_data = ''

			## If any of the args exist, add a tag which is output with this line
			if ( _arg_task_on_preload == 'true' ):		_data = _data + '[ OnPreLoad ]'
			if ( _arg_task_on_ready == 'true' ):		_data = _data + '[ OnReady ]'
			if ( _arg_task_on_load == 'true' ):			_data = _data + '[ OnLoad ]'
			if ( _arg_task_on_load_frame == 'true' ):	_data = _data + '[ OnLoadFrame ]'
			if ( _arg_task_on_setup_obs == 'true' ):	_data = _data + '[ OnSetupObserver ]'

			## If _data has data, output a new entry with the information collected above added to it.. Also, add a blank line before this entry to make everything easier to read / follow..
			if ( _data != '' ):
				## Only add a space if it isn't the first entry...
				if ( self.HasCategoryEntries( CATEGORY_FUNCTION_ACCESSOR ) ):
					self.AddBlankEntry( CATEGORY_FUNCTION_ACCESSOR )
				## // Tasks:		[ OnPreLoad ][ OnReady ][ OnLoad ][ OnLoadFrame ][ OnSetupObserver ]
				## self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, '// Tasks:\t\t' + _data, _depth, _mode, _pattern )
				self.AddSimpleEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, '// Tasks:\t\t' + _data, _depth )


		##
		## function SetupProcesses( _tag, _process_anchors, _process_divs, _process_iframes, _process_mutation_observer )
		##
		if ( _pattern == 'SetupProcesses' ):
			## Arguments for the function along with the defaults ( False ) for each ( in which case we output nothing to the info comment ).. 0 - 4 == 5 total args.
			_arg_tag				= AcecoolLib.Table.GetValue( _args, _args_count, 0, False )
			_arg_process_anchors	= AcecoolLib.Table.GetValue( _args, _args_count, 1, False )
			_arg_process_divs		= AcecoolLib.Table.GetValue( _args, _args_count, 2, False )
			_arg_process_iframes	= AcecoolLib.Table.GetValue( _args, _args_count, 3, False )
			_arg_process_observer	= AcecoolLib.Table.GetValue( _args, _args_count, 4, False )

			## Base Data
			_data = ''

			## If any of the args exist, add a tag which is output with this line
			if ( _arg_process_anchors == 'true' ):		_data = _data + '[ Anchor ]'
			if ( _arg_process_divs == 'true' ):			_data = _data + '[ DIV ]'
			if ( _arg_process_iframes == 'true' ):		_data = _data + '[ I-Frame ]'
			if ( _arg_process_observer == 'true' ):		_data = _data + '[ Observer ]'

			## If _data has data, output a new entry with the information collected above added to it..
			if ( _data != '' ):
				## // Processes:	[ Anchor ][ DIV ][ I-Frame ][ Observer ]
				## self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, '// Processes:\t' + _data, _depth, _mode, _pattern )
				self.AddSimpleEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, '// Processes:\t' + _data, _depth )


		##
		## function AddFeature( _tag, _task = TASK_DEFAULT, _func )
		##
		if ( _pattern == 'AddFeature' ):
			##
			_arg_category = AcecoolLib.Table.GetValue( _args, _args_count, 0, 'TAG_NONE' )
			_arg_task = AcecoolLib.Table.GetValue( _args, _args_count, 1, 'TASK_NONE' )
			_arg_func = AcecoolLib.Table.GetValue( _args, _args_count, 2, 'FUNC_NONE' )

			## Add the Get<Name> and Set<Name> functions to CATEGORY_FUNCTION category - note, you can use CATEGORY_FUNCTION_ACCESSOR if you want it more out of the way...
			## self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'function AddFeature( ' + _arg_category + ', ' + _arg_task + ', function( _task, _data ) ... end );', _depth, _mode, _pattern )
			self.AddSimpleEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'function AddFeature( ' + _arg_category + ', ' + _arg_task + ', function( _task, _data ) ... end );', _depth )


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnAccessorFuncExpansion( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )		## self.CallParent( 'OnAccessorFuncExpansion', _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


	##
	## Create Custom AccessorFuncs here...
	##
	def OnSetupAccessorFuncs( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupAccessorFuncs', 'Creating custom AccessorFuncs for the current mapper ( ' + self.GetSetting( 'active_language', 'None' ) + ' )!' )


		##
		## Custom AccessorFunc Additions for this mapper
		##

		## For storing and recalling the last _tag used in a function call so _tag can be replaced with the full-version such as EXAMPLE, DEFAULT, BITBUCKET, etc..
		self.AccessorFunc( 'LastTag', CFG_ACECOOL_JAVASCRIPT_DEFAULT_LAST_TAG )


		##
		## Custom Config Additions for this mapper
		##

		## Example A
		## assignment a


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		return super( ).OnSetupAccessorFuncs( )		## return self.CallParent( 'OnSetupAccessorFuncs' )


	##
	## Setup this mapper configuration values here...
	##
	def OnSetupConfig( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupConfig', 'Applying custom settings for the current mapper ( ' + self.GetSetting( 'active_language', 'None' ) + ' )!' )


		##
		## Set the values of the config additions the mapper...
		##

		## Set the value for the Last Tag Tracker to
		self.SetLastTag( CFG_ACECOOL_JAVASCRIPT_DEFAULT_LAST_TAG )


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnSetupConfig( )		## self.CallParent( 'OnSetupConfig' )


	##
	## Called for each function argument - allows you to do anything you want...
	##
	## example: by replacing _p and _w which are hard-coded variable-names in my coding standard..
	##	function GM:PlayerDeath( _p, _w )
	## can become
	##	function GM:PlayerDeath( <Player>, <Weapon> )
	## or
	##	function GM:PlayerDeath( <Player> _p, <Weapon> _w )
	##
	def OnFunctionArgument( self, _arg, _category, _line_number, _code, _depth, _mode, _pattern ):
		## If our argument starts with TAG_ then remmember it for when the argument is _tag so we can ensure to show the developer the active TAG_ for each _tag...
		_word = 'TAG_'
		if ( _arg.startswith( _word ) ):
			_arg = AcecoolLib.String.SubStr( _arg, len( _word ), len( _arg ) )
			self.SetLastTag( _arg )

		## Show the previously remembered TAG_
		if ( _arg == '_tag' ):
			_arg = self.GetLastTag( )

		## Remove TASK_ from each argument which begins with it to keep things short...
		_word = 'TASK_'
		if ( _arg.startswith( _word ) ):
			_arg = AcecoolLib.String.SubStr( _arg, len( _word ), len( _arg ) )


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		return super( ).OnFunctionArgument( _arg, _category, _line_number, _code, _depth, _mode, _pattern )		## return self.CallParent( 'OnFunctionArgument', _arg, _category, _line_number, _code, _depth, _mode, _pattern )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End JavaScript using additional Acecool Mappings
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------