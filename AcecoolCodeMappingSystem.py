##
## Acecool Code Mapping System, a Stand-Alone Plugin for Sublime Text 3 which minimizes wasted time by scrolling, looking for classes, definitions, functions, and more.. while adding easy ways to include targeted information in the output panel to further improve coding efficiency - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task: Determine whether or not I can use output_panel or exec panel style panels without tabs for the output data, and if they can be vertical... If so, it means less wasted space and no minimap meaning I don't need to create a workaround for it... It also means I can resize it in real-time instead of removing, so it is always up, but if disabled nothing is processed... etc..
## Task: Make changes with some categories - remove from debugging type classification - and give notifications if debugging type categories aren't being output because debug mode is off with a switch to disable that warning in the config files soon
## Work: Create new Output System integrated with new object system for output... This new output system should create / inject text / code everywhere necessary and then force HIDE that text... For instance, string block code for categories, etc... and then HIDE that text... so the only code not in a string block or string ( escaped ) is actual code which can be processed / highlighted..
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Sublime Text Imports
## System Library
import os, sys, io, errno;

## Arrays
import array;

## Math functions - This shouldn't be required for floor / ceil... etc... odd..
import math;

## Regular Expressions for Search and Replace...
import re;

##
import time, datetime;

# File I/O
import codecs;

##
import imp;

##
import sqlite3;

##
## import AcecoolCodeMappingSystem.Acecool;

## Import the definitions ( Categories, enumerators, etc... everything definition based )
from AcecoolCodeMappingSystem.AcecoolCodeMappingSystemDefinitions import *;

## Import Acecool's Library of useful functions and globals / constants...
import AcecoolCodeMappingSystem.Acecool as AcecoolLib;

## ## Import Acecool's Library of useful functions and globals / constants...
## from AcecoolCodeMappingSystem.AcecoolLib_Python import *;

## ## Import Acecool's Library of useful functions and globals / constants...
## from AcecoolCodeMappingSystem.AcecoolLib_Sublime import *;

## ## Import Acecool's Library of useful functions and globals / constants...
## from AcecoolCodeMappingSystem.AcecoolLib_SublimeText3 import *;

from AcecoolCodeMappingSystem.AcecoolCodeMappingSystemPlugin import *;

## Task: Figure out why the plugin reloader reloads the data into memory but these files don't gain acess to new data despite importing them unless they call reload....
## sublime_plugin.reload_plugin( 'AcecoolCodeMappingSystem.AcecoolCodeMappingSystemDefinitions' );
## sublime_plugin.reload_plugin( 'AcecoolCodeMappingSystem.AcecoolLib_Python' );
## sublime_plugin.reload_plugin( 'AcecoolCodeMappingSystem.AcecoolLib_Sublime' );
## sublime_plugin.reload_plugin( 'AcecoolCodeMappingSystem.AcecoolLib_SublimeText3' );
## sublime_plugin.reload_plugin( 'AcecoolCodeMappingSystem.AcecoolCodeMappingSystem' );


## Grab Sublime Text Settings for packages which are ignored and which are installed!
ignored		= sublime.load_settings( 'Preferences.sublime - settings' ).get( 'ignored_packages' );
installed	= sublime.load_settings( 'Package Control.sublime - settings' ).get( 'installed_packages' );

ACMS_PRINT_TYPE_SYMBOLS						= 'symbols';
ACMS_PRINT_TYPE_CACHE						= 'cache';
ACMS_PRINT_TYPE_ERROR						= 'error';
ACMS_PRINT_TYPE_SETUP						= 'init';
ACMS_PRINT_TYPE_INIT						= 'init';
ACMS_PRINT_TYPE_SHUTDOWN					= 'shutdown';
ACMS_PRINT_TYPE_RUN							= 'run';
ACMS_PRINT_TYPE_CLASS_ARGS					= 'args';
ACMS_PRINT_TYPE_FUNCTION_ARGS				= 'args';
ACMS_PRINT_TYPE_ACCESSOR_EXPANSION			= 'accessor';
ACMS_PRINT_TYPE_RUN_INDENTATION				= 'run_repeat';
ACMS_PRINT_TYPE_ENTRY_ARGS					= 'args';
ACMS_PRINT_TYPE_ACCESSOR_RESET				= 'args';
ACMS_PRINT_TYPE_PROCESS_VARARGS				= 'args';
ACMS_PRINT_TYPE_EVERY_ENTRY					= 'lines';
ACMS_PRINT_TYPE_PROCESSING_RESULT			= 'result';
## ACMS_PRINT_TYPE_INIT						= '';
## ACMS_PRINT_TYPE_							= '';
## ACMS_PRINT_TYPE_							= '';
## ACMS_PRINT_TYPE_							= '';



##
## Default Configuration ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##

##
## End Configuration --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##



##
## End Development Example Configuration --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##




##
## Begin XCodeMapper Code --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##




##
## Entry Class - This is used in all callbacks which modify entries... It contains all of the information such as the type of data found, target category, original category, and much more... Used so the arguments don't need to change in the callbacks.
##
class AcecoolCodeMappingSystemDataPoint:
	##
	## Important Object Values!
	##

	## Name / ID of the XCodeMapperBase
	__name__ = 'AcecoolCodeMappingSystemDataEntry';


	##
	##
	##
	def __init__( self ):
		## Base Data for the data-point found

		##
		self.category = None;

		##
		self.category_original = None;

		## The data found and the code discovered on the line - ie the entire line, or lines
		self.data = '';
		self.code = '';

		## Line the data is found on - Start line when line_end is used
		self.line = -1;

		##
		self.line_end;

		## Character length
		self.len = 0;

		## Sublime Text Region of the discovered data - useful for so many reasons
		self.region = sublime.Region( 0, 0 );

		## Method of discovery - ie sublime symbols, line by line, file chunking
		self.method = '';

		## Update HasChanged when anything has been altered...
		self.has_changed = False;




##
## Note: For the region map - I want a single mapper type to be used, and the data fetched from entry will be based on type of region map.....
## but I want to make it so I only need to initialize one mapper for each feature: line goto, bookmark goto, activate macro, insert snippet, and more...
##
## So, I need a way to identify the type of map so the entry can pass the appropriate data to it - although, ideally I'd use generic calls and the entry weould simply pass itself to it...
## that way the entry can store line.. then the Line mapper will just use _entry.line to goto... yes....
##
## For bookmark mapper, I'd need bookmark id, or something... so I'd need a way to grab those entries and store data for the mapper....
## For macros, I'd need a way to load all macros and store by id, too....
## for links, a link / url is needed.... maybe I should use a generic entry_data_mapper_id to use, although that's not good because I want to add mappers to it..... so for each mapper, although
## that's bad too....
##
## OK... so in the event listenr... on double-click, etc.... I simply use a MAP / table to determine the entry... one line per entry... easy.. then in the entry, for each mapper, call the event.... easy... pass the entry through to it too...
## So on double-click if goto line exists, and _entry.line exists, then it jumps to the line.... If a Hover / Tooltip mapper exists, then on hover is called, with different event along with the LOCATION of the text where we click, and if within the
## 	range of where the mapper was added, then we do it...
##
## so if I add a hover for each individual arg, start char end char, on hover event called on line x > get entry > entry.ProcessRegionMaps( char_hovered, on hover ).... for each mapper, if mapper is set up for the range called on then execute.... YES...
##
## That looks like it.
##
##
## So Tasks:
## Task: Create table / Map for each line to map to an ACMSEntry object
## Task: Create table / map in ACMSEntry for all mappers... start and end char... Default if empty then it uses the GotoLine mapper
##
##
##
##
##
##
##

##
## Region Mapper - This is used to define new types of mappers such as Go to line, on hover, go to bookmark line, call macro, insert snippet, and more....
## Task: Add AccessorFuncBase Calls for the Code Mapping System Class, and also for the Plugin Object - easy lookups...
##
class ACMSRegionMapBase:
	pass;
class ACMSRegionMap( ACMSRegionMapBase ):
	##
	__name__	= 'ACMSRegionMap';


	##
	##
	##
	def __init__( self, _data = None ):
		self.data = _data;


	##
	## on_hover Region Map Callback - this is called when on_hover is called... _entry is the entry stored in the table / map for the line we're hovering over... _location is the current char in the line we're hovering over... can be local or world / global based...
	## so if on_hover exists for that location ( needs to be in Entry or using a helper ) then run this callback... same with others, just in different times...
	##
	def on_hover( self, _entry, _location ):
		self.region = true; # sublime.Region( );


	##
	## on_click Region Map Callback is called when an entry / line is clicked once.... This is left-click, right will have context menu type callback...
	##
	def on_click( self, _entry, _location ):
		self.region = true; # sublime.Region( );


	##
	## on_double_click Region Map Callback is called when an entry / line is left-clicked twice in rapid succession....
	##
	def on_double_click( self, _entry, _location ):
		self.region = true; # sublime.Region( );


##
## Region Map: Goto Line
##
class ACMSRegionMapGotoLine( ACMSRegionMap ):
	##
	__name__	= 'ACMSRegionMapGotoLine';


	##
	##
	##
	def on_double_click( self, _entry, _location ):
		if ( _entry.line > 0 ):
			print( 'ACMSRegionMapGotoLine ' + str( _entry.line ) );


##
## Region Map: Goto Website
##
class ACMSRegionMapGotoURL( ACMSRegionMap ):
	##
	__name__	= 'ACMSRegionMapGotoURL';


	##
	## If control click on an element with this, it goes to the website...
	##
	## def on_click( self, _entry, _location ):
	def on_double_click( self, _entry, _location ):
		if ( _entry.line > 0 ):
			print( 'ACMSRegionMapGotoURL ' + str( _entry.line ) );


##
## Region Map: Goto Bookmark
##
class ACMSRegionMapGotoBookmark( ACMSRegionMap ):
	##
	__name__	= 'ACMSRegionMapGotoBookmark';


	##
	## Goto Bookmark
	##
	## def on_click( self, _entry, _location ):
	def on_double_click( self, _entry, _location ):
		if ( _entry.line > 0 ):
			print( 'ACMSRegionMapGotoBookmark ' + str( _entry.line ) );


##
## Region Map: ToolTip / Popup Information Box
##
class ACMSRegionMapToolTip( ACMSRegionMap ):
	##
	__name__	= 'ACMSRegionMapToolTip';


	##
	## When hovering over a tooltip element, a dialog will open - Note: when open force on screen, also allow moving mouse over the tooltip without disappearing so links can be added...
	##
	def on_hover( self, _entry, _location ):
		if ( _entry.line > 0 ):
			print( 'Display Tooltip ' + str( _entry.line ) );


##
## Region Map: Advanced Description / Information Box for WIKI or WIKI Style Information...
##
class ACMSRegionMapWikiToolTip( ACMSRegionMapToolTip ):
	##
	__name__	= 'ACMSRegionMapWikiToolTip';


	##
	## Advanced Tooltip which shows advanced information from a wiki, or stored info... may also be able to display site when done...
	##
	def on_hover( self, _entry, _location ):
		if ( _entry.line > 0 ):
			print( 'ACMSRegionMapWikiToolTip ' + str( _entry.line ) );


##
## Region Map: Execute Macro
##
class ACMSRegionMapRunMacro( ACMSRegionMap ):
	##
	__name__	= 'ACMSRegionMapRunMacro';


	##
	## Activates a Macro when used...
	##
	## def on_hover( self, _entry, _location ):
	## def on_click( self, _entry, _location ):
	def on_double_click( self, _entry, _location ):
		if ( _entry.line > 0 ):
			print( 'ACMSRegionMapRunMacro ' + str( _entry.line ) );


##
## Region Map: Insert Snippet
##
class ACMSRegionMapRunSnippet( ACMSRegionMap ):
	##
	__name__	= 'ACMSRegionMapRunSnippet';


	##
	## Inserts a Snippet when activated
	##
	## def on_hover( self, _entry, _location ):
	## def on_click( self, _entry, _location ):
	def on_double_click( self, _entry, _location ):
		if ( _entry.line > 0 ):
			print( 'ACMSRegionMapRunSnippet ' + str( _entry.line ) );


##
## Region Map: Executes a command, either for the plugin or otherwise such as reprocessing the file, or otherwise...
##
class ACMSRegionMapRunCommand( ACMSRegionMap ):
	##
	__name__	= 'ACMSRegionMapRunCommand';


	##
	##
	##
	## def on_double_click( self, _entry, _location ):
	## def on_click( self, _entry, _location ):
	def on_hover( self, _entry, _location ):
		if ( _entry.line > 0 ):
			print( 'ACMSRegionMapRunCommand ' + str( _entry.line ) );


##
## Region Map: Output / Display - Helper, this creates a pre-set length of text ( you set len ) which can be edited on the fly in the output panel without reprocessing the file... Useful for showing in toolbox how many files are being processed in threads, and more..
##
class ACMSRegionMapOutputText( ACMSRegionMap ):
	##
	__name__	= 'ACMSRegionMapOutputText';


	##
	##
	##
	## def on_double_click( self, _entry, _location ):
	## def on_click( self, _entry, _location ):
	def on_hover( self, _entry, _location ):
		if ( _entry.line > 0 ):
			print( 'ACMSRegionMapOutputText ' + str( _entry.line ) );






##
##
##
class AcecoolCodeMappingSystemBase( ):
	##
	## Important Object Values!
	##

	## Name / ID of the XCodeMapperBase
	__name__ = 'AcecoolCodeMappingSystemBase';
	__panel__ = None;


##
## Used for temporary or developer functions... or for empty callbacks..
##
class AcecoolCodeMappingSystemBaseDev( AcecoolCodeMappingSystemBase ):
	##
	## Important Object Values!
	##

	## Name / ID of the XCodeMapperBase
	__name__ = 'AcecoolCodeMappingSystemBaseDev';




##
## Used for temporary or developer functions... or for empty callbacks..
##
class AcecoolCodeMappingSystemBaseEmptyCallbacks( AcecoolCodeMappingSystemBaseDev ):
	##
	## Important Object Values!
	##

	## Name / ID of the AcecoolCodeMappingSystemBase
	__name__ = 'AcecoolCodeMappingSystemBaseEmptyCallbacks';

	##
	## Callbacks for initializing local settings so the defaults aren't used...
	##
	def DeveloperFunc( self ):
		pass;
	def OnRunDeveloperFunc( self ):
		pass;
	def PreOnReset( self ):
		pass;
	def PostOnReset( self ):
		pass;
	def PreOnSetup( self ):
		pass;
	def PostOnSetup( self ):
		pass;
	def PreOnShutdown( self ):
		pass;
	def PostOnShutdown( self ):
		pass;
	def OnInitAccessors( self ):
		pass;
	def OnInitConfigAccessors( self ):
		pass;
	def PreOnRun( self ):
		pass;
	def OnRun( self ):
		pass;
	def PostOnRun( self ):
		pass;
	def PreOnSetupInternalAccessorFuncs( self ):
		pass;
	def PostOnSetupInternalAccessorFuncs( self ):
		pass;
	def PreOnSetupConfigAccessorFuncs( self ):
		pass;
	def PostOnSetupConfigAccessorFuncs( self ):
		pass;
	def Snippets( self ):
		pass;


	##
	## Empty, by default, Callbacks...
	##


	##
	## Helper to call Super...
	## Task: See if it is possible to alter the super call structure to make it seem more elegant and more in line with my standards...
	##
	def CallParent( self, _func_name, *_args ):
		pass;


	##
	## Add Syntax RuleSet ( _category is the type of data to retrieve such as function or comment.. mode is how we are to search and pattern is the text used to search)
	##
	def AddSyntaxRule( self, *_patterns ):
		pass;


	##
	## Internal Helper - Initializes values to the AccessorFunctions created for this segment..
	##
	def OnInitInternalConfigAccessors( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnInitInternalConfigAccessors' );


	##
	## Internal Helper - Initializes values to the AccessorFunctions created for this segment..
	##
	def OnInitInternalAccessors( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnInitInternalAccessors' );


	##
	##
	##
	def OnSetupAccessorFuncs( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupAccessorFuncs' );


	##
	##
	##
	def OnSetupClassArgument( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupClassArgument' );


	##
	##
	##
	def OnSetupFunctionArgument( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupFunctionArgument' );


	##
	##
	##
	def OnSetupCategories( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupCategories' );


	##
	##
	##
	def PreOnInit( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'PreOnInit' );


	##
	##
	##
	def PostOnInit( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'PostOnInit' );


	##
	##
	##
	def OnInitConfigAccessorFuncs( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnInitConfigAccessorFuncs' );




##
## Used for a Class Template - holds all of the standard replacement callbacks...
##
class AcecoolCodeMappingSystemClassTemplate( AcecoolCodeMappingSystemBaseEmptyCallbacks ):

	##
	## Callback - Return True or False to decide whether or not you want the line-numbers to be added to an entry - by returning False it also means you can't double-click to jump to that line... This was added for the .txt mapper...
	##
	def ShouldDisplayLineJumpLink( self, _category, _line_number, _code, _depth ):
		##
		return True;


	##
	## Callback - these are the symbols from the file we're currently editings symbols( ) from the view - ie all extracted symbols from Sublime Text...
	## In Short: If working this should give us all of the information from the Command Palette when we look at @, :, etc.. for function names and everything... A potentially almost perfect way to create a universal mapper...
	##
	## Task:
	##
	def OnMappingFileSymbols( self, _view, _symbols ):
		## pass
		self.print( ACMS_PRINT_TYPE_SYMBOLS, 'OnMappingFileSymbols', ' >> Symbols: ' + str( _symbols ) );
		## Symbols also don't give their function prefix...
		## if ( _code.startswith( 'def' ):

		## Symbols appear in a list, in a tuple with a region and the symbol...[((515, 525), 'PANEL:Init'), ((891, 910), 'PANEL:ControlValues'), ((1159, 1178), 'PANEL:PerformLayout')]

		## Make sure symbols actually exist...
		if ( _symbols == None ):
			return;

		## So for each entry
		for _entry in _symbols:
			## Tuples and lists use numerical indices
			## Grab the region object ( chars from start of file to X, and Total Chars in Region + X )
			_region = _entry[ 0 ];

			## Grab the partial code we're given - we can grab the full line using a helper function...
			_code = _entry[ 1 ];

			## Grab the line number using another helper...
			_line_number = self.GetSublimeViewLineNumberByRegion( _region );

			## Grab the line code by another helper...
			_line = self.GetFileDataByLine( _line_number );

			## Note - Symbols use regions meaning the starting char of from the entire code blob to the end... not line numbering so this has to be calculated...
			## Since other info is also lost we can really only guess at the data unless we check the file and look at other infomration...
			## self.AddEntry( CATEGORY_FUNCTION, _line_number, _code, _depth, _mode, _pattern, _class_cat );
			self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ), 0 );


	##
	## re.search( _pattern, _code )
	##
	def OnVariableDetection( self, _category, _line_number, _code_search, _depth, _search_mode, _pattern ):
		pass


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	##	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	##	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	##	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	## Task: Allow OnSetupSyntax to use the Config file for base / vanilla definitions? Maybe on this one...
	#
	##
	## Callback - Used to setup rules to track code, expand AccessorFuncs, etc.. at to display the output in the Code - Map Panel. Priority is handled using standard English Reading direction ie using self.AddSyntax( COMMENT, STARTSWITH, '#', '##' ) means ## would never be handled.
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' );

		##
		## The following are added to almost every mapper.. These categories use a category type which is only active inside of comments so they can trigger inside of comments, or out.
		## Comments are stripped from the line and that result is used to search so StartsWidth will work even if the line starts with ;; TODO: Testing..
		##

		## Task Related - I highly recommend adding a space in front to prevent detecting function calls which start with these words in some languages which use : such as Lua..
		self.AddSyntax( CATEGORY_TODO, SEARCH_TYPE_STARTSWITH, 'TODO: ', 'Do: ', 'Fix: ', 'Task: ', 'Update: ', 'Debug: ', 'Important: ', 'Work: ', 'Repair: ', 'Alter: ', 'Change: ' );
		self.AddSyntax( CATEGORY_COMPLETED_TASK, SEARCH_TYPE_STARTSWITH, 'Done: ', 'Fixed: ', 'Updated: ', 'Worked: ', 'Debugged: ', 'Updated: ', 'Solved: ', 'Worked: ', 'Repaired: ', 'Altered: ', 'Changed: ' );

		## These are typically added to code for the developer.. they aren't important enough to list in the "important work / todo" area of our list... but they can be added..
		self.AddSyntax( CATEGORY_NOTE, SEARCH_TYPE_STARTSWITH, 'Note: ', 'Info: ', 'Debugging: ' );


	##
	## Same as OnSetupSyntax but kept as a deparate function to set up indentation tracking, and other rules...
	##
	## Task:
	##
	def OnSetupSyntaxRules( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntaxRules' );

		##
		## self.AddSyntax( CATEGORY_RULE_INDENT, SEARCH_TYPE_CONTAINS, "for" );
		## self.AddSyntax( CATEGORY_RULE_INDENT, SEARCH_TYPE_CONTAINS, "do" );
		## self.AddSyntax( CATEGORY_RULE_INDENT, SEARCH_TYPE_CONTAINS, "while" );
		## self.AddSyntax( CATEGORY_RULE_INDENT, SEARCH_TYPE_CONTAINS, "if" );

		## then is used in many cases - determine which rules I can skip by including then and add the ones which don't use it..
		## self.AddSyntax( CATEGORY_RULE_INDENT, SEARCH_TYPE_CONTAINS, "then" );

		##
		## self.AddSyntax( CATEGORY_RULE_UNINDENT, SEARCH_TYPE_CONTAINS, "end" );

		## These cancel each other out - and since an IF always preceeds these, it doesn't matter...
		## self.AddSyntax( CATEGORY_RULE_INDENT, SEARCH_TYPE_CONTAINS, "else if" );
		## self.AddSyntax( CATEGORY_RULE_INDENT, SEARCH_TYPE_CONTAINS, "else" );
		## self.AddSyntax( CATEGORY_RULE_UNINDENT, SEARCH_TYPE_CONTAINS, "else if" );
		## self.AddSyntax( CATEGORY_RULE_UNINDENT, SEARCH_TYPE_CONTAINS, "else" );


	##
	## Setup your code_map.<ext>.py mapper configuration in this callback - Copy the OnSetupConfigDefaults to give yourself a layout...
	##
	def OnSetupConfig( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupConfig' );


	##
	## Setup Data-Type to Arg Conversion. This is used for each function / class argument... This feature is to give more information to the overview panel so it can appear similar to a WIKI overview for all of the data without the long descriptions ( depending on the cfg )
	## The entire systems purpose is to give just enough data to be incredibly useful, but not enough to overwhelm or confuse.. Keep it concise.. This feature can inform you of the data-type to expect to use. For this to work, you need to set and follow coding-standards - more specifically: Argument Naming Standards sub-section.
	##
	## If the configuration is set up to do anything ( ie non-default ) then these are processed. If set to Default or _ARG then nothing happens...
	## If set to Both then they'll appear as: '<DataType> _arg' / If set to Data-Type the args will be replaced with only: '<DataType>'
	## The Space after the suffix, prefix as '<', and suffix as '>' chars and the arg is controlled by the Spacer, Prefix and Suffix Configuration Options respectfully.
	##
	## Task:
	##
	def OnSetupArgDataType( self ):
		##
		## Note: I populated this mapper with several examples to show they are unique - ie - and several to show what this system can do.
		##	I will likely add more elements to this to map out what each function should receive for developmental purposes or as a sort of quick-wiki output / helper for CodeMap..
		##	Additionally, If you modify the arguments with OnClassArgument or OnFunctionArgument - this system will NOT overwrite your modifications, this is by design.
		##	I may modify this system so it reads the modified version into the helper-function to determine whether or not it can modify the modification.. However, being able to have the last-say in OnClass/FunctionArgument can be useful in certain scenarios!!
		##
		## This system was created to simplify modifying arguments - add a single line and modify function / class arguments with ease instead of adding the callbacks and setting up if / elif lines of code...
		##

		## Adds an example which replaces 'str' argument of functions with <BlahBlahBlah> to show that class TestingString( str ): will not be affected by it...
		## self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, 'BlahBlahBlah', 'str' )

		## Adds a replacement for 'str' argument in CLASS categories - Functions will not receive this modification... This is to show that class TestingString( str ): will be modified properly..
		## self.AddArgDataTypeEntry( CATEGORY_TYPE_CLASS, 'Extends Default Object String', 'str' )

		## Adds a replacement for 'self' argument in FUNCTION categories - Classes will not receive this modification... This is to show that all functions with self as an argument will be properly modified!
		## self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, 'This', 'self' )
		pass;


	##
	## Similar to OnFunctionArgument - does the same thing for CLASS arguments - type specific...
	##
	## Task:
	##
	def OnClassArgument( self, _arg, _category, _line_number, _code, _depth, _search_mode, _pattern ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_CLASS_ARGS, 'OnClassArgument', _arg );

		## Return the original, or altered, argument.. If you return True or False, the argument is removed. If you return None, then the original argument is used.
		return _arg;


	##
	## Called for each function argument - allows you to do anything you want...
	##
	## example: by replacing _p and _w which are hard-coded variable-names in my coding standard..
	##	function GM:PlayerDeath( _p, _w )
	## can become
	##	function GM:PlayerDeath( <Player>, <Weapon> )
	## or
	##	function GM:PlayerDeath( <Player> _p, <Weapon> _w )
	##
	## Task:
	##
	def OnFunctionArgument( self, _arg, _category, _line_number, _code, _depth, _search_mode, _pattern ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_FUNCTION_ARGS, 'OnFunctionArgument', _arg );

		##
		return _arg;


	##
	## When an AccessorFunc type is found, this is the callback executed which allows you to expand the AccessorFunc into multiple entries...
	##
	## example: When encountering a GMod Lua / GLua util function, it is expanded into 2 entries representative of the 2 functions AccessorFunc creates..
	##
	## Code:
	##	AccessorFunc( perp.lighting, "somevar", "LightsEnabled", FORCE_BOOL )
	##
	## XCodeMapper Entries - I set it to add into CATEGORY_FUNCTION_ACCESSOR categories:
	##	function perp.lighting:GetLightsEnabled( );:102
	##	function perp.lighting:SetLightsEnabled( _value :: Note: _value forced tobool( ) on set! );:102
	##
	##
	## Because each AccessorFunc creation method is different for each function we look for... they're all routed through here to be expanded upon...
	## If pattern x then args[ 1 ] = classname, args[ 2 ] = name ( so S/Get<Name> etc.. ), and more... so if args[ n ] is "" then we don't add Get prefix to the Getter.. and so on..
	## Also, we can force the data to show up in different categories.. for example __ functions can be put into CATEGORY_FUNCTION_INTERNAL which appears last in list of functions..
	## or we can determine whether or not x function was already added which has an optional creation function ( ie on first time call of an accessor func, it
	##	creates __GetDataTable, __GetData, __SetData internal function but the next time we call it on the same class, it shouldn't output these ).. etc...
	##
	## The benefit of this function is that you can do ANYTHING you want with the data of AccessorFuncs ( I may add callbacks for each category type or search later on, but right now AccessorFunc is what needs it )
	##
	## Task:
	##
	def OnAccessorFuncExpansion( self, _search_mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_ACCESSOR_EXPANSION, 'OnAccessorFuncExpansion', _code );

		## Add the entry, if we haven't...
		self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, _code, _depth, _search_mode, _pattern );


	##
	## Callback - Called for each line in the file - can also prevent entries from being added ( when found by AddSyntax method ) by returning a boolean. Can also alter entries code ( by returning a string - will only alter if the code being returned was going to be added to a category - otherwise the line is ignored anyway )
	## Note: This is a simple callback so basic info is available only such as the line-number, the code for that line, and the depth of any contents of that line. It is called regardless of comment-status so comment-status is also provided..
	##
	## Task:
	##
	def OnProcessLine( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		pass;


	##
	## Callback - This will be used to enable parsing the file multiple ways to speed up processing... First pass will be in chunks
	##
	## Task: Create
	##
	def OnProcessChunk( self, _line_number_start, _line_number_end, _code, _depth, _in_comment, _in_comment_block ):
		pass;


	##
	## Override / Helper - Callback used to override the indent characters used... IE Lets say you use tabs for indenting... Now lets say you want to create a TREE style view.. Use this function... Inject code - ie injecting into tabs is easy, add at the beginning and it is absorbed, up to 3 chars without the tab jumping ( or 7 on computers which have it set to 8 spaces per tab )... Or use spaces and use my FormatColumn code to absorb your changes...
	##
	## Task: Instead of calling this during the whole file processing - I'm going to call this callback during the building-output-data process..
	## Note: Meaning I can simply return the blank-space on the left-side of the _code and include more info such as how many additional entries are in this current category, sub-category data and much much much more meaning the lines below
	##		would be within reach because all of the necessary info would be available to make it much easier... Returning would simply be the WHITESPACE zone with new info and with a monospaced font, adding the lines won't cause any issues...
	##
	##
	## Useful Chars:
	##
	##	Unknown:		⌐ ¬ ☼ ∙ · ‼ ₧ ¡ µ τ ε ¶ §
	##	Math:			Φ Θ Ω δ φ α σ Δ
	##		Numeric:	½ ¼ ² ⁿ °
	##		Operators:	≥ ≤ ± √ ≡ Σ ≈ ÷
	##		Constants:	π ∞
	##		Currency:	¢ £
	##		Other:		ª º ⌠ ⌡
	##	Programming:		ƒ ∩
	##	French:			« »
	##	German:			ß
	##	Emojis:			☺ ☻
	##	Genders:			♂ ♀
	##	Media:			♪ ♫
	##	Arrows:			↨ ↕ ↑ ↓ → ← ↔ ▲ ▼ ► ◄
	##	Cards:			♥ ♦ ♣ ♠
	##	Bullets:			• ◘ ○ ◙
	##	Fading:			░ ▒ ▓
	##	Blocks:			█ ▄ ▌ ▐ ▀ ▬ ■
	##	Single:			│ ┤ ┐ └ ┴ ├ ┬ ─ ┼ ┘ ┌ ∟ Γ
	##	Double:			╣ ║ ╗ ╝ ╚ ╔ ╩ ╦ ╠ ═ ╬
	##	Both:			╡ ╢ ╖ ╕ ╜ ╛ ╞ ╟ ╧ ╨ ╤ ╥ ╙ ╘ ╒ ╓ ╫ ╪
	##
	## Used to create output similar to this ( Callback is currently a WORK IN PROGRESS and is considered non-functional ):
	##
	##	.			|	First element - others exist... This is a category... Select these chars: [ ┌ ] because of next - and top / [ ─ ] Categories aren't indented, but because of next / [ ☼ ] Represents an end-point... Category icon.. I could simply do because of nested objects:	[ ┬ ] OR [ ┐ ] if no icon char is used or if I wanted no danglers..
	##
	##
	##	☼ Class Functions Table - Category...		|		##	☼ Class Functions Table - Category...		|	##	☼ Class Functions Table - Category..		|
	##		Vector Class							|		##		☼ Vector Class							|	##		☼ Vector Class							|
	##			ƒ X( )								|		##			function X( )						|	##			function X( )						|
	##			ƒ Y( )								|		##			function Y( )						|	##			function Y( )						|
	##			ƒ Z( )								|		##			function Z( )						|	##			function Z( )						|
	##												|		##												|	##												|
	##		☼ Angle Class							|		##		☼ Angle Class							|	##		☼ Angle Class							|
	##			ƒ P( )								|		##			function P( )						|	##			function P( )						|
	##			ƒ Y( )								|		##			function Y( )						|	##			function Y( )						|
	##			ƒ R( )								|		##			function R( )						|	##			function R( )						|
	##												|		##												|	##												|
	##	☼ Functions Category						|		##	☼ Functions Category						|	##	☼ Functions Category						|
	##		ƒ isstring( _data )						|		##		function isstring( _data )				|	##		function isstring( _data )				|
	##		ƒ isnumber( _data )						|		##		function isnumber( _data )				|	##		function isnumber( _data )				|
	##		ƒ isfunction( _data )					|		##		function isfunction( _data )			|	##		function isfunction( _data )			|
	##		ƒ isbool( _data )						|		##		function isbool( _data )				|	##		function isbool( _data )				|

	##												|
	##	┌─☼ Class Functions Table - Category...		|		##	┌─┬☼ Class Functions Table - Category...	|	##	┌─┬☼ Class Functions Table - Category..		|
	##	│ ├──☼ Vector Class							|		##	│ ├─┬☼ Vector Class							|	##	│ ├──☼ Vector Class							|
	##	│ │ ├─☼ ƒ X( )								|		##	│ │ ├──☼ function X( )						|	##	│ │ ├─☼ function X( )						|
	##	│ │ ├─☼ ƒ Y( )								|		##	│ │ ├──☼ function Y( )						|	##	│ │ ├─☼ function Y( )						|
	##	│ │ └─☼ ƒ Z( )								|		##	│ │ └──☼ function Z( )						|	##	│ │ └─☼ function Z( )						|
	##	│ │											|		##	│ │											|	##	│ │											|
	##	│ └──☼ Angle Class							|		##	│ └┬☼ Angle Class							|	##	│ └──☼ Angle Class							|
	##	│	├─☼ ƒ P( )								|		##	│	├☼ function P( )							|	##	│	├─☼ function P( )						|
	##	│	├─☼ ƒ Y( )								|		##	│	├☼ function Y( )							|	##	│	├─☼ function Y( )						|
	##	│	└─☼ ƒ R( )								|		##	│	└☼ function R( )							|	##	│	└─☼ function R( )						|
	##	│											|		##	│											|	##	│											|
	##	└─☼ Functions Category						|		##	└┬☼ Functions Category						|	##	└─☼ Functions Category						|
	##	├──☼ ƒ isstring( _data )					|		##	├─☼ function isstring( _data )				|	##	├──☼ function isstring( _data )			|
	##	├──☼ ƒ isnumber( _data )					|		##	├─☼ function isnumber( _data )				|	##	├──☼ function isnumber( _data )			|
	##	├──☼ ƒ isfunction( _data )				|		##	├─☼ function isfunction( _data )			|	##	├──☼ function isfunction( _data )			|
	##	└──☼ ƒ isbool( _data )					|		##	└─☼ function isbool( _data )				|	##	└──☼ function isbool( _data )				|
	##
	## Task: Add more args...
	##
	def OnCalcDepthChars( self, _category, _line_number, _code, _depth ):
		## self.OnCalcDepthChars( _category, _line, _code, _depth );


		## self.GetCfgIndentChars( ) + self.GetDepthString( _depth );
		## return '│' + self.GetCfgIndentChars( ) + '│' + self.GetDepthString( _depth );
		## return self.GetCfgIndentChars( ) + self.GetDepthString( _depth );
		return self.GetSetting( 'output_indent_chars', '\t' ) + self.GetDepthString( _depth );


	##
	## Override / Helper to allow for depth to be all uniform or based on indentation ( if you return _depth )
	##
	def OnCalcDepth( self, _category, _line_number, _code, _depth, _search_mode, _pattern ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_ENTRY_ARGS, 'OnCalcDepth' );

		## For functions and objects / classes.. allow depth to be based on indentation - this is primarily for Python until I add the nested categories system...
		if ( _category == CATEGORY_FUNCTION or _category == CATEGORY_CLASS ):
			return _depth;

		return 0;


	##
	## Callback - Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _search_mode, _pattern, _category_original = None ):
		## Do nothing by default...
		return _code;


	##
	## Callback - Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	##
	def PostOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _search_mode, _pattern, _category_original = None ):
		pass;


	##
	## Similar to PreOnEntry except instead of modifying Code, it modifies the Category...
	##
	def PreOnAddEntryCategory( self, _category, _line_number, _code, _depth, _search_mode, _pattern ):
		return _category;


	##
	## OVerride / Helper to modify gutter icons before they're added or to prevent them from being added at all...
	## Task: Create functionality
	##
	def OnCalcGutterIcon( self ):
		## Basically - If function discovered in PreOnAddEntry we can do self.AddGutterIcon( line_number, self.GetGutterIcon( 'function', 'ƒ' ) ) and this function will let you capture it before it is added - so if we want to modify behavior ( for the _User class - to alter to a different icon return string id / or number enum of gutter icon ) or prevent it from being displayed ( Return boolean )
		## if ( _category == CATEGORY_CLASS and _icon_id == ICON_CLASS ):
		##		## something along these lines...
		##		return ICON_CLASS_HEADER;
		##
		##		## or.. to stop it from showing up...
		##		return False;
		pass;



	##
	## Internal Helper - Reset the internal AccessorFuncs to their default values..
	##
	def OnResetInternalAccessors( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_ACCESSOR_RESET, 'OnResetInternalAccessors' );

		## Reset Class Data
		self.ResetAccessors( 'ActiveClass' );
		## self.ResetAccessorFuncValues( 'ActiveClass' );

		## Are we in a comment block - note, this can't be in the line by line scope as blocks can extend beyond a single line...
		self.ResetAccessors( 'InComment', 'InCommentBlock', 'CommentBlockStartLine', 'CommentBlockEndLine', 'CommentBlockIndexStart', 'CommentBlockIndexEnd', 'Depth', 'LastDepth', 'Realm', 'FileData', 'FileLines' );
		## self.ResetAccessorFuncValues( 'InComment', 'InCommentBlock', 'CommentBlockStartLine', 'CommentBlockEndLine', 'CommentBlockIndexStart', 'CommentBlockIndexEnd', 'Depth', 'LastDepth', 'Realm', 'FileData', 'FileLines' );


	##
	## Callback used to alter Code - Map Panel configuration per mapper...
	##
	## @Arg: self:		Reference to the __ACTIVE__ XCodeMapper Object - With inheritence this'll point to XCodeMapper_Python_Default if you're mapping a Python file and you're using the Default Python mapper...
	## @Arg: _panel:	References the "Code - Map" Panel - This is a Sublime Text View Object with ALL of the functions, etc.. associated with it...
	## @Arg: _settings: This is a shortcut reference - instead of having to type _panel.settings( ).set( 'key', 'value' ) you can simply call _settings.set( 'key', 'value' )
	##
	def OnSetupCodeMapPanel( self, _panel, _settings ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupCodeMapPanel' );

		## Plugin Update function...
		self.GetPluginObject( ).UpdateOutputPanelSettings( );

		## Helpers
		## _settings.set( 'font_face', self.GetSetting( 'panel_font_face', 'Consolas' ) );
		## _settings.set( 'font_size', self.GetSetting( 'panel_font_size', 10 ) );

		## Disable Scroll Past End...
		## print( ' >> Code - Map Panel >> scroll_pass_end set to False!' );
		## _panel.settings( ).set( 'scroll_past_end', False );


		##
		## Minimap Options..
		##

		## Minimap - Unfortunately this is for the entire window... not just the panel and setting show_minimap doesn't alter it..
		## _panel.window( ).set_minimap_visible( False );
		## _panel.window( ).set_minimap_visible( True );

		## Always visualise the viewport on the minimap, as opposed to only showing it on mouse over.
		## _panel.settings( ).set( "always_show_minimap_viewport", True );

		## Set to true to draw a border around the visible rectangle on the minimap if visible. The color of the border will be determined by the "minimapBorder" key in the color scheme.
		## _panel.settings( ).set( "draw_minimap_border", True );


		##
		## Gutter options
		##

		## If set to true, a gutter will apear which can show line numbers, the fold-arrows, icons and more.... This needs to be true for the other options to work...
		## _code_map.settings( ).set( "gutter", False );

		## Show line-numbers on the gutter
		## _code_map.settings( ).set( "line_numbers", True );

		## Show fold-arrows / buttons on the gutter...
		## _code_map.settings( ).set( "fold_buttons", False );


		##
		## Other Options...
		##

		## If you want a different font-size to appear in the mapping panel compared to the rest of your code - set it here...
		## _code_map.settings( ).set( "font_size", 10.5 );


		##
		## Options which don't exist, but should...
		##

		## _panel.settings( ).set( "draw_minimap", False );
		## _panel.settings( ).set( "show_minimap", False );
		## _panel.settings( ).set( "hide_minimap", True );
		## _panel.settings( ).set( "minimap_hide", True );
		## _panel.settings( ).set( "minimap", False );


	##
	## Internal Processor
	## TODO: Add code to unset all data used by this instance and then destroy itself...
	##
	def OnShutdown( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_SHUTDOWN, 'OnShutdown' );


	##
	## Callback for displaying Help & Support Information for XCodeMapper created by Josh 'Acecool' Moser which is an extension of CodeMap created by oleg-shilo for Sublime Text Editor!
	##
	def OnSetupHelp( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupHelp' );

		##
		## Adding help info here!
		##
		self.AddHelp( 'Welcome to \'Acecool\'sCode Mapping System, a Stand Alone Sublime Text 3 Package!' );
		self.AddHelp( 'This help category is to list tips & tricks in addition to providing help and support information!' );
		self.AddHelp( '' );
		## self.AddHelp( 'For help and support with CodeMap by oleg-shilo:' );
		## ## self.AddHelp( '' );
		## ## self.AddHelp( 'Website:\thttps://github.com/oleg-shilo/' );
		## self.AddHelp( self.GetCfgIndentChars( ) + 'Download:' + self.GetCfgIndentChars( ) + 'https://github.com/oleg-shilo/sublime-codemap/' );
		## self.AddHelp( self.GetCfgIndentChars( ) + 'Support:' + self.GetCfgIndentChars( ) + 'https://github.com/oleg-shilo/sublime-codemap/issues/' );
		## self.AddHelp( '' );
		self.AddHelp( 'For help and support with XCodeMapper and the Acecool Development Library by Josh \'Acecool\' Moser' );
		## self.AddHelp( '' );
		## self.AddHelp( 'Website: http://www.acecoolco.com/' );
		## self.AddHelp( 'Website:\thttps://bitbucket.org/Acecool/' );
		self.AddHelp( self.GetSetting( 'output_indent_chars', '' ) + 'Download:' + self.GetSetting( 'output_indent_chars', '' ) + 'https://bitbucket.org/Acecool/acecooldev_sublimetext3' );
		self.AddHelp( self.GetSetting( 'output_indent_chars', '' ) + 'Support:' + self.GetSetting( 'output_indent_chars', '' ) + 'https://bitbucket.org/Acecool/acecooldev_sublimetext3/Issues/' );
		self.AddHelp( self.GetSetting( 'output_indent_chars', '' ) + 'Donate:' + self.GetSetting( 'output_indent_chars', '' ) + 'https://www.paypal.me/Acecool' );
		self.AddHelp( '' );

		if ( self.GetSetting( 'show_developer_story', False ) ):
			self.AddHelp( 'I have a broken neck, back and severe nerve damage from an automobile accident on May 7, 2011 where' );
			self.AddHelp( ' the other person ran a stop sign and hit me. I\'ve been fighting this for over 6 years and am receiving' );
			self.AddHelp( ' no help from them or the insurance. Any support you can offer would be graceously accepted and would go' );
			self.AddHelp( ' a long way towards helping cover costs of medication, treatment, physical therapy, etc..' );
			self.AddHelp( 'Thank you for taking the time to read this!' );
			self.AddHelp( '' );
			self.AddHelp( self.GetSetting( 'output_indent_chars', '' ) + 'Donate:' + self.GetSetting( 'output_indent_chars', '' ) + 'https://www.paypal.me/Acecool' );
			self.AddHelp( '' );

		self.AddHelp( '' )
		self.AddHelp( 'To hide this category - or any other - update the configuration value in one of the configuration files ( specifically: ACMS_Definitions ).' );
		self.AddHelp( ' Update:' );
		self.AddHelp( '		output_categories_hidden' );
		self.AddHelp( ' Un-Comment:' );
		self.AddHelp( '		"CATEGORY_HELP": true,' );




##
## Helper base...
##
class AcecoolCodeMappingSystemBaseHelpers( AcecoolCodeMappingSystemClassTemplate ):
	##
	## Important Object Values!
	##

	## Name / ID of the AcecoolCodeMappingSystemBase
	__name__ = 'AcecoolCodeMappingSystemBaseHelpers';


	## Helper - Grabs the Code - Map Panel..
	def GetCodeMapPanel( self ):											return self.__panel__;

	## Helper - Grabs the Code - Map Panel..
	def GetCodeMapView( self ):												return self.__panel__;

	## Helper - Grabs the Code - Map Panel..
	def GetMappedFileView( self ):											return self.__mapped_file_panel__;


	##
	## ACMS Plugin Object Helpers
	##

	##
	def GetPluginObject( self ):											return ACMS( );
	## def GetPluginDatabase( self ):											return ACMS( ).Database( );
	## def GetPluginDatabase( self ):											return ACMS( ).GetThread( ).Database( );
	## def GetPluginDatabase( self ):											return ACMSDatabase( ACMS( ) );
	def GetPluginDatabase( self ):										#	return ACMSDatabase( ACMS( ) ) if ( ACMS( ).GetThreadState( ) ) else ACMS( ).Database( );
		##
		_plugin = None;
		_database = None;

		##
		if ( ACMS( ).UseMappingThreads( ) ):
			_plugin														= GetPluginNode( ).GetThreadedPlugin( );
			_database													= GetPluginNode( ).GetThreadedDatabase( );
		else:
			_plugin														= GetPluginNode( ).GetThreadedPlugin( );
			_database													= GetPluginNode( ).GetThreadedDatabase( );

		return _database;


	def GetPlugin( self ):													return ACMS( );
	def Plugin( self ):														return ACMS( );

	##
	def GetSettingsObject( self, _name = '' ):								return self.GetPluginObject( ).GetSettingsObject( _name );
	def SaveSettings( self, _name = '' ):									return self.GetPluginObject( ).SaveSettings( _name );

	## Sublime Text Preferences - Specifically for the Operating System in Use...
	def GetSyntaxSettingsFileName( self, _view = None, _syntax = None ):	return self.GetPluginObject( ).GetSyntaxSettingsFileName( _view, _syntax );
	def GetSyntaxSettingsObject( self, _syntax = None ):					return self.GetPluginObject( ).GetSyntaxSettingsObject( _syntax );
	def SaveSyntaxSettings( self, _syntax = None ):							return self.GetPluginObject( ).SaveSyntaxSettings( _syntax );
	def HasSyntaxSetting( self, _key ):										return self.GetSyntaxSettingsObject( ).has( _key );
	def GetSyntaxSetting( self, _key, _default = None, _syntax = None ):	return self.GetPluginObject( ).GetSyntaxSetting( _key, _default, _syntax );
	def SetSyntaxSetting( self, _key, _value = None, _syntax = None ):		return self.GetPluginObject( ).SetSyntaxSetting( _key, _value, _syntax );

	## Sublime Text Preferences
	def GetSublimeSettingsFileName( self ):									return self.GetPluginObject( ).GetSublimeSettingsFileName( );
	def GetSublimeSettingsObject( self ):									return self.GetPluginObject( ).GetSublimeSettingsObject( );
	def SaveSublimeSettings( self ):										return self.GetPluginObject( ).SaveSublimeSettings( );
	def HasSublimeSetting( self, _key ):									return self.GetSublimeSettingsObject( ).has( _key );
	def GetSublimeSetting( self, _key, _default = None ):					return self.GetPluginObject( ).GetSublimeSetting( _key, _default );
	def SetSublimeSetting( self, _key, _value = None ):						return self.GetPluginObject( ).SetSublimeSetting( _key, _value );

	## Sublime Text Preferences - Specifically for the Operating System in Use...
	def GetPlatformSettingsFileName( self ):								return self.GetPluginObject( ).GetPlatformSettingsFileName( );
	def GetPlatformSettingsObject( self ):									return self.GetPluginObject( ).GetPlatformSettingsObject( );
	def SavePlatformSettings( self ):										return self.GetPluginObject( ).SavePlatformSettings( );
	def HasPlatformSetting( self, _key ):									return self.GetPlatformSettingsObject( ).has( _key );
	def GetPlatformSetting( self, _key, _default = None ):					return self.GetPluginObject( ).GetPlatformSetting( _key, _default );
	def SetPlatformSetting( self, _key, _value = None ):					return self.GetPluginObject( ).SetPlatformSetting( _key, _value );

	## Plugin Definitions.
	def GetDefinitionsSettingsFileName( self ):								return self.GetPluginObject( ).GetDefinitionsSettingsFileName( );
	def GetDefinitionsSettingsObject( self ):								return self.GetPluginObject( ).GetDefinitionsSettingsObject( );
	def SaveDefinitionSettings( self ):										return self.GetPluginObject( ).SaveDefinitionSettings( );
	def HasDefinitionsSetting( self, _key ):								return self.GetDefinitionsSettingsObject( ).has( _key );
	def GetDefinitionsSetting( self, _key, _default = None ):				return self.GetPluginObject( ).GetDefinitionsSetting( _key, _default );
	def SetDefinitionsSetting( self, _key, _value = None ):					return self.GetPluginObject( ).SetDefinitionsSetting( _key, _value );

	## Plugin Setting
	def GetPluginSettingsFileName( self ):									return self.GetPluginObject( ).GetPluginSettingsFileName( );
	def GetPluginSettingsObject( self ):									return self.GetPluginObject( ).GetPluginSettingsObject( );
	def SavePluginSettings( self ):											return self.GetPluginObject( ).SavePluginSettings( );
	def HasPluginSetting( self, _key ):										return self.GetPluginSettingsObject( ).has( _key );
	def GetPluginSetting( self, _key, _default = None ):					return self.GetPluginObject( ).GetPluginSetting( _key, _default );
	def SetPluginSetting( self, _key, _value = None ):						return self.GetPluginObject( ).SetPluginSetting( _key, _value );

	## Plugin Mapping Panel Configuration.
	def GetPanelsSettingsFileName( self ):									return self.GetPluginObject( ).GetPanelsSettingsFileName( );
	def GetPanelsSettingsObject( self ):									return self.GetPluginObject( ).GetPanelsSettingsObject( );
	def SavePanelSettings( self ):											return self.GetPluginObject( ).SavePanelSettings( );
	def HasPanelSetting( self, _key ):										return self.GetPanelsSettingsObject( ).has( _key );
	def GetPanelSetting( self, _key, _default = None ):						return self.GetPluginObject( ).GetPanelSetting( _key, _default );
	def SetPanelSetting( self, _key, _value = None ):						return self.GetPluginObject( ).SetPanelSetting( _key, _value );

	## Plugin Mapping Settings.
	def GetMapSettingsFileName( self ):										return self.GetPluginObject( ).GetMapSettingsFileName( );
	def GetMappingSettingsObject( self ):									return self.GetPluginObject( ).GetMappingSettingsObject( );
	def SaveMapSettings( self ):											return self.GetPluginObject( ).SaveMapSettings( );
	def HasMapSetting( self, _key ):										return self.GetMappingSettingsObject( ).has( _key );
	def GetMapSetting( self, _key, _default = None ):						return self.GetPluginObject( ).GetMapSetting( _key, _default );
	def SetMapSetting( self, _key, _value = None ):							return self.GetPluginObject( ).SetMapSetting( _key, _value );

	## Plugin RunTime Settings.
	def GetRunTimeSettingsFileName( self ):									return self.GetPluginObject( ).GetRunTimeSettingsFileName( );
	def GetRunTimeSettingsObject( self ):									return self.GetPluginObject( ).GetRunTimeSettingsObject( );
	def SaveRunTimeSettings( self ):										return self.GetPluginObject( ).SaveRunTimeSettings( );
	def HasRunTimeSetting( self, _key ):									return self.GetRunTimeSettingsObject( ).has( _key );
	def GetRunTimeSetting( self, _key, _default = None ):					return self.GetPluginObject( ).GetRunTimeSetting( _key, _default );
	def SetRunTimeSetting( self, _key, _value = None ):						return self.GetPluginObject( ).SetRunTimeSetting( _key, _value );

	## Get plugin setting in this order... RunTime > Map > Plugin > Panel > Definitions > Default...
	def GetSetting( self, _key, _default = None ):							return self.GetPluginObject( ).GetSetting( _key, _default );

	## Get Setting string and convert the string to a variable value...
	def GetSettingENUM( self, _key, _default_key = None, _default = None ):	return self.GetPluginObject( ).GetSettingENUM( _key, _default_key, _default );

	def GetPackageJoinedFolder( self, _base, _extra = None ):				return self.GetPluginObject( ).GetPackageJoinedFolder( _base, _extra );
	def GetPanelName( self ):												return self.GetPluginObject( ).GetPanelName( );
	def GetPackageName( self ):												return self.GetPluginObject( ).GetPackageName( );

	## Returns the default syntax highlighter file...
	def GetDefaultSyntaxHighlighter( self ):								return self.GetPluginObject( ).GetDefaultSyntaxHighlighter( );

	## Returns the Package Folder
	def GetPackageFolder( self, _extra = None ):							return self.GetPluginObject( ).GetPackageFolder( _extra );

	## Returns the Package Folder
	def GetPackageUserFolder( self, _extra = None ):						return self.GetPluginObject( ).GetPackageUserFolder( _extra );

	## Returns the Package Folder
	def GetPackageMapsFolder( self, _extra = None ):						return self.GetPluginObject( ).GetPackageMapsFolder( _extra );

	## Returns the Package Folder
	def GetPackageUserMapsFolder( self, _extra = None ):					return self.GetPluginObject( ).GetPackageUserMapsFolder( _extra );

	## Returns the Package Folder
	def GetPackageSettingsFolder( self, _extra = None ):					return self.GetPluginObject( ).GetPackageSettingsFolder( _extra );

	## Returns the Package Folder
	def GetPackageUserSettingsFolder( self, _extra = None ):				return self.GetPluginObject( ).GetPackageUserSettingsFolder( _extra );

	## Returns the Package Folder
	def GetSaveSettingsPath( self, _extra = None ):							return self.GetPluginObject( ).GetSaveSettingsPath( _extra );

	## Returns the Package Folder
	def GetCodeMapUserMapsFolder( self, _extra = None ):					return self.GetPluginObject( ).GetCodeMapUserMapsFolder( _extra );

	## Return the name of the mapping file we use for scratch...
	def GetPanelFileName( self, _extra = None ):							return self.GetPluginObject( ).GetPanelFileName( _extra );

	def Notify( self, _text ):												return self.GetPluginObject( ).Notify( _text );
	def GetMapSyntaxHighlighter( self, _mapper ):							return self.GetPluginObject( ).GetMapSyntaxHighlighter( _mapper );
	def GetMapClass( self, _path, _filename, _ext = 'py' ):					return self.GetPluginObject( ).GetMapClass(  _path, _filename, _ext );
	def IsValidOutputPanel( self, _view ):									return self.GetPluginObject( ).IsValidOutputPanel( _view );


	##
	## Replaceable Helpers
	##
	## Note: This category exists so the custom_mapper can replace these to alter core behavior easily... These are mostly True / False return-type functions...
	##

	## Returns true when in a comment or debugging category type.. This is used as a controlled toggle to ensure parsing occurs in comments and not in non-comment-sections. This allows Notes / Tasks, etc.. items to be located within comments.
	def InCommentDebuggingCategory( self, _category ):						return ( ( self.GetCategoryType( _category ) == CATEGORY_TYPE_IN_COMMENT ) or ( self.GetCategoryType( _category ) == CATEGORY_TYPE_DEBUGGING ) );


	##
	## Translation / Compiling / etc... helpers..
	##

	##
	def GetCategoryEnumeratedName( self, _id ):								return MAP_CATEGORIES_ID.get( _id, 'CATEGORY_UNKNOWN' );
	def GetCategoryIDFromEnumeratedName( self, _category ):					return MAPR_CATEGORIES_ID.get( _category, CATEGORY_UNKNOWN );

	## Return the user-friendly category name - used for category header...
	def GetCategoryName( self, _category ):									return MAP_CATEGORIES.get( _category, CATEGORY_UNKNOWN );

	## Return the Category Type ID
	def GetCategoryType( self, _category ):									return MAP_CATEGORIES_TYPE.get( _category, CATEGORY_TYPE_UNKNOWN );

	## Return the Category Type ID
	def GetCategoryTypeName( self, _category ):								return self.GetCategoryName( _category );

	## Return the Category Description String
	def GetCategoryDesc( self, _category ):									return MAP_CATEGORIES_DESC.get( _category, CATEGORY_DESC_UNKNOWN );

	## Return the Depth String - which is the number of tabs, spaces or whatever the config is set to for chars to multiply when indentation is required...
	def GetDepthString( self, _depth = 0 ):									return self.GetSetting( 'output_indent_chars', '\t' ) * _depth;

	## Simple Helper / Redirect to grab a value from a table of arguments ( for OnFunction/ClassArgument and AccessorExpansion )
	def GetTableValue( self, _args, _args_count, _key, _default ):			return AcecoolLib.Table.GetValue( _args, _args_count, _key, _default );

	##
	def GetSublimeActiveView( self ):										return self.GetSublimeActiveWindow( ).active_view( );

	##
	def GetSublimeActiveWindow( self ):										return sublime.active_window( );

	## Returns the Character Offset from the start of the file to the beginning of the row + num chars to the chosen column
	def GetSublimeViewTextPoint( self, _row = 0, _col = 0 ):				return self.GetMappedFileView( ).text_point( _row - 1, _col );

	## Returns a region from the lines provided
	def GetSublimeViewRegionByLines( self, _line_number, _line_number_2 ):	return self.GetMappedFileView( ).line( sublime.Region( self.GetSublimeViewTextPoint( _line_number ), self.GetSublimeViewTextPoint( _line_number ) ) );

	## Returns the region of a single entire-line
	def GetSublimeViewLineRegion( self, _line_number ):						return sublime.Region( self.GetSublimeViewTextPoint( _line_number ), self.GetSublimeViewTextPoint( _line_number ) );

	## Return the line number from a provided Region Tuple, or from the char value...
	def GetSublimeViewLineNumberByRegion( self, _region ):					return self.GetMappedFileView( ).rowcol( _region.begin( ) )[ 0 ] + 1;


	##
	## Helper - Get the view we're looking for...
	##
	def GetSublimeView( self, _name = 'ACMS - Panel' ):
		## Grab all of the active views for the current window...
		_views = sublime.active_window( ).views( );

		## The view we're targeting
		_view = None;

		## If we're looking for the output panel then see if the plugin has already found it..
		if ( _name == 'ACMS - Panel' ):
			_view_plugin = self.Plugin( ).GetOutputView( );

			if ( AcecoolLib.Sublime.IsValidView( _view_plugin ) ):
				return _view_plugin;

		## Test the first view views

		## Code - Map is typically the LAST view, always...
		_view_last = _views[ len( _views ) - 1 ]
		if ( not AcecoolLib.Sublime.IsValidView( _view ) and AcecoolLib.Sublime.IsValidView( _view_last ) and _view_last.file_name( ).strip( ).endswith( _name ) ):
			return _view_last;

		## First view..
		_view_first = _views[ 0 ]
		if ( not AcecoolLib.Sublime.IsValidView( _view ) and AcecoolLib.Sublime.IsValidView( _view_first ) and _view_first.file_name( ).strip( ).endswith( _name ) ):
			return _view_first;

		## Active View
		_view_active = sublime.active_window( ).active_view( )
		if ( not AcecoolLib.Sublime.IsValidView( _view ) and AcecoolLib.Sublime.IsValidView( _view_active ) and _view_active.file_name( ).strip( ).endswith( _name ) ):
			return _view_active;

		## if the last view is what we're looking for, don't continue..
		if ( not AcecoolLib.Sublime.IsValidView( _view ) ):
			## Let the user know
			## print( 'Suspected Code - Map Panel is incorrent.. loop needed!' );

			## Loop through up to all views - 1 to find the Code - Map Panel if the above one is incorrect... We don't need to look at the last one because we tried that up above...
			for _i in range( len( _views ) - 1 ):
				## Reference the view
				_view = _views[ _i ];

				## If the view is what we we're looking for then we exit the loop so we can return it ( we could return it here too... )
				if ( AcecoolLib.Sublime.GetViewFileName( _view ).endswith( _name ) ) : #_view.file_name( ).strip( ).endswith( _name ) ):
					## Notify
					## print( '>> Code - Map Panel Identified: ID: "' + str( _view.id( ) ) + '" / Name: "' + _view.name( ) + '" / File-Name: "' + _view.file_name( ) + '"' );

					## Exit the loop
					## break;

					## Return from here.. Same as breaking the loop...
					return _view;

		##
		return _view;


	##
	## Helper - For the standard function which executes Pre<Name>( ), On<Name>( ) and Post<Name>( ) - this calls all 3 so the helper only needs to be a single line...
	##
	def CallFunctionHooks( self, _name, *_args ):
		getattr( self, 'PreOn' + _name )( *_args );
		getattr( self, 'On' + _name )( *_args );
		getattr( self, 'PostOn' + _name )( *_args );


	## ##
	## ## Create AccessorFuncs Get/Set<Name> + helpers
	## ##
	## def AccessorFunc( self, _name, _default = None, *_args ):
	## 	return self.AccessorFuncEx( '', _name, _default, *_args );


	## ##
	## ## self.AccessorFunc( 'Bleh', False ) == self.GetCfgBleh( _default_override ) / __.SetCfgBleh( _value ) / __.GetCfgBlehToString( ) / __.ResetCfgBleh( )
	## ##
	## def ConfigAccessorFunc( self, _name, _default = None, *_args ):
	## 	return self.AccessorFuncEx( 'Cfg', _name, _default, *_args );


	## ##
	## ## Sets all AccessorFunc Names provided to the values provided..
	## ##
	## def SetAccessorFuncValues( self, _name, _value, *_args ):
	## 	return self.SetAccessorFuncValuesEx( '', _name, _value, *_args );


	## ##
	## ## Sets all Config AccessorFunc Names provided to the values provided..
	## ##
	## def SetCfgAccessorFuncValues( self, _name, _value, *_args ):
	## 	return self.SetAccessorFuncValuesEx( 'Cfg', _name, _value, *_args );


	## ##
	## ## Resets all AccessorFuncs..
	## ##
	## def ResetAccessorFuncValues( self, _name, *_args ):
	## 	return self.ResetAccessorFuncValuesEx( '', _name, *_args );


	## ##
	## ## Resets all Config AccessorFuncs..
	## ##
	## def ResetCfgAccessorFuncValues( self, _name, *_args ):
	## 	return self.ResetAccessorFuncValuesEx( 'Cfg', _name, *_args );


	##
	## Helper Function used to process VarArgs...
	##
	def ProcessVarArgs( self, _args, _callback, _print_category, _print_text, *_text_args ):
		## Set up out output / return value to a blank-string...
		_output = ''

		## Make sure we have something to process
		if ( _args != None and len( _args ) > 0 ):
			## Process the data...
			for _i, _arg in enumerate( _args ):
				_output = _output + _callback( self, _i, _arg );

		## Convert the _text_args to print-data... _output is always {0}
		_print = [ ];
		_print.append( str( _output ) );
		if ( len( _text_args ) > 0 ):
			for _, _value in enumerate( _text_args ):
				_print.append( str( _value ) );

		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_PROCESS_VARARGS, _print_category, ( _print_text.format( *_print ) ) );

		## Always return something
		return _output;


	##
	## Similar to ProcessVarArgs without text... Simple processor of keyed varargs...
	##
	def ProcessSimpleVarArgs( self, _callback, *_args ):
		## Define the amount of iterations required...
		_varargs = len( _args );

		## If callback is provided and arguments are provided and we have a valid number of arguments...
		if ( AcecoolLib.isfunction( _callback ) and _args != None and _varargs > 0 ):

			## Loop through the varargs...
			for _index in range( _varargs ):
				## Grab the key / value for our index..
				_key = _args[ _index ];

				## Execute the callback with the key / value...
				_callback( self, _key );
		else:
			## Return false if we can't do anything..
			return False;

		## Return true when done...
		return True;


	##
	## Similar to ProcessVarArgs without text... Simple processor of keyed varargs...
	##
	def ProcessKeyVarArgs( self, _callback, *_args ):
		## Define the amount of iterations required...
		_vararg_pairs = math.floor( len( _args ) / 2 );

		## If callback is provided and arguments are provided and we have a valid number of arguments...
		if ( AcecoolLib.isfunction( _callback ) and _args != None and _vararg_pairs > 0 ):

			## Loop through the varargs...
			for _i in range( _vararg_pairs ):
				## Grab the appropriate index for the first element, ie the key...
				_index = ( _i * 2 );

				## Grab the key / value for our index..
				_key = _args[ _index ];
				_value = _args[ _index + 1 ];

				## Execute the callback with the key / value...
				_callback( self, _key, _value );
		else:
			## Return false if we can't do anything..
			return False;

		## Return true when done...
		return True;


	## ##
	## ## Helper - Extra Functionality for setting all AccessorFunc Names provided to the Values provided..
	## ##
	## def AccessorFuncEx( self, _name_prefix, *_args ):
	## 	## Callback - Called for each _key as function name and _value pairing - used to run functions named, provided, with the input provided - single-arg mainly to set AccessorFunc values to their assigned names..
	## 	def Callback( self, _name, _default ):
	## 		_name_get		= 'self.Get' + _name_prefix + _name + '( _default, _skip_defaults )';
	## 		_name_str		= 'self.Get' + _name_prefix + _name + 'ToString( _default )';
	## 		_name_len		= 'self.Get' + _name_prefix + _name + 'Len( _default )';
	## 		_name_lenstr	= 'self.Get' + _name_prefix + _name + 'LenToString( _default )';
	## 		_name_set		= 'self.Set' + _name_prefix + _name + '( _value )';
	## 		_name_reset		= 'self.Reset' + _name_prefix + _name + '( _default )';
	## 		_name_isset		= 'self.Is' + _name_prefix + _name + 'Set( _true_on_default )';

	## 		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
	## 		## self.print( 'AccessorFuncEx', 'Creating functions: self.Get' + _name_prefix + _name + '( _default ) / self.Set' + _name_prefix + _name + '( _value ) / self.Get' + _name_prefix + _name + 'ToString( ) / self.Reset' + _name_prefix + _name + '( )' );
	## 		self.print( 'AccessorFuncEx', 'Creating functions: ' + _name_get + ' / ' + _name_str + ' / ' + _name_len + ' / ' + _name_lenstr + ' / ' + _name_set + ' / ' + _name_reset + ' / ' + _name_isset );

	## 		## Create them...
	## 		## return AcecoolLib.Util.AccessorFunc( self, _name, 				_default, _initialize = True, _debug = False ):
	## 		## return AcecoolLib.Util.AccessorFunc( self, _name_prefix + _name, _default, True, self.GetSetting( 'debugging_mode', False ) );
	## 		return AcecoolLib.Util.AccessorFunc( self, _name, _default, 'Get', None, True, self.GetSetting( 'debugging_mode', False ) );

	## 	## Execute...
	## 	self.ProcessKeyVarArgs( Callback, *_args );


	## ##
	## ## Helper - Extra Functionality for setting all AccessorFunc Names provided to the Values provided..
	## ##
	## def SetAccessorFuncValuesEx( self, _name_prefix, *_args ):
	## 	## Callback - Called for each _key as function name and _value pairing - used to run functions named, provided, with the input provided - single-arg mainly to set AccessorFunc values to their assigned names..
	## 	def Callback( self, _name, _value ):
	## 		## Debugging..
	## 		self.print( 'SetAccessorFuncValuesEx', 'Executing: ' + 'Set' + _name_prefix + _name + '( "' + str( _value ) + '" )' );

	## 		## Execute the Set function for the AccessorFunc Names provided with their associated values...
	## 		return getattr( self, 'Set' + _name_prefix + _name )( _value );

	## 	## Execute...
	## 	self.ProcessKeyVarArgs( Callback, *_args );


	## ##
	## ## Helper - Extra Functionality for setting all AccessorFunc Names provided to the Values provided..
	## ##
	## def ResetAccessorFuncValuesEx( self, _name_prefix, *_args ):
	## 	## Callback - Called for each _key as function name and _value pairing - used to run functions named, provided, with the input provided - single-arg mainly to set AccessorFunc values to their assigned names..
	## 	def Callback( self, _name ):
	## 		## Debugging..
	## 		self.print( 'SetAccessorFuncValuesEx', 'Executing: ' + 'Reset' + _name_prefix + _name + '( )' );

	## 		## Execute the Set function for the AccessorFunc Names provided with their associated values...
	## 		return getattr( self, 'Reset' + _name_prefix + _name )( );

	## 	## Execute...
	## 	self.ProcessSimpleVarArgs( Callback, *_args );




##
## This is used for each entry... Category, parent category, etc.. is stored here... along with output text, and the region mapping
## ( local - with local to world conversion for char-location when the data is actually output it is updated ) for everything... on hover, on click, on double-click, etc...
## Note: for PreAddEntry or so - entry is passed in and that's the callback used to apply regioin mapping ( default is entire line is clickable to go to line, but other segments can be replaced and take precedent over that feature )
##
class ACMSEntry( ):
	##
	##
	##
	maps = { }


	##
	##
	##
	def __init__( self ):
		self.region = True; # sublime.Region( );

		## ##
		## self.SetupAccessorFunc( 'LineCode', None, 'Get', None, True, True );
		## self.SetupAccessorFunc( 'StrippedLineCode', None, 'Get', None, True, True );
		## self.SetupAccessorFunc( 'LineDepth', None, 'Get', None, True, True );
		## self.SetupAccessorFunc( 'LineNumber', None, 'Get', None, True, True );
		## self.SetupAccessorFunc( 'LineCharStart', None, 'Get', None, True, True );
		## self.SetupAccessorFunc( 'LineCharEnd', None, 'Get', None, True, True );

		## self.SetupAccessorFunc( 'Category', None, 'Get', None, True, True );
		## self.SetupAccessorFunc( 'CategoryParent', None, 'Get', None, True, True );
		## self.SetupAccessorFunc( 'CategoryParent', None, 'Get', None, True, True );

		## ##
		## self.SetupAccessorFunc( 'HasChanged', None, '', None, True, True );
		## self.SetupAccessorFunc( 'SourceFileName', None, 'Get', None, True, True );
		## self.SetupAccessorFunc( 'SourceFileExt', None, 'Get', None, True, True );


	##
	## Helper - Used to add the region maps... By default each entry map links to the line... but other types can be used such as hover, on click, etc... so instead of linking to line, you can link to snippets, bookmarks, macros, websites, etc...
	## Note:
	##
	def AddRegionMap( self, _local_char_start, _local_char_end, _region_mapper ):
		self.maps = _region_mapper;




##
## Data Point Object- This is to be used for callbacks instead of arguments to simplify tasks...
##
class ACMSDataPoint( ):
	##
	##
	##
	def __init__( self, _acms = None, **_varargs ):
		## Internal Data for ALL Data stored for the object ( children and parents too ) for all data, AccessorFunc Data / Info, etc... Everything.. Easy to copy to new object....
		## self.this_data = {
		## 	'accessorfunc': {
		## 		'data': { },
		## 		'names': { },
		## 	},
		## };

		## Call the __init__ function of inherited classes...
		super( ).__init__( **_varargs );

		## ## View / File, the View ID, the Views Index and the GroupID...
		## self.SetupAccessorFunc( 'OutputView', None, 'Get', None, True, True );
		## self.SetupAccessorFunc( 'OutputViewID', None, 'Get', None, True, True );
		## self.SetupAccessorFunc( 'OutputViewIndex', None, 'Get', None, True, True );
		## self.SetupAccessorFunc( 'OutputGroupID', None, 'Get', None, True, True );

		## ## The file we need to map - this gets set when we activate a file - if not ACMS - Panel, or Code - Map, etc... and has an extension... set it so we know what to map...
		## self.SetupAccessorFunc( 'SourceView', None, 'Get', None, True, True );
		## self.SetupAccessorFunc( 'SourceFileName', None, 'Get', None, True, True );
		## self.SetupAccessorFunc( 'SourceFileExt', None, 'Get', None, True, True );


	def __str__( ):
		_text = '';

		return self.GetAccessorFuncSupportObjectToString( );




##
## This is used for each entry... Category, parent category, etc.. is stored here... along with output text, and the region mapping
## ( local - with local to world conversion for char-location when the data is actually output it is updated ) for everything... on hover, on click, on double-click, etc...
## Note: for PreAddEntry or so - entry is passed in and that's the callback used to apply regioin mapping ( default is entire line is clickable to go to line, but other segments can be replaced and take precedent over that feature )
##
class ACMSFileLineChunk( ):
	##
	##
	##
	maps = { };


	##
	##
	##
	def __init__( self ):
		self.region = True; # sublime.Region( );

		## ##
		## self.SetupAccessorFunc( 'LineCode', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'LineCodeSearchable', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'LineCodeStripped', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'LineCodeStrippedLeft', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'LineCodeStrippedRight', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'LineDepth', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'LineNumber', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'LineCharStart', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'LineCharEnd', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'IsLineCommented', None, '', None, True, True )
		## self.SetupAccessorFunc( 'LineInComment', None, '', None, True, True )
		## self.SetupAccessorFunc( 'LineInCommentBlock', None, '', None, True, True )


		## self.SetupAccessorFunc( 'FileLines', None, 'Get', None, True, True )

		## self.SetupAccessorFunc( 'Category', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'CategoryParent', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'CategoryParent', None, 'Get', None, True, True )

		## ##
		## self.SetupAccessorFunc( 'HasChanged', None, '', None, True, True )
		## self.SetupAccessorFunc( 'SourceFileName', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'SourceFileExt', None, 'Get', None, True, True )


	##
	## Helper - Used to add the region maps... By default each entry map links to the line... but other types can be used such as hover, on click, etc... so instead of linking to line, you can link to snippets, bookmarks, macros, websites, etc...
	## Note:
	##
	def AddRegionMap( self, _local_char_start, _local_char_end, _region_mapper ):
		self.maps = _region_mapper;
















































## ##
## ##
## ##
## class CacheDatabase( AcecoolLib.Database ):
## 	##
## 	##
## 	##
## 	def __init__( self ):
## 		super( ).__init__( 'Acecool', os.path.join( sublime.packages_path( ), 'User' ) + '/' )

## 		## Create table
## 		self.CreateTableIfNotExists( 'cache', 'filename text, data text, timestamp text' )


## 	##
## 	##
## 	##
## 	def FetchCachedFile( self, _file, _timestamp = 0 ):
## 		print( '[ ACMS.DB ] FetchCachedFile' )
## 		## return self.Fetch( 'SELECT data FROM cache WHERE filename="' + _file + '" LIMIT 1' )
## 		## return self.Fetch( 'cache', 'filename=?', ( _file, ) )
## 		return self.Fetch( 'cache', 'filename=?', ( _file, ) )


## 	##
## 	##
## 	##
## 	def HasCachedFile( self, _file, _timestamp = 0 ):
## 		print( '[ ACMS.DB ] HasCachedFile' )
## 		## return self.Fetch( 'SELECT cache, timestamp FROM cache WHERE filename="' + _file + '" LIMIT 1' )
## 		_data = self.FetchCachedFile( _file )

## 		print( _data )
## 		return True


## 	##
## 	##
## 	##
## 	def SaveFileCache( self, _file, _data = '', _timestamp = 0 ):
## 		print( '[ ACMS.DB ] SaveFileCache' )

## 		## return self.Update( self, _table, _where, _where_data, _columns = 1 )
## 		## Update( self, _table, _where, _where_data, _values, _columns = 1 )
## 		## return self.Update( 'data', 'filename', _file, ( _file, _data, '12345' ), 3 )

## 		return self.Update( 'cache', 'filename = ?, data = ?, timestamp = ?', ( _file, _data, '12345' ) )

## 		## return self.Update( 'data', 'filename = "' + _file + '", data = "' + _data + '", timestamp = "' + str( 0 ) + '"' )





















##
##
##
class AcecoolCodeMappingSystem( AcecoolCodeMappingSystemBaseHelpers ): #AcecoolAccessorFuncSupport,
	##
	## Important Object Values!
	##

	## Name / ID of the XCodeMapperBase
	__name__ = 'AcecoolCodeMappingSystem';
	__panel__ = None;


	##
	## AccessorFuncs
	##
	## __Name		= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'Name',		name = 'Name',		default = 'AcecoolCodeMappingSystemBase',	getter_prefix = 'Get',			documentation = 'Name Docs',		allowed_types = ( TYPE_STRING ),					allowed_values = VALUE_ANY,					allow_erroneous_default = False,	options = { } );
	## __x			= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'x',			name = 'X',			default = 1111,								getter_prefix = 'Get',			documentation = 'X Docs',			allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),		allowed_values = VALUE_ANY,					allow_erroneous_default = False,	options = { } );
	## __y			= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'y',			name = 'Y',			default = 2222,								getter_prefix = 'Get',			documentation = 'Y Docs',			allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),		allowed_values = VALUE_ANY,					allow_erroneous_default = False,	options = { } );
	## __z			= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'z',			name = 'Z',			default = 3333,								getter_prefix = 'Get',			documentation = 'Z Docs',			allowed_types = ( TYPE_INTEGER, TYPE_FLOAT ),		allowed_values = VALUE_ANY,					allow_erroneous_default = False,	options = { } );
	## __Blah		= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'Blah',		name = 'Blah',		default = str( TYPE_INTEGER ),				getter_prefix = 'Get',			documentation = 'Blah Docs',		allowed_types = TYPE_STRING,						allowed_values = VALUE_ANY,					allow_erroneous_default = False,	options = { } );
	## __Width		= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'Width',		name = 'Width',		default = 1,								getter_prefix = 'Get',			documentation = 'Width Docs',		allowed_types = ( TYPE_INTEGER, TYPE_BOOLEAN ),		allowed_values = VALUE_ANY,					allow_erroneous_default = False,	options = { } );
	## __Height	= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'Height',		name = 'Height',	default = 0,								getter_prefix = 'Get',			documentation = 'Height Docs',		allowed_types = TYPE_INTEGER,						allowed_values = VALUE_SINGLE_DIGITS,		allow_erroneous_default = False,	options = { } );
	## __Depth		= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'Depth',		name = 'Depth',		default = 2,								getter_prefix = 'Get',			documentation = 'Depth Docs',		allowed_types = TYPE_ANY,							allowed_values = VALUE_SINGLE_DIGITS,		allow_erroneous_default = False,	options = { } );

	## __InLanguage						= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'lang',						name = 'Language',						default = 'AcecoolCodeMappingSystemBase',	getter_prefix = 'In',			documentation = 'Name Docs',		allowed_types = ( TYPE_STRING ),					allowed_values = VALUE_ANY,																					allow_erroneous_default = False,	options = { } );
	## __ActiveLanguage					= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'active_lang',				name = 'ActiveLanguage',				default = 'AcecoolCodeMappingSystemBase',	getter_prefix = 'Get',			documentation = 'Name Docs',		allowed_types = ( TYPE_STRING ),					allowed_values = VALUE_ANY,																					allow_erroneous_default = False,	options = { } );
	## __FileData						= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'file_data',					name = 'FileData',						default = CONST_DATA_NONE,					getter_prefix = 'Get',			documentation = 'Name Docs',		allowed_types = ( TYPE_STRING ),					allowed_values = ( CONST_DATA_NONE ),																		allow_erroneous_default = False,	options = { } );
	## __FileLines						= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'file_lines',					name = 'FileLines',						default = 'AcecoolCodeMappingSystemBase',	getter_prefix = 'Get',			documentation = 'Name Docs',		allowed_types = ( TYPE_INTEGER ),					allowed_values = VALUE_ANY,																					allow_erroneous_default = False,	options = { } );
	## __ActiveClass					= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'active_class',				name = 'ActiveClass',					default = 'AcecoolCodeMappingSystemBase',	getter_prefix = 'Get',			documentation = 'Name Docs',		allowed_types = ( TYPE_STRING ),					allowed_values = VALUE_ANY,																					allow_erroneous_default = False,	options = { } );
	## __ActiveClassDepth				= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'active_class_depth',			name = 'ActiveClassDepth',				default = 'AcecoolCodeMappingSystemBase',	getter_prefix = 'Get',			documentation = 'Name Docs',		allowed_types = ( TYPE_INTEGER ),					allowed_values = VALUE_ANY,																					allow_erroneous_default = False,	options = { } );
	## __ClassStack						= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'class_stack',				name = 'ClassStack',					default = 'AcecoolCodeMappingSystemBase',	getter_prefix = 'Get',			documentation = 'Name Docs',		allowed_types = ( TYPE_LIST ),						allowed_values = VALUE_ANY,																					allow_erroneous_default = False,	options = { } );
	## __InComment						= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'in_comment',					name = 'Comment',						default = 'AcecoolCodeMappingSystemBase',	getter_prefix = 'In',			documentation = 'Name Docs',		allowed_types = ( TYPE_BOOLEAN ),					allowed_values = VALUE_ANY,																					allow_erroneous_default = False,	options = { } );
	## __InCommentBlock					= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'in_comment_block',			name = 'CommentBlock',					default = 'AcecoolCodeMappingSystemBase',	getter_prefix = 'In',			documentation = 'Name Docs',		allowed_types = ( TYPE_BOOLEAN ),					allowed_values = VALUE_ANY,																					allow_erroneous_default = False,	options = { } );
	## __CommentBlockStartLine			= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'comment_block_line_start',	name = 'CommentBlockStartLine',			default = 'AcecoolCodeMappingSystemBase',	getter_prefix = 'Get',			documentation = 'Name Docs',		allowed_types = ( TYPE_INTEGER ),					allowed_values = VALUE_ANY,																					allow_erroneous_default = False,	options = { } );
	## __CommentBlockEndLine			= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'comment_block_line_end',		name = 'CommentBlockEndLine',			default = 'AcecoolCodeMappingSystemBase',	getter_prefix = 'Get',			documentation = 'Name Docs',		allowed_types = ( TYPE_INTEGER ),					allowed_values = VALUE_ANY,																					allow_erroneous_default = False,	options = { } );
	## __CommentBlockIndexStart			qdwwwwwwwww= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'comment_block_index_start',	name = 'CommentBlockIndexStart',		default = 'AcecoolCodeMappingSystemBase',	getter_prefix = 'Get',			documentation = 'Name Docs',		allowed_types = ( TYPE_INTEGER ),					allowed_values = VALUE_ANY,																					allow_erroneous_default = False,	options = { } );
	## __CommentBlockIndexEnd			= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'comment_block_index_end',	name = 'CommentBlockIndexEnd',			default = 'AcecoolCodeMappingSystemBase',	getter_prefix = 'Get',			documentation = 'Name Docs',		allowed_types = ( TYPE_INTEGER ),					allowed_values = VALUE_ANY,																					allow_erroneous_default = False,	options = { } );
	## __Depth							= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'depth',						name = 'Depth',							default = 'AcecoolCodeMappingSystemBase',	getter_prefix = 'Get',			documentation = 'Name Docs',		allowed_types = ( TYPE_INTEGER ),					allowed_values = VALUE_ANY,																					allow_erroneous_default = False,	options = { } );
	## __LastDepth						= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'last_depth',					name = 'LastDepth',						default = 'AcecoolCodeMappingSystemBase',	getter_prefix = 'Get',			documentation = 'Name Docs',		allowed_types = ( TYPE_INTEGER ),					allowed_values = VALUE_ANY,																					allow_erroneous_default = False,	options = { } );
	## __IndentLength					= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'indent_length',				name = 'IndentLength',					default = 'AcecoolCodeMappingSystemBase',	getter_prefix = 'Get',			documentation = 'Name Docs',		allowed_types = ( TYPE_INTEGER ),					allowed_values = VALUE_ANY,																					allow_erroneous_default = False,	options = { } );
	## __Realm							= AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'realm',						name = 'Realm',							default = REALM_UNKNOWN,					getter_prefix = 'Get',			documentation = 'Name Docs',		allowed_types = None,								allowed_values = ( REALM_UNKNOWN, REALM_SHARED, REALM_CLIENT, REALM_SERVER, REALM_NOLOAD ),					allow_erroneous_default = False,	options = { } );

	##
	__process_text						= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'process_text',				name = 'ProcessText',					default = '',								getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_STRING ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'Output Processing Text for notification or print statement on a single line, or formatting to multiple'							);


	##
	__lang								= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'lang',						name = 'InLanguage',					default = 'ACMS_LANG_UNKNOWN',				getter_prefix = '',				allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'Language Docs'										);
	__active_lang						= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'active_lang',				name = 'ActiveLanguage',				default = 'Default',						getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'ActiveLanguage Docs'								);
	__active_lang_name					= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'active_lang_name',			name = 'ActiveLanguageName',			default = 'Default',						getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'ActiveLanguage Docs'								);
	__file_data							= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'file_data',					name = 'FileData',						default = CONST_DATA_NONE,					getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'FileData Docs'										);
	__file_lines						= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'file_lines',					name = 'FileLines',						default = 0,								getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'FileLines Docs'									);
	__active_class						= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'active_class',				name = 'ActiveClass',					default = '',								getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'ActiveClass Docs'									);
	__active_class_depth				= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'active_class_depth',			name = 'ActiveClassDepth',				default = -1,								getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'ActiveClassDepth Docs'								);
	__class_stack						= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'class_stack',				name = 'ClassStack',					default = [ ],								getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'ClassStack Docs'									);
	__in_comment						= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'in_comment',					name = 'InComment',						default = False,							getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'InComment Docs'									);
	__in_comment_block					= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'in_comment_block',			name = 'InCommentBlock',				default = False,							getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'InCommentBlock Docs'								);
	__comment_block_line_start			= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'comment_block_line_start',	name = 'CommentBlockStartLine',			default = -1,								getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'CommentBlockStartLine Docs'						);
	__comment_block_line_end			= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'comment_block_line_end',		name = 'CommentBlockEndLine',			default = -1,								getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'CommentBlockEndLine Docs'							);
	__comment_block_index_start			= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'comment_block_index_start',	name = 'CommentBlockIndexStart',		default = -1,								getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'CommentBlockIndexStart Docs'						);
	__comment_block_index_end			= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'comment_block_index_end',	name = 'CommentBlockIndexEnd',			default = -1,								getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'CommentBlockIndexEnd Docs'							);
	__in_string							= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'in_string',					name = 'InString',						default = False,							getter_prefix = '',				allowed_types = ( AcecoolLib.TYPE_BOOLEAN ),				allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'InString Docs'										);
	__in_string_block					= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'in_string_block',			name = 'InStringBlock',					default = False,							getter_prefix = '',				allowed_types = ( AcecoolLib.TYPE_BOOLEAN ),				allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'InStringBlock Docs'								);
	__string_block_line_start			= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'string_block_line_start',	name = 'StringBlockStartLine',			default = -1,								getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'StringBlockStartLine Docs'							);
	__string_block_line_end				= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'string_block_line_end',		name = 'StringBlockEndLine',			default = -1,								getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'StringBlockEndLine Docs'							);
	__string_block_index_start			= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'string_block_index_start',	name = 'StringBlockIndexStart',			default = -1,								getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'StringBlockIndexStart Docs'						);
	__string_block_index_end			= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'string_block_index_end',		name = 'StringBlockIndexEnd',			default = -1,								getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'StringBlockIndexEnd Docs'							);
	__depth								= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'depth',						name = 'Depth',							default = -1,								getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'Depth Docs'										);
	__last_depth						= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'last_depth',					name = 'LastDepth',						default = -1,								getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'LastDepth Docs'									);
	__indent_length						= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'indent_length',				name = 'IndentLength',					default = -1,								getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY ),														documentation = 'IndentLength Docs'									);
	__realm								= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'realm',						name = 'Realm',							default = REALM_UNKNOWN,					getter_prefix = 'Get',			allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( REALM_UNKNOWN, REALM_SHARED, REALM_CLIENT, REALM_SERVER, REALM_NOLOAD ),		documentation = 'Realm Docs'										);


	## Serialize turns this object into a human readable format - Note: There will be a serialize function in AccessorFunc in the future where you can easily define separators, prefix, suffix, etc..
	__is_in_comment						= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'is_in_comment',				name = 'IsInComment',					default = None,								getter_prefix = '',				documentation = 'Returns whether or we are in a Comment Block',				setup = { 'get': ( lambda this: this.GetInComment( ) or this.GetInCommentBlock( ) ) }															);
	__is_in_string						= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemBase,		key = 'is_in_string',				name = 'IsInString',					default = None,								getter_prefix = '',				documentation = 'Returns whether or we are in a String Block',				setup = { 'get': ( lambda this: this.InString( ) or this.InStringBlock( ) ) }															);

	## ##
	## def __str__( self ):
	## 	_text = ''

	## 	_text += self.GetNameKeyOutput( 'Name', True, None )
	## 	_text += self.GetXKeyOutput( 'x', None, None )
	## 	_text += self.GetYKeyOutput( 'y', None, None )
	## 	_text += self.GetZKeyOutput( 'z', None, None )
	## 	_text += self.GetBlahKeyOutput( 'Blah', None, None )
	## 	_text += self.GetWidthKeyOutput( 'Width', None, None )
	## 	_text += self.GetDepthKeyOutput( 'Height', None, None )
	## 	_text += self.GetHeightKeyOutput( 'Depth', False, True )
	## 	_text += '\n'

	## 	_default_override = None
	## 	_ignore_defaults = False
	## 	_text += String.FormatSimpleColumn( 150,	'------------------------------------------------------------------------------------------------------------------------------------------------------' ) + '\n'
	## 	_text += self.GetNameGetterOutput( 'Name', _default_override, _ignore_defaults ) + '\n'
	## 	_text += self.GetXGetterOutput( 'X', _default_override, _ignore_defaults ) + '\n'
	## 	_text += self.GetYGetterOutput( 'Y', _default_override, _ignore_defaults ) + '\n'
	## 	_text += self.GetZGetterOutput( 'Z', _default_override, _ignore_defaults ) + '\n'
	## 	_text += self.GetBlahGetterOutput( 'Blah', _default_override, _ignore_defaults ) + '\n'
	## 	_text += self.GetWidthGetterOutput( 'Width', _default_override, _ignore_defaults ) + '\n'
	## 	_text += self.GetDepthGetterOutput( 'Depth', _default_override, _ignore_defaults ) + '\n'
	## 	_text += self.GetHeightGetterOutput( 'Height', _default_override, _ignore_defaults ) + '\n'

	## 	return _text




##
## XCodeMapper - Universal CodeMap extension to easily add languages and support for your projects.. - Josh 'Acecool' Moser
## Note: I am rebuilding it here under a different name to ensure it works properly without making changes to the current variant.. And so I can extract things in favor of using a RunTime object so this behaves more like a simple library of functions..
##
class XCodeMapperBase( AcecoolCodeMappingSystem ):
	##
	## Important Object Values!
	##

	## Name / ID of the XCodeMapperBase
	__name__ = 'XCodeMapperBase';
	__panel__ = None;



	##
	## PreProcessor - This is added so common mistakes / syntax errors, etc.. can be detected on each line using a new TypoFixEntry system...
	##
	def ProcessLineEx( self, _line_object ):
		##
		## Process the typo list.... If there are entries...
		##
		if ( len( self.typo_list ) > 0 ):
			##
			for _pattern, _error, _solution, in self.typo_list:
				## If a typo was found on the line, then notate it... Also, make sure it wasn't on a self.AddTypoFixEntry line...
				if ( not _code.startswith( 'self.AddTypoFixEntry' ) and _code.find( _pattern ) >= 0 ):
					## Add the error to the errors category
					self.AddError( 'Typo: ' + _error + ' is a common typo and doesn\'t exist - use ' + _solution + ' instead!', _line_number );

					## Modify the output code-line by adding TYPO to it...
					_code = _code + ' [ TYPO ]';


		## Pass-Through to our Callback...
		return self.OnProcessLineEx( _line_object );


	##
	## Callback - Called for each line in the file - can also prevent entries from being added ( when found by AddSyntax method ) by returning a boolean. Can also alter entries code ( by returning a string - will only alter if the code being returned was going to be added to a category - otherwise the line is ignored anyway )
	## Note: This is a simple callback so basic info is available only such as the line-number, the code for that line, and the depth of any contents of that line. It is called regardless of comment-status so comment-status is also provided..
	##
	## Task:
	##
	def OnProcessLineEx( self, _line_object ):
		pass;


	##
	## Note: Update - give a better and more accurate name... Update to return false when the comment is ( A line comment which doesn't start at the beginning of the line... OR if a block-comment is used but doesn't take up the entire line... Ie it begins before the line and ends somewhere on the line with the line containing more code... It starts anywhere on the line and ends on the line with code left-over... Or it starts anywhere on the line and ends elsewhere as long as code is left over after stripping it )
	##
	def LineContainsComment( self, _category ):
		return not ( ( not self.InCommentDebuggingCategory( _category ) ) and ( ( not self.GetInCommentBlock( ) and not self.GetInComment( ) ) or ( self.GetInCommentBlock( ) and self.GetCommentBlockStartLine( ) == self.GetCommentBlockEndLine( ) and self.GetCommentBlockIndexStart( ) > 0 ) ) );


	##
	##
	##
	def LineContainsStringBlock( self, _category ):
		return ( self.InStringBlock( ) or self.InString( ) );


	##
	## Return True or False depending on whether or not the line contains data we're looking for...
	##
	def LineContainsWatchedToken( self, _search_mode, _pattern, _code ):
		##
		_bFoundRegEx			= ( _search_mode == SEARCH_TYPE_REGEX and re.search( _pattern, _code ) );

		##
		_bFoundStartsEndsWith	= ( ( _search_mode == SEARCH_TYPE_STARTSWITH and _code.startswith( _pattern ) ) or ( _search_mode == SEARCH_TYPE_ENDSWITH and _code.endswith( _pattern ) ) );

		##
		_bFoundContains			= ( ( _search_mode == SEARCH_TYPE_CONTAINS and AcecoolLib.String.IndexOfExists( _pattern, _code, False ) ) or ( _search_mode == SEARCH_TYPE_CONTAINSI and AcecoolLib.String.IndexOfExists( _pattern, _code, True ) ) );

		##
		return ( _bFoundRegEx or _bFoundStartsEndsWith or _bFoundContains );


	##
	## Internal Handler / Hook Caller
	## Task: Split Run into several helper functions to make reprocessing a line easier...
	##
	def Run( self, _file, _variant, _force_nocache_remap = False ):
		## If a DeveloperFunc exists, specifically designed to run prior to everything else, run it...
		if ( AcecoolLib.isfunction( self.OnRunDeveloperFunc ) ):
			self.OnRunDeveloperFunc( );


		##
		## self.AddProcessText( File: ' + AcecoolLib.File.GetName( _file, False ) + ' -> ' );

		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		## self.print( ACMS_PRINT_TYPE_RUN, 'Run', '[ XCodeMapperBase > ' + self.GetSetting( 'active_language', 'None' ) + ' > Run ] ----------------------------------------------------------------------------------------------------------------------------' );


		## Note: For some reason AcecoolLib.TernaryFunc is having a problem because of functions being called inline despite not being accessed... Quite annoying...
		## print( '[ XCodeMapper > Run > Specific Mapping Variant Chosen: ' + GetLangVariantClassName( _variant ) + ' ]' );

		## Let them know...
		## self.print( ACMS_PRINT_TYPE_RUN, 'Run', '[ XCodeMapper > Run > Specific Mapping Variant Chosen: ' + self.GetLangVariantName( _variant ) + ' ]' );


		##
		## Get the current file view...
		##
		_file_view = self.GetSublimeView( _file );
		self.__mapped_file_panel__ = _file_view;

		self.AddProcessText( 'Run -> Window: ' + AcecoolLib.String.FormatColumn( 3, str( _file_view.window( ).id( ) ) ) + ' -> View: ' + AcecoolLib.String.FormatColumn( 5, str( _file_view.id( ) ) ) + ' -> File: ' + AcecoolLib.String.FormatColumn( 50, str( AcecoolLib.Sublime.GetViewFileName( _file_view, False ) ) ) + ' -> ' );

		##
		## self.database = CacheDatabase( )


		##
		_plugin, _database			= GetPluginNode( ).PluginDatabase( );


		## ##
		## _plugin = None;
		## _database = None;

		## ##
		## if ( ACMS( ).UseMappingThreads( ) ):
		## 	_plugin														= GetPluginNode( ).GetThreadedPlugin( );
		## 	_database													= GetPluginNode( ).GetThreadedDatabase( );
		## else:
		## 	_plugin														= GetPluginNode( ).GetThreadedPlugin( );
		## 	_database													= GetPluginNode( ).GetThreadedDatabase( );


		##
		## Caching System - If enabled, save the file...
		##
		if ( not _force_nocache_remap and self.GetSetting( 'use_caching_system', False ) ):

			## _data_cached = str( self.database.FetchCachedFile( _file ) );
			## return _data_cached;

			## Grab the latest core files timestamp
			## _core_time = self.GetLatestCoreTimestamp( );

			## Make sure the file timestamp is higher than the core timestamps
			## if ( _row_time > _core_time ):
			## 	return True, _row_data, _row_map, _row_time, _core_time;
			## Grab the row data for the file - if we return True we return other data too...
			_has_cache, _row_data, _row_map, _row_time, _core_time = _database.HasCache( _file ); #self.GetPluginDatabase( ).HasCache( _file )

			## Determine if cached exists, etc...
			## if ( self.GetPluginObject( ).ShouldLoadCachedFile( _file ) ):
			if ( _has_cache ):
				## Load the file before so we get a more accurate tally of time...
				## _data_cached = self.GetPluginObject( ).LoadFileCache( _file );

				## Output.. To show how long / short it takes to execute - as of January 21, 2018 a Python file of around 5000 lines takes 0.01 to 0.02 seconds to load the file ( SSD ) and completely finish processing anything to the point of this return ( which then outputs the data ).
				## print( ' >> Execution Time took a total of: ' + str( self.GetTimeDelta( ) ) );
				## self.print( ACMS_PRINT_TYPE_CACHE, 'Run', ' >Cache Execution Time: ' + str( self.__timekeeper__ ) );

				## Return the cached data, if the data isn't blank...
				if ( _row_data != '' and _row_data != '{}' ):
					self.AddProcessText( 'Loading Cache -> ' + str( self.__timekeeper__ ) );
					##
					## self.print( ACMS_PRINT_TYPE_CACHE, 'Run', ' >> Returning Cached Data >> Cache Execution Time: ' + str( self.__timekeeper__ ) )
					self.print( ACMS_PRINT_TYPE_RUN, self.GetProcessText( ) );

					##
					return _row_data;
			else:
				self.AddProcessText( 'No Cache -> ' );
		else:
			self.AddProcessText( 'Mapping File -> ' );
				## else:
				## 	self.print( ACMS_PRINT_TYPE_CACHE, 'Run', ' >> Cached Data Is NOT Defined >> Execution Time: ' + str( self.__timekeeper__ ) );

				## else:
				## 	##
				## 	print( ' >> Cached Data is not defined... Row Data :: ' + str( _row_data ) );
				## 	print( ' >> Cached Data is not defined... Row Map  :: ' + str( _row_map ) );


		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		## self.print( ACMS_PRINT_TYPE_RUN, 'Run' );

		## Task: Consider adding PreOnRun / OnRun / PostOnRun Helpers
		## self.CallFunctionHooks( 'Run' );

		## Reset the values of our internal AccessorFuncs
		self.OnResetInternalAccessors( );

		##
		_name = AcecoolLib.File.GetName( _file );
		_ext = AcecoolLib.File.GetExt( _file );


		## Read the code / file into memory - exploded on newline
		_lines, _count = AcecoolLib.File.Read( _file, self.GetSetting( 'file_new_line_chars', '\n' ) );

		## Update some helper Accessors so we can use them in certain situations...
		self.SetFileData( _lines );
		self.SetFileLines( _count );

		## Process symbols for the file...
		if ( _file_view != None ):
			## _window.extract_variables( );

			self.MappingFileSymbols( _file_view, _file_view.symbols( ) );

		## For Each Line...
		for _line_number, _line in enumerate( _lines, 1 ):
			##
			_code = _line.strip( );
			_code_search = _code;
			_code_lstrip = _line.lstrip( );
			_code_rstrip = _line.rstrip( );

			## Grab the difference between a left-stripped bit of code and non-stripped to count how many spaces or tabs there are...
			_depth = len( _line ) - len( _code_lstrip )

			## Determine Indentation Character Type and Quantity - if tabs, then set indent-length to the value to 1 so no division occurs - if spaces are used then set to number of spaces used so we can use that to divide to ensure accurate indentation occurs..
			## If they're using spaces, we divide _depth by the tabs-to-spaces value ( default 4 - however if they're using 2 spaces per indent we catch and log it by setting a value the FIRST time indents are seen )
			if ( _depth > 0 and self.GetIndentLength( ) == 0 ):
				## Grab the indent chars
				_indent_chars = AcecoolLib.String.Strip( _line, _depth, len( _line ) - 1 )

				## Debugging...
				self.print( ACMS_PRINT_TYPE_RUN_INDENTATION, 'Indentation', ' >> Depth: ' + str( _depth ) + ' / using chars: "' + _indent_chars + '"' );

				## Determine if spaces
				if ( _indent_chars == AcecoolLib.String.repeat( ' ', _depth ) ):
					self.print( ACMS_PRINT_TYPE_RUN_INDENTATION, ' >> Syntax > Indentation > Using ' + str( _depth ) + '	spaces per Indentation!' );
					self.SetIndentLength( _depth );
				## Determine if Tabs
				elif ( _indent_chars == AcecoolLib.String.repeat( '	', _depth ) ):
					self.print( ACMS_PRINT_TYPE_RUN_INDENTATION, ' >> Syntax > Indentation > Using ' + str( _depth ) + ' tabs per indentation!' );
					self.SetIndentLength( 1 );

			## If IndentLength has been set, and it is over 0, then use it to divide...
			if ( self.GetIndentLength( ) > 0 ):
				## Set the depth by using the depth provided and dividing by the number of spaces, if spaces are used for indentation, or by 1 for tabs ie nothing...
				self.SetDepth( int( _depth / self.GetIndentLength( ) ) );
			else:
				self.SetDepth( _depth );

			## Are we in a comment block - note, this can't be in the line by line scope as blocks can extend beyond a single line...
			self.ResetInComment( );


			##
			## TODO: Add comment checks out here... Then if we are supposed to look at data within comment blocks we go deeper... As of right now OnProcessLine isn't receiving the right in comment / in commentblock vars... They're from the previous line..
			##
			## Task: Process comments separately.. and before the other data so we can skip other syntax rules if data is found... meaning specific rules need to be moved if added to comment lines - the beginning of the rule system... It'll make the code quicker though!
			##
			## If we have comment rules...
			if ( self.data_syntax_comments != None and len( self.data_syntax_comments ) > 0 ):
				## Process comments...
				for _category, _search_mode, _pattern, in self.data_syntax_comments:
					##
					## If we're processing our comment patterns, check to see if the line starts with a comment...
					##
					if ( _category == CATEGORY_COMMENT and not self.GetInCommentBlock( ) and not self.GetInComment( ) ):
						## If our comment is at the start of the line then
						if _code.startswith( _pattern ):
							## Mark that we're in a 1 line comment
							self.SetInComment( True );

							## Strip the comment... Since it starts with we have a different problem....
							_code_search = AcecoolLib.String.Strip( _code_search, 0, len( _pattern ) );
							_code_search = _code_search.strip( );

					##
					## If we're in a comment block, set our controlled toggle to true ( also set _in_comment to true so TODO / Tasks / etc.. can be picked up in comment blocks )
					##
					if ( _category == CATEGORY_COMMENT_BLOCK_BEGIN and AcecoolLib.String.IndexOfExists( _pattern, _code_search, True ) and not self.GetInCommentBlock( ) ):
						## Let the code know we're in a comment and in a comment-block
						self.SetInComment( True );
						self.SetInCommentBlock( True );

						## Let the code know which line the comment block is on ( this is so we can strip it from the same line code output if necessary - as of right now a comment block at the end of the line will prevent code being output )
						self.SetCommentBlockStartLine( _line_number );
						self.SetCommentBlockEndLine( -1 );

						## The index where the comment block started
						self.SetCommentBlockIndexStart( AcecoolLib.String.IndexOf( _pattern, _code_search ) - 1 );

						##
						self.AddEntry( CATEGORY_DEBUGGING, _line_number, 'Comment Block Start on Line: ' + str( _line_number ), _depth, _search_mode, _pattern );

						## Strip the comment block starting code from the line - will advance this later so it searches until the end if on the same line...
						if ( self.GetCommentBlockIndexStart( ) > 0 ):
							_code_search = AcecoolLib.String.Strip( _code_search, AcecoolLib.String.IndexOf( _pattern, _code_search ) - 1, len( _pattern ) - 1 );
							_code_search = _code_search.strip( );
						else:
							_code_search = AcecoolLib.String.Strip( _code_search, 0, len( _pattern ) );
							_code_search = _code_search.strip( );


					##
					## When a comment block ends, set the controlled toggle to false ( ignore comment-line though )
					##
					if ( _category == CATEGORY_COMMENT_BLOCK_FINISH and AcecoolLib.String.IndexOfExists( _pattern, _code, True ) and self.GetInCommentBlock( ) ):
						## Strip the end if on different line or strip from start to end if on the same line
						if _line_number == self.GetCommentBlockStartLine( ):
							## Strip the comment block starting point to the comment block end pos
							_code_search = AcecoolLib.String.Strip( _code_search, self.GetCommentBlockIndexStart( ), AcecoolLib.String.IndexOf( _pattern, _code_search ) + len( _pattern ) - 1 );
							_code_search = _code_search.strip( );
						else:
							## Otherwise, we strip everything up to the ending..
							_code_search = AcecoolLib.String.Strip( _code_search, 0, AcecoolLib.String.IndexOf( _pattern, _code_search ) + len( _pattern ) - 1 );
							_code_search = _code_search.strip( );

						## Reset out comment block line data because we've encountered a comment-block end-point...
						self.ResetAccessors( 'InComment', 'InCommentBlock', 'CommentBlockStartLine', 'CommentBlockIndexStart', 'CommentBlockIndexEnd' );
						## self.ResetAccessorFuncValues( 'InComment', 'InCommentBlock', 'CommentBlockStartLine', 'CommentBlockIndexStart', 'CommentBlockIndexEnd' );
						self.SetCommentBlockEndLine( _line_number );

						##
						self.AddEntry( CATEGORY_DEBUGGING, _line_number, 'Comment Block END on Line: ' + str( _line_number ), _depth, _search_mode, _pattern );


			##
			## Task: Incorporate String Block Checks to prevent String Blocks from triggering various filters...
			##
			##
			## String Block System - So we don't extract String Block Contents as information...
			## Note: If it is nearly identical to comments, and so far it seems that way, convert into helper function to slim it down...
			##
			## InString
			## InStringBlock
			## StringBlockStartLine
			## StringBlockEndLine
			## StringBlockIndexStart
			## StringBlockIndexEnd
			##
			## If we have String rules...
			if ( not self.IsInComment( ) and self.data_syntax_strings != None and len( self.data_syntax_strings ) > 0 ):
				## Process Strings...
				for _category, _search_mode, _pattern, in self.data_syntax_strings:
					##
					_string_block_count = _code.count( _pattern );

					## Should we trigger single-line string blocks?
					_trigger_single_line_string_blocks = self.GetSetting( 'plugin_trigger_single_line_string_blocks', False );

					##
					## If we're in a String block, set our controlled toggle to true ( also set _in_comment to true so TODO / Tasks / etc.. can be picked up in comment blocks )
					##
					## If there are an odd number of patterns found, it means we turn it on at 1, 3, 5, etc... evens turn it on ( for the odd ) then back on ( for the even ) and repeat...
					## Ie: If there are 2, we don't enable it... Although, I think it would be a good idea to always trigger it...
					if ( _category == CATEGORY_STRING_BLOCK_BEGIN and _string_block_count > 0 and ( _trigger_single_line_string_blocks or not _trigger_single_line_string_blocks and _string_block_count % 2 == 1 ) and not self.IsInString( ) ): #
						## Let the code know we're in a comment and in a comment-block
						self.SetInString( True );
						self.SetInStringBlock( True );

						## Let the code know which line the comment block is on ( this is so we can strip it from the same line code output if necessary - as of right now a comment block at the end of the line will prevent code being output )
						self.SetStringBlockStartLine( _line_number );
						self.SetStringBlockEndLine( -1 );

						##
						self.AddEntry( CATEGORY_DEBUGGING, _line_number, 'String Block Start on Line: ' + str( _line_number ), _depth, _search_mode, _pattern );

						## The index where the comment block started
						self.SetStringBlockIndexStart( AcecoolLib.String.IndexOf( _pattern, _code ) - 1 );


					##
					## When a String block ends, set the controlled toggle to false ( ignore comment-line though )
					##
					if ( _category == CATEGORY_STRING_BLOCK_FINISH and self.IsInString( ) ):
						## If there are an odd number of patterns found, it means we turn it off at 1 found, 3, 5, etc... even means it was turned off ( for the odd ) and back on ( for the even )...
						## Note: If it is on, we can still trigger OFF on the same line... So if on the same line, we use EVEN to turn off.. If on another line, we use ODD...
						if ( _string_block_count > 0 and _string_block_count % 2 == ( 0, 1 )[ self.GetStringBlockStartLine( ) < _line_number ] ): # ( self.GetStringBlockStartLine( ) == _line_number and  ): or self.GetStringBlockStartLine( ) < _line_number and _string_block_count % 2 == 1 ) ): #or _code.rfind( _pattern ) > ( self.GetStringBlockIndexStart( ) + len( _pattern ) - 1 ) or _code.find( _pattern ) > -1 and _line_number > self.GetStringBlockStartLine( ) ):
							## Reset out comment block line data because we've encountered a comment-block end-point...
							self.ResetAccessors( 'InString', 'InStringBlock', 'StringBlockStartLine', 'StringBlockEndLine', 'StringBlockIndexStart', 'StringBlockIndexEnd' );
							## self.ResetAccessorFuncValues( 'InComment', 'InCommentBlock', 'CommentBlockStartLine', 'CommentBlockIndexStart', 'CommentBlockIndexEnd' );

							## Let the code know we're in a comment and in a comment-block
							self.SetInString( False );
							self.SetInStringBlock( False );
							self.SetStringBlockStartLine( -1 );
							self.SetStringBlockEndLine( _line_number );
							self.SetStringBlockIndexStart( -1 );
							self.SetStringBlockIndexEnd( -1 );

							##
							self.AddEntry( CATEGORY_DEBUGGING, _line_number, 'String Block END on Line: ' + str( _line_number ), _depth, _search_mode, _pattern );




			##
			## Line Data - used to remove all of the args from all of the callbacks...
			##

			## ## Object
			## _line_object = ACMSFileLineChunk( )

			## ## Source File Information
			## _line_object.SetSourceFileName( _name )
			## _line_object.SetSourceFileExt( _ext )
			## _line_object.SetFileLines( _count )

			## ## Code for the current line - raw and stripped...
			## _line_object.SetLineCode( _line )
			## _line_object.SetLineCodeStripped( _code )
			## _line_object.SetLineCodeLeftStripped( _code_lstrip )
			## _line_object.SetLineCodeRightStripped( _code_rstrip )

			## ## Line number, searchable code, line code depth, in comment, or in comment block...
			## _line_object.SetLineNumber( _line_number )
			## _line_object.SetLineCodeSearchable( _code_search )
			## _line_object.SetLineDepth( self.GetDepth( ) )
			## _line_object.SetIsLineCommented( self.GetInComment( ) )
			## _line_object.SetLineInComment( self.GetInComment( ) )
			## _line_object.SetLineInCommentBlock( self.GetInCommentBlock( ) )
			## ## _line_object.SetLineCharStart( _ )
			## ## _line_object.SetLineCharEnd( _ )
			## ## _line_object.Set( _ )
			## ## _line_object.Set( _ )
			## ## _line_object.Set( _ )

			## New callback, changes the line object, no need for returns...
			## self.ProcessLineEx( _line_object )


			##
			## Callback for OnProcessLine - Can completely modify the code for that line... Call it here before Comment Status, etc... is reset.
			## Task: Correct issue of OnProcessLine receiving outdated information regarding comment and commentblock status..
			##
			_xcode = self.ProcessLine( _line_number, _code_search, self.GetDepth( ), self.GetInComment( ), self.GetInCommentBlock( ) )
			if ( _xcode and _xcode != True and _xcode != False ):
				_code = _xcode

			## For each syntax type
			## CATEGORY_COMMENT_BLOCK		CATEGORY_COMMENT_BLOCK_BEGIN	CATEGORY_COMMENT_BLOCK_FINISH	CATEGORY_COMMENT			CATEGORY_CLASS
			## CATEGORY_TODO				CATEGORY_UNKNOWN				CATEGORY_REALM					CATEGORY_DEFINITION
			## CATEGORY_FUNCTION			CATEGORY_FUNCTION_LOCAL			CATEGORY_FUNCTION_CALLBACK		CATEGORY_FUNCTION_ACCESSOR
			## for _category, _pattern, in self.data_syntax:
			for _category, _search_mode, _pattern, in self.data_syntax:
				##
				## Map the search-modes to search-functions and determine whether or not a match has been found. If it did, then continue...
				##
				if ( self.LineContainsWatchedToken( _search_mode, _pattern, _code_search ) ):
					## if ( _search_mode == SEARCH_TYPE_REGEX and re.search( _pattern, _code_search ) ) or ( _search_mode == SEARCH_TYPE_STARTSWITH and _code_search.startswith( _pattern ) ) or ( _search_mode == SEARCH_TYPE_ENDSWITH and _code_search.endswith( _pattern ) ) or ( _search_mode == SEARCH_TYPE_CONTAINS and AcecoolLib.String.IndexOfExists( _pattern, _code_search, False ) ) or ( _search_mode == SEARCH_TYPE_CONTAINSI and AcecoolLib.String.IndexOfExists( _pattern, _code_search, True ) ):
					## Make sure comments are not looked at - ie a line comment at the end of the line is ok. A block comment anywhere as long as it isn't the entire line is ok. Or no comment is ok...
					if ( not self.LineContainsComment( _category ) and not self.LineContainsStringBlock( _category ) ):
						## If we're processing an accessor func, we reroute it through self.OnAccessorFuncExpansion...
						if ( _category == CATEGORY_FUNCTION_ACCESSOR ):
							##
							_args, _args_count, _args_raw = AcecoolLib.Table.FromFunctionArgsString( _code )

							##
							self.OnAccessorFuncExpansion( _search_mode, _category, _pattern, _line_number, _code_search, self.GetDepth( ), _args, _args_count )

						elif ( _category == CATEGORY_VARIABLE ):
							## For variable detection... and processing...
							self.OnVariableDetection( _category, _line_number, _code_search, self.GetDepth( ), _search_mode, _pattern )
						else:
							##
							self.AddEntry( _category, _line_number, _code_search, self.GetDepth( ), _search_mode, _pattern )

						## We've discovered an entry ( either an AccessorFunc entry or a regular entry - we break here to avoid writing it twice ) - skip processing the rest...
						break;
					else:
						##
						if ( self.InCommentDebuggingCategory( _category ) ):
							_output = self.GetSetting( 'output_task_line_prefix', '' ) + _code_search + self.GetSetting( 'output_task_line_suffix', '' )


							##
							## Task: Turn this into a rule - anything which can screw up the highlighter such as strings left open, open and closing string-elements, multi-line string definitions, etc... make sure simple 1 line rules can be added to make sure they're ended...
							##

							_output_cap = ''

							## Make sure an even number of single-quotes are on each line
							_single_quotes = _output.count( '\'' )
							if ( _single_quotes % 2 != 0 ):
								_output_cap = '\''

							## Make sure an even number of double-quotes are on each line...
							_double_quotes = _output.count( '"' )
							if ( _double_quotes % 2 != 0 ):
								_output_cap = '"'



							## Add the uncommented TODO remark - and add a hack so any syntax highlighting errors are ignored for the next line...
							self.AddEntry( _category, _line_number, _output + _output_cap, self.GetDepth( ), _search_mode, _pattern )

							## We've discovered an entry - skip processing the rest...
							break;


			## End ForEach Pattern Option

		## Capture the output data
		_output_data = self.ProcessOutput( _file, _count, self.entries )

		## Timestamp of finished data
		self.__timekeeper__.Stop( )

		## Output.. To show how long / short it takes to execute - as of January 21, 2018 a Python file of around 5000 lines takes 0.48 seconds to process ( _User Class with nested support enabled ) plus 0.02 seconds to save ( SSD ).
		## self.print( ACMS_PRINT_TYPE_RUN, 'Run', ' >> Base Execution Time: ' + str( self.__timekeeper__ ) )
		self.AddProcessText( 'File Mapped: ' + str( self.__timekeeper__ ) + ' -> '  )


		##
		## Caching System - If enabled, save the file...
		##
		if ( self.GetSetting( 'use_caching_system', False ) ):
			## Save it...
			## _result = self.GetPluginObject( ).SaveFileCache( _file, _output_data )
			_result = _database.UpdateCache( _file, _output_data ); # self.GetPluginDatabase( ).UpdateCache( _file, _output_data )

			## And how much time it took to save... We output this to show the time-savings ( typically 0.01 seconds...
			## self.print( ACMS_PRINT_TYPE_CACHE, 'Run', ' >> Cache Saved Execution Time: ' + self.__timekeeper__.DeltaStr( ) )

			## self.AddProcessText( 'Cache.Saved -> ' + str( self.__timekeeper__.DeltaStr( ) )  )
			self.AddProcessText( 'Cache.Saved: ' + str( self.__timekeeper__ ) )
			## print( ' >> Saving Result: ' + str( _result ) )

			##
			## self.database.SaveFileCache( _file, _output_data )

			## ## If an error occured during cache-save - add it to the output ( data has already been processed so we can't add it to a category and expect it to show up without reprocessing everything and then it'd be saved if it saves that time, etc.. We simply add it to the top! )
			## if ( _result == None ):
			## 	_output_data = '[ Caching Error ] ' + str( _result ) + '\n\n' + _output_data

		self.print( ACMS_PRINT_TYPE_RUN, self.GetProcessText( ) )

		## Everything has been processed - we return ProcessOutput because ProcessOutput will use callbacks to process the order of things the user wants to see and so on...
		return _output_data


	## Task: Turn into AccessorFunc
	## Task: AccessorFunc Getter helpers such as ToString need to grab the raw result, the default included result, etc..
	## Task: so I can do something like this easily - ie 'get_str': lambda this, _raw, _value: ( 'Disabled', 'Enabled' )[ _raw != None ] --- OR, Has or Isset needs to be easily callable from within these helpers... That would be ideal...
	def IsUsingCacheSystemToString( self ):
		return ( 'Disabled', 'Enabled' )[ self.GetSetting( 'use_caching_system', False ) ]


	##
	## Internal Handler / Hook Caller - Called when a new instance is being created...
	##
	##
	def __init__( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, '__init__', 'New instance created!' )


		##
		## print( '<<<<\n' * 4 )
		## print( str( self.GetSetting( 'debug_mode', False ) ) )

		## Calculate how long it takes to process files vs cache...
		self.__timekeeper__ = AcecoolLib.TimeKeeper( ).Start( )

		## Add new marker because we're doing things prior to run which matter...
		## self.print( ACMS_PRINT_TYPE_INIT, '__init__', '[ XCodeMapperBase > Initialization ] ----------------------------------------------------------------------------------------------------------------------------' )
		self.AddProcessText( 'Init -> ' )



		## Code - Map is typically the LAST view, always...
		## _code_map = self.GetSublimeView( 'Code - Map' )
		_code_map = self.GetSublimeView( 'ACMS - Panel' )
		self.__mapped_file_panel__ = None

		## Just in case...
		if ( _code_map != None ):
			## Notify the user...
			## self.print( ACMS_PRINT_TYPE_SETUP, '__init__', ' >> Code - Map Panel >> Updating Configuration!' )
			self.AddProcessText( 'Panel.Config -> ' )

			## Update the var so it can be grabbed later...
			self.__panel__ = _code_map

			## Callback for editing configuration... Default sets the scroll past end to false...
			self.SetupCodeMapPanel( _code_map )

		## Caching System Notification
		## self.print( ACMS_PRINT_TYPE_INIT, '__init__', ' >> XCodeMapper Caching System is ' + self.IsUsingCacheSystemToString( ) )
		self.AddProcessText( 'Caching is ' + self.IsUsingCacheSystemToString( ) + ' -> ' )

		## This is what is returned after everything has been processed..
		## self.output = ''

		## Entries counter
		self.entries = 0

		#-- Syntax Rules
		self.data_syntax = [ ]
		self.data_syntax_comments = [ ]
		self.data_syntax_strings = [ ]
		self.output_order = [ ]

		## For typos...
		self.typo_list = [ ]

		## Data-Table for the Arg / Data-Type Addition / Modification / Conversion System...
		self.data_arg_conversion = { }

		## These are the categories we add things to...
		self.categories = { }
		self.category_types = { }
		self.category_count = { }

		## Mapped .. Basically the map is the category name as a string to a key from category_children.append.... This lets us sort the children...
		self.category_children_map = { }
		self.category_children = { }

		self.data_hidden_categories = { }

		##
		self.error_example_log = { }

		##
		self.AccessorFuncHelpersDefined = { }
		## Call the helper Callback functions for Pre / Post which can be overridden by the user - OnInit should be used internally and not overridden..
		## self.CallFunctionHooks( 'Init' )
		self.OnInit( )


	##
	## Internal helper for XCodeMapper - This creates helper AccessorFuncs - Getter / Setter functions...
	## TODO: Redefine the config system so while the Helpers are available it uses GetFlag / SetFlag and the Accessors work off that... maybe..
	##
	def OnSetupAccessorFuncsDefault( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupAccessorFuncsDefault' )


		## Internal / Primary AccessorFuncs

		## ## For languages such as ASP and PHP which have markers which designate where its code is vs HTML or other...
		## ## For non-.js excentions this also applies to JavaScript in php, html and asp extension files..
		## ## TODO: Extend code / OnSetupSyntax callback and nested calls to specify which language the syntax applies to to allow one file to manage all or many languages...
		## ## For files which allow multiple languages to be executed - This simply tells the script which language is currently being processed thereby telling the script which syntax to use
		## ## .php allows HTML / JavaScript and PHP.. .asp allows the same with ASP.. .js allows JavaScript.. .htm / .html allows HTML / JavaScript..
		## ## 'Lua / JavaScript / PHP / ASP / python / etc..'
		## self.AccessorFunc( 'InLanguage', False, 'ActiveLanguage', 'None' )

		## ## The Entire File stored as a new-line delimited table...
		## self.AccessorFunc( 'FileData', CONST_DATA_NONE, 'FileLines', 0 )

		## ## Class Data
		## self.AccessorFunc( 'ActiveClass', None, 'ActiveClassDepth', 0, 'ClassStack', [ ] )

		## ## Create helper functions to be used as controlled-toggles in certain areas / Manage which line the comment block started on and where it ended / Manage which character the comment-block started with and the index of where the comment-block end is...
		## self.AccessorFunc( 'InComment', False, 'InCommentBlock', False, 'CommentBlockStartLine', -1, 'CommentBlockEndLine', -1, 'CommentBlockIndexStart', -1, 'CommentBlockIndexEnd', -1 )

		## ## Manage the Depth / indentation
		## self.AccessorFunc( 'Depth', 0, 'LastDepth', 0, 'IndentLength', 0 )

		## ## GLua Execution Realm - LIMIT TO REALM_ ENUMeration...
		## self.AccessorFunc( 'Realm', REALM_UNKNOWN )

		##
		## Call the Individual Code-Mapper Callback
		##
		self.OnSetupAccessorFuncs( )


	##
	## Setup your configuration here...
	##
	def OnSetupConfigDefaults( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupConfigDefaults' )


		## AccessorFunc - Defaults
		## TODO: Replace all values with CONST_DEFAULT_XXX or DEFAULT_XXX style vars so the data is consistent and not repeated throughout..

		## Defaults for Language, file-data, etc....
		self.SetAccessors( 'InLanguage', False, 'ActiveLanguage', 'None', 'FileData', CONST_DATA_NONE, 'FileLines', 0 )
		## self.SetAccessorFuncValues( 'InLanguage', False, 'ActiveLanguage', 'None', 'FileData', CONST_DATA_NONE, 'FileLines', 0 )

		## Class tracking... Soon to be a stack..
		self.SetAccessors( 'ActiveClass', '', 'ActiveClassDepth', 0, 'ClassStack', [ ] )
		## self.SetAccessorFuncValues( 'ActiveClass', '', 'ActiveClassDepth', 0, 'ClassStack', [ ] )

		## Are we in a comment block - note, this can't be in the line by line scope as blocks can extend beyond a single line...
		self.SetAccessors( 'InComment', False, 'InCommentBlock', False, 'CommentBlockStartLine', -1, 'CommentBlockEndLine', -1, 'CommentBlockIndexStart', -1, 'CommentBlockIndexEnd', -1 )
		self.SetAccessors( 'Depth', 0, 'LastDepth', 0, 'IndentLength', 0, 'Realm', REALM_UNKNOWN )
		## self.SetAccessorFuncValues( 'InComment', False, 'InCommentBlock', False, 'CommentBlockStartLine', -1, 'CommentBlockEndLine', -1, 'CommentBlockIndexStart', -1, 'CommentBlockIndexEnd', -1 )
		## self.SetAccessorFuncValues( 'Depth', 0, 'LastDepth', 0, 'IndentLength', 0, 'Realm', REALM_UNKNOWN )

		##
		if ( self.GetSetting( 'show_categories_xcm', True ) == False ):
			self.AddHiddenCategory( 'CATEGORY_XCM_ERROR' );
			self.AddHiddenCategory( 'CATEGORY_XCM_INFO' );
			self.AddHiddenCategory( 'CATEGORY_XCM_DEBUG' );
			self.AddHiddenCategory( 'CATEGORY_XCM_OPTIMIZATION' );
			self.AddHiddenCategory( 'CATEGORY_XCM_SYNTAX_ENTRY' );
			self.AddHiddenCategory( 'CATEGORY_XCM_SYNTAX_RULE_ENTRY' );
			self.AddHiddenCategory( 'CATEGORY_XCM_OUTPUT_ORDER_ENTRY' );
			self.AddHiddenCategory( 'CATEGORY_XCM_OUTPUT_ENTRY' );

		##
		## Call the Individual Code-Mapper Callback
		##
		self.OnSetupConfig( )


	##
	## Internal function used to pre-define base modifications to the code map panel...
	##
	def SetupCodeMapPanel( self, _panel ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'SetupCodeMapPanel' )

		## Default: Disable Scroll Past End...
		self.print( ACMS_PRINT_TYPE_SETUP, 'SetupCodeMapPanel', ' >> Code - Map Panel >> scroll_pass_end set to False!' )
		_panel.settings( ).set( 'scroll_past_end', False )

		## ##
		## _syntax_source = self.GetSetting( 'view_syntax_source', False )
		## _syntax_output = self.GetSetting( 'view_syntax_output', False )

		## ## If we want to override the syntax used by the source view based on configuration
		## if ( self.GetSetting( 'plugin_override_syntax', False ) ):

		## 	print( '[ ACMS ] >>>> PLUGIN_OVERRIDE_SYNTAX == True' )

		## 	## and valid data exists for the source view, set it...
		## 	if ( AcecoolLib.IsString( _syntax_source ) ):
		## 		print( '[ ACMS ] >>>> PLUGIN_OVERRIDE_SYNTAX == true >>>> Setting Source: ' + 'Packages/' + _syntax_source )
		## 		self.Plugin( ).GetSourceView( ).set_syntax_file( 'Packages/' + _syntax_source )

		## 	## and valid data exists for the output view, set it...
		## 	if ( AcecoolLib.IsString( _syntax_output ) ):
		## 		print( '[ ACMS ] >>>> PLUGIN_OVERRIDE_SYNTAX == true >>>> Setting Output: ' + 'Packages/' + _syntax_output )
		## 		self.Plugin( ).GetOutputView( ).set_syntax_file( 'Packages/' + _syntax_output )
		## 	elif ( AcecoolLib.IsString( _syntax_source ) ):
		## 		print( '[ ACMS ] >>>> PLUGIN_OVERRIDE_SYNTAX == true >>>> Setting Output: ' + 'Packages/' + _syntax_source )
		## 		self.Plugin( ).GetOutputView( ).set_syntax_file( 'Packages/' + _syntax_source )

		## Call the callback for everyone else
		self.OnSetupCodeMapPanel( _panel, _panel.settings( ) )



	##
	## Return whether or not an error example has been displayed - it is set to true as soon as it is called so the if statement can show the example without having to worry about setting a value to true...
	##
	def HasDisplayedErrorExample( self, _id ):
		##
		_was_shown = self.error_example_log.get( _id, False )

		##
		self.error_example_log[ _id ] = True

		##
		return _was_shown


	##
	## Internal Handler / Hook Caller
	##
	def Shutdown( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_SHUTDOWN, 'Shutdown', 'This instance of XCodeMapper / XCodeMapperBase is being wiped from memory!' )

		## Call the helper Callback functions for Pre / Post which can be overridden by the user - OnInit should be used internally and not overridden..
		self.CallFunctionHooks( 'Shutdown' )

		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'Shutdown', '[ XCodeMapper ] Finished ------------------------------------------------------------------------------------------------------------------------------------' )

		## If a DeveloperFunc exists, run it...
		if ( AcecoolLib.isfunction( self.DeveloperFunc ) ):
			self.DeveloperFunc( )


	##
	## Used to set up default replacements for the arguments to data-type to display one, the other or both...
	##
	def OnSetupArgDataTypeDefaults( self ):
		##
		## Add default replacements here..
		##

		## Example A
		## assignment..


		## Run the callback
		self.OnSetupArgDataType( )


	##
	## Returns a string with delimited elements such as a comma and space with the optional wrap around the data and then around the entire pack...
	## self.GetJoinedDelimitedString( _i, self.GetCfgObjectArgJoinPrefix( ), _pattern, "'", "'" ) == 'x', 'y', 'z'
	## self.GetJoinedDelimitedString( _i, ',', _pattern, " '", "' ", '[', ']' ) == [ 'x' ][, 'y' ][, 'z' ] ## Note: Maybe alter this one a bit? Or have this as the third set.. for optional args?
	##
	def GetJoinedDelimitedString( self, _i, _delimiter = ', ', _data = '', _data_prefix = '', _data_suffix = '', _text_prefix = '', _text_suffix = '' ):
		return _text_prefix + AcecoolLib.TernaryFunc( ( _i > 0 ), _delimiter, '' ) + _data_prefix + _data + _data_prefix + _text_suffix


	##
	## Helper - Outputs data to the console in an identifiable way...
	##
	def print( self, _type, _name, _text = '' ):
		## _debug = self.GetCfgDebugMode( )

		## ACMS_PRINT_TYPE_SYMBOLS
		## ACMS_PRINT_TYPE_CACHE
		## ACMS_PRINT_TYPE_ERROR

		## ACMS_PRINT_TYPE_SETUP
		## ACMS_PRINT_TYPE_INIT
		## ACMS_PRINT_TYPE_SHUTDOWN

		## ACMS_PRINT_TYPE_RUN
		## ACMS_PRINT_TYPE_RUN_INDENTATION
		## ACMS_PRINT_TYPE_CLASS_ARGS
		## ACMS_PRINT_TYPE_FUNCTION_ARGS
		## ACMS_PRINT_TYPE_ACCESSOR_EXPANSION
		## ACMS_PRINT_TYPE_ENTRY_ARGS
		## ACMS_PRINT_TYPE_ACCESSOR_RESET
		## ACMS_PRINT_TYPE_PROCESS_VARARGS
		## ACMS_PRINT_TYPE_EVERY_ENTRY

		## ACMS_PRINT_TYPE_PROCESSING_RESULT
		_developing		= self.GetSetting( 'debugging_mode', False ) == True
		_debugging		= self.GetSetting( 'debugging_task', False ) == _name or self.GetSetting( 'debugging_task', False ) == '*'

		##
		_is_repetitive	= ( _type == ACMS_PRINT_TYPE_RUN or _type == ACMS_PRINT_TYPE_RUN_INDENTATION or _type == ACMS_PRINT_TYPE_CLASS_ARGS or _type == ACMS_PRINT_TYPE_FUNCTION_ARGS or _type == ACMS_PRINT_TYPE_ACCESSOR_EXPANSION or _type == ACMS_PRINT_TYPE_ENTRY_ARGS or _type == ACMS_PRINT_TYPE_ACCESSOR_RESET or _type == ACMS_PRINT_TYPE_PROCESS_VARARGS or _type == ACMS_PRINT_TYPE_EVERY_ENTRY )
		_is_init		= ( _type == ACMS_PRINT_TYPE_INIT or _type == ACMS_PRINT_TYPE_SHUTDOWN or _type == ACMS_PRINT_TYPE_SETUP )
		_is_result		= ( _type == ACMS_PRINT_TYPE_PROCESSING_RESULT or _type == ACMS_PRINT_TYPE_SYMBOLS )
		_is_run_info	= ( _type == ACMS_PRINT_TYPE_RUN or _type == ACMS_PRINT_TYPE_CACHE )

		##
		## if ( _type == _is_repetitive ):
		## 	return False

		## To allow printing only what we need....
		if ( _developing or _debugging or _is_run_info or _type == ACMS_PRINT_TYPE_ERROR ):
			## Debugging - in a nice formated output..
			_pretext = '[ ' + self.__name__ + ' > ' + self.GetSetting( 'active_language', 'None' ) + ' > ' + _name + ' ]'
			print( AcecoolLib.String.FormatColumn( len( _pretext ) + 4, _pretext ) + _text )


	##
	## End Callback functions
	##


	##
	## Helper - these are the symbols from the file we're currently editings symbols( ) from the view - ie all extracted symbols from Sublime Text...
	##
	def MappingFileSymbols( self, _view, _symbols ):
		## Call the callback
		return self.OnMappingFileSymbols( _view, _symbols )


	##
	##
	##
	def GetLangVariantName( self, _variant ):
		_mapper = ''
		if ( _variant != 'Default' and _variant != '' and _variant != None ):
			if ( AcecoolLib.IsString( _variant ) ):
				_mapper = str( _variant )
			elif ( AcecoolLib.IsNumber( _variant ) ):
				_mapper = GetLangVariantClassName( _variant )

		return _mapper


	##
	## Callback - Used to determine what to do when code isn't properly aligned ( check for =, and more than 1 space or 1 or more tabs.. if so then we add or subtract with a minimum of 1 space the delta depth * our indent char to the center to allow the alignment to remain flush...
	##
	def OnCalcCodeAlignment( self, _category, _line_number, _code, _depth, _depth_delta, _search_mode, _pattern, _category_original = None ):
		##
		## Detect if we have something to align ( existing = in line )
		##

		## If the _depth_delta is 0, we don't need to even bother..
		if ( _depth_delta == 0 ):
			return _code

		## Track the type...
		_type = 0

		## Grab the location
		_pos = AcecoolLib.String.IndexOf( '	= ', _code )

		## Check other chars
		if ( not _pos > 0 ):
			_pos = AcecoolLib.String.IndexOf( '\t= ', _code )
			_type = 1

		## Check other chars
		if ( not _pos > 0 ):
			_pos = AcecoolLib.String.IndexOf( ':	', _code )
			_type = 2

		## Check other chars
		if ( not _pos > 0 ):
			_pos = AcecoolLib.String.IndexOf( ':\t', _code )
			_type = 3

		## If we have one - and we're not in a comment...
		if ( _pos > 0 and not self.GetInComment( ) and not self.GetInCommentBlock( ) ):
			## We need to add...
			## self.print( 'OnCalcCodeAlignment', ' >> ADD DepthDif( ' + str( _depth_delta ) + ' ); line( ' + str( _line_number ) + '); code( "' + _code + '" ); - BEFORE' )

			## Determine what we need to do... Add or subtract...

			## Add...
			if ( _depth_delta > 0 ):
				## Type 0: Spaces before =...
				if ( _type == 0 ):
					_code = _code.replace( '	= ', AcecoolLib.String.repeat( ' ', _depth_delta ) + ' = ', 1 )

				## Type 1: Tabs before =
				if ( _type == 1 ):
					_code = _code.replace( '\t= ', AcecoolLib.String.repeat( '\t', _depth_delta ) + '\t= ', 1 )

				## Type 2: Spaces after :
				if ( _type == 2 ):
					_code = _code.replace( ':	', ':	' + AcecoolLib.String.repeat( ' ', _depth_delta ), 1 )

				## Type 3: Tabs after :
				if ( _type == 3 ):
					_code = _code.replace( ':\t', ':\t' + AcecoolLib.String.repeat( '\t', _depth_delta ), 1 )

			## Show the result...
			## self.print( 'OnCalcCodeAlignment', ' >> ADD DepthDif( ' + str( _depth_delta ) + ' ); line( ' + str( _line_number ) + '); code( "' + _code + '" ); - FINISHED' )

			## Subtract Mode...
			if ( _depth_delta < 0 ):
				## Task: Create Subtraction algorithm... Make sure we have whitespace to subtract... Determine depth of data to subtract.. etc...

				## Logic not implemented...
				pass

			## Show the final data - used instead of - FINISHED...
			## self.print( 'OnCalcCodeAlignment', ' >> FLUSH SYSTEM __DONE Diff( ' + str( _depth_delta ) + ' ): ' + _code )

		## Do nothing by default...
		return _code


	##
	## Callback - Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	##
	def PreAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _search_mode, _pattern, _category_original = None ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_EVERY_ENTRY, 'PreOnAddEntry', _code )

		##
		## Shorten local function to l-f....
		## Task: Implement RegEx method of defining function name search ...
		## ##
		## _func_name_search = self.GetSetting( 'function_local_name_search', '' )
		## _func_name_search_len = len( _func_name_search )
		## if ( _func_name_search_len > 0 ):
		## 	## Grab the char directly after the start.. _code[ _func_name_search_len : _func_name_search_len + 1 ]
		## 	_func_name_search_next_char = AcecoolLib.String.SubStrNextChars( _code, _func_name_search_len, 1 )

		## 	## Make sure the code begins with it, and there is a space after it...
		## 	if ( _code.startswith( _func_name_search ) and _func_name_search_next_char == ' ' or _func_name_search_next_char == '	' ):
		## 		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		## 		self.print( 'PreOnAddEntry', 'Stripping Function Before: ' + _code )

		## 		_code = AcecoolLib.String.SubStr( _code, len( _func_name_search ), len( _code ) )
		## 		_code = self.GetSetting( 'function_local_name_replace', '' ) + _code

		## 		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		## 		self.print( 'PreOnAddEntry', 'Stripping Function After: ' + _code )

		##
		## Shorten function to f....
		## Task: Implement RegEx method of defining function name search ...
		##
		_func_name_search = self.GetSetting( 'function_name_search', '' )
		_func_name_search_len = len( _func_name_search )

		##
		if ( _func_name_search_len > 0 ):
			## Since we have something to look for... add the prefix search too...
			_func_name_search_prefix = self.GetSetting( 'function_name_prefix_search', '' )
			_func_name_search_len_prefix = len( _func_name_search_prefix )

			## Grab the char directly after the start.. _code[ _func_name_search_len : _func_name_search_len + 1 ]
			_func_name_search_next_char = AcecoolLib.String.SubStrNextChars( _code, _func_name_search_len, 1 )
			_func_name_search_next_char_prefix = AcecoolLib.String.SubStrNextChars( _code, _func_name_search_len_prefix + _func_name_search_len, 1 )

			## Make sure the code begins with it, and there is a space after it...
			if ( _code.startswith( _func_name_search ) and _func_name_search_next_char == ' ' or _func_name_search_next_char == '	' ):

				## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
				## self.print( 'PreOnAddEntry', 'Stripping Function Before: ' + _code )

				_code = _code[ _func_name_search_len : len( _code ) ] # AcecoolLib.String.SubStr( _code, _func_name_search_len, len( _code ) )
				_code = self.GetSetting( 'function_name_replace', '' ) + _code

				## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
				## self.print( 'PreOnAddEntry', 'Stripping Function After: ' + _code )

			## If code starts with local then normal, rip it..
			if ( _code.startswith( _func_name_search_prefix + _func_name_search ) and _func_name_search_next_char_prefix == ' ' or _func_name_search_next_char_prefix == '	' ):

				_code = _code[ _func_name_search_len_prefix + _func_name_search_len : len( _code ) ] # AcecoolLib.String.SubStr( _code, len( _func_name_search ), len( _code ) )
				_code = self.GetSetting( 'function_name_prefix_replace', '' ) + self.GetSetting( 'function_name_replace', '' ) + _code



		##
		## Determine whether or not we need to modify the code...
		##
		_xcode = self.OnCalcCodeAlignment( _category, _line_number, _code, _depth, _depth_delta, _search_mode, _pattern, _category_original )
		if ( _xcode != None and _xcode != '' and _xcode != False and _xcode != True ):
			_code = _xcode
			_xcode = None

		## Return the normal PreOnAddEntry to allow for canceling entries to occur, etc... We do our internal edits prior to the user though - I may change it to after... or add a Post function to give more flexibility to the user...
		return self.PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _search_mode, _pattern, _category_original )


	##
	## Helper - Used to add a code entry to a category... This calls various callbacks such as OnFunction/ClassArgument ( For each arg, to modify the arg ), PreOnAddEntry ( to edit the data, add data, look for syntax errors, deny entry from being added, etc.. ), PreOnAddEntryCategory ( to mod the category the entry goes to )
	##
	def AddEntry( self, _category, _line_number, _code, _depth, _search_mode, _pattern, _parent = None ):
		## Allow function / Class args to be processed...
		## Function Arguments Replacement Callback - Will move this out of this callback function into an internal PreOnAddEntry callback or PostOnAddEntryCallbackButPreAddAlterations... but more concise..
		if ( self.GetCategoryType( _category ) == CATEGORY_TYPE_FUNCTION ):
			_code = self.ProcessEntryArgs( _category, _line_number, _code, _depth, _search_mode, _pattern, 'OnFunctionArgument' )

		## Allow function / Class args to be processed...
		## Function Arguments Replacement Callback - Will move this out of this callback function into an internal PreOnAddEntry callback or PostOnAddEntryCallbackButPreAddAlterations... but more concise..
		if ( self.GetCategoryType( _category ) == CATEGORY_TYPE_CLASS ):
			_code = self.ProcessEntryArgs( _category, _line_number, _code, _depth, _search_mode, _pattern, 'OnClassArgument' )


		##
		## Allow modifying the data on entry...
		## Note: Testing with the ability to return '' to also skip it or to add an empty line...
		##

		## Category Replacement... Do this after the function args above to ensure they're applied properly - then change for other reasons..
		_xcategory = self.PreOnAddEntryCategory( _category, _line_number, _code, _depth, _search_mode, _pattern )

		## Make sure we remember the original category in case PreOnAddEntry needs it...
		_category_original = _category

		## If we're to modify the category, update the category - original category will already be set...
		if ( _xcategory != None and _xcategory != '' and _xcategory != False and _xcategory != True ):
			## Update the category to the replacement..
			_category = _xcategory

		## Calculate new depth, and the delta-depth...
		## Task: Re-Introduce OnCalcDepth
		_calc_depth = 0 #self.OnCalcDepth( _category, _line_number, _code, _depth, _search_mode, _pattern )
		_depth_delta = self.GetDeltaDepth( _depth, _calc_depth )

		## _xcode, _xline, _xdepth, _xcategory = self.PreOnAddEntry( _category, _line_number, _code, _depth, _search_mode, _pattern )
		_xcode = self.PreAddEntry( _category, _line_number, _code, _depth, _depth_delta, _search_mode, _pattern, _category_original )

		## If the returned data exists, isn't blank and isn't a boolean then update our _code value because we're using what the dev decided to replace _code with..
		if ( _xcode != None and _xcode != '' and _xcode != False and _xcode != True ):
			_code = _xcode

		## If we return '' then add a blank entry...
		if ( _xcode == '' ):
			## self.AddBlankEntry( _category )
			return

		## If we returned a boolean then we don't add anything - we assume the coder who returned True or False in the callback called AddEntry or RecalculateLine...
		if ( _xcode == False or _xcode == True ):
			return

		## If the category doesn't add, we need to add it...MAP_CATEGORIES[ _category ]
		if ( self.categories.get( str( _category ) ) == None ):
			self.categories[ str( _category ) ] = [ ]

		##
		_entry = { 'line_number': _line_number, 'category': _category, 'code': _code, 'depth': _calc_depth, 'realm': REALM_UNKNOWN, 'parent': _parent }
		self.entries = self.entries + 1

		##
		_category_str = str( _category )

		## Convert the string _parent code / id into an id - if the id doesn't exist we make it...
		## _parent_id = self.GetMappedNestedCategoryID( _category, _category_str, _parent )

		## If _parent isset ( must be a string ) - it means we're adding an entry to a primary-category _category CHILD - the entry is inserted into _parent because _parent is - or will be - a nested category WITHIN _category...
		if ( _parent != None ):
			## If the primary category doesn't have a child table - make it...
			if ( self.category_children.get( _category_str, None ) == None ):
				## print( 'Created self.category_chilren[ ' + _category_str + ' ]' )
				self.category_children[ _category_str ] = { }

			## Note: If the parent doesn't exist - add it ( This is where I'd have to map the _parent text to an id )
			if ( self.category_children[ _category_str ].get( _parent, None ) == None ):
				## print( 'Created self.category_chilren[ ' + _category_str + ' ][ ' + _parent + ' ]' )
				self.category_children[ _category_str ][ _parent ] = [ ]

			## Add the entry to the child, of the primary category, category - which is known as the entry parent... So we have ( MainCategory > NestedCategoryKnownAsParent > Entry ) instead of ( MainCategory > Entry ) and instead of using a standard table, we use another - will change soon when I generalize everything...
			self.print( ACMS_PRINT_TYPE_EVERY_ENTRY, 'AddEntry', 'Added child _entry to self.category_chilren[ ' + _category_str + ' ][ ' + _parent + ' ] = ' + str( _entry ) )
			self.category_children[ _category_str ][ _parent ].append( _entry )
			## self.category_children[ _category_str ][ _parent_id ].append( _entry )
		else:
			##
			self.categories[ _category_str ].append( _entry )

		##
		self.AddCategoryEntryCount( _category )


		self.PostOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _search_mode, _pattern, _category_original )


	## ##
	## ## Helper - Outputs the line number based on the mode to the code, depth, etc..
	## ##
	## def GetOutputIndentedLineText( self, _code, _line = -1, _depth = 0 ):
	## 	## Grab the line indent mode - this tells us how we add it - note: If we need to subtract text, that will be returned too so the full output can be confined using - Line Indent Mode as ENUM as Value / Width as Whole Number / Int
	## 	_indent_mode = self.GetVariableValueByName( self.GetSetting( 'output_line_column_mode', 'ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT' ), 'ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT', ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT )

	## 	## Helpers
	## 	_code_len		= len( _code )
	## 	_indentation	= self.OnCalcDepthChars( _category, _line, _code, _depth )

	## 	## _indent_mode	= self.GetSettingENUM( 'output_line_column_mode', 'ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT', ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT )
	## 	_indent_code_w	= self.GetSetting( 'output_line_column_width', 0 ) - len( _indentation )



	## 		## Add Blank Line Entry Support - Blank lines will not supply a line-number or code - ie line number will be a blank string as will code...
	## 		if ( _line != '' and _line >= 0 and _code.strip( ) != '' ):
	## 			## Set the output for the line-number to be ':<line-number>'
	## 			_output_line = ':' + str( _line )

	## 			## If we don't want to output the line-number - ie line number is -1 or so, then set it to an empty string - this is used for Help lines, Empty Lines, etc..
	## 			if ( _line < 0 ):
	## 				_output_line = ''

	## 			## DEFAULT CLAUSE meaning no need for - ie Default doesn't need anything to happen..: if ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT ):
	## 			_output = _code + _output_line

	## 			## If we force truncate, then force it... Or, if we are organized, then we apply the truncatable function only if it doesn't truncate.... If it does, we leave it to default above!!
	## 			if ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_TRUNCATE or ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_ORGANIZED and _code_len <= _indent_code_w ) ):
	## 				_output = AcecoolLib.String.FormatColumn( _indent_code_w, _code ) + _output_line

	## 			## Output the data in the format of ( + connects 2 pieces of data ) TAB + ( repeat TAB_CHAR using _depth as multiple ) + LINE_CODE + : + LINE_NUMBER + LINE_BREAK / NEW-LINE
	## 			_data += _indentation + _output + self.GetSetting( 'output_new_line_chars', '\n' )


	## 		else:
	## 			##	New line no matter what?
	## 			_data += self.GetSetting( 'output_new_line_chars', '\n' )

	## 	## and after outputting the data for the category, add an additional newline to allow a space between categories..
	## 	_data += self.GetSetting( 'output_new_line_chars', '\n' )


	##
	## Helper - Returns the FormatColumn column size based on code provided - if Truncate then width is absolute cutoff. If organized, then len( _code ) is cut-off ie none.. If none / default then len( _code )... Simpler way to do it...
	##
	def GetFormatColumnCodeLenAllotted( self, _code, _depth = 0 ):
		## Grab the line indent mode - this tells us how we add it - note: If we need to subtract text, that will be returned too so the full output can be confined using - Line Indent Mode as ENUM as Value / Width as Whole Number / Int
		_indent_mode = self.GetVariableValueByName( self.GetSetting( 'output_line_column_mode', 'ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT' ), 'ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT', ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT )

		##
		_indentation = self.GetDepthString( _depth )
		_indentation_len = len( _indentation.replace( '\t', ' ' * ACMS( ).GetOutputViewTabSizeSetting( ) ) )

		## Returns the allotted width length
		_indent_code_w	= self.GetSetting( 'output_line_column_width', 0 ) - _indentation_len

		## Helper
		_code_len = len( _code )

		## If the indent mode is default, then the line number will ALWAYS be at the end of the line... If mode is organized and code length is longer than allotted width, then it is added to the end of the line...
		if ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT or ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_ORGANIZED and _code_len >= _indent_code_w ) ):
			return _code_len, ''

		## If indent mode is truncate and code len is longer than allotted size, we return the allotted size so the code can be cut at that point...
		if ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_TRUNCATE and _code_len >= _indent_code_w ):
			return _indent_code_w, ''

		## As Default is covered - and truncate max, and organized max - the only logic left, is for truncate and organized mode if code len isn't the same as the allotted size, so there is room... so we return the size and the number of spaces needed, maybe..
		return _indent_code_w, ( _indent_code_w - _code_len ) * ' '







	##
	## Helper - Returns the Category Line Text ( With Depth, and with indented line number if applicable )
	##
	def GetOutputCategoryLine( self, _category, _category_str, _category_name, _category_desc, _line = -1, _depth = 0 ):
		## Set the output for the line-number to be ':<line-number>'
		_output_line = ''

		## If we have a line number to add from either the function arg, or if we find one already added - make sure we separate it into its own var so we can ensure it is added to the end of the line...
		if ( AcecoolLib.IsNumber( _line ) and _line > 0 ):
			_output_line = ':' + str( _line )
		else:
			_tab_colon = _category_name.rfind( ":" )
			if ( _tab_colon >= 0 ):
				_end = _category_name[ _tab_colon + 1 : ]

				if ( len( _end ) > 0 ):
					_output_line = _end
					_category_name = _category_name[ : _tab_colon ]


		## The Extra Length we need to keep to ensure the hack survives and the name - so we trim from within the description only..
		_cat_desc_extra_len = len( _category_name + self.GetSetting( 'output_category_desc_prefix', '' ) + self.GetSetting( 'output_category_desc_suffix', '' ) )

		## If we are set to limit the length, then so be it... The length will be a separate config because we know the line-ending is 1 + numbers - so 5 for up to 9,999 line file, 6 for 99,999 which will be assumed..
		if ( self.GetSetting( 'output_category_column_width', -1 ) >= 0 ):
			## Ensure it is truncated at the right level, then strip the spaces so shorter messages to not extend the suffix so far out of the reasonable area...
			_output_desc = AcecoolLib.String.FormatColumn( self.GetSetting( 'output_category_column_width', -1 ) - _cat_desc_extra_len, _category_desc ).strip( )

		## If we're showing the category description, then append the Prefix, Description and Suffix together - otherwise use a blank line as the return.. then apply append this var to the center of the data var...
		_cat_desc = AcecoolLib.TernaryFunc( ( self.GetSetting( 'output_category_desc', False ) == True and self.GetSetting( 'output_category_column_width', -1 ) >= 0 ), self.GetSetting( 'output_category_desc_prefix', '' ) + _output_desc + self.GetSetting( 'output_category_desc_suffix', '' ), '' )

		## Converted into single var so we can use substr to cut anything off which is needed...
		## self.GetSetting( 'output_indent_chars', '\t' ) * _depth +
		_output = self.GetSetting( 'output_category_prefix', '' ) + _category_name + _cat_desc + self.GetSetting( 'output_category_suffix', '' )

		##
		_output_substr_end = len( _output )
		_output_spaces = ''

		##
		if ( _output_line != '' ):
			_output_line = ':' + _output_line
			_output_substr_end, _output_spaces = self.GetFormatColumnCodeLenAllotted( _output, _depth )

		## Add the category friendly-name to the output data...
		return self.GetSetting( 'output_indent_chars', '\t' ) * _depth + _output[ : _output_substr_end ] + _output_spaces + _output_line + self.GetSetting( 'output_new_line_chars', '\n' )


	##
	## Callback the generate function uses to simplify each problem... Simply adds a helping text-notification at the top of the map so we can be sure CodeMap is actually running on our current file ( I've had issues during updates where it'd be open but not active which required a lot of meanuevering to re-engage it ) along with some other information...
	## Note: XCodeMapper.entries is _matches
	##
	## Note: This is called by Run... It is the FIRST function called in the line of helpers...
	##
	## @Arg: _file - The File-Name being processed..
	## @Arg: _lines - The total number of lines in the file being processed.
	## @Arg: _entries - The total number of entries added to XCodeMapper from the current file being processed..
	## @Returns: _data - The ENTIRE CodeMap Output for all of the categories the user wants information on...
	##
	def ProcessOutput( self, _file, _lines, _entries ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_PROCESSING_RESULT, 'ProcessOutput', 'Compiling output data for each populated category...' )

		## Start with a clean-slate....
		_data = ''


		## Grab the ordered categories list
		_ordered_cats = self.GetDefinitionsSetting( 'output_categories_order', ( ) )

		##
		## Because I want to add additional - real-time info - I actually have to modify this.. I could look at hidden categories on setup - and I may change this to match later, but categories can be hidden on the fly, right up to and including the end...
		##
		## If the user wants the message to appear which shows which file was parsed along with the number of entries added and total number of lines in the file.. then show it...
		if ( self.GetSetting( 'output_file_info_line', False ) ):
			## Mapping: <FILENAME>
			_data += 'Mapping: ' + AcecoolLib.String.GetFileName( _file )

			##	containing : <TOTAL_LINES> lines and <TOTAL_ENTRIES> entries!<NEW_LINE * CONFIG'd_NUMBER_OF_NEW_LINES>
			_data += ' containing: ' + str( _lines ) + ' lines and ' + str( _entries ) + ' entries! ' + str( len( _ordered_cats ) ) + ' categories are enabled!' + AcecoolLib.String.repeat( self.GetSetting( 'output_new_line_chars', '\n' ), self.GetSetting( 'output_file_info_new_lines', 1 ) )

		## For each category in our output_order list - compile the data...
		## for _category, _category_str, _category_name, _category_desc, in self.output_order:
		for _category_enum in _ordered_cats:
			_category			= self.GetVariableValueByName( _category_enum, None, -1 )
			_category_str		= str( _category )
			_category_name		= self.GetCategoryName( _category )
			_category_desc		= self.GetCategoryDesc( _category )

			##
			_hidden_categories	= self.data_hidden_categories;

			## If we hide this category, skip it...
			if ( _hidden_categories.get( _category_enum, False ) ):
				## If developer mode is enabled, output a note showing this category is hidden...
				if ( self.GetSetting( 'developer_mode' ) ):
					print( 'ProcessOutput -> Category Hidden! -> Category ENUM Name: ' + _category_enum + ' -> Category ID: ' + _category_str + ' -> Category Name: ' + _category_name + ' -> Category Description: ' + _category_desc );

				## Skip this loop
				continue;


			##
			## Task: Update logic - for each category in ordered cats... If category has data then proceed, else skip... if has children then output children if has data... After children, output normal data - note with a GetSetting this can be reversed so data first then children categories...  Much better than 2 or 3 identical or similar output functions with repeated code bleh..
			##

			## Check to see if we have data for the category type...
			_value = self.categories.get( _category_str )
			_values = '0'

			if ( _value != None ):
				_values = str( len( _value ) )

			##
			self.print( ACMS_PRINT_TYPE_PROCESSING_RESULT, 'ProcessOutput', '[ ACMS ] Processing ' + _values + ' Entries in Category( ' + _category_enum + ' = ' + str( _category ) + ', ' + _category_str + ', ' + str( _category_name ) + ' )' )

			## Add the Category Data
			_data += self.CompileCategoryEntriesString( _category, _category_str, _category_name, _category_desc )

			## Add the Category Data and Children Data...
			_data += self.ProcessCategoryOutput( _category, _category_str, _category_name, _category_desc )


		## Print Plugin Object Data to the console...
		if ( self.GetSetting( 'developer_mode', False ) and self.GetSetting( 'output_plugin_object_details', False ) ):
			_data += '\n\n' + str( self.Plugin( ) )

		## ##
		## if ( self.GetSetting( 'developer_mode', False ) ):
		## 	_data += '\n\nNote: File Load order is based on map > EXT to LANG then LANG Table for config for Language file / Mapper name..'
		## 	_data += '\n\tNext, if that file doesn\'t exist, and ext.py doesn\'t exist, then the error is displayed...'

		##
		_data += '\n'

		## As our output is now assigned to memory with a variable to reference it, we can shutdown the mapper and properly clean up the memory space
		self.Shutdown( )

		return _data


	##
	##
	##
	def GetVariableValueByName( self, _key, _default_key = None, _default = None, _alt_tab = { } ):
		return self.Plugin( ).GetVariableValueByName( _key, _default_key, _default, _alt_tab )


	##
	## PreProcessor - This is added so common mistakes / syntax errors, etc.. can be detected on each line using a new TypoFixEntry system...
	##
	def ProcessLine( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		##
		## Process the typo list.... If there are entries...
		##
		if ( len( self.typo_list ) > 0 ):
			##
			for _pattern, _error, _solution, in self.typo_list:
				## If a typo was found on the line, then notate it... Also, make sure it wasn't on a self.AddTypoFixEntry line...
				if ( not _code.startswith( 'self.AddTypoFixEntry' ) and _code.find( _pattern ) >= 0 ):
					## Add the error to the errors category
					self.AddError( 'Typo: ' + _error + ' is a common typo and doesn\'t exist - use ' + _solution + ' instead!', _line_number );

					## Modify the output code-line by adding TYPO to it...
					_code = _code + ' [ TYPO ]'


		## Pass-Through to our Callback...
		return self.OnProcessLine( _line_number, _code, _depth, _in_comment, _in_comment_block )


	##
	##
	##
	def ProcessCategoryOutput( self, _category, _category_str, _category_name, _category_desc ):
		##
		_data = ''

		## Append <THIS> Category Entry Contents to _data..
		## _data += self.CompileCategoryEntriesString( _category, _category_str, _category_name, _category_desc )

		##
		_children = self.category_children.get( _category_str, None )

		## If there aren't any children, stop at returning only the primary category...
		if ( _children == None or not len( _children ) > 0 ):
			return _data

		## Add the category friendly-name to the output data...
		## Check to see if we have data for the category type...
		_value = self.categories.get( _category_str )

		## Modify the title of the category...
		_category_name += ' - Children'

		## Grab the category Line we output to the output panel... Name, Description, etc...
		_data += self.GetOutputCategoryLine( _category, _category_str, _category_name, _category_desc )

		## ## The Extra Length we need to keep to ensure the hack survives and the name - so we trim from within the description only..
		## _cat_desc_extra_len = len( _category_name + self.GetSetting( 'output_category_desc_prefix', '' ) + self.GetSetting( 'output_category_desc_suffix', '' ) )

		## ## Simple Redirect...
		## _output_desc = _category_desc

		## ## If we are set to limit the length, then so be it... The length will be a separate config because we know the line-ending is 1 + numbers - so 5 for up to 9,999 line file, 6 for 99,999 which will be assumed..
		## if ( self.GetSetting( 'output_category_column_width', -1 ) >= 0 ):
		## 	## Ensure it is truncated at the right level, then strip the spaces so shorter messages to not extend the suffix so far out of the reasonable area...
		## 	_output_desc = AcecoolLib.String.FormatColumn( self.GetSetting( 'output_category_column_width', -1 ) - _cat_desc_extra_len, _output_desc ).strip( )

		## ## If we're showing the category description, then append the Prefix, Description and Suffix together - otherwise use a blank line as the return.. then apply append this var to the center of the data var...
		## _cat_desc = TernaryFunc( ( self.GetSetting( 'output_category_desc', False ) == True and self.GetSetting( 'output_category_column_width', -1 ) >= 0 ), self.GetSetting( 'output_category_desc_prefix', '' ) + _output_desc + self.GetSetting( 'output_category_desc_suffix', '' ), '' )

		## ## Add the category friendly-name to the output data...
		## _data += self.GetSetting( 'output_category_prefix', '' ) + _category_name + _cat_desc + self.GetSetting( 'output_category_suffix', '' ) + self.GetSetting( 'output_new_line_chars', '\n' )

		## Category Sort Exceptions - Disable Sorting for specific categories...
		_category_sort_exceptions	= self.GetSetting( 'output_categories_disable_sort', { } );
		_category_enum_name 		= self.GetCategoryEnumeratedName( _category );
		_category_enum				= self.GetCategoryIDFromEnumeratedName( _category_enum_name );
		_category_sort_default		= _category_sort_exceptions.get( _category_enum_name, False );

		## If developer mode is enabled, and this category is one which sorting has been disabled for, let the developer know...
		if ( self.GetSetting( 'developer_mode', False ) and _category_sort_default ):
			print( 'ProcessCategoryOutput -> Category ENUM Name: ' + _category_enum_name + ' -> Category Sort Default Only: ' + str( _category_sort_default ) + ' -> Category ENUM ID: ' + str( _category_enum ) + ' -> Category Description: ' + _category_desc );


		## Sort the children by name...
		## _method =  self.GetSettingENUM( 'output_sorting_method', 'ENUM_SORT_METHOD_DEFAULT', ENUM_SORT_METHOD_DEFAULT )
		_method = self.GetVariableValueByName( self.GetSetting( 'output_children_sorting_method', 'ENUM_SORT_METHOD_DEFAULT' ), 'ENUM_SORT_METHOD_DEFAULT', ENUM_SORT_METHOD_DEFAULT );


		## If a sorting method is defined - use it...
		if ( not _category_sort_default and _method != ENUM_SORT_METHOD_DEFAULT ):
			_children = sorted( _children.keys( ), reverse = AcecoolLib.TernaryFunc( ( _method == ENUM_SORT_METHOD_LINE_NUMBER or _method == ENUM_SORT_METHOD_ALPHABETICAL ), False, True ) )


		for _tab in _children:
			## _name			= _tab[ 'category_name' ]
			## _entries			= _tab[ 'entries' ]


			## Process each entry...
			_data += self.ProcessTableOutputEntries( self.category_children[ _category_str ][ _tab ], _category, _category_str, _category_name, _category_desc )

			## Todo modify...
			## _data += self.ProcessTableOutputEntries( self.category_children[ _category_str ][ _values ], _category, _category_str, _category_name, _category_desc )

		## Return data..
		return _data


	##
	## Task: Update this to be more generic in order to be able to process data recursively from a single table entry point... The category data can be retrieved inside or from a single var...
	##
	def ProcessTableOutputEntries( self, _list, _category, _category_str, _category_name, _category_desc ):
		## if ( _method == ENUM_SORT_METHOD_ALPHABETICAL or _method == ENUM_SORT_METHOD_ALPHABETICAL_DESC ):

		## Make sure we're working with a blank set of data to output...
		_data = ''

		## Sort the children by name...
		## _method = self.GetSettingENUM( 'output_sorting_method', 'ENUM_SORT_METHOD_DEFAULT', ENUM_SORT_METHOD_DEFAULT )
		_method = self.GetVariableValueByName( self.GetSetting( 'output_sorting_method', 'ENUM_SORT_METHOD_DEFAULT' ), 'ENUM_SORT_METHOD_DEFAULT', ENUM_SORT_METHOD_DEFAULT )

		## if ( _method == None ):
		## 	_method = ENUM_SORT_METHOD_ALPHABETICAL

		##
		def Sorter( _item ):
			if ( _item == None ):
				return ''

			##
			_result = ''

			## If we're sorting alphabetically, then return the appropriate key... Modify the data accordinly and then return it...
			## Task: Add a Callback so the data can be modified further during the sorting process - so def / class can be removed from consideration, for example...
			if ( _method == ENUM_SORT_METHOD_ALPHABETICAL or _method == ENUM_SORT_METHOD_ALPHABETICAL_DESC ):
				_code = _item[ 'code' ] ## .replace( 'def', '' ).replace( 'class', '' )
				_stripped = _code.strip( )
				_result = _stripped.upper( )
				if ( _result == '' ):
					return 'zzz'

				return _result

			## If we're sorting by line-number - which is the default method although we only process it if specified so some things can end up in different places despite being 'similar' ie blank lines being moved, and so on... Or line-number in reverse... I don't see much use for this but I'm leaving it here as a place-holder to add callback support for one or more of the keys - code, line_number, depth, etc..
			if ( _method == ENUM_SORT_METHOD_LINE_NUMBER or _method == ENUM_SORT_METHOD_LINE_NUMBER_DESC ):
				_code = str( _item[ 'line_number' ] )
				return ( len( _code ), _code )

		## Category Sort Exceptions - Disable Sorting for specific categories...
		_category_sort_exceptions	= self.GetSetting( 'output_categories_disable_sort', { } );
		_category_enum_name 		= self.GetCategoryEnumeratedName( _category );
		_category_enum				= self.GetCategoryIDFromEnumeratedName( _category_enum_name );
		_category_sort_default		= _category_sort_exceptions.get( _category_enum_name, False );

		## If developer mode is enabled, and this category is one which sorting has been disabled for, let the developer know...
		if ( self.GetSetting( 'developer_mode', False ) and _category_sort_default ):
			print( 'ProcessTableOutputEntries -> -> Category ENUM Name: ' + _category_enum_name + ' -> Category Sort Default Only: ' + str( _category_sort_default ) + ' -> Category ENUM ID: ' + str( _category_enum ) + ' -> Category Description: ' + _category_desc );

		## If we chose to sort the category data - The Ternary within sets up Reverse because if it isn't any one of the standards non _DESC then the only one it can be is DESC.. Except default isn't needed...
		if ( not _category_sort_default and _method != ENUM_SORT_METHOD_DEFAULT ):
			## Non-Default means we are going to SORT the data... How and in which order depends on the _method...
			_list = sorted( _list, key = Sorter, reverse = AcecoolLib.TernaryFunc( ( _method == ENUM_SORT_METHOD_LINE_NUMBER or _method == ENUM_SORT_METHOD_ALPHABETICAL ), False, True ) )

		##
		## Process the table - For each entry in the category... enter add it to the local data pool so it can be returned and added to the global pool..
		##
		_i = 0
		for _values in _list:
			_line			= _values[ 'line_number' ];
			_category		= _values[ 'category' ];
			_category_str	= str( _category );
			_code			= _values[ 'code' ];
			_depth			= _values[ 'depth' ];
			_realm			= _values[ 'realm' ];
			_parent			= _values[ 'parent' ];

			##
			## _data += self.GetCfgCategoryLinePrefix( ) + _category_name + _cat_desc + self.GetCfgCategoryLineSuffix( ) + self.GetCfgNewLine( )
			if ( _i == 0 ):
				## Grab the category Line we output to the output panel... Name, Description, etc...
				_data += self.GetOutputCategoryLine( _category, _category_str, str( _parent ), _category_desc, -1, _depth )
				## _data += self.GetSetting( 'output_indent_chars', '\t' ) * _depth + self.GetSetting( 'output_category_prefix', '' ) + str( _parent ) + self.GetSetting( 'output_category_suffix', '' ) + self.GetSetting( 'output_new_line_chars', '\n' )
				_i += 1


			## Helpers
			## _code_len		= len( _code )
			## _indentation	= self.OnCalcDepthChars( _category, _line, _code, _depth )

			## ## Line Indent Mode as ENUM as Value / Width as Whole Number / Int
			## ## _indent_mode	= self.GetSettingENUM( 'output_line_column_mode', 'ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT', ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT )
			## _indent_mode = self.GetVariableValueByName( self.GetSetting( 'output_line_column_mode', 'ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT' ), 'ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT', ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT )
			## _indent_code_w	= self.GetSetting( 'output_line_column_width', 0 ) - len( _indentation )

			## Add Blank Line Entry Support - Blank lines will not supply a line-number or code - ie line number will be a blank string as will code...
			if ( _line != '' and _line >= 0 and _code.strip( ) != '' ):
				## Set the output for the line-number to be ':<line-number>'
				_output_line = ':' + str( _line );

				## If we don't want to output the line-number - ie line number is -1 or so, then set it to an empty string - this is used for Help lines, Empty Lines, etc..
				if ( _line < 0 ):
					_output_line = '';

				## ## DEFAULT CLAUSE meaning no need for - ie Default doesn't need anything to happen..: if ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT ):
				## _output = _code + _output_line

				## ## If we force truncate, then force it... Or, if we are organized, then we apply the truncatable function only if it doesn't truncate.... If it does, we leave it to default above!!
				## if ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_TRUNCATE or ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_ORGANIZED and _code_len <= _indent_code_w ) ):
				## 	_output = AcecoolLib.String.FormatColumn( _indent_code_w, _code ) + _output_line

				## ## Output the data in the format of ( + connects 2 pieces of data ) TAB + ( repeat TAB_CHAR using _depth as multiple ) + LINE_CODE + : + LINE_NUMBER + LINE_BREAK / NEW-LINE
				## _data += _indentation + _output




				## Add the category friendly-name to the output data...self.GetSetting( 'output_indent_chars', '\t' ) * _depth
				_output_substr_end, _output_spaces = self.GetFormatColumnCodeLenAllotted( _code, _depth + 1 );

				## Make sure we indent the depth + 1 because this is the entry line, and above is the category line, otherwise they'd be on the same line...
				_data += self.GetSetting( 'output_indent_chars', '\t' ) * ( _depth + 1 ) + _code[ : _output_substr_end ] + _output_spaces + _output_line; #+ self.GetSetting( 'output_new_line_chars', '\n' )





				_data += self.GetSetting( 'output_new_line_chars', '\n' );
			else:
				##	New line no matter what?
				_data += self.GetSetting( 'output_new_line_chars', '\n' );

		## and after outputting the data for the category, add an additional newline to allow a space between categories..
		_data += self.GetSetting( 'output_new_line_chars', '\n' );

		return _data;


	##
	## Compiles all Category Entries as a string and returns it...
	##
	## @Arg: _category - Category ENUMeration as Integer
	## @Returns: String* - Long string of text returned, if data exists, otherwise an empty string is returned...
	##
	def CompileCategoryEntriesString( self, _category, _category_str, _category_name, _category_desc, _depth_mod = 0 ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_PROCESSING_RESULT, 'CompileCategoryEntriesString', 'Compiling output data...' )

		## Make sure we're working with a blank set of data to output...
		_data = '';

		## Check to see if we have data for the category type...
		_value = self.categories.get( _category_str );

		## If the data in the category has data then we can output something!
		if ( _value != None and len( _value ) > 0 ):
			## ## The Extra Length we need to keep to ensure the hack survives and the name - so we trim from within the description only..
			## _cat_desc_extra_len = len( _category_name + self.GetSetting( 'output_category_desc_prefix', '' ) + self.GetSetting( 'output_category_desc_suffix', '' ) )

			## ## Simple Redirect...
			## _output_desc = _category_desc

			## ## If we are set to limit the length, then so be it... The length will be a separate config because we know the line-ending is 1 + numbers - so 5 for up to 9,999 line file, 6 for 99,999 which will be assumed..
			## if ( self.GetSetting( 'output_category_column_width', -1 ) >= 0 ):
			## 	## Ensure it is truncated at the right level, then strip the spaces so shorter messages to not extend the suffix so far out of the reasonable area...
			## 	_output_desc = AcecoolLib.String.FormatColumn( self.GetSetting( 'output_category_column_width', -1 ) - _cat_desc_extra_len, _output_desc ).strip( )

			## ## If we're showing the category description, then append the Prefix, Description and Suffix together - otherwise use a blank line as the return.. then apply append this var to the center of the data var...
			## _cat_desc = TernaryFunc( ( self.GetSetting( 'output_category_desc', False ) == True and self.GetSetting( 'output_category_column_width', -1 ) >= 0 ), self.GetSetting( 'output_category_desc_prefix', '' ) + _output_desc + self.GetSetting( 'output_category_desc_suffix', '' ), '' )

			## ## Add the category friendly-name to the output data...
			## _data += self.GetSetting( 'output_category_prefix', '' ) + _category_name + _cat_desc + self.GetSetting( 'output_category_suffix', '' ) + self.GetSetting( 'output_new_line_chars', '\n' )



			## Grab the category Line we output to the output panel... Name, Description, etc...
			_data += self.GetOutputCategoryLine( _category, _category_str, _category_name, _category_desc, -1, _depth_mod );


			## The List to output...
			_list = self.categories[ _category_str ];

			## Sort the children by name...
			_method = self.GetVariableValueByName( self.GetSetting( 'output_sorting_method', 'ENUM_SORT_METHOD_DEFAULT' ), 'ENUM_SORT_METHOD_DEFAULT', ENUM_SORT_METHOD_DEFAULT );

			## _method =  self.GetSettingENUM( 'output_sorting_method', 'ENUM_SORT_METHOD_DEFAULT', ENUM_SORT_METHOD_DEFAULT );

			## if ( _method == None ):
			## 	_method = ENUM_SORT_METHOD_ALPHABETICAL;

			##
			def Sorter( _item ):
				if ( _item == None ):
					return '';

				##
				_result = '';

				## If we're sorting alphabetically, then return the appropriate key... Modify the data accordinly and then return it...
				## Task: Add a Callback so the data can be modified further during the sorting process - so def / class can be removed from consideration, for example...
				if ( _method == ENUM_SORT_METHOD_ALPHABETICAL or _method == ENUM_SORT_METHOD_ALPHABETICAL_DESC ):
					_code = _item[ 'code' ] ## .replace( 'def', '' ).replace( 'class', '' )
					_stripped = _code.strip( )
					_result = _stripped.upper( )
					if ( _result == '' ):
						return 'zzz'

					return _result

				## If we're sorting by line-number - which is the default method although we only process it if specified so some things can end up in different places despite being 'similar' ie blank lines being moved, and so on... Or line-number in reverse... I don't see much use for this but I'm leaving it here as a place-holder to add callback support for one or more of the keys - code, line_number, depth, etc..
				if ( _method == ENUM_SORT_METHOD_LINE_NUMBER or _method == ENUM_SORT_METHOD_LINE_NUMBER_DESC ):
					_code = str( _item[ 'line_number' ] )
					return ( len( _code ), _code )

				##
				return _item

			## Category Sort Exceptions - Disable Sorting for specific categories...
			_category_sort_exceptions	= self.GetSetting( 'output_categories_disable_sort', { } );
			_category_enum_name 		= self.GetCategoryEnumeratedName( _category );
			_category_enum				= self.GetCategoryIDFromEnumeratedName( _category_enum_name );
			_category_sort_default		= _category_sort_exceptions.get( _category_enum_name, False );

			## If developer mode is enabled, and this category is one which sorting has been disabled for, let the developer know...
			if ( self.GetSetting( 'developer_mode', False ) and _category_sort_default ):
				print( 'CompileCategoryEntriesString -> -> Category ENUM Name: ' + _category_enum_name + ' -> Category Sort Default Only: ' + str( _category_sort_default ) + ' -> Category ENUM ID: ' + str( _category_enum ) + ' -> Category Description: ' + str( _category_desc ) );

			## If we chose to sort the category data - The Ternary within sets up Reverse because if it isn't any one of the standards non _DESC then the only one it can be is DESC.. Except default isn't needed...
			if ( not _category_sort_default and _method != ENUM_SORT_METHOD_DEFAULT ):
				## Non-Default means we are going to SORT the data... How and in which order depends on the _method...
				_list = sorted( _list, key = Sorter, reverse = AcecoolLib.TernaryFunc( ( _method == ENUM_SORT_METHOD_LINE_NUMBER or _method == ENUM_SORT_METHOD_ALPHABETICAL ), False, True ) )


			##
			## Process the table - For each entry in the category... enter add it to the local data pool so it can be returned and added to the global pool..
			##
			## for _line, _category, _code, _depth, _realm, in _list:
			##
			for _values in _list:
				_line			= _values[ 'line_number' ];
				_category		= _values[ 'category' ];
				_code			= _values[ 'code' ];
				_depth			= _values[ 'depth' ] + _depth_mod;
				_realm			= _values[ 'realm' ];

				## Helpers
				_code_len		= len( _code );
				_indentation	= self.OnCalcDepthChars( _category, _line, _code, _depth );

				## Line Indent Mode
				## _indent_mode	= self.GetSettingENUM( 'output_line_column_mode', 'ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT', ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT )
				_indent_mode = self.GetVariableValueByName( self.GetSetting( 'output_line_column_mode', 'ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT' ), 'ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT', ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT );
				_indent_code_w	= self.GetSetting( 'output_line_column_width', 0 ) - len( _indentation );

				## Add Blank Line Entry Support - Blank lines will not supply a line-number or code - ie line number will be a blank string as will code...
				## if ( _line != -1 and _line != '' and _code.strip( ) != '' and _code != self.GetCfgEntryBlank( ) ):
				if ( _code != self.GetSetting( 'output_entry_blank', '' ) ):
					## Set the output for the line-number to be ':<line-number>'
					_output_line = ':' + str( _line );

					## If we don't want to output the line-number - ie line number is -1 or so, then set it to an empty string - this is used for Help lines, Empty Lines, etc..
					if ( _line < 0 ):
						_output_line = '';

					## DEFAULT CLAUSE meaning no need for - ie Default doesn't need anything to happen..: if ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT ):
					_output = _code + _output_line;

					## If we force truncate, then force it... Or, if we are organized, then we apply the truncatable function only if it doesn't truncate.... If it does, we leave it to default above!!
					if ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_TRUNCATE or ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_ORGANIZED and _code_len <= _indent_code_w ) ):
						_output = AcecoolLib.String.FormatColumn( _indent_code_w, _code ) + _output_line;

					## Output the data in the format of ( + connects 2 pieces of data ) TAB + ( repeat TAB_CHAR using _depth as multiple ) + LINE_CODE + : + LINE_NUMBER + LINE_BREAK / NEW-LINE
					_data += _indentation + _output + self.GetSetting( 'output_new_line_chars', '\n' );
				else:
					##	New line no matter what?
					_data += self.GetSetting( 'output_new_line_chars', '\n' );

			## and after outputting the data for the category, add an additional newline to allow a space between categories..
			_data += self.GetSetting( 'output_new_line_chars', '\n' );

		return _data;


	##
	## Internal Processor
	##
	def OnInit( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnInit' );

		## Reset the data each time Run is called, otherwise the data will continue stacking...
		self.Reset( );

		## Setup the object
		self.Setup( );


	##
	## Internal Processor
	##
	def OnSetup( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetup' );

		## Setup the AccessorFuncs by creating and initializing them..
		self.OnSetupAccessorFuncsDefault( );		# Calls Callback OnSetupAccessorFuncs to create other callbacks..

		## Setup Default AccessorFunc values...
		self.OnSetupConfigDefaults( );			# Calls Callback OnSetupConfig to setup the data in the other callbacks..

		##
		self.OnSetupArgDataTypeDefaults( );		# Calls Callback OnSetupArgDataType( ) to setup the data-types...

		## Callback: This lets you create new categories if you don't want to add them at the top of the script...
		self.OnSetupCategories( );

		## ## Callback: This is how the language is declared - and you can also earmark other special segments of code that you'd like categorized or watched for syntax errors...
		self.OnSetupSyntax( );

		## Setup Rules for indentation tracking, and more..
		self.OnSetupSyntaxRules( );

		## Callback: This lets you choose the order the categories output their information...
		## self.OnSetupCategoryOutputOrderDefaults( );

		## Sets up the help section of this script...
		self.OnSetupHelp( );


	##
	## Internal Handler / Hook Caller
	##
	def Setup( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'Setup' );

		self.CallFunctionHooks( 'Setup' );


	##
	## Internal Handler / Hook Caller
	##
	def Reset( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'Reset' );

		##
		## self.CallFunctionHooks( 'Reset' );
		self.PreOnReset( );
		self.OnReset( );
		self.PostOnReset( );


	##
	## Internal Processor
	##
	def OnReset( self ):
		## This is what is returned after everything has been processed..
		## self.output = '';

		## Entries counter
		self.entries = 0;

		#-- Syntax Rules
		## self.data_syntax = [ ];
		self.output_order = [ ];

		## These are the categories we add things to...
		## self.categories = { };
		## self.category_types = { };
		self.data_hidden_categories = { };

		##
		## self.AccessorFuncHelpersDefined = { };


	##
	## Returns DATA_NONE, DATA_OUT_OF_RANGE, or code on a specific line..
	##
	def GetFileDataByLine( self, _line ):
		## Adjust the requested line by offsetting it -1 because line 1 is 0...
		_line = _line - 1;

		## Return the file-data... DATA_NONE is the default response..
		_lines = self.GetFileData( );

		## Return the number of lines in the file... 0 is the default response..
		_count = self.GetFileLines( );

		## If data doesn't exist..
		if ( _lines == CONST_DATA_NONE or _count == 0 ):
			return CONST_DATA_NONE;

		## If we're out of range..
		if ( _line > _count or _line < 0 ):
			return CONST_DATA_OUT_OF_RANGE;

		## Fetch the data...
		_data = _lines[ _line ];

		##
		if ( not AcecoolLib.IsString( _data ) ):
			self.print( ACMS_PRINT_TYPE_ERROR, ' >> ERROR: _data is not a string GetFileDataByLine: ' + str( _data ) )
			_data = str( _data );

		## Return it...
		return _data;


	##
	## Add a value to the category entry counter for the input category and value - value is defaulted to 1 so it can be called without a number with our add entry function to incrememnt the counter...
	##
	def AddCategoryEntryCount( self, _category, _value = 1 ):
		## Make sure the value being used is numerical, otherwise don't set it..
		if ( type( _value ) == int ):
			## Add our current _value together with our Getter value ( which will return 0 if none is set ) making this valid all the time..
			self.SetCategoryEntryCount( _category, _value + self.GetCategoryEntryCount( _category ) );


	##
	## Forces the value of the category count to be set if the input value is numeric. This controls all of the setting functionality of all helpers.
	##
	def SetCategoryEntryCount( self, _category, _value = 0 ):
		## If an entry wasn't made / doesn't exist, then set the value as 0...
		if ( self.category_count.get( str( _category ) ) == None ):
			self.category_count[ str( _category ) ] = 0;

		## Make sure the value being used is numerical, otherwise don't set it..
		if ( type( _value ) == int ):
			self.category_count[ str( _category ) ] = _value;


	##
	## Returns either 0 or the actual numeric value of the counter ( forced numeric by the setter, or forced to be 0 if a value was never set )
	##
	def GetCategoryEntryCount( self, _category ):
		## If an entry wasn't made / doesn't exist, then return 0 to ensure a number always returns...
		if ( self.category_count.get( str( _category ) ) == None ):
			return 0;

		return ( self.category_count.get( str( _category ) ) );


	##
	## Returns True if the category has entries, False if it doesn't - useful for determining first entry, etc...
	##
	def HasCategoryEntries( self, _category ):
		return ( self.GetCategoryEntryCount( _category ) > 0 );


	##
	##
	##
	def AddHiddenCategory( self, _category ):
		##
		_category_enum_name			= '';
		_category_enum				= -1;

		## If its a number, then we convert to enum name
		if ( AcecoolLib.IsInt( _category ) ):
			_category_enum			= _category;
			_category_enum_name		= self.GetCategoryEnumeratedName( _category )

		## If string, we get enum id
		if ( AccecoolLib.IsString( _category ) ):
			_category_enum_name		= _category;
			_category_enum			= self.GetCategoryIDFromEnumeratedName( _category );

		## Add the entry as CATEGORY_UNKNOWN = -1;
		self.data_hidden_categories[ _category_enum_name ] = _category_enum;


	##
	## Add Syntax RuleSet ( _category is the type of data to retrieve such as function or comment.. mode is how we are to search and pattern is the text used to search)
	##
	def AddSyntax( self, _category, _search_mode, *_patterns ):
		## Callback ForEach Arg...
		def Callback( self, _i, _pattern ):
			## Depending on whether the data-set is for comments, or not... add them to a different list...
			if ( _category == CATEGORY_COMMENT or _category == CATEGORY_COMMENT_BLOCK_BEGIN or _category == CATEGORY_COMMENT_BLOCK_FINISH ):
				## Comments are added to our comments list so we can process them in advance..
				self.data_syntax_comments.append( ( _category, _search_mode, _pattern ) );
			## Depending on whether the data-set is for comments, or not... add them to a different list...
			elif ( _category == CATEGORY_STRING_BLOCK_BEGIN or _category == CATEGORY_STRING_BLOCK_FINISH ):
				## Comments are added to our comments list so we can process them in advance..
				self.data_syntax_strings.append( ( _category, _search_mode, _pattern ) );
			else:
				## Add the Syntax entry to our dataset.. Non-comment rules are added elsewhere...
				self.data_syntax.append( ( _category, _search_mode, _pattern ) );

			## Append the pattern to the string so we only output the debug message once per call instead of once per pattern added...
			return self.GetJoinedDelimitedString( _i, self.GetSetting( 'object_arg_join_prefix', ' ' ), _pattern, '\'', '\'' ); ## return AcecoolLib.TernaryFunc( ( _i > 0 ), ', ', '' ) + '\'' + _pattern + '\''

		## Call our Callback Processor for VarArgs and return the data..
		_output = self.ProcessVarArgs( _patterns, Callback, 'AddSyntax', 'Category {1} [ id \'{2}\' ]; Patterns: {0}', self.GetCategoryName( _category ), str( _category ) );


	##
	## Add Syntax RuleSet ( _category is the type of data to retrieve such as function or comment.. mode is how we are to search and pattern is the text used to search)
	##
	def AddArgDataTypeEntry( self, _category_type, _datatype, *_args ):
		## Helpers which can be passed through Callback as it is generated in this scope..
		_type_str = str( _category_type );

		## As this only needs to happen ONCE because the Category Type doesn't change , I may as well do it out here to save the cost of running the logic every time Callback is called.. - Create the nested category if it doesn't exist.. This element is to ensure the right args are applied to the right object / function...
		if ( self.data_arg_conversion.get( _type_str, None ) == None ): self.data_arg_conversion[ _type_str ] = { };

		## Callback ForEach Arg...
		def Callback( self, _i, _arg ):
			## self.data_arg_conversion.append( ( _category_type, _id_str, _name ) )
			self.data_arg_conversion[ _type_str ][ _arg ] = _datatype;

			## Append the pattern to the string so we only output the debug message once per call instead of once per pattern added...
			return self.GetJoinedDelimitedString( _i, self.GetSetting( 'object_arg_join_prefix', ' ' ), _arg, '\'', '\'' ); ## return TernaryFunc( ( _i > 0 ), ', ', '' ) + '\'' + _arg + '\''

		## Call our Callback Processor for VarArgs and return the data..
		_output = self.ProcessVarArgs( _args, Callback, 'AddArgDataTypeEntry', 'Added Arg Conversion( s ) from: [ {0} ] > To > [ {1} ] in Category: {2}', str( _datatype ), self.GetCategoryName( _category_type ) );


	##
	## Adds a Typo / Fix Entry for commonly messed up data to look for in code...
	##
	def AddTypoFixEntry( self, _pattern, _error, _solution ):
		## _entry = {
		##	'pattern':	_pattern,
		##	'error':	_error,
		##	'solution': _solution
		## }

		## Entry...
		_entry = {
			_pattern,
			_error,
			_solution
		};

		## Add it..
		self.typo_list.append( _entry );


	##
	## Add Helpful information to the bottom of the XCodeMapper Output ( IF the user wants it ) such as contact information to submit bugs, ask for help, report issues, etc... Tips and tricks, etc...
	##
	def AddHelp( self, _text, _line_number = -1 ):
		self.AddEntry( CATEGORY_HELP, _line_number, _text, 0, 0, '' );


	##
	## Allows you to add Syntax Error Checking for your code and add it to a uniform category...
	##
	def AddError( self, _text, _line_number = -1 ):
		self.AddEntry( CATEGORY_ERROR, _line_number, _text, 0, 0, '' );


	##
	## Adds an entry to the Errors Example Category - this is to show the user how something should look in case they're using XCodeMapper as an educational utility...
	##
	def AddErrorExample( self, _text, _line_number = -1 ):
		self.AddEntry( CATEGORY_ERROR_EXAMPLE, _line_number, _text, 0, 0, '' );


	##
	## Allows you to add Warning Checking for your code and add it to a uniform category...
	##
	def AddWarning( self, _text, _line_number = -1 ):
		self.AddEntry( CATEGORY_WARNING, _line_number, _text, 0, 0, '' );


	##
	## Allows you to add a feature entry - to let the user know what features the current mapper is using......
	##
	def AddFeature( self, _text, _line_number = -1 ):
		self.AddEntry( CATEGORY_MAPPER_FEATURES, _line_number, _text, 0, 0, '' );


	##
	## Helper - Adds a blank-line to the Category ID of your choosing. Also hides line-number hotlink!
	##
	def AddBlankEntry( self, _category, _count = 1 ):
		## Allow multiple blank entries to be added at any given time...
		for _i in range( _count ):
			self.AddEntry( _category, '', self.GetSetting( 'output_entry_blank', '' ), 0, 0, '' );


	##
	## Helper - Aides in avoiding Stack Overflow by simplifying the data needed to add an entry so we have a way to detect if we're adding something from within the PreOnAddEntry Callback, etc...
	##
	def AddSimpleEntry( self, _category, _line_number, _code, _depth, _parent = None ):
		## Call AddEntry without setting the Pattern or the Mode as they're not used in the actual data that is set... This prevents a stack-overflow by preventing a repeat when you look at category and pattern...
		## ( _depth, _depth + 1 )[ _parent != None ];
		self.AddEntry( _category, _line_number, _code, _depth, None, None, _parent );


	##
	## Helper - Converts _arg and DataType to a string based on supplied configuration...
	##
	def GetDataTypeArgString( self, _arg, _category ):
		## Data-Type Arg Output Mode
		## _config =  self.GetSettingENUM( 'datatype_display_mode', 'ENUM_DISPLAY_DATA_TYPE_DEFAULT', ENUM_DISPLAY_DATA_TYPE_DEFAULT )
		_config = self.GetVariableValueByName( self.GetSetting( 'datatype_display_mode', 'ENUM_DISPLAY_DATA_TYPE_DEFAULT' ), 'ENUM_DISPLAY_DATA_TYPE_DEFAULT', ENUM_DISPLAY_DATA_TYPE_DEFAULT );

		## If the config is set to show only the arg, then don't go any further..
		if ( _config == ENUM_DISPLAY_DATA_TYPE_DEFAULT ):
			return _arg;
		else:
			## Grab the Category Type
			_type = str( self.GetCategoryType( _category ) );

			## See if we can read the list...
			_map = self.data_arg_conversion.get( _type, None );

			## If the category isn't set, then just return the argument as we've already know we can't continue as the list isn't initialized..
			if ( _map == None ):
				return _arg;

			## See if the arg exists in the list... If it does, it has a translation to Data-Type..
			_datatype = _map.get( _arg, None );

			## If it doesn't exist, we can't go any further.. Return the submitted arg..
			if ( _datatype == None ):
				return _arg;

			## Grab the Data-Type Prefix, Suffix, and Spacer
			_prefix = self.GetSetting( 'datatype_display_prefix', '<' );
			_suffix = self.GetSetting( 'datatype_display_suffix', '>' );
			_spacer = self.GetSetting( 'datatype_display_spacer', ' ' );

			## If We are set to show BOTH, then we need to load the spacer between the Data-Type and the Arg
			if ( _config == ENUM_DISPLAY_DATA_TYPE_BOTH ):
				_arg = _prefix + _datatype + _suffix + _spacer + _arg;
			elif ( _config == ENUM_DISPLAY_DATA_TYPE_ONLY ):
				_arg = _prefix + _datatype + _suffix;

		return _arg;


	##
	## Helper - Designed to reduce repitition... Used for OnFunction/ClassArgument Callbacks..
	##
	def ProcessEntryArgs( self, _category, _line_number, _code, _depth, _search_mode, _pattern, _func_name ):
		##
		if ( AcecoolLib.String.IndexOfExists( self.GetSetting( 'object_arg_delim_open', '(' ), _code, True ) ):
			##
			## Fix: If multiple ( / )s are found, process each char using the old method - The exact scenario eludes me, but it was where two separate paren-sets where the first one opened and closed before the second one leading to the first paren from the first set being used and the last paren of the last set being used..
			##


			## ## Extract everything before the first (
			## _pre_paren = AcecoolLib.String.Strip( _code, AcecoolLib.String.find( _code, self.GetSetting( 'object_arg_delim_open', '(' ) ) + 1, len( _code ) );

			## ## Strip everything except including and after the last )
			## _end_paren = AcecoolLib.String.Strip( _code, 0, AcecoolLib.String.rfind( self.GetSetting( 'object_arg_delim_close', ')' ), _code ) - 1 );

			## Extract everything before the first (
			_pre_paren = AcecoolLib.String.Strip( _code, _code.find( self.GetSetting( 'object_arg_delim_open', '(' ) ) + 1, len( _code ) );

			## Strip everything except including and after the last )
			_end_paren = AcecoolLib.String.Strip( _code, 0, _code.rfind( self.GetSetting( 'object_arg_delim_close', ')' ) ) - 1 );

			## Grab everything between the ( )s
			_args = AcecoolLib.String.SubStr( _code, len( _pre_paren ), len( _code ) - len( _end_paren ) ).strip( );

			##
			_args_raw = _args;

			## Make sure we have args to process....
			if ( len( _args ) > 0 and _args != '' ):
				## Split the args...
				_args_raw = _args.split( self.GetSetting( 'object_arg_delim_split', ',' ) );

				## Counter Shifter - if '' is returned, then we want to remove it so shift the next into its place...
				_i_shift = 0;

				## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
				self.print( ACMS_PRINT_TYPE_ENTRY_ARGS, 'ProcessEntryArgs', '\nTotal Args: ' + str( len( _args_raw ) ) + ' - args: [ "' + _args + '" ] -\tcode: "' + _code + '":' + str( _line_number ) );

				## Process all of the arguments - run the callback on each to allow for modification...
				for _i, _arg, in enumerate( _args_raw ):
					## Make sure the arg is without spaces before and after...
					_arg = _arg.strip( );

					## _data = self.OnFunctionArgument( _arg, _category, _line_number, _code, _depth, _search_mode, _pattern );

					## Run the callback - OnFunction/ClassArgument and determine whether or not the arg is to be modified!
					_data = getattr( self, _func_name )( _arg, _category, _line_number, _code, _depth, _search_mode, _pattern );

					## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
					_debug_a = 'arg: [ "' + str( _arg ) + '" ]';
					_debug_b = '> into > [ "' + str( _data ) + '" ]';
					self.print( ACMS_PRINT_TYPE_ENTRY_ARGS, 'ProcessEntryArgs', AcecoolLib.String.FormatColumn( 25, _debug_a, len( _debug_b ) + 5, _debug_b ) );

					## Helper
					_overwrite = ( _data == '' or _data == None or _data == True or _data == False );

					## If our callback modifies the arg, then it'll return a string. If None or '' is returned, do nothing...
					if ( _data ):
						_args_raw[ _i + _i_shift ] = AcecoolLib.TernaryFunc( ( _data == None or _data == '' ), _arg, _data );


					## As the above takes precedence - we only modify with DataType + Arg IFF the above didn't modify anything...
					## Task: Modify the above code so everything gets put into GetDataTypeArgString - possibly, or a different function to slim this function down a little bit...
					if ( not _data or _data == _arg ):
						##
						self.print( ACMS_PRINT_TYPE_ENTRY_ARGS, 'ProcessEntryArgs', '_data was not used, now we look at the other option..: ' + _arg );

						## Set the Arg as either itself, or as the Data-Type only or combination with arg depending on settings...
						_args_raw[ _i + _i_shift ] = self.GetDataTypeArgString( _arg, _category );
			else:
				## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
				self.print( ACMS_PRINT_TYPE_ENTRY_ARGS, 'ProcessEntryArgs', '\nTotal Args: 0 -\tcode: "' + _code + '":' + str( _line_number ) );


			## Re-Assemble the code-line using my standard airy format...
			_code = _pre_paren + self.GetSetting( 'object_arg_join_prefix', ' ' ) + self.GetSetting( 'object_arg_delim_join', ' ' ).join( _args_raw ) + self.GetSetting( 'object_arg_join_suffix', ' ' ) + _end_paren;

		## Return the updated line...
		return _code;



	##
	## Internal - Gets the original depth and the new depth and provides the difference... This is so lines which has multiple tabs in the middle can be properly re-tabbed when tabs are subtracted from the beginning..
	##
	##d GetDeltaDepth( self, _category, _line_number, _code, _depth, _search_mode, _pattern ):
	def GetDeltaDepth( self, _depth, _calc_depth ):
		## Grab the calculated depth... If I choose to use all args
		## _calc_depth = self.OnCalcDepth( _category, _line_number, _code, _depth, _search_mode, _pattern );

		## If we add indentation, we return negative value to subtract that depth from the center.. If we subtract depth, we have to return positive to add that depth back to the center...
		## so _depth = 3, _calc = 2 == subtract 1 from start meaning add 1 to later.. positive ... _depth = 3, _calc = 4 meaning indent 1 more meaning subtract from center... 1
		## _depth_delta = _depth - _calc_depth;

		## Return the difference...
		return _depth - _calc_depth;