##
## Garry's Mod Lua - AcecoolDev_Framework Mapper - <Author Name>
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Extend from GMod Lua User - we could also simply use GMod_Lua, or Lua...
from AcecoolCodeMappingSystem.maps.GMod_Lua_User import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin ALua
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Configuration ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##

## Set to true if you want the DataTypes to be short ( ie P instead of Player )
CFG_DISPLAY_ARG_DATA_TYPE_SHORT					= False


##
## Configuration for custom Lua data-type output ( will be moved to settings files soon and generalized ) --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##


##
## Helper Function for Data-Type System: Should we use Long vs Short output?
##
def SetupArgDataTypeOutput( _short, _long = None ):
	## If Config is set to use Short, use short... If set for Long, ie False, then if Long isn't set use Short, otherwise use Long..
	return ( _long, _short )[ CFG_DISPLAY_ARG_DATA_TYPE_SHORT == True or _long == None ]


##
## To be used for Arg <> DataType Translation...
##
DATA_TYPE_ENUM									= SetupArgDataTypeOutput( 'ENUM', 'ENUM' )
ENUM_TYPE_REALM									= DATA_TYPE_ENUM
ENUM_TYPE_NAMESPACE								= DATA_TYPE_ENUM

DATA_TYPE_ANY									= SetupArgDataTypeOutput( '*', '*Any' )
DATA_TYPE_UNKNOWN								= SetupArgDataTypeOutput( '%', '%Unknown' )

## Basic Data-Types
DATA_TYPE_BOOLEAN								= SetupArgDataTypeOutput( 'b', 'Boolean' )
DATA_TYPE_NUMBER								= SetupArgDataTypeOutput( 'n', 'Number' )
DATA_TYPE_NUMBER_EXP							= SetupArgDataTypeOutput( '^', 'Number EXP' )
DATA_TYPE_INTEGER								= SetupArgDataTypeOutput( 'i', 'INTeger' )
DATA_TYPE_FLOAT									= SetupArgDataTypeOutput( 'f', 'Float' )
DATA_TYPE_DOUBLE								= SetupArgDataTypeOutput( 'd', 'Double' )
DATA_TYPE_STRING								= SetupArgDataTypeOutput( 'S', 'String' )

## Special Data-Types
DATA_TYPE_VARARGS								= SetupArgDataTypeOutput( '...', '...' ) # ☼☼☼
DATA_TYPE_DELTA									= SetupArgDataTypeOutput( '▲', '▲' )

## Object / Other Data-Types
DATA_TYPE_FUNCTION								= SetupArgDataTypeOutput( 'F', 'function' )
DATA_TYPE_FUNCTION_CALLBACK						= SetupArgDataTypeOutput( 'Fc', 'Callback' )
DATA_TYPE_FUNCTION_REFERENCE					= SetupArgDataTypeOutput( 'Fr', 'FUNC_REF' )
DATA_TYPE_TABLE									= SetupArgDataTypeOutput( 'T', 'Table' )
DATA_TYPE_TABLE_INDEX							= SetupArgDataTypeOutput( 'Ti', 'Table-Index' )

DATA_TYPE_VECTOR								= SetupArgDataTypeOutput( 'V', 'Vector' )
DATA_TYPE_VECTOR_NORM							= SetupArgDataTypeOutput( 'Vn', 'VectorNorm' )
DATA_TYPE_ANGLE									= SetupArgDataTypeOutput( 'A', 'Angle' )
DATA_TYPE_COLOR									= SetupArgDataTypeOutput( 'C', 'Color' )
DATA_TYPE_COLOR_ALPHA							= SetupArgDataTypeOutput( 'Ca', 'ColorAlpha' )

DATA_TYPE_BULLET_TABLE							= SetupArgDataTypeOutput( 'BulletS', 'BulletStruct' )
DATA_TYPE_META_TABLE							= SetupArgDataTypeOutput( 'Tm', 'Meta-Table' )
DATA_TYPE_DAMAGEINFO							= SetupArgDataTypeOutput( 'Dmg', 'DmgInfo' )

DATA_TYPE_CAMERA								= SetupArgDataTypeOutput( 'Cam', 'Camera' )

DATA_TYPE_MATRIX								= SetupArgDataTypeOutput( 'Matrix', 'Matrix' )

DATA_TYPE_OBJECT								= SetupArgDataTypeOutput( 'Obj', 'Object' )
DATA_TYPE_TIMEKEEPER							= SetupArgDataTypeOutput( 'TimeK', 'TimeKeeper' )

DATA_TYPE_ENTITY								= SetupArgDataTypeOutput( 'Ent', 'Entity' )
DATA_TYPE_ENTITY_PLAYER							= SetupArgDataTypeOutput( 'P', 'Player' )
DATA_TYPE_ENTITY_PLAYER_LOCAL					= SetupArgDataTypeOutput( 'LP', 'LocalPlayer' )
DATA_TYPE_ENTITY_VEHICLE						= SetupArgDataTypeOutput( 'V', 'Vehicle' )
DATA_TYPE_ENTITY_WEAPON							= SetupArgDataTypeOutput( 'W', 'Weapon' )
DATA_TYPE_ENTITY_NPC							= SetupArgDataTypeOutput( 'NPC', 'NPC' )
DATA_TYPE_ENTITY_PHYSICS						= SetupArgDataTypeOutput( 'PhysE', 'PhysicsEnt' )
DATA_TYPE_ENTITY_NEXTBOT						= SetupArgDataTypeOutput( 'NBot', 'NextBot' )

DATA_TYPE_EFFECT								= SetupArgDataTypeOutput( 'Effect', 'Effect' )
DATA_TYPE_EFFECT_DATA							= SetupArgDataTypeOutput( 'EffectData', 'EffectData' )
DATA_TYPE_EFFECT_EMITTER						= SetupArgDataTypeOutput( 'Emitter', 'Emitter' )
DATA_TYPE_EFFECT_PARTICLE						= SetupArgDataTypeOutput( 'Particle', 'Particle' )

DATA_TYPE_VGUI_PANEL							= SetupArgDataTypeOutput( 'Panel', 'Panel' )
DATA_TYPE_IMATERIAL								= SetupArgDataTypeOutput( 'IMaterial', 'IMaterial' )
DATA_TYPE_TEXTURE								= SetupArgDataTypeOutput( 'ITexture', 'ITexture' )
DATA_TYPE_AUDIO									= SetupArgDataTypeOutput( 'AudioSource', 'AudioSource' )
DATA_TYPE_LOCOMOTION							= SetupArgDataTypeOutput( 'LocoMotion', 'LocoMotion' )
DATA_TYPE_PATH_FOLLOWER							= SetupArgDataTypeOutput( 'PathFollower', 'PathFollower' )

DATA_TYPE_TRACE									= SetupArgDataTypeOutput( 'Tr', 'Trace' )
DATA_TYPE_TRACE_RESULT							= SetupArgDataTypeOutput( 'TrR', 'TraceResult' )

DATA_TYPE_SIZE_BYTES							= SetupArgDataTypeOutput( 'Bytes', 'Bytes' )




##
## AcecoolDev_Framework, Garry's Mod, and Vanilla Lua Mappings
##
class ACMS_GMod_Lua_AcecoolDev_Framework( ACMS_GMod_Lua_User ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_GMod_Lua_Acecool'


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	##	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	##	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	##	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' )

		## TODO: Turn the line-processing system into a helper function so it can call itself - ie strip comments from a line and call the code in one call ( for classes, functions, AccessorFuncs, etc.. ) and the stripped comment in another ( to look for TODO lines )
		## From python Callback below...
		## Note: When declaring what is a comment, please remember if you set up your comments other than the default ( line 2 ) such as seen below ( line 1 ) you must put the alteration first so it can be stripped to look for TODO lines...
		## Or you can use a more expensive search method for looking for TODO lines which will miss less ( such as if the end of a declaration, on the same line, has a comment ) by using SEARCH_TYPE_CONTAINS - but, soon, I will set up a helper
		## function to handle parsing lines so if a line needs to be called once for the basics, then again on a stripped comment it won't be tedious... ie the loop on each line would be for line, code ... process_line( line, code ) and that'd
		## call itself with the stripped comment, for example...

		## Lua Vanilla - Note: Because comment-block for Lua starts with comment of Lua, It needs to be in this SPECIFIC ORDER: Comment block begin, COMMENT, COMMENT BLOCK END...
		## If not in this order ie comment first then comment block will bug out... if comment is last then it'll RE INIT comments on lines that have already been processed...
		## self.AddSyntax( CATEGORY_COMMENT_BLOCK_BEGIN, SEARCH_TYPE_CONTAINS, '--[[', '/*' )
		## self.AddSyntax( CATEGORY_COMMENT, SEARCH_TYPE_STARTSWITH, '--', '//' )
		## self.AddSyntax( CATEGORY_COMMENT_BLOCK_FINISH, SEARCH_TYPE_CONTAINS, ']]', '*/' )

		## Networking
		self.AddSyntax( CATEGORY_NETWORKING, SEARCH_TYPE_STARTSWITH, 'Acecool.C.networking:Send', 'Acecool.C.networking:Broadcast' )

		##
		self.AddSyntax( CATEGORY_FUNCTION_ACCESSOR, SEARCH_TYPE_STARTSWITH, 'Acecool:AddClassOverrides', 'Acecool:AccessorFunc', 'Acecool:AccessorSet', 'Acecool:AccessorGet', 'Acecool:AccessorSimple', 'admin:AddTool' )

		## Function Callback Related
		self.AddSyntax( CATEGORY_FUNCTION_CALLBACK, SEARCH_TYPE_STARTSWITH, 'Acecool.C.networking:SetHelper', 'Acecool.C.networking:AddReceiver' )

		## Console Commands
		self.AddSyntax( CATEGORY_CMD_CONSOLE, SEARCH_TYPE_STARTSWITH, 'dev_example.Add' )


		## Registry
		self.AddSyntax( CATEGORY_REGISTRY, SEARCH_TYPE_STARTSWITH, 'Acecool.C.networking:RegisterReceiver', 'Acecool.C.networking:RegisterSyncableFlag' )

		## Function Acecool.C. - funtion definitions... I use the META_ prefix for all of my META_ replacements such as META_ENTITY, META_PLAYER, etc... so they remain in AcecoolDev_Framework...
		self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_STARTSWITH, 'function Acecool.C.' )
		self.AddSyntax( CATEGORY_FUNCTION_HOOK, SEARCH_TYPE_STARTSWITH, 'draw.This', 'render.This', 'hook.AddTemp' )

		## Note: This is called before super( ).OnSetupSyntax( ) meaning it will be spotted prior to local function and function search terms in Lua Vanilla using this path: GMod_Lua_User > GMod_Lua_Default > Lua_User > Lua_Default > XCodeMapper BASE
		self.AddSyntax( CATEGORY_FUNCTION_META, SEARCH_TYPE_STARTSWITH, 'function META_' )

		## For everything else which starts with Acecool.C. - add it to the definitions category
		self.AddSyntax( CATEGORY_DEFINITION, SEARCH_TYPE_STARTSWITH, 'Acecool.C.' )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupSyntax( )



	##
	## Called for each function argument - allows you to do anything you want...
	##
	## example: by replacing _p and _w which are hard-coded variable-names in my coding standard..
	##	function GM:PlayerDeath( _p, _w )
	## can become
	##	function GM:PlayerDeath( <Player>, <Weapon> )
	## or
	##	function GM:PlayerDeath( <Player> _p, <Weapon> _w )
	##
	def OnFunctionArgument( self, _arg, _category, _line_number, _code, _depth, _mode, _pattern ):
		##
		## If argument is known to my coding standard, replace with the data-type....
		## Note: You can append the arg, or you can replace completely... You can use data-type or other info... I recommend making the info CLEAR when you replace something..
		##

		## Determine which quotes the arg is using, if any...
		_type_quote = AcecoolLib.String.GetQuotesType( _arg )
		_quotes_double = AcecoolLib.Logic.TernaryFunc( _type_quote == '"', True, False )
		_quotes_single = AcecoolLib.Logic.TernaryFunc( _type_quote == "'", True, False )

		## Remove the quotes from the arguments... We'll re-add them later..
		_arg = AcecoolLib.Logic.TernaryFunc( ( _quotes_double == True or _quotes_single == True ), AcecoolLib.String.StripQuotes( _arg ), _arg )

		## If a function was used as an argument - it gets replaced with FUNC_REF so we can set what the function should look like - including all args - in place of it.. especially if other arguments come after as multi-line scanning isn't enabled in this mapper..
		if ( _arg == 'FUNC_REF' ):
			if ( _pattern == 'Acecool.C.networking:SetHelper' ):
				_arg = 'function( _p, _ent, _flag, _value, _private, _category ) ... end'

			if ( _arg == 'FUNC_REF' ):
				_arg = 'function( ... ) ... end'

		##
		if ( not _arg.startswith( '_' ) and not _arg.startswith( 'function' ) and not _arg.startswith( ');' ) and not _arg.startswith( '"' ) and not _arg.startswith( "'" ) and _arg != 'self' and _arg != '...' and _arg != '' and _arg != 'FUNC_REF' ):
			## If it is something all upper-case then allow it...
			if ( _arg !=_arg.upper( ) ):
				self.AddWarning( '[ Coding Standards Issue ] Arg: ' + _arg, _line_number )

		## else:
			## ( _arg != 'If arg doesn\'t exist in list of approved arguments, then we show a warning message..' )
			## self.AddWarning( 'Argument is not in approved Coding Standards List', _line_number )

		if ( _quotes_double == True ):
			_arg = '"' + _arg + '"'

		if ( _quotes_single == True ):
			_arg = "'" + _arg + "'"


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).OnFunctionArgument( _arg, _category, _line_number, _code, _depth, _mode, _pattern )


	##
	## Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original ):
		##
		## Remove Acecool.C. from entries to shorten them - This isn't necessary when displaying class-functions in nested categories as Acecool.C. will only appear in category names...
		##
		## Note: May need to be output_class instead...
		if ( self.GetSetting( 'output_library_functions_in_new_category', False ) == False and _category != CATEGORY_DEFINITION ):
			## The search term we're looking for
			_search = 'Acecool.C.'

			## Remove it...
			_code = AcecoolLib.String.StripAll( _search, _code )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )



	##
	## Setup Data-Type to Arg Conversion. This is used for each function / class argument... This feature is to give more information to the overview panel so it can appear similar to a WIKI overview for all of the data without the long descriptions ( depending on the cfg )
	## The entire systems purpose is to give just enough data to be incredibly useful, but not enough to overwhelm or confuse.. Keep it concise.. This feature can inform you of the data-type to expect to use. For this to work, you need to set and follow coding-standards - more specifically: Argument Naming Standards sub-section.
	##
	## If the configuration is set up to do anything ( ie non-default ) then these are processed. If set to Default or _ARG then nothing happens...
	## If set to Both then they'll appear as: '<DataType> _arg' / If set to Data-Type the args will be replaced with only: '<DataType>'
	## The Space after the suffix, prefix as '<', and suffix as '>' chars and the arg is controlled by the Spacer, Prefix and Suffix Configuration Options respectfully.
	##
	def OnSetupArgDataType( self ):
		## VarArgs > Convert ... to the value set to DATA_TYPE_VARARGS but only when inside of a FUNCTION!!!
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_VARARGS, '...', )

		## Entities > Convert _ent to DATA_TYPE_ENTITY, and so on for these uniques but only when inside of a FUNCTION!!!
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY, '_ent' )
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY_PLAYER, '_p', '_ply', '_player', '_attacker', '_victim' )
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY_PLAYER_LOCAL, '_lp', '_client' )
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY_WEAPON, '_w', '_weapon', '_swep' )
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY_VEHICLE, '_v', '_veh', '_vehicle' )
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY_NEXTBOT, '_nextbot' )
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY_NEXTBOT, '_bot' )
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY_PHYSICS, '_phys' )
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY_NPC, '_npc' )

		## Camera > Convert _vm to DATA_TYPE_CAMERA in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_CAMERA, '_vm' )

		## Numbers > Convert these _* values to DATA_TYPE_NUMBER in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_NUMBER, '_delay', '__category', '_start_value', '_target_value', '_target_alpha', '_count', '_radius', '_duration', '_cycle', '_id' )

		## Booleans < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_BOOLEAN, '_private', '_shouldsync', '_autocomplete', '_desc', '_show_delay', '_override', '_request', '_active' )

		## Strings < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_STRING, '_name', '_flag', '_category', '_help', '_text', '_chars', '_hook', '_lang', '_phrase', '_extends_name', '_getter_prefix', '_path', '_file', '_string', '_prefix', '_suffix' )

		## Functions < _* Conversion in Category Type FUNCTION - note, most of these are callbacks...
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_FUNCTION, '_func', '_callback', '_callback_getter', '_callback_setter', '_info', 'FUNC_REF', '_func_oninit', '_func_onrefresh', '_func_onshutdown', '_func_preoninit', '_func_postoninit', '_func_preonrefresh', '_func_postonrefresh', '_func_preonshutdown', '_func_postonshutdown' )

		## Byte-Size < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_SIZE_BYTES, '_bytes' )

		## Meta-Tables < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_META_TABLE, '_meta', '__object', '_xdir', '_time', '_parent' )

		## Tables < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_TABLE, '_map', '_values', '_forced', '_allowed', '_types', '_force_types', '_force_values', '_library', '_table', '_tab', '_namespace', '_players', '_packet', '_flags', '_data_tab', '_options' )

		## Any Data-Type Acceptable < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ANY, '_data', '_default', '_value', '_start', '_key', '_index' )

		## ENUMeration < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENUM, '_datatype', '_typeid', '_mode', '_type', '_realm', '_instruction' )

		## Angles < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ANGLE, '_start_angle', '_target_angle', '_ang' )

		## Vectors < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_VECTOR, '_start_vector', '_target_vector', '_pos', '_vector' )

		## Colors < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_COLOR, '_start_color', '_target_color', '_color', '_col' )

		## Color Alpha < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_COLOR_ALPHA, '_target_color_alpha' )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupArgDataType( )


	##
	## When an AccessorFunc type is found, this is the callback executed which allows you to expand the AccessorFunc into multiple entries...
	##
	## example: When encountering a GMod Lua / GLua util function, it is expanded into 2 entries representative of the 2 functions AccessorFunc creates..
	##
	## Code:
	##	AccessorFunc( perp.lighting, "somevar", "LightsEnabled", FORCE_BOOL )
	##
	## XCodeMapper Entries - I set it to add into CATEGORY_FUNCTION_ACCESSOR categories:
	##	function perp.lighting:GetLightsEnabled( );:102
	##	function perp.lighting:SetLightsEnabled( _value :: Note: _value forced tobool( ) on set! );:102
	##
	##
	## Because each AccessorFunc creation method is different for each function we look for... they're all routed through here to be expanded upon...
	## If pattern x then args[ 1 ] = classname, args[ 2 ] = name ( so S/Get<Name> etc.. ), and more... so if args[ n ] is "" then we don't add Get prefix to the Getter.. and so on..
	## Also, we can force the data to show up in different categories.. for example __ functions can be put into CATEGORY_FUNCTION_INTERNAL which appears last in list of functions..
	## or we can determine whether or not x function was already added which has an optional creation function ( ie on first time call of an accessor func, it
	##	creates __GetDataTable, __GetData, __SetData internal function but the next time we call it on the same class, it shouldn't output these ).. etc...
	##
	## The benefit of this function is that you can do ANYTHING you want with the data of AccessorFuncs ( I may add callbacks for each category type or search later on, but right now AccessorFunc is what needs it )
	##
	def OnAccessorFuncExpansion( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Base Args - _tab will be the first arg in all cases...
		_arg_tab = self.GetTableValue( _args, _args_count, 0, 'CLASS_NONE' )

		## This is the first arg without any decimals - and we remove everything before last decimal in TitleCase - ClassName ie Timekeeper or Data...
		_arg_class = AcecoolLib.String.SubStr( _arg_tab, 10, len( _arg_tab ) ).title( )


		##
		## In an attempt to slim this down to make it easier to read what was added I've converted them into helper functions...
		##

		## Single Entry... the name of the admin tool...
		if ( _pattern == 'admin:AddTool' ):
			self.AddEntry( CATEGORY_FUNCTION_OTHER, _line_number, 'Admin Tool: ' + self.GetTableValue( _args, _args_count, 1, 'NAME_NONE' ), _depth, _mode, _pattern )

		## function Acecool:AccessorSimple( _tab, _name, _callback )
		if ( _pattern == 'Acecool:AccessorSimple' ):
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'function ' + _arg_tab + ':__Set' + self.GetTableValue( _args, _args_count, 1, 'NAME_NONE' ) + '( _value );', _depth, _mode, _pattern )

		## 3 to 9 entries based on config values... OnInit, OnShutdown, OnRefresh and Pre / Post of all 3...
		if ( _pattern == 'Acecool:AddClassOverrides' ):
			self.AddAccessorFuncEntries_AcecoolAddClassOverrides( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )

		## Full AccessorFunc functions list - all helpers for the class, setters and getters including internals...
		if ( _pattern == 'Acecool:AccessorFunc' ):
			self.AddAccessorFuncEntries_AcecoolAccessorFunc( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )

		## Setters and Setter Internals only..
		if ( _pattern == 'Acecool:AccessorSet' ):
			self.AddAccessorFuncEntries_AcecoolAccessorSet( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )

		## Getters and Getter Internals only...
		if ( _pattern == 'Acecool:AccessorGet' ):
			self.AddAccessorFuncEntries_AcecoolAccessorGet( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnAccessorFuncExpansion( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


	##
	## function Acecool:AccessorFunc( _tab, _name, _default, _force_types, _force_values, _getter_prefix, _callback_getter, _callback_setter, _key ):1177
	## Note: As Acecool:AccessorFunc uses helper-functions to create everything and since we can call those - we're defining vars and setting them to true so we can ensure we don't repeat code by adding funcs here and in the helpers... this way we only need to update one location...
	##
	## Calls Acecool:AccessorSet( ), Acecool:AccessorGet( ), Acecool:AccessorHelpers( )
	##
	def AddAccessorFuncEntries_AcecoolAccessorFunc( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		self.AddAccessorFuncEntries_AcecoolHelpers( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )
		self.AddAccessorFuncEntries_AcecoolHelpersKey( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )
		self.AddAccessorFuncEntries_AcecoolAccessorSet( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )
		self.AddAccessorFuncEntries_AcecoolAccessorGet( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count, self.GetTableValue( _args, _args_count, 5, 'Get' ) )



	##
	## Looks for: function Acecool:AccessorSet( _tab, _name, _force_types, _force_values, _callback, _key, _extends_name )	in order to create the following helper-functions ( Whether or not they show in the Code - Map Panel is a matter of configuration )
	##
	## Always Adds to Code - Map: function <TAB>:Set<Name>( _value );
	## Configuration based additions: function <TAB>:__Has<Name>AllowedTypes( ),	<TAB>:__Get<Name>AllowedTypes( ),	<TAB>:__Set<Name>AllowedTypes( )
	## Configuration based additions: function <TAB>:__Has<Name>AllowedValues( ),	<TAB>:__Get<Name>AllowedValues( ),	<TAB>:__Set<Name>AllowedValues( )
	##
	def AddAccessorFuncEntries_AcecoolAccessorSet( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Helpers
		_arg_tab	= self.GetTableValue( _args, _args_count, 0, 'CLASS_NONE' )
		_arg_name = self.GetTableValue( _args, _args_count, 1, 'NAME_NONE' )

		## Forced Types / Values used in the Setter - if not either the data-type, or value, or both then the data isn't set
		_arg_force_types = self.GetTableValue( _args, _args_count, AcecoolLib.Logic.TernaryFunc( ( _pattern == 'Acecool:AccessorFunc' ), 3, 2 ), '' )
		if ( _arg_force_types != '' ): _arg_force_types = ' ForcedTypes = [ ' + _arg_force_types + ' ];'

		_arg_force_values = self.GetTableValue( _args, _args_count, AcecoolLib.Logic.TernaryFunc( ( _pattern == 'Acecool:AccessorFunc' ), 4, 3 ), '' )
		if ( _arg_force_values != '' ): _arg_force_values = ' ForcedValues = [ ' + _arg_force_values + ' ];'

		## A callback to manipulate the value prior to it being returned
		_arg_callback = self.GetTableValue( _args, _args_count, AcecoolLib.Logic.TernaryFunc( ( _pattern == 'Acecool:AccessorFunc' ), 7, 4 ), '' )
		if ( _arg_callback != '' ): _arg_callback = ' Callback = [ ' + _arg_callback + ' ];'

		##
		## _accessor_helpers = True
		self.AddAccessorFuncEntries_AcecoolHelpers( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )
		self.AddAccessorFuncEntries_AcecoolHelpersKey( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )
		## _accessor_helpers_set = True

		## function Acecool.C.<ClassName>:Set<FuncName>( _value );
		self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'function ' + _arg_tab + ':Set' + _arg_name + '( _value );' + _arg_force_types + _arg_force_values + _arg_callback, _depth, _mode, _pattern )

		## If the configuration value is True, then add the internal helper functions to the Code - Map Panel
		if ( self.GetSetting( 'output_accessorfunc_internals', False ) ):
			## Adds helper-functions to determine whether Allowed-Types were set so they can be enforced elsewhere, to retrieve them and to reassign them using:	function class:__Has<Name>AllowedTypes( );	class:__Get<Name>AllowedTypes( _default );	class:__Set<Name>AllowedTypes( _types );
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Has' + _arg_name + 'AllowedTypes( );' + _arg_force_types, _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Get' + _arg_name + 'AllowedTypes( _default );', _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Set' + _arg_name + 'AllowedTypes( _types );', _depth, _mode, _pattern )

			## Adds helper-functions to determine whether Allowed-Values were set so they can be enforced elsewhere, to retrieve them and to reassign them using: function class:__Has<Name>AllowedValues( );	class:__Get<Name>AllowedValues( _default ); class:__Set<Name>AllowedValues( _types );
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Has' + _arg_name + 'AllowedValues( );' + _arg_force_values, _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Get' + _arg_name + 'AllowedValues( _default );', _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Set' + _arg_name + 'AllowedValues( _values );', _depth, _mode, _pattern )



	##
	## function Acecool:AccessorGet( _tab, _name, _default, _callback, _key ):1058
	##
	def AddAccessorFuncEntries_AcecoolAccessorGet( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count, _arg_getter_prefix = '' ):
		## Helpers
		_arg_tab	= self.GetTableValue( _args, _args_count, 0, 'CLASS_NONE' )
		_arg_name	= self.GetTableValue( _args, _args_count, 1, 'NAME_NONE' )

		##
		## _accessor_helpers = True
		self.AddAccessorFuncEntries_AcecoolHelpers( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )
		_accessor_helpers_get = True

		## function Acecool.C.<ClassName>:<Prefix><FuncName>( _default );
		self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'function ' + _arg_tab + ':' + _arg_getter_prefix + _arg_name + '( _default );', _depth, _mode, _pattern )

		## function Acecool.C.<ClassName>:__Get<Name>DefaultValue
		## function Acecool.C.<ClassName>:__Set<Name>DefaultValue
		if ( self.GetSetting( 'output_accessorfunc_internals', False ) ):
			_arg_name = self.GetTableValue( _args, _args_count, 1, '<_arg_name>' )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Get' + _arg_name + 'DefaultValue( );', _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Set' + _arg_name + 'DefaultValue( _value );', _depth, _mode, _pattern )


	##
	## function Acecool:AddClassOverrides( _tab, _func_oninit, _func_onrefresh, _func_onshutdown, _func_preoninit, _func_postoninit, _func_preonrefresh, _func_postonrefresh, _func_preonshutdown, _func_postonshutdown )
	## Creates: function <TAB>:OnInit( ); <TAB>:Pre<Name>OnInit( ); <TAB>:Post<Name>OnInit( );
	## Creates: function <TAB>:OnRefresh( ); <TAB>:Pre<Name>OnRefresh( ); <TAB>:Post<Name>OnRefresh( );
	## Creates: function <TAB>:OnShutdown( ); <TAB>:Pre<Name>OnShutdown( ); <TAB>:Post<Name>OnShutdown( );
	## Calls: Acecool:AccessorHelpers( );
	##
	def AddAccessorFuncEntries_AcecoolAddClassOverrides( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Helpers
		_arg_tab	= self.GetTableValue( _args, _args_count, 0, 'CLASS_NONE' )
		_arg_key	= self.GetTableValue( _args, _args_count, 1, 'KEY_NONE' )
		_arg_name	= self.GetTableValue( _args, _args_count, 2, 'NAME_NONE' )
		_arg_force	= self.GetTableValue( _args, _args_count, 3, 'FORCE_NONE' )
		_arg_class	= AcecoolLib.String.SubStr( _arg_tab, 10, len( _arg_tab ) ).title( )

		## Add 3 internal functions: OnInit, OnRefresh and OnShutdown
		self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':OnInit( );', _depth, _mode, _pattern )
		self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':OnRefresh( );', _depth, _mode, _pattern )
		self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':OnShutdown( );', _depth, _mode, _pattern )

		## Add 6 hooks / callbacks for developers: Pre / Post <Class> OnInit, OnRefresh and OnShutdown
		self.AddEntry( CATEGORY_FUNCTION_CALLBACK, _line_number, 'function ' + _arg_tab + ':Pre' + _arg_class + 'OnInit( );', _depth, _mode, _pattern )
		self.AddEntry( CATEGORY_FUNCTION_CALLBACK, _line_number, 'function ' + _arg_tab + ':Pre' + _arg_class + 'OnRefresh( );', _depth, _mode, _pattern )
		self.AddEntry( CATEGORY_FUNCTION_CALLBACK, _line_number, 'function ' + _arg_tab + ':Pre' + _arg_class + 'OnShutdown( );', _depth, _mode, _pattern )
		self.AddEntry( CATEGORY_FUNCTION_CALLBACK, _line_number, 'function ' + _arg_tab + ':Post' + _arg_class + 'OnInit( );', _depth, _mode, _pattern )
		self.AddEntry( CATEGORY_FUNCTION_CALLBACK, _line_number, 'function ' + _arg_tab + ':Post' + _arg_class + 'OnRefresh( );', _depth, _mode, _pattern )
		self.AddEntry( CATEGORY_FUNCTION_CALLBACK, _line_number, 'function ' + _arg_tab + ':Post' + _arg_class + 'OnShutdown( );', _depth, _mode, _pattern )

		## Add the helpers
		self.AddAccessorFuncEntries_AcecoolHelpers( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


	##
	## function Acecool.C.<ClassName>:__GetDataTable( );
	## function Acecool.C.<ClassName>:__SetData( _key, _value );
	## function Acecool.C.<ClassName>:__GetData( _key, _default );
	##
	def AddAccessorFuncEntries_AcecoolHelpers( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Helpers
		_arg_tab	= self.GetTableValue( _args, _args_count, 0, 'CLASS_NONE' )

		## Grab the value for the current class... if it exists - otherwise false...
		_exists = self.AccessorFuncHelpersDefined.get( _arg_tab, False )

		## If no entry exists for this table - we can add the helper functions...
		if ( _exists == False ):
			## Add the output for GetDataTable, GetData, SetData if it hasn't been shown before...
			self.AccessorFuncHelpersDefined[ _arg_tab ] = True
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__GetDataTable( );', _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__SetData( _key, _value );', _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__GetData( _key, _default );', _depth, _mode, _pattern )


	##
	## function Acecool.C.<ClassName>:__Get<Name>Key
	## function Acecool.C.<ClassName>:__Set<Name>Key
	##
	def AddAccessorFuncEntries_AcecoolHelpersKey( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## If we allow these functions to be added ( can accumulate quickly )...
		if ( self.GetSetting( 'output_accessorfunc_internals', False ) ):
			## Helpers
			_arg_tab	= self.GetTableValue( _args, _args_count, 0, 'CLASS_NONE' )
			_arg_name	= self.GetTableValue( _args, _args_count, 1, '<NAME_NONE>' )

			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Get' + _arg_name + 'Key( );', _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Set' + _arg_name + 'Key( _key );', _depth, _mode, _pattern )


	##
	##
	##
	def Snippets( self ):
		pass


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End ALua
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------