##
## PHP - Default / Vanilla / Base Mapper - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent ACMS Mapper - As this is the User mapper, I've set up all possible includes for this mapper out!
from AcecoolCodeMappingSystem.maps.__mapper_imports__ import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##  Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
class ACMS_PHP_Default( XCodeMapperBase ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_PHP_Default'


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' )

		## Comments
		self.AddSyntax( CATEGORY_COMMENT, SEARCH_TYPE_STARTSWITH, '//' )
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_BEGIN, SEARCH_TYPE_CONTAINS, '/*' )
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_FINISH, SEARCH_TYPE_CONTAINS, '*/' )

		## Determine when php is active and when it isn't...
		self.AddSyntax( CATEGORY_INFO, SEARCH_TYPE_CONTAINS, '<?php', '<?', '?>' )

		## Functions and Class - Since python has classes and because I haven't extended the category system, yet, to allow nesting which class belongs to which class ( even though dynamic categories could be generated ), I am putting them both in the class category and I've enabled Callback.OnCalcDepth to indent them to differentiate them better..
		## Note: You can replace StartsWith with Contains and remove the 3 after as the first will map all if set to contains.
		self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_STARTSWITH, 'function ', 'public function', 'private function', 'static function' )
		self.AddSyntax( CATEGORY_CLASS, SEARCH_TYPE_STARTSWITH, 'class ' )

		## Map Inclue lines..
		self.AddSyntax( CATEGORY_IMPORT, SEARCH_TYPE_STARTSWITH, 'require(', 'require_once(', 'include(', 'include_once(' )


		## Other definitions... These can be uncommented if you want them..
		## Note: Debugging category looks within comments.. I may change this later - for now I've used category info to show the tries, catches, throwing exceptions,
		## self.AddSyntax( CATEGORY_INFO, SEARCH_TYPE_CONTAINS, 'try', 'catch', 'Exception', 'throw', 'finally' )
		## self.AddSyntax( CATEGORY_DEFINITION, SEARCH_TYPE_STARTSWITH, 'new ', 'array', 'real', 'double', 'float', 'integer', 'int', 'boolean', 'bool', 'string', 'object', 'unset', 'class', 'clone', 'var', 'function', 'interface', 'object' )
		## self.AddSyntax( CATEGORY_DEFINITION, SEARCH_TYPE_STARTSWITH, 'global', 'abstract', 'const', 'extends', 'implements', 'final', 'static', 'private', 'protected', 'public' )


		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )


	##
	## Override / Callback - Used to define the rules of the language - ie when to parse PHP and when not to so we know when to switch languages, etc..
	## Note: Not active - but this will be what it will look like - or thereabouts..
	## Note: As the design is still being created for how to accomplish live-switching of syntax mappers, this is subject to change. It exists here to serve as how the system should look on the front-end...
	##
	def OnSetupSyntaxRules( self ):
		##
		## Note: Design AddSyntaxRule - It needs to be plyable so possibly allow it to run any accessor-func by name to set the value based on search-results? That'd make things easier so instead of if X then set( Y ) or so, it can be one line...
		##

		## Define the PHP Start markers... If using full-tags, short-tags, quick-entry for forms, etc...
		self.AddSyntaxRule( 'SetActiveLanguage', 'php', SEARCH_TYPE_CONTAINS, '<?php', '<?', '<?=' )

		## If coming out of the PHP tag, then set the language to html...
		self.AddSyntaxRule( 'SetActiveLanguage', 'html', SEARCH_TYPE_CONTAINS, '?>' )



		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntaxRules( )



	##
	## Override / Callback - Used to determine which syntax extension from XCodeMapper to use... Return the file extension - the rest is handled elsewhere... Each extension is exactly like php.py, it has their own config, etc...
	## Note: Not active - but this will be what it will look like - or thereabouts..
	## Note: As the design is still being created for how to accomplish live-switching of syntax mappers, this is subject to change. It exists here to serve as how the system should look on the front-end... This may not be necessary with the SyntaxRules system - however it may serve as an additional layer to easily override the default settings for child-classes..
	##
	def OnUseSyntax( self ):
		## PHP Is primary
		if ( self.IsActiveLanguage( 'php' ) ):
			return 'php'
		## SQL is secondary to PHP because SQL is used within PHP bounds...
		elif( self.IsActiveLanguage( 'sql' ) ):
			return 'sql'
		## HTML Is secondary primary language of a .php file ( because HTML is what is used when outside of php tags and because HTML decides when JavaScript is used )
		elif( self.IsActiveLanguage( 'html' ) or self.IsActiveLanguage( 'htm' ) ):
			return 'html'
		## CSS is a subset of HTML - ie HTML decides when JavaScript OR CSS is used based on <style> or <script> tags..
		elif ( self.IsActiveLanguage( 'css' ) ):
			return 'css'
		## JavaScript is a subset of HTML - ie HTML decides when JavaScript OR CSS is used based on <style> or <script> tags..
		elif( self.IsActiveLanguage( 'javascript' ) or self.IsActiveLanguage( 'js' ) ):
			return 'js'


		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		super( ).OnUseSyntax( )


	##
	## Override / Helper to allow for depth to be all uniform or based on indentation ( if you return _depth )
	##
	def OnCalcDepth( self, _category, _line_number, _code, _depth, _mode, _pattern ):
		## ## Make sure the class-headers table keeps all of the classes at the same level...
		## if ( _category == CATEGORY_CLASS ):
		## 	return 0

		## ## If not a class specified above, use the standard depth...
		## return _depth

		## Keep everything at 0 depth for now..
		return 0


	##
	## Callback - Called for each line for additional error-checking and for processing rules for class recognition, stack popping, etc...
	##
	def OnProcessLine( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		## Note: Still working on the algorithm - it'll likely be easier with PHP because of { and } but I don't want to rely on them because single-lined calls don't need them... I won't add the class-child system here until I've perfected the algorithm..
		pass


	##
	## Callback - Used to either prevent an entry from being added by returning True / False, to alter the code being added by returning a String, to look for Syntax or other errors and add an Error Entry or Warning Entry, etc..
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original ):
		##
		## If we're looking for our php tags and the current category for something which was found is in category info
		##
		if ( _category == CATEGORY_INFO ):
			## And the pattern for the object which was found is one of the PHP tags....
			if ( _pattern == '<?' or _pattern == '<?php' ):
				## _code = '\n' + self.GetDepthString( 1 ) + 'PHP Code is being parsed as of line'

				## For now, prevent these entries from showing up as they can become quite numerous depending on the file - I've been using syntax_test_php.php for testing, and there are a lot of these...
				return False

			##
			if ( _pattern == '?>' ):
				_code = 'PHP Code isn`t being parsed at line'

				## For now, prevent these entries from showing up as they can become quite numerous depending on the file - I've been using syntax_test_php.php for testing, and there are a lot of these...
				return False


		##
		## Insert Function into Class Nested Category...
		## Note: This can stay - it is only active if ActiveClass is set... Beause the OnProcessLine is empty, this can stay - all this does is redirect a CATEGORY_FUNCTION discovered line to the CATEGORY_CLASS_FUNCTION so it can be nested underneath a class... Global / local functions will still go to the normal functions category..
		##
		if ( _category == CATEGORY_FUNCTION ):
			## Grab our class category...
			_class_cat = self.GetActiveClass( )

			## If the class has a category of its own - ie the class was set... move our function into it...
			if ( self.GetActiveClass( ) != None ):
				## Add the function under the class category...
				self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, _code, _depth, _mode, _pattern, self.GetActiveClass( ) )

				## Return True / False, etc.. in order to prevent this entry from being added because we're adding it above..
				return False


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------