##
## Base Mapper Imports - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Config
##

## Force reloading of plugins?
ACMS_DEVELOPER_MODE = True


##
## Imports
##


##
import sys

## Import Sublime Library
import sublime, sublime_plugin

## Import the definitions ( Categories, enumerators, etc... everything definition based )
from AcecoolCodeMappingSystem.AcecoolCodeMappingSystemDefinitions import *

## Import Acecool's Library of useful functions and globals / constants...
import AcecoolCodeMappingSystem.Acecool as AcecoolLib

## Import XCodeMapper
from AcecoolCodeMappingSystem.AcecoolCodeMappingSystem import *


##
##
##
if ( ACMS_DEVELOPER_MODE ):
	sublime_plugin.reload_plugin( 'AcecoolCodeMappingSystem.AcecoolCodeMappingSystemDefinitions' )
	sublime_plugin.reload_plugin( 'AcecoolCodeMappingSystem.Acecool' )
	sublime_plugin.reload_plugin( 'AcecoolCodeMappingSystem.AcecoolCodeMappingSystem' )

	## ## Import Acecool's Library of useful functions and globals / constants...
	## from AcecoolCodeMappingSystem.AcecoolLib_Python import *

	## ## Import Acecool's Library of useful functions and globals / constants...
	## from AcecoolCodeMappingSystem.AcecoolLib_Sublime import *

	## ## Import Acecool's Library of useful functions and globals / constants...
	## from AcecoolCodeMappingSystem.AcecoolLib_SublimeText3 import *
	## sublime_plugin.reload_plugin( 'AcecoolCodeMappingSystem.AcecoolLib_Python' )
	## sublime_plugin.reload_plugin( 'AcecoolCodeMappingSystem.AcecoolLib_Sublime' )
	## sublime_plugin.reload_plugin( 'AcecoolCodeMappingSystem.AcecoolLib_SublimeText3' )