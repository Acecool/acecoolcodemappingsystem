##
## AutoHotkey - Default / Vanilla / Base Mapper - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent ACMS Mapper - As this is the User mapper, I've set up all possible includes for this mapper out!
from AcecoolCodeMappingSystem.maps.__mapper_imports__ import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##  Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
class ACMS_AutoHotkey_Default( XCodeMapperBase ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_AutoHotkey_Default'


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' )

		##
		## Comments - Notice how I added ;; before ; - As ; is the standard comment for AutoHotkey, but because I use ;;, if I were to look for ; first, then ;; would never trigger.
		## This isn't an issue with comments that are of the same character, but other AddSyntax calls may rely on the order to actually show up..
		## For example, if I used startswith function , and then I added another search for startswidth function Acecool.C. ...
		## The second one would never be detected as function  already sees it - this isn't always bad, but if you want to categorize Acecool.C. functions differently, then you'll want to search for them first.
		## Always  make sure you look at the order of your additions to ensure you're not blocking something else from categorizing an earmark..
		##
		self.AddSyntax( CATEGORY_COMMENT, SEARCH_TYPE_STARTSWITH, ';;', ';' )
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_BEGIN, SEARCH_TYPE_CONTAINS, '/*' )
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_FINISH, SEARCH_TYPE_CONTAINS, '*/' )

		## Classes, Functions and HotKeys
		self.AddSyntax( CATEGORY_CLASS, SEARCH_TYPE_STARTSWITH, 'class ' )
		self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_REGEX, '^\s*(\w+)(\()(.*)(\))' )
		self.AddSyntax( CATEGORY_ANCHOR, SEARCH_TYPE_REGEX, '^\s*(\w+):' )
		self.AddSyntax( CATEGORY_HOTKEY, SEARCH_TYPE_ENDSWITH, '::' )

		## Config
		self.AddSyntax( CATEGORY_DIRECTIVE, SEARCH_TYPE_STARTSWITH, 'SetWorkingDir', 'Menu, Tray, Icon,', '#ClipboardTimeout', '#CommentFlag', '#ErrorStdOut', '#EscapeChar', '#HotkeyInterval', '#HotkeyModifierTimeout', '#HotString', '#If', '#IfWinActive', '#IfWinExist', '#IfTimeout', '#Include', '#IncludeAgain', '#InputLevel', '#InstallKeybdHook', '#InstallMouseHook', '#KeyHistory', '#MaxHotkeysPerInterval', '#MaxMem', '#MaxThreads', '#MaxThreadsBuffer', '#MaxThreadsPerHotkey', '#MenuMaskKey', '#NoEnv', '#NoTrayIcon', '#Persistent', '#SingleInstance', '#UseHook', '#Warn', '#WinActivateForce' )
		self.AddSyntax( CATEGORY_DEPRECATED, SEARCH_TYPE_STARTSWITH, '#AllowSameLineComments' )






		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )


	##
	## Callback - these are the symbols from the file we're currently editings symbols( ) from the view - ie all extracted symbols from Sublime Text...
	## In Short: If working this should give us all of the information from the Command Palette when we look at @, :, etc.. for function names and everything... A potentially almost perfect way to create a universal mapper...
	##
	## Task: Allow symbols to be processed through the AddSyntax processor ( Make a syntax processor function outside of Run to do this ) - Then the full-line of code can be grabbed from the symbols SublimeText provides us and we'll have a basic universal mapping system....
	##
	def OnMappingFileSymbols( self, _view, _symbols ):
		## pass
		print( ' >> Symbols: ' + str( _symbols ) )
		## Symbols also don't give their function prefix...
		## if ( _code.startswith( 'def' ):

		## Symbols appear in a list, in a tuple with a region and the symbol...[((515, 525), 'PANEL:Init'), ((891, 910), 'PANEL:ControlValues'), ((1159, 1178), 'PANEL:PerformLayout')]

		## So for each entry
		for _entry in _symbols:
			## Tuples and lists use numerical indices
			## Grab the region object ( chars from start of file to X, and Total Chars in Region + X )
			_region = _entry[ 0 ]

			## Grab the partial code we're given - we can grab the full line using a helper function...
			_code = _entry[ 1 ]

			## Grab the line number using another helper...
			_line_number = self.GetSublimeViewLineNumberByRegion( _region )

			## Grab the line code by another helper...
			_line = self.GetFileDataByLine( _line_number )

			## Note - Symbols use regions meaning the starting char of from the entire code blob to the end... not line numbering so this has to be calculated...
			## Since other info is also lost we can really only guess at the data unless we check the file and look at other infomration...
			## self.AddEntry( CATEGORY_FUNCTION, _line_number, _code, _depth, _mode, _pattern, _class_cat )
			self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ), 0 )


	##
	## Helper function for this mapper - Combines the Prefix and Suffix to input text to form the full Key String..
	##
	def GetKeyTextString( self, _text ):
		return self.GetSetting( 'ahk_key_text_prefix', '' ) + _text + self.GetSetting( 'ahk_key_text_suffix', '' )


	##
	## Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	## Issue: For some reason _code isn't being passed through in stripped form - on top of that, depth isn't being processed properly... It may be limited to AHK though..
	## Todo: Add Code Raw variable...
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original ):
		##
		##
		##
		if ( _code.strip( ) != '' ):
			_code = _code.strip( )


		##
		## Because a function call and a function definition in AutoHotkey are similar ( especially as I don't have the RegEx looking at multiple lines yet ) This will ensure the next line has a { if it is a function we're looking at...
		##
		if ( _category_original == CATEGORY_FUNCTION ):
			##
			_code = 'function ' + _code
			## So, we're in the function category... Lets make sure the next line ( or this line ) has a {.. Alternatively, some people add the { on the same line, lets check there too.. Another possibility is a single line definition - check for } at the end of the line...
			if ( not self.GetFileDataByLine( _line_number + 1 ).strip( ).startswith( '{' ) and not _code.strip( ).endswith( '{' ) and not _code.strip( ).endswith( '}' ) ):
				## None of those options were discovered - this is highly likely to be a function call and not a definition... Return True or False to prevent this entry from being used...
				return _code


		##
		## For Classes, Functions and Hotkeys - Add a new line before the entry if it isn't the first and also add the comment line above the output - as AutoHotkey is an odd language, comments help therefore adding them to the output felt appropriate..
		##
		if ( _category_original == CATEGORY_CLASS and _code.startswith( 'class ' ) ) or ( _category_original == CATEGORY_HOTKEY and _code.endswith( '::' ) ) or ( _category_original == CATEGORY_FUNCTION and _code.endswith( ')' ) ):
			## Only add a space if it isn't the first entry...
			if ( self.HasCategoryEntries( _category ) ):
				self.AddBlankEntry( _category )

			## Add the description from the comment using my comment structure ( 3 lines per function / hotkey with the center containing the description ) above the hotkey with a blank line separating the previous..
			self.AddEntry( _category, _line_number - 2, self.GetFileDataByLine( _line_number - 2 ), _depth, _mode, _pattern )


			## Rewrites a bind which looks like !#^+F22 into something easily identifiable ( The chars are always foreign to me exccept + which is intuitive.. # could work as a frame.. ^ as a carrot to control the donkey? ! alt to?? Alternative sentence type? I wonder why they chose these particular chars other than being easily accessible on the keyboard..
			## Note: ! == ALT, ^ == Control, + == SHIFT, # == WIN, * == Allow Other Active Mods, ~ No Block Pass-Through of Keys Pressed
			_code = _code.replace( '^', self.GetKeyTextString( self.GetSetting( 'ahk_key_text_ctrl', 'Ctrl' ) ) )
			_code = _code.replace( '!', self.GetKeyTextString( self.GetSetting( 'ahk_key_text_alt', 'Alt' ) ) )
			_code = _code.replace( '+', self.GetKeyTextString( self.GetSetting( 'ahk_key_text_shift', 'Shift' ) ) )
			_code = _code.replace( '#', self.GetKeyTextString( self.GetSetting( 'ahk_key_text_win', 'Win' ) ) )
			_code = _code.replace( '*', self.GetKeyTextString( self.GetSetting( 'ahk_key_text_allow_mods', 'Allow Other Modifiers' ) ) )
			_code = _code.replace( '~', self.GetKeyTextString( self.GetSetting( 'ahk_key_text_allow_pass_through', 'Bind no-block / Allow Pass-Through' ) ) )


		##
		## Add descriptions to the configuration values...
		##
		if ( _category == CATEGORY_DIRECTIVE and self.GetSetting( 'ahk_wiki_display_directive_info', False ) ):
			## Set the working directory to be that of my AHK Scripts folder for easy includes...
			if ( _pattern == 'SetWorkingDir' ):
				_code = _code + ' - Defines the base directory used when including other scripts / files.'

			## ;; Set the default tray icon ( Can only be set 1 time?? )... eg: Menu, Tray, Icon, shell32.dll, 44 ;; Favorites Star
			if ( _pattern == 'Menu, Tray, Icon,' ):
				_code = _code + ' - Sets the default Tray Icon - Example Menu, Tray, Icon, shell32.dll, 44 == Favorites Star.'

			## ClipboardTimeout
			if ( _pattern == '#ClipboardTimeout' ):
				_code = _code + ' - Arg uses Milliseconds. Changes how long the script keeps trying to access the clipboard when the first attempt fails.'

			## CommentFlag
			if ( _pattern == '#CommentFlag' ):
				_code = _code + ' - Arg uses String. Changes the script\'s comment symbol from semicolon to some other string. Note: EscapeChar is related.'

			##  ErrorStdOut
			if ( _pattern == '#ErrorStdOut' ):
				_code = _code + ' - Sends any syntax error that prevents a script from launching to stderr rather than displaying a dialog. Note: As of v1.0.90+ Errors are written to stderr instead of stdout. CmdPrompt and Editors may or may not display both.'

			##  EscapeChar
			if ( _pattern == '#EscapeChar' ):
				_code = _code + ' - Changes the script\'s escape character (e.g. accent vs. backslash).'

			##  HotkeyInterval
			if ( _pattern == '#HotkeyInterval' ):
				_code = _code + ' - Uses Milliseconds - Along with #MaxHotkeysPerInterval, specifies the rate of hotkey activations beyond which a warning dialog will be displayed.'

			##  HotkeyModifierTimeout
			if ( _pattern == '#HotkeyModifierTimeout' ):
				_code = _code + ' - Uses Milliseconds - Can be -1 to never timeout - Affects the behavior of hotkey modifiers: CTRL, ALT, WIN, and SHIFT.'

			##  HotString
			if ( _pattern == '#HotString' ):
				_code = _code + ' - Changes hotstring options or ending characters. - #Hotstring NoMouse - #Hotstring EndChars NewChars - #Hotstring NewOptions'

			##  If
			if ( _pattern == '#If' ):
				_code = _code + ' - Creates context-sensitive hotkeys and hotstrings. Such hotkeys perform a different action (or none at all) depending on the result of an expression. - #If WinActive("ahk_class Notepad") or WinActive(MyWindowTitle)'

			##  IfWinActive
			if ( _pattern == '#IfWinActive' ):
				_code = _code + ' - Creates context-sensitive hotkeys and hotstrings. Such hotkeys perform a different action (or none at all) depending on the type of window that is active or exists.'

			##  IfWinExist
			if ( _pattern == '#IfWinExist' ):
				_code = _code + ' - Creates context-sensitive hotkeys and hotstrings. Such hotkeys perform a different action (or none at all) depending on the type of window that is active or exists.'

			##  IfTimeout
			if ( _pattern == '#IfTimeout' ):
				_code = _code + ' - Uses Milliseconds - Sets the maximum time that may be spent evaluating a single #If expression.'

			##  Include
			if ( _pattern == '#Include' ):
				_code = _code + ' - Causes the script to behave as though the specified file\'s contents are present at this exact position.'

			##  IncludeAgain
			if ( _pattern == '#IncludeAgain' ):
				_code = _code + ' - Causes the script to behave as though the specified file\'s contents are present at this exact position.'

			##  InputLevel
			if ( _pattern == '#InputLevel' ):
				_code = _code + ' - Uses Int between 0 and 100 - Controls which artificial keyboard and mouse events are ignored by hotkeys and hotstrings.'

			##  InstallKeybdHook
			if ( _pattern == '#InstallKeybdHook' ):
				_code = _code + ' - Forces the unconditional installation of the keyboard hook..'

			##  InstallMouseHook
			if ( _pattern == '#InstallMouseHook' ):
				_code = _code + ' - Forces the unconditional installation of the mouse hook...'

			## #KeyHistory -
			if ( _pattern == '#KeyHistory' ):
				_code = _code + ' - Sets the maximum number of keyboard and mouse events displayed by the KeyHistory window. You can set it to 0 to disable key history..'

			##  MaxHotkeysPerInterval
			if ( _pattern == '#MaxHotkeysPerInterval' ):
				_code = _code + ' - The maximum number of hotkeys that can be pressed in the interval specified by #HotkeyInterval without triggering a warning dialog..'

			##  MaxMem
			if ( _pattern == '#MaxMem' ):
				_code = _code + ' - Uses MegaBytes - Sets the maximum capacity of each variable to the specified number of megabytes.'

			##  MaxThreads
			if ( _pattern == '#MaxThreads' ):
				_code = _code + ' - The maximum total number of threads that can exist simultaneously. Specifying a number higher than 255 is the same as specifying 255 (in versions prior to 1.0.48, the limit was 20).'

			##  MaxThreadsBuffer
			if ( _pattern == '#MaxThreadsBuffer' ):
				_code = _code + ' - Causes some or all hotkeys to buffer rather than ignore keypresses when their #MaxThreadsPerHotkey limit has been reached.'

			##  MaxThreadsPerHotkey
			if ( _pattern == '#MaxThreadsPerHotkey' ):
				_code = _code + ' - Sets the maximum number of simultaneous threads per hotkey or hotstring.'

			##  MenuMaskKey
			if ( _pattern == '#MenuMaskKey' ):
				_code = _code + ' - Changes which key is used to mask Win or Alt keyup events. A key name or vkNN sequence which specifies a non-zero virtual keycode. Scan codes are not used.'

			## NoEnv - DEFAULT: Recommended for performance and compatibility with future AutoHotkey releases.
			if ( _pattern == '#NoEnv' ):
				_code = _code + ' - Assists with providing compatibility and overall performance benefits with future AutoHotkey Releases. Avoids checking empty variables to see if they are environment variables (recommended for all new scripts).'

			## NoTrayIcon
			if ( _pattern == '#NoTrayIcon' ):
				_code = _code + ' - Disables the showing of a tray icon, even if the script was compiled into an EXE.'

			## #Persistent
			if ( _pattern == '#Persistent' ):
				_code = _code + ' - Ensures the script does not exist unless ExitApp is called in the script via code-call or by manually exiting the script.'

			## SingleInstance - Disable single-instance ( For Jobs / Processes )
			if ( _pattern == '#SingleInstance' ):
				_code = _code + ' - Ensures only one copy of the script can run at any given time if set to On.'

			##  UseHook
			if ( _pattern == '#UseHook' ):
				_code = _code + ' - Forces the use of the hook to implement all or some keyboard hotkeys.'

			## Warn - Enable warnings to assist with detecting common errors.
			if ( _pattern == '#Warn' ):
				_code = _code + ' - Enables warnings to assist with detecting common errors. Enables or disables warnings for specific conditions which may indicate an error, such as a typo or missing "global" declaration.'

			##  WinActivateForce
			if ( _pattern == '#WinActivateForce' ):
				_code = _code + ' - Skips the gentle method of activating a window and goes straight to the forceful method.'




		##
		##
		##
		if ( _category == CATEGORY_DEPRECATED ):
			##
			if ( _pattern == '#AllowSameLineComments' ):
				_code = _code + ' - DEPRECATED from v1.1.09+'
				self.AddWarning( '#AllowSameLineComments has been DEPRECATED from v1.1.09+ - Directive only for AutoIt Scripts. AutoIt is no longer supported. Note: Same line comments do work using Vanilla AutoHotkey, therefore this is no longer needed anyway..', _line_number )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )


	##
	## Alter Categories when AddEntry is called - Note: This occurs after the Function / Class Arg Callbacks are processed to ensure each occurs in the proper category. This only alters category the item is visible in, no other functionality is altered..
	##
	def PreOnAddEntryCategory( self, _category, _line_number, _code, _depth, _mode, _pattern ):
		## Alter Functions in the Functions Category to the Class / Object Category for Python so indentation will look nice / natural...
		if ( _category == CATEGORY_FUNCTION ):
			## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
			## self.print( 'PreOnAddEntryCategory', 'CATEGORY FUNCTION!!! ' + _code )
			return CATEGORY_CLASS


	##
	## Override / Helper to allow for depth to be all uniform or based on indentation ( if you return _depth )
	##
	def OnCalcDepth( self, _category, _line_number, _code, _depth, _mode, _pattern ):
		if ( _category == CATEGORY_CLASS ):
			return _depth

		return 0


	##
	## Callback - Called for each line for additional error-checking and for processing rules for class recognition, stack popping, etc...
	##
	def OnProcessLine( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		## Lets avoid comments
		if ( _in_comment or _in_comment_block ):
			return super( ).OnProcessLine( _line_number, _code, _depth, _in_comment, _in_comment_block )		## self.CallParent( 'OnSetupConfig' )


		##
		## If we're calling a function and we used : instead of . - report the issue...
		##
		if ( re.search( "\w+:\w+\((.*)\)", _code ) ):
			## Add an Error Entry
			self.AddError( 'Syntax Error: ' + _code + ' [ Boolean Statement Error - Proper Usage is "=", not "==" ]', _line_number )

			## Mark the code... If applicable..
			return _code + ' [ Error ]'


		##
		## Detect Common AHK Typos and Errors coming from other lanugages..
		##
		if ( AcecoolLib.String.IndexOfExists( '==', _code, True ) ):

			## Add an example of how they should appear, but only add the example once...
			if ( not self.HasDisplayedErrorExample( 'AHK::BooleanStatements' ) ):
				## Convert the line number to string for easily adding it to the example output...
				_line = str( _line_number )

				## Add an Error Entry
				self.AddError( 'Syntax Error: ' + _code + ' [ Boolean Statement Error - Proper Usage is "=", not "==" ]', _line_number )

				## Create a simple if / else if structure as a copy / paste example..
				_example = ''
				_example += ';; ' + ':' + _line
				_example += '\n\t;; AutoHotkey Boolean Statement and Comparison Examples: -- From the error on line' + ':' + _line
				_example += '\n\t;; ' + ':' + _line
				_example += '\n\t;; Notes: ' + ':' + _line
				_example += '\n\t;;\t\tAutoHotkey uses = instead of == or === for is equal or equivalent to, and != for not equals to which is why it can be confusing for the non-evaluated data assignment operator to be used for determining equivalency ..' + ':' + _line
				_example += '\n\t;;\t\tNo error is invoked when using "==" in a boolean statement, and the script will execute. However, the code within the if statement will not be executed making this issue difficult to track down and an annoyance about this language!' + ':' + _line
				_example += '\n\t;; ' + ':' + _line
				_example += '\n\t;; Working Operators and Important Keywords: =, !=, >, >=, <, <=, not, and, or, true, false, break, return, continue' + ':' + _line
				_example += '\n\t;; ' + ':' + _line
				_example += '\n\t;; if ( _a == _b ) { ... } [ ERROR ON THIS LINE - Proper equivalency statements requires "=" to be used instead of "==" ]' + ':' + _line
				_example += '\n\t;; ' + ':' + _line
				_example += '\n\t\t;; Begin Boolean Statement to Compare...'
				_example += '\n\t\tif ( _a = _b && _b = _c ) then'
				_example += '\n\t\t\t< Code >'
				_example += '\n\t\t;; else if is identical to calling else and then adding an additional if within it - however when they are side by side only a single "end" is needed afterwards...'
				_example += '\n\t\telse if ( _c != _b && _b != _a ) then'
				_example += '\n\t\t\t< Code >'
				_example += '\n\t\t;; The last catch-all for this statement...'
				_example += '\n\t\telse'
				_example += '\n\t\t\t< Code >'
				_example += '\n\t\tend'
				## _example += '\n\t\t-- Logic outside and not influenced by the if statement above...'
				## _example += '\n\t\t< Logic / Code > -- End of Example for the error occurring on line:'


				## Add a blank line before the entry to allow space between examples...
				if ( not self.HasCategoryEntries( CATEGORY_ERROR_EXAMPLE ) ):
					self.AddBlankEntry( CATEGORY_ERROR_EXAMPLE )

				## Add the Example...
				self.AddErrorExample( _example, _line_number )

			return _code + ' [ Error ]'



			## Grab code from the next line...
			_code_pp = self.GetFileDataByLine( _line_number + 1 )

			## Helpers
			_bEndsWithEnd	= _code.endswith( 'end' )
			_bEndsWithThen	= _code.endswith( 'then' )
			_bThenExists	= AcecoolLib.String.IndexOfExists( 'then', _code, True )
			_bThenNextLine	= AcecoolLib.String.IndexOfExists( 'then', _code_pp, True ) #_code_pp.startswith( 'then' )

			## If The line ends with end and then doesn't exist somewhere - error.... If the line doesn't end with end, and then isn't the last word either, and then doesn't exist on this or the next line then error..
			if ( ( _bEndsWithEnd and not _bThenExists ) or ( not _bEndsWithThen and not _bThenExists and not _bThenNextLine ) ):
				## Add an Error Entry
				self.AddError( 'Syntax Error: ' + _code + ' [ Error - "then" keyword missing in or at the end of the line! ]', _line_number )


				## ## Add error Example Notice of repeated error...
				## self.AddErrorExample( '\n\t[ Error - Repeat: "then" keyword missing in-line ]', _line_number )

				## Add an example of how they should appear, but only add the example once...
				if ( not self.HasDisplayedErrorExample( 'Lua::if/ elseif / else if' ) ):
					##
					_line = str( _line_number )

					## Create a simple if / else if structure as a copy / paste example..
					_example = ''
					_example += '-- ' + ':' + _line
					_example += '\n\t-- Lua Structure Example: if / else if / elseif / else end -- From the error on line' + ':' + _line
					## _example += '\n\t-- Note: This will only appear once per file even if the same error occurs multiple times - A shorter message will appear for repeats...' + ':' + _line
					_example += '\n\t-- ' + ':' + _line
					_example += '\n\t\t-- Begin the if Statement...'
					_example += '\n\t\tif ( < Boolean Statements > ) then'
					_example += '\n\t\t\t< Code >'
					_example += '\n\t\t-- Add an elseif call...'
					_example += '\n\t\telseif ( < Boolean Statements > ) then'
					_example += '\n\t\t\t< Code >'
					_example += '\n\t\t-- else if is identical to calling else and then adding an additional if within it - however when they are side by side only a single "end" is needed afterwards...'
					_example += '\n\t\telse if ( < Boolean Statements > ) then'
					_example += '\n\t\t\t< Code >'
					_example += '\n\t\t-- The last catch-all for this statement...'
					_example += '\n\t\telse'
					_example += '\n\t\t\t< Code >'
					_example += '\n\t\tend'
					## _example += '\n\t\t'
					## _example += '\n\t\t-- Logic outside and not influenced by the if statement above...'
					## _example += '\n\t\t< Logic / Code > -- End of Example for the error occurring on line:'

					## Add the Example...
					self.AddErrorExample( _example, _line_number )

				## Return the modified line - Note: If the line isn't to be added then this doesn't matter.. It won't add it simply because of an error - the error above will be added though..



		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		return super( ).OnProcessLine( _line_number, _code, _depth, _in_comment, _in_comment_block )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------