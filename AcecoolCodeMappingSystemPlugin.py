##
## Acecool Code Mapping System Main Plugin Loader - a Stand-Alone Plugin for Sublime Text 3 which minimizes wasted time by scrolling, looking for classes, definitions, functions, and more.. while adding easy ways to include targeted information in the output panel to further improve coding efficiency - Josh 'Acecool' Moser
##

##
## Task List - Settings System
##

## Task: Fix issue where double-clicking a line in the output doesn't always allow navigating to the line in the file... Newly introduced?
## Task: Convert all callbacks in System to use a single object...
## Task: Convert all configuration to use the helpers defined in Definitions config or to simply grab the setting value... with a default... it's best to define the default once though so the dev doesn't need to repeat the same thing over and over..
## Task: Resolve having to include this file - or move the event listener elsewhere so the plugin file isn't called numerous times, etc... Figure out a way to import without cloning event listeners would be better and a way to ensure the plugin object is global to this project and nothing else and so it's easy to grab the object from elsewhere... ideally I'd like the plugin object in its own place...
##
## Region Mapping System with caching.... loading...
##
## In short - the output window / regions are simple character position from the entire file so double click at pos 1000 could be line 1, or line 1001.. The system is to simply map a range and use the intersect
## function to determine where or what it maps to... Which should be loaded as either an object or a simple table which can be stored as JSON in a cached file... So I'd need a few simple things... I'd need the
## actual type of link - is it a link to a line in the source file, or is it a link to activate a snippet, or a bookmark, a macro, etc... Then I need associated data... So a line link jumps to the line number..
## A bookmark jumps to a bookmark id or region or however they're stored. Snippet name or macro name for those which are activated - I could even add in run command ( I'll need those for the toolbox )...
##
## Toolbox will have Mapping filename data in it - plus text for SEARCH... FILTER... and MASK... On SEARCH opens a box where find opens at the bottom and on change depending on what is selected filter or mask
## 	to hide / show data in the output window... This should be very easy because all of the regions are set up although an output line would be more than one region and if the entire line is the entry if any
##	thing is matched in any portion of them then display or hide the entire line instead of components... Categories would need to be hidden or not based on if anything is left... So I'll need some object or system
## 	which can quickly and easily filter everything... OH... Live sorting would be nice too - so instead of category entries being sorted using config - it'll be alphabetically by default then chosen via the toolbox..
## meaning ASC or DESC for the 2 options available currently - Alphabetically, or by Line Number / Discovery... Separate groups like radio buttons where it can be either ASC or DESC and Alpha OR Line...
##
## As to how we will go about creating clickable elements for the output window... Should be a basic object to use similar to the object for a ll of the callbacks... Basic info - an Enumerator for type, and data to
##	determine what it goes to - line number, bookmark id, macro name, snippet id or name, etc... simple..
##
##
##
##
##
##
##
##
##
##



##
## Imports
##

## Sublime Text
import sublime, sublime_plugin;

## Operating System and Input / Output
import os, io, threading, subprocess, time;

## For loading mappers on the fly...
from importlib.machinery import SourceFileLoader;

##
import ctypes;
import sqlite3;
import zipfile;

##
## Plugin Imports - - this ensures ALL of the files are loaded for the plugin - ie the commands are very important...
##

## Acecool Library
## import AcecoolCodeMappingSystem.Acecool;
## from AcecoolCodeMappingSystem.Acecool import *;

## Libraries
## import AcecoolCodeMappingSystem.Acecool as AcecoolLib;

## Import Acecool's Library of useful functions and globals / constants...
import AcecoolCodeMappingSystem.Acecool as AcecoolLib;

## import AcecoolCodeMappingSystem.Acecool;
## from AcecoolCodeMappingSystem.Acecool import *;
## from AcecoolCodeMappingSystem.AcecoolLib_Python import *;
## from AcecoolCodeMappingSystem.AcecoolLib_Sublime import *;
## from AcecoolCodeMappingSystem.AcecoolLib_SublimeText3 import *;

## Definitions
from AcecoolCodeMappingSystem.AcecoolCodeMappingSystemDefinitions import *;

## Deprecated
from AcecoolCodeMappingSystem.AcecoolCodeMappingSystemClassLinker import *;

## Commands
## from AcecoolCodeMappingSystem.commands.edit_settings_plus import *;
## from AcecoolCodeMappingSystem.commands.toggle_acecool_code_mapping_system_panel import *;

## Helpers
from AcecoolCodeMappingSystem.AcecoolHelper_GetSublimePackageFolder import *;

## Task: Figure out why the plugin reloader reloads the data into memory but these files don't gain acess to new data despite importing them unless they call reload....
## sublime_plugin.reload_plugin( 'AcecoolCodeMappingSystem.AcecoolCodeMappingSystemDefinitions' );
## sublime_plugin.reload_plugin( 'AcecoolCodeMappingSystem.AcecoolLib_Python' );
## sublime_plugin.reload_plugin( 'AcecoolCodeMappingSystem.AcecoolLib_Sublime' );
## sublime_plugin.reload_plugin( 'AcecoolCodeMappingSystem.AcecoolLib_SublimeText3' );
## sublime_plugin.reload_plugin( 'AcecoolCodeMappingSystem.AcecoolCodeMappingSystem' );



##
## Important Declarations
##

## This will contact the Code mapping system object containing the status of the plugin and more... It's needed throughout Sublime Text...
global ACECOOL_CODE_MAPPING_SYSTEM;
ACECOOL_CODE_MAPPING_SYSTEM = None;


global ACECOOL_CODE_MAPPING_SYSTEM_JOBS;
ACECOOL_CODE_MAPPING_SYSTEM_JOBS = [ ];

## Sublime Version
SUBLIME_VERSION			= sublime.version( );
SUBLIME_VERSION_INT		= int( SUBLIME_VERSION );

## Simple helper to determine whether or not we're using Sublime Text Version 3 on up... The _INT var casts the version to int
USING_SUBLIME_TEXT_3	= ( SUBLIME_VERSION_INT >= 3000 );

## Install Types...
PLUGIN_INSTALL_TYPE_PATH = 0;
PLUGIN_INSTALL_TYPE_FILE = 1;















##
## ACMS Region Link - This is the base class for creating new types of region linking and what happens in that case... So line link extends this and defines how it jumps to that line... Bookmark link, macro link, snippet link, etc.. would all do something different...
##
## Task: Determine if these are going to be added to the line output object
## 	ie: so region 0 to 7 for Acecool would jump to line X for when Acecool var was defined ( capture it ).. Region 8 though 17 is timekeeper class definition and the rest for the function definition line...
## 	etc... or if I'll create a mapping table after the fact - it's probably easier to have an object which can be a basic table stored to json and retrieved for caching with lines / chars or regions of important areas so they can be
## 	referenced later, easily...
##
## It's probably best to have n regions defined per line entry object ( which is passed to all callbacks for manipulation ) ... I'll try a few things out once I solve some things...
##
class ACMSRegionLinkBase:
	##
	##
	##
	def __init__( self ):
		pass;




##
## Plugin Functionality...
##





##
##
##
class ACMSDatabase:
	##
	##
	##
	ACMSDatabaseCacheEntries = [
		( 'AcecoolCodeMappingSystem/AcecoolCodeMappingSystemClassLinker.py',													'{}',		'{}' ),
		( 'AcecoolCodeMappingSystem/AcecoolCodeMappingSystem.py',																'{}',		'{}' ),
		( 'AcecoolCodeMappingSystem/Acecool.py',																				'{}',		'{}' )
	];
	ACMSDatabaseBackupEntries = [
		( 'C:/Users/Acecool/AppData/Roaming/Sublime Text 3/Packages/AcecoolCodeMappingSystem',									'',			'' )
	];
	ACMSDatabaseRunTimeEntries = [
		( 'plugin_installed',																									'0.0.0' ),
		( 'source_file',																										'untitled' ),
		( 'source_path',																										'' ),
		( 'source_ext',																											'' ),
		( 'source_syntax',																										'Python' ),
		( 'source_syntax_path',																									'Python/Python.sublime-syntax' ),
		( 'source_syntax_ext',																									'sublime-syntax' ),
		( 'map_file',																											'Python_ACMS' ),
		( 'map_path',																											'AcecoolCodeMappingSystem/maps/' ),
		( 'map_ext',																											'py' ),
		( 'map_syntax',																											'Python' ),
		( 'map_syntax_path',																									'AcecoolCodeMappingSystem/syntax_files/Python.sublime-syntax' ),
		( 'map_syntax_ext',																										'sublime-syntax' ),
		( 'output_file',																										'ACMS - Panel' ),
		( 'timestamp_latest_file_config',																						'' ),
		( 'timestamp_latest_file_core',																							'' ),
		( 'timestamp_latest_file_map',																							'' )
	];
	ACMSDatabaseCoreEntries = [
		( 'AcecoolCodeMappingSystem/.no-sublime-package',																		'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/Acecool.py',																				'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/AcecoolCodeMappingSystem.py',																'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/AcecoolCodeMappingSystem.sublime-commands',													'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/AcecoolCodeMappingSystem.sublime-project',													'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/AcecoolCodeMappingSystem.sublime-workspace',												'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/AcecoolCodeMappingSystemClassLinker.py',													'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/AcecoolCodeMappingSystemDefinitions.py',													'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/AcecoolCodeMappingSystemPlugin.py',															'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/AcecoolCodeMappingSystemPluginReloader.py',													'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/AcecoolHelper_GetSublimePackageFolder.py',													'{}',				"-1" ),
		## ( 'AcecoolCodeMappingSystem/AcecoolLib_Python.py',																		'{}',				"-1" ),
		## ( 'AcecoolCodeMappingSystem/AcecoolLib_Sublime.py',																		'{}',				"-1" ),
		## ( 'AcecoolCodeMappingSystem/AcecoolLib_SublimeText3.py',																'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/ACMS_Definitions.sublime-settings',															'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/ACMS_Mapping.sublime-settings',																'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/ACMS_Panels.sublime-settings',																'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/ACMS_Plugin.sublime-settings',																'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/ACMS_RunTime.sublime-settings',																'{}',				"-1" ),
		( 'User/ACMS_Definitions.sublime-settings',																				'{}',				"-1" ),
		( 'User/ACMS_Mapping.sublime-settings',																					'{}',				"-1" ),
		( 'User/ACMS_Panels.sublime-settings',																					'{}',				"-1" ),
		( 'User/ACMS_Plugin.sublime-settings',																					'{}',				"-1" ),
		( 'User/ACMS_RunTime.sublime-settings',																					'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/changelog.md',																				'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/messages.json',																				'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/README.md',																					'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/todo.md',																					'{}',				"-1" ),
		## ( 'AcecoolCodeMappingSystem/version.txt',																				'{}',				"-1" ),
		## ( 'AcecoolCodeMappingSystem/commands/acecool_convert_spaces_to_tabs.py',												'{}',				"-1" ),
		## ( 'AcecoolCodeMappingSystem/commands/acms_cmd_open_all_menus.py',														'{}',				"-1" ),
		## ( 'AcecoolCodeMappingSystem/commands/acms_menu_settings_open_keybinds.py',												'{}',				"-1" ),
		## ( 'AcecoolCodeMappingSystem/commands/acms_menu_settings_open_plugin_prefs.py',											'{}',				"-1" ),
		## ( 'AcecoolCodeMappingSystem/commands/acms_menu_settings_open_sublime_prefs.py',											'{}',				"-1" ),
		## ( 'AcecoolCodeMappingSystem/commands/acms_menu_settings_open_syntax.py',												'{}',				"-1" ),
		## ( 'AcecoolCodeMappingSystem/commands/edit_settings_plus.py',															'{}',				"-1" ),
		## ( 'AcecoolCodeMappingSystem/commands/toggle_acecool_code_mapping_system_panel.py',										'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/edit_settings_plus.py',																		'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/AcecoolCodeMappingSystemCommands.py',														'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/keymaps/Default (Linux).sublime-keymap',													'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/keymaps/Default (OSX).sublime-keymap',														'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/keymaps/Default (Windows).sublime-keymap',													'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/keymaps/Default (Windows).sublime-keymap',													'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/keymaps/Default.sublime-keymap',															'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/keymaps/Default.sublime-keymap',															'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/__default__.py',																		'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/__mapper_imports__.py',																'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/ASP.py',																				'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/ASP_User.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/AutoHotkey.py',																		'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/AutoHotkey_AcecoolAHK_Framework.py',													'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/AutoHotkey_User.py',																	'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/BatchFile.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/BatchFile_User.py',																	'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/C.py',																					'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/C_User.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/CPP.py',																				'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/CPP_User.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/CSharp.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/CSharp_User.py',																		'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/CSS.py',																				'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/CSS_User.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/GMod_Lua.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/GMod_Lua_AcecoolDev_Framework.py',														'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/GMod_Lua_DarkRP.py',																	'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/GMod_Lua_NutScript.py',																'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/GMod_Lua_User.py',																		'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/HTML.py',																				'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/HTML_User.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/Java.py',																				'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/Java_User.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/JavaScript.py',																		'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/JavaScript_AcecoolWeb_Framework.py',													'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/JavaScript_User.py',																	'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/JSON.py',																				'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/JSON_User.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/Lua.py',																				'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/Lua_User.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/MarkDown.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/MarkDown_User.py',																		'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/PHP.py',																				'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/PHP_ACMS.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/PHP_User.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/PlainText.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/PlainText_User.py',																	'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/Python.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/Python_ACMS.py',																		'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/Python_Dev.py',																		'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/Python_User.py',																		'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/SQF.py',																				'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/SQF_User.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/SQL.py',																				'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/SQL_User.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/SQS.py',																				'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/SQS_User.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/SublimeKeyMap.py',																		'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/SublimeKeyMap_User.py',																'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/SublimeLanguage.py',																	'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/SublimeLanguage_User.py',																'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/SublimeMouseMap.py',																	'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/SublimeMouseMap_User.py',																'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/SublimeSettings.py',																	'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/SublimeSettings_User.py',																'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/SublimeSyntax.py',																		'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/SublimeSyntax_User.py',																'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/TypeScript.py',																		'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/TypeScript_User.py',																	'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/XML.py',																				'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/maps/XML_User.py',																			'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/menus/Context.sublime-menu',																'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/menus/Find in Files.sublime-menu',															'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/menus/Main.sublime-menu',																	'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/menus/Side Bar Mount Part.sublime-menu',													'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/menus/Side Bar.sublime-menu',																'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/menus/Tab Context.sublime-menu',															'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/menus/Widget Context.sublime-menu',															'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/syntax_files/AutoHotkey.tmLanguage',														'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/syntax_files/C++.sublime-syntax',															'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/syntax_files/JavaScript.sublime-syntax',													'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/syntax_files/Lua.tmLanguage',																'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/syntax_files/PHP Source.sublime-syntax',													'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/syntax_files/Python.sublime-syntax',														'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/snippets/Lua_GarrysMod/concommand.Add.sublime-snippet',										'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/snippets/Lua_GarrysMod/function.sublime-snippet',											'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/snippets/Lua_GarrysMod/function_local.sublime-snippet',										'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/snippets/Lua_GarrysMod/hook.Add.sublime-snippet',											'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/snippets/Lua_GarrysMod/hook.Add_SERVER_AcceptInput.sublime-snippet',						'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/snippets/Lua_GarrysMod/hook.Add_SERVER_PlayerDeath.sublime-snippet',						'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/snippets/Lua_GarrysMod/if_CLIENT_then_end.sublime-snippet',									'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/snippets/Lua_GarrysMod/if_SERVER_then_end.sublime-snippet',									'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/snippets/Lua_GarrysMod/if_then_continue_end.sublime-snippet',								'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/snippets/Lua_GarrysMod/if_then_return_end.sublime-snippet',									'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/snippets/Lua_GarrysMod/networking_AddReceiver.sublime-snippet',								'{}',				"-1" ),
		( 'AcecoolCodeMappingSystem/snippets/Lua_GarrysMod_AcecoolDev_Framework/function_Acecool_C_class.sublime-snippet',		'{}',				"-1" )
	];

	ACMSDatabaseDropCache				= 'DROP TABLE IF EXISTS "acms_cache"';
	ACMSDatabaseDropCore				= 'DROP TABLE IF EXISTS "acms_core"';
	ACMSDatabaseDropRunTime				= 'DROP TABLE IF EXISTS "acms_runtime"';
	ACMSDatabaseDropBackiup				= 'DROP TABLE IF EXISTS "acms_backup"';

	ACMSDatabaseCache					= '''CREATE TABLE IF NOT EXISTS 'acms_cache'	( 'cache_id'	INTEGER NOT NULL,		'cache_file'	TEXT  NULL,			'cache_data'	TEXT,		'cache_map'		TEXT,				'cache_time'	DATETIME NOT NULL DEFAULT ( strftime( '%s', 'now' ) ),		PRIMARY KEY ( 'cache_id' ),		UNIQUE( 'cache_file' ) )''';
	ACMSDatabaseCore					= '''CREATE TABLE IF NOT EXISTS 'acms_core'		( 'core_id'		INTEGER NOT NULL,		'core_file'		TEXT NOT NULL,		'core_data'		TEXT,		'core_crc'		TEXT NOT NULL,		'core_time'		DATETIME NOT NULL DEFAULT ( strftime( '%s', 'now' ) ),		PRIMARY KEY ( 'core_id' ),		UNIQUE( 'core_file' ) )''';
	ACMSDatabaseRunTime					= '''CREATE TABLE IF NOT EXISTS 'acms_runtime'	( 'runtime_id'	INTEGER NOT NULL,		'runtime_key'	TEXT NOT NULL,		'runtime_data'	TEXT,											'runtime_time'	DATETIME NOT NULL DEFAULT ( strftime( '%s', 'now' ) ),		PRIMARY KEY ( 'runtime_id' ),	UNIQUE( 'runtime_key' ) )''';
	ACMSDatabaseBackup					= '''CREATE TABLE IF NOT EXISTS 'acms_backup'	( 'backup_id'	INTEGER NOT NULL,		'backup_file'	TEXT NOT NULL,		'backup_data'	TEXT,		'backup_crc'	TEXT,				'backup_time'	DATETIME NOT NULL DEFAULT ( strftime( '%s', 'now' ) ),		PRIMARY KEY ( 'backup_id' ),	UNIQUE( 'backup_file' ) )''';


	##
	ACMSDatabaseCacheQuery				= '''INSERT OR REPLACE INTO "acms_cache" ( "cache_file", "cache_data", "cache_map" ) VALUES ( ?, ?, ? )''';
	ACMSDatabaseCacheQueryTime			= '''INSERT OR REPLACE INTO "acms_cache" ( "cache_file", "cache_data", "cache_map", "cache_time" ) VALUES ( ?, ?, ?, ? )''';
	ACMSDatabaseCacheQueryDefault		= '''INSERT OR IGNORE INTO "acms_cache" ( "cache_file", "cache_data", "cache_map" ) VALUES ( ?, ?, ? )''';


	##
	ACMSDatabaseBackupQuery				= '''INSERT OR REPLACE INTO "acms_backup" ( "backup_file", "backup_data", "backup_crc" ) VALUES ( ?, ?, ? )''';
	ACMSDatabaseBackupQueryDefault		= '''INSERT OR IGNORE INTO "acms_backup" ( "backup_file", "backup_data", "backup_crc" ) VALUES ( ?, ?, ? )''';


	##
	ACMSDatabaseRunTimeQuery			= '''INSERT OR REPLACE INTO "acms_runtime" ( "runtime_key", "runtime_data" ) VALUES ( ?, ? )''';
	ACMSDatabaseRunTimeQueryDefault		= '''INSERT OR IGNORE INTO "acms_runtime" ( "runtime_key", "runtime_data" ) VALUES ( ?, ? )''';

	##
	ACMSDatabaseCoreQuery				= '''INSERT OR REPLACE INTO "acms_core" ( "core_file", "core_data", "core_crc" ) VALUES ( ?, ?, ? )''';
	ACMSDatabaseCoreQueryTime			= '''INSERT OR REPLACE INTO "acms_core" ( "core_file", "core_data", "core_crc", "core_time" ) VALUES ( ?, ?, ?, ? )''';
	ACMSDatabaseCoreQueryTimeI			= '''INSERT OR IGNORE INTO "acms_core" ( "core_file", "core_data", "core_crc", "core_time" ) VALUES ( ?, ?, ?, ? )''';
	ACMSDatabaseCoreQueryDefault		= '''INSERT OR IGNORE INTO "acms_core" ( "core_file", "core_data", "core_crc" ) VALUES ( ?, ?, ? )''';

	##
	__connection						= None;
	__cursor							= None;

	##
	DefaultCacheFile					= '';
	DefaultCacheData					= '{}';
	DefaultCacheMap						= '{}';
	DefaultCacheTime					= -1;


	##
	## Helper: Connects to the database, or returns the current connection...
	##
	def Connection( self ):
		if ( self.__connection == None ):
			##
			print( '[ ACMS.DB ] Connecting to the database!' );

			## Connect to the database
			self.__connection	= sqlite3.connect( os.path.join( sublime.packages_path( ), 'User', 'AcecoolCodeMappingSystem.db.sqlite' ) );
			self.__cursor		= self.__connection.cursor( );

		return self.__connection;


	##
	## Helper: Connects to the database, if necessary, and returns the cursor
	##
	def Cursor( self ):
		return self.Connection( ).cursor( );


	##
	## Runs a query without commit...
	##
	def Execute( self, _query, _data = None ):
		## print( '[ ACMS.DB ] Execute: ' + _query );
		## print( '[ ACMS.DB ] Using: ' + str( _data ) );

		if ( _data != None ):
			return self.Cursor( ).execute( _query, _data );

		return self.Cursor( ).execute( _query );


	##
	## Runs many queries without commit..
	##
	def ExecuteMany( self, _query, _data = None ):
		if ( _data != None ):
			return self.Cursor( ).executemany( _query, _data )

		return self.Cursor( ).executemany( _query );


	##
	## Returns a single row from the query
	##
	def Fetch( self, _cursor = None ):
		if ( _cursor == None ):
			return self.Cursor( ).fetchone( );

		return _cursor.fetchone( );


	##
	## Alias of Fetch
	##
	def FetchOne( self, _cursor = None ):
		return self.Fetch( _cursor );


	##
	## Returns all affected rows from the query
	##
	def FetchAll( self, _cursor = None ):
		if ( _cursor == None ):
			return self.Cursor( ).fetchall( );

		return _cursor.fetchall( );


	##
	## Commit the previous changes to the database
	##
	def Commit( self ):
		return self.Connection( ).commit( );


	##
	##
	##
	IgnoredCoreFileExtension = {
		##
		'md':							True,

		##
		'txt':							True,

		##
		'no-sublime-package':			True,

		##
		'json':							True,
	};


	##
	##
	##
	def IsIgnoredCoreFileExtension( self, _ext = None ):
		## print( 'IsIgnoredCoreFileExtension( ' + str( _ext ) + ' == ' + str( self.IgnoredCoreFileExtension.get( _ext, False ) ) + ' );' );
		return self.IgnoredCoreFileExtension.get( _ext, False );


	##
	## Updates a core file entry
	##
	def IsCoreFile( self, _file ):
		##
		_row_id, _row_file, _row_data, _row_crc, _row_time = self.GetCore( _file );
		_is_ignored_core_ext = self.IsIgnoredCoreFileExtension( AcecoolLib.File.GetExt( _file ).lower( ) )

		## If the file has a file-extension which has no bearing on how data is processed or handled, then we treat the file as a non-core file so it doesn't trigger a full reload...
		## If we have data, it is a core file... However, the Core File Bypass must be false - if True then we treat the file as a non-core file. .... Core file bypass - not all core files should be able to force all files to be reprocessed.. For instance, text / md files don't hold configuration data, nor do they hold back-end or front-end coding - only information... So if a core file is saved which doesn't affect how the data is processed, we return false...
		if ( _row_id != -1 and not _is_ignored_core_ext ):
			##
			## print( 'ACMS - > IsCoreFile -> True - Did not execute override...' )

			## Return True, this is a core file...
			return True, _row_id, _row_file, _row_data, _row_crc, _row_time;

		print( 'ACMS - > IsCoreFile -> False - File isn\'t a core file either due to override or because it is not a core file ... IsIgnoredCoreFileExtension? ..' + str( _is_ignored_core_ext ) )

		return False, _row_id, _row_file, _row_data, _row_crc, _row_time;


	##
	## Returns a Core file row
	##
	def GetCore( self, _file ):
		## Seting / Mode for retrieval of data...
		## _setting = AMCS( ).GetSetting( 'use_caching_system', False ) and ACMS( ).GetSetting( 'use_sql', False );

		##
		_query		= '''SELECT core_id, core_file, core_data, core_crc, core_time FROM acms_core WHERE core_file=? OR core_file=? ORDER BY core_time DESC LIMIT 1''';

		##
		_file_path	= os.path.join( sublime.packages_path( ), _file ).replace( '\\', '/' );
		_file_time	= AcecoolLib.File.TimeStamp( _file_path );
		_cursor		= self.Execute( _query, ( _file_path, _file, ) );
		_row		= self.Fetch( _cursor );

		## Defaults..
		_row_id		= -1;
		_row_file	= '';
		_row_data	= '';
		_row_crc	= -1;
		_row_time	= -1;

		##
		## print( '[ ACMS.DB ] GetCore -> ' + str( _file ) )
		## print( '[ ACMS.DB ] GetCore Query -> ' + str( _query ).replace( '?', '{}' ).format( _file_path, _file ) )

		##
		if ( _row != None ):
			_row_id		= _row[ 0 ];
			_row_file	= _row[ 1 ];
			_row_data	= _row[ 2 ];
			_row_crc	= _row[ 3 ];
			_row_time	= round( _row[ 4 ], 2 );

			## print( AcecoolLib.String.FormatColumn( -4, '[ ACMS.DB ]',	-4, 'GetCore -> Row Exists', 			-1, '->', -1, 'Row ID:', -5, str( _row_id ), 			-1, '->', -1, 'Stored TimeStamp:', -5, str( _row_time ), 			-1, '->', -1, 'Core File TimeStamp:', -5, str( _file_time ),		 	-1, '->', -1, 'File:', -5, _file_path ) )
			## print( '[ ACMS.DB ] GetCore -> Row Exists -> _row_id: ' + str( _row_id ) + ' -> _row_time: ' + str( _row_time ) + ' -> File: ' + str( _row_file ) )
		## else:
		## 	## print( '[ ACMS.DB ] GetCore -> NO ROW DATA FOUND -> File: ' + str( _row_file ) )
		## 	print( AcecoolLib.String.FormatColumn( -4, '[ ACMS.DB ]',	-4, 'GetCore -> NO ROW DATA FOUND',		 	-1, '->', -1, 'File:', -5, _file_path ) )

		## 	print( '[ ACMS.DB ] GetCore _row != None .... _row_file: ' + _row_file + ' || _row_time: ' + str( _row_time ) + ' || _row_id: ' + str( _row_id ) )
		## else:
		## 	print( '[ ACMS.DB ] GetCore _row == None' )

		##
		return _row_id, _row_file, _row_data, _row_crc, _row_time


	##
	## Updates a core file entry
	##
	def UpdateCore( self, _file, _data = '{}', _query_ignore = False ):
		## Helpers
		_file_path	= os.path.join( sublime.packages_path( ), _file ).replace( '\\', '/' );
		_file_time	= AcecoolLib.File.TimeStamp( _file_path );
		_file_crc	= AcecoolLib.File.CRC( _file_path );
		## _timestamp	= round( AcecoolLib.TimeKeeper.TimeStamp( ), 2 );

		## If query ignore == true then insert or ignore, if false, by default, insert or replace ( update the entire row )...;
		## print( '[ ACMS.DB ] Core Update: ' + _file_path + ' == ' + str( _file_time ) );

		## ACMSDatabaseCoreQueryTime			= '''INSERT OR REPLACE INTO "acms_core" ( "core_file", "core_data", "core_crc", "core_time" ) VALUES ( ?, ?, ?, ? )''';
		self.Execute( ( self.ACMSDatabaseCoreQueryTime, self.ACMSDatabaseCoreQueryTime )[ _query_ignore == False ], ( _file_path, _data, _file_crc, _file_time ) );

		## Queue the query
		## self.execute( self.ACMSDatabaseCoreQuery, ( _file, _data, _crc ) );

		## Save it...
		self.Commit( );

		##
		return True;


	##
	## Updates a cached file entry
	##
	def UpdateCache( self, _file, _data = '{}', _map = '{}' ):
		## Helpers
		_file			=  _file.replace( '\\', '/' )

		## Grab the current TimeStamp for the Current Time, The Core File, And the Current File we're looking into
		_file_time		= AcecoolLib.File.TimeStamp( _file )
		_timestamp		= round( AcecoolLib.TimeKeeper.TimeStamp( ), 2 )

		## Queue the query - Note: Record the actual timestamp for the time when the cache was created - not the file-timestamp as that is easy to come by...
		## self.Execute( self.ACMSDatabaseCacheQueryTime, ( _file, _data, _map, _file_time ) )
		self.Execute( self.ACMSDatabaseCacheQueryTime, ( _file, _data, _map, _timestamp ) )

		## Save it...
		self.Commit( )

		## Convert all of the relevant data to strings for easy output..
		_file_time_str	= str( _file_time )
		_timestamp_str	= str( _timestamp )

		##
		## print( 'Cache is being updated for: ' + str( _file ) )
		## print( 'Cache update file-time: ' + _file_time_str )
		## print( 'Cache update _timestamp: ' + str( _timestamp ) )
		## print( 'Cache update file-data: ' + AcecoolLib.String.Truncate( str( _data ), 100 ) )
		## print( 'Cache Update Query Called: ' + self.ACMSDatabaseCacheQueryTime.replace( '?', '{}' ).format( _file, AcecoolLib.String.Truncate( _data, 100 ), _map, _file_time ) )
		##
		## print( AcecoolLib.String.FormatColumn( -4, '[ ACMS.DB ]',	-4, 'UpdateCache', 			-1, '->', -1, 'Current TimeStamp:', -5, _timestamp_str, 			-1, '->', -1, 'File TimeStamp:', -5, _file_time_str,		 	-1, '->', -1, 'File:', -5, _file ) )

		##
		return True


	##
	## Returns whether or not a row exists, also returns the data so we don't need to waste query calls
	##
	def HasCache( self, _file ):
		## Helpers
		_file			=  _file.replace( '\\', '/' )

		## Grab the row data for the file - if we return True we return other data too...
		_row_file, _row_data, _row_map, _row_time, _exists = self.GetCache( _file )

		## Grab the current TimeStamp for the Current Time, The Core File, And the Current File we're looking into
		_timestamp		= round( AcecoolLib.TimeKeeper.TimeStamp( ), 2 )
		_core_time		= self.GetLatestCoreTimestamp( )
		_file_time		= AcecoolLib.File.TimeStamp( _file )

		## Convert all of the relevant data to strings for easy output..
		_core_time_str	= str( _core_time )
		_file_time_str	= str( _file_time )
		_row_time_str	= str( _row_time )
		_timestamp_str	= str( _timestamp )

		##
		## print( '[ ACMS.DB ] LATEST CORE TIME: ' + _core_time_str )
		## print( '[ ACMS.DB ] LATEST FILE TIME: ' + _file_time_str )
		## print( '[ ACMS.DB ] LATEST Cache TIME: ' + _row_time_str )
		## print( '[ ACMS.DB ] CURRENT TIME: ' + _timestamp_str )

		##
		## print( AcecoolLib.String.FormatColumn( 15, '[ ACMS.DB ]',	15, 'HasCache', 	4, '->',		15, '_file_time:',		25, _file_time_str, 	4, '->',		15, '_row_time:',		25, _row_time_str, 	4, '->',		15, '_core_time:',		25, _core_time_str, 	4, '->',		15, '_timestamp:',		25, _timestamp_str, 	4, '->',		15, '_row_file:',		150, _row_file ) )
		## print( AcecoolLib.String.FormatColumn( -4, '[ ACMS.DB ]',	-4, 'HasCache', 	-2, '->',		-4, '_file_time:',		-15, _file_time_str, 	-2, '->',		-4, '_row_time:',		-15, _row_time_str, 	-2, '->',		-4, '_core_time:',		-15, _core_time_str, 	-2, '->',		-4, '_timestamp:',		-15, _timestamp_str, 	-2, '->',		-4, '_row_file:',		-5, _row_file ) )
		## 5, '->',	15, '_row_time: ' + str( _row_time ) + ' > _core_time: ' + str( _core_time ) + ' ->> _row_file: ' + str( _row_file ) ) )



		## print( AcecoolLib.String.FormatColumn( -4, '[ ACMS.DB ]',	-4, 'HasCache -> ' + ( ( 'NO ROW DATA FOUND', 'Row Exists' )[ _exists ] ),			-1, '->', -1, 'Current TimeStamp:', -5, _timestamp_str,		 	-1, '->', -1, 'Stored Core-File Latest TimeStamp:', -5, _core_time_str, 			-1, '->', -1, 'Stored File TimeStamp:', -5, _row_time_str, 	-1, '->', -1, 'File TimeStamp:', -5, _file_time_str, 			-1, '->', -4, '_row_file:', -5, _row_file ) )

		##
		## Note: I am saving the file-time, not the cache update time, which is a problem... We look at file time vs cache update time, and we look at core time vs cache update time... otherwise we get stuck updating files which haven't changed but core files have, and the cache has already been updated... I had the right idea with my old system to store the current time the cache was recorded because the file-time can be grabbed easily...
		##
		##
		## We need to make sure the cached file time is the highest on record, or equal to others... Core files affect everything so we consider the core time... The actual file-time, if higher than row time, m  eans the file has changed since last cache, so we don't have a valid cache to return for outdated cached-data...
		if ( _file_time <= _row_time and _core_time <= _row_time ):
			return True, _row_data, _row_map, _row_time, _core_time

		return False, '', '', -1, _core_time



	##
	## Returns a cached file row
	##
	def GetCache( self, _file ):
		##
		_query			= '''SELECT cache_id, cache_file, cache_data, cache_map, cache_time FROM acms_cache WHERE cache_file=? LIMIT 1'''
		## _file_path	= _file.replace( '\\', '/' )

		## Helpers
		_file			=  _file.replace( '\\', '/' )

		##
		_cursor			= self.Execute( _query, ( _file, ) )
		_row			= self.Fetch( _cursor )
		_file_time		= AcecoolLib.File.TimeStamp( _file )



		##
		_row_id			= ''
		_row_file		= self.DefaultCacheFile
		_row_data		= self.DefaultCacheData
		_row_map		= self.DefaultCacheMap
		_row_time		= self.DefaultCacheTime
		_row_exists		= False

		##
		## print( '[ ACMS.DB ] GetCache -> ' + str( _file ) )
		## print( '[ ACMS.DB ] GetCache Query -> ' + str( _query ).replace( '?', '{}' ).format( _file ) )

		##
		if ( _row != None ):
			_row_id		= _row[ 0 ]
			_row_file	= _row[ 1 ]
			_row_data	= _row[ 2 ]
			_row_map	= _row[ 3 ]
			_row_time	= round( _row[ 4 ], 2 )
			_row_exists	= True


			## print( AcecoolLib.String.FormatColumn( -4, '[ ACMS.DB ]',	-4, 'GetCache -> Row Exists', 				-1, '->', -1, 'Row ID:', -5, str( _row_id ), 			-1, '->', -1, 'Stored TimeStamp:', -5, str( _row_time ), 			-1, '->', -1, 'File TimeStamp:', -5, str( _file_time ),		 	-1, '->', -1, 'File:', -5, _file ) )
			## print( '[ ACMS.DB ] GetCache -> Row Exists -> _row_id: ' + str( _row_id ) + ' -> _row_time: ' + str( _row_time ) + ' -> File: ' + str( _file ) )
			## print( '[ ACMS.DB ] GetCache _row != None .... _row_file: ' + _row_file + ' || _row_time: ' + str( _row_time ) + ' || _row_id: ' + str( _row_id ) )
		## else:
		## 	## print( '[ ACMS.DB ] GetCache -> NO ROW DATA FOUND -> File: ' + str( _file ) )
		## 	print( AcecoolLib.String.FormatColumn( -4, '[ ACMS.DB ]',	-4, 'GetCache -> NO ROW DATA FOUND',	3 	-1, '->', -1, 'File:', -5, _file ) )
		## 	## print( '[ ACMS.DB ] GetCache _row == None' )

		##
		return _row_file, _row_data, _row_map, _row_time, _row_exists



	##
	## Returns the latest timestamp of ALL of the core files...
	##
	def GetLatestCoreTimestamp( self ):
		##
		_query			= '''SELECT core_time FROM acms_core ORDER BY core_time DESC LIMIT 1'''

		## Queue the query
		_cursor			= self.Execute( _query )

		## Return the data... Normally a callback is used for SQL, but this is SQLite and I'm testing a few things... I could easily update runtime setting, then run a command to see if the file needs to be reprocessed...
		_row			= self.Fetch( _cursor )

		if ( _row != None ):
			return round( _row[ 0 ], 2 )

		return -1


	##
	## Returns a cached file timestamp ( technically this isn't necessary as the timestamp should be the same as the file itself so we could just use Acecool.File.TimeStamp )
	##
	def GetFileTimestamp( self, _file ):
		##
		_query			= '''SELECT cache_time FROM acms_cache WHERE cache_file=? LIMIT 1'''

		## Helpers
		_file			=  _file.replace( '\\', '/' )

		## Queue the query
		_cursor			= self.Execute( _query, ( _file, ) )

		## Return the data... Normally a callback is used for SQL, but this is SQLite and I'm testing a few things... I could easily update runtime setting, then run a command to see if the file needs to be reprocessed...
		_row			= self.Fetch( )

		if ( _row != None ):
			return _row[ 0 ]

		return -1


	##
	##
	##
	def __init__( self, _plugin ):
		##
		self.type		= 0

		## Keep track of the plugin object as ACMS( ) won't be assigned until after __init__ of ACMS, etc... May mdecide to manually update reference to ensure it works as soon as __init__ is entered...
		self.plugin		= _plugin

		## Client Installed Version - Used to determine update files to execute ( When I add in update files for database maintanance, etc.. )
		self.version	= _plugin.GetSetting( 'plugin_installed', '0.0.0' )

		## Connect to the database
		## self.__connection = sqlite3.connect( os.path.join( sublime.packages_path( ), 'User', 'AcecoolCodeMappingSystem.db.sqlite' ) )
		self.Connection( )

		## ## We'll start with Caching system to start and build from there... I will need to add core file data soon though for forced updates... Simple SELECT core_time from core ORDER DESC LIMIT 1 which will give us the most recent timestamp we can compare against current filetime.. Core will contain ALL files, config, etc... and User too..
		## if ( self.version == '0.0.0' ):
		## 	## Version 0.0.0 == INSTALL
		self.Execute( self.ACMSDatabaseCache )
		self.Execute( self.ACMSDatabaseCore )
		self.Execute( self.ACMSDatabaseRunTime )
		self.Execute( self.ACMSDatabaseBackup )
		## elif( self.version == '0.9.0' ):
		## 	## Version 0.9.0 means update..
		## 	## Task: Add update to log how many times files are accessed, and how many times files are saved... This will be very useful information... Could also determine average time between saves... and slast save delta to determine errors... and quick fixes..
		## 	pass

		## Commit the previous changes to the file...
		self.Commit( )

		## For core files, we want the full path to make things easier so I'll use a loop for this..
		for _core_file, _core_data, _core_crc in self.ACMSDatabaseCoreEntries:
			self.UpdateCore( _core_file, _core_data )

		##
		self.ExecuteMany( self.ACMSDatabaseRunTimeQueryDefault, self.ACMSDatabaseRunTimeEntries )

		## Backup and Cache don't need default content...
		self.ExecuteMany( self.ACMSDatabaseCacheQueryDefault, self.ACMSDatabaseCacheEntries )
		self.ExecuteMany( self.ACMSDatabaseBackupQueryDefault, self.ACMSDatabaseBackupEntries )

		## Commit the previous changes to the file...
		self.Commit( )


		## _query = '''SELECT * FROM acms_cache WHERE cache_file=? LIMIT 1'''
		## _query = '''SELECT * FROM acms_cache WHERE cache_file LIKE ? LIMIT 1'''
		## _query = 'SELECT * FROM acms_cache LIMIT 1'

		##
		## _file = '"' + _file + '"'

		##
		## _file = '"' + _file + '"'


		## Grab the cursor
		## self.cursor = self.connection.cursor( )

		## ## If we reset all tables every session.. drop them...
		## if ( _plugin.GetSetting( 'database_system_wipe_every_session', False ) ):
		## 	##
		## 	print( '[ ACMS.DB ] Clearing ACMS Database... Deleting Cached files, Core File Information, RunTime information, and Files in Backup...' )

		## 	##
		## 	self.Execute( self.ACMSDatabaseDropCache )
		## 	self.Execute( self.ACMSDatabaseDropCore )
		## 	self.Execute( self.ACMSDatabaseDropRunTime )
		## 	self.Execute( self.ACMSDatabaseDropBackup )

		## 	## Commit the previous changes to the file...
		## 	self.Commit( )

		## self.ExecuteMany( self.ACMSDatabaseCoreQueryDefault, self.ACMSDatabaseCoreEntries )
			## _core_file_path = os.path.join( sublime.packages_path( ), _core_file )
			## _core_file_time = AcecoolLib.File.TimeStamp( _core_file_path )
			## print( '[ ACMS.DB ] Core Update: ' + _core_file_path + ' == ' + str( _core_file_time ) )
			## self.Execute( self.ACMSDatabaseCoreQueryTime, ( _core_file_path, _core_data, _core_crc, _core_file_time ) )


		## _query = '''SELECT * FROM acms_core'''
		## self.Cursor( ).execute( _query )
		## print( self.Cursor( ).fetchall( ) )
		## print( self.FetchAll( ) )


		##
		## _timestamp = Acecool.TimeKeeper.TimeStamp( )

		## Technically we don't need the file-timestamp since the database can manage the timestamp... If a core file is updated, then that database will be just as offset as one of these, and, by default, the install time will also be fine..
		## _file_time = Acecool.File.GetTimeStamp( _file )

		## Query with timestamp - not needed...
		## _query = '''UPDATE acms_cache ( cache_file, cache_data, cache_time ), VALUES( ?, ?, ? )'''
		## _query = '''UPDATE OR REPLACE INTO acms_cache ( cache_file, cache_data ), VALUES( ?, ? )'''

		## self.cursor.execute( _query, ( _file, _data, _file_time ) )




##
## Plugin Object - this is the core of the plugin and runs everything...
##
class AcecoolCodeMappingSystemPluginBase:
	pass
class AcecoolCodeMappingSystemPlugin( AcecoolCodeMappingSystemPluginBase ): #( AccessorFuncSupport ):
	## def SetupAccessorFunc( self,		_name, 						_default,	_getter_prefix = 'Get',		_key = None,		_initialize = True,			_debug = False ):
	## 	## View / File, the View ID, the Views Index and the GroupID...
	## 	self.SetupAccessorFunc(			'OutputView',				None,		'Get',						None,				True,						True )
	## 	self.SetupAccessorFunc(			'OutputViewID',				None,		'Get',						None,				True,						True )
	## 	self.SetupAccessorFunc(			'OutputViewIndex',			None,		'Get',						None,				True,						True )
	## 	self.SetupAccessorFunc(			'OutputGroupID',			None,		'Get',						None,				True,						True )

	## 	## The file we need to map - this gets set when we activate a file - if not ACMS - Panel, or Code - Map, etc... and has an extension... set it so we know what to map...
	## 	self.SetupAccessorFunc(			'SourceView',				None,		'Get',						None,				True,						True )
	## 	self.SetupAccessorFunc(			'SourceFileName',			None,		'Get',						None,				True,						True )
	## 	self.SetupAccessorFunc(			'SourceFileExt',			None,		'Get',						None,				True,						True )

	## 	##
	## 	self.SetupAccessorFunc(			'SourceViewFileSyntax',		None,		'Get',						None,				True,						True )
	## 	self.SetupAccessorFunc(			'SourceViewFileSyntaxExt',	None,		'Get',						None,				True,						True )
	## 	self.SetupAccessorFunc(			'SourceViewFileSyntaxPath',	None,		'Get',						None,				True,						True )

	## __active_lang					= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'active_lang',				name = 'ActiveLanguage',				default = 'Default',						getter_prefix = 'Get',			documentation = 'ActiveLanguage Docs',				allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY )																																																	)
	## __active_lang_name				= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'active_lang_name',			name = 'ActiveLanguageName',			default = 'Default',						getter_prefix = 'Get',			documentation = 'ActiveLanguage Docs',				allowed_types = ( AcecoolLib.TYPE_ANY ),					allowed_values = ( AcecoolLib.VALUE_ANY )																																																	)


	## ##
	## ## @staticmethod
	## _thread_jobs = [ ]

	## ##
	## @staticmethod
	## def SetThreadJobs( self, _jobs = [ ] ):
	## 	self._thread_jobs = _jobs;
	## ##
	## @staticmethod
	## def GetThreadJobs( self ):
	## 	return self._thread_jobs;
	## ##
	## ## @staticmethod
	## _thread = None

	## ##
	## @staticmethod
	## def SetThread( self, _thread = None ):
	## 	self._thread = _thread;
	## ##
	## @staticmethod
	## def GetThread( self ):
	## 	return self._thread;


	## ##
	## ##
	## ##
	## def Database( self ):
	## 	return self.__database
	## ##
	## ## Helper: Returns the Active Language or Default
	## ##
	## def GetActiveLanguageName( self ):
	## 	return self.GetSetting( 'language_name', 'Default' )


	## ##
	## ## Helper: Returns the Active Language or Default
	## ##
	## def GetActiveLanguage( self ):
	## 	return self.GetSetting( 'active_language', 'Default' )


	## ##
	## ## Helper: Returns the Active Mapper or User
	## ##
	## def GetActiveMapper( self ):
	## 	return self.GetSetting( 'active_mapper', 'User' )


	## ##
	## ## Helper: Returns the Active Class or null
	## ##
	## def GetActiveClass( self ):
	## 	return self.GetSetting( 'active_class', None )


	## ## Helper - Returns the Package Folder Name Dynamically
	## ## def GetPackageName( ):											return GetThisPackageName( )

	## ## Task: Create AccessorFunc Creator for the 5 settings functions - Also, Convert the Extended Var Expander from edit_settings_plus to Acecool.Sublime so it can be used in place of manually setting them up...
	## ## Task: -> GetNameSettingsFileName( _view, _format_text_override ), GetNameSettingsObject( _format_text_override ), SaveNameSettings( _format_text_override ), GetNameSetting( _key, _default, _format_text_override ), SetNameSetting( _key, _value, _format_text_override )


	## ## Accessor - For Developer Mode - pass-through for caching system..
	## def GetDeveloperMode( self ):									return self.GetSetting( 'developer_mode', False )

	## ##
	## def GetPanelName( self ):										return self.__panel_name__

	## ##
	## def GetPackageName( self ):										return self.__package_name__

	## ## Returns the default syntax highlighter file...
	## def GetDefaultSyntaxHighlighter( self ):						return self.__syntax_highlighter__




	## ## Defaults / Overrides for Accessorfuncs until I move the AccessorFunc Generator into this class..... Note: I need to add the accessorfuncs to the class prior to calling init...
	## def GetOutputView( self ): return AcecoolLib.Sublime.FindView( 'ACMS - Panel' )
	## def GetOutputViewID( self ): return None
	## def GetOutputViewIndex( self ): return None
	## def GetOutputGroupID( self ): return None
	## ## def GetSourceView( self ): return None
	## def GetSourceFileName( self ): return None
	## def GetSourceFileExt( self ): return 'ACMS_Default'
	## def GetSourceViewFileSyntax( self ): return None
	## def GetSourceViewFileSyntaxExt( self ): return None
	## def GetSourceViewFileSyntaxPath( self ): return None

	## def GetSourceView( self ):
	## 	return sublime.active_window( ).active_view( )

	##
	## Main --
	##





	##
	## Output Panel and Source-Code Panel Helpers
	##

	##
	__OutputView					= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'OutputView',						name = 'OutputView',					default = lambda this: AcecoolLib.Sublime.FindView( 'ACMS - Panel' ),						group = '',			getter_prefix = 'Get',			documentation = 'OutputView',					allowed_types = ( sublime.View ),									allowed_values = AcecoolLib.VALUE_ANY																																	);
	__OutputViewID					= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'OutputViewID',					name = 'OutputViewID',					default = None,																				group = '',			getter_prefix = 'Get',			documentation = 'OutputViewID',					allowed_types = ( AcecoolLib.TYPE_INTEGER ),						allowed_values = AcecoolLib.VALUE_ANY																																	);
	__OutputViewIndex				= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'OutputViewIndex',				name = 'OutputViewIndex',				default = None,																				group = '',			getter_prefix = 'Get',			documentation = 'OutputViewIndex',				allowed_types = ( AcecoolLib.TYPE_INTEGER ),						allowed_values = ( CONST_DATA_NONE )																																	);
	__OutputGroupID					= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'OutputGroupID',					name = 'OutputGroupID',					default = None,																				group = '',			getter_prefix = 'Get',			documentation = 'OutputGroupID',				allowed_types = ( AcecoolLib.TYPE_INTEGER ),						allowed_values = AcecoolLib.VALUE_ANY																																	);
	__SourceView					= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'SourceView',						name = 'SourceView',					default = lambda this: sublime.active_window( ).active_view( ),								group = '',			getter_prefix = 'Get',			documentation = 'SourceView',					allowed_types = ( sublime.View ),									allowed_values = AcecoolLib.VALUE_ANY																																	);
	__SourceFileName				= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'SourceFileName',					name = 'SourceFileName',				default = None,																				group = '',			getter_prefix = 'Get',			documentation = 'SourceFileName',				allowed_types = ( AcecoolLib.TYPE_STRING ),							allowed_values = AcecoolLib.VALUE_ANY																																	);
	__SourceFileExt					= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'SourceFileExt',					name = 'SourceFileExt',					default = 'ACMS_Default',																	group = '',			getter_prefix = 'Get',			documentation = 'SourceFileExt',				allowed_types = ( AcecoolLib.TYPE_STRING ),							allowed_values = AcecoolLib.VALUE_ANY																																	);
	__SourceViewFileSyntax			= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'SourceViewFileSyntax',			name = 'SourceViewFileSyntax',			default = None,																				group = '',			getter_prefix = 'Get',			documentation = 'SourceViewFileSyntax',			allowed_types = ( AcecoolLib.TYPE_STRING ),							allowed_values = AcecoolLib.VALUE_ANY																																	);
	__SourceViewFileSyntaxExt		= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'SourceViewFileSyntaxExt',		name = 'SourceViewFileSyntaxExt',		default = None,																				group = '',			getter_prefix = 'Get',			documentation = 'SourceViewFileSyntaxExt',		allowed_types = ( AcecoolLib.TYPE_STRING ),							allowed_values = AcecoolLib.VALUE_ANY																																	);
	__SourceViewFileSyntaxPath		= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'SourceViewFileSyntaxPath',		name = 'SourceViewFileSyntaxPath',		default = None,																				group = '',			getter_prefix = 'Get',			documentation = 'SourceViewFileSyntaxPath',		allowed_types = ( AcecoolLib.TYPE_STRING ),							allowed_values = AcecoolLib.VALUE_ANY																																	);


	##
	## Threading Helpers
	##

	## Creates a single getter which returns a configuration / setting value regarding whether or not we're to launch threads or not...
	__UseMappingThreads				= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'UseMappingThreads',				name = 'UseMappingThreads',				default = lambda this: this.GetSetting( 'use_plugin_mapping_threads', True ),				group = '',			getter_prefix = '',				documentation = 'UseMappingThreads',			allowed_types = ( AcecoolLib.TYPE_BOOLEAN ),						allowed_values = ( True, False ),						setup = { 'get': ( lambda this: this.GetSetting( 'use_plugin_mapping_threads', True ) ) }						);

	## Returns the thread state - True means running / idle, False means not running - This exists and is assigned on class initialization so if the threading configuration is changed during run-time we know whether or not we have any action to take. If True, we don't as the thread puts itself to sleep when the user changes the configuration if it started with Sublime Text.. If False, we start the thread as we would on plugin_loaded because the thread terminated before fully starting up to free up space ( Note: I may change the behavior so it shutsdown if the value is changed to false, at which point this won't be necessary )
	__ThreadState					= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'ThreadState',					name = 'ThreadState',					default = True,																				group = '',			getter_prefix = 'Get',			documentation = 'ThreadState',					allowed_types = ( AcecoolLib.TYPE_BOOLEAN ),						allowed_values = ( True, False )						);

	## Returns a reference to the worker-thread for the plugin...
	## __ThreadJobs					= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'ThreadJobs',						name = 'ThreadJobs',					default = [ ],																				group = '',			getter_prefix = 'Get',			documentation = 'ThreadJobs',					allowed_types = ( AcecoolLib.TYPE_LIST ),							allowed_values = AcecoolLib.VALUE_ANY																																	);
	__thread						= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'thread',							name = 'Thread',						default = None,																				group = '',			getter_prefix = 'Get',			documentation = 'Thread',						allowed_types = ( AcecoolLib.TYPE_ANY ),							allowed_values = AcecoolLib.VALUE_ANY																																	);

	## Thread Job / Task Helpers - These are several helpers to create helpers to manage thread jobs.. - In short, this creates a common set of accessors such as GetThreadJobs( ), SetThredJobs( [ ] ), AddThreadJobs( _job, ... ), PopThreadJobs( _index = len - 1, _count = 1 ), and more which redirect to the thread so the thread can lock, etc.. and have full access over the data...
	## __thread_job_helpers			= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'thread_job_helpers',				name = 'ThreadJobs',					default = None,																				group = '',			getter_prefix = 'Get',			documentation = 'Thread Job Helpers',			allowed_types = ( AcecoolLib.TYPE_ANY ),							allowed_values = AcecoolLib.VALUE_ANY,					setup = { 'get': ( lambda this: this.GetThread( ).GetJobs( ) ), 'len': ( lambda this: this.GetThread( ).GetJobsLen( ) ), 'set': ( lambda this, _jobs: this.GetThread( ).SetJobs( _jobs ) ), 'add': ( lambda this, *_jobs: this.GetThread( ).AddJobs( *_jobs ) ), 'pop': ( lambda this: ( { }, this.GetThread( ).GetJobs( ).pop( ) )[ this.GetThreadJobsLen( ) > 0 ] ) }					);
	__thread_job_helpers			= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'thread_job_helpers',				name = 'ThreadJobs',					default = None,																				group = '',			getter_prefix = 'Get',			documentation = 'Thread Job Helpers',			allowed_types = ( AcecoolLib.TYPE_ANY ),							allowed_values = AcecoolLib.VALUE_ANY,					setup = { 'get': ( lambda this: this.GetThread( ).GetJobs( ) ), 'len': ( lambda this: this.GetThread( ).GetJobsCount( ) ), 'set': ( lambda this, _jobs: this.GetThread( ).SetJobs( _jobs ) ), 'add': ( lambda this, *_jobs: this.GetThread( ).AddJobs( *_jobs ) ), 'pop': ( lambda this, _index = None, _count = 1: this.GetThread( ).PopJobs( _index, _count ) ) }					);


	##
	## Miscellaneus Plugin Helpers
	##
	__ActiveLanguageName			= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'ActiveLanguageName',				name = 'ActiveLanguageName',			default = lambda this: this.GetSetting( 'language_name', 'Default' ),						group = '',			getter_prefix = 'Get',			documentation = 'ActiveLanguageName',			allowed_types = ( AcecoolLib.TYPE_ANY ),							allowed_values = AcecoolLib.VALUE_ANY,					setup = { 'get': ( lambda this: this.GetSetting( 'language_name', 'Default' ) ) }								);
	__ActiveLanguage				= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'ActiveLanguage',					name = 'ActiveLanguage',				default = lambda this: this.GetSetting( 'active_language', 'Default' ),						group = '',			getter_prefix = 'Get',			documentation = 'ActiveLanguage',				allowed_types = ( AcecoolLib.TYPE_ANY ),							allowed_values = AcecoolLib.VALUE_ANY,					setup = { 'get': ( lambda this: this.GetSetting( 'active_language', 'Default' ) ) }								);
	__ActiveMapper					= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'ActiveMapper',					name = 'ActiveMapper',					default = lambda this: this.GetSetting( 'active_mapper', 'User' ),							group = '',			getter_prefix = 'Get',			documentation = 'ActiveMapper',					allowed_types = ( AcecoolLib.TYPE_ANY ),							allowed_values = AcecoolLib.VALUE_ANY,					setup = { 'get': ( lambda this: this.GetSetting( 'active_mapper', 'User' ) ) }									);
	__ActiveClass					= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'ActiveClass',					name = 'ActiveClass',					default = lambda this: this.GetSetting( 'active_class', None ),								group = '',			getter_prefix = 'Get',			documentation = 'ActiveClass',					allowed_types = ( AcecoolLib.TYPE_ANY ),							allowed_values = AcecoolLib.VALUE_ANY,					setup = { 'get': ( lambda this: this.GetSetting( 'active_class', None ) ) }										);
	__DeveloperMode					= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'DeveloperMode',					name = 'DeveloperMode',					default = lambda this: this.GetSetting( 'developer_mode', False ),							group = '',			getter_prefix = 'Get',			documentation = 'DeveloperMode',				allowed_types = ( AcecoolLib.TYPE_ANY ),							allowed_values = AcecoolLib.VALUE_ANY,					setup = { 'get': ( lambda this: this.GetSetting( 'developer_mode', False ) ) }									);
	__PanelName						= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'PanelName',						name = 'PanelName',						default = lambda this: this.__panel_name__,													group = '',			getter_prefix = 'Get',			documentation = 'PanelName',					allowed_types = ( AcecoolLib.TYPE_ANY ),							allowed_values = AcecoolLib.VALUE_ANY,					setup = { 'get': ( lambda this: this.__panel_name__ ) }															);
	__PackageName					= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'PackageName',					name = 'PackageName',					default = lambda this: this.__package_name__,												group = '',			getter_prefix = 'Get',			documentation = 'PackageName',					allowed_types = ( AcecoolLib.TYPE_ANY ),							allowed_values = AcecoolLib.VALUE_ANY,					setup = { 'get': ( lambda this: this.__package_name__ ) }														);
	__DefaultSyntaxHighlighter		= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'DefaultSyntaxHighlighter',		name = 'DefaultSyntaxHighlighter',		default = lambda this: this.__syntax_highlighter__,											group = '',			getter_prefix = 'Get',			documentation = 'DefaultSyntaxHighlighter',		allowed_types = ( AcecoolLib.TYPE_ANY ),							allowed_values = AcecoolLib.VALUE_ANY,					setup = { 'get': ( lambda this: this.__syntax_highlighter__ ) }													);
	## __Database						= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'Database',						name = 'Database',						default = lambda this: this.__database__,													group = '',			getter_prefix = '',				documentation = 'Database',						allowed_types = ( AcecoolLib.TYPE_ANY ),							allowed_values = AcecoolLib.VALUE_ANY,					setup = { 'get': ( lambda this: this.__database__ ) }															);
	__PluginStructure				= AcecoolLib.AccessorFuncBase( parent = AcecoolCodeMappingSystemPluginBase,		key = 'PluginStructure',				name = 'PluginStructure',				default = lambda this: this.__plugin_structure__,											group = '',			getter_prefix = 'Get',			documentation = 'PluginStructure',				allowed_types = ( AcecoolLib.TYPE_ANY ),							allowed_values = AcecoolLib.VALUE_ANY,					setup = { 'get': ( lambda this: this.__plugin_structure__ ) }													);


	##
	## Important Object Values!
	##

	## Name / ID of the XCodeMapperBase
	__name__				= 'AcecoolCodeMappingSystemPlugin';

	## Version
	__version__				= '0.9.1';

	## This is the base folder used for the package...
	__package_name__		= 'AcecoolCodeMappingSystem';

	## This is the file-name / panel-name used by the plugin...
	__panel_name__			= 'ACMS - Panel';
	## __panel_name__		= 'Code - Map';

	## RunTime Settings File Name
	__runtime_cfg_name__	= 'plugin_runtime.sublime-settings';

	## Default Syntax Highlighter File...
	__syntax_highlighter__	= 'Packages/AcecoolCodeMappingSystem/syntax_files/Python.tmLanguage';

	## Define all of the custom folders which need to exist... Root is sublime.packages_path( )
	__plugin_structure__ = {
		## This is the plugin folder - I've set it to not be packaged by default...
		## Note: You can use { 'type': PLUGIN_INSTALL_TYPE_PATH } for a file, or True - a simple Dictionary as settings means sub / nested folders... use type: _FILE for the file to be created. data or contents to set the contents... You can also set copy to a path of a file you want the contents to come from ( for updates, etc... )
		'AcecoolCodeMappingSystem':
		{
			## ## This folder contains all plugin command files
			## 'commands':			True,

			## This folder contains all plugin documentation / auto generated wiki entries, etc..
			'documentation':	True,

			## This folder contains examples of various programming languages to be used to determine whether they're being properly mapped...
			'examples':			True,

			## Keymaps folder...
			'keymaps':			True,

			## This folder contains all of the default mapping classes
			'maps':		True,

			## This folder contains all of the menus added to Sublime Text such as the Context Menu ( Right Click Menu ), and the Preferences > Package Settings > * Menus...
			'menus':			True,

			## This folder contains all of the individual change-log entries
			'messages':			True,

			## This folder contains all of the default plugin settings files
			'settings':
			{

				## ## Package File...
				## 'plugin_runtime.sublime-settings':
				## {
				## 	'type': PLUGIN_INSTALL_TYPE_FILE,
				## 	'data': '//\n// Acecool - Code Mapping System - Plugin RunTime Settings - User - Edit at your own risk!\n//\n{\n\t$0\n}\n',
				## },
			},

			## ## Package File...
			## '' + __package_name__ + '2':
			## {
			## 	'type': PLUGIN_INSTALL_TYPE_FILE,
			## 	'data': '',
			## },

			## This folder contains snippets I've created for various programming languages you may find helpful
			'snippets':			True,

			## This folder contains the syntax highlighting files used for the mapping panel - these are different than the default ones because I add shorteners for function declarations, etc... to reduce the amount of space needed to convey the same amount of information to you - those will smaller screens will be able to show just as much information with less characters - additionally, the code in the output panel will be properly highlighted by the syntax highlighting system because of these..
			'syntax_files':		True,
		},

		## User Folders
		'User':
		{
			## Everything is sorted within this folder...
			'AcecoolCodeMappingSystem':
			{
				## Assets are for icons, etc...
				'assets':
				{
					## Gutter Icons, etc..
					'icons':		True,
				},

				## Backups to files being edited - this is for a future feature
				'backup':		True,

				## Cached Mapped files
				'cache':		True,

				## Keymaps folder...
				## 'keymaps':	True,

				## Custom Mappers - if one exists in here and in the default plugin folder, then this will override the default..
				'maps': {
					##
					'default':	True,
				},

				## Custom Settings
				## Task: Create the RunTime settings file with the structure system...
				## 'settings':
				## {

					## Package File...
					## 'plugin_runtime.sublime-settings':
					## {
					## 	'type': PLUGIN_INSTALL_TYPE_FILE,
					## 	'data': '//\n// Acecool - Code Mapping System - Plugin RunTime Settings - User - Edit at your own risk!\n//\n{\n\t$0\n}\n',
					## },
				## },

				## Package / Panel Empty Scratch File...
				'ACMS - Panel':
				{
					'type': PLUGIN_INSTALL_TYPE_FILE,
					'data': '',
				},

				## Package / Panel Empty Scratch File...
				'ACMS - ToolBox':
				{
					'type': PLUGIN_INSTALL_TYPE_FILE,
					'data': '',
				},

				## Package / Panel Empty Scratch File...
				'ACMS - Panel - Snippets':
				{
					'type': PLUGIN_INSTALL_TYPE_FILE,
					'data': '',
				},

				## Package / Panel Empty Scratch File...
				'ACMS - Panel - Macros':
				{
					'type': PLUGIN_INSTALL_TYPE_FILE,
					'data': '',
				},

				## Package / Panel Empty Scratch File...
				'ACMS - Panel - Bookmarks':
				{
					'type': PLUGIN_INSTALL_TYPE_FILE,
					'data': '',
				},

				## ## This folder contains snippets I've created for various programming languages you may find helpful
				## 'snippets':			True,

				## This folder contains the syntax highlighting files used for the mapping panel - these are different than the default ones because I add shorteners for function declarations, etc... to reduce the amount of space needed to convey the same amount of information to you - those will smaller screens will be able to show just as much information with less characters - additionally, the code in the output panel will be properly highlighted by the syntax highlighting system because of these..
				'syntax_files':		True,
			},
		},
	}


	##
	## Called when the class is initialized...
	##
	def __init__( self, _acms = None, **_varargs ):
		## If we're debugging the specific task / Function then allow the output...
		if ( self.GetSettingDebugging( '__init__', True ) ):
			self.Notify( '-> __init__( _varargs = ' + str( **_varargs ) + ' )' )

		## Internal Data for ALL Data stored for the object ( children and parents too ) for all data, AccessorFunc Data / Info, etc... Everything.. Easy to copy to new object....
		## Priming the settings vars
		## Internal Data for ALL Data stored for the object ( children and parents too ) for all data, AccessorFunc Data / Info, etc... Everything.. Easy to copy to new object....
		self.this_data = {
			## 'accessorfunc': {
			## 	'data': { },
			## 	'names': { },
			## },
		}

		## Call the __init__ function of inherited classes...
		super( ).__init__( **_varargs )

		## Priming the settings vars
		self.__settings__ = {

		}


		##
		## ACMSDatabase( ACMS( ) )
		self.__database__ = ACMSDatabase( self )

		## self.thread = ACMSThread( )

		## global ACMS_Thread_Worker
		## ACMS_Thread_Worker = self.thread

		## ## ACMS( ).SetThread( ACMS_Thread_Worker );
		## ACMS_Thread_Worker.start( );

		## print( self.GetOutputView( ) )
		## print( self.GetOutputView( ) )
		## print( self.GetOutputView( ) )
		## print( self.GetOutputView( ) )
		## print( self.GetOutputView( ) )
		## print( self.GetOutputView( ) )
		## print( self.GetOutputView( ) )
		## print( self.GetOutputView( ) )
		## print( self.GetOutputView( ) )
		## print( self.GetOutputView( ) )
		## print( self.GetOutputView( ) )
		## print( self.GetOutputView( ) )
		## print( self.GetOutputView( ) )
		## print( self.GetOutputView( ) )
		## print( self.GetOutputView( ) )
		## print( self.GetOutputView( ) )
		## print( self.GetOutputView( ) )
		## print(  )
		## print( '1\t\t' + str( AcecoolLib.TYPE_SUBLIME_VIEW ) )
		## print( '2\t\t' + str( type( self.GetOutputView( ) ) ) )
		## print( '3\t\t' + str( type( self.GetOutputView( ) ) ) )
		## print( '4\t\t' + str( isinstance( self.GetOutputView( ), AcecoolLib.TYPE_SUBLIME_VIEW ) ) )
		## print( '5\t\t' + str( type( self.GetOutputView( ) ) == AcecoolLib.TYPE_SUBLIME_VIEW ) )

		##
		self.Init( )


		## print( '' )
		## print( '>>>>>	>>>>>	>>>>>	>>>>>	>>>>>	>>>>>	>>>>>' )




	##
	## Caching System
	##


	##
	##
	##
	def HasCachedFile( self, _file ):

		##
		_exists, _row_data, _row_map, _row_time, _core_time = self.Database( ).HasCache( _file )

		return _exists
		## return AcecoolLib.File.exists( _file )


	##
	## Helper - Used to determine whether or not we need to load the cached file vs generate...
	##
	def ShouldLoadCachedFile( self, _file ):
		## If force refresh is enabled - we force reload the file every time...
		if ( self.GetSetting( 'caching_system_force_refresh', False ) ):
			return False

		##
		_exists, _row_data, _row_map, _row_time, _core_time = self.Database( ).HasCache( _file )

		##
		return _exists and _row_time > _core_time


		## ## The Core Files Max placeholder ( although it seems like Python doesn't care about scope if you se it inside of an if in a function if you need it in a function - strange )...
		## _timestamp_core_files_max = 0

		## ## If Developer mode is enabled, then the core files can be reloaded so we need to look at those timestamps...
		## if ( self.GetSetting( 'developer_mode', False ) ):
		## 	## Assign _max the maximum timestamp value of all of the core files...
		## 	_timestamp_core_files_max = self.GetTimeStampMaxOfCoreFiles( )

		## 	## Notify
		## 	## self.print( 'Caching', ' >> Caching > The max timestamp of all of the core files is: ' + str( _timestamp_core_files_max ) )

		## ## Get the extension of the current file to determine which custom mapper is being used... We know a custom mapper is loaded because we're in XCM...
		## ## _file_split = _file.split( '.' )			## _ext = _view.file_name( ).split( '.' )[ - 1 ]
		## _raw_file_split, _file_split = os.path.splitext( _file )

		## ## Check to see if it is possible to split the filename into ext and base name
		## if ( _file_split == None ):
		## 	print( ' >> _file_split is None - ERROR' )

		## ## If the file starts with a decimal, remove it...
		## if ( _file.startswith( '.' ) or _file_split[ 0 : 1 ] == '.' ):
		## 	_file_split = _file_split[ 1 :  ]

		## ## _ext = _file_split[ len( _file_split ) - 1 ].lower( )
		## _ext = _file_split.lower( )

		## ## Notify
		## ## self.print( 'Caching', ' >> Caching > The extension for our mapper is: ' + _ext )

		## ## Cached File
		## _timestamp_cached = self.GetCachedFileTimeStamp( _file )

		## ## The file we're mapping...
		## _timestamp_file = AcecoolLib.File.TimeStamp( _file )

		## ## Get the current mapper ( using the file extension, explode . and use last )
		## ## Note: This will become OBSOLETE very soon...
		## ## _timestamp_mapper = AcecoolLib.File.TimeStamp( os.path.join( sublime.packages_path( ), 'User', 'CodeMap', 'custom_mappers', _ext + '.py' ) )
		## _timestamp_mapper_user = AcecoolLib.File.TimeStamp( os.path.join( sublime.packages_path( ), 'User', 'AcecoolCodeMappingSystem', 'maps', self.GetMapperFileName( ) + '.py' ) )
		## _timestamp_mapper = AcecoolLib.File.TimeStamp( os.path.join( sublime.packages_path( ), 'AcecoolCodeMappingSystem', 'maps', self.GetMapperFileName( ) + '.py' ) )


		## ## Get the configuration files timestamp max...
		## _timestamp_config_files_max = self.GetTimeStampMaxOfCfgFiles( _ext )

		## ## The maximum timestamp of all non-cached files..
		## _timestamp_max_all = max( _timestamp_file, _timestamp_mapper_user, _timestamp_mapper, _timestamp_core_files_max, _timestamp_config_files_max )

		## ## The easiest way to do this is math.Max all of the values except the cache then see if the cache is newer than all we reload the cache file...
		## if ( _timestamp_cached > _timestamp_max_all ):

		## 	## We load the Cache file...
		## 	return True

		## ## Return false - we Re-Process the File...
		## return False


	##
	## Returns the timestamp from the last time the file was processed, if ever. If not, it'll return 0..
	##
	def GetCachedFileTimeStamp( self, _file ):
		## If force refresh is enabled - we force reload the file every time...
		if ( self.GetSetting( 'caching_system_force_refresh', False ) ):
			return 0

		## Return the timestamp..
		return AcecoolLib.File.TimeStamp( self.GetCachePath( _file ) )


	##
	## Helper - Saves the cache file..
	##
	def SaveFileCache( self, _filename, _data = '' ):
		## ## Grab the path to and including a safe-filename to save the cached file...
		## _path = self.GetCachePath( _filename )

		## ## Output the information..
		## if ( self.GetSetting( 'caching_system_notify', False ) ):
		## 	print( ' >> Saving CACHED FILE: ' + _filename + ' as ' + _path )

		## ## Make sure we're not writing an empty file... I may allow this later...
		## if ( _data != None and _data.strip( ) != '' ):
		## 	return AcecoolLib.File.Write( _path, _data )
		return self.Database( ).UpdateCache( _filename, _data, '' )


	##
	## Helper - Returns the Cache directory by itself, or if the filename is provided then provide the full path with the converted file-name...
	##
	def GetCachePath( self, _file = None ):
		## Base path...
		## _path = PLUGIN_PATH_USER_CACHE
		_path = ''
		## print( ' >> PLUGIN_PATH_USER_CACHE == ' + PATH_PACKAGES )

		## If the filename is provided, we'll add that and convert it to the safe-string..
		if ( _file != None ):
			_path = os.path.join( sublime.packages_path( ), 'User', 'AcecoolCodeMappingSystem', 'cache', AcecoolLib.File.GetSafeName( _file ) )

		## Returns %AppData%\Sublime Text 3\Packages\User\CodeMap\Cache\ and adds the filename if _file isn't None...
		return _path


	##
	## Helper - Loads the cache file
	##
	def LoadFileCache( self, _filename ):
		## ## Grab the path to and including a safe-filename to save the cached file...
		## _path = self.GetCachePath( _filename )

		## ## Output the information
		## if ( self.GetSetting( 'caching_system_notify', False ) ):
		## 	print( ' >> Trying to LOAD CACHED File: ' + _filename + ' as ' + _path )

		## ## Try to read the file data... This function should only be called if the file exists...
		## return AcecoolLib.File.Read( _path )


		_exists, _row_data, _row_map, _row_time, _core_time = self.Database( ).HasCache( _file )
		return _row_data


	## ##
	## ## Helper - Returns math.Max of each of the core files ( XCodeMapper.py, XCodeMapper_Definitions.py and AcecoolST3_Library.py )
	## ##
	## def GetTimeStampMaxOfCoreFiles( self ):
	## 	## TimeStamp of each core file ( done this way to have a notification if needed )
	## 	_timestamp_xcmb		= AcecoolLib.File.TimeStamp( PLUGIN_PATH_FILE_CORE )
	## 	_timestamp_xcmbd	= AcecoolLib.File.TimeStamp( PLUGIN_PATH_FILE_CORE_DEFS )
	## 	_timestamp_acst3l	= AcecoolLib.File.TimeStamp( PLUGIN_PATH_FILE_CORE_LIB )

	## 	##	Notify
	## 	## self.print( 'Caching', ' >> Caching > the timestamps for each core file is: XCodeMapper.py( ' + str( _timestamp_xcmb ) + ' ); XCodeMapper_Definitions.py( ' + str( _timestamp_xcmbd ) + ' ); AcecoolST3_Library.py( ' + str( _timestamp_acst3l ) + '); ' )

	## 	## Return the max timestamp of all of These - we only need to prove that cache is greater than all to load cached file...
	## 	return max( _timestamp_xcmb, _timestamp_xcmbd, _timestamp_acst3l )


	## ##
	## ## Returns the max timestamp of the configuration files ( Default, User, and Custom Mapper )
	## ##
	## def GetTimeStampMaxOfCfgFiles( self, _ext ):
	## 	## Base Path
	## 	## _path = os.path.join( sublime.packages_path( ), 'User', 'CodeMap' )
	## 	## _path = os.path.join( sublime.packages_path( ), 'User' )


	## 	## Core / Default Config Files
	## 	_timestamp_cfg_core_plugin_definitions		= AcecoolLib.File.TimeStamp( PLUGIN_PATH_CFG_CORE_DEFS )
	## 	_timestamp_cfg_core_plugin_settings			= AcecoolLib.File.TimeStamp( PLUGIN_PATH_CFG_CORE_CORE )
	## 	_timestamp_cfg_core_map_defaults			= AcecoolLib.File.TimeStamp( PLUGIN_PATH_CFG_CORE_MAP )
	## 	_timestamp_cfg_core_map_active_defaults		= AcecoolLib.File.TimeStamp( os.path.join( PLUGIN_PATH_BASE_CFG, 'MapSettings_' + _ext + '.sublime-settings' ) )

	## 	## User Config Files
	## 	_timestamp_cfg_user_plugin_definitions		= AcecoolLib.File.TimeStamp( PLUGIN_PATH_CFG_CORE_USER_DEFS )
	## 	_timestamp_cfg_user_plugin_settings			= AcecoolLib.File.TimeStamp( PLUGIN_PATH_CFG_CORE_USER_CORE )
	## 	_timestamp_cfg_user_map_globals				= AcecoolLib.File.TimeStamp( PLUGIN_PATH_CFG_CORE_USER_MAP )
	## 	_timestamp_cfg_user_map_active_config		= AcecoolLib.File.TimeStamp( os.path.join( PLUGIN_PATH_USER_CFG, 'MapSettings_' + _ext + '.sublime-settings' ) )

	## 	## Newest...
	## 	## Task: Fix this...
	## 	_timestamp_cfg_a							= AcecoolLib.File.TimeStamp( os.path.join( sublime.packages_path( ), 'User', 'ACMS_Definitions.sublime-settings' ) )
	## 	_timestamp_cfg_b							= AcecoolLib.File.TimeStamp( os.path.join( sublime.packages_path( ), 'User', 'ACMS_Panels.sublime-settings' ) )
	## 	_timestamp_cfg_c							= AcecoolLib.File.TimeStamp( os.path.join( sublime.packages_path( ), 'User', 'ACMS_Mapping.sublime-settings' ) )
	## 	_timestamp_cfg_d							= AcecoolLib.File.TimeStamp( os.path.join( sublime.packages_path( ), 'User', 'ACMS_Plugin.sublime-settings' ) )
	## 	_timestamp_cfg_base_a						= AcecoolLib.File.TimeStamp( os.path.join( sublime.packages_path( ), 'AcecoolCodeMappingSystem', 'ACMS_Definitions.sublime-settings' ) )
	## 	_timestamp_cfg_base_b						= AcecoolLib.File.TimeStamp( os.path.join( sublime.packages_path( ), 'AcecoolCodeMappingSystem', 'ACMS_Panels.sublime-settings' ) )
	## 	_timestamp_cfg_base_c						= AcecoolLib.File.TimeStamp( os.path.join( sublime.packages_path( ), 'AcecoolCodeMappingSystem', 'ACMS_Mapping.sublime-settings' ) )
	## 	_timestamp_cfg_base_d						= AcecoolLib.File.TimeStamp( os.path.join( sublime.packages_path( ), 'AcecoolCodeMappingSystem', 'ACMS_Plugin.sublime-settings' ) )
	## 	## _timestamp_cfg_e							= AcecoolLib.File.TimeStamp( os.path.join( sublime.packages_path( ), 'User', 'ACMS_RunTime.sublime-settings' ) )
	## 	## _timestamp_cfg_f							= AcecoolLib.File.TimeStamp( os.path.join( sublime.packages_path( ), 'User', 'ACMS_Definitions.sublime-settings' ) )

	## 	## If the file doesn't exist - 0 is returned... otherwise the actual timestamp is returned..
	## 	return max( _timestamp_cfg_a, _timestamp_cfg_b, _timestamp_cfg_c, _timestamp_cfg_d, _timestamp_cfg_base_a, _timestamp_cfg_base_b, _timestamp_cfg_base_c, _timestamp_cfg_base_d, _timestamp_cfg_core_plugin_definitions, _timestamp_cfg_core_plugin_settings, _timestamp_cfg_core_map_defaults, _timestamp_cfg_core_map_active_defaults, _timestamp_cfg_user_plugin_definitions, _timestamp_cfg_user_plugin_settings, _timestamp_cfg_user_map_globals, _timestamp_cfg_user_map_active_config )



	##
	## End Caching System
	##






	##
	##
	##
	##
	##
	##
	##
	##
	##

	##
	def GetSettingsObject( self, _name = '' ):								return sublime.load_settings( _name )
	def SaveSettings( self, _name = '' ):									return sublime.save_settings( _name )
	## def SetSettings( self, _name = '', _key = '', _value = None ):		return self.GetSettingsObject( _name ).set( _key, _value )
	## def GetSettings( self, _name = '', _key = '', _value = None ):		return self.GetSettingsObject( _name ).get( _key, _value )

	## Sublime Text Preferences - Specifically for the Operating System in Use...
	__settings_file_syntax__												= '${syntax}.sublime-settings'
	def GetSyntaxSettingsFileName( self, _view = None, _syntax = None ):	return sublime.expand_variables( self.__settings_file_syntax__, { 'syntax': ( _syntax, AcecoolLib.Sublime.GetSyntaxName( _view ) )[ _syntax == None ] } )
	def GetSyntaxSettingsObject( self, _syntax = None ):					return self.GetSettingsObject( self.GetSyntaxSettingsFileName( None, _syntax ) )
	def SaveSyntaxSettings( self, _syntax = None ):							return self.SaveSettings( self.GetSyntaxSettingsFileName( None, _syntax ) )
	def HasSyntaxSetting( self, _key ):										return self.GetSyntaxSettingsObject( ).has( _key )
	def GetSyntaxSetting( self, _key, _default = None, _syntax = None ):	return self.GetSyntaxSettingsObject( _syntax ).get( _key, _default )
	def SetSyntaxSetting( self, _key, _value = None, _syntax = None ):		return self.GetSyntaxSettingsObject( _syntax ).set( _key, _value )

	## Sublime Text Preferences
	__settings_file_sublime__												= 'Preferences.sublime-settings'
	def GetSublimeSettingsFileName( self ):									return self.__settings_file_sublime__
	def GetSublimeSettingsObject( self ):									return self.GetSettingsObject( self.GetSublimeSettingsFileName( ) )
	def SaveSublimeSettings( self ):										return self.SaveSettings( self.GetSublimeSettingsFileName( ) )
	def HasSublimeSetting( self, _key ):									return self.GetSublimeSettingsObject( ).has( _key )
	def GetSublimeSetting( self, _key, _default = None ):					return self.GetSublimeSettingsObject( ).get( _key, _default )
	def SetSublimeSetting( self, _key, _value = None ):						return self.GetSublimeSettingsObject( ).set( _key, _value )

	## Sublime Text Preferences - Specifically for the Operating System in Use...
	__settings_file_platform__												= 'Preferences (${platform}).sublime-settings'
	def GetPlatformSettingsFileName( self ):								return sublime.expand_variables( self.__settings_file_platform__, { 'platform': AcecoolLib.Sublime.GetPlatformName( ) } )
	def GetPlatformSettingsObject( self ):									return self.GetSettingsObject( self.GetPlatformSettingsFileName( ) )
	def SavePlatformSettings( self ):										return self.SaveSettings( self.GetPlatformSettingsFileName( ) )
	def HasPlatformSetting( self, _key ):									return self.GetPlatformSettingsObject( ).has( _key )
	def GetPlatformSetting( self, _key, _default = None ):					return self.GetPlatformSettingsObject( ).get( _key, _default )
	def SetPlatformSetting( self, _key, _value = None ):					return self.GetPlatformSettingsObject( ).set( _key, _value )

	## Plugin Definitions
	__settings_file_definitions__											= 'ACMS_Definitions.sublime-settings'
	def GetDefinitionsSettingsFileName( self ):								return self.__settings_file_definitions__
	def GetDefinitionsSettingsObject( self ):								return self.GetSettingsObject( self.GetDefinitionsSettingsFileName( ) )
	def SaveDefinitionSettings( self ):										return self.SaveSettings( self.GetDefinitionsSettingsFileName( ) )
	def HasDefinitionsSetting( self, _key ):								return self.GetDefinitionsSettingsObject( ).has( _key )
	def GetDefinitionsSetting( self, _key, _default = None ):				return self.GetDefinitionsSettingsObject( ).get( _key, _default )
	def SetDefinitionsSetting( self, _key, _value = None ):					return self.GetDefinitionsSettingsObject( ).set( _key, _value )

	## Plugin Settings
	__settings_file_plugin__												= 'ACMS_Plugin.sublime-settings'
	def GetPluginSettingsFileName( self ):									return self.__settings_file_plugin__
	def GetPluginSettingsObject( self ):									return self.GetSettingsObject( self.GetPluginSettingsFileName( ) )
	def SavePluginSettings( self ):											return self.SaveSettings( self.GetPluginSettingsFileName( ) )
	def HasPluginSetting( self, _key ):										return self.GetPluginSettingsObject( ).has( _key )
	def GetPluginSetting( self, _key, _default = None ):					return self.GetPluginSettingsObject( ).get( _key, _default )
	def SetPluginSetting( self, _key, _value = None ):						return self.GetPluginSettingsObject( ).set( _key, _value )

	## Plugin Mapping Panel Configuration
	__settings_file_panels__												= 'ACMS_Panels.sublime-settings'
	def GetPanelsSettingsFileName( self ):									return self.__settings_file_panels__
	def GetPanelsSettingsObject( self ):									return self.GetSettingsObject( self.GetPanelsSettingsFileName( ) )
	def SavePanelSettings( self ):											return self.SaveSettings( self.GetPanelsSettingsFileName( ) )
	def HasPanelSetting( self, _key ):										return self.GetPanelsSettingsObject( ).has( _key )
	def GetPanelSetting( self, _key, _default = None ):						return self.GetPanelsSettingsObject( ).get( _key, _default )
	def SetPanelSetting( self, _key, _value = None ):						return self.GetPanelsSettingsObject( ).set( _key, _value )

	## Plugin Mapping Settings
	__settings_file_mapping__												= 'ACMS_Mapping.sublime-settings'
	def GetMapSettingsFileName( self ):										return self.__settings_file_mapping__
	def GetMappingSettingsObject( self ):									return self.GetSettingsObject( self.GetMapSettingsFileName( ) )
	def SaveMapSettings( self ):											return self.SaveSettings( self.GetMapSettingsFileName( ) )
	def HasMapSetting( self, _key ):										return self.GetMappingSettingsObject( ).has( _key )
	def GetMapSetting( self, _key, _default = None ):						return self.GetMappingSettingsObject( ).get( _key, _default )
	def SetMapSetting( self, _key, _value = None ):							return self.GetMappingSettingsObject( ).set( _key, _value )

	## Plugin RunTime Settings
	__settings_file_runtime__												= 'ACMS_RunTime.sublime-settings'
	def GetRunTimeSettingsFileName( self ):									return self.__settings_file_runtime__
	def GetRunTimeSettingsObject( self ):									return self.GetSettingsObject( self.GetRunTimeSettingsFileName( ) )
	def SaveRunTimeSettings( self ):										return self.SaveSettings( self.GetRunTimeSettingsFileName( ) )
	def HasRunTimeSetting( self, _key ):									return self.GetRunTimeSettingsObject( ).has( _key )
	def GetRunTimeSetting( self, _key, _default = None ):					return self.GetRunTimeSettingsObject( ).get( _key, _default )
	def SetRunTimeSetting( self, _key, _value = None ):						return self.GetRunTimeSettingsObject( ).set( _key, _value )



	##
	## Get plugin setting in this order...
	## RunTime [ _key ] > Syntax [ acms_key ] > Syntax [ _key ] > Map [ _key ] > Plugin [ _key ] > Panel [ _key ] > Definition [ _key ] > Platform [ acms_key ] > Sublime [ acms_key ] > Platform [ _key ] > Sublime [ _key ] > DEFAULT VALUE
	## RunTime config is important so it's first and can overwrite all - platform specific changes for sublime prefs, then sublime prefs next... then the syntax settings file in User/ folder.. then the Mapper Settings, Plugin, then plugin panel config, before plugin definitions and finally default value if nothing exists elsewhere...
	##
	## Altered: Became helper so we could dive into language keys first without running into other issues...
	##
	def GetSettingBase( self, _key = 'NOT_SET', _default = None ):
		## return self.GetRunTimeSetting( _key, self.GetSyntaxSetting( 'acms_' + _key, self.GetSyntaxSetting( _key, self.GetMapSetting( _key, self.GetPluginSetting( _key, self.GetPanelSetting( _key, self.GetDefinitionsSetting( _key, self.GetPlatformSetting( 'acms_' + _key, self.GetSublimeSetting( 'acms_' + _key, self.GetPlatformSetting( _key, self.GetSublimeSetting( _key, _default ) ) ) ) ) ) ) ) ) ) )
		return self.GetRunTimeSetting( _key, self.GetDefinitionsSetting( _key, self.GetSyntaxSetting( 'acms_' + _key, self.GetMapSetting( _key, self.GetPluginSetting( _key, self.GetPanelSetting( _key, _default ) ) ) ) ) )


	##
	## Get Setting string and convert the string to a variable value...
	##
	def GetSettingENUM( self, _key, _default_key = None, _default = None, _alt_tab = None ):
		## Grab the variable string-name
		_data = self.GetSetting( _key, _default )

		## If the var is a string, then grab the data from the globals or locals table - otherwise return the data as is...
		_value = self.GetVariableValueByName( _key, _default_key, _default, _alt_tab )
		if ( _value != None ):
			return _value

		## Return the data as-is otherwise...
		return _default


	##
	## Grab the variable value based on a variable name
	##
	## def GetVariableValueByName( _key, _default_key = None, _default = None, _alt_tab = None ):
	## 	## return globals( ).get( _key, locals( ).get( _key, _default ) )
	## 	if ( _default_key ):
	## 		_default = globals( ).get( _default_key, locals( ).get( _default_key, _default ) );

		## return globals( ).get( _key, locals( ).get( _key, _default ) );

	def GetVariableValueByName( self, _key, _default_key = None, _default = None, _alt_tab = { } ):

		## return globals( ).get( _key, locals( ).get( _key, _default ) )
		if ( _default_key ):
			_default_override = ( _default, _alt_tab.get( _default_key, _default ) )[ len( _alt_tab ) > 0 ]
			_default = globals( ).get( _default_key, locals( ).get( _default_key, _default_override ) );

		##
		_default_override = ( _default, _alt_tab.get( _key, _default ) )[ len( _alt_tab ) > 0 ]
		return globals( ).get( _key, locals( ).get( _key, _default_override ) );

	## def GetVariableValueByName( self, _key, _default_key = None, _default = None, _alt_tab = None ):
	## 	if ( _default_key ):
	## 		_default = globals( ).get( _default_key, locals( ).get( _default_key, ( _default, _alt_tab.get( _default_key, _default ) )[ _alt_tab != None ] ) )

	## 	return globals( ).get( _key, locals( ).get( _key, ( _default, _alt_tab.get( _default_key, _default ) )[ _alt_tab != None ] ) )

	##
	## Helper
	##
	def GetSourceViewSyntaxData( self ):
		## If the view is valid
		if ( AcecoolLib.Sublime.IsValidView( self.GetSourceView( ) ) ):
			## Grab the source view settings
			_settings = self.GetSourceView( ).settings( )

			## If the settings object is valid
			if ( _settings ):
				## Grab the syntax from the view settings
				_syntax_path = _settings.get( 'syntax', None )

				if ( _syntax_path != None ):
					_syntax, _syntax_ext = os.path.splitext( os.path.basename( _syntax_path ) )
					_syntax_ext = _syntax_ext[ 1 :  ]

					## Snip off packages - this is good for applying it to a view, but not when our starting folder is packages for EVERYTHING ELSE ( Even Sublime Text use it for everything EXCEPT this )...
					if ( _syntax_path.startswith( 'Packages' ) ):
						_syntax_path = _syntax_path[ 9 : ]

					##
					return ( _syntax, _syntax_ext, _syntax_path )
				else:
					return ( 'ACMS_Default_Ext', 'ACMS_Default_Ext', 'ACMS_Default_Ext' )

			## If not valid source view... return some base data...
			return ( 'ACMS_Default_Ext', 'ACMS_Default_Ext', 'ACMS_Default_Ext' )

	## Helpers for the file-extension to language id mapping key / values - Note: This will eventually be a table of data so the structure will later match below - however now it is a single string so we don't need .get / .has, etc..
	def GetDefaultLanguageMap( self, _default = 'ACMS_Default_Language' ):								return self.GetDefinitionsSetting( 'acms_language_map->ACMS_Default_Ext', _default )
	def GetLanguageMap( self, _ext = 'ACMS_Default_Ext', _default = 'ACMS_Default_Language' ):			return self.GetDefinitionsSetting( 'acms_language_map->' + str( _ext ), self.GetDefaultLanguageMap( _default ) )

	## Helpers for the language id configuration table data...
	def GetDefaultLanguageSettings( self, _default = { } ):												return self.GetDefinitionsSetting( 'acms_language_config->ACMS_Default_Language', _default )
	def GetLanguageSettings( self, _lang = 'ACMS_Default_Language', _default = { } ):					return self.GetDefinitionsSetting( 'acms_language_config->' + str( _lang ), self.GetDefaultLanguageSettings( _default ) )
	def GetLanguageSettingEx( self, _lang = 'ACMS_Default_Language', _key = '', _default = None ):		return self.GetDefinitionsSetting( 'acms_language_config->' + str( _lang ) + '->' + str( _key ), _default )
	def HasLanguageSetting( self, _lang = 'ACMS_Default_Language', _key = '' ):							return self.GetLanguageSettings( _lang ).has( _key )
	def GetLanguageSetting( self, _lang = 'ACMS_Default_Language', _key = '', _default = None ):		return self.GetLanguageSettingEx( _lang, _key, self.GetLanguageSettings( _lang ).get( _key, _default ) )

	## Converts a file extension into a String Return for the Language ID to be used by way of language_map[ syntax ] and a default of the default language...
	def GetSourceViewSyntax( self ):
		##
		_syntax = self.GetSourceFileExt( )

		##
		return self.GetLanguageMap( ( _syntax, 'ACMS_Default_Language' )[ _syntax == None or not AcecoolLib.IsString( _syntax ) ] )


	## New func... Can call GetSettingBase several times to grab the key - lang first, if None then normal or default...
	def GetSetting( self, _key = 'NOT_SET', _default = None ):
		## If the language table exists, then see if the key / data exist...
		_lang_setting = self.GetLanguageSetting( self.GetSourceViewSyntax( ), _key, None )

		## If the key exists, return it...
		if ( _lang_setting != None ):
			## print( 'GetSetting -> ' + str( self.GetSourceViewSyntax( ) ) + ' -> ' + str( _key ) + ' == _lang_setting == ' + str( _lang_setting ) )
			return _lang_setting

		## Otherwise return the default / normal way to grab the key / data...
		return self.GetSettingBase( _key, _default )


	##
	## Load all settings into memory...
	## Task: Make this dynamically load the files from settings\ext\, settings\plugin\, settings\lang\, etc... OR?? and User\ACMS\settings\*... So if a file exists there that doesn't exist here.. it'll be default and user.. also, for the extension settings, I can simply load them as files are opened..
	##
	def LoadAllSettings( self ):
		## Sublime Text Preferences and Preferences for the Platform / Operating System
		self.GetSublimeSettingsObject( )
		self.GetPlatformSettingsObject( )
		self.GetSyntaxSettingsFileName( )

		## Plugin Preferences, Definitions, etc...
		self.GetDefinitionsSettingsObject( )
		self.GetMappingSettingsObject( )
		self.GetPanelsSettingsObject( )
		self.GetPluginSettingsObject( )
		self.GetRunTimeSettingsObject( )


	##
	## Save All Settings - Right now all we do is alter the RunTime configuration.. nothing else...
	##
	def SaveAllSettings( self ):
		## self.SaveDefinitionSettings( )
		## self.SaveMapSettings( )
		## self.SavePanelSettings( )
		## self.SavePluginSettings( )
		self.SaveRunTimeSettings( )


	##
	## Simple Helpers
	##

	##
	def GetPackageJoinedFolder( self, _base, _extra = None ):
		if ( not _extra ):
			return _base
		return os.path.join( _base, _extra )

	## Returns the Package Folder
	def GetPackageFolder( self, _extra = None ):					return self.GetPackageJoinedFolder( os.path.join( sublime.packages_path( ), self.GetPackageName( ) ), _extra )

	## Returns the Package Folder
	def GetPackageUserFolder( self, _extra = None ):				return self.GetPackageJoinedFolder( os.path.join( sublime.packages_path( ), 'User', self.GetPackageName( ) ), _extra )

	## Returns the Package Folder
	def GetPackageMapsFolder( self, _extra = None ):				return self.GetPackageJoinedFolder( os.path.join( self.GetPackageFolder( ), 'maps' ), _extra )

	## Returns the Package Folder
	def GetPackageUserMapsFolder( self, _extra = None ):			return self.GetPackageJoinedFolder( os.path.join( self.GetPackageUserFolder( ), 'maps' ), _extra )

	## Returns the Package Folder
	def GetPackageSettingsFolder( self, _extra = None ):			return self.GetPackageJoinedFolder( os.path.join( self.GetPackageFolder( ), 'settings' ), _extra )

	## Returns the Package Folder
	def GetPackageUserSettingsFolder( self, _extra = None ):		return self.GetPackageJoinedFolder( os.path.join( self.GetPackageUserFolder( ), 'settings' ), _extra )

	## Returns the Package Folder
	def GetSaveSettingsPath( self, _extra = None ):					return self.GetPackageJoinedFolder( os.path.join( self.GetPackageName( ), 'settings' ), _extra )

	## Returns the Package Folder
	def GetCodeMapUserMapsFolder( self, _extra = None ):			return self.GetPackageJoinedFolder( os.path.join( sublime.packages_path( ), 'User', 'CodeMap', 'custom_mappers' ), _extra )

	## Return the name of the mapping file we use for scratch...
	def GetPanelFileName( self, _extra = None ):					return self.GetPackageJoinedFolder( os.path.join( self.GetPackageUserFolder( ), self.GetPanelName( ) ), _extra )


	##
	## Initialize anything left to initialize
	##
	def Init( self ):
		## If we're debugging the specific task / Function then allow the output...
		if ( self.GetSettingDebugging( '', True ) ):
			self.Notify( '-> Init( )' )

		## Load the settings references..
		## self.LoadAllSettings( )

		## If the plugin hasn't been installed yet, then install the structure, etc...
		if ( not self.HasRunTimeSetting( 'plugin_installed' ) or self.GetRunTimeSetting( 'plugin_installed', '0.0.0' ) != self.__version__ ):
			## Run any Installation necessary such as creating the AcecoolCodeMappingSystem\ folder in User\, the cache\ folder, creating files, moving files over, and more...
			self.Install( )

			## Update and save RunTime Config
			self.SetRunTimeSetting( 'plugin_installed', self.__version__ )
			self.SaveRunTimeSettings( )


		## Extract all map files ( if the file-size is different than what exists ) to the folder...
		## AcecoolLib.SublimeResource.ExtractFiles( 'C:/Users/Acecool/Downloads/SublimePackageTest', '', True, 'C:/Users/Acecool/Downloads/SublimePackageTest' );
		## AcecoolLib.SublimeResource.ExtractFiles( 'C:/Users/Acecool/Downloads/SublimePackageTest', '', True, 'C:/Users/Acecool/Downloads/SublimePackageTest' );
		## AcecoolLib.SublimeResource.ExtractFiles( 'C:/Users/Acecool/Downloads/SublimePackageTest', '', True, 'C:/Users/Acecool/Downloads/SublimePackageTest' );
		## AcecoolLib.SublimeResource.ExtractFiles( 'C:/Users/Acecool/Downloads/SublimePackageTest', 'maps/', True, 'C:/Users/Acecool/Downloads/SublimePackageTest' );
		## AcecoolLib.SublimeResource.ExtractFiles( 'C:/Users/Acecool/Downloads/SublimePackageTest', 'Acecool.py', True, 'C:/Users/Acecool/Downloads/SublimePackageTest' );
		## AcecoolLib.SublimeResource.ExtractFiles( 'C:/Users/Acecool/Downloads/SublimePackageTest', [ 'Acecool.py', 'AcecoolCodeMappingSystem.py' ], True, 'C:/Users/Acecool/Downloads/SublimePackageTest' );
		print( 'Acecool Code Mapping System -> InstallPluginFiles -> Extracting all Language Maps!' );
		## AcecoolLib.SublimeResource.ExtractFiles( os.path.join( sublime.packages_path( ), 'ACMS_TestExtraction' ), { 'Acecool.py': True, 'maps/': True }, True, 'C:/Users/Acecool/Downloads/SublimePackageTest/AcecoolCodeMappingSystem.sublime-package' );
		AcecoolLib.SublimeResource.ExtractFiles( os.path.join( sublime.packages_path( ), 'AcecoolCodeMappingSystem' ), 'maps/', True );

		##
		_maps = os.path.join( sublime.packages_path( ), 'AcecoolCodeMappingSystem', 'maps' );
		_user_maps = os.path.join( sublime.packages_path( ), 'User', 'AcecoolCodeMappingSystem', 'maps' );
		_user_maps_default = os.path.join( _user_maps, 'default' );

		##
		print( 'Copying default user map to User/AcecoolCodeMappingSystem/maps/default/ to serve as a template - do not put anything is this folder, it is meant for default user maps only - editing a file in here will cause all changes to be lost and editing in this folder will not have any effect - put into User/AcecoolCodeMappingSystem/maps/ to function - we put the defaults in this folder so we do not overwrite any you have placed or created in the correct folder!' );

		## For each map file - if ends in _user.py then we move copy and replace it to the User/AcecoolCodeMappingSystem/maps/defaults/ folder..
		# for _name in AcecoolLib.Folder.ListFiles( os.path.join( sublime.packages_path( ), 'AcecoolCodeMappingSystem', 'maps' ), '_User.py' ):
		for _name in AcecoolLib.Folder.ListFiles( _maps, '.py' ):
			## If the timestamp is different, make a copy or replace it...
			if ( AcecoolLib.File.TimeStamp( os.path.join( _maps, _name ) ) != AcecoolLib.File.TimeStamp( os.path.join( _user_maps_default, _name ) ) ):
				##
				AcecoolLib.File.Copy( _name, _maps, _user_maps_default );


			if ( not AcecoolLib.File.Exists( os.path.join( _user_maps, _name ) ) ):
				AcecoolLib.File.Copy( _name, _maps, _user_maps );

		##
		AcecoolLib.File.Write( os.path.join( _user_maps, '__readme_first__.txt' ), '''//
// The AcecoolCodeMappingSystem User Maps folder
//

This folder "''' + _user_maps + '''"
is the folder you may copy maps to, create new maps in, etc... If new maps are added
to Acecool's Code Mapping System, then they will be copied here. No files in this
directory will be overwritten by default.

Only new files will be copied to this directory, making it safe for you to use.


The "''' + _user_maps_default + '''"
directory is overwritten when any changes are detected. That defaults folder is there
in case you need to revert a map to default, or to see examples of other maps which
exist. Do NOT EDIT anything in that directory as all changes will be lost. DO NOT EDIT
ANYTING IN: ''' + _user_maps_default );

		##
		AcecoolLib.File.Write( os.path.join( _user_maps_default, '__readme_default_maps_path__DO_NOT_EDIT_WITHIN__.txt' ), '''//
// The AcecoolCodeMappingSystem Default Maps folder
//

This "''' + _user_maps_default + '''"
directory is overwritten when any changes are detected. The only purpose of this folder
is to house default copies of original maps which come with Acecool's Code Mapping System.
If any are altered, they will be reverted back to default!


If you want to create your own maps, or edit any existing maps, use the following directory:
"''' + _user_maps + '''"
which will retain changes!''' );


		## ## View / File, the View ID, the Views Index and the GroupID...
		## self.SetupAccessorFunc( 'OutputView', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'OutputViewID', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'OutputViewIndex', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'OutputGroupID', None, 'Get', None, True, True )

		## ## The file we need to map - this gets set when we activate a file - if not ACMS - Panel, or Code - Map, etc... and has an extension... set it so we know what to map...
		## self.SetupAccessorFunc( 'SourceView', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'SourceFileName', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'SourceFileExt', None, 'Get', None, True, True )

		## ##
		## self.SetupAccessorFunc( 'SourceViewFileSyntax', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'SourceViewFileSyntaxExt', None, 'Get', None, True, True )
		## self.SetupAccessorFunc( 'SourceViewFileSyntaxPath', None, 'Get', None, True, True )

		## Run any other Setup necessary....
		self.Setup( )

		## Now, run the plugin...
		self.Run( )


	##
	## Run any setup which is important such as creating folders in the User\ folder, etc..., copying files, etc.. and more..
	## Installation - Run any install relating procedures such as creating folders, moving files, etc...
	##
	def Install( self ):
		## If we're debugging the specific task / Function then allow the output...
		if ( self.GetSettingDebugging( 'Install', True ) ):
			self.Notify( '-> Install( )' );

		## Create all missing plugin folders
		self.InstallPluginStructure( );


	##
	## Finish setting up the plugin - locate the open panel, if open, etc...
	##
	def Setup( self ):
		## If we're debugging the specific task / Function then allow the output...
		if ( self.GetSettingDebugging( 'Setup', True ) ):
			self.Notify( '-> Setup( )' );

		## Determine if the panel is open, and update the vars...
		if ( self.DetectPanel( ) ):
			self.ApplyOutputViewSettings( self.GetOutputView( ) );

		##
		_source_filename = self.GetRunTimeSetting( 'source_filename', None );

		##
		if ( _source_filename ):
			##
			_path	= self.GetRunTimeSetting( 'source_path', None );
			_file	= self.GetRunTimeSetting( 'source_filename', None );
			_ext	= self.GetRunTimeSetting( 'source_ext', None );


			## If we're debugging the specific task / Function then allow the output...
			if ( self.GetSettingDebugging( 'Setup', True ) ):
				##
				print( '_source_path = ' + str( _path ) );
				print( '_source_filename = ' + str( _file ) );
				print( '_source_ext = ' + str( _ext ) );

			##
			_view = AcecoolLib.Sublime.FindView( _path );
			if ( _view == None ):
				_view = AcecoolLib.Sublime.FindView( _file );

			##
			if ( _view != None ):
				self.ResetViewInfo( _view, True );

		##
		if ( self.GetSetting( 'acms_open_sublime_console', False ) ):
			AcecoolLib.Sublime.ToggleConsole( );

		##
		if ( self.GetSetting( 'acms_enable_sublime_log_commands', False ) ):
			sublime.log_commands( True );

		## Setup a configuration callback so if threading is enabled / disabled, the appropriate action can be taken...
		self.GetPluginSettingsObject( ).add_on_change( 'use_plugin_mapping_threads', lambda this, _value: ( plugin_thread_start( ), plugin_thread_stop( ) )[ _value == False ] );





	##
	## GetSetting Helper - Returns whether or not we're debugging the specific task submitted...
	##
	def GetSettingDebugging( self, _task_id, _in_plugin_object = False ):
		## Grab the task setting because we will re-use it several times... Default as empty string for simplicity...
		_task = self.GetSetting( 'debugging_task', '' );

		## If debugging mode is enabled and task string is either empty, or set to 'Plugin'-wide ( if in plugin object is true - then also see if task id is Plugin.<TaskID> ) or task is the supplied task id
		return ( self.GetSetting( 'debugging_mode', False ) and ( _task == '' or _task == _task_id or ( _in_plugin_object and _task == 'Plugin' or _task == 'Plugin.' + _task_id ) ) );

	##
	## Run the plugin itself - ie populate the panel, if it is open... If using the advanced panel, and the panel was open the last time Sublime Text closed, then open it... Or populate it if it was open - otherwise perform a soft-shut-down...
	##
	def Run( self ):
		## If we're debugging the specific task / Function then allow the output...
		if ( self.GetSettingDebugging( 'Run', True ) ):
			self.Notify( '-> Run( )' );


	##
	## Shutdown the plugin - Close the panel, if open, remove everything from memory, etc.. Return the data stored in case we want to move the data to a new plugin...
	##
	def Shutdown( self ):
		## If we're debugging the specific task / Function then allow the output...
		if ( self.GetSettingDebugging( 'Shutdown', True ) ):
			self.Notify( '-> Shutdown( )' );

		## Task: Setup data system so all data is stored in data...
		## return self.data


	##
	## Create all files and folders - file tables have the object to copy a file from elsewhere, and only replace if CRC has not changed from previous version, etc...
	##
	def InstallPluginStructure( self ):
		## If we're debugging the specific task / Function then allow the output...
		if ( self.GetSettingDebugging( 'InstallPluginStructure', True ) ):
			self.Notify( '-> InstallPluginFileStucture( )' );

		## Helper.. The first entries are currently set as folders only...
		_structure = self.GetPluginStructure( );

		## Cache the last mapped file in a separate file...
		## Acecool.File.Write( self.GetPanelFileName( ), '' );

		## Process base values... The first entries will need to be folders... this may change later - but this locks them into Packages/PackageName/ and Packages/User/ - not really, but hey...
		for _folder in _structure:
			## print( 'Installing Folder: ' + _folder );
			self.ProcessPluginStructure( sublime.packages_path( ), _folder, _structure.get( _folder, None ) );





	##
	## Install / Create all Plugin Folders so all paths exist...
	##
	def ProcessPluginStructure( self, _base, _name, _data ):
		##
		## self.Notify( '-> ProcessPluginStructure( _base = ' + str( _base ) + ',\t\t_name = ' + str( _name ) + ',\t\t_data = ' + str( _data ) + ' )' )

		## Combine the base path with the folder or file-name to create a single usable path...
		_path = os.path.join( _base, _name )

		## If the data type we've found IS a file or contains more information.
		if ( AcecoolLib.IsDict( _data ) and _data.get( 'type', PLUGIN_INSTALL_TYPE_PATH ) == PLUGIN_INSTALL_TYPE_FILE ):
			## if it doesn't exist, create it...
			if ( not AcecoolLib.File.Exists( _path ) ):
				## Try to grab the data from the authorized keys... I create these here for simplicity..
				_contents_a = _data.get( 'data', None )
				_contents_b = _data.get( 'contents', None )
				_contents	= ( ( _contents_a, _contents_b )[ not _contents_a ], '' )[ not _contents_a and not _contents_b ] #TernaryFunc( not _contents_a and not _contents_b, '', TernaryFunc( not _contents_a, _contents_b, _contents_a ) )

				## Let the user know via the console what we're doing...
				self.Notify( '-> CreateFile( ' + _path + ', _data_truncated = "' + _contents[ 0 : 25 ] + '" )!' )

				## Create the file using contents from a, b or empty string... If both are None return ''.. Else.. if a is None return b, else return a...
				AcecoolLib.File.Create( _path, _contents )

		if ( not AcecoolLib.IsDict( _data ) or AcecoolLib.IsDict( _data ) and ( _data.get( 'type', PLUGIN_INSTALL_TYPE_PATH ) == PLUGIN_INSTALL_TYPE_PATH ) ):
			## If the data type we've found isn't a file... Basically if _data is True then it's a folder... if it is a dict, if type is set and the type isn't set to FILE, then we can determine it is a folder we're looking at...
			## Else means it shouldn't be a file - ie Not Dict OR == PATH, inverse - If the folder doesn't exist, create it...
			if ( not AcecoolLib.Folder.Exists( _path ) ):
				## Let the user know via the console what we're doing...
				self.Notify( '-> CreateFolder( ' + _path + ' )' )

				## Create the folder...
				AcecoolLib.Folder.Create( _path )

		## If we have data left to process, we process it...
		if ( AcecoolLib.IsDict( _data ) and _data.get( 'type', PLUGIN_INSTALL_TYPE_PATH ) != PLUGIN_INSTALL_TYPE_FILE ):
			## For each _entry in the Dictionary { } of the _data / Dictionary we were provided.
			for _entry in _data:
				self.ProcessPluginStructure( _path, _entry, _data.get( _entry, None ) )


	## ##
	## ##
	## ##
	## def InstallPluginFolders( self ):
	## 	## Helper..
	## 	_struct = self.GetPluginStructure( )

	## 	## Process base values... The first layer is folders.. may change this later so files can be put into Packages\ root dir... although I don't see much of a point at this current moment in time - if there is a reason for it which I discover then I'll update the code...
	## 	for _folder in _struct:
	## 		## print( 'Installing Folder: ' + _folder )
	## 		self.InstallPluginFolder( sublime.packages_path( ), _folder, _struct.get( _folder, None ) )


	## ##
	## ## Install / Create all Plugin Folders so all paths exist...
	## ##
	## def InstallPluginFolder( self, _base, _name, _struct ):
	## 	## Update the base..
	## 	_base = os.path.join( _base, _name )

	## 	## If the folder doesn't exist, create it...
	## 	if ( not AcecoolLib.Folder.Exists( _base ) ):
	## 		## Let the user know via the console what we're doing...
	## 		self.Notify( 'Create Missing Plugin Folder:\t\t' + _base )

	## 		## Create the folder...
	## 		AcecoolLib.Folder.Create( _base )

	## 	## If we have more folders to process...
	## 	if ( AcecoolLib.IsDict( _struct ) ):
	## 		## Install all nested folders...
	## 		for _folder in _struct:
	## 			self.InstallPluginFolder( _base, _folder, _struct.get( _folder, None ) )


	## ##
	## ##
	## ##
	## def InstallPluginFiles( self ):
	## 	## Cache the last mapped file in a separate file...
	## 	AcecoolFile.Write( self.GetPanelFileName( ), '' );


	##
	##
	##
	def SublimeWindowToString( self, _window ):
		##
		_col_name			= 45
		_col_short			= 11
		_col_ids			= 30
		_col_data			= 250

		## sheets()	[Sheet]	Returns all open sheets in the window.
		## sheets_in_group(group)	[Sheet]	Returns all open sheets in the given group.
		## views()	[View]	Returns all open views in the window.
		## views_in_group(group)	[View]	Returns all open views in the given group.
		## num_groups()	int	Returns the number of view groups in the window.
		## active_group()	int	Returns the index of the currently selected group.
		## is_menu_visible()	bool	Returns True if the menu is visible.
		## is_sidebar_visible()	bool	Returns True if the sidebar will be shown when contents are available.
		## get_tabs_visible()	bool	Returns True if tabs will be shown for open files.
		## set_minimap_visible(flag)	None	Controls the visibility of the minimap.
		## is_status_bar_visible()	bool	Returns True if the status bar will be shown.
		## folders()	[str]	Returns a list of the currently open folders.
		## project_file_name()	str	Returns name of the currently opened project file, if any.
		## project_data()	dict	Returns the project data associated with the current window. The data is in the same format as the contents of a .sublime-project file.

		##
		_id					= _window.id( )
		_sheets				= _window.sheets( )
		_views				= _window.views( )
		_groups				= _window.num_groups( )
		_folders			= _window.folders( )
		_project			= _window.project_file_name( )
		_project_data		= _window.project_data( )
		_group				= _window.active_group( )
		_menu				= _window.is_menu_visible( )
		_sidebar			= _window.is_sidebar_visible( )
		_tabs				= _window.get_tabs_visible( )
		_minimap			= _window.is_minimap_visible( )
		_statusbar			= _window.is_status_bar_visible( )

		##
		_layout				= sublime.active_window( ).layout( )
		_layout_rows		= len( _layout.get( 'rows', [ ] ) ) - 1
		_layout_cols		= len( _layout.get( 'cols', [ ] ) ) - 1
		_layout_cells		= len( _layout.get( 'cells', [ ] ) )




		##
		_text = ''
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Window Identifiers:', _col_ids, 'Active Group: ' + str( _group ) + ' [ ' + str( _group + 1 ) + ' of ' + str( _groups ) + ' ]', _col_ids, 'ID: ' + str( _id ) )
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Window Layout Info:', _col_data, str( _layout_rows ) + ' Row' + AcecoolLib.TernaryFunc( _layout_rows == 0 or _layout_rows > 1, 's', '' ) + ' ' + AcecoolLib.TernaryFunc( _layout_rows > 3, '≡≡≡', AcecoolLib.TernaryFunc( _layout_rows > 2, '▄=◙=▀', AcecoolLib.TernaryFunc( _layout_rows > 1, '▄-▀', '░' ) ) ) + ', ' + str( _layout_cols ) + ' Column' + AcecoolLib.TernaryFunc( _layout_cols == 0 or _layout_cols > 1, 's', '' ) + ' ▒' + AcecoolLib.String.repeat( '|▒', _layout_cols - 1 ) + ', ' + str( _layout_cells ) + ' Cell' + AcecoolLib.TernaryFunc( _layout_cells == 0 or _layout_cells > 1, 's', '' ) + ' ' + AcecoolLib.String.repeat( '▒ ', _layout_cells ) )
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Window Layout Raw:', _col_data, str( _layout ) )
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Window Project / Session:', _col_data, _project )
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Window Project / Session Data:' ) + str( _project_data )
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Window Open Folders:' ) + str( _folders )
		## _text += '\n\t\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Window Sheets:', _col_ids, 'Sheets: ' + str( len( _sheets ) ) + ' - ' ) + str( _sheets )
		## _text += '\n\t\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Window Views:', _col_ids, 'Views: ' + str( len( _views ) ) + ' - ' ) + str( _views )
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Window Sheets:', _col_short, 'Sheets:', _col_short, str( len( _sheets ) ), _col_data, str( _sheets ) )
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Window Views:', _col_short, 'Views:', _col_short, str( len( _views ) ), _col_data, str( _views ) )
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Window Visibles:', _col_short, 'Sidebar:', _col_ids, AcecoolLib.TernaryFunc( _sidebar, 'Visible', 'Not' ), _col_short, 'TabsBar:', _col_ids, AcecoolLib.TernaryFunc( _tabs, 'Visible', 'Not' ), _col_short, 'Statusbar:', _col_ids, AcecoolLib.TernaryFunc( _statusbar, 'Visible', 'Not' ) )
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Window Visibles:', _col_short, 'Menu:', _col_ids, AcecoolLib.TernaryFunc( _menu, 'Visible', 'Not' ), _col_short, 'MiniMap:', _col_ids, AcecoolLib.TernaryFunc( _minimap, 'Visible', 'Not' ) )
		_text += '\n'

		##
		return _text


	##
	##
	##
	def SublimeViewToString( self, _name, _view ):
		## Is the view valid?
		if ( not AcecoolLib.Sublime.IsValidView( _view ) ):
			return 'Error: View submitted to SublimeViewToString is not a valid Sublime View...\n'

		##
		_col_name				= 45
		_col_ids				= 30
		_col_data				= 250


		## Make sure _window is valid... We grab the active window, or the window from the source view...
		## _window					= sublime.active_window( )

		## if ( _view_valid ):
		_window				= _view.window( )



		_id						= _view.id( ) #( _view.id( ), -1 )[ _view_valid ]
		_groups					= _window.num_groups( )
		_group, _index			= _window.get_view_index( _view )

		##
		_file					= AcecoolLib.Sublime.GetViewFileName( _view )
		_path					= AcecoolLib.Sublime.GetViewFileName( _view, True )
		_syntax					= _view.settings( ).get( 'syntax', 'None' )[ 9 : ]

		_syntax_base, _syntax_ext = os.path.splitext( os.path.basename( _syntax ) )

		## _text += '\n\t\t> ' + AcecoolLib.String.FormatColumnStripR( _col_name, _name + ' View Grouping:', _col_data, AcecoolLib.String.FormatColumn( _col_ids, 'Group: ' + str( _group ), _col_ids, 'Group Index: ' + str( _index ), _col_ids, 'ID: ' + str( _id ) ) )
		## _text += '\n\t\t> ' + AcecoolLib.String.FormatColumnStripR( _col_name, _name + ' View Grouping:', _col_data, 'ID: ' + str( _id ) + ', Group: ' + str( _group ) + ', Index in Group: ' + str( _index ) )

		##
		_text = ''
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, _name + ' View Identifiers:', _col_ids, 'View Group: ' + str( _group ) + ' [ ' + str( _group + 1 ) + ' of ' + str( _groups ) + ' ]', _col_ids, 'Group Index: ' + str( _index ), _col_ids, 'ID: ' + str( _id ) )
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, _name + ' View:', _col_data, str( _view ) + ' - ' + str( type( _view ) ) )
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, _name + ' View File-Name Base:', _col_data, _file )
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, _name + ' View File=Name Full:', _col_data, _path )
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, _name + ' Cached File-Name:', _col_data, AcecoolLib.File.SafeName( _path ) )
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, _name + ' Syntax Path:', _col_data, _syntax )
		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, _name + ' Language:', _col_data, _syntax_base )
		_text += '\n'

		return _text


	##
	## ToString Method
	##
	def __str__( self ):
		##
		_col_name				= 45;
		_col_ids				= 30;
		_col_short				= 11;
		_col_data				= 250;

		## Plugin Views - Source Panel ( Left ) and Output Panel( s ) ( Right );
		_view_source			= self.GetSourceView( );
		_view_output			= self.GetOutputView( );

		##
		if ( not IsPluginObjectValid( ) ):
			return 'Plugin Object ACMS not valid!';

		## Make sure _window is valid... We grab the active window, or the window from the source view...
		_window									= sublime.active_window( );

		##
		if ( AcecoolLib.Sublime.IsValidView( _view_source ) ):
			_window								= _view_source.window( );

		##
		_plugin_valid							= IsPluginObjectValid( );
		_is_plugin_valid						= AcecoolLib.TernaryFunc( _plugin_valid, 'Valid', 'InValid' );
		_is_plugin_open							= AcecoolLib.TernaryFunc( self.IsPanelOpen( ) == True, 'Opened', 'Closed' );

		##
		## _active_language		= self.GetSetting( 'active_language', 'Default' );
		## _active_mapper			= self.GetSetting( 'active_mapper', 'User' );
		## _active_mapper_default	= ( _active_mapper == None or _active_mapper == 'Default' or _active_mapper == '' );

		## ## Grab the mapper filename.. excluding ext for now...
		## ## If the language name is Default, instead of Default.py it'll be __default__.py to appear before all others..
		## _mapper_filename		= TernaryFunc( _active_language == 'Default' and _active_mapper_default, '__default__', _active_language );

		## ## If the mapper isn't Default then add it to the name ( this is so instead of Python_Default.py being wedged in the middle somewhere - it'll show up above all others... )...
		## _mapper_filename		+= TernaryFunc( _active_mapper_default, '', '_' + _active_mapper );

		##
		( _class_name, _class_name_a, _class_name_b ) = self.GetMapperClassNamesList( );
		_source_ext								= self.GetSetting( 'source_ext', 'None' );
		_active_language_name					= self.GetActiveLanguageName( );
		_active_language						= self.GetActiveLanguage( );
		_active_mapper							= self.GetActiveMapper( );
		_active_class							= self.GetActiveClass( );
		_active_mapper_filename					= self.GetMapperFileName( ) + '.py';
		_active_class_parent					= ( 'ACMS_' + _active_language + '_Default', 'XCodeMapperBase' )[ _active_mapper == 'Default' ];


		##
		## Plugin Base Data
		##
		_object_details_plugin_data				= '';
		## _object_details_plugin_data				+= '\n';
		## _object_details_plugin_data				+= '\n◙◙◙ Plugin Data ◙◙◙';
		_object_details_plugin_data				+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Definitions -> plugin_name:',					_col_data, self.GetSetting( 'plugin_name', 'None' ) ) + '\n';
		_object_details_plugin_data				+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Definitions -> plugin_author:',					_col_data, self.GetSetting( 'plugin_author', 'None' ) ) + '\n';
		_object_details_plugin_data				+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Definitions -> plugin_company:',				_col_data, self.GetSetting( 'plugin_company', 'None' ) ) + '\n';
		_object_details_plugin_data				+= '\n';
		_object_details_plugin_data				+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Plugin Validity:',								_col_data, _is_plugin_valid ) + '\n';

		## Task: When it is possible to open more than one panel, or link a panel in a different window - ad the number of panels opened here...
		if ( _plugin_valid ):
			_object_details_plugin_data			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Plugin Panel Open-State:',						_col_data, _is_plugin_open ) + '\n';

		##
		_object_details_plugin_data				+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Plugin Run-Mode:',								_col_data, 'Run-In Window Where Used [ vs Run in Own Window ]' ) + '\n';
		_object_details_plugin_data				+= '\t\tNote: This shows you how the plugin is set to operate... It can run in its own window mapping any file which is focused, or, by default, run in the window where it is used.\n';
		_object_details_plugin_data				+= '\t\tNote: This will be included in a future update - a system to allow the plugin to run in its own window, or to run in as many windows as you want. If you run in a window with files, it won\'t map any other file..\n';


		##
		## Configuration Data
		## Task: Update RunTime Config output and stored data so it conincides with new system to run in own window, or to run in, up to, every window..
		##
		_object_details_runtime_data			= '';
		## _object_details_runtime_data			+= '\n\n';
		## _object_details_runtime_data			+= '\n◙◙◙ RunTime Data ◙◙◙';
		_object_details_runtime_data			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'RunTime[ 0 ] -> source_ext:',						_col_data, self.GetRunTimeSetting( 'source_ext', 'Not Set' ) ) + '\n';
		_object_details_runtime_data			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'RunTime[ 0 ] -> source_filename:',					_col_data, self.GetRunTimeSetting( 'source_filename', 'Not Set' ) ) + '\n';
		_object_details_runtime_data			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'RunTime[ 0 ] -> source_path:',						_col_data, self.GetRunTimeSetting( 'source_path', 'Not Set' ) ) + '\n';
		_object_details_runtime_data			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'RunTime[ 0 ] -> source_syntax:',					_col_data, self.GetRunTimeSetting( 'source_syntax', 'Not Set' ) ) + '\n';
		_object_details_runtime_data			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'RunTime[ 0 ] -> source_syntax_ext:',				_col_data, self.GetRunTimeSetting( 'source_syntax_ext', 'Not Set' ) ) + '\n';
		_object_details_runtime_data			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'RunTime[ 0 ] -> source_syntax_path:',				_col_data, self.GetRunTimeSetting( 'source_syntax_path', 'Not Set' ) ) + '\n';


		##
		## Configuration Data
		##
		_object_details_config_data				= '';
		## _object_details_config_data				+= '\n\n';
		## _object_details_config_data				+= '\n◙◙◙ Important / Misc Configuration Data ◙◙◙';
		_object_details_config_data				+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Panel -> output_sorting_method:',					_col_data, self.GetSetting( 'output_sorting_method', 'None' ) ) + '\n';
		_object_details_config_data				+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Panel -> output_children_sorting_method:',			_col_data, self.GetSetting( 'output_children_sorting_method', 'None' ) ) + '\n';
		_object_details_config_data				+= '\n';
		_object_details_config_data				+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Lang -> function_name_search:',						_col_data, self.GetSetting( 'function_name_search', 'None' ) ) + '\n';
		_object_details_config_data				+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Lang -> function_name_replace:',					_col_data, self.GetSetting( 'function_name_replace', 'None' ) ) + '\n';
		## _object_details_config_data				+= '\n'; + '\n'


		##
		## Configuration Data
		## Task: Create dynamic method of listing all file extensions - worst case, maybe just have a table of 'supported' extensions ( actually, languages would be better, and then those languages would be linked to file-extensions )
		##
		_object_details_file_ext_config			= '';
		## _object_details_file_ext_config			+= '\n\n';
		## _object_details_file_ext_config			+= '\n◙◙◙ Map File-Extensions to Language / Project Table Configs ◙◙◙';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ACMS_Default_Ext:',							_col_data, self.GetSetting( 'acms_language_map->ACMS_Default_Ext', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> asp:',										_col_data, self.GetSetting( 'acms_language_map->asp', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ahk:',										_col_data, self.GetSetting( 'acms_language_map->ahk', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> bat:',										_col_data, self.GetSetting( 'acms_language_map->bat', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> c:',											_col_data, self.GetSetting( 'acms_language_map->c', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> cs:',										_col_data, self.GetSetting( 'acms_language_map->cs', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> css:',										_col_data, self.GetSetting( 'acms_language_map->css', 'None' ) ) + '\n';

		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> cpp:',										_col_data, self.GetSetting( 'acms_language_map->cpp', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> h:',											_col_data, self.GetSetting( 'acms_language_map->h', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> htm:',										_col_data, self.GetSetting( 'acms_language_map->htm', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> html:',										_col_data, self.GetSetting( 'acms_language_map->html', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> java:',										_col_data, self.GetSetting( 'acms_language_map->java', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> js:',										_col_data, self.GetSetting( 'acms_language_map->js', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> json:',										_col_data, self.GetSetting( 'acms_language_map->json', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> sublime-keymap:',							_col_data, self.GetSetting( 'acms_language_map->sublime-keymap', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> sublime-mousemap:',							_col_data, self.GetSetting( 'acms_language_map->sublime-mousemap', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> sublime-settings:',							_col_data, self.GetSetting( 'acms_language_map->sublime-settings', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> sublime-syntax:',							_col_data, self.GetSetting( 'acms_language_map->sublime-syntax', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> tmLanguage:',								_col_data, self.GetSetting( 'acms_language_map->tmLanguage', 'None' ) ) + '\n';

		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> lua:',										_col_data, self.GetSetting( 'acms_language_map->lua', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> md:',										_col_data, self.GetSetting( 'acms_language_map->md', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> php:',										_col_data, self.GetSetting( 'acms_language_map->php', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> py:',										_col_data, self.GetSetting( 'acms_language_map->py', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> sqf:',										_col_data, self.GetSetting( 'acms_language_map->sqf', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> sql:',										_col_data, self.GetSetting( 'acms_language_map->sql', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> sqs:',										_col_data, self.GetSetting( 'acms_language_map->sqs', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ts:',										_col_data, self.GetSetting( 'acms_language_map->ts', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> txt:',										_col_data, self.GetSetting( 'acms_language_map->txt', 'None' ) ) + '\n';
		_object_details_file_ext_config			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> xml:',										_col_data, self.GetSetting( 'acms_language_map->xml', 'None' ) ) + '\n';

		## _object_details_file_ext_config			+= '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) ) + '\n';
		## _object_details_file_ext_config			+= '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) ) + '\n';
		## _object_details_file_ext_config			+= '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) ) + '\n';
		## _object_details_file_ext_config			+= '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) ) + '\n';
		## _object_details_file_ext_config			+= '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) ) + '\n';
		## _object_details_file_ext_config			+= '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) ) + '\n';
		## _object_details_file_ext_config			+= '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) ) + '\n';
		## _object_details_file_ext_config			+= '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) ) + '\n';
		## _object_details_file_ext_config			+= '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) ) + '\n';
		## _object_details_file_ext_config			+= '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) ) + '\n';
		## _object_details_file_ext_config			+= '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) ) + '\n';
		## _object_details_file_ext_config			+= '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) ) + '\n';
		## _object_details_file_ext_config			+= '\n';


		##
		## Mapping data
		##
		_object_details_mapping_data			= '';
		## _object_details_mapping_data			+= '\n\n';
		## _object_details_mapping_data			+= '\n◙◙◙ Mapping Data ◙◙◙';
		_object_details_mapping_data			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper -> language_name:',							_col_data, _active_language_name ) + '\n';
		_object_details_mapping_data			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper -> active_language:',							_col_data, _active_language ) + '\n';
		_object_details_mapping_data			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper -> active_mapper:',							_col_data, _active_mapper ) + '\n';
		_object_details_mapping_data			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper -> active_class:',								_col_data, str( _active_class ) ) + '\n';
		_object_details_mapping_data			+= '\n';
		_object_details_mapping_data			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper File Name:',									_col_data, _active_mapper_filename ) + '\n';
		_object_details_mapping_data			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper Class Name ( Most Probable ):',				_col_data, 'class ' + _class_name + '( ' + _active_class_parent + ' ):' ) + '\n';
		_object_details_mapping_data			+= '\t\tNote: This class / line of code should exist in the file. If it is missing, you will get an error and the source-file will not be mapped. The class parent can vary, but the name should be the same...\n';
		_object_details_mapping_data			+= '';

		## Error Output Notes
		_object_details_mapping_data			+= '\tClass Names - Any of these should appear in the file ( if "active_class" is set, you will see 3, otherwise only 1 )!\n';
		_object_details_mapping_data			+= '\t\tclass ' + _class_name + '( PARENT_CLASS_NAME ):\n';

		if ( _active_class != None ):
			_object_details_mapping_data		+= '\t\tclass ' + str( _class_name_a ) + '( PARENT_CLASS_NAME ):\n';
			_object_details_mapping_data		+= '\t\tclass ' + str( _class_name_b ) + '( PARENT_CLASS_NAME ):\n';

		_object_details_mapping_data			+= '\n';
		_object_details_mapping_data			+= '\t\tNote: PARENT_CLASS_NAME will most likely be one of the following without quotes - I only use it as a placeholder because of how widely the data can vary:\n';
		_object_details_mapping_data			+= '\t\tNote: Look at the _template_*.py files to see how Default, User and other Project Files should look new or with data!\n';
		_object_details_mapping_data			+= '\n';
		_object_details_mapping_data			+= '\t\t' + AcecoolLib.String.FormatColumnStripR( _col_name, 'XCodeMapperBase',									_col_data, '-- Used in most cases when the class name ends with "_Default"' ) + '\n';
		_object_details_mapping_data			+= '\t\t' + AcecoolLib.String.FormatColumnStripR( _col_name, 'ACMS_' + ACMS( ).GetActiveLanguage( ) + '_Default',	_col_data, '-- Typically used for the same language when the class name does not end with "_Default"' ) + '\n';
		_object_details_mapping_data			+= '\t\t' + AcecoolLib.String.FormatColumnStripR( _col_name, 'ACMS_ParentLanguageName_User',						_col_data, '-- Example: class ACMS_GMod_Lua_Default( ACMS_Lua_User ):' ) + '\n';
		_object_details_mapping_data			+= '\t\t' + AcecoolLib.String.FormatColumnStripR( _col_name, 'ACMS_ParentLanguageName_Default',					_col_data, '-- Example: class ACMS_GMod_Lua_Default( ACMS_Lua_Default ):' ) + '\n';

		_object_details_mapping_data			+= '\n';

		_object_details_mapping_data			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper File Name Using <ext>.py Method:',			_col_data, _source_ext + '.py' ) + '\n';
		_object_details_mapping_data			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper Class ( OLD ):',								_col_data, 'function generate( ... ): is executed!' ) + '\n';
		_object_details_mapping_data			+= '\t\tNote: The generate function returns a class reference which should contain the same functions as the new system, ie .Run is executed.\n';
		_object_details_mapping_data			+= '\t\tNote: The downside to the generate method is a lot of repeated code is necessary in ALL files which use it, and / or you will need to declare all possible classes in the file, and set which class to load in that file making confiuration tedious.\n';

		## If config option is true, then add additional folder to output for what it looks for...
		if ( self.GetSetting( 'enable_codemap_mappers', False ) ):
			_object_details_mapping_data		+= '\n';
			_object_details_mapping_data		+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper FileName CodeMap:',						_col_data, '*/User/CodeMap/custom_mappers/' + _source_ext + '.py' ) + '\n';


		##
		## Plugin Paths
		##
		_object_details_plugin_paths			= '';
		## _object_details_plugin_paths			+= '\n';
		## _object_details_plugin_paths			+= '\n◙◙◙ Plugin Paths ◙◙◙';
		_object_details_plugin_paths			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Path -> Base ( * ):',								_col_data, sublime.packages_path( ) ) + '\n';
		_object_details_plugin_paths			+= '\t\tNote: This path is represented as * everywhere in this output.\n';
		_object_details_plugin_paths			+= '\t\tNote: When multiple directories are checked for information, the User folder ALWAYS comes first.\n';
		_object_details_plugin_paths			+= '\n';
		_object_details_plugin_paths			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Path -> Plugin Base:',								_col_data, '*/AcecoolCodeMappingSystem/' ) + '\n';
		_object_details_plugin_paths			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Path -> Plugin User:',								_col_data, '*/User/AcecoolCodeMappingSystem/' ) + '\n';
		_object_details_plugin_paths			+= '\n';
		_object_details_plugin_paths			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Path -> Default Mappers:',							_col_data, '*/AcecoolCodeMappingSystem/maps/' ) + '\n';
		_object_details_plugin_paths			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Path -> User Mappers:',								_col_data, '*/User/AcecoolCodeMappingSystem/maps/' ) + '\n';
		_object_details_plugin_paths			+= '\n';
		_object_details_plugin_paths			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Path -> Default Configuration Files:',				_col_data, '*/AcecoolCodeMappingSystem/ACMS_*.sublime-settings' ) + '\n';
		_object_details_plugin_paths			+= '\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Path -> User Configuration Files:',					_col_data, '*/User/ACMS_*.sublime-settings' ) + '\n';
		_object_details_plugin_paths			+= '\n';


		##
		## Window / View Data
		## Task: Add a system so every single window / panel which is opened is listed here...
		##
		_object_details_window_view_data		= '';
		## _object_details_window_view_data		+= '\n\n';
		## _object_details_window_view_data		+= '\n◙◙◙ Window / View Data ◙◙◙';
		_object_details_window_view_data		+= '\t\tNote: For-Each Window Open, this data will show Window information, Source and Output ( if apoplicable ) View Information\n';

		if ( _window != None ):
			_object_details_window_view_data	+= self.SublimeWindowToString( _window );

		_object_details_window_view_data		+= self.SublimeViewToString( 'Source', _view_source );
		_object_details_window_view_data		+= self.SublimeViewToString( 'Output', _view_output );

		##
		PLUGIN_OBJECT_DETAILS_PLUGIN_DATA, PLUGIN_OBJECT_DETAILS_RUNTIME_DATA, PLUGIN_OBJECT_DETAILS_CONFIG_DATA, PLUGIN_OBJECT_DETAILS_FILE_EXT_CONFIG, PLUGIN_OBJECT_DETAILS_MAPPING_DATA, PLUGIN_OBJECT_DETAILS_PLUGIN_PATHS, PLUGIN_OBJECT_DETAILS_WINDOW_VIEW_DATA, ENUM_LIST_PLUGIN_OBJECT_DETAILS, MAP_PLUGIN_OBJECT_DETAILS_NAMES, MAPR_PLUGIN_OBJECT_DETAILS_NAMES, MAP_PLUGIN_OBJECT_DETAILS, MAPR_PLUGIN_OBJECT_DETAILS = AcecoolLib.ENUM( None, [ 'Plugin Data', 'RunTime Data', 'Important / Misc Configuration Data', 'Map File-Extensions to Language / Project Table Configs', 'Mapping Data', 'Plugin Paths', 'Window / View Data' ], [ _object_details_plugin_data, _object_details_runtime_data, _object_details_config_data, _object_details_file_ext_config, _object_details_mapping_data, _object_details_plugin_paths, _object_details_window_view_data ] );

		## Grab the ordered categories list
		_ordered_object_data = self.GetSetting( 'output_plugin_details_order', ( ) );

		##
		_text = AcecoolLib.Text.FormatPipedHeader( 'Acecool Code Mapping System Plugin Object ToString', '◙◙' )
		## _text = '◙◙ Acecool Code Mapping System Plugin Object ToString ◙◙';
		## _text = AcecoolLib.String.FormatColumn( len( _text ) - 2, '◙◙' ) + '◙◙\n' + _text + '\n' +AcecoolLib.String.FormatColumn( len( _text ) - 2, '◙◙' )  + '◙◙\n'
		## '◙◙\n◙◙  ◙◙\n◙◙\n';

		## For each category in our output_order list - compile the data...
		## for _category, _category_str, _category_name, _category_desc, in self.output_order:
		for _object_detail_enum in _ordered_object_data:
			##
			_detail		= self.GetVariableValueByName( _object_detail_enum, None, -1, locals( ) );
			_name		= MAP_PLUGIN_OBJECT_DETAILS_NAMES[ _detail ];
			_data		= MAP_PLUGIN_OBJECT_DETAILS[ _detail ];

			##
			_text += '\n\n';
			## _text += '◙◙◙ ' + _name + ' ◙◙◙\n';
			_text += AcecoolLib.Text.FormatPipedHeader( _name, '◙◙◙' )
			_text += _data + '';

		## 	_text += '\n\n -- Debugging Plugin Object ToString Output Data --\n'
		## 	_text += 'Name: ' + _name + '\n'
		## 	_text += 'ENUM: ' + _object_detail_enum + ' == ' + str( _detail ) + '\n'
		## 	## _text += 'ENUM: ' + _object_detail_enum + '\n'



		## _text += 'PLUGIN_OBJECT_DETAILS_PLUGIN_DATA: ' + str( PLUGIN_OBJECT_DETAILS_PLUGIN_DATA ) + '\n'
		## _text += 'PLUGIN_OBJECT_DETAILS_RUNTIME_DATA: ' + str( PLUGIN_OBJECT_DETAILS_RUNTIME_DATA ) + '\n'
		## _text += 'PLUGIN_OBJECT_DETAILS_CONFIG_DATA: ' + str( PLUGIN_OBJECT_DETAILS_CONFIG_DATA ) + '\n'
		## _text += 'PLUGIN_OBJECT_DETAILS_FILE_EXT_CONFIG: ' + str( PLUGIN_OBJECT_DETAILS_FILE_EXT_CONFIG ) + '\n'
		## _text += 'PLUGIN_OBJECT_DETAILS_MAPPING_DATA: ' + str( PLUGIN_OBJECT_DETAILS_MAPPING_DATA ) + '\n'
		## _text += 'PLUGIN_OBJECT_DETAILS_PLUGIN_PATHS: ' + str( PLUGIN_OBJECT_DETAILS_PLUGIN_PATHS ) + '\n'
		## _text += 'PLUGIN_OBJECT_DETAILS_WINDOW_VIEW_DATA: ' + str( PLUGIN_OBJECT_DETAILS_WINDOW_VIEW_DATA ) + '\n'
		## ## _text += 'MAP_PLUGIN_OBJECT_DETAILS_NAMES: ' + str( MAP_PLUGIN_OBJECT_DETAILS_NAMES ) + '\n'
		## ## _text += 'MAP_PLUGIN_OBJECT_DETAILS: ' + str( MAP_PLUGIN_OBJECT_DETAILS ) + '\n'










		## ##
		## ## Plugin Base Data
		## ##
		## if ( self.GetSetting( 'plugin_details_show_plugin_data', True ) ):
		## 	_text += '\n'
		## 	_text += '\n◙◙◙ Plugin Data ◙◙◙'
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Definitions -> plugin_name:',							_col_data, self.GetSetting( 'plugin_name', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Definitions -> plugin_author:',						_col_data, self.GetSetting( 'plugin_author', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Definitions -> plugin_company:',						_col_data, self.GetSetting( 'plugin_company', 'None' ) )
		## 	_text += '\n'

		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Plugin Validity:',									_col_data, _is_plugin_valid )

		## 	## Task: When it is possible to open more than one panel, or link a panel in a different window - ad the number of panels opened here...
		## 	if ( _plugin_valid ):
		## 		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Plugin Panel Open-State:',						_col_data, _is_plugin_open )

		## 	##
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Plugin Run-Mode:',									_col_data, 'Run-In Window Where Used [ vs Run in Own Window ]' )
		## 	_text += '\n\t\tNote: This shows you how the plugin is set to operate... It can run in its own window mapping any file which is focused, or, by default, run in the window where it is used.'
		## 	_text += '\n\t\tNote: This will be included in a future update - a system to allow the plugin to run in its own window, or to run in as many windows as you want. If you run in a window with files, it won\'t map any other file..'


		## ##
		## ## Configuration Data
		## ## Task: Update RunTime Config output and stored data so it conincides with new system to run in own window, or to run in, up to, every window..
		## ##
		## if ( self.GetSetting( 'plugin_details_show_runtime_data', True ) ):
		## 	_text += '\n\n'
		## 	_text += '\n◙◙◙ RunTime Data ◙◙◙'
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'RunTime[ 0 ] -> source_ext:',						_col_data, self.GetRunTimeSetting( 'source_ext', 'Not Set' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'RunTime[ 0 ] -> source_filename:',					_col_data, self.GetRunTimeSetting( 'source_filename', 'Not Set' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'RunTime[ 0 ] -> source_path:',						_col_data, self.GetRunTimeSetting( 'source_path', 'Not Set' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'RunTime[ 0 ] -> source_syntax:',						_col_data, self.GetRunTimeSetting( 'source_syntax', 'Not Set' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'RunTime[ 0 ] -> source_syntax_ext:',					_col_data, self.GetRunTimeSetting( 'source_syntax_ext', 'Not Set' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'RunTime[ 0 ] -> source_syntax_path:',				_col_data, self.GetRunTimeSetting( 'source_syntax_path', 'Not Set' ) )


		## ##
		## ## Configuration Data
		## ##
		## if ( self.GetSetting( 'plugin_details_show_config_data', True ) ):
		## 	_text += '\n\n'
		## 	_text += '\n◙◙◙ Important / Misc Configuration Data ◙◙◙'
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Panel -> output_sorting_method:',					_col_data, self.GetSetting( 'output_sorting_method', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Panel -> output_children_sorting_method:',			_col_data, self.GetSetting( 'output_children_sorting_method', 'None' ) )
		## 	_text += '\n'
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Lang -> function_name_search:',						_col_data, self.GetSetting( 'function_name_search', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Lang -> function_name_replace:',						_col_data, self.GetSetting( 'function_name_replace', 'None' ) )
		## 	## _text += '\n'


		## ##
		## ## Configuration Data
		## ##
		## if ( self.GetSetting( 'plugin_details_show_file_ext_config', True ) ):
		## 	_text += '\n\n'
		## 	_text += '\n◙◙◙ Map File-Extensions to Language / Project Table Configs ◙◙◙'
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ACMS_Default_Ext:',							_col_data, self.GetSetting( 'acms_language_map->ACMS_Default_Ext', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> asp:',										_col_data, self.GetSetting( 'acms_language_map->asp', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ahk:',										_col_data, self.GetSetting( 'acms_language_map->ahk', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> bat:',										_col_data, self.GetSetting( 'acms_language_map->bat', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> c:',											_col_data, self.GetSetting( 'acms_language_map->c', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> cs:',											_col_data, self.GetSetting( 'acms_language_map->cs', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> css:',										_col_data, self.GetSetting( 'acms_language_map->css', 'None' ) )

		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> cpp:',										_col_data, self.GetSetting( 'acms_language_map->cpp', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> h:',											_col_data, self.GetSetting( 'acms_language_map->h', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> htm:',										_col_data, self.GetSetting( 'acms_language_map->htm', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> html:',										_col_data, self.GetSetting( 'acms_language_map->html', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> java:',										_col_data, self.GetSetting( 'acms_language_map->java', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> js:',											_col_data, self.GetSetting( 'acms_language_map->js', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> json:',										_col_data, self.GetSetting( 'acms_language_map->json', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> sublime-keymap:',								_col_data, self.GetSetting( 'acms_language_map->sublime-keymap', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> sublime-mousemap:',							_col_data, self.GetSetting( 'acms_language_map->sublime-mousemap', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> sublime-settings:',							_col_data, self.GetSetting( 'acms_language_map->sublime-settings', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> sublime-syntax:',								_col_data, self.GetSetting( 'acms_language_map->sublime-syntax', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> tmLanguage:',									_col_data, self.GetSetting( 'acms_language_map->tmLanguage', 'None' ) )

		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> lua:',										_col_data, self.GetSetting( 'acms_language_map->lua', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> md:',											_col_data, self.GetSetting( 'acms_language_map->md', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> php:',										_col_data, self.GetSetting( 'acms_language_map->php', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> py:',											_col_data, self.GetSetting( 'acms_language_map->py', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> sqf:',										_col_data, self.GetSetting( 'acms_language_map->sqf', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> sql:',										_col_data, self.GetSetting( 'acms_language_map->sql', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> sqs:',										_col_data, self.GetSetting( 'acms_language_map->sqs', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ts:',											_col_data, self.GetSetting( 'acms_language_map->ts', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> txt:',										_col_data, self.GetSetting( 'acms_language_map->txt', 'None' ) )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> xml:',										_col_data, self.GetSetting( 'acms_language_map->xml', 'None' ) )

		## 	## _text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) )
		## 	## _text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) )
		## 	## _text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) )
		## 	## _text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) )
		## 	## _text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) )
		## 	## _text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) )
		## 	## _text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) )
		## 	## _text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) )
		## 	## _text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) )
		## 	## _text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) )
		## 	## _text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) )
		## 	## _text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Map -> ext:',										_col_data, self.GetSetting( 'acms_language_map->ext', 'None' ) )
		## 	## _text += '\n'


		## ##
		## ## Mapping data
		## ##
		## if ( self.GetSetting( 'plugin_details_show_mapping_data', True ) ):
		## 	_text += '\n\n'
		## 	_text += '\n◙◙◙ Mapping Data ◙◙◙'
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper -> language_name:',							_col_data, _active_language_name )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper -> active_language:',							_col_data, _active_language )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper -> active_mapper:',							_col_data, _active_mapper )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper -> active_class:',							_col_data, str( _active_class ) )
		## 	_text += '\n'
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper File Name:',									_col_data, _active_mapper_filename )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper Class Name ( Most Probable ):',				_col_data, 'class ' + _class_name + '( ' + _active_class_parent + ' ):' )
		## 	_text += '\n\t\tNote: This class / line of code should exist in the file. If it is missing, you will get an error and the source-file will not be mapped. The class parent can vary, but the name should be the same...'
		## 	_text += '\n'


		## 	## Error Output Notes
		## 	_text += '\n\tClass Names - Any of these should appear in the file ( if "active_class" is set, you will see 3, otherwise only 1 )!'
		## 	_text += '\n\t\tclass ' + _class_name + '( PARENT_CLASS_NAME ):'

		## 	if ( _active_class != None ):
		## 		_text += '\n\t\tclass ' + str( _class_name_a ) + '( PARENT_CLASS_NAME ):'
		## 		_text += '\n\t\tclass ' + str( _class_name_b ) + '( PARENT_CLASS_NAME ):'

		## 	_text += '\n'
		## 	_text += '\n\t\tNote: PARENT_CLASS_NAME will most likely be one of the following without quotes - I only use it as a placeholder because of how widely the data can vary:'
		## 	_text += '\n\t\tNote: Look at the _template_*.py files to see how Default, User and other Project Files should look new or with data!'
		## 	_text += '\n'
		## 	_text += '\n\t\t' + AcecoolLib.String.FormatColumnStripR( _col_name, 'XCodeMapperBase',									_col_data, '-- Used in most cases when the class name ends with "_Default"' )
		## 	_text += '\n\t\t' + AcecoolLib.String.FormatColumnStripR( _col_name, 'ACMS_' + ACMS( ).GetActiveLanguage( ) + '_Default',	_col_data, '-- Typically used for the same language when the class name does not end with "_Default"' )
		## 	_text += '\n\t\t' + AcecoolLib.String.FormatColumnStripR( _col_name, 'ACMS_ParentLanguageName_User',						_col_data, '-- Example: class ACMS_GMod_Lua_Default( ACMS_Lua_User ):' )
		## 	_text += '\n\t\t' + AcecoolLib.String.FormatColumnStripR( _col_name, 'ACMS_ParentLanguageName_Default',					_col_data, '-- Example: class ACMS_GMod_Lua_Default( ACMS_Lua_Default ):' )

		## 	_text += '\n'

		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper File Name Using <ext>.py Method:',			_col_data, _source_ext + '.py' )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper Class ( OLD ):',								_col_data, 'function generate( ... ): is executed!' )
		## 	_text += '\n\t\tNote: The generate function returns a class reference which should contain the same functions as the new system, ie .Run is executed.'
		## 	_text += '\n\t\tNote: The downside to the generate method is a lot of repeated code is necessary in ALL files which use it, and / or you will need to declare all possible classes in the file, and set which class to load in that file making confiuration tedious.'


		## 	## If config option is true, then add additional folder to output for what it looks for...
		## 	if ( self.GetSetting( 'enable_codemap_mappers', False ) ):
		## 		_text += '\n'
		## 		_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Mapper FileName CodeMap:',						_col_data, '*/User/CodeMap/custom_mappers/' + _source_ext + '.py' )


		## ##
		## ## Plugin Paths
		## ##
		## if ( self.GetSetting( 'plugin_details_show_plugin_paths', True ) ):
		## 	_text += '\n'
		## 	_text += '\n◙◙◙ Plugin Paths ◙◙◙'
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Path -> Base ( * ):',								_col_data, sublime.packages_path( ) )
		## 	_text += '\n\t\tNote: This path is represented as * everywhere in this output.'
		## 	_text += '\n\t\tNote: When multiple directories are checked for information, the User folder ALWAYS comes first.'
		## 	_text += '\n'
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Path -> Plugin Base:',								_col_data, '*/AcecoolCodeMappingSystem/' )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Path -> Plugin User:',								_col_data, '*/User/AcecoolCodeMappingSystem/' )
		## 	_text += '\n'
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Path -> Default Mappers:',							_col_data, '*/AcecoolCodeMappingSystem/maps/' )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Path -> User Mappers:',								_col_data, '*/User/AcecoolCodeMappingSystem/maps/' )
		## 	_text += '\n'
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Path -> Default Configuration Files:',				_col_data, '*/AcecoolCodeMappingSystem/ACMS_*.sublime-settings' )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumnStripR( _col_name, 'Path -> User Configuration Files:',					_col_data, '*/User/ACMS_*.sublime-settings' )
		## 	_text += '\n'


		## ##
		## ## Window / View Data
		## ## Task: Add a system so every single window / panel which is opened is listed here...
		## ##
		## if ( self.GetSetting( 'plugin_details_show_window_view_data', True ) ):
		## 	_text += '\n\n'
		## 	_text += '\n◙◙◙ Window / View Data ◙◙◙'
		## 	_text += '\n\t\tNote: For-Each Window Open, this data will show Window information, Source and Output ( if apoplicable ) View Information\n'

		## 	if ( _window != None ):
		## 		_text += self.SublimeWindowToString( _window )

		## 	_text += self.SublimeViewToString( 'Source', _view_source )
		## 	_text += self.SublimeViewToString( 'Output', _view_output )


		## _text += '\n\tNote: The following Mapper entries follow a specific order... * can be Packages/ or Packages/User/ - but it is always User/ first for both before looping back for non User/ ... before default..'
		## '*/AcecoolCodeMappingSystem/maps/' +

		## _text += '\n\t\t• Note: '


		## _text += '\n\n'

		## ##
		## _text += '\n\t• ' + AcecoolLib.String.FormatColumn( _col_name, 'Mapper -> active_class:',						_col_data, _active_class )
		## _text += '\n\t\tNote: If active_class is set, then there are 2 other possibile class-names which will be checked for in the file.'
		## _text += '\n'

		## ##
		## if ( _active_class != None ):
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumn( _col_name, 'Mapper Class Option using active_class:',	_col_data, 'class ' + _class_name_a + '( XCodeMapperBase ):' )
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumn( _col_name, 'Mapper Class Option using active_class:',	_col_data, 'class ' + _class_name_b + '( XCodeMapperBase ):' )
		## 	_text += '\n'
		## 	_text += '\n\t• ' + AcecoolLib.String.FormatColumn( _col_name, 'Mapper active_class ( NEW Alt ):',					_col_data, 'class ACMS_' + self.GetMapperFileName( ) + '( XCodeMapperBase ):' )
		## 	_text += '\n\t\tNote: The Alternative class-name set using "active_class" should be the <active_mapper> portion of the name, or the complete name of the class.'
		## 	_text += '\n\t\tNote: If active_class is set, the NEW classname will still be checked, but only AFTER checking 2 Alt possible class names. IE setting it to Blah will load: class ACMS_ActiveLanguage_Blah( PARENT ): OR class Blah( PARENT ):'
		## 	_text += '\n'
		## _text += '\n'

		## _text += self.GetAccessorFuncSupportObjectToString( )
		## _text += '\n'


		##
		## Mapping data
		##
		## _text += '\n\n'
		## _text += '\n◙◙◙ Mapping Class Data ◙◙◙'




		## 		## _mapper_classname = ACMS( ).GetMapperClassName( )
		## 		( _mapper_classname, _mapper_classname_a, _mapper_classname_b ) = ACMS( ).GetMapperClassNamesList( )
		## 		_mapper_filename	= ACMS( ).GetMapperFileName( )
		## 		_active_class		= ACMS( ).GetActiveClass( )
		## 		## _mapper_chosen		= 'old <Extension>.py'
		## 		## _mapper_method		= 'generate( ... )'
		## 		## _mapper_error		= 'Errors and Quick Resolutions List:\n\n'
		## 		## 	## _mapper_error += '\t[ ACMS ] new mapper: EXISTS - Using new <Language>_<Mapper>.py method!\n'
		## 		## 	_output += '\tMapper File Name Mode: <active_language>_<active_mapper>.py - _<active_mapper> is only added if active_mapper is NOT "Default"\n'
		## 		## 	_output += '\tMapper File Name: maps/' + _mapper_filename + '.py\n'
		## 		## 	_output += '\t\tNote: FILE EXISTS! The most likely cause of THIS error is invalid class-name in the file...\n\n'

		## 		_output += '''------------------------------------------------------------------------------------------------------------------------
		## The following provides the shortest method of debugging why you're seeing this error. If you proceed beyond
		## this segment, you'll see a LOT of information which may help, but it outlines a lot of the back-end system and how choices are made.

		## Quick Debugging Steps
		## ---------------------
		## 1 ) Make sure the file exists - if the Errors list above says the file exists, then move on to step 2 or verify the file exists.
		## '''

		## 		_output += '\tThe file will either follow the new <active_language>_<active_mapper>.py / <active_language>.py OR old <ext>.py file-naming method\n'
		## 		_output += '\t\tNew:\t' + ACMS( ).GetMapperFileName( ) + '.py\n'
		## 		_output += '\t\tOld:\t' + ACMS( ).GetSourceFileExt( ) + '.py\n\n'

		## 		_output += '\tThe file will exist in either Packages/AcecoolCodeMappingSystem/maps/ OR Packages/User/AcecoolCodeMappingSystem/maps/\n\n'

		## 		_output += '''2 ) Make sure the class exists within the function - even in the Code Map and Old <ext>.py method of loading files, class-name is always looked at first...

		## '''



		## 		## Error Output Notes
		## 		_output += '\tClass Names - Any of these should appear in the file ( if "active_class" is set, you will see 3, otherwise only 1 )!\n'
		## 		_output += '\t\tclass ' + _mapper_classname + '( PARENT_CLASS_NAME ):\n'

		## 		if ( _active_class != None ):
		## 			_output += '\t\tclass ' + str( _mapper_classname_a ) + '( PARENT_CLASS_NAME ):\n'
		## 			_output += '\t\tclass ' + str( _mapper_classname_b ) + '( PARENT_CLASS_NAME ):\n'

		## 		_output += '\n'
		## 		_output += '\t\t\tNote: PARENT_CLASS_NAME will most likely be one of the following without quotes - I only use it because of how widely it can vary:'
		## 		_output += '\n\t\t\t\tXCodeMapperBase\t\t\t\t\t\t\t\t\t\t\t-- Used in most cases when the class name ends with "_Default"'
		## 		_output += '\n\t\t\t\tACMS_' + ACMS( ).GetActiveLanguage( ) + '_Default\t\t\t\t\t\t\t\t\t-- Typically used for the same language when the class name does not end with "_Default"'
		## 		_output += '\n\t\t\t\tACMS_ParentLanguageName_User\t\t\t\t-- Example: class ACMS_GMod_Lua_Default( ACMS_Lua_User ):'
		## 		_output += '\n\t\t\t\tACMS_ParentLanguageName_Default\t\t\t-- Example: class ACMS_GMod_Lua_Default( ACMS_Lua_Default ):'
		## 		_output += '\n'
		## 		_output += '\n'
		## 		_output += '\t\t\tNote: The most important part is after "class " and before "( PARENT_CLASS_NAME ):" - ie the class name!\n'
		## 		_output += '\t\t\tNote: If the mapper file is empty, look at a _template_User.py file to see how a new file should look!\n'
		## 		_output += '\t\t\tNote: If a class name exists in the file, but is not the same as any above, rename it!\n'
		## 		_output += '\t\t\tNote: For PARENT_CLASS_NAME use XCodeMapperBase or ACMS_LanguageName_Default if you\'re not sure what to put, or keep it the same as it was in the file!\n\n'
		## 		_output += '''3 ) If the file exists, and the correct class name / line exists in the file then the issue is likely with
		## 	configuration, OR an error in the file preventing it from loading... It could also be a different issue. The best way to determine
		## 	what type of issue it is, is to read through the

		## 	Acecool Code Mapping System Plugin Object ToString ---- data....

		## 	That data, included below, provides a lot of important information about the plugin, such as the window it is loaded in, configuration
		## 	keys and values, the active configuration values, the source-filename used for mapping the file, the output panel id, etc... It also contains
		## 	information on what file-name is loaded or looked for in the maps/ folder, and gives you an idea of class-names it tries to load. Some of that
		## 	information has been provided above so the debugging steps can be placed in one area... But the Object ToString will contain the most important
		## 	information about the plugin, and the configuration used...

		## '''



		## _text += '\n'
		## _text += '\n\t\t• ' + AcecoolLib.String.FormatColumn( _col_name, 'Window Layout Info:', _col_data, str( _layout_rows ) + ' Row' + TernaryFunc( _layout_rows == 0 or _layout_rows > 1, 's', '' ) + ' ' + TernaryFunc( _layout_rows > 3, '≡≡≡', TernaryFunc( _layout_rows > 2, '▄=◙=▀', TernaryFunc( _layout_rows > 1, '▄-▀', '░' ) ) ) + ', ' + str( _layout_cols ) + ' Column' + TernaryFunc( _layout_cols == 0 or _layout_cols > 1, 's', '' ) + ' ▒' + AcecoolString.repeat( '|▒', _layout_cols - 1 ) + ', ' + str( _layout_cells ) + ' Cell' + TernaryFunc( _layout_cells == 0 or _layout_cells > 1, 's', '' ) + ' ' + AcecoolString.repeat( '▒ ', _layout_cells ) )
		## _text += '\n\t\t• ' + AcecoolLib.String.FormatColumn( _col_name, 'Window Layout Raw:', _col_data, str( _layout ) )
		## _view_source_id								= _view_source.id( )
		## _view_source_group, _view_source_index		= _window.get_view_index( _view_source )
		## _view_output_group, _view_output_index		= _window.get_view_index( _view_output )
		## _text += '\n\t> ' + AcecoolLib.String.FormatColumn( _col_name, 'IsPanelOpen:', _col_data, str( self.IsPanelOpen( ) ) )
		## _text += '\n\t\t> ' + AcecoolLib.String.FormatColumn( _col_name, 'Output View:', _col_data, 'ID / Group :: ' + self.GetOutputViewIDToString( ) + ' / ' + self.GetOutputGroupIDToString( ) + ' :: ' + self.GetOutputViewToString( ) + ' - ' + str( type( self.GetOutputView( ) ) ) )
		## _text += '\n\t\t> ' + AcecoolLib.String.FormatColumn( _col_name, 'Source View:', _col_data, 'ID / Group :: ' + str( _view_source_id ) + ' / ' + str( self.GetSourceView( ).group_id( ) ) + ' :: ' + self.GetSourceViewToString( ) + ' - ' + str( type( self.GetSourceView( ) ) ) )
		## _text += '\n\t\t> ' + AcecoolLib.String.FormatColumn( _col_name, 'Cached Source File-Name:', _col_data, AcecoolFile.SafeName( self.GetRunTimeSetting( 'source_path', 'Not Set' ) ) )
		## _text += '\n\t\t> ' + AcecoolLib.String.FormatColumn( _col_name, 'active_language:', _col_data, self.GetSetting( 'active_language', 'None' ) )
		## _text += '\n\t\t> ' + AcecoolLib.String.FormatColumn( _col_name, 'active_mapper:', _col_data, self.GetSetting( 'active_mapper', 'None' ) )

		## Δ•
		## _text += _is_panel_open
		## _text += _is_panel_open
		## _text += '\n\t\t> ' + AcecoolLib.String.FormatColumn( _col_name, 'Source Syntax Path:', _col_data, self.GetSourceView( ).settings( ).get( 'syntax', 'None' )[ 9 : ] )
		## _text += '\n\t\t> ' + AcecoolLib.String.FormatColumn( _col_name, 'Source File:', _col_data, AcecoolSublime.GetViewFileName( self.GetSourceView( ) ) )
		## _text += '\n\t\t> ' + AcecoolLib.String.FormatColumn( _col_name, 'Output File:', _col_data, AcecoolSublime.GetViewFileName( self.GetOutputView( ) ) )
		## _text += '\n\t\t> ' + AcecoolLib.String.FormatColumn( _col_name, 'Output View ID:', _col_data, self.GetOutputViewIDToString( ) )
		## _text += '\n\t\t> ' + AcecoolLib.String.FormatColumn( _col_name, 'Output Group ID:', _col_data, self.GetOutputGroupIDToString( ) )


		## _text += '\n\t\t> ' + AcecoolLib.String.FormatColumn( _col_name, 'Source Syntax:', _col_data, self.GetRunTimeSetting( 'source_syntax', 'Not Set' ) )
		## _text += '\n\t\t> ' + AcecoolLib.String.FormatColumn( _col_name, 'GetSourceViewID:', _col_data, self.GetSourceViewIDToString( ) )
		## _text += '\n\t\t> ' + AcecoolLib.String.FormatColumn( _col_name, 'GetSourceGroupID:', _col_data, self.GetSourceGroupIDToString( ) )

		return _text


	##
	## Helper - This prints text to the console, and elsewhere with our prefix...
	##
	def Notify( self, _text ):
		## Print... If we're allowed to..
		if ( self.GetSetting( 'print_notifications' ) or self.GetSetting( 'debugging_mode' ) ):
			print( '\t> [ AcecoolCodeMappingSystem ]\t\t' + str( _text ) )



	## Helper - Updates the contents of the Mapping Panel File
	def UpdatePanelContents( self, _data = 'xxxxx', _syntax = '', _edit = None ):
		## Grab the panel view
		_view = self.GetOutputView( )

		## If the view doesn't exist, we can't update the contents...
		if ( not self.IsValidOutputPanel( _view ) ):
			return False

		## Alter read only state...
		AcecoolLib.Sublime.SetViewReadOnly( _view, False )

		## Create a Sublime Text Region containing the entire set of text from the panel...
		_region = AcecoolLib.Sublime.GetViewContentsRegion( _view )

		## Replace ALL of the text with our _data
		_view.replace( _edit, _region, _data )

		##
		AcecoolLib.Sublime.SetViewScratch( _view, True )

		## Update the syntax highlighter for the mapping panel...
		## AcecoolLib.Sublime.SetViewSyntaxFile( _view, _syntax )

		## Reset our read-only status...
		AcecoolLib.Sublime.SetViewReadOnly( _view, True )


	##
	##
	##
	def IsValidOutputPanel( self, _view ):
		return self.IsOutputPanel( _view )


	##
	##
	##
	def IsOutputPanel( self, _view ):
		##
		if ( AcecoolLib.Sublime.IsValidView( _view ) and AcecoolLib.Sublime.GetViewFileName( _view ) == self.GetPanelName( ) ):
			return True


		return False


	##
	## Helper - Detects the panel when the class is created...
	##
	def DetectPanel( self ):
		## Detect the ACMS Panel.
		_view = AcecoolLib.Sublime.FindView( self.GetPanelName( ) )
		if ( AcecoolLib.Sublime.IsValidView( _view ) and AcecoolLib.Sublime.GetViewFileName( _view ) == self.GetPanelName( ) ):
			self.ResetViewInfo( _view )
		else:
			self.ResetViewInfo( )

		## Detect the last Source View from the RunTime settings...
		_filename = sublime.load_settings( '' )


	##
	## Helper
	##
	def ResetViewInfo( self, _view = None, _source = False ):
		##
		_file = AcecoolLib.Sublime.GetViewFileName( _view )
		_path = AcecoolLib.Sublime.GetViewFileName( _view, True )

		##
		_source = False if ( _file == self.GetPanelName( ) ) else _source

		## The Type of view we're updated...
		_type = 'Output' if ( _source == False ) else 'Source'

		## self.Notify( 'ResetViewInfo -> ' + _type + '. -- View is: ' + str( _view ) )

		##
		if ( _view == None ):
			self.Notify( 'ResetViewInfo -> ' + _type + ' View Reset...' )
		else:
			self.Notify( 'ResetViewInfo -> ' + _type + ' View being set... ' + _path )

		## Grab the active window...
		_window = AcecoolLib.Sublime.GetWindow( )

		## If the view is valid - update the source / output view data of all relevant info - if the view isn't valid ( else ) then clear the stored data to prevent ovverwriting user files and the like...
		if ( AcecoolLib.Sublime.IsValidView( _view ) ):
			if ( _source == True ):
				## Update the Source View data - view, file path, filename, file extension
				self.UpdateSourceViewData( _view, _path, _file, AcecoolLib.Sublime.GetViewFileExt( _view ) )
			else:
				## Grab the group and index for the Output view
				_group, _index = _window.get_view_index( _view )

				## Update the Output View Data - view, view id, group id, and view index
				self.UpdateOutputViewData( _view, _view.id( ), _group, _index )
		else:
			if ( _source == True ):
				## Reset the Source View data - view, file path, filename, file extension
				self.UpdateSourceViewData( )
			else:
				## Reset the Output View Data - view, view id, index, group id
				self.UpdateOutputViewData( )


	##
	## Helper - Updates a group of AccessorFuncs with relevant data for the Source View - using no args resets all data to default / empty string
	##
	def UpdateSourceViewData( self, _view = None, _path = '', _filename = '', _ext = '' ):
		##
		_file = AcecoolLib.Sublime.GetViewFileName( _view )
		if ( _file == self.GetPanelName( ) ):
			print( 'UpdateSourceViewData -> _file == self.GetPanelName( ) :: ' + self.GetPanelName( ) )
			return False

		print( 'UpdateSourceViewData -> _file != self.GetPanelName( ) :: ' + self.GetPanelName( ) )
		## Update the internal AccessorFuncs
		self.SetAccessors( 'SourceView', _view, 'SourceFileName', _path, 'SourceFileExt', _ext )
		## self.SetSourceView( _view )
		## self.SetSourceFileName( _path )
		## self.SetSourceFileExt( _ext )

		## Update the RunTime configuration
		if ( _path != None and _path != '' and not _path.endswith( self.GetPanelName( ) ) ):
			self.SetRunTimeSetting( 'source_path', _path ) #, 'source_filename', _filename, 'source_ext', _ext )

		##
		if ( _filename != None and _filename != '' and _filename != self.GetPanelName( ) ):
			self.SetRunTimeSetting( 'source_filename', _filename )

		##
		if ( _ext != None and _ext != '' ):
			self.SetRunTimeSetting( 'source_ext', _ext )

		##
		if ( AcecoolLib.Sublime.IsValidView( _view ) ):
			( _syntax, _syntax_ext, _syntax_path ) = self.GetSourceViewSyntaxData( )
			self.SetAccessors( 'SourceViewFileSyntax', _syntax, 'SourceViewFileSyntaxExt', _syntax_ext, 'SourceViewFileSyntaxPath', _syntax_path )
			## self.SetSourceViewFileSyntax( _syntax )
			## self.SetSourceViewFileSyntaxExt( _syntax_ext )
			## self.SetSourceViewFileSyntaxPath( _syntax_path )

			##
			if ( _syntax != None and _syntax != '' ):
				self.SetRunTimeSetting( 'source_syntax', _syntax )

			##
			if ( _syntax_ext != None and _syntax_ext != '' ):
				self.SetRunTimeSetting( 'source_syntax_ext', _syntax_ext )

			##
			if ( _syntax_path != None and _syntax_path != '' ):
				self.SetRunTimeSetting( 'source_syntax_path', _syntax_path )


	##
	## Helper - Updates a group of AccessorFuncs with relevant data for the Output View - using no args resets all data to default / None
	##
	def UpdateOutputViewData( self, _view = None, _view_id = None, _view_group = None, _view_index = None ):
		##
		_file = AcecoolLib.Sublime.GetViewFileName( _view )
		if ( _file != self.GetPanelName( ) ):
			return False

		## Update the Output View Information - ID, Index, and Group ID...
		self.SetOutputView( _view )
		self.SetOutputViewID( _view_id )
		self.SetOutputViewIndex( _view_index )
		self.SetOutputGroupID( _view_group )


	##
	## Helper
	## Task: Add additional logic to make sure the panel is open in the correct context - if it is open and not in the right context, then close it... Correct context would be if set to its own window then make sure it is in its own window... If not set, then make sure it is in its own group / panel...
	##
	def IsPanelOpen( self ):
		##
		_view = self.GetOutputView( )
		if ( AcecoolLib.Sublime.IsValidView( _view ) and AcecoolLib.Sublime.GetViewFileName( _view ) == self.GetPanelName( ) ):
			return True

		return False


	##
	##
	##
	def ProcessSourceCodeMapping( self, _file, _force_nocache_remap = False, _preprocessor_only = False ):
		##
		self.Notify( 'Attempting to Execute plugin for file: ' + _file )

		## Grab the panel view
		_view = self.GetOutputView( )

		## If the view doesn't exist, we can't update the contents...
		if ( not AcecoolLib.Sublime.IsValidView( _view ) ):
			return False

		## Use a command so we get an Edit object...
		_view.run_command( 'acms_map_file', { 'file': _file, 'force_nocache_remap': _force_nocache_remap, 'preprocessor_only': _preprocessor_only } ) # , 'SourceView': self.GetSourceView( ), 'MappingPanel': _view

		## Reset Horizontal Scroll so we're looking at what matters
		AcecoolLib.Sublime.ResetViewHorizontalScroll( _view )


	##
	## Helper - Returns the Output View Settings
	##
	def GetOutputViewSettings( self ):
		return self.GetOutputView( ).settings( )


	##
	## Helper - Returns the Output View TabSize
	##
	def GetOutputViewTabSizeSetting( self ):
		return self.GetOutputViewSettings( ).get( 'tab_size', self.GetSetting( 'panel_tab_size', 4 ) )


	##
	##
	##
	def UpdateOutputPanelSettings( self ):
		## Helpers
		_view = self.GetOutputView( )
		_settings = _view.settings( )

		## Make sure the view is valid before continuing...
		if ( not self.IsValidOutputPanel( _view ) ):
			return False

		## default margin: 8
		_settings.set( 'margin', self.GetSetting( 'panel_margin', 8 ) )

		## allow custom font face/size, it's optional and it doesn't need to be in settings
		_settings.set( 'font_size', self.GetSetting( 'panel_font_size', 10.5 ) )

		## Set Font
		## if settings( ).has( 'codemap_font_face' ):
		_settings.set( 'font_face', self.GetSetting( 'panel_font_face', 'Consolas' ) )

		## Make sure word-wrap is disabled because it'd make the output look awful...
		_settings.set( 'word_wrap', self.GetSetting( 'panel_word_wrap', False ) )

		## We want the gutter enabled for folding - unless the user specifies otherwise...
		_settings.set( 'gutter', self.GetSetting( 'panel_gutter', True ) )

		## Hide white-space
		_settings.set( 'draw_white_space', self.GetSetting( 'panel_draw_white_space', 'none' ) )

		## Set whether to use tab indent or spaces...
		if ( self.GetSetting( 'panel_use_tab_indent_option', True ) ):
			_settings.set( 'translate_tabs_to_spaces', False )
			_settings.set( 'expand_tabs', False )
			_settings.set( 'unexpand_tabs', True )
		else:
			_settings.set( 'translate_tabs_to_spaces', True )
			_settings.set( 'unexpand_tabs', False )
			_settings.set( 'expand_tabs', True )

		## Set the Tab size to use...
		_settings.set( 'tab_size', self.GetSetting( 'panel_tab_size', 4 ) )




	##
	## Helper - Sets up the basic / default settings ( Note: The user settings get loaded at a different time based on each mapper - these are overall defaults )...
	##
	def ApplyOutputViewSettings( self, _view ):
		##
		_window			= AcecoolLib.Sublime.GetWindow( )

		if ( not self.IsValidOutputPanel( _view ) ):
			return False

		##
		_settings = _view.settings( )

		##
		self.UpdateOutputPanelSettings( )

		## HideTabs? Window Setting - great... I'll try scooting the column into the negatives to hide it to prevent un-natural closing out...
		## _window.set_tabs_visible( self.GetSetting( 'panel_draw_tabs', True ) )

		##
		## _window.set_view_index( _view, _group, 0 )

		## Clear the panel...
		_view.sel( ).clear( )

		## Reset our read-only status...
		AcecoolLib.Sublime.SetViewReadOnly( _view, True )





	##
	## Helper - Toggle Panel Visibility
	##
	def TogglePanel( self ):
		##
		## return TernaryFunc( ( self.IsPanelOpen( ) ), self.SetOutputViewID( self.HidePanel( ) ), self.SetOutputViewID( self.ShowPanel( ) )	)

		##
		if ( self.IsPanelOpen( ) == True ):
			return self.SetOutputViewID( self.HidePanel( ) )
		else:
			return self.SetOutputViewID( self.ShowPanel( ) )


	##
	## Helper - Show panel
	##
	def ShowPanel( self ):
		##
		## self.SetPanelOpen( True )
		_group = self.ProcessShowHidePanel( )

		## print( '\t\t> Calling Show Panel!' )
		## print( '\t\t\t> IsPanelOpen: ' + str( self.IsPanelOpen( ) ) )
		## print( '\t\t\t> GetOutputViewID: ' + str( _group ) )

		return _group


	##
	## Helper - Hide Panel
	##
	def HidePanel( self ):
		##
		## self.SetPanelOpen( False )
		self.ProcessShowHidePanel( False )

		## Debugging / Helpful information...
		## print( '\t\t> Calling Hide Panel!' )
		## print( '\t\t\t> IsPanelOpen: ' + str( self.IsPanelOpen( ) ) )
		## print( '\t\t\t> GetOutputViewID: ' + str( None ) )

		return None


	##
	## Helper - Processes Opening / Closing the Mapping Panel
	##
	def ProcessShowHidePanel( self, _show = True ):
		## Grab the window
		_window			= AcecoolLib.Sublime.GetWindow( )
		_count_groups	= AcecoolLib.Sublime.GetGroupCount( _window )

		## Grab the currently focused view so we can return to it when we close the view...
		_active_view = AcecoolLib.Sublime.GetActiveView( _window )

		## Grab the layout object / Table
		_layout			= _window.get_layout( )

		## print( 'Layout Before: ' + str( _layout ) )

		## Grab each individual element - Rows because we are going to allow splitting the right column into multiple at some point...
		_columns		= _layout[ 'cols' ]
		_cells			= _layout[ 'cells' ]
		_rows			= _layout[ 'rows' ]

		## Counts
		_count_columns	= len( _columns )
		_count_cells	= len( _cells )
		_count_rows		= len( _rows )

		##
		if ( _show == False and len( _cells ) < 2 ):
			return False

		## The width of the added panel compared to the entire window... If the width is greater than 1, assume they're using 25 == 0.25 for percent.. ie whole number percentages..
		_cfg_width = self.GetSetting( 'panel_width', 0.30 )
		_cfg_width = AcecoolLib.TernaryFunc( _cfg_width > 1, _cfg_width / 100, _cfg_width )
		_width			= 1 - _cfg_width

		## The new panel - We want it to be on the right and span all rows.. _map_cell = AcecoolLib.Sublime.CreateLayoutCell( _col_left, _col_right, _row_top, _row_bottom )
		_map_cell		= AcecoolLib.Sublime.CreateLayoutCell( _count_columns - 1, _count_columns, 0, _count_rows - 1 )

		## For each column, we subtract the total width the cell we're processing takes up relative to the new cell... to undo we divide...
		for _index, _column in enumerate( _columns ):
			## if _column > 0:
			if ( _show ):
				## If we're showing, then we need to offset the width of the added column with the currently opened panels so the new cell / column will fit...
				_columns[ _index ] = _column * _width
			else:
				## Now we're hiding, so we need to undo what we did - ie we need to ensure the cells / columns which will remain after we close the mapping panel are given the width from the mapping panel..
				_columns[ _index ] = _column / _width

		## If we're showing vs hiding the column...
		if ( _show ):
			## Oversize Default
			_cfg_width_oversize = 0.0

			## If we want to try to hide the minimap we'll oversize the panel...
			if ( self.GetSetting( 'panel_hide_minimap', False ) ):
				_cfg_width_oversize	= _cfg_width / 10

			## If we don't have overlay scrollbars, then we'll need to give another 5% width or so...
			if ( self.GetSublimeSetting( 'overlay_scroll_bars', False ) == False ):
				_cfg_width_oversize = _cfg_width_oversize + 0.05

			## If we want to oversize even further.
			_cfg_width_oversize += self.GetSetting( 'panel_oversize', 0 )

			## Add the column ( since it is added to the right, the number 1 needs to be added... )
			_columns.append( 1.0 + _cfg_width_oversize )

			## Add our _map_cell to the list of cells...
			_cells.append( _map_cell )
		else:
			## Delete the last column and cell...
			_columns.pop( )
			_cells.pop( )

			## Make sure the second to last is 1.0 - it should be from the loop, but just in case it's off... Can be done at any time...
			_columns[ - 1 ] = 1.0

		## Update the window to reflect the new layout...
		AcecoolLib.Sublime.RunCmdSetLayout( _layout, _window )

		##
		if ( _show ):
			_view = _window.open_file( self.GetPanelFileName( ) )

			self.SetOutputView( _view )
			self.SetOutputGroupID( _count_groups + 1 )
			self.ApplyOutputViewSettings( _view )
		else:
			## Grab the mapping view...
			_view = self.GetOutputView( )

			if ( self.IsValidOutputPanel( _view ) ):
				## Grab the group / index of the view - in case we want to close by index...
				_group, _index = _window.get_view_index( _view )

				## Close the mapping panel...
				if ( _group != None and _index != None and _view != None and AcecoolLib.Sublime.GetViewFileName( _view ) != None ):
					## Make sure we're focused on the mapping panel view before we execute close...
					_window.focus_view( _view )

					##
					_window.run_command( 'close' )

				## Refocus source-code / mapped view...
				AcecoolLib.Sublime.SetActiveView( _active_view, _window )

			##
			self.SetOutputGroupID( None )

		## print( 'Layout After: ' + str( _layout ) )

		## See if we can scoot the layout into the negatives - up a bit so we can hide the tabs... also for the minimap... if the settings are set... I need to find out the exact size of minimap though...
		## _tabs = _window.get_tabs_visible( )
		## _tabs = _window.is_sidebar_visible( )
		## _tabs = _window.is_menu_visible( )
		## _tabs = _window.is_minimap_visible( )
		## _tabs = _window.is_status_bar_visible( )
		## _tabs = _window.is_sidebar_visible( )

		## Return the number of groups + 1 so we have an ID to the new cell thereby avoiding an expensive search
		return _count_groups + 1








	##
	## Helper - Shortcuts looking confirming file existance and loading the module...
	##
	def LoadMapClass( self, _filename, _path ):
		## Short path removes C:\....\Sublime Text 3\Packages\AcecoolCodeMappingSystem\
		_short_path = _path[ len( sublime.packages_path( ) ) + 1 : ].replace( '\\', '/' );

		## If it doesn't exist, we'll try to extract... - The User/ folder doesn't exist in the package - so don't try to extract anything which starts with it...
		if ( not AcecoolLib.File.Exists( _path ) and not _short_path.startswith( 'User/' ) ):
			##
			_package = os.path.join( sublime.installed_packages_path( ), 'AcecoolCodeMappingSystem.sublime-package' );

			##
			if ( AcecoolLib.File.Exists( _package ) ):
				##
				_package_base = os.path.join( sublime.packages_path( ), 'AcecoolCodeMappingSystem' );

				## Convert the sublime-package path to an archive object so we can extract files...
				_archive = zipfile.ZipFile( _package );

				##
				_short_path = _short_path[ len( 'AcecoolCodeMappingSystem' ) + 1 : ];

				_archive.extract( _short_path, _package_base )


		## See if a it exists..
		if ( AcecoolLib.File.Exists( _path ) ):
			self.Notify( 'Using Mapper: ' + _filename );
			return SourceFileLoader( _filename, _path ).load_module( );








		## _short_path = 'Packages/AcecoolCodeMappingSystem/' + _short_path;
		## _res = None;

		## try:
		## 	_res = sublime.load_resource( _short_path );
		## except Exception as _error:
		## 	print( _error );
		## 	_res = None;

		## print( 'Trying short path instead: ' + _short_path );
		## if ( _res != None ):
		## 	self.Notify( 'Using Res:// Mapper: ' + _filename );

		## 	_res_loaded = _res.load_module( );
		## 	if ( _res_loaded != None ):
		## 		return _res_loaded;
		## 	else:
		## 		return _res;



		return None


	##
	## Helper - Processes looking for mappers of various configutions / types in different plugin folders User based or not...
	##
	def GetMapClass( self, _path ):
		##
		_filename = self.GetMapperFileName( )
		_ext = AcecoolLib.File.GetExt( _path )

		## Support new mapping class method in User Package Maps Folder -
		_acms_custom		= self.LoadMapClass( _filename, self.GetPackageUserMapsFolder( _filename + '.py' ) )
		if ( _acms_custom ):
			print( '[ ACMS ] Using Custom / User <Language>_<Type> Mapper: ' + self.GetPackageUserMapsFolder( _filename + '.py' ) )
			return _acms_custom

		## Support CodeMap ext.py .generate Mappers in User Package Maps Folder - Try to load a mapper using User\ACMS\maps\<ext>.py
		_ext_custom			= self.LoadMapClass( 'ACMS_Ext_Mapper_' + _ext, self.GetPackageUserMapsFolder( _ext + '.py' ) )
		if ( _ext_custom ):
			print( '[ ACMS ] Using Custom / User <Extension> Style Mapper: ' + self.GetPackageUserMapsFolder( _ext + '.py' ) )
			return _ext_custom

		## Support new mapping class method in Package Maps Folder -
		_acms_base			= self.LoadMapClass( _filename, self.GetPackageMapsFolder( _filename + '.py' ) )
		if ( _acms_base ):
			print( '[ ACMS ] Using Base / Default <Language>_<Type> Mapper: ' + self.GetPackageMapsFolder( _filename + '.py' ) )
			return _acms_base

		## Support CodeMap ext.py .generate Mappers in Package Maps Folder - Try to load a mapper using ACMS\maps\<ext>.py
		_ext_base			= self.LoadMapClass( 'ACMS_Ext_Mapper_' + _ext, self.GetPackageMapsFolder( _ext + '.py' ) )
		if ( _ext_base ):
			print( '[ ACMS ] Using Base / Default <Extension> Style Mapper: ' + self.GetPackageMapsFolder( _ext + '.py' ) )
			return _ext_base

		## Support CodeMap ext.py .generate Mappers for Easy Switching - Try to load a mapper using User\CodeMap\custom_mappers\<ext>.py
		if ( ACMS( ).GetSetting( 'enable_codemap_mappers', False ) ):
			_path_codemap		= self.LoadMapClass( 'ACMS_Ext_Mapper_' + _ext, self.GetCodeMapUserMapsFolder( _ext + '.py' ) )
			if ( _path_codemap ):
				print( '[ ACMS ] Using Custom / User <Extension> Style CodeMap Mapper: ' + _filename )
				return _path_codemap

		## Last Resort - New - Support new mapping class method in Package Maps Folder - Default_Default
		_acms_default_user		= self.LoadMapClass( 'Default_Default', self.GetPackageUserMapsFolder( '__default__.py' ) )
		if ( _acms_default_user ):
			print( '[ ACMS ] Using User / Default Customized Default <Language>_<Type> Mapper: ' + self.GetPackageUserMapsFolder( '__default__.py' ) )
			return _acms_default_user

		## Last Resort - New - Support new mapping class method in Package Maps Folder - Default_Default
		_acms_default_base		= self.LoadMapClass( 'Default_Default', self.GetPackageMapsFolder( '__default__.py' ) )
		if ( _acms_default_base ):
			print( '[ ACMS ] Using Base / Default Absolute Default <Language>_<Type> Mapper: ' + self.GetPackageMapsFolder( '__default__.py' ) )
			return _acms_default_base

		##
		raise( Exception( 'ACMS( ).GetMapClass( ) has no value return!' ) )


	##
	## self.GetMapperFileName( )
	##
	def GetMapperFileName( self ):
		##
		_active_language		= self.GetActiveLanguage( )
		_active_mapper			= self.GetActiveMapper( )
		_active_mapper_default	= ( _active_mapper == None or _active_mapper == 'Default' or _active_mapper == '' )

		## Grab the mapper filename.. excluding ext for now...
		## If the language name is Default, instead of Default.py it'll be __default__.py to appear before all others..
		_mapper_filename		= AcecoolLib.TernaryFunc( _active_language == 'Default' and _active_mapper_default, '__default__', _active_language )

		## If the mapper isn't Default then add it to the name ( this is so instead of Python_Default.py being wedged in the middle somewhere - it'll show up above all others... )...
		_mapper_filename		+= AcecoolLib.TernaryFunc( _active_mapper_default, '', '_' + _active_mapper )

		return _mapper_filename


	##
	## self.GetMapperClassName( )
	##
	def GetMapperClassName( self ):
		##
		_active_language		= self.GetActiveLanguage( )
		_active_mapper			= self.GetActiveMapper( )
		## _active_class			= self.GetActiveClass( )

		## Grab the Mapper via the Mapper ID / Class-Name using either the Globals dictionary or by using the active module [ Preferred ]... Run the mapper and return the output...
		## _class_name = 'ACMS_' + _active_language + '_' + _active_mapper
		_class_name = 'ACMS_{active_language}_{active_mapper}'.format( active_language = _active_language, active_mapper = _active_mapper )

		return _class_name


	##
	## Returns a list of all possible class-names to use...
	##
	def GetMapperClassNamesList( self ):
		##
		_active_language		= self.GetActiveLanguage( )
		_active_class			= self.GetActiveClass( )

		## Grab the Mapper via the Mapper ID / Class-Name using either the Globals dictionary or by using the active module [ Preferred ]... Run the mapper and return the output...
		if ( _active_class != None ):
			## _class_alt_a = 'ACMS_' + _active_language + '_' + _active_class
			_class_alt_a = 'ACMS_{active_language}_{active_class}'.format( active_language = _active_language, active_class = _active_class )
			_class_alt_b = _active_class

			return ( self.GetMapperClassName( ), _class_alt_a, _class_alt_b )
		else:
			return ( self.GetMapperClassName( ), '', '' )


	##
	##
	##
	def GetMapSyntaxHighlighter( self, _mapper ):
		## The syntax we use... Look for map_syntax in the mapper, otherwise use the default which is python...
		_syntax = AcecoolLib.TernaryFunc( hasattr( _mapper, 'map_syntax' ), getattr( _mapper, 'map_syntax', None ), None )

		return _syntax


































##
## Plugin Text Commands
##


## Command - This Developer Command Prints out whether or not the panel is open... Only enabled if the plugin Object is set...
class acms_dev_is_panel_open( sublime_plugin.TextCommand ):
	def description( self ):				return '[ ACMS ] Print Panel Open Status'
	def is_enabled( self ):					return IsPluginObjectValid( )
	def run( self, _edit, **_varargs ):		print( '[ ACMS.Dev ] Is Panel Open?' + AcecoolLib.TernaryFunc( IsPluginObjectValid( ), 'ACMS == VALID, Panel is: ' + AcecoolLib.TernaryFunc( IsPluginPanelOpen( ) == True, 'OPEN', 'SHUT' ), 'ACMS == NULL, Panel is likely NOT open!' ) )

## Command - This Developer Command prints the ACMS Object tostring method - useful for getting all of the necessary info without needing it print every single time the plugin is updated, etc...
class acms_dev_print_object( sublime_plugin.TextCommand ):
	def description( self ):				return '[ ACMS ] Print Plugin Data'
	def run( self, _edit, **_varargs ):		print( '[ ACMS.Dev ]---------\n' + str( GetPluginObject( ) ) )

## Command - Opens the ACMS - Panel... Non-Grayed when the panel is closed...
class acms_open_panel( sublime_plugin.TextCommand ):
	def description( self ):				return '[ ACMS ] Open Panel'
	def is_enabled( self ):					return IsPluginObjectValid( ) and not IsPluginPanelOpen( )
	def run( self, _edit, **_varargs ):		AcecoolLib.TernaryFunc( IsPluginObjectValid( ), GetPluginObject( ).Notify( 'Open Panel :: ' + AcecoolLib.TernaryFunc( IsPluginPanelOpen( ) == True, 'OPEN', 'SHUT' ) + AcecoolLib.TernaryFunc( GetPluginObject( ).ShowPanel( ), '', '' ) ), print( 'ACMS Object Is NOT Valid!' ) )

## Command - Closes the ACMS - Panel... Non-Grayed when the panel is open...
class acms_close_panel( sublime_plugin.TextCommand ):
	def description( self ):				return '[ ACMS ] Close Panel'
	def is_enabled( self ):					return IsPluginObjectValid( ) and IsPluginPanelOpen( ) == True
	def run( self, _edit, **_varargs ):		AcecoolLib.TernaryFunc( IsPluginObjectValid( ), GetPluginObject( ).Notify( 'Close Panel :: ' + AcecoolLib.TernaryFunc( IsPluginPanelOpen( ) == True, 'OPEN', 'SHUT' ) + AcecoolLib.TernaryFunc( GetPluginObject( ).HidePanel( ), '', '' ) ), print( 'ACMS Object Is NOT Valid!' ) )

## toggle_acecool_code_mapping_system_panel Command Object		## If not initialized then prevent it from running...		## Toggle the panel visibility
class acms_toggle_panel( sublime_plugin.TextCommand ):
	def description( self ):				return '[ ACMS ] Toggle Panel'
	def is_enabled( self ):					return IsPluginObjectValid( )
	def run( self, _edit, **_varargs ):		GetPluginObject( ).TogglePanel( )


	## TernaryFunc( IsPluginObjectValid( ), GetPluginObject( ).TogglePanel( ), print( 'ACMS -> Error Toggling Panel via acms_toggle_panel command - Plugin Object is NOT VALID!' ) )



































##
## Called when we need to update the data displayed in the output panel....
##
## Task: Make this complex so multiple regions are added, etc.... tons of info so we can fold everything, icons being displayed, and much much more!
##
class acms_update_panel_contents( sublime_plugin.TextCommand ):
	##
	def description( self ):
		return '[ ACMS.Dev ] acms_update_panel_contents'

	##
	## When the command has been executed...
	##
	def run( self, _edit, **_varargs ):
		_file = _varargs[ 'file' ]



































##
## Command: Text - This plugin modifies the replaces the entire contents of a file with the contents supplied to it by one or more mappers...
##
class acms_map_file( sublime_plugin.TextCommand ):


	##
	##
	##
	def run( self, _edit, **_varargs ):
		##
		if ( not ACMS( ) ):
			## print( 'ACMS == None' );
			return None;

		## Grab the 2 args - although we can simply grab it all through ACMS( )
		## _source = _varargs[ 'SourceCode' ];
		## _view = _varargs[ 'MappingPanel' ];
		## _view_code = _varargs[ 'SourceView' ];

		##
		_file = _varargs.get( 'file', ACMS( ).GetSourceFileName( ) );
		## if ( _file == None ):
		## 	print( '[ ACMS.Dev ] Had to force file using acms_map_file' );
		## 	_file = ACMS( ).GetSourceFileName( );

		## If force nocache remap, it means we ignore existing cache to ensure the file is remapped - if caching system is enabled, the new map will be saved...
		_force_nocache_remap = _varargs.get( 'force_nocache_remap', False );

		## If preprocessor only is enabled, it means we want to recache the file, but we do not want to update the output panel...
		_preprocessor_only = _varargs.get( 'preprocessor_only', False );

		##
		## print( 'Attempting to map: ' + _file );

		##
		## ACMS( ).UpdatePanelContents( 'Welcome to the mapping revolution!', '', _edit );


		## Grab the panel view
		_view = ACMS( ).GetOutputView( );

		## If the view we're targeting isn't the right panel, prevent the change from occurring...
		if ( not ACMS( ).IsValidOutputPanel( _view ) ):
			return False;

		## Alter read only state...
		AcecoolLib.Sublime.SetViewReadOnly( _view, False );

		## Create a Sublime Text Region containing the entire set of text from the panel...
		_region				= AcecoolLib.Sublime.GetViewContentsRegion( _view );

		## Set up base output string
		_output = '';

		## Set up the new mapping system using individual files - if they exist, otherwise using the old..
		_language_name			= ACMS( ).GetSetting( 'language_name', 'Default' );
		## _active_language		= ACMS( ).GetSetting( 'active_language', 'Default' );
		## _active_mapper			= ACMS( ).GetSetting( 'active_mapper', 'User' );
		## _active_mapper_default	= ( _active_mapper == None or _active_mapper == 'Default' or _active_mapper == '' );

		## ## Grab the mapper filename.. excluding ext for now...
		## ## If the language name is Default, instead of Default.py it'll be __default__.py to appear before all others..
		## _mapper_filename	= TernaryFunc( _active_language == 'Default' and _active_mapper_default, '__default__', _active_language );

		## ## If the mapper isn't Default then add it to the name ( this is so instead of Python_Default.py being wedged in the middle somewhere - it'll show up above all others... )...
		## _mapper_filename += TernaryFunc( _active_mapper_default, '', '_' + _active_mapper );

		##
		## print( '[ ACMS ] Trying to load new mapper: AcecoolCodeMappingSystem/maps/' + _mapper_filename + '.py' );
		## print( '[ ACMS ] based on file: ' + _file );
		## print( '[ ACMS ] source filenm: ' + AcecoolLib.String.GetFileName( ACMS( ).GetSourceFileName( ) ) );

		## _mapper_classname = ACMS( ).GetMapperClassName( );
		( _mapper_classname, _mapper_classname_a, _mapper_classname_b ) = ACMS( ).GetMapperClassNamesList( );
		_mapper_filename	= ACMS( ).GetMapperFileName( );
		_active_class		= ACMS( ).GetActiveClass( );
		_mapper_chosen		= 'old <Extension>.py';
		_mapper_method		= 'generate( ... )';
		_mapper_error		= 'Errors and Quick Resolutions List:\n\n';

		## Syntax using new system is either the default language syntax for source view, or it is in the config file.... For the output panel, it is in the config file...
		_syntax = None;

		## If the file exists - use the new, otherwise the old...
		if ( AcecoolLib.File.Exists( os.path.join( sublime.packages_path( ), 'AcecoolCodeMappingSystem', 'maps', _mapper_filename + '.py' ) ) ):
			##
			## print( '[ ACMS ] new mapper: EXISTS - Using new <Language>_<Mapper>.py method!' );
			## _mapper_error += '\t[ ACMS ] new mapper: EXISTS - Using new <Language>_<Mapper>.py method!\n';
			_mapper_error	+= '\tMapper File Name Mode: <active_language>_<active_mapper>.py - _<active_mapper> is only added if active_mapper is NOT "Default"\n';
			_mapper_error	+= '\tMapper File Name: maps/' + _mapper_filename + '.py\n';
			_mapper_error	+= '\t\tNote: FILE EXISTS! The most likely cause of THIS error is invalid class-name in the file...\n\n';
			_mapper_chosen	= 'new <Language>_<Mapper>.py [ ' + _mapper_filename + '.py ]';

			## Grab the mapper file...
			_mapper			= ACMS( ).GetMapClass( _file );
		else:
			##
			## print( '[ ACMS ] new mapper: DOES NOT EXIST - Using old <Extension>.py method!' )
			_mapper_error	+= '\t[ ACMS ] new mapper: DOES NOT EXIST - Using old <Extension>.py method!\n';

			## This is the path/to/ the file we're using to map the current source-code - note: This will change to ACMS folder and we'll map languages to extensions so we can use lang name later... and multiple languages...
			_mapper			= ACMS( ).GetMapClass( _file );

			## The syntax we use for old method is set in the mapper file - this is for the output panel..... Look for map_syntax in the mapper, otherwise use the default which is python...
			_syntax			= ACMS( ).GetMapSyntaxHighlighter( _mapper );

		## ## Error Output Notes
		## _mapper_error += '\tClass Names - Any of these should appear in the file ( if "active_class" is set, you will see 3, otherwise only 1 )!\n'
		## _mapper_error += '\t\tclass ' + _mapper_classname + '( PARENT_CLASS_NAME ):\n';

		## if ( _active_class != None ):
		## 	_mapper_error += '\t\tclass ' + str( _mapper_classname_a ) + '( PARENT_CLASS_NAME ):\n';
		## 	_mapper_error += '\t\tclass ' + str( _mapper_classname_b ) + '( PARENT_CLASS_NAME ):\n';

		## _mapper_error += '\n'
		## _mapper_error += '\t\t\tNote: PARENT_CLASS_NAME will most likely be: XCodeMapperBase, ACMS_' + ACMS( ).GetActiveLanguage( ) + '_Default, or ACMS_ParentLanguageName_User or Default!\n'
		## _mapper_error += '\t\t\tNote: The most important part is after "class " and before "( PARENT_CLASS_NAME ):" - ie the class name!\n'
		## _mapper_error += '\t\t\tNote: If the mapper file is empty, look at a _template_User.py file to see how a new file should look!\n'
		## _mapper_error += '\t\t\tNote: If a class name exists in the file, but is not the same as any above, rename it!\n'
		## _mapper_error += '\t\t\tNote: For PARENT_CLASS_NAME use XCodeMapperBase or ACMS_LanguageName_Default if you\'re not sure what to put, or keep it the same as it was in the file!\n'

		## elif ( AcecoolLib.IsString( _syntax_source ) ):
		## 	## print( '[ ACMS ] >>>> PLUGIN_OVERRIDE_SYNTAX == true >>>> Setting Output: ' + 'Packages/' + _syntax_source );
		## 	_view.set_syntax_file( 'Packages/' + _syntax_source );


		## Grab a list of mapper class-names to try...
		_is_active_class_set	= ( ACMS( ).GetActiveClass( ) != None );
		_is_mapper_valid		= False;

		## Determine the mapper class-name to use... - First we try Default, then we try active_class, if set, either by itself or as active_mapper replacement, then we try the left-over ( by itself or replacer )
		if ( hasattr( _mapper, _mapper_classname ) ):
			_is_mapper_valid	= True;
			## _mapper_classname = _mapper_classname;
			## print( '[ ACMS.Dev ] Using Mapper Class: ' + _mapper_classname );
		elif ( _is_active_class_set and hasattr( _mapper, _mapper_classname_a ) ):
			_is_mapper_valid	= True;
			_mapper_classname	= _mapper_classname_a;
			## print( '[ ACMS.Dev ] Using Mapper Class: ' + _mapper_classname );
		elif ( _is_active_class_set and hasattr( _mapper, _mapper_classname_b ) ):
			_is_mapper_valid	= True;
			_mapper_classname	= _mapper_classname_b;
			## print( '[ ACMS.Dev ] Using Mapper Class: ' + _mapper_classname );

		## If the file has the class then it should have .Run - we'll need to look for that...
		if ( _is_mapper_valid and hasattr( getattr( _mapper, _mapper_classname ), 'Run' ) ):
			##
			## print( '[ ACMS ] Using Class.Run( ... ) method of execution!' );
			_mapper_error += '[ ACMS ] Using Class.Run( ... ) method of execution!\n';
			_mapper_method = 'Class.Run( ... )';

			##
			_output += getattr( _mapper, _mapper_classname )( ).Run( _file, _mapper_classname, _force_nocache_remap );
		elif ( hasattr( _mapper, 'generate' ) ):
			##
			## print( '[ ACMS ] Using globals generate( ... ) method of execution!' );
			_mapper_error += '[ ACMS ] Using globals generate( ... ) method of execution!\n';

			## If the file has a generate function - assume it is CodeMap style...
			_output += _mapper.generate( _file );
		else:
			##
			## print( '[ ACMS ] Showing Missing Mapper Error and Solutions Output!' );
			_mapper_method = 'no';
			_mapper_error += '\tShowing Missing Mapper Error and Solutions Output!\n';

			## If it doesn't, we build the class name and execute the Run function... 2 args - _file / full path... and the generated class-name for debugging...
			_output += self.GetMissingMapperOutput( _mapper_error + '\n\t' );

		##
		## print( '[ ACMS ] Using ' + _mapper_chosen + ' mapper! Using ' + _mapper_method + ' mapper method to process the data! ' + _mapper_error )

		## If we have the preprocessor enabled, we only wanted to run the script in the background to update the cache - we don't want to reoplace the output window mapping...
		if ( _preprocessor_only ):
			## print( 'File Preprocessed - Stopping before outputting to output and overriding settings...: ' + _file + '' )
			return;

		## Replace ALL of the text with our _data
		_view.replace( _edit, _region, _output );

		## Set the output view as a scratch / temp view so changes or the update doesn't trigger file-changed flag...
		AcecoolLib.Sublime.SetViewScratch( _view, True );

		## Setup Output Panel Syntax Highlighting...
		self.SetupOutputViewSyntaxHighlighting( _syntax );


		## Reset our read-only status...
		AcecoolLib.Sublime.SetViewReadOnly( _view, True );

		## Reset Horizontal Scroll so we're looking at what matters
		AcecoolLib.Sublime.ResetViewHorizontalScroll( _view );

		## Add some distance between the last refresh so we don't need to do it manually for debugging
		if ( ACMS( ).GetSetting( 'acms_enable_clear_console', False ) ):
			AcecoolLib.Sublime.ClearConsole( min( ACMS( ).GetSetting( 'acms_clear_console_new_lines', 5 ), 1 ) );


	##
	## Helper - Gets the Syntax Highlighter file we intend to use from our override syntax_files/ folder, or we use the one which is already loaded in the Source View if our new variant doesn't exist...
	##
	def SetupOutputViewSyntaxHighlighting( self, _syntax = None ):
		##
		_plugin			= ACMS( );
		_view			= _plugin.GetOutputView( );
		_syntax_output	= _plugin.GetDefaultSyntaxHighlighter( );

		## Setup a default output panel / view syntax in case view_syntax_output isn't set for a particular language..
		if ( _syntax != None ):
			_syntax_output	= _syntax;

		## If we want to override the syntax used by the source view based on configuration
		if ( _plugin.GetSetting( 'plugin_override_syntax', False ) ):
			##
			_syntax_source	= _plugin.GetSetting( 'view_syntax_source', False );
			_syntax_output	= _plugin.GetSetting( 'view_syntax_output', _syntax_output );

			##
			## print( '[ ACMS ] >>>> PLUGIN_OVERRIDE_SYNTAX == ' + str( _syntax_override ) + ' >>>> Setting Source: ' + 'Packages/' + str( _syntax_source ) + ' || Setting Output: ' + 'Packages/' + str( _syntax_output ) )


			## and valid data exists for the source view, set it...
			if ( AcecoolLib.IsString( _syntax_source ) ):
				## print( '[ ACMS ] >>>> PLUGIN_OVERRIDE_SYNTAX == true >>>> Setting Source: ' + 'Packages/' + _syntax_source );
				_plugin.GetSourceView( ).set_syntax_file( 'Packages/' + _syntax_source );

			## and valid data exists for the output view, set it...
			if ( AcecoolLib.IsString( _syntax_output ) ):
				## print( '[ ACMS ] >>>> PLUGIN_OVERRIDE_SYNTAX == true >>>> Setting Output: ' + 'Packages/' + _syntax_output );
				_view.set_syntax_file( 'Packages/' + _syntax_output );
		else:
			_view.set_syntax_file( 'Packages/' + _syntax_output );


	##
	def description( self ):
		return '[ ACMS.Dev ] acms_map_file'

		##
		_active_class			= self.GetActiveClass( )
		( _class_name, _class_name_a, _class_name_b ) = self.GetMapperClassNamesList( )
		##
		_text += '\n\t• ' + AcecoolLib.String.FormatColumn( _col_name, 'Mapper -> active_class:',						_col_data, _active_class )
		_text += '\n\t\tNote: If active_class is set, then there are 2 other possibile class-names which will be checked for in the file.'
		_text += '\n'

		##
		if ( _active_class != None ):
			_text += '\n\t• ' + AcecoolLib.String.FormatColumn( _col_name, 'Mapper Class Option using active_Class:',	_col_data, 'class ' + _class_name_a + '( XCodeMapperBase ):' )
			_text += '\n\t• ' + AcecoolLib.String.FormatColumn( _col_name, 'Mapper Class Option using active_Class:',	_col_data, 'class ' + _class_name_b + '( XCodeMapperBase ):' )
			_text += '\n'
			_text += '\n\t• ' + AcecoolLib.String.FormatColumn( _col_name, 'Mapper Class ( NEW Alt ):',					_col_data, 'class ACMS_' + self.GetMapperFileName( ) + '( XCodeMapperBase ):' )
			_text += '\n\t\tNote: The Alternative class-name set using "active_class" should be the <active_mapper> portion of the name, or the complete name of the class.'
			_text += '\n\t\tNote: If active_class is set, the NEW classname will still be checked, but only AFTER checking 2 Alt possible class names. IE setting it to Blah will load: class ACMS_ActiveLanguage_Blah( PARENT ): OR class Blah( PARENT ):'
			_text += '\n'


	##
	##
	##
	def GetMissingMapperOutput( self, _error = '' ):
		## Scope
		_output = ''

		##
		if ( _error != '' ):
			_output += 'ACMS Mapper System Encountered a Fatal Flaw: \n\n\t' + str( _error ) + '\n\n\n'


		## _mapper_classname = ACMS( ).GetMapperClassName( )
		( _mapper_classname, _mapper_classname_a, _mapper_classname_b ) = ACMS( ).GetMapperClassNamesList( )
		_mapper_filename	= ACMS( ).GetMapperFileName( )
		_active_class		= ACMS( ).GetActiveClass( )
		## _mapper_chosen		= 'old <Extension>.py'
		## _mapper_method		= 'generate( ... )'
		## _mapper_error		= 'Errors and Quick Resolutions List:\n\n'
		## 	## _mapper_error += '\t[ ACMS ] new mapper: EXISTS - Using new <Language>_<Mapper>.py method!\n'
		## 	_output += '\tMapper File Name Mode: <active_language>_<active_mapper>.py - _<active_mapper> is only added if active_mapper is NOT "Default"\n'
		## 	_output += '\tMapper File Name: maps/' + _mapper_filename + '.py\n'
		## 	_output += '\t\tNote: FILE EXISTS! The most likely cause of THIS error is invalid class-name in the file...\n\n'

		_output += '''------------------------------------------------------------------------------------------------------------------------
The following provides the shortest method of debugging why you're seeing this error. If you proceed beyond
this segment, you'll see a LOT of information which may help, but it outlines a lot of the back-end system and how choices are made.

Quick Debugging Steps
---------------------
1 ) Make sure the file exists - if the Errors list above says the file exists, then move on to step 2 or verify the file exists.
'''

		_output += '\tThe file will either follow the new <active_language>_<active_mapper>.py / <active_language>.py OR old <ext>.py file-naming method\n'
		_output += '\t\tNew:\t' + ACMS( ).GetMapperFileName( ) + '.py\n'
		_output += '\t\tOld:\t' + ACMS( ).GetSourceFileExt( ) + '.py\n\n'

		_output += '\tThe file will exist in either Packages/AcecoolCodeMappingSystem/maps/ OR Packages/User/AcecoolCodeMappingSystem/maps/\n\n'

		_output += '''2 ) Make sure the class exists within the function - even in the Code Map and Old <ext>.py method of loading files, class-name is always looked at first...

'''



		## Error Output Notes
		_output += '\tClass Names - Any of these should appear in the file ( if "active_class" is set, you will see 3, otherwise only 1 )!\n'
		_output += '\t\tclass ' + _mapper_classname + '( PARENT_CLASS_NAME ):\n'

		if ( _active_class != None ):
			_output += '\t\tclass ' + str( _mapper_classname_a ) + '( PARENT_CLASS_NAME ):\n'
			_output += '\t\tclass ' + str( _mapper_classname_b ) + '( PARENT_CLASS_NAME ):\n'

		_output += '\n'
		_output += '\t\t\tNote: PARENT_CLASS_NAME will most likely be one of the following without quotes - I only use it because of how widely it can vary:'
		_output += '\n\t\t\t\tXCodeMapperBase\t\t\t\t\t\t\t\t\t\t\t-- Used in most cases when the class name ends with "_Default"'
		_output += '\n\t\t\t\tACMS_' + ACMS( ).GetActiveLanguage( ) + '_Default\t\t\t\t\t\t\t\t\t-- Typically used for the same language when the class name does not end with "_Default"'
		_output += '\n\t\t\t\tACMS_ParentLanguageName_User\t\t\t\t-- Example: class ACMS_GMod_Lua_Default( ACMS_Lua_User ):'
		_output += '\n\t\t\t\tACMS_ParentLanguageName_Default\t\t\t-- Example: class ACMS_GMod_Lua_Default( ACMS_Lua_Default ):'
		_output += '\n'
		_output += '\n'
		_output += '\t\t\tNote: The most important part is after "class " and before "( PARENT_CLASS_NAME ):" - ie the class name!\n'
		_output += '\t\t\tNote: If the mapper file is empty, look at a _template_User.py file to see how a new file should look!\n'
		_output += '\t\t\tNote: If a class name exists in the file, but is not the same as any above, rename it!\n'
		_output += '\t\t\tNote: For PARENT_CLASS_NAME use XCodeMapperBase or ACMS_LanguageName_Default if you\'re not sure what to put, or keep it the same as it was in the file!\n\n'
		_output += '''3 ) If the file exists, and the correct class name / line exists in the file then the issue is likely with
	configuration, OR an error in the file preventing it from loading... It could also be a different issue. The best way to determine
	what type of issue it is, is to read through the

	Acecool Code Mapping System Plugin Object ToString ---- data....

	That data, included below, provides a lot of important information about the plugin, such as the window it is loaded in, configuration
	keys and values, the active configuration values, the source-filename used for mapping the file, the output panel id, etc... It also contains
	information on what file-name is loaded or looked for in the maps/ folder, and gives you an idea of class-names it tries to load. Some of that
	information has been provided above so the debugging steps can be placed in one area... But the Object ToString will contain the most important
	information about the plugin, and the configuration used...

'''
		_output += '\n------------------------------------------------------------------------------------------------------------------------\n\n'

		_output += '\n' + str( ACMS( ) )

		_output += '\n------------------------------------------------------------------------------------------------------------------------\n\n'

		## Main Notice Message
		_output += '''A Mapper does not currently exist for this language... This message can appear for a number of reasons..

◙ If the file exists ( <active_language>.py or <active_language>_<active_mapper>.py - Case Sensitive ) and you're receiving
	this message then the most likely reason is because the Class doesn't exist in the file... The old system <ext>.py used
	a generate function in the file which used a system to load the configured class - this system was bulky and code lines
	were repeated for each file - it was messy to read in some cases, and so much code was repeated which I don't like...
	When I went to a stand-alone plugin I got away from the CodeMap method of using a generate file by using the new language
	and mapper way to load files. But each class-name needs to be unique too, especially if you're loading into another or if
	you want to load multiple languages to map a single file ( which is the goal so .php which uses 6 languages or so, can
	use 6 mappers instead of needing to rewrite a mapper for each extension, etc.. and copy over language definitions, etc... )

	So, the way I load the class for the mapper is by using a set naming scheme. That naming scheme is similar to the file
	naming system, with a few differences.

	First: I do not remove _Default from the class-name as I do with file-names if active_mapper is Default.
	Second: ACMS_ is used as a prefix for the class-name.

	So the class name will be defined in the file as:

		class ACMS_<active_language>_<active_mapper>( XCodeMapper ):


	There are some instances where this will not be the same. XCodeMapper is the BASE Code Mapping System for this plugin,
	and that name will change, but in one case I use ACMS_Lua_Default or ACMS_Lua_User instead of XCodeMapper for the parent
	class in the GMod_Lua file as GMod Lua extends Lua, so there's no point in repeating code written in the Lua mappers.

	Additionally, I added another system to set the class-name manually... By default the value is null or None in Python.

	The configuration key you'd set in a Language / Project table is "active_class". There are several class-names I look
	for when active_class is defined, and I do this because if you put "Default" as the value then any number of classes
	could be named "Default" and that can cause issues... So I allow 2 options for the name to be used, and I may add
	another configuration key to make the checks faster in the future meaning you'd define the "active_class_mode" for
	exact to use the "active_class" value as the class-name in the file, or replace to use "active_class" for the class
	name to replace what "active_mapper" would be... so class ACMS_Language_Blah( PARENT ): or class Blah( PARENT ): if
	you used active_class as Blah without the mode being set... if replace is set for mode, then the first example.
	If exact is used for mode, then the second.. So you can set active_class to: Blah and I'll look for both if mode
	isn't set meaning class Blah and class ACMS_Language_Blah. If you set it to: ACMS_Language_Blah then I'll look for
	class ACMS_Language_Blah and ACMS_Language_ACMS_Language_Blah if mode isn't set - if you do use active_class then,
	by default, I will assume you're using replace mode. If you set the mode, then I won't look for both..

	Take all of this into consideration when setting up file-names.


◙ "acms_language_map->XXX" takes a file extension and maps it to a "LanguageID" - if there is a typo and
	the acms_language_map->LanguageID table doesn't exist, then the "acms_language_map->ACMS_Default_Language"
	key / data table is used in ACMS_Definitions! Note: It doesn't matter what the LanguageID is as long as the
	data table exists with its key / name in the following format: "acms_language_config->YYY"

◙ If the language_map maps a valid file-extension to a valid language id table and they all exist, then make
	sure active_language and active_mapper keys / data point to valid files. The filename generated isn't
	difficult after you understand the rules used to generate them...

	◙ If active_language is set to "Default" and active_mapper is set to "Default", "", or null then the
		resulting filename will be __default__.py and the class-name within to be loaded will be

		class ACMS_Default_Default( XCodeMapperBase ):

	◙ If active_language is anything other than "Default" such as "AnythingElse" and active_mapper is set
		to "Default", "", or null then the filename will be AnythingElse.py and the class name in the file will be:

		class ACMS_AnythingElse_Default( XCodeMapperBase ):

	◙ If active_language is anything other than "Default" such as "AnythingElse" and active_mapper is
		anything other than the previous examples such as "XXX" then the filename will be AnythingElse_XXX.py
		and the class name in the file will be:

		class ACMS_AnythingElse_XXX( XCodeMapperBase ):

	◙ Note: By default.. if active_language / mapper isn't in your language table, they're defaulted to
		Default and Default meaning the file used is __default__.py with a class name of

		class ACMS_Default_Default( XCodeMapperBase ):

	◙ Note: Another issue which can cause this message to appear is if the Class.Run( ... ) function
		and the globals generate( ... ) function doesn't exist. So if everything looks right - make
		sure those exist! If you used XCodeMapperBase as parent class then you're fine - or any other
		class which links via long chain to it, you're ok... generate will work too...

	◙ This error message only shows up when a mapper DOES NOT LOAD... The file-names looked for will
		be displayed at the bottom of the Acecool Code Mapping System Plugin Object ToString Message
		under Plugin / Mapping Data beside Mapper FileName New and Ext for the new system shown above,
		and the old system which simply looks for the Source View FileName Extension ( so py.py or
		lua.py ) would be the filenames in the maps/ directories...

		Make sure the names it displays are what you are expecting - these will properly generate
		the exact name to be used... so if you see:

		• Mapper FileName New:                         */AcecoolCodeMappingSystem/maps/Default_DefaultX.py
		• Mapper FileName Ext:                         */AcecoolCodeMappingSystem/maps/py.py

		then you know active_mapper is set to DefaultX and active_language is set to Default - possible
		typo, or not... but the file-names expected will be shown.

		ACMS Always looks in Packages/User/AcecoolCodeMappingSystem/maps/ first before looking in the
		default Packages/AcecoolCodeMappingSystem/maps/ folder for data...

		Optionally, if you enabled "enable_codemap_mappers" in ACMS_Plugin then you will also see:

		• Mapper FileName CodeMap Ext:                 */User/CodeMap/custom_mappers/py.py

		as another alternative location for the file to be....

		Note: ext.py files use a generate( _source_filename ) function to run the class whereas the
			new system uses the Class Run( _source_filename, _mapper_filename ) function..

		If this doesn't help - documentation is on the way, and there is another guide inside of the
			Packages/AcecoolCodeMappingSystem/maps/ folder as to how the filenames are decided...

			Please review other files in the maps. folder to see how the links are made - every file
			within the maps/ folder is linked via the ACMS_Definitions file too...
		'''


		return _output







































































##
## ACMS Helpers
##


##
## Returns the Plugin Object
##
def ACMS( ):
	## The Getter and Setter are the only 2 places this should be, aside from the file scope...
	global ACECOOL_CODE_MAPPING_SYSTEM

	## If the plugin object isn't set, or isn't valid - we return None..
	if ( not IsPluginObjectValid( ) ):
		return None

	## Return the ACMS Plugin Object
	return ACECOOL_CODE_MAPPING_SYSTEM


##
## Sets the Plugin Object
##
def SetACMS( _object = None, _old_shutdown = True, _old_override = True, _notify = False ):
	## The Getter and Setter are the only 2 places this should be, aside from the file scope...
	global ACECOOL_CODE_MAPPING_SYSTEM

	##
	if ( _object == None ):
		return False

	## Scope
	_data = None

	## Cleanly remove the old one...
	if ( IsPluginObjectValid( ) ):
		##
		if ( _old_override ):
			##
			if ( _old_shutdown ):
				## Shutdown the plugin, nicely, and return the important data to be used when we create the new object...
				_data = ACMS( ).Shutdown( )

			## Set it to None so the GetPluginObject( ) will return None for below...
			ACECOOL_CODE_MAPPING_SYSTEM = None

	## If the Plugin Object hasn't been set yet or it has been set and _old_override is set to true ( so the previous one can be unset ), then set it...
	if ( not IsPluginObjectValid( ) ):
		## Set the ACMS Plugin Object
		ACECOOL_CODE_MAPPING_SYSTEM = _object

		## Whether or not we should notify the client / console...
		## if ( _notify ):
		## Notify the client using the Notifcation call
		_object.Notify( 'SetPluginObject( <PluginObject>, _old_shutdown, _old_override, _notify ) was called!' )

			## Print the Object from the __str__ method...
			## print( ACMS( ) )
	else:
		## Whether or not we should notify the client / console...
		## if ( _notify ):
		## Task: Take the relevant data from the old and copy it to the new object....
		ACMS( ).Notify( 'SetPluginObject( <PluginObject>, _old_shutdown, _old_override, _notify ) was called when the object already exists, and Overriding is disabled!' )


##
## Helper - Returns whether or not the ACMS Plugin Object is valid...
##
def IsValidACMS( ):
	## The Getter and Setter are the only 2 places this should be, aside from the file scope...
	global ACECOOL_CODE_MAPPING_SYSTEM

	##
	## _PluginObjectGlobal = GetVariableValueByName( 'ACECOOL_CODE_MAPPING_SYSTEM', None )
	## _PluginObjectGlobal = globals( ).get( 'ACECOOL_CODE_MAPPING_SYSTEM', None )

	## globals( ) is a Dict - we use .get to determine whether or not the value has been set yet - we do this instead of if not THIS_PLUGIN_OBJECT or if THIS_PLUGIN_OBJECT == None, etc.. because all of those will ERROR meaning they'd need to be in a try / catch which is ugly for this... In short - if THIS_PLUGIN_OBJECT Plugin Object isn't set, return None...
	if ( globals( ).get( 'ACECOOL_CODE_MAPPING_SYSTEM', None ) == None or ACECOOL_CODE_MAPPING_SYSTEM == None ):
		return False

	## Assume False
	return True


##
## Helper - Returns whether or not the ACMS Plugin Object, and the panel( s ) are valid..
##
def IsValidACMSPanel( _view_src = None ):
	## If the plugin object isn't set / is invalid, or if a view object is provided and the view is invalid return false... The view does not need to be the panel... It shouldn't be...
	if ( not IsPluginObjectValid( ) ):
		return False

	## If we've provided a source panel to compare - make sure it isn't the output panel...
	if ( AcecoolLib.Sublime.IsValidView( _view_src ) ):
		## If we've provided a view - make sure it isn't the output panel...
		_path		= AcecoolLib.Sublime.GetViewFileName( _view_src, True )
		_filename	= AcecoolLib.Sublime.GetViewFileName( _view_src )

		## Look at the plugin object view and make sure it is the plugin object view...
		if ( _path == '' or _filename == GetPluginObject( ).GetPanelName( ) ):
			return False

	## Grab the output panel...
	_view_map = GetPluginObject( ).GetOutputView( )

	## Make sure the output panel exists...
	if ( not AcecoolLib.Sublime.IsValidView( _view_map ) or AcecoolLib.Sublime.GetViewFileName( _view_map ) != GetPluginObject( ).GetPanelName( ) ):
		return False

	## If all of the above failed to stop then assume true because the view given is a source view, the plugin is set, the plugin panel is set, etc...
	return True


##
##
##
def IsPluginPanelOpen( ):
	return IsPluginObjectValid( ) and GetPluginObject( ).IsPanelOpen( )


##
## Aliases
##
def GetPluginObject( *_args ):					return ACMS( *_args );
def GetPluginDatabase( *_args ):				return ACMSDatabase( ACMS( ) ) if ( ACMS( ).GetThreadState( ) ) else ACMS( ).Database( );

	## return ACMS( ).GetPluginDatabase( *_args );
def SetPluginObject( *_args ):					return SetACMS( *_args );
def IsPluginObjectValid( ):						return IsValidACMS( );
def IsPluginPanelValid( _view_src = None ):		return IsValidACMSPanel( _view_src );




##
## Returns the Plugin Object
##
def GetPluginNode( ):
	## The Getter and Setter are the only 2 places this should be, aside from the file scope...
	global ACECOOL_CODE_MAPPING_SYSTEM_NODE;

	## Return the ACMS Plugin Object
	return ACECOOL_CODE_MAPPING_SYSTEM_NODE


##
## Sets the Plugin Object
##
def SetPluginNode( _object = None ):
	## The Getter and Setter are the only 2 places this should be, aside from the file scope...
	global ACECOOL_CODE_MAPPING_SYSTEM_NODE;

	##
	ACECOOL_CODE_MAPPING_SYSTEM_NODE = _object;


##
## Plugin Node contains references to everything needed to properly use the references in a multi-threaded environment... IE, plugin reference for ui thread, plugin reference in worker thread, same with database, and more...
##
## Task: Determine whether or not to use this ... Also determine whether or not to break the plugin object down in favor of this as the information container, and the plugin object to be used as a basic plugin controls library, etc... We'll see...
##
class PluginNodeBase( ):
	pass
class PluginNode( PluginNodeBase ):
	##
	__thread				= AcecoolLib.AccessorFuncBase( parent = PluginNodeBase,		key = 'thread',					name = 'Thread',				getter_prefix = '',			documentation = 'Thread: This instance is the worker thread' );

	##
	__plugin				= AcecoolLib.AccessorFuncBase( parent = PluginNodeBase,		key = 'plugin',					name = 'Plugin',				getter_prefix = '',			documentation = 'Plugin: This instance is initialized within the main plugin thread, it can cause UI Thread Lockups', setup_override = { 'on_set': lambda this, _value, _new, _is_valid: SetPluginObject( _value ) } );
	__database				= AcecoolLib.AccessorFuncBase( parent = PluginNodeBase,		key = 'database',				name = 'Database',				getter_prefix = '',			documentation = 'Database: This instance is initialized within the main plugin thread, it can cause UI Thread Lockups' );

	##
	__threaded_plugin		= AcecoolLib.AccessorFuncBase( parent = PluginNodeBase,		key = 'threaded_plugin',		name = 'ThreadedPlugin',		getter_prefix = '',			documentation = 'Threaded Plugin: This instance is initialized within the worker thread, so it should be executed outside of the UI / Main Plugin Thread' );
	__threaded_database		= AcecoolLib.AccessorFuncBase( parent = PluginNodeBase,		key = 'threaded_database',		name = 'ThreadedDatabase',		getter_prefix = '',			documentation = 'Threaded Database: This instance is initialized within the worker thread, so it should be executed outside of the UI / Main Plugin Thread' );


	##
	## Helpers - Depending on if in a thread or not, we can use GetPlugin or PluginObject and Database / DatabaseObject to grab the threaded instance vs using different calls everywhere... This may make things more confusing for some people, but technically you should know your scope, including the threading scope... although, sometimes it isn't clear - so I'm still on the fence as to whether or not I should use this, or simply create a helper in the getters to check the thread scope and redirect to the other function from within, and display an error but allow it... That'd provide corrective information, and prevent breakage while ensuring function names are named for their proper thread..
	##
	def PluginObjectEx( self, _thread = False ):
		if ( _thread ):
			return self.ThreadedPlugin( );

		return self.Plugin( );


	##
	## Helpers - Depending on if in a thread or not, we can use GetPlugin or PluginObject and Database / DatabaseObject to grab the threaded instance vs using different calls everywhere... This may make things more confusing for some people, but technically you should know your scope, including the threading scope... although, sometimes it isn't clear - so I'm still on the fence as to whether or not I should use this, or simply create a helper in the getters to check the thread scope and redirect to the other function from within, and display an error but allow it... That'd provide corrective information, and prevent breakage while ensuring function names are named for their proper thread..
	##
	def PluginObject( self ):
		return self.PluginObjectEx( ACMS( ).UseMappingThreads( ) );


	##
	##
	##
	def DatabaseObjectEx( self, _thread = False ):
		if ( _thread ):
			return self.ThreadedDatabase( );

		return self.Database( );


	##
	##
	##
	def DatabaseObject( self ):
		return self.DatabaseObjectEx( ACMS( ).UseMappingThreads( ) );


	##
	##
	##
	def PluginDatabase( self ):
		return self.PluginObject( ), self.DatabaseObject( );
		## ##
		## _plugin						= None;
		## _database					= None;

		## ##
		## if ( ACMS( ).UseMappingThreads( ) ):
		## 	_plugin					= GetPluginNode( ).GetThreadedPlugin( );
		## 	_database				= GetPluginNode( ).GetThreadedDatabase( );
		## else:
		## 	_plugin					= GetPluginNode( ).GetThreadedPlugin( );
		## 	_database				= GetPluginNode( ).GetThreadedDatabase( );

		## ##
		## return _plugin, _database;


##
## Thread...
##
class ACMSThreadBase( threading.Thread ):
	pass
class ACMSThread( ACMSThreadBase ):
	## Jobs( _override_default = None, _none_if_default = False ), JobsLen( < Getter Args ), JobsLenToString( < Getter Args ), PopJobs( _index = len - 1, _count = 1 ), AddJobs( *_jobs ) and many more....
	## Task: Update Len helper to ensure it is properly returning the length of the object - right now it isn't returning the proper values which is why I have it in setup override as lambda
	__jobs			= AcecoolLib.AccessorFuncBase( parent = ACMSThreadBase,			key = 'jobs',			name = 'Jobs',					default = [ ],																				group = '',			getter_prefix = '',			documentation = 'Thread Job Helpers',			allowed_types = ( AcecoolLib.TYPE_LIST ),							allowed_values = AcecoolLib.VALUE_ANY,			setup_override = { 'use_threading_lock': False, 'add': True, 'pop': True, 'get_len': lambda this: len( this.Jobs( ) ) } )
	__database		= AcecoolLib.AccessorFuncBase( parent = ACMSThreadBase,		key = 'database',		name = 'Database',				default = None,																				group = '',			getter_prefix = '',			documentation = 'Thread Job Helpers'					 )


	##
	## Helper: Process the Job Entry
	##
	def ProcessJobEntry( self ):
		print( 'We have unprocessed jobs... #: ' + str( len( self.Jobs( ) ) ) + ' -- ' + str( self.Jobs( ) ) );

		## print( 'Jobs count: ' + str( len( self.Jobs( ) ) ) );
		print( 'Jobs count: ' + str( self.JobsLen( ) ) );

		## Grab the Job / Entry - Note: By default the len - 1'th entry is grabbed meaning the latest one inserted is the first one we look at, which is good... If we switch between files, then the most up-to-date will be the first to process.. Also, when I add a time-check to it, if another view is being processed and is asking to re-process the file and or output the data, we can easily check the timestamp to see which one is newer and also look at which view has focus to prevent any mishaps..
		_job	= self.PopJobs( );

		## Grab the view we're targeting... Could also be from file-name instead if we do project-wide preprocessing instead of opening all files in views, we target files...
		_view_id	= _job.get( 'view_id', None );
		_view		= None;

		## If the _view_id is set, then we should be able to grab the View - but having the view isn't everything - we can easily grab the full file path, view, or a few other things to process the update..
		if ( _view_id != None ):
			_view		= sublime.View( _view_id );

		## Grab the job type ( Update all open files in this window or all windows, all project files, or a single view / file )
		_type		= _job.get( 'type', None );

		## Does this job require us to remap the file?
		_path		= _job.get( 'path', AcecoolLib.Sublime.GetViewFileName( _view, True ) );

		## Does this job require us to remap the file?
		_remap		= True if ( _job.get( 'map', True ) or _job.get( 'force_nocache_remap', True ) ) else False;

		## Should we update the output panel?
		_update		= True if ( _job.get( 'output', False ) or _job.get( 'preprocessor_only', False ) ) else False;


		##
		## Several ways to update the output panel...
		## Note: Depending on the job, ie whether or not to update all open files ( in all windows or just local ), or all project files, or just a single file / view - handle the job call here... There'll be another helper here, most likely, for each different job type..
		##
		## ACMS( ).ProcessSourceCodeMapping( _path, _remap, _update );
		## sublime.set_timeout_async( lambda: ACMS( ).ProcessSourceCodeMapping( _path, _remap, _update ), 0 );
		sublime.set_timeout_async( lambda: _view.run_command( 'acms_map_file', { 'view_id': _view_id, 'file': _path, 'force_nocache_remap': _remap, 'preprocessor_only': _update } ), 0 );


	##
	##
	##
	def run( self ):
		print( '[ ACMS.ACMSThread -> run' );
		##
		if ( IsPluginObjectValid( ) ):
			print( 'ACMS Object is valid - exiting thread...' );
			return;

		## Plugin is already loaded - don't re-load it... Call plugin_refreshed( );
		if ( not IsPluginObjectValid( ) ):
			## Create a new instance of the Plugin Object...
			PluginObject = AcecoolCodeMappingSystemPlugin( );

			## ## ## Print out the object data...
			## PluginObject.Notify( 'PluginObject Loaded for the first time from within the Thread...' );
			## PluginObject.Notify( PluginObject );

			## ## This is the first load...
			## SetPluginObject( PluginObject );

			## ## Set up the database we'll be using..
			## self.SetDatabase( ACMSDatabase( ACMS( ) ) );


			GetPluginNode( ).SetThreadedPlugin( PluginObject );
			GetPluginNode( ).SetThreadedDatabase( ACMSDatabase( PluginObject ) );
			PluginObject.SetThread( self );

		## Wait until ACMS Is Valid
		while True:
			##
			if ( IsValidACMS( ) ):
				## If Threads are disabled, we shutdown... We can only do this when we have a reference to the plugin object since we're starting outside of the plugin class as the plugin class can be unloaded and reloaded whenever...
				if ( not ACMS( ).UseMappingThreads( ) ):
					print( '[ ACMS.WorkerThread ] Threading has been disabled in the configuration options - this may cause lockups in the UI thread, it is highly recommended you enable threading support to prevent coding delays.' );

					## Make sure the plugin knows the thread state isn't running
					ACMS( ).SetThreadState( False );

					## Abort the thread startup sequence...
					return False;

				##
				print( 'ACMS Plugin Object IsValid - Starting Thread...' );

				## Make sure the plugin knows the thread state is running...
				ACMS( ).SetThreadState( True );

				##
				break;

		## Assigning Referencce to thread...
		ACMS( ).SetThread( self );

		##
		print( '[ ACMS.WorkerThread ] Threading has been enabled and the Plugin Object has received the worker-thread reference: ' + str( self ) );

		##
		## while True:
		while True:
			## print( 'Thread is running...   ' + str( ACMS( ).GetThread( ) ) + ' / ' + str( self ) );
			## print( 'Thread Notes...   Jobs#: ' + str( self.JobsLen( ) ) + ' / ' + str( self.Jobs( ) ) );

			##
			time.sleep( 1.0 );


			## If Threads are disabled, we idle, check back 4 times per minute to see if we re-enabled ( We do this until we shutdown the thread properly with the ability to relaunch if the user changes their config )...
			if ( not ACMS( ).UseMappingThreads( ) ):
				print( '[ ACMS.WorkerThread.Shutdown ] Threading has been disabled in the configuration options - and is now entering IDLE mode until re-opened... If you start Sublime Text with threading off, it will not enter IDLE and you will have to restart Sublime Text for the thread to start until a system is in place to handle changes and referencing..' );

				while ( not ACMS( ).UseMappingThreads( ) ):
					time.sleep( 15.0 );


			## Preventing some repeats...
			## _lock.acquire( );
			_jobs	= self.Jobs( );
			_len	= self.JobsLen( );
			## _lock.release( );

			##
			if ( _len > 0 ):
				## print( AcecoolLib.Text.FormatPipedHeader( 'We have unprocessed jobs... #: ' + str( len( self.Jobs( ) ) ) + ' -- ' + str( self.Jobs( ) ) + '' ) );
				print( AcecoolLib.Text.FormatPipedHeader( 'We have unprocessed jobs... #: ' + str( _len ) + ' -- ' + str( _jobs ) + '' ) );

			## While we have jobs to process, we process them...
			while ( self.JobsLen( ) > 0 ):
				self.ProcessJobEntry( );
				time.sleep( 0.15 );

				## When we are out of jobs, exit the Job Loop...
				if ( self.JobsLen( ) < 1 ):
					print( 'Jobs Queue has been processed... Waiting for new jobs..' );
					break;



##
## Callback - This is called as soon as the plugin has been loaded by Sublime Text... This is where we initialize everything...
##
global ACMS_Thread_Worker
ACMS_Thread_Worker = None;
def plugin_loaded( ):
	global ACMS_Thread_Worker;
	##
	## print( '[ ACMS.plugin_loaded' );

	##
	if ( IsPluginObjectValid( ) ):
		##
		## print( 'ACMS Object is valid - exiting plugin_loaded...' );

		##
		return;

	## Create a new instance of the Plugin Object...
	PluginObject = AcecoolCodeMappingSystemPlugin( )

	## Plugin is already loaded - don't re-load it... Call plugin_refreshed( );
	if ( ( not IsPluginObjectValid( ) and ACMS_Thread_Worker == None ) or ( ACMS( ) != None and ACMS( ).GetThread( ) == None ) ):
		## ## ## Print out the object data...
		PluginObject.Notify( 'PluginObject Loaded for the first time!' );
		PluginObject.Notify( PluginObject );

		## ## This is the first load...
		SetPluginObject( PluginObject );

		##
		## print( '[ ACMS.plugin_loaded -> AcecoolCodeMappingSystemPlugin.py' );

		_node = PluginNode( );
		SetPluginNode( _node );

		##
		## plugin_thread_start( );

		ACMS_Thread_Worker = ACMSThread( );
		## ACMS( ).SetThread( ACMS_Thread_Worker );
		GetPluginNode( ).SetPlugin( PluginObject );
		GetPluginNode( ).SetDatabase( ACMSDatabase( PluginObject ) );
		GetPluginNode( ).SetThread( ACMS_Thread_Worker );
		PluginObject.SetThread( ACMS_Thread_Worker );
		ACMS_Thread_Worker.start( );

	## else:
	## 	## Run the plugin_refreshed callback... and let it decide whether or not to update the reference or not...
	## 	return plugin_refreshed( GetPluginObject( ), PluginObject );


##
## Plugin Unloaded / Shutdown - Clear up memory, etc..
##
def plugin_unloaded( ):
	print( '[ ACMS.plugin_unloaded' );
	## If the plugin is set, then we can unload it...
	if ( GetPluginObject( ) != None ):
		GetPluginObject( ).Shutdown( );
		## plugin_thread_stop( );


## ##
## ## Plugin Auto-Refreshed / Reloaded - Do something slightly different than on first load...
## ##
## def plugin_refreshed( _old, _new, _notify = False ):
## 	## Add some distance between the last refresh so we don't need to do it manually for debugging
## 	AcecoolLib.Sublime.ClearConsole( 5 );

## 	## We do want to update it - so set it..
## 	## SetPluginObject( _new );

## 	##
## 	## plugin_thread_start( );


## ##
## ##
## ##
## def plugin_thread_start( ):
## 	##
## 	print( '[ ACMS.plugin_thread_start' );
## 	global ACMS_Thread_Worker;
## 	ACMS_Thread_Worker = ACMSThread( );
## 	## ACMS( ).SetThread( ACMS_Thread_Worker );
## 	ACMS_Thread_Worker.start( );

## def plugin_thread_start( ):
## 	global ACMS_Thread_Worker
## 	global ACMS_Thread_Worker_Jobs


## 	ACMS_Thread_Worker_Jobs = [ ]

## 	ACMS_Thread_Worker = ThreadingSystem( );
## 	ACMS_Thread_Worker.start( );

##
##
##
def plugin_thread_stop( ):
	## If the Thread State wasn't initially started, it means the value had to have been changed to True, then False - ie it should be running... - this is if we never adjust the thread state..
	## If the thread state is started, then we should be able to stop...
	if ( not ACMS( ).GetThreadState( ) ):
		## 	print( 'Stopping Thread ( If thread is set to True then it simply goes to idle )' );
		## else:
		## Set the Thread state to true
		ACMS( ).SetThreadState( True );
		plugin_thread_start( );





























































































































































































## ##
## ## Callback - This is called as soon as the plugin has been loaded by Sublime Text... This is where we initialize everything...
## ##
## def plugin_loaded( ):
## 	## Create a new instance of the Plugin Object...
## 	PluginObject = AcecoolCodeMappingSystemPlugin( );

## 	##
## 	print( '[ ACMS.plugin_loaded -> AcecoolCodeMappingSystemPluginReloader.py' );

## 	##
## 	plugin_thread_start( );






##
## Returns the Plugin Object
##
def GetThreadJobs( ):
	## ## The Getter and Setter are the only 2 places this should be, aside from the file scope...
	## global ACECOOL_CODE_MAPPING_SYSTEM_JOBS;

	## ##
	## if ( ACECOOL_CODE_MAPPING_SYSTEM_JOBS == None ):
	## 	ACECOOL_CODE_MAPPING_SYSTEM_JOBS = [ ];

	## ##
	## print( 'GetThreadJobs has been called! -> ' + str( ACECOOL_CODE_MAPPING_SYSTEM_JOBS ) );


	## ## Return the ACMS Plugin Object
	## return ACECOOL_CODE_MAPPING_SYSTEM_JOBS;
	## ## return ( ACECOOL_CODE_MAPPING_SYSTEM_JOBS )[ AcecoolLib.islist( ACECOOL_CODE_MAPPING_SYSTEM_JOBS ) ];

	##
	return ACMS( ).GetThread( ).Jobs( );


##
## Sets the Plugin Object
##
def SetThreadJobs( _jobs ):
	## ## The Getter and Setter are the only 2 places this should be, aside from the file scope...
	## global ACECOOL_CODE_MAPPING_SYSTEM_JOBS;

	## ##
	## print( 'SetThreadJobs has been called! -> ' + str( _jobs ) );

	## ##
	## ACECOOL_CODE_MAPPING_SYSTEM_JOBS = _jobs;

	##
	ACMS( ).GetThread( ).SetJobs( _jobs );


## ##
## ## Sets the Plugin Object
## ##
def AddThreadJobs( _job ):
	## ## The Getter and Setter are the only 2 places this should be, aside from the file scope...
	## global ACECOOL_CODE_MAPPING_SYSTEM_JOBS;
	## _jobs = ACECOOL_CODE_MAPPING_SYSTEM_JOBS;

	## ##
	## print( 'AddThreadJobs has been called! -> ' + str( _job ) );

	## ##
	## ## GetThreadJobs( ).append( _job );
	## ## ACECOOL_CODE_MAPPING_SYSTEM_JOBS =
	## ACECOOL_CODE_MAPPING_SYSTEM_JOBS.append( _job );


	## ##
	## print( 'AddThreadJobs has appended the job! -> ' + str( GetThreadJobs( ) ) );
	## print( 'AddThreadJobs has appended the job! 2-> ' + str( ACECOOL_CODE_MAPPING_SYSTEM_JOBS ) );
	## ## SetThreadJobs(  );

	##
	ACMS( ).GetThread( ).AddJobs( _job );
	print( 'AddThreadJobs has appended the job! -> ' + str( _job ) );
	print( 'Total Jobs! -> ' + str( ACMS( ).GetThread( ).Jobs( ) ) );
	## pass


##
##
##
class StopThread( StopIteration ):
    pass;


global ACMS_WORKER_THREAD
global ACMS_WORKER_THREAD_JOBS
ACMS_WORKER_THREAD			= None;
ACMS_WORKER_THREAD_JOBS		= [ ];

##
##
##
def ProcessThreadJob( _cmd = None, _dict = None ):
	pass


## ##
## ##
## ##
## def ACMSWorkerThreadJob( self, _sema, _lock, _jobs ):
## 	## global ACMS_WORKER_THREAD
## 	## global ACMS_WORKER_THREAD_JOBS



## 	##
## 	try:
## 		with _lock:
## 			for _job in _jobs:
## 				##
## 				_view					= None;
## 				_cmd					= _job.get( 'cmd', 'acms_preprocess_view' );
## 				_view_id				= _job.get( 'view_id', None );

## 				##
## 				_force_nocache_remap	= _job.get( 'force_nocache_remap', False );
## 				_preprocessor_only		= _job.get( 'preprocessor_only', False );

## 				##
## 				if ( _view_id != None ):
## 					_view = sublime.View( _view_id );

## 				##
## 				_view.run_command( _cmd, _job );
## 				print( 'Threading: ' + _cmd, str( _job ) );
## 	finally:
## 		_sema.release( );

## 	time.sleep( 1.0 );


## ##
## ##
## ##
## def ProcessThreadJob( _cmd = None, _dict = None ):
## 	global ACMS_WORKER_THREAD
## 	global ACMS_WORKER_THREAD_JOBS

## 	## ##
## 	_sema		= threading.Semaphore( 10 );
## 	_lock		= threading.Lock( );
## 	## _thread		= None;

## 	ACMS_WORKER_THREAD_JOBS = ACMS_WORKER_THREAD_JOBS.insert( 0, { 'cmd': _cmd, 'view_id': _dict.get( 'view_id', None ), 'force_nocache_remap': _dict.get( 'force_nocache_remap', False ), 'preprocessor_only': _dict.get( 'preprocessor_only', False )  } )

## 	##
## 	if ( ACMS_WORKER_THREAD == None ):
## 		ACMS_WORKER_THREAD = threading.Thread( target = lambda: ACMSWorkerThreadJob( ACMS_WORKER_THREAD, _sema, _lock, ACMS_WORKER_THREAD_JOBS ) );
## 		ACMS_WORKER_THREAD.start( );


## 	## _thread. );
## 	## _thread.start( );


## 	## 	try:
## 	## 		logging.info( "see: look at %d", _num );

## 	## 		with _seenlock:
## 	## 			if _num in _seen:
## 	## 				# this should be unreachable if each thread processes a unique number
## 	## 				logging.error( "see: already saw %d", _num );
## 	## 			else:
## 	## 				_seen.append( _num );

## 	## 		time.sleep( 0.3 );

## 	## 	finally:
## 	## 		_sema.release( );


##
## Sets the Plugin Object
##
def GetThreadJobsCount( ):
	## ## The Getter and Setter are the only 2 places this should be, aside from the file scope...
	## global ACECOOL_CODE_MAPPING_SYSTEM_JOBS;

	## ##
	## print( 'GetThreadJobsCount has been called! -> ' + str( len( GetThreadJobs( ) )	) );

	## ##
	## ## return len( GetThreadJobs( ) );
	## return len( ACECOOL_CODE_MAPPING_SYSTEM_JOBS );

	##
	return ACMS( ).GetThread( ).JobsCount( );


##
## Sets the Plugin Object
##
def PopThreadJobs( _index = None ):
	## ## The Getter and Setter are the only 2 places this should be, aside from the file scope...
	## global ACECOOL_CODE_MAPPING_SYSTEM_JOBS;
	## ## _jobs = ACECOOL_CODE_MAPPING_SYSTEM_JOBS;

	## ##
	## print( 'PopThreadJobs has been called! -> X' );

	## ##
	## ## return GetThreadJobs( ).pop( ( GetThreadJobsCount( ) - 1, _index )[ _index != None and AcecoolLib.isinteger( _index ) ] );
	## _job = ACECOOL_CODE_MAPPING_SYSTEM_JOBS.pop( ( GetThreadJobsCount( ) - 1, _index )[ _index != None and AcecoolLib.isinteger( _index ) ] );
	## ## SetThreadJobs( ACECOOL_CODE_MAPPING_SYSTEM_JOBS );


	## return _job

	##
	return ACMS( ).GetThread( ).PopJobs( );

## ##
## ## Threading System - This should behave like my GMod hooks... When the first item is added to a list, create the thread to process the data... When the last one has been processed, remove the Thread... This would be a successful threading system..
## ## First, I'd like to find out why the threading.Thread isn't working properly...
## ##
## class ThreadingSystemBase( threading.Thread ):
## 	pass
## class ThreadingSystem( ThreadingSystemBase ):
## 	##
## 	##
## 	##
## 	def HandleJob( self ):
## 		print( '\tThread Job is Processing: ' );


## 	##
## 	##
## 	##
## 	def run( self ):
## 		global ACMS_Thread_Worker_Jobs
## 		print( 'Thread is executing...' );
## 		print( 'Thread is executing...' );
## 		print( 'Thread is executing...' );
## 		print( 'Thread is executing...' );
## 		print( 'Thread is executing...' );
## 		print( 'Thread is executing...' );
## 		print( 'Thread is executing...' );
## 		time.sleep( 1.1 );

## 		while True:
## 			## print( str( ACMS_Thread_Worker_Jobs ) );
## 			time.sleep( 1.1 );

## 			if ( len( ACMS_Thread_Worker_Jobs ) > 0 ):
## 				print( 'Processing Jobs: ' )
## 				while len( ACMS_Thread_Worker_Jobs ) > 0:
## 					_job = ACMS_Thread_Worker_Jobs.pop( );
## 					## del ACMS_Thread_Worker_Jobs[ : ]

## 					print( _job );

## 					## Grab the view we're targeting... Could also be from file-name instead if we do project-wide preprocessing instead of opening all files in views, we target files...
## 					_view_id	= _job.get( 'view_id', None );
## 					_view		= None;

## 					if ( _view_id != None ):
## 						_view		= sublime.View( _view_id );

## 					##
## 					_type		= _job.get( 'type', None );

## 					## ## Does this job require us to remap the file?
## 					## _file		= _job.get( 'file', None );
## 					## _path		= _job.get( 'path', None );

## 					## Does this job require us to remap the file?
## 					_remap		= True if ( _job.get( 'map', True ) or _job.get( 'force_nocache_remap', True ) ) else False;

## 					## Should we update the output panel?
## 					_update		= True if ( _job.get( 'output', False ) or _job.get( 'preprocessor_only', False ) ) else False;

## 					## _f = AcecoolLib.Sublime.GetViewFileName( _view, False )
## 					_p = AcecoolLib.Sublime.GetViewFileName( _view, True )
## 					## _acms_panel = _f.strip( ) == ACMS( ).GetPanelName( ).strip( )

## 					_view.run_command( 'acms_map_file', { 'file': _p, 'force_nocache_remap': _remap, 'preprocessor_only': _update } )

## 					## _view.run_command( 'acms_preprocess_view', { 'view_id': _view_id, 'force_nocache_remap': _remap, 'preprocessor_only': _update } )

## 					time.sleep( 1.0 );
## 			## else:
## 			## 	print( 'Waiting for jobs...' );
## 			## 	time.sleep( 2.5 );


## global ACMS_Thread_Worker
## ACMS_Thread_Worker = None

## global ACMS_Thread_Worker_Jobs
## ACMS_Thread_Worker_Jobs = None
## def AddThreadJobs( _job ):
## 	global ACMS_Thread_Worker_Jobs

## 	if ( ACMS_Thread_Worker_Jobs == None ):
## 		ACMS_Thread_Worker_Jobs = [ ]

## 	print( 'AddThreadJobs has appended the job! -> ' + str( _job ) );
## 	print( 'Total Jobs! -> ' + str( ACMS_Thread_Worker_Jobs ) );
## 	ACMS_Thread_Worker_Jobs.append( _job )

##
##
##
## __jobs			= AcecoolLib.AccessorFuncBase( parent = ACMSThreadBase,		key = 'jobs',			name = 'Jobs',					default = [ ],			group = '',			getter_prefix = '',				documentation = 'Jobs',			allowed_types = ( AcecoolLib.TYPE_LIST ),								allowed_values = AcecoolLib.VALUE_ANY																																	)
## __Thread		= AcecoolLib.AccessorFuncBase( parent = ACMSThreadBase,		key = 'Thread',			name = 'Thread',				default = None,			group = '',			getter_prefix = 'Get',			documentation = 'Thread',		allowed_types = ( AcecoolLib.TYPE_ANY, type( threading.Thread ) ),		allowed_values = AcecoolLib.VALUE_ANY																																	)

##
## jobs = None;
## lock = None;

## 'get': ( lambda this: this.GetThread( ).GetJobs( ) ),
## 'len': ( lambda this: this.GetThread( ).GetJobsCount( ) ),
## 'set': ( lambda this, _jobs: this.GetThread( ).SetJobs( _jobs ) ),
## 'add': ( lambda this, *_jobs: this.GetThread( ).AddJobs( *_jobs ) ),
## 'pop': ( lambda this, _index = None, _count = 1: this.GetThread( ).PopJobs( _index, _count ) )


## __thread_job_helpers			= AcecoolLib.AccessorFuncBase( parent = ACMSThreadBase,		key = 'thread_job_helpers',				name = 'ThreadJobs',					default = None,																				group = '',			getter_prefix = 'Get',			documentation = 'Thread Job Helpers',			allowed_types = ( AcecoolLib.TYPE_ANY ),							allowed_values = AcecoolLib.VALUE_ANY,					setup = {
## 	## Note: This was a temp solution to try a few things out - the AccessorFuncSystem is fully capable of handling these jobs without redirecting them but I was having a strange bug I thought was related to def x( a = [ ] ): a.append( 'x' ); x( ), x( ), x( ) with a == [ 'x', 'x', 'x' ] because lists when defined in functions retain in memory...
## 	'get': ( lambda this: this.GetJobs( ) ),
## 	'len': ( lambda this: this.GetJobsCount( ) ),
## 	'set': ( lambda this, _jobs: this.SetJobs( _jobs ) ),
## 	'add': ( lambda this, *_jobs: this.AddJobs( *_jobs ) ),
## 	'pop': ( lambda this, _index = None, _count = 1: this.PopJobs( _index, _count ) )
## } )

## ##
## def Jobs( self ):
## 	return self.GetJobs( );

## ##
## def GetJobs( self ):
## 	## _lock = threading.Lock( );
## 	## _lock = self.lock;

## 	## _lock.acquire( );

## 	## if ( not self.jobs ):
## 	## 	self.jobs = [ ];

## 	_jobs = self.jobs;
## 	## _lock.release( );

## 	return _jobs;

## ##
## def SetJobs( self, _jobs ):
## 	## _lock = threading.Lock( );
## 	## _lock = self.lock;
## 	## _lock.acquire( );
## 	self.jobs = _jobs;
## 	## _lock.release( );

## ##
## def AddJobs( self, *_jobs ):
## 	## _lock = threading.Lock( );
## 	## _lock = self.lock;
## 	## _lock.acquire( );

## 	for _job in _jobs:
## 		self.GetJobs( ).append( _job );

## 	## _lock.release( );


## ##
## def GetJobsCount( self ):
## 	return len( self.GetJobs( ) );

## ##
## def PopJobs( self, _index = None, _count = 1 ):
## 	## _lock = self.lock;
## 	## _lock = threading.Lock( );
## 	## _lock.acquire( );
## 	_job = self.GetJobs( ).pop( ( ( 0, self.GetJobsCount( ) - 1 )[ _index == None ] ) );

## 	## _lock.release( );
## 	## del self.GetJobs( )[ : ];

## 	return _job;


## ##
## ##
## ##
## def __init__( self ):
## 	## _lock = threading.Lock( );
## 	## _lock.acquire( );
## 	## self.jobs = [ ];
## 	## _lock.release( );
## 	##self.lock = threading.Lock( );

## 	super( ).__init__( );


## If we have any jobs, process them...
## _jobs = self.Jobs( );
## self.SetJobs( [ ] );
## _jobs	= self.Jobs( );
## _len	= len( _jobs );


## The Getter and Setter are the only 2 places this should be, aside from the file scope...
## global ACECOOL_CODE_MAPPING_SYSTEM_JOBS;

##
## _lock = threading.Lock( );



## _lock.acquire( );
## _jobs = self.Jobs( );

## Always pop the last one added - so if we switch to a new panel we can update the output immediately..
## _job = self.Jobs( ).pop( );
## self.SetJobs( self.Jobs( ) );
## _job = PopThreadJobs( 0 );
## self.SetJobs( self.Jobs( ) );
## _lock.release( );

##
## self.jobs = _jobs

## print( 'Jobs - popped one so... - count: ' + str( len( self.Jobs( ) ) ) );

## else:
## 	print( 'There are no unprocessed jobs...' );


## ##
## if ( _type and _type == 'UpdateAllOpen' ):
## 	##
## 	##
## 	##
## 	def UpdateAllOpenViews( ):
## 		##
## 		if ( ACMS( ).GetSetting( 'use_caching_system_preprocessor', False ) ):
## 			print( 'ACMS -> PreProcessing all views in all windows...' )

## 			##
## 			_active_window = sublime.active_window( )
## 			_window = _active_window

## 			for _i, _v in enumerate( _active_window.views( ) ):
## 				##
## 				if ( _i >= 10 ):
## 					break;

## 				## for _v in _active_window.views( ):

## 			## for _window in sublime.windows( ):
## 				## print( 'ACMS -> Processing Window: ' + str( _window.id( ) ) )
## 				## for _v in _window.views( ):
## 				else:
## 					_f = AcecoolLib.Sublime.GetViewFileName( _v, False )
## 					_p = AcecoolLib.Sublime.GetViewFileName( _v, True )
## 					_acms_panel = _f.strip( ) == ACMS( ).GetPanelName( ).strip( )

## 					if ( not _acms_panel ):
## 						## print( 'ACMS -> Processing Window: ' + str( _window.id( ) ) + ' View: ' + str( _v.id( ) ) + ' -- File: ' + str( AcecoolLib.Sublime.GetViewFileName( _v, False ) ) )

## 						## if ( _whitelist.get( _f, False ) ):
## 						## _targeted_view = _v
## 						## print( 'ACMS -> Processing Window: ' + str( _window.id( ) ) + ' View: ' + str( _v.id( ) ) + ' -- File: ' + str( AcecoolLib.Sublime.GetViewFileName( _v, False ) ) )
## 						## sublime.set_timeout_async( lambda: self._instance.ExecuteCodeMappingSystem( _targeted_view, True, True ), 10 )
## 						## self._instance.ExecuteCodeMappingSystem( _v, True, True )
## 						_v.run_command( 'acms_map_file', { 'file': _p, 'force_nocache_remap': True, 'preprocessor_only': True } )
## 						## sublime.set_timeout_async( lambda: self._instance.ExecuteCodeMappingSystem( _v, True, True ), 0 );
## 					## else:
## 					## 	print( 'ACMS -> Processing Window: ' + str( _window.id( ) ) + ' View: ' + str( _v.id( ) ) + ' -- Can not process this id, because it is the ACMS Panel file... ' + _f + ' is ' + str( _acms_panel ) )

## 				## ## Set up a delay betwen files being processed
## 				## _delay = ACMS( ).GetSetting( 'use_caching_system_preprocessor_delay', 0 )

## 				## ## If we have a delay, then use it - sleep for that duration.
## 				## if ( _delay > 0 ):
## 				## 	time.sleep( _delay );


## 		## If we updated a core file, and we have our caching preprocessor enabled for all project files, we need to reprocess every project file ( extra rules apply, make sure we've opened at least once, etc.. with higher activation and higher save files first )...
## 		if ( ACMS( ).GetSetting( 'use_caching_system_project_preprocessor', False ) ):
## 			pass


## 	UpdateAllOpenViews( );
## 	## acms_async_handler.invoke_async( lambda: UpdateAllOpenViews( ) )

## else:
## 	##
## 	if ( not _view ):
## 		## if ( _file ):
## 		## 	print( 'Processing by _file' );
## 		## 	sublime.active_window( ).active_view( ).run_command( 'acms_map_file', { 'file': _file, 'force_nocache_remap': _remap, 'preprocessor_only': _update } )

## 		_p = AcecoolLib.Sublime.GetViewFileName( _view, True )
## 		if ( _p ):
## 			print( 'Processing by _path' );
## 			sublime.active_window( ).active_view( ).run_command( 'acms_map_file', { 'file': _p, 'force_nocache_remap': _remap, 'preprocessor_only': _update } )
## 		if ( _path ):
## 			print( 'Processing by _path' );
## 			sublime.active_window( ).active_view( ).run_command( 'acms_map_file', { 'file': _path, 'force_nocache_remap': _remap, 'preprocessor_only': _update } )
## 	else:
## 		##
## 		if ( AcecoolLib.Sublime.IsValidView( _view ) ):
## 			print( 'Processing by _view / _view_id' );
## 			##
## 			## sublime.set_timeout_async( lambda: self.ExecuteCodeMappingSystem( _view, False ), 10 )
## 			## sublime.set_timeout( lambda: self.ExecuteCodeMappingSystem( _view, False ), 0 )
## 			## sublime.set_timeout_async( lambda: _view.run_command( 'acms_preprocess_view', { 'view_id': _view.id( ), 'force_nocache_remap': False, 'preprocessor_only': False } ), 100 )
## 			_p = AcecoolLib.Sublime.GetViewFileName( _view, True )
## 			## _view.run_command( 'acms_map_file', { 'file': _p, 'force_nocache_remap': _remap, 'preprocessor_only': _update } )
## 			if ( _path ):
## 				print( 'Processing by _path' );
## 				sublime.active_window( ).active_view( ).run_command( 'acms_map_file', { 'file': _path, 'force_nocache_remap': _remap, 'preprocessor_only': _update } )
## 			## _view.run_command( 'acms_preprocess_view', { 'view_id': _view_id, 'force_nocache_remap': _remap, 'preprocessor_only': _update } )
## 			## _view.run_command( 'acms_map_file', { 'file': _path, 'force_nocache_remap': True, 'preprocessor_only': True } )


##

## for _job in _jobs:
## while ( len( self.Jobs( ) ) > 0 ):

## _file		= _job.get( 'file', AcecoolLib.Sublime.GetViewFileName( _view, False ) );

## _jobs	= self.Jobs( );
