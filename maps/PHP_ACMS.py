##
## PHP - Acecool Content Management System - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent ACMS Mapper - As this is the User mapper, I've set up all possible includes for this mapper out!
from AcecoolCodeMappingSystem.maps.PHP import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Everything you need to create your own - The base class which very limited structure, the CFG_ options above and the ENUM value to use to set to it... Simply rename them...
##
## class ACMS_PHP_ACMS( ACMS_PHP_Default ):
## class ACMS_PHP_ACMS( XCodeMapperBase ):
class ACMS_PHP_ACMS( ACMS_PHP_Default ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_PHP_ACMS'




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------