##
## Plain Text - Default / Vanilla / Base Mapper - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task: Set up Sublime Text Symbols Mapper as alternative to file-parsing... OR precursor so the default can use symbols and other mappers can add on to that - or I'll add an additional layer...
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Base Mapper Imports
from AcecoolCodeMappingSystem.maps.__mapper_imports__ import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##	Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
class ACMS_PlainText_Default( ACMS_Text_Default ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_PlainText_Default'


	##
	## Initialize the object - we're adding raw_output_data to the object and that's all - we call super because we want all of the other features enabled ( Caching, etc.. ) without the category system being used...
	##
	def __init__( self ):
		## Contains the output data for this override...
		self.raw_output_data = ''

		## Fallthrough to the parent so their syntax can be added to our own...
		super( ).__init__( )


	##
	## Callback - Called for each line in the file - can also prevent entries from being added ( when found by AddSyntax method ) by returning a boolean. Can also alter entries code ( by returning a string - will only alter if the code being returned was going to be added to a category - otherwise the line is ignored anyway )
	## Note: This is a simple callback so basic info is available only such as the line-number, the code for that line, and the depth of any contents of that line. It is called regardless of comment-status so comment-status is also provided..
	##
	def OnProcessLine( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		## Format the line how we want it to appear... In this case "<LineNumber>. <LineData>"... so 1. Hello World
		_code_entry = str( _line_number ) + '. ' + _code

		## Only add a new-line when it's needed... ie after the first entry..
		if ( self.raw_output_data != '' ):
			self.raw_output_data = self.raw_output_data + self.GetSetting( 'output_new_line_chars', '\n' )

		## Add the data...
		self.raw_output_data = self.raw_output_data + _code_entry

		## return True to prevent any categorized entries from being used - this is the simple mapper for a reason...
		return True


	##
	## Override the back-end data-processing system - this means instead of using the default system, this will prevent anything from AddSyntax, etc.. from being used - the only text being output is that in __output_data__.. this is essentially a devolution of the system...
	##
	def ProcessOutput( self, _file, _count, _entries ):
		return self.raw_output_data


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------