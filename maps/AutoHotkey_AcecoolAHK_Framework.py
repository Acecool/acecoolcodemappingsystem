##
## AutoHotkey - AcecoolAHK_Framework Mapper - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent ACMS Mapper - As this is the User mapper, I've set up all possible includes for this mapper out!
## from AcecoolCodeMappingSystem.maps.AutoHotkey import *
from AcecoolCodeMappingSystem.maps.AutoHotkey_User import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Everything you need to create your own - The base class which very limited structure, the CFG_ options above and the ENUM value to use to set to it... Simply rename them...
##
## class ACMS_AutoHotkey_Acecool( ACMS_AutoHotkey_Default ):
## class ACMS_AutoHotkey_Acecool( XCodeMapperBase ):
class ACMS_AutoHotkey_AcecoolAHK_Framework( ACMS_AutoHotkey_User ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_AutoHotkey_Acecool'




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------