##
## Batch File - Source Engine Dedicated Server Mapper - <Author Name>
##


##
## Task List - Proper User Mapper Creation and / or Modification Procedure
##
## Task: Copy this file to Packages\User\AcecoolCodeMappingSystem\maps\LanguageName_MapperName.py
## Task: Make a plan or design of what you want to accomplish with this mapper...
## Task: Modify this mapper...
## Task: If you need more tools at your disposal or find an issue: Submit bug reports, feature requests or constructive criticism!
## Task: When you've finished with this mapper for your project, or for a language - Release it?
## Note: If you want to, you may submit it on my repo and if I like it, I will add it and it will retain your author name, site info, other info you want to include, etc... I may ask you to make changes before accepting it, or I may make the changes myself if I find any bugs, etc..
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent ACMS Mapper - As this is the User mapper, I've set up all possible includes for this mapper out!
from AcecoolCodeMappingSystem.maps.BatchFile_User import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Everything you need to create your own - The base class which very limited structure, the CFG_ options above and the ENUM value to use to set to it... Simply rename them...
##
## class ACMS_BatchFile_User( ACMS_BatchFile_Default ):
## class ACMS_BatchFile_User( XCodeMapperBase ):
class ACMS_BatchFile_SRCDS( ACMS_BatchFile_User ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_BatchFile_SRCDS'


	##
	## Callback - Used to setup rules to track code, expand AccessorFuncs, etc.. at to display the output in the Code - Map Panel. Priority is handled using standard English Reading direction ie using self.AddSyntax( COMMENT, STARTSWITH, '#', '##' ) means ## would never be handled.
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' )

		## Comments
		self.AddSyntax( CATEGORY_COMMENT, SEARCH_TYPE_STARTSWITH, 'rem', 'REM' )

		## Functions and Class
		self.AddSyntax( CATEGORY_FUNCTION_ACCESSOR, SEARCH_TYPE_CONTAINS, 'srcds.exe' )


		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )


	##
	## Callback - Used to convert found AccessorFunc Calls into Multiple Entries in XCodeMapper so the Getter / Setter and other functions created by the AccessorFunc call are added to the Map rather than the call.
	##
	def OnAccessorFuncExpansion( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):

		##
		##
		if ( _pattern == 'srcds.exe' ):
			## Grab all of the keys / values - splitting is complicated since there are + and - commands...
			## Purpose is to give a quick overview of everything - workshop, workshop id ( eventually with a link to Steam Workshop URL ), master list info, and more...
			## All under INFO Category without having to use wordwrap or scroll or anything...
			## self.AddEntry( CATEGORY_INFO, _line_number, 'Exec SRCDS: ' + _code.expandtabs( self.GetSetting( 'file_tab_size', 4 ) ), _depth, _mode, _pattern )

			##
			_srcds			= 'srcds.exe'
			_srcds_len		= len( 'srcds.exe' )
			_srcds_start	= _code.find( _srcds )
			_code_args		= _code[ _srcds_start + _srcds_len : ].strip( ).split( ' ' )

			## _code_args		= _code[ _srcds_start + _srcds_len : ].strip( ).split( ' ' )
			## _args			= [ ' '.join( _code_args[ _i : _i + 2 ] ) for _i in range( 0, len( _code_args ), 2 ) ]
			## self.AddEntry( CATEGORY_INFO, _line_number, str( _code_args ), _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_INFO, _line_number, AcecoolLib.String.FormatColumn( 30, 'Execute SRCDS: ' ) + _code[ : _srcds_start + _srcds_len ], _depth, _mode, _pattern )

			_len_args = len( _code_args )
			## self.AddEntry( CATEGORY_INFO, _line_number, str( _len_args ), _depth, _mode, _pattern )
			## self.AddEntry( CATEGORY_INFO, _line_number, str( _code_args[ 19 ] ), _depth, _mode, _pattern )
			## self.AddEntry( CATEGORY_INFO, _line_number, str( _code_args[ 20 ] ), _depth, _mode, _pattern )
			for _i, _arg in enumerate( _code_args ):
				_is_cmd = ( _arg.startswith( '+' ) or _arg.startswith( '-' ) )
				_has_next = _i + 1 < _len_args

				_next = ''
				if ( _has_next ):
					_next = _code_args[ _i + 1 ]

				_next_is_cmd = ( _has_next and ( _next.startswith( '+' ) or _next.startswith( '-' ) ) )

				if ( _is_cmd ):
					if ( _has_next and not _next_is_cmd ):
						self.AddEntry( CATEGORY_INFO, _line_number, AcecoolLib.String.FormatColumn( 30, _arg ) + '' + _next, _depth, _mode, _pattern )
					else:
						self.AddEntry( CATEGORY_INFO, _line_number, AcecoolLib.String.FormatColumn( 30, _arg ) + 'Enabled', _depth, _mode, _pattern )



			## for _i, _arg in enumerate( _args ):
			## 	_components	= _arg.split( ' ' )
			## 	_len		= len( _components )
			## 	_key		= _components[ 0 ]

			## 	if ( _key.startswith( '+' ) or _key.startswith( '-' ) ):
			## 		if ( _len == 2 ):
			## 			_value		= _components[ 1 ]

			## 			self.AddEntry( CATEGORY_INFO, _line_number, _key + ': ' + _value, _depth, _mode, _pattern )
			## 		elif ( _len == 1 ):
			## 			self.AddEntry( CATEGORY_INFO, _line_number, _key + ': Enabled', _depth, _mode, _pattern )
			## 		else:
			## 			for _ii, _x in enumerate( _components ):
			## 				if ( _ii > 1 ):
			## 					if ( len( _args ) <= _i + 1 ):
			## 						_args.append( _x )
			## 					else:
			## 						_args[ _i + 1 ] = _x + ' ' + _args[ _i + 1 ]

			## 	else:
			## 		if ( len( _args ) <= _i + 1 ):
			## 			_args.append( _key )
			## 		else:
			## 			_args[ _i + 1 ] = _args[ _i + 1 ] + ' ' + _key
			## 		## _args.insert( _i, _key )




			return True



##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------