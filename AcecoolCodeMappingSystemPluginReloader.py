##
## Acecool Code Mapping System Main Plugin Auto-Refresh System - a Stand-Alone Plugin for Sublime Text 3 which minimizes wasted time by scrolling, looking for classes, definitions, functions, and more.. while adding easy ways to include targeted information in the output panel to further improve coding efficiency - Josh 'Acecool' Moser
##


##
## Imports
##

##
import sublime, sublime_plugin

##
import time
import os
import subprocess
import threading

## Commands
from AcecoolCodeMappingSystem.AcecoolCodeMappingSystemPlugin import *
from AcecoolCodeMappingSystem.AcecoolCodeMappingSystemCommands import *
from AcecoolCodeMappingSystem.edit_settings_plus import *
## from AcecoolCodeMappingSystem.commands.acecool_convert_spaces_to_tabs import *
## from AcecoolCodeMappingSystem.commands.acms_cmd_open_all_menus import *
## from AcecoolCodeMappingSystem.commands.acms_menu_settings_open_keybinds import *
## from AcecoolCodeMappingSystem.commands.acms_menu_settings_open_plugin_prefs import *
## from AcecoolCodeMappingSystem.commands.acms_menu_settings_open_sublime_prefs import *
## from AcecoolCodeMappingSystem.commands.acms_menu_settings_open_syntax import *
## from AcecoolCodeMappingSystem.commands.edit_settings_plus import *
## from AcecoolCodeMappingSystem.commands.toggle_acecool_code_mapping_system_panel import *





##
## Event Listeners
##


##
## This Event Listener is specifically designed to maintain the plugin itself - it is used to reload / re-process the mapping panel output when files it is mapping have changed, etc...
##
class AcecoolCodeMappingSystemEventListener( sublime_plugin.EventListener ):
	##
	##
	##

	##
	__debug_mode__	= False
	__debugging__	= '' # 'on_text_command'

	## Tracks whether a core file was saved when switching between windows - if so, it means the current file needs to be re-cached..
	core_file_saved	= False

	## Commands which are ignored ( For output - or so we can get more information on when these occur so we don't need to wade through the stuff already known )
	__ignored_text_cmds__ = {
		## Called on CTRL + V
		'paste': True,

		## Called on CTRL + C
		'copy': True,

		## Called when Arrow keys are used to move through text
		'move': True,

		##
		## 'move_to': True,

		## Called when Insert key is pressed
		'toggle_overwrite': True,

		## Called when Backspace button is pressed
		'left_delete': True,

		## Called when Delete button is pressed
		'right_delete': True,

		## Called on Left Mouse DOWN
		'drag_select': True,

		## Called when a word is clicked - it doesn't need to be highlighted...
		'word_highlight_click': True,

		##
		## 'insert': True,

		## Called when tab or auto-complete is used to insert word / replace word while typing... Also called when tab and only tab is pressed
		'insert_best_completion': True,

		## Called when indent doesn't indent just once - ie when it goes to the next proper indentation slot
		'reindent': True,

		## Called on CTRL + Q
		'toggle_comment': True,

		##
		'hide_auto_complete': True,

		## Called on right-click when context menu opens...
		'context_menu': True,

		## Called during arrows being used, etc...
		'on_query_context': True
	}




	##
	## Task: Transfer these 2 helpers into the Plugin Core... Or create a global for IsPluginActive then it's easy to check debugging in the plugin if active...
	##


	##
	## Hack - When Sublime Text starts up and the user clicks on a panel such as ACMS or anywhere - the Source Panel is NOT SET ( maybe because of active view, window? or some other reason - Task is to find the root cause to get rid of this hack ) - this hack makes sure it is valid.. the bright side is this only needs to run once after start-up if the user jumps right to double-clicking... then never again...
	## Task: Figure out the root cause why clicking on a panel which doesn't trigger resetting anything else would reset GetSourceView when the rest of the code tells us that it is properly found and set..... and not reset or deleted anywhere...
	##
	def DetectSourceViewHack( self ):
		## print( '1 ACMS > SOURCE VIEW ' + str( GetPluginObject( ).GetSourceView( ) ) )
		## Make sure the Sublime Source View is set so we know what to navigate to...
		if ( not IsValidSublimeView( GetPluginObject( ).GetSourceView( ) ) ):
			##
			## print( 'ACMS > Hack active! Source View is set as None because user jumped right into clicking on things before Sublime Text was fully loaded... This hack ensures the Source View is set on double-click even though the rest of the code shows that it was set, and it was not reset anywhere else - if the user clicks nothing during start up then it works properly... maybe because the actual view is focused? Start there...' )

			## Grab the full path and basic filename for the source view from the RunTime config file...
			_path = GetPluginObject( ).GetSetting( 'source_path', 'untitled' );
			_file = GetPluginObject( ).GetSetting( 'source_filename', 'untitled' );

			## See if the view can be found using the path or the filename...
			_view_path = AcecoolLib.Sublime.FindView( _path );
			_view_file = AcecoolLib.Sublime.FindView( _file );

			## Make sure it's valid...
			## _view_src = TernaryFunc( IsValidSublimeView( _view_path ) and _view_path.file_name( ) != None and _view_path.file_name( ) != '', _view_path, TernaryFunc( IsValidSublimeView( _view_file ) and _view_file.file_name( ) != None and _view_file.file_name( ) != '', _view_file, None ) )
			_view_src = TernaryFunc( IsValidSublimeView( _view_path ), _view_path, TernaryFunc( IsValidSublimeView( _view_file ), _view_file, None ) );

			## Reset the View Info for Source View...
			GetPluginObject( ).ResetViewInfo( _view_src, True );


	##
	##
	##
	def IsTextCommandIgnored( self, _cmd = '' ):
		## print( 'ACMS -> EventListener -> IsTextCommandIgnored' );

		## If the command is ignored - we ignore it...
		if ( self.__ignored_text_cmds__.get( _cmd, None ) != None ):
			return True;

		## Otherwise we don't...
		return False;


	##
	## Helper - Returns whether or not the plugin is active
	##
	def IsPluginActive( self ):
		## If the plugin object isn't valid or the output view doesn't exist then the plugin isn't running...
		if ( not IsPluginObjectValid( ) or not IsValidSublimeView( GetPluginObject( ).GetOutputView( ) ) ):
			return False;

		## Otherwise we'll assume it is because the plugin object IS valid and the output view does exist...
		return True;


	##
	## helper - Returns whether or not the plugin is in debugging mode...
	##
	def IsPluginTaskDebuggerActive( self, _task = '' ):
		## If the plugin isn't active, we don't want to debug anything plugin related...
		if ( not self.IsPluginActive( ) ):
			return False;

		## Helpers
		_config_mode	= GetPluginObject( ).GetSetting( 'debugging_mode', False );
		_config_task	= GetPluginObject( ).GetSetting( 'debugging_task', '' );

		## The plugin is active, lets see if debugging is enabled and the set task is '' or non blank
		if ( _config_mode != False and ( _config_task == '' or ( _task != '' and _config_task == _task ) ) ):
			return True;

		## Otherwise it isn't in debug mode..
		return False;


	##
	##
	##
	def Notify( self, _text = '', _display_print = True ):
		##
		_text = '[ ACMS.PluginEventsManager.Notification ] {text}'.format( text = _text );

		##
		if ( _display_print	):
			print( _text );

		##
		return sublime.status_message( _text );


	##
	## Returns True if Single-Click and Release was Detected
	##
	def UseSingleClick( self ):
		return GetPluginObject( ).GetSetting( 'plugin_control_use_single_click_jump_to', False );


	##
	## Returns True if Single-Click and Release was Detected
	##
	def UseDoubleClick( self ):
		return not self.UseSingleClick( );


	##
	## Returns True if Single-Click and Release was Detected
	##
	def HasSingleClicked( self, _cmd, _args = None ):
		print( 'HasSingleClicked: ' + str( ( self.UseSingleClick( ) and ( ( _cmd == 'word_highlight_click' ) or ( _cmd == 'drag_select' and _args.get( 'by', None ) == None ) ) ) ) );
		print( 'HasSingleClicked: ' + str( _cmd == 'word_highlight_click' ) );
		print( 'HasSingleClicked: ' + str( ( _cmd == 'drag_select' and _args.get( 'by', None ) == None ) ) );
		print( 'HasSingleClicked: ' + str( _args.get( 'by', None ) ) );
		return ( self.UseSingleClick( ) and ( ( _cmd == 'word_highlight_click' ) or ( _cmd == 'drag_select' and _args.get( 'by', None ) == None ) ) );


	##
	## Returns True if Double-Click was Detected...
	## ( not _use_single_click and _cmd == 'drag_select' and _args.get( 'by', '' ) == 'words' )
	##
	def HasDoubleClicked( self, _cmd, _args = None ):
		print( 'HasDoubleClicked: ' + str( ( self.UseSingleClick( ) and ( ( _cmd == 'word_highlight_click' ) or ( _cmd == 'drag_select' and _args.get( 'by', None ) == None ) ) ) ) );
		return ( self.UseDoubleClick( ) and _cmd == 'drag_select' and _args.get( 'by', '' ) == 'words' );


	##
	## Task: If active and the file hasn't changed, reset all globals, if close group when empty is set to true then reset the layout ( hide the panel ) and refocus the source-code view...
	##

	##
	## Callback - Called when a file / view is closed...
	##
	## Task: Detect if the tab was closed using close button or middle click vs command... If closed using middle click or X then create untitled file to act as symbolic sacrifice until bug is fixed... Setting scratch to false works when using the command, maybe use a pre close function?
	## Note: Altered to ASYNC
	def on_close( self, _view ):
		##
		## print( 'ACMS -> EventListener -> on_close' )

		##
		_file			= AcecoolLib.Sublime.GetViewFileName( _view );
		_path			= AcecoolLib.Sublime.GetViewFileName( _view, True );
		if ( not IsValidSublimeView( _view ) ):
			return False;

		##
		if ( self.IsPluginTaskDebuggerActive( 'on_close' ) ):
			self.Notify( 'ACMS -> EventListener -> on_close :: ' + AcecoolLib.String.FormatColumn( 50, AcecoolLib.Sublime.GetViewFileName( _view ), 4, ' :: ', 100, str( _view ) ) );


	##
	## Callback - Called when a file / view is closed...
	##
	## Task: Detect if the tab was closed using close button or middle click vs command... If closed using middle click or X then create untitled file to act as symbolic sacrifice until bug is fixed... Setting scratch to false works when using the command, maybe use a pre close function?
	## Note: Altered to ASYNC
	## Note: Altered to on-pre-close...
	def on_pre_close( self, _view ):
		##
		## print( 'ACMS -> EventListener -> on_pre_close' );

		##
		_file			= AcecoolLib.Sublime.GetViewFileName( _view );
		_path			= AcecoolLib.Sublime.GetViewFileName( _view, True );
		if ( not IsValidSublimeView( _view ) ):
			return False;

		##
		if ( self.IsPluginTaskDebuggerActive( 'on_pre_close' ) ):
			self.Notify( 'ACMS -> EventListener -> on_pre_close :: ' + AcecoolLib.String.FormatColumn( 50, AcecoolLib.Sublime.GetViewFileName( _view ), 4, ' :: ', 100, str( _view ) ) );


		## Bug Fix - Called when ACMS - If Scratch is set to True, and it is in a group by itself, and the file is closed manually by the user - then it'll close the Scratch file, and the next file which takes focus... Ensuring Scratch is false corrects the issue...
		if ( _file == GetPluginObject( ).GetPanelName( ) ):
			## Bug Fix..
			AcecoolLib.Sublime.SetViewScratch( _view, False );

			## On Panel being closed - update...
			GetPluginObject( ).ResetViewInfo( None, False );


	##
	##
	##
	## If active - and the view file name has CHANGED then run the code to load the cached, or reprocess the file...
	## If it isn't active but the panel is open, then re-activate ( this is so on restart the plugin will reload )
	##
	## Note: Altered to ASYNC
	def on_load_async( self, _view ):
		##
		if ( self.IsPluginTaskDebuggerActive( 'on_load' ) ):
			if ( IsValidSublimeView( _view ) ):
				self.Notify( 'ACMS -> EventListener -> on_load :: ' + AcecoolLib.String.FormatColumn( 50, AcecoolLib.Sublime.GetViewFileName( _view, True ), 4, ' :: ', 100, str( _view ) ) );

		##
		_file = AcecoolLib.Sublime.GetViewFileName( _view );
		_path = AcecoolLib.Sublime.GetViewFileName( _view, True );
		if ( not GetPluginObject( ) or not IsValidSublimeView( _view ) or _path == '' ):
			return False;



	##
	##
	##
	## If active then stop all work... basically...
	##
	## Note: Altered to ASYNC
	def on_deactivated( self, _view ):
		##
		if ( self.IsPluginTaskDebuggerActive( 'on_deactivated' ) ):
			self.Notify( 'ACMS -> EventListener -> on_deactivated' );

		if ( not GetPluginObject( ) ):
			return False;


	##
	##
	##
	## Processing double-click - If active: double_click = command_name == 'drag_select' and 'by' in args and args['by'] == 'words' -- if double_click and view.file_name( ) is the file being mapped, then navigate to line - if not navigating through code then give focus to the source-code file, otherwise reset back to mapping view... return whether or not to highlight the entire line - all in lambda function from marshaller system
	##
	## Note: Altered to ASYNC
	def on_text_command( self, _view, _cmd, _args ):

		def JumpToLineHelper( ):
			## Make sure the Plugin Object is Valid and the _view is a valid Sublime Text View with a Valid File-Name...
			if ( not IsPluginObjectValid( ) or not IsValidSublimeView( _view ) ):
				return False;

			##
			_view_source		= GetPluginObject( ).GetSourceView( );

			## TernaryFunc( ( IsValidSublimeView( _view ) ), self.Notify( 'ACMS -> EventListener -> on_text_command :: ' + AcecoolLib.String.FormatColumn( 25, AcecoolLib.Sublime.GetViewFileName( _view ), 4, ' :: ', 50, str( _cmd ), 4, ' :: ', 100, str( _args ) ) ), self.Notify( 'ACMS -> EventListener -> on_text_command - _view ERROR :: ' + AcecoolLib.String.FormatColumn( 50, str( _cmd ), 4, ' :: ', 100, str( _args ) ) ) )
			if ( self.IsPluginTaskDebuggerActive( 'on_text_command' ) ):
				if ( IsValidSublimeView( _view ) ):
					self.Notify( 'ACMS -> EventListener -> on_text_command :: ' + AcecoolLib.String.FormatColumn( 25, AcecoolLib.Sublime.GetViewFileName( _view ), 4, ' :: ', 50, str( _cmd ), 4, ' :: ', 100, str( _args ) ) );
				else:
					self.Notify( 'ACMS -> EventListener -> on_text_command - _view ERROR :: ' + AcecoolLib.String.FormatColumn( 50, str( _cmd ), 4, ' :: ', 100, str( _args ) ) );


			##
			if ( _cmd == 'set_file_type' ):
				return GetPluginObject( ).GetSyntaxSettingsObject( _args.get( 'syntax', None ) );

			##
			if ( not IsValidSublimeView( _view_source ) ):
				self.DetectSourceViewHack( );
				## _view_source = GetPluginObject( ).GetSourceView( );

			## Task: make this into a plugin global func to make it easier.... along with others.
			if ( AcecoolLib.Sublime.GetViewFileName( _view ) == GetPluginObject( ).GetPanelName( ) ):

				## if ( ( GetPluginObject( ).GetSetting( 'plugin_control_use_single_click_jump_to', False ) and _cmd == 'drag_select' ) or ( _cmd == 'drag_select' and _args.get( 'by', '' ) == 'words' ) ):
				if ( self.HasSingleClicked( _cmd, _args ) or self.HasDoubleClicked( _cmd, _args ) ):
					## Grab the region which was double-clicked and convert to a line of text...
					_point		= _view.sel( )[ 0 ].a;

					_region		= _view.line( _point );
					_text		= _view.substr( _region );
					_split		= _text[ len( _text ) - 10 : ].split( ':' );
					_line		= _split[ -1 ].strip( );

					print( 'Trying to determine the line this links to: ' + _text )

					## Make sure there's data to be converted to an int - or simply use the first line so it'll always jump... Although I could simply use 01 and prevent a jump...
					_line = int( _line ) if ( len( _split ) >= 1 and _line != '' and AcecoolLib.String.Numbers( _split ) != '' ) else -1;

					## Navigate to the line...
					if ( _line > 0 ):
						## AcecoolLib.Sublime.ScrollViewToLine( _view_source, _line );
						AcecoolLib.Sublime.ScrollViewToLine( _view_source, _line );
						AcecoolLib.Sublime.SetActiveView( _view_source );
						self.Notify( 'Navigating to line: ' + str( _line ) );
					else:
						self.Notify( 'Error Navigating - Line Not Found :: ' + str( _split ) );

		##
		sublime.set_timeout_async( lambda: JumpToLineHelper( ), 10 );



		## ##
		if ( self.IsTextCommandIgnored( _cmd ) ):
			return False




	##
	## Used to ensure the source code from _view is mapped to our output panel...
	##
	def ExecuteCodeMappingSystem( self, _view, _force_nocache_remap = False, _preprocessor_only = False ):
		##
		if ( self.IsPluginTaskDebuggerActive( 'ExecuteCodeMappingSystem' ) ):
			self.Notify( 'ACMS -> EventListener -> ExecuteCodeMappingSystem' )

		##
		_view_map = GetPluginObject( ).GetOutputView( )

		## Up to now everything is valid and checking out... Grab the full path/to/file.py and the file.py name only...
		_file = AcecoolLib.Sublime.GetViewFileName( _view )
		_path = AcecoolLib.Sublime.GetViewFileName( _view, True )

		## print( 'ACMS -> Determining whether _file is Panel... ' + _file + ' == ' + ACMS( ).GetPanelName( ) )
		## print( '\n\nACMS -> ExecuteCodeMappingSystem -> Processing View: ' + str( _view.id( ) ) + ' -- File: ' + str( AcecoolLib.Sublime.GetViewFileName( _view, False ) ) )

		## It shouldn't be none with the above checks, but just in case...
		if ( _path == None or _path == '' ):
			return False

		## Make sure we populate the ACMS View...
		## GetPluginObject( ).ProcessSourceCodeMapping( _path, _force_nocache_remap, _preprocessor_only )
		## _view.run_command( 'acms_preprocess_view', { 'view_id': _view.id( ), 'force_nocache_remap': False, 'preprocessor_only': False } )

		##
		if ( ACMS( ).UseMappingThreads( ) ):
			## ACMS( ).GetThread( ).Jobs( ).append( { 'type': 'Update', 'view_id': _view.id( ), 'force_nocache_remap': False, 'preprocessor_only': False } );
			## ACMS( ).GetThread( ).AddJobs( { 'type': 'Update', 'view_id': _view.id( ), 'force_nocache_remap': False, 'preprocessor_only': False } );
			AddThreadJobs( { 'type': 'Update', 'view_id': _view.id( ), 'force_nocache_remap': False, 'preprocessor_only': False } );
			ProcessThreadJob( 'acms_preprocess_view', { 'view_id': _view.id( ), 'force_nocache_remap': False, 'preprocessor_only': False } );
		else:
			sublime.set_timeout_async( lambda: _view.run_command( 'acms_preprocess_view', { 'view_id': _view.id( ), 'force_nocache_remap': False, 'preprocessor_only': False } ), 100 )




	##
	##
	##
	## if active -- If the view selected is the mapping panel, update the panel id ( redundant )	return None
	##	also in active -- update view group, and if view group is the map_group then ignore... else if view is not the active view then update the map system
	##
	def on_activated( self, _view ):
		## Make sure the Plugin Output Panel is Valid and the _view is a valid Sublime Text View with a Valid File-Name...
		if ( not IsValidACMSPanel( ) or not IsValidSublimeView( _view ) ):
			return False


		##
		_file = AcecoolLib.Sublime.GetViewFileName( _view )
		_path = AcecoolLib.Sublime.GetViewFileName( _view, True )

		## If we've switched to the ACMS - Panel then make sure we don't remove our current settings on the Source View, etc...
		if ( _file == GetPluginObject( ).GetPanelName( ) or AcecoolLib.Sublime.GetViewFileName( _view ) == GetPluginObject( ).GetPanelName( ) ):
			return False

		##
		if ( self.IsPluginTaskDebuggerActive( 'on_activated_async' ) ):
			self.Notify( 'ACMS -> EventListener -> on_activated_async :: ' + AcecoolLib.String.FormatColumn( 50, AcecoolLib.Sublime.GetViewFileName( _view ), 4, ' :: ', 100, str( _view ) ) )

		## If the activated view is not in the same window as the plugin then don't contionue... don't map it..
		if ( ACMS( ).GetOutputView( ).window( ).id( ) != _view.window( ).id( ) ):
			return False

		## If we switched to our output panel, then back ( the source view won't change ) we don't need to refresh anything...
		if ( _view == GetPluginObject( ).GetSourceView( ) ):
			## Unless we saved a core file in another window...
			if ( not self.core_file_saved ):
				##
				if ( ACMS( ).GetSetting( 'acms_notify_on_return_to_source_view', False ) ):
					self.Notify( 'Returning to Source-View from Output-View... No action necessary!' );

				return False
			else:
				if ( ACMS( ).GetSetting( 'acms_notify_on_core_file_altered', False ) ):
					self.Notify( 'CORE FILE ALTERED - Triggering Re-Building file..' );

		## Make sure the Syntax Settings File is loaded for this syntax for the view - I need to check the command used to switch and trigger it there too...
		GetPluginObject( ).GetSyntaxSettingsFileName( _view );

		## It is valid, and has a file name, etc... set the data...
		GetPluginObject( ).ResetViewInfo( _view, True );

		##
		self.core_file_saved = False;

		## Save all settings...

		##
		## sublime.set_timeout_async( lambda: self.ExecuteCodeMappingSystem( _view, False ), 10 )
		## sublime.set_timeout( lambda: self.ExecuteCodeMappingSystem( _view, False ), 0 )

		##
		if ( ACMS( ).UseMappingThreads( ) ):
			## ACMS( ).GetThread( ).Jobs( ).append( { 'type': 'Update', 'view_id': _view.id( ), 'force_nocache_remap': False, 'preprocessor_only': False } );
			## ACMS( ).GetThread( ).AddJobs( { 'type': 'Update', 'view_id': _view.id( ), 'force_nocache_remap': False, 'preprocessor_only': False } );
			AddThreadJobs( { 'type': 'Update', 'view_id': _view.id( ), 'force_nocache_remap': False, 'preprocessor_only': False } );
			ProcessThreadJob( 'acms_preprocess_view', { 'view_id': _view.id( ), 'force_nocache_remap': False, 'preprocessor_only': False } );
		else:
			sublime.set_timeout_async( lambda: _view.run_command( 'acms_preprocess_view', { 'view_id': _view.id( ), 'force_nocache_remap': False, 'preprocessor_only': False } ), 100 );

		## _view.run_command( 'acms_preprocess_view', { 'view_id': _view.id( ), 'force_nocache_remap': False, 'preprocessor_only': False } );
		## _view.run_command( 'acms_map_file', { 'file': _path, 'force_nocache_remap': True, 'preprocessor_only': True } );


	##
	##
	##
	## When a save has been detected - if the mapping panel is active / open, then refresh / re-process the file... If the panel is open then reactivate ( redundant check to make sure the panel, when open, has code processing done to it )
	## Note: Removed from async
	##
	def on_post_save( self, _view ):
		## Make sure the Plugin Output Panel is Valid and the _view is a valid Sublime Text View with a Valid File-Name...
		if ( not IsValidACMSPanel( ) or not IsValidSublimeView( _view ) ):
			return False;


		##
		## If file saved is a core file, update the time in the database immediately...
		##
		_file															= AcecoolLib.Sublime.GetViewFileName( _view );
		_path															= AcecoolLib.Sublime.GetViewFileName( _view, True );
		## _database														= ACMS( ).Database( );
		## _database														= ACMS( ).GetThread( ).Database( );

		##
		_plugin, _database			= GetPluginNode( ).PluginDatabase( );

		## ##
		## _plugin															= None;
		## _database														= None;

		## if ( ACMS( ).UseMappingThreads( ) ):
		## 	_plugin														= GetPluginNode( ).GetThreadedPlugin( );
		## 	_database													= GetPluginNode( ).GetThreadedDatabase( );
		## else:
		## 	_plugin														= GetPluginNode( ).GetThreadedPlugin( );
		## 	_database													= GetPluginNode( ).GetThreadedDatabase( );

		## ## If in the thread, connect each time - until I create a threaded system.. If not threaded, old style ( 0.1 seconds time - connecting each time has 2.5 second which is a problem, even for threaded mode but the goal is to figure out why it is locking the UI thread, then working on optimization and keeping references intact )
		## if ( ACMS( ).GetThreadState( ) ):
		## 	_database													= ACMSDatabase( ACMS( ) );
		## else:
		## 	_database													= ACMS( ).Database( );

		## _database														= ACMSDatabase( ACMS( ) );
		_is_core, _row_id, _row_file, _row_data, _row_crc, _row_time	= _database.IsCoreFile( _path );


		## ##
		## _whitelist = {
		## 	'AcecoolCodeMappingSystem.py': True,
		## 	'AcecoolCodeMappingSystemDefinitions.py': True,
		## 	'AcecoolCodeMappingSystemPlugin.py': True,
		## 	'AcecoolCodeMappingSystemPluginReloader.py': True,
		## 	## '': True,
		## 	## '': True,
		## 	## '': True,
		## 	## '': True,
		## 	## '': True,
		## 	## '': True,
		## 	## '': True,
		## 	## '': True,
		## 	## '': True,
		## 	## '': True,
		## 	## '': True,
		## 	## '': True,
		## };

		##
		if ( _is_core ):
			self.Notify( 'ACMS -> EventListener -> on_post_save :: The file is a CORE file - ' + _path );
			_database.UpdateCore( _path );
			self.core_file_saved = True;

			## ##
			## ##
			## ##
			## def PreprocessorUpdateFiles(  ):
			## 	## If we updated a core file, and we have our caching preprocessor enabled, we need to reprocess every open file...
			## 	if ( ACMS( ).GetSetting( 'use_caching_system_preprocessor', False ) ):
			## 		print( 'ACMS -> PreProcessing all views in all windows...' );

			## 		##
			## 		_active_window = sublime.active_window( );

			## 		## for _window in sublime.windows( ):
			## 			## print( 'ACMS -> Processing Window: ' + str( _window.id( ) ) );
			## 			## for _v in _window.views( ):
			## 		for _i, _v in enumerate( _active_window.views( ) ):
			## 				## Only allow 4...
			## 				if ( _i > 5 ):
			## 					break;

			## 				_f = AcecoolLib.Sublime.GetViewFileName( _v, False );
			## 				_p = AcecoolLib.Sublime.GetViewFileName( _v, True );
			## 				_acms_panel = _f.strip( ) == ACMS( ).GetPanelName( ).strip( );

			## 				if ( not _acms_panel ):
			## 					## print( 'ACMS -> Processing Window: ' + str( _window.id( ) ) + ' View: ' + str( _v.id( ) ) + ' -- File: ' + str( AcecoolLib.Sublime.GetViewFileName( _v, False ) ) );

			## 					## if ( _whitelist.get( _f, False ) ):
			## 					## _targeted_view = _v;
			## 					print( 'ACMS -> Processing Window: ' + str( _window.id( ) ) + ' View: ' + str( _v.id( ) ) + ' -- File: ' + str( AcecoolLib.Sublime.GetViewFileName( _v, False ) ) );
			## 					## sublime.set_timeout_async( lambda: self.ExecuteCodeMappingSystem( _targeted_view, True, True ), 10 );
			## 					self.ExecuteCodeMappingSystem( _v, True, True );
			## 				else:
			## 					print( 'ACMS -> Processing Window: ' + str( _window.id( ) ) + ' View: ' + str( _v.id( ) ) + ' -- Can not process this id, because it is the ACMS Panel file... ' + _f + ' is ' + str( _acms_panel ) );

			## 	## If we updated a core file, and we have our caching preprocessor enabled for all project files, we need to reprocess every project file ( extra rules apply, make sure we've opened at least once, etc.. with higher activation and higher save files first )...
			## 	if ( ACMS( ).GetSetting( 'use_caching_system_project_preprocessor', False ) ):
			## 		pass;


			##

			##
			if ( _plugin.UseMappingThreads( ) ):
				## ACMS( ).GetThread( ).Jobs( ).append( { 'type': 'UpdateAllOpen' } );
				## ACMS( ).GetThread( ).AddJobs( { 'type': 'UpdateAllOpen' } );
				## AddThreadJobs( { 'type': 'UpdateAllOpen' } );


				for _i, _v in enumerate( sublime.active_window( ).views( ) ):
					ProcessThreadJob( 'acms_preprocess_view', { 'view_id': _v.id( ), 'force_nocache_remap': True, 'preprocessor_only': True } );

			## else:
			## 	## sublime.set_timeout_async( lambda: PreprocessorUpdateFiles( ), 100 );
			## 	ACMSUpdateAllViews( self );


		##
		if ( self.IsPluginTaskDebuggerActive( 'on_post_save' ) ):
			self.Notify( 'ACMS -> EventListener -> on_post_save :: ' + AcecoolLib.String.FormatColumn( 50, AcecoolLib.Sublime.GetViewFileName( _view, True ), 4, ' :: ', 100, str( _view ) ) )

		## IMake sure the user didn't save then jump to a different window - or save a file in a different window... - If not in private window mode, then switching windows is a problem... With private window mode true, we have the ACMS panel in a different window to source views... one output, many potential inputs...
		if ( _plugin.GetSetting( 'plugin_private_window_mode', False ) == False and _plugin.GetOutputView( ).window( ).id( ) != _view.window( ).id( ) ):
			return False

		## Make sure the user didn't save and jump to a different file... Note: Since this is ASYNC we need to see if a config file was saved, if so then we can refresh when this executes...
		if ( _plugin.GetSourceView( ) != _view ):
			return False

		## If a configuration file was saved - update the panel...
		if ( _file.startswith( 'ACMS_' ) and _file.endswith( '.sublime-settings' ) ):
			_plugin.UpdateOutputPanelSettings( )


		## When the file has been saved - we remap the file with force_nocache_remap to ensure it is processed and that new output will overwrite the old cached file.....
		## sublime.set_timeout_async( lambda: self.ExecuteCodeMappingSystem( _view, True ), 10 )
		## sublime.set_timeout( lambda: self.ExecuteCodeMappingSystem( _view, True ), 0 )
		## _view.run_command( 'acms_map_file', { 'file': _path, 'force_nocache_remap': True, 'preprocessor_only': True } )
		## _view.run_command( 'acms_preprocess_view', { 'view_id': _view.id( ), 'force_nocache_remap': True, 'preprocessor_only': False } )

		## sublime.set_timeout_async( lambda: _view.run_command( 'acms_preprocess_view', { 'view_id': _view.id( ), 'force_nocache_remap': True, 'preprocessor_only': False } ), 100 )

		##
		if ( _plugin.UseMappingThreads( ) ):
			## ACMS( ).GetThread( ).Jobs( ).append( { 'type': 'Update', 'view_id': _view.id( ), 'force_nocache_remap': True, 'preprocessor_only': False } );
			## ACMS( ).GetThread( ).AddJobs( { 'type': 'Update', 'view_id': _view.id( ), 'force_nocache_remap': True, 'preprocessor_only': False } );
			AddThreadJobs( { 'type': 'Update', 'view_id': _view.id( ), 'force_nocache_remap': True, 'preprocessor_only': False } );
			ProcessThreadJob( 'acms_preprocess_view', { 'view_id': _view.id( ), 'force_nocache_remap': True, 'preprocessor_only': False } );
		else:
			sublime.set_timeout_async( lambda: _view.run_command( 'acms_preprocess_view', { 'view_id': _view.id( ), 'force_nocache_remap': True, 'preprocessor_only': False } ), 100 )





##
## Command: Text - This plugin modifies the replaces the entire contents of a file with the contents supplied to it by one or more mappers...
##
class acms_preprocess_view( sublime_plugin.TextCommand ):


	##
	##
	##
	def run( self, _edit, **_varargs ):
		## ##
		## if ( not ACMS( ) ):
		## 	return None;


		##
		_view_id				= _varargs.get( 'view_id', None );
		_view					= sublime.View( _view_id );
		_force_nocache_remap	= _varargs.get( 'force_nocache_remap', False );
		_preprocessor_only		= _varargs.get( 'preprocessor_only', False );

		_path					= AcecoolLib.Sublime.GetViewFileName( _view, True )
		_file					= AcecoolLib.Sublime.GetViewFileName( _view )

		##
		print( 'acms_preprocess_view -> Checking Data -> View: ' + str( _view ) + ' -> ReMap: ' + str( _force_nocache_remap ) + ' -> PreProcess Only: ' + str( _preprocessor_only ) + ' -> File: ' + _file + ' -> Path: ' + _path );

		## ## Task: Add custom settings system with callbacks on any change, etc...
		## sublime.set_timeout_async( lambda: GetPluginObject( ).SaveAllSettings( ), 10 )

		## sublime.set_timeout( lambda: self.ExecuteCodeMappingSystem( _view, _force_nocache_remap, _preprocessor_only ), 0 );
		## sublime.set_timeout_async( lambda: self.ExecuteCodeMappingSystem( _view, _force_nocache_remap, _preprocessor_only ), 0 );
		## _view.run_command( 'acms_map_file', { 'file': _path, 'force_nocache_remap': _force_nocache_remap, 'preprocessor_only': _preprocessor_only } );
		## Make sure we populate the ACMS View...
		## GetPluginObject( ).ProcessSourceCodeMapping( _path, _force_nocache_remap, _preprocessor_only )

		## ACMS( ).GetThread( ).Jobs( ).append( { 'type': 'Update', 'view_id': _view_id, 'force_nocache_remap': _force_nocache_remap, 'preprocessor_only': _preprocessor_only } );
		## ACMS( ).GetThread( ).AddJobs( { 'type': 'Update', 'view_id': _view_id, 'force_nocache_remap': _force_nocache_remap, 'preprocessor_only': _preprocessor_only } );

		## ProcessThreadJob( 'acms_preprocess_view', { 'view_id': _view_id, 'force_nocache_remap': _force_nocache_remap, 'preprocessor_only': _preprocessor_only } );

		## If the Threading system is used, add the job... Otherwise, use the standard call ( can use acms_preprocess_view, accms_map_file, etc... or the function call itself.. )
		if ( ACMS( ).UseMappingThreads( ) ):
			AddThreadJobs( { 'type': 'Update', 'view_id': _view_id, 'force_nocache_remap': _force_nocache_remap, 'preprocessor_only': _preprocessor_only } );
		else:
			sublime.set_timeout( lambda: GetPluginObject( ).ProcessSourceCodeMapping( _path, _force_nocache_remap, _preprocessor_only ), 0 );

		## sublime.set_timeout_async( lambda: GetPluginObject( ).ProcessSourceCodeMapping( _path, _force_nocache_remap, _preprocessor_only ), 0 );
		## _view.run_command( 'acms_preprocess_view', { 'view_id': _view.id( ), 'force_nocache_remap': _force_nocache_remap, 'preprocessor_only': _preprocessor_only } )











##
## This Event Listener is specifically to enable auto-refreshing of plugin core files when they're edited... It is purposefully kept separate so others can learn from it / use as a drop-in module for thier own plugins...
##
class AcecoolCodeMappingSystemDynamicPluginReloaderEventListener( sublime_plugin.EventListener ):
	##
	## Important Data...
	##

	## The package name used for reloading files listed below...
	package_name				= 'AcecoolCodeMappingSystem'

	## Packages Folder Name ( For generating id when not a part of our package )
	packages_folder				= 'Packages'

	## The file-type to monitor changes from ( ie files we re-import so the changes are relfected in memory - If the file extension isn't this, then we don't return ANY name )
	monitored_extensions		= { 'py': True, 'sublime-settings': True }

	## Simple text variant of -1, for the last element in a list..
	## get_last_element			= -1

	## Returns all text from n to - 3rd char or up until '.py'... this is a simple text helper to clarify that..
	subtract_ext_from_text		= -3 # -( len( monitored_extension ) + 1 )

	## The text we monitor to determine whether or not the updated file is in the User\ folder..
	monitored_user_text			= 'User.'

	## Simple helper for defining 'User.' len
	shift_by_user_dot_length	= 5 # len( monitored_user_text )


	## ##
	## ## Convert the Platform name into the Correct Case / Variant so the file can be found on *nix systems...
	## ##
	## def GetPlatformName( self ):
	## 	return {
	## 		'osx':			'OSX',
	## 		'windows':		'Windows',
	## 		'linux':		'Linux',
	## 	}[ sublime.platform( ) ]





	##
	## Returns the plugin name for use in importing it...
	##
	def GetPluginNameFromPath( self, _path = '' ):
		## Grab the extension of the saved file - note this doesn't work on files with more than 1 extension or decimal - as per Python file-naming / import conventions, do not add decimals to the file-name. Only use 1 extension such as file.ext - anything else will not be importable using the import call, you'll need to manually execute the code behind the import call which is messy and can lead to issues if you're unfamiliar with the system and it can lead to other problems..
		## _ext = _path.split( '.' )[ self.get_last_element ]
		_raw_ext, _ext = os.path.splitext( _path )
		if ( _ext[ 0 : 1 ] == '.' ):
			_ext = _ext[ 1 :  ]

		## Determine the index of the package name, if it exists..
		_index = _path.find( self.package_name )

		## Helper - If True then it is part of our package, if False then it isn't ( user can decide what to do )
		_is_plugin_file	= ( _index > 0 )

		## If the file isn't from our package, make sure our index var is set correctly so we can still generate the correct string by removing *Packages\ from the output...
		if ( not _is_plugin_file ):
			_index = _path.find( self.packages_folder ) + len( self.packages_folder ) + 1

		## If we've saved a python file inside of our Package, then we assemble the include...
		if ( self.monitored_extensions.get( _ext, None ) ):
			## Note: Either of the following _plugin assignments will work, simply uncomment one and comment the other - the comment details exactly what happens with each example...

			if ( _ext == 'sublime-settings' ):
				_folder_slash = '/'
				## if ( self.GetPlatformName( ) == 'Windows' ):
				if ( AcecoolLib.Sublime.GetPlatformName( ) == 'Windows' ):
					_folder_slash = '\\'

				return ( _path.split( _folder_slash )[ - 1 ], _is_plugin_file )
			else:
				## For simplicty convert all folders to decimals...
				_path = _path.replace( '\\', '.' )
				_path = _path.replace( '/', '.' )

				## This method takes Path\To\||PackageName\InternalFolders\FileName||[[.py]] - [[...]] being subtracted from the return using -3, ||...|| being what's captured starting with _index + len( ... ) through -3... - After we capture what's needed ignoring the rest, we replace backslashes with decimals.
				_plugin = _path[ _index : self.subtract_ext_from_text ]

				## User Folder Clause - If User is found in the file-name, and the index is LESS than the name of the package-name, then we are in Packages\User\ACMS\ so we need to account for that...
				_plugin_user = _path[ _index - self.shift_by_user_dot_length : self.subtract_ext_from_text ]
				if ( _plugin_user.startswith( self.monitored_user_text ) ):
					_plugin = self.monitored_user_text + _plugin

				return ( _plugin, _is_plugin_file )

		## Default - If the file saved isn't a Python file...
		return ( None, _is_plugin_file )


	##
	## When a package Python file has been saved, we auto-refresh only that particular file... - Note: If you update this function you will need to save twice - the first time uses the current method in memory for output, and the second time shows you what the function has become...
	##
	def on_post_save_async( self, _view ):
		## Grab the file-name of the file which was saved
		_file = _view.file_name( )

		## Grab the clean plugin import string / name and whether or not the file is part of our library so we can choose whether or not to include it ( or to call some other callback in our plugin with this information )
		( _plugin, _is_plugin_file ) = self.GetPluginNameFromPath( _file )

		## If the file saved is a plugin file, and a name was generated then re-import the files to allow the changes made in code to be reflected by code held in memory...
		if ( _is_plugin_file and _plugin != None ):

			if ( _plugin.endswith( 'py' ) ):
				## Print it out to make sure it works..
				print( '>> Dynamic Package File Reloader > On Save Event Triggered for Package File: "' + _file + '" - Which converts to Import Plugin: "' + _plugin + '"' )

				## Reload our Package File...
				sublime_plugin.reload_plugin( _plugin )
			elif ( _plugin.endswith( 'sublime-settings' ) ):
				## Print it out to make sure it works..
				print( '>> Dynamic Package File Reloader > On Save Event Triggered for Package Settings File: "' + _file + '" - Which converts to sublime.load_settings: "' + _plugin + '"' )

				sublime.load_settings( _plugin )
