##
## JavaScript - Default / Vanilla / Base Mapper - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent ACMS Mapper - As this is the User mapper, I've set up all possible includes for this mapper out!
from AcecoolCodeMappingSystem.maps.__mapper_imports__ import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##  Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
class ACMS_JavaScript_Default( XCodeMapperBase ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_JavaScript_Default'


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' )

		self.AddSyntax( CATEGORY_COMMENT, SEARCH_TYPE_STARTSWITH, '//' )

		## If False then we register block-comments to hide the code examples in my universal site feature adder userscript...
		if ( self.GetSetting( 'parse_code_within_comments', False ) == False ):
			self.AddSyntax( CATEGORY_COMMENT_BLOCK_BEGIN, SEARCH_TYPE_STARTSWITH, '/*' )
			self.AddSyntax( CATEGORY_COMMENT_BLOCK_FINISH, SEARCH_TYPE_STARTSWITH, '*/' )

		##
		self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_STARTSWITH, 'function ' )
		self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_CONTAINS, '= function', '= async function' )

		##
		self.AddSyntax( CATEGORY_DEBUGGING, SEARCH_TYPE_STARTSWITH, 'console.log' )
		## self.AddSyntax( CATEGORY_DEFINITION, SEARCH_TYPE_STARTSWITH, 'var ', 'return ', 'const ' )


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )		## self.CallParent( 'OnSetupSyntax' )


	## ##
	## ## Callback - these are the symbols from the file we're currently editings symbols( ) from the view - ie all extracted symbols from Sublime Text...
	## ## In Short: If working this should give us all of the information from the Command Palette when we look at @, :, etc.. for function names and everything... A potentially almost perfect way to create a universal mapper...
	## ##
	## ## Task: Allow symbols to be processed through the AddSyntax processor ( Make a syntax processor function outside of Run to do this ) - Then the full-line of code can be grabbed from the symbols SublimeText provides us and we'll have a basic universal mapping system....
	## ##
	## def OnMappingFileSymbols( self, _view, _symbols ):
	## 	## pass
	## 	print( ' >> Symbols: ' + str( _symbols ) )
	## 	## Symbols also don't give their function prefix...
	## 	## if ( _code.startswith( 'def' ):

	## 	## Symbols appear in a list, in a tuple with a region and the symbol...[((515, 525), 'PANEL:Init'), ((891, 910), 'PANEL:ControlValues'), ((1159, 1178), 'PANEL:PerformLayout')]

	## 	## So for each entry
	## 	for _entry in _symbols:
	## 		## Tuples and lists use numerical indices
	## 		## Grab the region object ( chars from start of file to X, and Total Chars in Region + X )
	## 		_region = _entry[ 0 ]

	## 		## Grab the partial code we're given - we can grab the full line using a helper function...
	## 		_code = _entry[ 1 ]

	## 		## Grab the line number using another helper...
	## 		_line_number = self.GetSublimeViewLineNumberByRegion( _region )

	## 		## Grab the line code by another helper...
	## 		_line = self.GetFileDataByLine( _line_number )

	## 		## Note - Symbols use regions meaning the starting char of from the entire code blob to the end... not line numbering so this has to be calculated...
	## 		## Since other info is also lost we can really only guess at the data unless we check the file and look at other infomration...
	## 		## self.AddEntry( CATEGORY_FUNCTION, _line_number, _code, _depth, _mode, _pattern, _class_cat )
	## 		self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ), 0 )


	##
	## Called for each function argument - allows you to do anything you want...
	##
	## example: by replacing _p and _w which are hard-coded variable-names in my coding standard..
	##	function GM:PlayerDeath( _p, _w )
	## can become
	##	function GM:PlayerDeath( <Player>, <Weapon> )
	## or
	##	function GM:PlayerDeath( <Player> _p, <Weapon> _w )
	##
	def OnFunctionArgument( self, _arg, _category, _line_number, _code, _depth, _mode, _pattern ):
		## If a function was used as an argument - it gets replaced with FUNC_REF so we can set what the function should look like - including all args - in place of it.. especially if other arguments come after as multi-line scanning isn't enabled in this mapper..
		if ( _arg == '' ):
			self.AddWarning( 'Possible Syntax Error - function argument empty which may be due to an extra comma..', _line_number );


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).OnFunctionArgument( _arg, _category, _line_number, _code, _depth, _mode, _pattern )


	##
	## Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original ):
		##
		## Check for syntax error
		##
		if ( _line_number != '' ):
			if ( _pattern == 'var ' or _pattern == 'return ' or _pattern == 'const ' ):
				## If a definition doesn't end with ; then mark it - but only if the definition isn't a table or something - ie ends with the opening bracket.. and it isn't a function definition with { on the next line... and comments aren't obscuring the end of the line
				## Task: Ensure _code is the edited code which is passed through with comments STRIPPED!!! Add _code_raw arg for the unedited line..
				if ( not _code.endswith( ';' ) and not _code.endswith( '{' ) and not self.GetFileDataByLine( _line_number + 1 ).startswith( '{' ) and not AcecoolLib.String.IndexOfExists( '//', _code, True ) ):
					self.AddError( 'Syntax Error - Definition line does not end with ;', _line_number );
					_code = _code + ' [ ERROR ]'

			##
			if ( _pattern == 'console.log' and AcecoolLib.String.IndexOfExists( '..', _code, False ) ):
				## Mark the error so the developer can fix it..
				self.AddError( 'Possible Syntax Error: Using .. for concatenation instead of +', _line_number );

				## Add an error tag to the code in question to make it noticeable as the error only describes the type and line number to keep it concise..
				_code = _code + ' [ ERROR ]'


		## Transform = async function and = function lines of code to be always function name( args ) for uniformity...
		if ( AcecoolLib.String.IndexOf( '= function', _code ) > 0 or AcecoolLib.String.IndexOf( '= async function', _code ) > 0 ):
			_code = _code.replace( ' = function', '' )
			_code = _code.replace( ' = async function', '' )
			_code = 'function ' + _code;


		##
		## Don't run in the definitions category so we can see what they are in raw form...
		##
		if ( self.GetSetting( 'output_library_functions_in_new_category', False ) == False and self.GetCategoryType( _category ) == CATEGORY_TYPE_FUNCTION ):
			##
			## Helpers
			##

			## Decimal Delimeter
			_dot_delim = AcecoolLib.String.IndexOf( '.', _code )
			_varargs_delim = AcecoolLib.String.IndexOf( '...', _code )

			_parts = _code.split( '.' )
			## If VarArgs exist, then we need to limit _parts to code before it... because _parts_1 takes all of the left-over code...
			if ( _varargs_delim > 0 ):
				_parts = AcecoolLib.String.SubStr( _code, 0, _varargs_delim - 1 ).split( '.' )

			##
			_parts_0 = None
			_parts_1 = None

			## If . exists, but it isn't where ... is ( if . is at ... then that means . wasn't found in the name...
			if ( _dot_delim > 0 ):
				_parts_0 = ''

				for _k, _v in enumerate( _parts ):
					if ( _k < len( _parts ) - 1 ):
						_parts_0 = _parts_0 + _v + '.'

				_parts_1 = AcecoolLib.String.SubStr( _code, len( _parts_0 ), len( _code ) ) #_parts[ - 1 ]



			##
			if ( _parts_0 != None and _parts_1 != None ):
				##
				_lib_name = _parts_0.strip( )

				if ( AcecoolLib.String.IndexOf( '=', _lib_name ) > 0 ):
					_lib_name = _lib_name.split( '=' )[ 0 ].strip( )

				## Grab the function key... - Not all functions use a single decimal which is why this one goes first - : is easier to spot... decimal requires a bit more work to get right...
				_func_name = self.GetSetting( 'function_name_replace', 'ƒ' ) + ' ' + _parts_1

				## If we want to display all class-functions in a single category - allow it... except for internal functions as these grow to be MANY..
				if ( self.GetSetting( 'output_class_functions_in_new_category', False ) and _category != CATEGORY_FUNCTION_INTERNAL ):
					_category = CATEGORY_CLASS_FUNCTION

				_lib_name = _lib_name.replace( self.GetSetting( 'function_name_replace', 'ƒ' ) + ' ', '' )

				self.AddSimpleEntry( _category, _line_number, _func_name, _depth, '' + _lib_name + ' under ' + self.GetCategoryName( _category ) + ' as a Class / Object / Meta-Table' )

				## Prevent the default function from being added as an entry
				return False



		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------