##
## Garry's Mod Lua - User Mapper - <Author Name>
##


##
## Task List - Proper User Mapper Creation and / or Modification Procedure
##
## Task: Copy this file to Packages\User\AcecoolCodeMappingSystem\maps\LanguageName_MapperName.py
## Task: Make a plan or design of what you want to accomplish with this mapper...
## Task: Modify this mapper...
## Task: If you need more tools at your disposal or find an issue: Submit bug reports, feature requests or constructive criticism!
## Task: When you've finished with this mapper for your project, or for a language - Release it?
## Note: If you want to, you may submit it on my repo and if I like it, I will add it and it will retain your author name, site info, other info you want to include, etc... I may ask you to make changes before accepting it, or I may make the changes myself if I find any bugs, etc..
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Import the Parent ACMS Mapper - As this is the User mapper, I've set up all possible includes for this mapper out!
from AcecoolCodeMappingSystem.maps.GMod_Lua import *

## Alternate includes... Make sure you're not using a circular import by following the import chain...
## from AcecoolCodeMappingSystem.maps.GMod_Lua_AcecoolDev_Framework import *
## from AcecoolCodeMappingSystem.maps.GMod_Lua_DarkRP import *
## from AcecoolCodeMappingSystem.maps.GMod_Lua_NutScript import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Everything you need to create your own - The base class which very limited structure, the CFG_ options above and the ENUM value to use to set to it... Simply rename them...
##
## class ACMS_GMod_Lua_User( ACMS_GMod_Lua_AcecoolDev_Framework ):
## class ACMS_GMod_Lua_User( ACMS_GMod_Lua_Default ):
## class ACMS_GMod_Lua_User( XCodeMapperBase )
class ACMS_GMod_Lua_User( ACMS_GMod_Lua_Default ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_GMod_Lua_User'


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	##	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	##	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	##	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' )

		## ## Possible DRM or Back-Door malicious code detections...
		## self.AddSyntax( CATEGORY_DRM, SEARCH_TYPE_CONTAINS, 'TrackingStatistics', 'StatisticsTracking', 'http.Post' )
		## self.AddSyntax( CATEGORY_BACKDOOR, SEARCH_TYPE_CONTAINS, 'rcon_password', 'server.cfg' )


		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )



##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
