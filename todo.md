//
---
// Acecool Code Mapping System - Tasks / To Do / Updates Planned Lists
---
//
---





+ Add command which adds param headers to all functions...

+ Add template system for this so ...

	Python multi-line string '''{Comment}
		{Comment} {DESC}
		{Comment}
		{PARAM_LINE_REPEAT="{Comment}@{PARAM_TYPE}: {PARAM_NAME} - {PARAM_DESC}"}
		{Comment}'''

	Makes:
		//
		// PlayerDeath called on player death...
		//
		// @arg: _p = Player Entity, this is the player who died...
		// @arg: _w == Weapon .. This is the weapon used to kill the player _p
		// @arg _inflictor == Inflictor .. This is similar to the weapon used to kill the player _p, but it is like the parent so if an entity was used then it should show the player...
		//


+ Add command which re-orders all functions in a file ( store the first line of the function so headers aren't removed - for classes store class line then first line of first func inside... use comment as first line so comments move with them )

+ Add command to add param comments ( as seen above ) to all classes and functions in a file... If there are existing comments they'll be left as is, or moved above or below the new comment... likely above... I'll also attempt to copy the comment into DESC automatically...

+ Add rule system for sorting system so __ functions can be first or last in classes, etc...

+ Add system to map functions and their args for wiki style system... Data-Type, suggested arg name, and arg position ( arg position is important )....

+ Add arg position from functions system so arg name doesn't need to be mapped - so functions can be mapped and be done with it....

+ Add system to detect the users naming standards - ie if it detects UpperCamelCase first, then finds lowerCamelCase etc... issue a warning... Also allow standards to be declared so it can detect issues...

+ Add option to allow mapped / registered functions to be output properly - ie if GM:PlayerDeath is mapped with _p, _w, _inflictor as args and user uses GM:PlayerDeath( _p, _w ), then issue a warning for missing arg and either use a unique char ( configurable ) or simply output the correct system with RED background for error ( using dynamic coloring system )

+ Add dynamic coloring system so lines can have red-background... so Tasks which haven't been done can be normal, done can be green, black, red, or whatever... In progress can be another color... etc....

+ Alter the plugin object so it isn't centralized... Basically allow plugin ACMS - Panel to carry a reference or data for mapping to this window... Allow other windows to open a panel too... IE make the plugin object irrelevant except to process the data - the panel should store information such as window id, current loaded file, mapper, etc... and the plugin can then work on many many different windows at the same time.. Right now this isn't possible.. - at the very least due to the panel not being able to be opened because of it being opened in another window, and there may be issues of it opened in another window.... ALSO, I want to add a feature to allow it to work in another window..

+ Add a feature which allows the ACMS - Panel to be in its own window..

+ IMPORTANT TODO: Get rid of AcecoolLib_Python, AcecoolLib_Sublime, AcecoolLib_SublimeText3 and replace with Acecool.py - The reawson is I can import and leave base classes in that file so Acecool.Sublime.XXX or Acecool.String.FormatColumn( ) instead of using AcecoolString.FormatColumn, etc... it makes things cleaner in code...

+ Create system to copy all maps/*_User.py files to the User ACMS maps folder, if they don't already exist... so they don't exist in the default maps folder anymore...

+

+

+

+

+

+

+

+

+

+

+

+

+

+

+

+

+

+

+

+

+






Note: I may add additional arguments to edit_settings_plus to disable the linked saving, and file linker or I'll add settings to expressly enable them... We'll see... Or I'll add a config option in the preferences file for the basic release for some of these features...

 I'll add a way to add or subtract x size to ensure a snug fit later on... Right now if standard scrollbars are used 1% of the window size is used for that, and 5 percent is used for the minimap which is about right for a default width of 30% ( if I can find the exact measurements somewhere such as in a theme then I can pull the data from there - I am looking into this )






##
## Task List
##
## Task: Step 1 - Convert all of the settings to use the Plugin Definitions - then I'll have it load different values in the back-end once that;'s done based on active language or file ext or something else...
##        so convert what is universal then figure out what isn't ( although since all can be altered based on map as of right now - I need to add that system in - for now do main stuff..
## Task:
## Task:
## Task:
## Task: Add Language System
## Task: Alphabetize category outputs - IF the user config set it up to do so - or if AddCategoryAlphabetize( HELP, FUNCTIONS, CLASSES ) or whatever is called...
## Task: Add new Arg to OnFunction/ClassArg Callback - _arg_default = '', using ArgDefaultSplitDelimiter - Note: May need to check for more than 1 delimiter... will see....
## Task: Create Dynamic nested Category system for Python language or any language with objects so the CLASS category can be the headers, and the functions category can have the class-headers as a category ( with line jump ) and the functions underneath ( which can then be sorted as they're in their own category instead of mixed )
## Task: Create system to grab the Delta Depth / Delta Indentation Length - for when OnCalcDepth returns a different value and definitions or other code is converted thereby offsetting what used to be all indented to the same column to incorrectly offset... Taking the DELTA and re-adding it to the middle where the tabs are is very important...
## Task: Figure out why the TypoFix Entries aren't being processed....
## Task: Add alternate Line Functions - ie instead of simply jumping to a line, allow snippets to be added to the cursor location, or to clipboard, and more...
## Task:
## Task:

##
## Rule system
##
## Task: Add rule system
## Task: Add basic rule-sets such as: Tracking scope ( for classes, if statements, files, etc.. ), variable tracking ( along with a rule to define if the language is case sensitive or not so errors can be pointed out if a variable is used but never declared )
## Task:
## Task:

##
##
##
##
##
## Caching System - Implemented..
##
## X-Task: Add / Create Cache System - If the file we're trying to map isn't cached OR the file being mapped timestamp is newer than cached OR If the mapper timestamp is newer than the file: then reload... otherwise: Simply use the cached version - this should speed certain things up...
## X-Note: Caching System: By default it is set to False because of some other features I need to add - it is quicker than reparsing the data if the data hasn't changed but without processing the timestamp of the mapper which is loaded then editing the mapper ( which would change the data ) won't trigger the system to regenerate the file yet....
## X-Task: Add in a system to delete caches which are over 1 day old for automatic reparsing ( and have it configurable )
## O-Task: Logic needs to be added to determine which custom mapper is being used - then timestamp the mapper too! this way if the mapper has changed, then the
## O-TODO: Add in a system to delete caches which are over 1 day old for automatic reparsing ( and have it configurable ) This is so you don't end up with thousands of files ( only one cache file is generated PER although I may incorporate a backup system which can create a backup every save, every 5 or 10 saves, or how many you want, and delete after having a total of X backups, ie deletes the oldest, or deletes if x days old, etc.. )... This caching system opens the door to a lot of other features which are incredibly useful to have!!!!
## O-TODO: I need to read the MAPPER timestamp too so if the Python mapper has changed, but not the python file we want to map - it'll remap it so if you change the config on how it should output, you'll be able to see it... etc.. Logic needs to be added for this..
##
## Task: Create Backup System
## Task-: Creates backup every N Saves ( 1, 5, 10, 25 or how ever many you want. 0 and -1 disables )....
## Task-: Delete oldest backups after N files ( of the same file - 10 backups, 25, 100, 1000, or how ever many.. -1 disables ).
## Task-: Delete oldest backups after they are N days old ( 1 day, multiple... Note: This will apply to cache output too although it'll be a different variable )
##
## Task: Update Cache System
## Task-: Add Delete old caches if they are older than N Days ( 7 by default - can be anything you want - Time in seconds so it can be in seconds, minutes, hours, days, months, years, millenia, etc.. or disable deleting by using -1 )..
## Task-DONE: Add logic to check the Mapper file Timestamp - If it has changed, then reparse a file even if it hasn't changed because the mapper can change the output...
## Task-DONE: Add logic to check if DEVELOPER_MODE is True - if so then check timestamps for XCodeMapper.py, XCodeMapper_Definitions.py, and AcecoolST3_Library.py bePLUGIN_FOLDER_NAMEcause reloading without restarting SublimeText is enabled therefore we need to be able to reparse the file..
##
##
##


- Known bugs
	- [ SUBLIME TEXT ]
		- If you update a default / package configuration file - Sublime Text will NOT receive the update, even if you try reloading the file... but Sublime Text WILL receive the update if a User settings file is updated - counterintuitive for plugin development if you have to use user files to prevent needing to restart Sublime Text...
	- Acecool Code Mapping System
		- There is an issue where the line number isn't always properly detected when jumping between lines for some reason - but if you jump to others and then come back to it it will work... odd..



Tasks
---
###### This list contains entries for code to be implemented, altered, fixed, and so on.


- Plugin Installer Tasks
	- Ensure it is properly working
	- Add features to copy files from the User\ folder in the Package folder to the same locations on the other side... but only if the files were not modified by the user... If the files were modified by the user then show the output of what was changed by comparing changes between the last version of the user file to the latest so the user knows what to manually change...


- Settings System Tasks
	- Convert all Configuration / Settings Files to use all lowercase keys with underscores instead of UpperCamelCase to be in-line with Sublime Text Settings Standards in place...
	- Move all of the configuration from the extensions and languages into the default mapping settings file until I expand the system later...
	- Finish the RunTime Configuration System


- ACMS Tasks
	- Add logic to detect the Source Panel on start up or auto-refresh so the user doesn't need to switch files, etc... when they first launch Sublime Text...


- Map Tasks
	- Convert all ext.py files into separate Language_Project.py files
	- Convert all CFG_ constants in each ext.py file to use the new settings system..
	- Create Object to be used as an argument for ALL Callbacks - this is so it can be changed without having to update every single callback...
		- Required:
			- Original / UnModified Line of Code
			- Stripped of all whitespace L / R code
			- Original Line Number
			- Whether or not additional Lines are added to the data scanned ( multi-line function / class, etc.. definitions and function calls, etc.. )
			- Length of what the output will be ( for Region mapping - I'll use a LocalToWorld / WorldToLocal style mapping which is basically LocalToWorld( Vector( 123, 0, 0 ) ) converts to :PlayerPos + that Vector, and vice versa - in short basic offsets with functions to easily map based on those offsets - this is so Region mapping will be much much easier and quicker...
			- Catetgory - Original category is absolutely important, but final category will be there - so SetCategory will keep the original somewhere which can be referenced - the CategoryType for original and current is important, and more...
			- and
			- much much
			- more...


- XCodeMapper Changes
	- Convert all names of XCodeMapper * to ACMS *
	-
	-
	- REMOVE OnSetupConfig Callback from ALL objects in favor of the configuration system - Add configuration for file extensions / languages....
		- This will create one of the largest shifts in file-size across all mappers... It'll also pave the way for splitting the files up into languages...
		- Decide: How do I set up the configuration fetcher... should I set up extension / lang config files ( MANY ) or use one for now and use a special way to grab the config... Also - because I'm moving towards language based systems I should alter the data...
		- NOTE: I can also remove all or most of the config accessor funcs...
		- Note: Some of the configuration can be adjusted globally - such as indent chars ( in to the mapping settings, or panel settings for output? - Panels... for now.. with override available in syntax file or something... )
	-
	-
	-
	-
	-


- edit_settings_plus Tasks
	- Finish edit_settings_plus trying to remain as vanilla as possible for public release...
	- X Add variable expansion for ALL entry methods ( [ base_file, user_file, data - as single or lists ] , file_list [ base_file, user_file, data - as single or lists ] )
	- Add new variables to the variable expansion system
		- Lang for the active Language for the Source View
		- X Syntax for the active Syntax file for the Source View
		- MapSyntax for the active Syntax for the Output View
		- and more...
	- Add res:\\ lookup for files to extract / open if the package ever goes to zipped... and for packages which use edit_settings_plug which ARE zipped - they'll look in the package folder and User, and User\Package\ folder by default.
	-
	- Correct an issue where the active window / view is grabbed when the files are opened instead of when the command is executed ( meaning the syntax var is corrupted - ie if the syntax file isn't opened FIRST then the active view / window will be the previous settings file meaning the syntax will always show JSON or whichever syntax / language is used for each setting.... This should now be resolved.
	-
	-
	-


- Caching System Tasks
	- Ensure it is fully working
	- Add res:\\ lookup for the caching system
	- Export it to it's own class which can be included so it can be more generic / stand-alone for other projects
	- Add Caching of Region data so lines such as Full.Path.To.ClassName.FuncName( args ) will let you double click on Full, Path, To, ClassName and FuncName( _args ) to jump you to those definitions without needing a line number on the line - and so snippets can be added to region mapping, etc... The cache is so the data doesn't need to be reprocessed just to rebuild the region info....


- Regioning System Tasks
	- Add a feature for the region output system and folding... This will mimic the dropdown on an advanced panel elsewhere... IE at the very top of the output there'll be multiple elements per line thanks to the region output system... Click or double-click any of those to hide all other entries except those - so based on class, or special types, etc... This will make using the system so much better / easier... Also add a system to search so click a SEARCH ALL region and the box at the bottom opens and will let you filter ( remove all which doesn't contain the string ) or mask ( Remove all which do contain the string ) ALL output based on the string.... This will be epic for usability...
-
-
-
-
-
-


##
## Settings System...
##
## Definitions File > User Definitions File - these contain constants, etc... things which shouldn't be changed, but can be... primary tab length should go in here since it's part of os or? or maybe I should have OS Settings files... Tab length is different per OS, some use 2, 4 or 8...
## Plugin Settings File > User Plugin Settings File - This file is for the main plugin settings... It is for important decisions such as how many panels to open, whether or not debug / developer modes are active, etc...
##
##

##
## Task List
##
## X - Task: On Load - Detect the opened source file - this needs to be immediate otherwise double-clicks don't know where to go...
## X - Task: On SublimeText Open > On Plugin Loaded - Detect ACMS - Panel... If Open then set active...
## X - Task: Basic structure finished -  Define the absolute structure of the Settings system - Note: This will not follow Sublime Text in terms of settings for the most part, however if the user sets configuration in their main user preferences file - it will override ALL OTHER SETTINGS, aside from main syntax settings file, maybe...
## X - Task: Add detection to determine whether or not line element is at the end of the line
## Task: Add / Create appropriate __str__ object for the plugin object - and extend the AccessorFunc Support Class so it can build an appropriate output based on all of the AccessorFuncs created
## Task:
## Task: Create Region / Object System so users can create their own region types as output - ie their own double-click options so to act as snippet, macro, bookmarks, line number, etc... will be some of the default methods..
## Task: - Add basic Region Mapping so double-clicking an area won't require any elements on / in the actual line....
## Task: - Add Bookmark Detection System so Bookmarks can be added to the new Region Map as a clickable - and bookmarks can be output into a new panel, or the same - maybe at the top?
## Task: - Add Snippets Detetion System so snippets can be added to the new Region Map as a clickable to be inserted in code...
## Task: - Add Line Jump Type option for the Region Mapping System...
## Task: Replace ALL Callback arguments with a single argument of an object containing data from the line, category, if any, and other information such as length of output data, etc...
## Task: Create Region Caching system
## Task:
## Task: OnClosePanel / View - if ACMS - Panel then set to not active
## Task: On Load / Open View - if ACMS - Panel then set to active... - These 3 should greatly simplify how the logic works for everything...
## Task: Because of the region mapping system, the caching system is going to need to not only store the cached data, but it'll need to store the map of data points for each output panel - a way to load it into more than one panel would be ideal so we can switch from one to many... so a world to local and local to world style offset system would be ideal...
## Task:
## Task: Add / Create method for moving data from an old object to a new one...
## Task: Figure out why reloads can take 2 to become live or why it may not until I reload Sublime Text - this is a huge issue... Maybe add reload to the main plugin for all of its imports - I can add a simple system to the dynamic reloader so I can determine whether or not a file has been reloaded since the last time it has been checked with it resetting to false after the refresh ( or after the call - similar to HasCooldown creating a new cooldown if one doesn't exist )
## Task:
## Task: Add Plugin Callbacks for certain events - such as when the config files are updated / saved ( not runtime ) so if panel width, etc.. is updated then the changes will update immediately instead of the user needing to close and re-open the panel...
## Task:
## Task:
## Task: Populate the entire Settings Files with relevant data - possibly use definitions config file for the language / extension maps and more... add the rules to map later and add that feature later..
## Task:
## Task: Alter method for grabbing the mapper name - instead - use Language_ClassName.py instead of ext.py.... the extension will be used via definitions to map to the language name...
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task: Re-Fix issue where closing the ACMS - Panel Manually closes the next focused view...
##









I've done a ton of updates and things are starting to come together...

I'm wondering if I should release almost as is right now ( after I convert all of the callbacks to use an object instead of all of the args ) and implement the region based output system ( multiple click elements per line and many more features ) afterwards ( along with the feature so users can define their own output elements by extending the base region output object ) or if I should get all of that taken care of first....


The downside to releasing earlier is a lot will change - but the benefit is with Package Control it'll auto update and people can start using the stand-alone version now and reporting on any issues...


I had a new idea for the region system - because I wanted to create an advanced gui to either render inside of a sublime text null area, or externally because all IDEs have code browsers with dropdowns and search parameters to filter ( show only things which include text ) or mask ( hide all which include text )... Basically with the region based system I am going to add a Toolbox category at the very top of the panel... under or above Mapping file name with x lines and n entries...

There will be the basic static elements to search using filter or inverse / mask with the panel at the bottom for search opening up and as you type the output updates on the fly...

Then dynamic elements added based on how the user sets up the mapper - by default I'll add classes to the top so by clicking them everything which isn't part of those classes are hidden making the usability of the plugin skyrocket. When using that mode - if you click filter / mask it'll either show / hide everything related to the dynamic objects you click so, by deffault - the classes...

My question to everyone is - especially because I am planning on making this plugin have multiple panels ( with the user / dev of mappers or using config - choosing how many panels there are currently with a design of 3 or 4 with up to 3 stacked and an optional extra vertical for other things ) - Should I, if this search system is enabled in the configuration, open it up in a different panel affixed to the top or bottom sized to fit the contents.... OR should I simply add it to the current output ( meaning if you scroll you can lose sight of it and be required to scroll up )..

Because I'm still working on some of the systems and moving things around to organize everything because it's stand-alone... and because I haven't coded the new design for the output system yet - if I simply output it to the single panel it'll be released faster at the cost of usability if a lot of things are output to the panel....

I'll probably set up the multiple panel system later after it's all set - but it'd be a good idea to ask everyone what they'd prefer... I personally would prefer a toolbox panel with these features at the top or bottom ( with key-binds to make keyboard only working possible )..















Updates Planned
---
###### Similar to the Tasks List except these entries are of work I haven't started yet.

- Updated the Plugin Installer system to allow copying files to the User\ACMS\ folder with logic in place to ensure if the user modified the user files, those will not be replaced - instead the user will be asked what to do - OR the changelog / readme will show the changes between the files for the user to implement them directly, if needed...
-
-

- Backup System Tasks
	- Create Backup System similar to Caching System
	- Add settings to ask how frequently to save a backup ( every n saves, or file size is changed by x ammout, etc.. )
	- Add Settings for how often the backups should be purged ( by number of backups kept, days to keep backups - if number of backups kept is set then this will only delete those OVER the limit of files if they are older than x days IF another setting to combine the behavior is set which it will be by default because otherwise backups will be removed and none saved if only days kept will be set..... etc.. )








Known Issues
---
###### This is a list of bugs or issues which have not been repaired.

- Logic Issues

	- MAJOR
		-
		-

	- Medium
		-
		-

	- Small
		-
		-

- Appearance / Output Issues
	- Nested categories are indented too far - same level as entries..
	-
	-
	-
	-
	-




Resolved Issues
---
###### This is a list of bugs or issues I've encountered - They have been FIXED, but they remain here in good faith to show I know I am not immune to creating issues despite being an expert with nearly 3 decades of development experience under my belt.

- Logic Issues

	- MAJOR
		- All contents of a file were replaced with the Output Panel Contents if the Output View was set incorrectly.. This has been resolved by ensuring we're updating the correct panel before the changes are made, and by ensuring the Output view is correctly set on Auto-Refresh ( which is when the bug would appear )...
		-

	- Medium
		-
		-

	- Small
		- On launch of Sublime-Text, if the user didn't touch or click anything everything loaded fine - but if the user clicked on the ACMS Panel, or the console then for some odd reason the Source View wouldn't get set despite the console stating it was set and nowhere was it reset.. I added a hack so if you use the double-click option on anything in the panel, before source view was set, it'd rescan for it - this corrects the issue but it shouldn't be necessary... I need to track down the root cause and fix it - but for now this hack works... but as I've been saying all along:  Just because it works, doesn't make it right - Josh 'Acecool' Moser 1989 or so...
		-

- Appearance / Output Issues
	-
	-
	-
	-
	-
