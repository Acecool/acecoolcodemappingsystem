##
## Garry's Mod Lua - Default / Vanilla / Base Mapper - Josh 'Acecool' Moser
##


##
## Task List - In order to modify
##
## Task: Copy this file to Packages\User\AcecoolCodeMappingSystem\maps\LanguageName_User.py
## Task: Make a plan or design of what you want to accomplish..
## Task: Begin Modifying...
## Task: Submit bug reports, feature requests or constructive criticism!
## Task: Release?
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Base Mapper Imports
## Note: Because this extends the Lua language, ie all of the base Lua language syntax, etc.. will be added to GMod Lua language files so we'll import Lua or Lua_User as a starting point..
## from AcecoolCodeMappingSystem.maps.__mapper_imports__ import *

## Base Lua.. Since we're loading from a base language we don't need the base mapper imports include as this will have it..
## from AcecoolCodeMappingSystem.maps.Lua import *
from AcecoolCodeMappingSystem.maps.Lua_User import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Garry's Mod Lua Mappings
##
class ACMS_GMod_Lua_Default( ACMS_Lua_User ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_GMod_Lua_Default'


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	##	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	##	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	##	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' )

		##
		## Category Rules
		##


		##
		## Syntax
		##

		## Garry's Mod Lua / GMod Lua / GLua - Note: Because comment-block for Lua starts with comment of Lua, It needs to be in this SPECIFIC ORDER: Comment block begin, COMMENT, COMMENT BLOCK END...
		## If not in this order ie comment first then comment block will bug out... if comment is last then it'll RE INIT comments on lines that have already been processed...
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_BEGIN, SEARCH_TYPE_CONTAINS, '/*' )
		self.AddSyntax( CATEGORY_COMMENT, SEARCH_TYPE_STARTSWITH, '//' )
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_FINISH, SEARCH_TYPE_CONTAINS, '*/' )

		##
		self.AddSyntax( CATEGORY_FUNCTION_ACCESSOR, SEARCH_TYPE_STARTSWITH, 'AccessorFunc' )

		## Function - funtion definitions...
		self.AddSyntax( CATEGORY_FUNCTION_HOOK, SEARCH_TYPE_STARTSWITH, 'hook.Add', 'hook.Remove', 'hook.Run', 'hook.Call', 'hook.GetTable' )

		## Function Callback Related - net.Receive callback and Callback = function, typically used in BulletStruct...
		self.AddSyntax( CATEGORY_FUNCTION_CALLBACK, SEARCH_TYPE_CONTAINS, 'Callback = function', ' = function' )

		## Networking - net.Receive could also be classified as a Callback because it is, but its classification as a networking object may be more useful...
		self.AddSyntax( CATEGORY_NETWORKING, SEARCH_TYPE_STARTSWITH, 'util.AddNetworkString', 'util.NetworkIDToString', 'util.NetworkStringToID' )
		self.AddSyntax( CATEGORY_NETWORKING, SEARCH_TYPE_STARTSWITH, 'net.Start', 'net.Broadcast', 'net.SendToServer', 'net.Receive', 'net.Send', 'net.SendOmit', 'net.SendPVS', 'net.SendPAS', 'net.Incoming', 'net.BytesWritten' )
		self.AddSyntax( CATEGORY_NETWORKING, SEARCH_TYPE_STARTSWITH, 'net.WriteAngle', 'net.WriteBit', 'net.WriteColor', 'net.WriteData', 'net.WriteDouble', 'net.WriteEntity', 'net.WriteFloat', 'net.WriteMatrix', 'net.WriteNormal', 'net.WriteString', 'net.WriteTable', 'net.WriteType', 'net.WriteUInt', 'net.WriteVector' )
		self.AddSyntax( CATEGORY_NETWORKING, SEARCH_TYPE_STARTSWITH, 'net.ReadAngle', 'net.ReadBit', 'net.ReadColor', 'net.ReadData', 'net.ReadDouble', 'net.ReadEntity', 'net.ReadFloat', 'net.ReadMatrix', 'net.ReadNormal', 'net.ReadString', 'net.ReadTable', 'net.ReadType', 'net.ReadUInt', 'net.ReadVector' )

		## Note: These should also be in optimization...
		self.AddSyntax( CATEGORY_NETWORKING, SEARCH_TYPE_STARTSWITH, 'BroadcastLua', 'self:SendLua' )
		self.AddSyntax( CATEGORY_NETWORKING, SEARCH_TYPE_CONTAINS, ':SendLua' )

		## These are in optimization because you should use the net system...
		self.AddSyntax( CATEGORY_OPTIMIZATION, SEARCH_TYPE_CONTAINS, ':GetNetworkAngles', ':SetNetworkAngles', ':GetNetworkedAngle', ':SetNetworkedAngle', ':GetNetworkOrigin', ':SetNetworkOrigin' )
		self.AddSyntax( CATEGORY_OPTIMIZATION, SEARCH_TYPE_CONTAINS, ':GetNetworkedBool', ':SetNetworkedBool', ':GetNetworkedEntity', ':SetNetworkedEntity', ':GetNetworkedFloat', ':SetNetworkedFloat', ':GetNetworkedInt', ':SetNetworkedInt', ':GetNetworkedString', ':SetNetworkedString', ':GetNetworkedVarProxy', ':SetNetworkedVarProxy', ':GetNetworkedVarTable', ':SetNetworkedVarTable', ':GetNetworkedVector', ':SetNetworkedVector' )
		self.AddSyntax( CATEGORY_OPTIMIZATION, SEARCH_TYPE_CONTAINS, ':EntityNetworkedVarChanged', ':NetworkEntityCreated', ':NetworkIDValidated' )
		self.AddSyntax( CATEGORY_OPTIMIZATION, SEARCH_TYPE_STARTSWITH, 'BuildNetworkedVarsTable', 'umsg.Angle', 'umsg.Bool', 'umsg.Char', 'umsg.End', 'umsg.Entity', 'umsg.Float', 'umsg.Long', 'umsg.PoolString', 'umsg.Short', 'umsg.Start', 'umsg.String', 'umsg.Vector', 'umsg.VectorNormal' )

		## Note, I could just use starts with 'cam.'
		self.AddSyntax( CATEGORY_INFO, SEARCH_TYPE_STARTSWITH, 'cam.Start', 'cam.End', 'cam.Push', 'cam.Pop', 'cam.IgnoreZ', 'cam.ApplyShake' )
		self.AddSyntax( CATEGORY_INFO, SEARCH_TYPE_CONTAINS, 'vgui.Create' )

		## Console Commands - Note: I could just use starts with 'concommand.'
		self.AddSyntax( CATEGORY_CMD_CONSOLE, SEARCH_TYPE_STARTSWITH, 'concommand.Add', 'concommand.Remove', 'concommand.Run', 'concommand.AutoComplete', 'concommand.GetTable' )

		## Realm
		self.AddSyntax( CATEGORY_REALM, SEARCH_TYPE_REGEX, '\(\s*(SERVER|CLIENT)\s*\)' )

		## Definitions for Panels, SWEP, etc...
		self.AddSyntax( CATEGORY_DEFINITION, SEARCH_TYPE_STARTSWITH, 'SWEP.', 'PANEL.', 'ENT.' )

		## Possible DRM or Back-Door malicious code detections...
		self.AddSyntax( CATEGORY_BACKDOOR, SEARCH_TYPE_CONTAINS, 'rcon_password', 'server.cfg' )
		self.AddSyntax( CATEGORY_DRM, SEARCH_TYPE_CONTAINS, 'TrackingStatistics', 'StatisticsTracking', 'http.Post' )


		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )


	##
	## Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original ):
		##
		## Make sure _code exists
		##
		## if ( _code == None ):
		##	return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )


		##
		## Don't run in the definitions category so we can see what they are in raw form...
		##
		## if ( self.GetCategoryType( _category ) == CATEGORY_TYPE_FUNCTION ):

		##
		if ( _category == CATEGORY_DRM or _pattern == 'server.cfg' or _pattern == 'rcon_password' ):
			## Grab the data on the line we're interested in..
			_backdoor_check = self.GetFileDataByLine( _line_number - 2 )
			_backdoor_check += self.GetFileDataByLine( _line_number - 1 )
			_backdoor_check += self.GetFileDataByLine( _line_number )
			_backdoor_check += self.GetFileDataByLine( _line_number + 1 )
			_backdoor_check += self.GetFileDataByLine( _line_number + 2 )
			_backdoor_check += self.GetFileDataByLine( _line_number + 3 )
			_backdoor_check += self.GetFileDataByLine( _line_number + 4 )
			_backdoor_check += self.GetFileDataByLine( _line_number + 5 )
			_backdoor_check += self.GetFileDataByLine( _line_number + 6 )
			_backdoor_check += self.GetFileDataByLine( _line_number + 7 )

			if ( AcecoolLib.String.IndexOfExists( 'http.', _code ) ):
				self.AddSimpleEntry( CATEGORY_BACKDOOR, _line_number, '@Backdoor Detected: Script appears to be trying to steal your SERVER CONFIG / RCON PASSWORD giving UNRESTRICTED ACCESS TO EVERYTHING;', _depth )

		## If hook.Run - add a notation so the user knows how hook.Run operates
		if ( _pattern == 'hook.Run' ):
			_code = _code + ' @ hook.Run( _id, _varargs ) - Does NOT execute if GM:[ _id ] does not exist!!! '

		## If hook.Call - add a notation so the user knows how hook.Call operates
		if ( _pattern == 'hook.Call' ):
			_code = _code + ' @ hook.Call( _id, _tab, _varargs ) '


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )


	##
	## Called for each function argument - allows you to do anything you want...
	##
	## example: by replacing _p and _w which are hard-coded variable-names in my coding standard..
	##	function GM:PlayerDeath( _p, _w )
	## can become
	##	function GM:PlayerDeath( <Player>, <Weapon> )
	## or
	##	function GM:PlayerDeath( <Player> _p, <Weapon> _w )
	##
	def OnFunctionArgument( self, _arg, _category, _line_number, _code, _depth, _mode, _pattern ):
		## If a function was used as an argument - it gets replaced with FUNC_REF so we can set what the function should look like - including all args - in place of it.. especially if other arguments come after as multi-line scanning isn't enabled in this mapper..
		if ( _arg == 'FUNC_REF' ):
			if ( _pattern == 'concommand.Add' ):
				_arg = 'function( _p, _cmd, _args, _args_text ) ... end, _autocomplete, _help'


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).OnFunctionArgument( _arg, _category, _line_number, _code, _depth, _mode, _pattern )


	##
	## When an AccessorFunc type is found, this is the callback executed which allows you to expand the AccessorFunc into multiple entries...
	##
	## example: When encountering a GMod Lua / GLua util function, it is expanded into 2 entries representative of the 2 functions AccessorFunc creates..
	##
	## Code:
	##	AccessorFunc( perp.lighting, "somevar", "LightsEnabled", FORCE_BOOL )
	##
	## XCodeMapper Entries - I set it to add into CATEGORY_FUNCTION_ACCESSOR categories:
	##	function perp.lighting:GetLightsEnabled( );:102
	##	function perp.lighting:SetLightsEnabled( _value :: Note: _value forced tobool( ) on set! );:102
	##
	##
	## Because each AccessorFunc creation method is different for each function we look for... they're all routed through here to be expanded upon...
	## If pattern x then args[ 1 ] = classname, args[ 2 ] = name ( so S/Get<Name> etc.. ), and more... so if args[ n ] is "" then we don't add Get prefix to the Getter.. and so on..
	## Also, we can force the data to show up in different categories.. for example __ functions can be put into CATEGORY_FUNCTION_INTERNAL which appears last in list of functions..
	## or we can determine whether or not x function was already added which has an optional creation function ( ie on first time call of an accessor func, it
	##	creates __GetDataTable, __GetData, __SetData internal function but the next time we call it on the same class, it shouldn't output these ).. etc...
	##
	## The benefit of this function is that you can do ANYTHING you want with the data of AccessorFuncs ( I may add callbacks for each category type or search later on, but right now AccessorFunc is what needs it )
	##
	def OnAccessorFuncExpansion( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Base Args - _tab will be the first arg in all cases...
		_arg_tab = self.GetTableValue( _args, _args_count, 0, 'CLASS_NONE' )

		## This is the first arg without any decimals - and we remove everything before last decimal in TitleCase - ClassName ie Timekeeper or Data...
		_arg_class = AcecoolLib.String.SubStr( _arg_tab, 10, len( _arg_tab ) ).title( )


		##
		## Garry's Mod: AccessorFunc( _tab, _key, _name, _force )
		##
		if ( _pattern == 'AccessorFunc' ):
			self.AddAccessorFuncEntries_GModAccessorFunc( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnAccessorFuncExpansion( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


	##
	## Helper to add AccessorFunc data...
	##
	## Garry's Mod lua/includes/util.lua - AccessorFunc adds Get/Set<Name> functions without decent support for forcing data-types..
	##
	## function AccessorFunc( _tab, _key, _name, _crude_force_type )
	## --[[---------------------------------------------------------
	##	AccessorFunc
	##	Quickly make Get/Set accessor fuctions on the specified table
	## -----------------------------------------------------------]]
	## function AccessorFunc( tab, varname, name, iForce )
	##
	def AddAccessorFuncEntries_GModAccessorFunc( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Values...
		## Task: Create helper to easily return these on a single line...
		_tab	= self.GetTableValue( _args, _args_count, 0, 'CLASS_NONE' )
		_key	= self.GetTableValue( _args, _args_count, 1, 'KEY_NONE' )
		_name	= self.GetTableValue( _args, _args_count, 2, 'NAME_NONE' )
		_force	= self.GetTableValue( _args, _args_count, 3, 'FORCE_NONE' )

		## The data we add to the function output...
		_force_info = ''

		## Forcing tostring, number or boolean
		if ( _force == "FORCE_STRING" ):
			_force_info = ':: Note: _value forced tostring( ) on set!'
		elif ( _force == "FORCE_NUMBER" ):
			_force_info = ':: Note: _value forced tonumber( ) on set!'
		elif ( _force == "FORCE_BOOL" ):
			_force_info = ':: Note: _value forced tobool( ) on set!'

		## // function <Class>.Get<Name>( );
		## // function <Class>.Set<Name>( _value <FORCED_TYPE_INFO> );
		## Add the Get<Name> and Set<Name> functions to CATEGORY_FUNCTION category - note, you can use CATEGORY_FUNCTION_ACCESSOR if you want it more out of the way...
		self.AddEntry( CATEGORY_FUNCTION, _line_number, 'function ' + _tab + ':Get' + _name + '( );', _depth, _mode, _pattern )
		self.AddEntry( CATEGORY_FUNCTION, _line_number, 'function ' + _tab + ':Set' + _name + '( _value ' + _force_info + ' );', _depth, _mode, _pattern )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------