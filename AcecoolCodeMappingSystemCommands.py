##
## - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##
import sublime_plugin


## Make sure we grab ACMS / the mapping plugin object from the global namespace...
## global ACECOOL_CODE_MAPPING_SYSTEM, ACMS


##
## Command - Open Sublime Preferences
##
class acms_menu_settings_open_sublime_prefs( sublime_plugin.WindowCommand ):
	##
	def description( self ):
		return '[ ACMS ] Settings: Sublime Text'

	## Enables / Disables the menu item based on whether the active / focused file-tab is set...
	def is_enabled( self ):
		return self.window.active_view( ) is not None

	##
	def run( self ):
		self.window.run_command(
			'edit_settings_plus',
			{
				'developer':	True,
				##'debugging':	True,

				'base_file':	[ '${default}/Preferences.sublime-settings',						'${default}/Preferences (${platform}).sublime-settings',			None ], # '${syntax_path}'
				'user_file':	[ '${user}/Preferences.sublime-settings',							'${user}/Preferences (${platform}).sublime-settings',				'${user}/${syntax}.sublime-settings' ],#'os.path.join( sublime.packages_path( ), 'User', _syntax + '.sublime-settings' ),
				'default':		[ '//\n// Sublime Text - Configuration - User\n//\n{\n\t$0\n}\n',	'//\n// Sublime Text - Configuration - User\n//\n{\n\t$0\n}\n',		'//\n// Acecool - Code Mapping System - ${syntax} Syntax Configuration - User\n//\n{\n\t$0\n}\n' ]
			}
		)


## Make sure we grab ACMS / the mapping plugin object from the global namespace...
## global ACECOOL_CODE_MAPPING_SYSTEM, ACMS


##
## Command - Open Plugin Menu Files
##
class acms_cmd_open_all_menus( sublime_plugin.WindowCommand ):
	##
	def description( self ):
		return '[ ACMS.Dev ] Menus: Open All Menu Files'

	## Enables / Disables the menu item based on whether the active / focused file-tab is set...
	def is_enabled( self ):
		return self.window.active_view( ) is not None

	## Open all of the menu files in the package...
	def run( self ):
		self.window.run_command(
			'edit_settings_plus',
			{
				## Allow editing of the files..
				'developer':	True,

				## use edit_settings method of opening files, although edit_settings only allows a single string, this allows a list...
				'base_file':
				[
					'${package}/menus/Context.sublime-menu',
					'${package}/menus/Find in Files.sublime-menu',
					'${package}/menus/Main.sublime-menu',
					## '${package}/menus/Package.sublime-menu',
					'${package}/menus/Side Bar Mount Part.sublime-menu',
					'${package}/menus/Side Bar.sublime-menu',
					'${package}/menus/Tab Context.sublime-menu',
					'${package}/menus/Widget Context.sublime-menu',
				]
			}
		);


##
## Command - Open KeyBinds
##
class acms_menu_settings_open_keybinds( sublime_plugin.WindowCommand ):
	##
	##
	##
	__default_keymap__				= '//\n// Sublime Text - Key-Map - User\n//\n{\n\t$0\n}\n'
	__default_mousemap__			= '//\n// Sublime Text - Mouse-Map - User\n//\n{\n\t$0\n}\n'
	__default_platform_keymap__		= '//\n// Sublime Text - Key-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'
	__default_platform_mousemap__	= '//\n// Sublime Text - Mouse-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'

	##
	def description( self ):
		return '[ ACMS ] Settings: Key Bindings'

	## Enables / Disables the menu item based on whether the active / focused file-tab is set...
	def is_enabled( self ):
		return self.window.active_view( ) is not None

	##
	def run( self ):
		## _default_binds_platform_file = '//\n// Acecool - Code Mapping System - Key-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'
		## _default_binds_platform_file = '//\n// Acecool - Code Mapping System - Key-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'
		self.window.run_command(
			'edit_settings_plus',
			{
				'developer':	True,
				##'debugging':	True, # '${default}/Default.sublime-keymap',

				## 'base_file':	[ '${default}/Default (${platform}).sublime-keymap',	'${default}/Default (${platform}).sublime-mousemap',	'${package}/keymaps/Default.sublime-keymap',		'${package}/keymaps/Default (${platform}).sublime-keymap',		'${package}/keymaps/Default (${platform}).sublime-mousemap' ],
				## 'user_file':	[ '${user}/Default (${platform}).sublime-keymap',		'${user}/Default (${platform}).sublime-mousemap',		'${user}/Default.sublime-keymap',					'${user}/Default (${platform}).sublime-mousemap',				'${user}/Default.sublime-mousemap' ],
				## 'default':		[ self.__default_platform_keymap__,						self.__default_platform_mousemap__,						_default_binds_platform_file,						_default_binds_platform_file ],

				'file_list':
				[
					## Sublime Text Key/MouseMaps
					{ 'base_file': '${default}/Default.sublime-keymap',							'user_file': '${user}/Default.sublime-keymap',						'default': self.__default_keymap__ },
					{ 'base_file': '${default}/Default (${platform}).sublime-keymap',			'user_file': '${user}/Default (${platform}).sublime-keymap',		'default': self.__default_platform_keymap__ },
					{ 'base_file': '${default}/Default.sublime-mousemap',						'user_file': '${user}/Default.sublime-mousemap',					'default': self.__default_mousemap__ },
					{ 'base_file': '${default}/Default (${platform}).sublime-mousemap',			'user_file': '${user}/Default (${platform}).sublime-mousemap',		'default': self.__default_platform_mousemap__ },

					## Package Key/MouseMaps
					{ 'base_file': '${package}/keymaps/Default.sublime-keymap',					},
					{ 'base_file': '${package}/keymaps/Default (${platform}).sublime-keymap',	},
					{ 'base_file': '${package}/keymaps/Default.sublime-mousemap',				},
					{ 'base_file': '${package}/keymaps/Default (${platform}).sublime-mousemap',	},
					## { 'base_file': '',		'user_file': '',		'default': '' },
					## { 'base_file': '',		'user_file': '',		'default': '' },
					## { 'base_file': None,											'user_file': '',			'default': '' },
				],

			}
		)


##
## Command - Open Syntax Configuration
##
class acms_menu_settings_open_syntax( sublime_plugin.WindowCommand ):
	##
	def description( self ):
		return '[ ACMS ] Settings: Active Syntax'

	## Enables / Disables the menu item based on whether the active / focused file-tab is set...
	def is_enabled( self ):
		return self.window.active_view( ) is not None

	##
	def run( self ):
		## _view = self.window.active_view( )
		## _syntax, _ = os.path.splitext( os.path.basename( _view.settings( ).get( 'syntax' ) ) )
		_default_syntax = '//\n// Acecool - Code Mapping System - Syntax Configuration - ${syntax} - These settings override both User and Default settings for the syntax - User\n//\n{\n\t$0\n}\n'

		##
		self.window.run_command(
			'edit_settings_plus',
			{
			 	'developer':	True,
				##'debugging':	True,

				'base_file':	'${default}/Preferences.sublime-settings', # '${default}/${syntax_path}'
				'user_file':	'${user_syntax}', # '${user}/${syntax}.sublime-settings', # '${user}/${syntax_path}' - User/JavaScript/JSON.sublime-settings for example... # os.path.join( sublime.packages_path( ), 'User', _syntax + '.sublime-settings' )		## 'user_file':	'${syntax}',
				'default':		_default_syntax,
			}
		)


##
## Command - Convert Spaces to Tabs ( Sublime Text [ Start / End of line ] + Acecool Variant [ Inline - ie alignment, etc.. ] )
##
class acecool_convert_spaces_to_tabs( sublime_plugin.WindowCommand ):
	##
	def description( self ):
		## return '[ ACMS.Dev ] Convert ALL Spaces to Tabs ( Translate, 4, 3, 2, s+1, 1+s )'
		return '[ ACMS.Dev ] Convert Spaces to Tabs for the ACTIVE VIEW!'

	## Enables / Disables the menu item based on whether the active / focused file-tab is set...
	def is_enabled( self ):
		return self.window.active_view( ) is not None

	##
	def run( self ):
		##
		_view = self.window.active_view( )

		## Convert spaces to tabs - note this will likely only convert the spaces at the start and end of a line, but will not fix alignment issues from converting tabs to spaces...
		self.window.run_command( 'unexpand_tabs', { "set_translate_tabs": True, } )


##
## Command - Convert Tabs to Spaces ( Sublime Text [ Start / End of line ] + Acecool Variant [ Inline - ie alignment, etc.. ] )
##
class acecool_convert_tabs_to_spaces( sublime_plugin.WindowCommand ):
	##
	def description( self ):
		## return '[ ACMS.Dev ] Convert ALL Spaces to Tabs ( Translate, 4, 3, 2, s+1, 1+s )'
		return '[ ACMS.Dev ] Convert Tabs to Spaces for the ACTIVE VIEW!'

	## Enables / Disables the menu item based on whether the active / focused file-tab is set...
	def is_enabled( self ):
		return self.window.active_view( ) is not None

	##
	def run( self ):
		##
		_view = self.window.active_view( )

		## Convert all \t characters to tab_size * ' ' spaces... Note: This will likely ruin = alignment in a file as the inverse operation doesn't fix tabs in-line, only at the start and end...
		self.window.run_command( 'expand_tabs', { "set_translate_tabs": True, } )



## Make sure we grab ACMS / the mapping plugin object from the global namespace...
## global ACECOOL_CODE_MAPPING_SYSTEM, ACMS


##
## Command - Open All Plugin Preferences
##
class acms_menu_settings_open_plugin_prefs( sublime_plugin.WindowCommand ):
	## Defaults
	__default_prefs__				= '//\n// Sublime Text - Configuration - User\n//\n{\n\t$0\n}\n'
	__default_prefs_platform__		= '//\n// Sublime Text - ${platform} Configuration - User\n//\n{\n\t$0\n}\n'

	## Package Defaults
	__default_package_definitions__	= '//\n// Acecool - Code Mapping System - Plugin Definitions - User\n//\n{\n\t$0\n}\n'
	__default_package_panels__		= '''//
// Acecool - Code Mapping System - Mapping Panel( s ) Configuration - User
//
{
	$0


	//
	// Examples using the new "acms_language_config->Language_Or_Project_Name_From_Extension_To_Unique_ID->Table_Key": "VALUE", system so the entire table doesn't need to be copied
	//		in order to make a change to a single key / value.... It also works on keys / values which aren't in the default table - anything you want to replace you can either copy
	//		the entire table over for the language / project id and make the changes, or do the following... Both examples are provided below... 4 examples showing the new method, and
	//		one example showing the entire table copied over with a few changes from the default - for sake of keeping this file short, I've removed all of the default commented keys...
	//

	// AutoHotkey Config
	// "acms_language_config->AutoHotkey->language_name":	"AutoHotkey: Acecool AHK Framework",
	// "acms_language_config->AutoHotkey->active_mapper":	"AcecoolAHK_Framework",

	// Garry's Mod Lua Config
	// "acms_language_config->GMod_Lua->language_name":	"Garry's Mod Lua :: Acecool Dev Framework",
	// "acms_language_config->GMod_Lua->active_language":	"GMod_Lua",
	// "acms_language_config->GMod_Lua->active_mapper":	"AcecoolDev_Framework",

	// Python Config
	// "acms_language_config->Python->language_name":		"Python: Acecool Code Mapping System",
	// "acms_language_config->Python->active_mapper":		"ACMS",

	// PHP Config
	// "acms_language_config->PHP->language_name":			"PHP: Acecool Content Management System",
	// "acms_language_config->PHP->active_mapper":			"ACMS",


	//
	// Python - A High-Level User/Beginner-Friendly, but POWERFUL, Language which imposes a few good standards ( forced tabbing ) but can introduce some very bad habits ( variable scope doesn't matter as much in Python as it does in Lua, C, etc.. )!
	//
	// Note: With this example, you need to copy EVERY key in the table which is used... If I were to delete Language Name or active_mapper it wouldn't detect it in the default as
	//		SublimeText doesn't have an inheritance system in place for configuration files so it'd see the global default which is "Default" instead of "Python".. This is why the
	//		new method above saves time and a lot of space in a file as you can make changes to the key / value you want to target - one or more - without needing to copy all of the
	//		other / default values...
	//
	// Note: Comments are added to prevent this table from overriding defaults... If you want to use this, remove the FIRST instance of //s from all lines starting with
	//		"acms_language_config->python" all the way to }, right after "output_category_suffix": ...
	//
	//"acms_language_config->Python":
	//{
	//	// The language name... - I updated this to reflect the mapper being used so the console output will show this as the active language showing that it is working...
	//	"language_name":								"Python: Acecool Code Mapping System",
	//
	//	// The Language Identifier ( Used in file-names )
	//	"active_language":								"Python",
	//
	//	// The Default language mapper ( IE AutoHotkey_Default.py in maps/ - Case Sensitive ) - I set this to ACMS
	//	"active_mapper":								"ACMS",
	//
	//	// Override the default Syntax Highlighter File to use for this language
	//	"view_syntax_source":							"Python/Python.sublime-syntax",
	//	// "view_syntax_source":							"AcecoolCodeMappingSystem/syntax_files/Python.sublime-syntax",
	//
	//	// Override the Syntax Highlighter File to use for the Output Window
	//	"view_syntax_output":							"AcecoolCodeMappingSystem/syntax_files/Python.sublime-syntax",
	//
	//	// Simple function definition replacement to shorten output
	//	"function_name_search":							"def",
	//
	//	// Define the chars to add to the start of a Task line output - this helps prevent discoloring the entire output panel because of a quote or other character..
	//	"output_task_line_prefix":						"## ",
	//
	//	// Define the chars to add at the end of a Task line output - this helps prevent discoloring the entire output panel because of a quote or other character..
	//	"output_task_line_suffix":						"",
	//
	//	// The default suffix set of characters to use at the end of a category line in the output panel...
	//	"output_category_suffix":						" ##",
	//},
}
'''

	## Base Default File Content
	__default_package_plugin__		= '//\n// Acecool - Code Mapping System - Plugin Configuration - User\n//\n{\n\t$0\n}\n'
	__default_package_mapping__		= '//\n// Acecool - Code Mapping System - Mapping Configuration - User\n//\n{\n\t$0\n}\n'
	__default_package_runtime__		= '//\n// Acecool - Code Mapping System - Plugin RunTime Configuration - User - EDIT AT YOUR OWN RISK!\n//\n{\n\t$0\n}\n'

	## Syntax Default File Content
	__default_active_syntax__		= '//\n// Acecool - Code Mapping System - ${syntax} Syntax Configuration - User\n//\n{\n\t$0\n}\n'
	__default_syntax__				= '//\n// Acecool - Code Mapping System - Syntax Configuration - ${syntax} - These settings override both User and Default settings for the syntax - User\n//\n{\n\t$0\n}\n'

	## Syntax File Links
	__syntax_python__				= 'Packages/Python/Python.sublime-syntax'
	__syntax_javascript__			= 'Packages/JavaScript/JavaScript.sublime-syntax'

	## Key / Mouse Map Default File Content
	__default_keymap__				= '//\n// Sublime Text - Key-Map - User\n//\n{\n\t$0\n}\n'
	__default_mousemap__			= '//\n// Sublime Text - Mouse-Map - User\n//\n{\n\t$0\n}\n'
	__default_platform_keymap__		= '//\n// Sublime Text - Key-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'
	__default_platform_mousemap__	= '//\n// Sublime Text - Mouse-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'

	##
	def description( self ):
		return '[ ACMS ] Settings: Plugin'

	## Enables / Disables the menu item based on whether the active / focused file-tab is set...
	def is_enabled( self ):
		return self.window.active_view( ) is not None

	##
	def run( self ):
		self.window.run_command(
			'edit_settings_plus',
			{
				'developer':	True,
				##'debugging':	True,
				## RunTime > Map > Plugin > Panel > Definitions > Default...
				'file_list':
				[
					## 'caption':			'Plugin Map Settings - Default',
					## 'caption':			'Plugin Map Settings - Default',
					## 'caption':			'Plugin Map Settings - Default',
					## 'caption':			'Plugin Map Settings - Default',
					## 'caption':			'Plugin Map Settings - Default',

					## 'caption':			'${syntax} Syntax Settings'											'${packages}/${syntax_path}'		== JavaScript/JavaScript.sublime-syntax
					{ 'base_file': '${package}/ACMS_Plugin.sublime-settings',									'user_file': '${user}/ACMS_Plugin.sublime-settings',					'default': self.__default_package_plugin__,				"syntax": None },
					{ 'base_file': '${package}/ACMS_Mapping.sublime-settings',									'user_file': '${user}/ACMS_Mapping.sublime-settings',					'default': self.__default_package_mapping__,			"syntax": None },
					{ 'base_file': '${package}/ACMS_Definitions.sublime-settings',								'user_file': '${user}/ACMS_Definitions.sublime-settings',				'default': self.__default_package_definitions__,		"syntax": None },
					{ 'base_file': '${package}/ACMS_Panels.sublime-settings',									'user_file': '${user}/ACMS_Panels.sublime-settings',					'default': self.__default_package_panels__,				"syntax": None },
					{ 'base_file': '${package}/ACMS_RunTime.sublime-settings',									'user_file': '${user}/ACMS_RunTime.sublime-settings',					'default': self.__default_package_runtime__,			"syntax": None },

					## Base / Left Group File																	User / Right Group File													Default user_file contents								Syntax - Force syntax highlighter for the file( s ) opened by this line... It's best to separate the files if they're of different type or if you want a different highlighter..
					{ 'base_file': '${default}/Preferences.sublime-settings',									'user_file': '${user}/Preferences.sublime-settings',					'default': self.__default_prefs__,						"syntax": None },
					{ 'base_file': '${default}/Preferences (${platform}).sublime-settings',						'user_file': '${user}/Preferences (${platform}).sublime-settings',		'default': self.__default_prefs_platform__,				"syntax": None },

					## Syntax Files
					{ 'base_file': None,																		'user_file': '${user}/${syntax}.sublime-settings',						'default': self.__default_active_syntax__,				"syntax": self.__syntax_javascript__ },
					{ 'base_file': None,																		'user_file': '${user_syntax}',											'default': self.__default_syntax__,						"syntax": None },

					## Sublime Text Key/MouseMaps
					## { 'base_file': '${default}/Default.sublime-keymap',										'user_file': '${user}/Default.sublime-keymap',							'default': self.__default_keymap__										},
					## { 'base_file': '${default}/Default.sublime-mousemap',									'user_file': '${user}/Default.sublime-mousemap',						'default': self.__default_mousemap__									},
					{ 'base_file': '${default}/Default (${platform}).sublime-keymap',							'user_file': '${user}/Default (${platform}).sublime-keymap',			'default': self.__default_platform_keymap__								},
					## { 'base_file': '${default}/Default (${platform}).sublime-mousemap',						'user_file': '${user}/Default (${platform}).sublime-mousemap',			'default': self.__default_platform_mousemap__							},

					## Package Key/MouseMaps
					{ 'base_file': '${package}/keymaps/Default.sublime-keymap',					},
				],
			},
		)










## Make sure we grab ACMS / the mapping plugin object from the global namespace...
## global ACECOOL_CODE_MAPPING_SYSTEM, ACMS


## { "caption": "Convert Indentation to Tabs", "command": "unexpand_tabs", "args": { "set_translate_tabs": true } },




		## self.window.run_command(
		##  'expand_tabs',
		##  {
		##    "set_translate_tabs": False,
		##    'args':
		##    {
		##     "set_translate_tabs": False,
		##    }
		##  }
		## )


				## 'args':
				## {
				##  "set_translate_tabs": True,
				## }

		## self.window.run_command(
		##  'toggle_setting',
		##  {
		##      "setting": "translate_tabs_to_spaces",
		##      'args':
		##      {
		##       "setting": "translate_tabs_to_spaces",
		##      }
		##  }
		## )
		## _view = self.window.active_view( )
		## _syntax, _ = os.path.splitext( os.path.basename( _view.settings( ).get( 'syntax' ) ) )

		## Convert all spaces to tabs at the start / end of a line using Sublime Text Method
		## self.window.run_command(
		##  'unexpand_tabs',
		##  {
		##    'set_translate_tabs': True
		##  }
		## )

		## Find and replace all tabs using my standard formula - ie for: number of spaces to tabs - convert that number to tabs in the document, subtract 1 and repeat. Do not convert single space to tab.
		## ## Set whether to use tab indent or spaces...
		## if ( self.GetSetting( 'panel_use_tab_indent_option', True ) ):
		##  _settings.set( 'translate_tabs_to_spaces', False )
		##  _settings.set( 'expand_tabs', False )
		##  _settings.set( 'unexpand_tabs', True )
		## else:
		##  _settings.set( 'translate_tabs_to_spaces', True )
		##  _settings.set( 'unexpand_tabs', False )
		##  _settings.set( 'expand_tabs', True )

				## { "command": "toggle_setting", "args": {"setting": "translate_tabs_to_spaces"}, "caption": "Indent Using Spaces", "checkbox": true },

				## { "command": "expand_tabs", "caption": "Convert Indentation to Spaces", "args": {"set_translate_tabs": true} },
				## { "command": "unexpand_tabs", "caption": "Convert Indentation to Tabs", "args": {"set_translate_tabs": true} }



		##
		## The following 6 are identical... you can use Packages/ or not, it is removed during processing... res://PackageName/Path/To/File.ext is all that is needed...
		##

		## ## Trying to load a res file..
		## { 'base_file': 'res://Default/Default (${platform}).sublime-keymap',						'user_file': '${user}/Default (${platform}).sublime-keymap',			'default': self.__default_platform_keymap__								},
		## { 'base_file': 'res://Default/Preferences.sublime-settings',								'user_file': '${user}/Preferences.sublime-settings',					'default': self.__default_prefs__								},
		## { 'base_file': 'res://Default/Preferences (${platform}).sublime-settings',					'user_file': '${user}/Preferences (${platform}).sublime-settings',		'default': self.__default_prefs_platform__								},
		## ## Trying to load a res file..
		## { 'base_file': 'res://Packages/Default/Default (${platform}).sublime-keymap',				'user_file': '${user}/Default (${platform}).sublime-keymap',			'default': self.__default_platform_keymap__								},
		## { 'base_file': 'res://Packages/Default/Preferences.sublime-settings',						'user_file': '${user}/Preferences.sublime-settings',					'default': self.__default_prefs__								},
		## { 'base_file': 'res://Packages/Default/Preferences (${platform}).sublime-settings',			'user_file': '${user}/Preferences (${platform}).sublime-settings',		'default': self.__default_prefs_platform__								},


		##
		## ...
		##

		## { 'base_file': '${package}/keymaps/Default (${platform}).sublime-keymap',	},
		## { 'base_file': '${package}/keymaps/Default.sublime-mousemap',				},
		## { 'base_file': '${package}/keymaps/Default (${platform}).sublime-mousemap',	},




	## ##
	## __default_keymap__				= '//\n// Sublime Text - Key-Map - User\n//\n{\n\t$0\n}\n'
	## __default_mousemap__			= '//\n// Sublime Text - Mouse-Map - User\n//\n{\n\t$0\n}\n'
	## __default_platform_keymap__		= '//\n// Sublime Text - Key-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'
	## __default_platform_mousemap__	= '//\n// Sublime Text - Mouse-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'
	## 				## Sublime Text Key/MouseMaps
	## 				{ 'base_file': '${default}/Default.sublime-keymap',							'user_file': '${user}/Default.sublime-keymap',						'default': self.__default_keymap__ },
	## 				{ 'base_file': '${default}/Default (${platform}).sublime-keymap',			'user_file': '${user}/Default (${platform}).sublime-keymap',		'default': self.__default_platform_keymap__ },
	## 				{ 'base_file': '${default}/Default.sublime-mousemap',						'user_file': '${user}/Default.sublime-mousemap',					'default': self.__default_mousemap__ },
	## 				{ 'base_file': '${default}/Default (${platform}).sublime-mousemap',			'user_file': '${user}/Default (${platform}).sublime-mousemap',		'default': self.__default_platform_mousemap__ },

	## 				## Package Key/MouseMaps
	## 				{ 'base_file': '${package}/keymaps/Default.sublime-keymap',					},
	## 				{ 'base_file': '${package}/keymaps/Default (${platform}).sublime-keymap',	},
	## 				{ 'base_file': '${package}/keymaps/Default.sublime-mousemap',				},
	## 				{ 'base_file': '${package}/keymaps/Default (${platform}).sublime-mousemap',	},





				## { 'base_file':	'${default}/Preferences.sublime-settings',				'user_file':	'${user_syntax}',				'default':		_default_syntax, },

				## { 'base_file':	'${default}/Preferences.sublime-settings', # '${default}/${syntax_path}'
				## 'user_file':	'${user_syntax}', # '${user}/${syntax}.sublime-settings', # '${user}/${syntax_path}' - User/JavaScript/JSON.sublime-settings for example... # os.path.join( sublime.packages_path( ), 'User', _syntax + '.sublime-settings' )		## 'user_file':	'${syntax}',
				## 'default':		_default_syntax,

		## '${syntax_path}'
		## 'Plugin Map Settings - Default', #'os.path.join( sublime.packages_path( ), 'User', _syntax + '.sublime-settings' ),
		## _view = self.window.active_view( )
		## _syntax, _ = os.path.splitext( os.path.basename( _view.settings( ).get( 'syntax' ) ) )

				## Note: If view isn't set when syntax var is used then it'll use the active window / view to grab that info - ie if syntax isn't the first file opened then the previous active view will be a settings file so it'll be JSON... This won't be necessary soon, hopefully - but for now it is..
				## 'view':			self.window.active_view( ),

				## Note: If view isn't set when syntax var is used then it'll use the active window / view to grab that info - ie if syntax isn't the first file opened then the previous active view will be a settings file so it'll be JSON... This won't be necessary soon, hopefully - but for now it is..
				## 'view':			self.window.active_view( ),

				## Note: If view isn't set when syntax var is used then it'll use the active window / view to grab that info - ie if syntax isn't the first file opened then the previous active view will be a settings file so it'll be JSON... This won't be necessary soon, hopefully - but for now it is..
				## 'view':			self.window.active_view( ),

				## Note: If view isn't set when syntax var is used then it'll use the active window / view to grab that info - ie if syntax isn't the first file opened then the previous active view will be a settings file so it'll be JSON... This won't be necessary soon, hopefully - but for now it is..
				## 'view':			self.window.active_view( ),


				## Note: If view isn't set when syntax var is used then it'll use the active window / view to grab that info - ie if syntax isn't the first file opened then the previous active view will be a settings file so it'll be JSON... This won't be necessary soon, hopefully - but for now it is..
				## 'view':			self.window.active_view( ),


					## 'caption':				'Sublime Text - Settings',
					## { 'base_file': '${default}/Preferences.sublime-settings',	'user_file': '${user}/Preferences.sublime-settings',		'default': '//\n// Sublime Text - Configuration - User\n//\n{\n\t$0\n}\n' },

					## 'caption':				'Plugin Settings - Definitions',
					## { 'base_file': '${package}/settings/plugin_definitions.sublime-settings',	'user_file': '${user_package}/plugin_definitions.sublime-settings',	'default': '//\n// Acecool - Code Mapping System - Plugin Definitions - User\n//\n{\n\t$0\n}\n' },

					## 'caption':				'Plugin Settings - RunTime',
					## { 'base_file': '${package}/settings/plugin_runtime.sublime-settings',		'user_file': '${user_package}/plugin_runtime.sublime-settings',		'default': '//\n// Acecool - Code Mapping System - Plugin RunTime Settings - User - Edit at your own risk!\n//\n{\n\t$0\n}\n' },

					## 'caption':				'Plugin Settings - General',
					## { 'base_file': '${package}/settings/plugin_main.sublime-settings',			'user_file': '${user_package}/plugin_main.sublime-settings',		'default': '//\n// Acecool - Code Mapping System Plugin Settings - User\n//\n{\n\t$0\n}\n' },

					## 'caption':				'Plugin Map Settings - Default',
					## { 'base_file': '${package}/settings/plugin_mapping.sublime-settings',		'user_file': '${user_package}/plugin_mapping.sublime-settings',		'default': '//\n// Acecool - Code Mapping System - Globally Inherited Mapper Default Settings - User\n//\n{\n\t$0\n}\n' },

		## Open the mapping preferences too
		## sublime.active_window( ).run_command( 'acms_menu_settings_open_plugin_map_prefs' )



