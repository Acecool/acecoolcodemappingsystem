##
## Lua - Default / Vanilla / Base Mapper - Josh 'Acecool' Moser
##


##
## Task List -
##
## Task: Set up Sublime Text Symbols Mapper as alternative to file-parsing... OR precursor so the default can use symbols and other mappers can add on to that - or I'll add an additional layer...
## Task:
## Task:
## Task:
## Task:
##


##
## Task List -
##
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## Base Mapper Imports
from AcecoolCodeMappingSystem.maps.__mapper_imports__ import *




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##
##
class ACMS_Lua_Default( XCodeMapperBase ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'ACMS_Lua_Default'


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	##	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	##	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	##	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( ACMS_PRINT_TYPE_INIT, 'OnSetupSyntax' )

		## Lua Vanilla - Note: Because comment-block for Lua starts with comment of Lua, It needs to be in this SPECIFIC ORDER: Comment block begin, COMMENT, COMMENT BLOCK END...
		## If not in this order ie comment first then comment block will bug out... if comment is last then it'll RE INIT comments on lines that have already been processed...
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_BEGIN, SEARCH_TYPE_CONTAINS, '--[[' )
		self.AddSyntax( CATEGORY_COMMENT, SEARCH_TYPE_STARTSWITH, '--' )
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_FINISH, SEARCH_TYPE_CONTAINS, ']]' )

		## Capture print statements and add them to the debugging category - print statements can be very poor for performance, use them sparingly and only when debugging or making sparse reports of startup, shutdown, and the like procedures..
		self.AddSyntax( CATEGORY_DEBUGGING, SEARCH_TYPE_CONTAINS, 'print(', 'PrintTable(' )
		self.AddSyntax( CATEGORY_DEBUGGING, SEARCH_TYPE_STARTSWITH, 'debug.' )

		## Function - funtion definitions...
		self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_STARTSWITH, 'function ' )
		self.AddSyntax( CATEGORY_FUNCTION_LOCAL, SEARCH_TYPE_STARTSWITH, 'local function ' )
		self.AddSyntax( CATEGORY_FUNCTION_OTHER, SEARCH_TYPE_CONTAINS, '= function' )

		## Constants and Important Variables ( all in uppercase, certain prefixes go in one location, others elsewhere )
		self.AddSyntax( CATEGORY_CONFIG, SEARCH_TYPE_REGEX, '^CFG_\w+\s+\=' )
		self.AddSyntax( CATEGORY_ENUMERATION, SEARCH_TYPE_REGEX, '^ENUM_\w+\s+\=' )
		self.AddSyntax( CATEGORY_CONSTANT, SEARCH_TYPE_REGEX, '^CONST_\w+\s+\=', '^[A-Z_]+\s+\=' )

		## This will detect all variable usage and declaration...
		## self.AddSyntax( CATEGORY_VARIABLE, SEARCH_TYPE_REGEX, '(local\s*)?([a-zA-Z_.0-9]+)(\W*)(([a-zA-Z_.0-9]+)?([, ]*)?)' ) )
		## Sublime text issue? (?!end|nd|d) No - it's because when a match is made, the next match looks at that position forward instead of that
		## _look_ahead = Acecool.RegEx.BuildSublimeTextLookAheadString( 'end', 'then', 'else', 'elif', 'if', 'local', 'return', 'break', 'true', 'false' )
		## self.AddSyntax( CATEGORY_VARIABLE, SEARCH_TYPE_REGEX, '([A-Za-z_]{1}[A-Za-z0-9_.]+)+' )
		## _look_ahead = Acecool.RegEx.BuildSublimeTextLookAheadString( 'end|then|else|elif|if|local|return|break|true|false' )
		##(?!end|then|else|elif|if|local|return|break|true|false)

		## Working for the most part - still need to exclude items within strings instead of only first, etc...
		## self.AddSyntax( CATEGORY_VARIABLE, SEARCH_TYPE_REGEX, '([A-Za-z_\"\']{1}[A-Za-z0-9_.:\(\"\']+)+' )


		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )


	##
	## Callback - Variables detected are passed through here... This is so we can determine errors because of variables in case-sensitive languages are incorrectly typed, not declared, etc... Even in loosely declared languages, they typically need to be set before trying to read a value or perform math, etc..
	##
	def OnVariableDetection( self, _category, _line_number, _code, _depth, _search_mode, _pattern ):

		## ## Basic rules for Lua - if it is a key-word, ignore it... If it starts with a number, ignore it...
		## ## _result = re.findall( _pattern, _code )
		## ## _results = str( _result )

		## ## _result = re.search( _pattern, _code )
		## ## _results = str( _result.group( 0 ) )
		## ## _tab = _result.groups( )

		## _result = Acecool.string.FindAll( _pattern, _code )
		## _results = str( _result )

		## _output = Acecool.string.FormatColumn( 50, _code, 300, 'Variables: ' + _results )
		## print( _output )

		## ## Add the line for debugging purposes...
		## self.AddSimpleEntry( _category, _line_number, _output, _depth )

		pass


	##
	## Override / Helper to allow for depth to be all uniform or based on indentation ( if you return _depth )
	##
	def OnCalcDepth( self, _category, _line_number, _code, _depth, _mode, _pattern ):
		return _depth


	## Helpers
	function_local_search = 'local '
	function_local_replace = 'L-'


	##
	## Helper - Returns the indent level of code on a certain line - Name longer so when I add this helper it won't be overwritten..
	##
	def GetIndentLevelForCodeInLine( self, _code ):
		## If there is no code on the line, return 0
		if ( _code.strip( ) == 0 ):
			return 0

		## Otherwise return the indent length which can be 0 to whatever ( this is number of chars so can be spaces, tabs, etc... doesn't matter because it'll match as long as the user uses the same indent chars throughout )..
		return len( _code ) - len( _code.lstrip( ) )


	##
	## Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original ):
		##
		## Make sure _code exists
		##
		## if ( _code == None ):
		##	return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )


		##
		## Don't run in the definitions category so we can see what they are in raw form...
		##
		if ( self.GetCategoryType( _category ) == CATEGORY_TYPE_FUNCTION ):
			##
			if ( _mode and _pattern ):
				##
				## Functional code finding the number of lines a function uses... Note: This is still a bit of a hack...
				##
				## If the text starts with local, then we get rid of that...
				##  + self.GetSetting( 'function_name_search', 'ƒ' )
				## if ( _code.startswith( self.GetSetting( 'function_local_name_search', 'local ' ) ) ):
				## 	_code = self.GetSetting( 'function_local_name_replace', 'l-' ) + _code[ len( self.GetSetting( 'function_local_name_search', 'local ' ) ) : ] #+ self.GetSetting( 'function_name_replace', 'ƒ' )

				## Grab the level of indent so we know what to look for ( this is in lieu of having an actual end / depth tracker for Lua implemented yet - this won't be accurate
				## unless the coder properly tabs code, and puts the end on its own line at the same indent level as the function start line )
				_func_depth = self.GetIndentLevelForCodeInLine( _code )

				## Track the number of lines we've gone
				_i = 0

				## Try to find the function ending so we can add the number of lines the function has... We'll proceed until we find it, or the end of the file...
				while ( ( _line_number + _i ) < self.GetFileLines( ) ):
					## Increment number of lines we've gone..
					_i += 1

					## If we've reached the end of the file, we need to break...
					## if ( ( _line_number + _i ) >= self.GetFileLines( ) ):
					## 	break

					## Grab the data on the line we're interested in..
					_data = self.GetFileDataByLine( _line_number + _i )

					## Make sure we're not on an empty line..
					if ( _data.strip( ) != '' ):
						## Grab the depth..
						_line_depth = self.GetIndentLevelForCodeInLine( _data )

						## If the line starts with end, and is the same depth level, then this is the line number...
						## Note: We don't need this :: and _code.startswith( 'end' )
						## 		because if code is at the same indent level, then we've likely stumbled across another function, comment for the next func, end, etc... so we only check for depth - simple..
						if ( _line_depth <= _func_depth ):
							## Exit the loop... _i will already be set...
							break

				## Helper: The total number of lines for the function including definition line, last end, and the body of the func..
				_function_len = _i + 1
				_function_end = _line_number + _function_len

				## Add the number of lines... + 1 for the function header, otherwise we could subtract 1 so it'd just be the body of the fund...
				_code = _code + ' @ ' + str( _function_len ) + ' lines ' + str( _line_number ) + ' - ' + str( _function_end ) + ' ---'





				##
				## System which breaks apart A.B.C:Func( args ) via decimals and :s..
				##
				if ( self.GetSetting( 'output_library_functions_in_new_category', False ) ):
					##
					## Helpers
					##

					## Decimal Delimeter
					_dot_delim		= _code.rfind( '.' )
					_dot_count		= _code.count( '.' )

					## Checks to see if a Colon exists in the line of code - : and . can be used almost interchangeably with the self arg being hidden when using :
					_colon_delim	= AcecoolLib.String.IndexOf( ':', _code )
					_colon_count	= _code.count( ':' )
					## _colon_delim	= AcecoolLib.String.IndexOfExists( ':', _code, True )


					## print( '[ --- ] DOT COUNT: ' + str( _dot_count ) + ' --- ' + str( _code ) )
					## print( '[ - ] COLON COUNT: ' + str( _colon_count ) + ' --- ' + str( _code ) )

					## Checks to see if VarArgs ... exist
					_vararg_chars	= AcecoolLib.String.IndexOf( '...', _code )

					## Helpers - defined for scope even though Python is really lax about it..
					_parts			= None
					_parts_0		= None
					_parts_1		= None

					## colon first as multiple dots can be used in these instances... If colon exists
					if ( _colon_delim > 0 and _colon_count > 0 ):
						## Basic Splitting...
						_parts = _code.split( ':' )
						_parts_0 = _parts[ 0 ]
						_parts_1 = _parts[ 1 ]
						## print( '[ --- ] DOT COUNT00: ' + str( _dot_count ) + ' --- ' + str( _code ) )

					## If . exists, but it isn't where ... is ( if . is at ... then that means . wasn't found in the name...
					elif ( _dot_count > 0 and _dot_delim != _vararg_chars ):
						_parts_0 = _code[ : _dot_delim ]
						_parts_1 = _code[ _dot_delim + 1 : ]

						## print( ' ------ "' + _parts_0 + '" ------ "' + _parts_1 + '"' )

						## ## If we only have 1, we can split...
						## if ( _dot_count == 1 ):
						## 	## Basic Splitting...
						## 	## _parts = _code.split( '.' )
						## 	## _parts_0 = _parts[ 0 ]
						## 	## _parts_1 = _parts[ 1 ]
						## 	_parts_0 = _code[ : _dot_delim ] #Acecool.string.SubStr( _code, 1, _dot_delim )
						## 	_parts_1 = _code[ _dot_delim : ] #Acecool.string.SubStr( _code, _dot_delim, len( _code ) )
						## 	## print( '[ --- ] 0: ' + str( _parts_0 ) + ' --- 1: ' + str( _parts_1 ) + ' --- ' )
						## 	## print( '[ --- ] DOT COUNT2: ' + str( _dot_count ) + ' --- ' + str( _code ) )
						## else:
						## 	## More than 1 we split to the LAST . with the first part being name or so...
						## 	_parts_0 = _code[ : _dot_delim ] #Acecool.string.SubStr( _code, 1, _dot_delim )
						## 	_parts_1 = _code[ _dot_delim : ] #Acecool.string.SubStr( _code, _dot_delim, len( _code ) )
						## 	## print( '[ --- ] DOT COUNT3: ' + str( _dot_count ) + ' --- ' + str( _code ) )

					## If we have parts defined by colon or decimal, then we can split them up into separate categories / entries and we'll cancel our current method...
					if ( _parts_0 != None and _parts_1 != None ):
						##
						_lib_name = _parts_0

						_lib_name = _lib_name.replace( 'local function', '' )
						_lib_name = _lib_name.replace( 'function', '' )
						_lib_name = _lib_name.replace( 'local ', '' )
						## _lib_name = _lib_name.replace( '= ( )', '' )
						_lib_name = _lib_name.replace( self.GetSetting( 'function_name_replace', 'function' ) + ' ', '' )

						if ( AcecoolLib.String.IndexOf( '=', _lib_name ) > 0 ):
							_lib_name = _lib_name.split( '=' )[ 0 ]

						_lib_name = _lib_name.strip( )

						## Lib name could be as simple as parts 0 and finding the first space starting from the right...
						## _lib_name = _lib_name[ _lib_name.find( ' ' ) : len( _lib_name ) ]

						## Grab the function key... - Not all functions use a single decimal which is why this one goes first - : is easier to spot... decimal requires a bit more work to get right...
						_func_name = _parts_1

						## ##
						## if ( _func_name.startswith( '.' ) ):
						## 	_func_name = _func_name[ 1 : ]

						## If we want to display all class-functions in a single category - allow it... except for internal functions as these grow to be MANY..
						if ( _category != CATEGORY_FUNCTION_INTERNAL and self.GetSetting( 'output_class_functions_in_new_category', False ) == False ):
							_category = CATEGORY_CLASS_FUNCTION

						self.AddSimpleEntry( _category, _line_number, 'function ' + _func_name, _depth, '' + _lib_name + ' under ' + self.GetCategoryName( _category ) + ' as a Class / Object / Meta-Table' )

						## return super( ).PreOnAddEntry( _category, _line_number, 'function ' + _func_name, _depth + 1, _depth_delta, None, None, '' + _lib_name + ' under ' + self.GetCategoryName( _category ) + ' as a Class / Object / Meta-Table' )
						## Prevent the default function from being added as an entry
						return False


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )


	##
	## Callback - Called for each line for additional error-checking and for processing rules for class recognition, stack popping, etc...
	##
	def OnProcessLine( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		##
		## Proccess data if not in a comment and the line contains data...
		##
		if not ( _code.strip( ) == '' or _in_comment or _in_comment_block ):
			##
			## Process potential syntax errors
			##
			_xcode = self.ProcessSyntaxErrors( _line_number, _code, _depth, _in_comment, _in_comment_block )

			## If our syntax error checker found an error...
			if ( _xcode and not _xcode == True and not _xcode == False ):
				return _xcode


			##
			## Process potential logical errors
			##
			_xcode = self.ProcessLogicalErrors( _line_number, _code, _depth, _in_comment, _in_comment_block )

			## If our syntax error checker found an error...
			if ( _xcode and not _xcode == True and not _xcode == False ):
				return _xcode


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnProcessLine( _line_number, _code, _depth, _in_comment, _in_comment_block )


	##
	## Helper for Lua - Look for logical errors such as attempting to use a variable which was never defined, etc...
	##
	def ProcessLogicalErrors( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		##
		## Task List
		##
		## Task: Track all defined variables ( Use RegisterVariable / AddVariableEntry or some other helper function ) and compare them to variables found in the code... If a variable is used which was never defined - add an error ... Errors to track would be if case doesn't match, etc..
		## Task:
		## Task:
		## Task:
		## Task:
		##





		pass


	##
	## Helper for Lua - Look for syntax errors...
	##
	def ProcessSyntaxErrors( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		## Detect Syntax Error with if / elseif / else if / else statements...
		if ( self.DetectSyntaxError_IfElse( _line_number, _code, _depth, _in_comment, _in_comment_block ) ):
			## Return the modified line - Note: If the line isn't to be added then this doesn't matter.. It won't add it simply because of an error - the error above will be added though..
			return _code + ' [ Error: If / ... / Else ]'




	##
	## Helper - Detect Syntax Error with if / elseif / else if / else statements...
	##
	def DetectSyntaxError_IfElse( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		if ( _code.startswith( 'elseif' ) or _code.startswith( 'else if' ) or _code.startswith( 'if' ) ):
			## Grab code from the next line...
			_code_pp = self.GetFileDataByLine( _line_number + 1 )

			## Helpers
			_bEndsWithEnd	= _code.endswith( 'end' )
			_bEndsWithThen	= _code.endswith( 'then' )
			_bThenExists	= AcecoolLib.String.IndexOfExists( 'then', _code, True )
			_bThenNextLine	= AcecoolLib.String.IndexOfExists( 'then', _code_pp, True ) #_code_pp.startswith( 'then' )

			## If The line ends with end and then doesn't exist somewhere - error.... If the line doesn't end with end, and then isn't the last word either, and then doesn't exist on this or the next line then error..
			if ( ( _bEndsWithEnd and not _bThenExists ) or ( not _bEndsWithThen and not _bThenExists and not _bThenNextLine ) ):
				## Add the error fix example and modify the code when this type of error is found...
				self.GenerateExample_IfElse( _line_number, _code, _depth, _in_comment, _in_comment_block )

				## Let the ProcessSyntaxErrors function know that an error was found...
				return True

		## The error was not detected...
		return False


	##
	## Helper - Generates the error for If / ... / Else statements and modifies the code...
	##
	def GenerateExample_IfElse( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		## Add an Error Entry
		self.AddError( 'Syntax Error: ' + _code + ' [ Error - "then" keyword missing in or at the end of the line! ]', _line_number )

		## Add a blank line before the entry to allow space between examples...
		if ( not self.HasCategoryEntries( CATEGORY_ERROR_EXAMPLE ) ):
			self.AddBlankEntry( CATEGORY_ERROR_EXAMPLE )

		## Add an example of how they should appear, but only add the example once...
		if ( not self.HasDisplayedErrorExample( 'Lua::if / ... / else' ) ):
			##
			_line = str( _line_number )

			## Create a simple if / else if structure as a copy / paste example..
			_example = ''
			_example += '-- ' + ':' + _line
			_example += '\n\t-- Lua Structure Example: if / else if / elseif / else end -- From the error on line' + ':' + _line
			_example += '\n\t-- Note: This will only appear once per file even if the same error occurs multiple times - A shorter message will appear for repeats...' + ':' + _line
			_example += '\n\t-- ' + ':' + _line
			_example += '\n\t\t-- Begin the if Statement...'
			_example += '\n\t\tif ( < Boolean Statements > ) then'
			_example += '\n\t\t\t< Code >'
			_example += '\n\t\t-- Add an elseif call...'
			_example += '\n\t\telseif ( < Boolean Statements > ) then'
			_example += '\n\t\t\t< Code >'
			_example += '\n\t\t-- else if is identical to calling else and then adding an additional if within it - however when they are side by side only a single "end" is needed afterwards...'
			_example += '\n\t\telse if ( < Boolean Statements > ) then'
			_example += '\n\t\t\t< Code >'
			_example += '\n\t\t-- The last catch-all for this statement...'
			_example += '\n\t\telse'
			_example += '\n\t\t\t< Code >'
			_example += '\n\t\tend'
			## _example += '\n\t\t'
			## _example += '\n\t\t-- Logic outside and not influenced by the if statement above...'
			## _example += '\n\t\t< Logic / Code > -- End of Example for the error occurring on line:'

			## ## Add error Example Notice of repeated error...
			## self.AddErrorExample( '\n\t[ Error - Repeat: "then" keyword missing in-line ]', _line_number )

			## Add a blank line before the entry to allow space between examples...
			if ( not self.HasCategoryEntries( CATEGORY_ERROR_EXAMPLE ) ):
				self.AddBlankEntry( CATEGORY_ERROR_EXAMPLE )

			## Add the Example...
			self.AddErrorExample( _example, _line_number )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------






	## ##
	## ## Callback - these are the symbols from the file we're currently editings symbols( ) from the view - ie all extracted symbols from Sublime Text...
	## ## In Short: If working this should give us all of the information from the Command Palette when we look at @, :, etc.. for function names and everything... A potentially almost perfect way to create a universal mapper...
	## ##
	## ## Task: Allow symbols to be processed through the AddSyntax processor ( Make a syntax processor function outside of Run to do this ) - Then the full-line of code can be grabbed from the symbols SublimeText provides us and we'll have a basic universal mapping system....
	## ##
	## def OnMappingFileSymbols( self, _view, _symbols ):
	## 	## pass
	## 	print( ' >> Symbols: ' + str( _symbols ) )
	## 	## Symbols also don't give their function prefix...
	## 	## if ( _code.startswith( 'def' ):

	## 	## Symbols appear in a list, in a tuple with a region and the symbol...[((515, 525), 'PANEL:Init'), ((891, 910), 'PANEL:ControlValues'), ((1159, 1178), 'PANEL:PerformLayout')]

	## 	## Make sure symbols actually exist...
	## 	if ( _symbols == None ):
	## 		return

	## 	## So for each entry
	## 	for _entry in _symbols:
	## 		## Tuples and lists use numerical indices
	## 		## Grab the region object ( chars from start of file to X, and Total Chars in Region + X )
	## 		_region = _entry[ 0 ]

	## 		## Grab the partial code we're given - we can grab the full line using a helper function...
	## 		_code = _entry[ 1 ]

	## 		## Grab the line number using another helper...
	## 		_line_number = self.GetSublimeViewLineNumberByRegion( _region )

	## 		## Grab the line code by another helper...
	## 		_line = self.GetFileDataByLine( _line_number )

	## 		## Note - Symbols use regions meaning the starting char of from the entire code blob to the end... not line numbering so this has to be calculated...
	## 		## Since other info is also lost we can really only guess at the data unless we check the file and look at other infomration...
	## 		## self.AddEntry( CATEGORY_FUNCTION, _line_number, _code, _depth, _mode, _pattern, _class_cat )
	## 		self.AddSimpleEntry( CATEGORY_SYMBOLS, _line_number, _line.strip( ), 0 )


	## ##
	## ## Callback - these are the symbols from the file we're currently editings symbols( ) from the view - ie all extracted symbols from Sublime Text...
	## ## In Short: If working this should give us all of the information from the Command Palette when we look at @, :, etc.. for function names and everything... A potentially almost perfect way to create a universal mapper...
	## ##
	## ## Task:
	## ##
	## def OnMappingFileSymbols( self, _view, _symbols ):
	## 	pass
	## 	print( ' >> Symbols: ' + str( _symbols ) )