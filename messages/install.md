//
---
// Acecool's Code Mapping System / Code Boiler / Source Code Navigator / ...
---
//
---


###### Sublime Text Plugin to display relevant data from the file you're viewing in a categorized concise layout with important information easily available to you and the ability to jump to those lines instantly by double-clicking the data in the mapping panel.


//
---
// Important Information
---
//
---

Thank you for taking the time to try out my plugin - there are a few very important first steps to take before using this plugin...

Immediately after installing the plugin, you must relaunch Sublime Text.

This is currently unavoidable as there is a bug with the way Sublime Text processes settings files - if a new key is added ( new files, etc.. ) and then the file is attempted to be loaded, the keys are not found until Sublime Text is closed and relaunched.

It is an annoying problem and I am working on designing my own settings system to get rid of a few of the other issues I've had ( no true inheritance, etc.. )


//
---
// About
---
//
---

Acecool's Code Mapping System is designed to make you more efficient while coding by preprocessing source-code files you're working on and extracting the most important bits of information such as Notes, Tasks, Errors, Warnings, Classes / Objects, Functions / Methods, etc... and compiling them into a categorized list of information and then those entries are alphabetized to make finding the data that much easier. With the dynamic unlimited nesting category system, categories are made, by default, for all object functions to make finding what you're looking for that much easier.

When you've found what you're looking for, simply click ( or double click - depending on your options ) a line in the output panel to jump to the line where that entry is represented in the source-code view and you'll be instantly brought to that location. The benefits are endless. In a 10,000+ line file, or even shorter files, it can take 10s of seconds to scroll to the general area you're looking for, then it may take even longer to find the exact location of the line of code you're looking for. Because this mapping system boils your code down to nothing except the most important parts, instead of 10,000 + lines to navigate, you are left with a tiny fraction of that amount in an easy to browse categorized friendly layout to help you get to where you need to go in the shortest possible time!



//
---
// Known Issues
---
//
---

- If you install ACMS and do not relaunch Sublime Text, the plugin will not work and you may encounter errors in the console or strange behavior resulting in incorrect configuration information being used..
- Sometimes the jump to line can get confused and be delayed by 1 click if you click a line which has no entry ( this was solved and now with setting up package system, it has resurfaced in a different way )



//
---
// A Side Note
---
//
---

Thank you for installing Acecool Code Mapping System, I it meeds and exceeds your expectations. If it doesn't, or if you can think of any feature which would make you more efficient, or just something you'd like added or changed... do not hesitate to submit an Issue Report and select Feature Request from the dropdown! I take user feedback very seriously so you can be sure all of the issue reports, feature requests, etc.. are going to be read and the feature is likely to make it in :-)

I dislike bugs very much which is why I have tested this addon thoroughly before releasing it. All know bugs have been squashed and you shouldn't experience any issues at the time of release - if you notice anything wrong, please report it. I have tested it using Windows 10 Pro x64 bit on a 10 year old computer.




//
---
// Default Features
---
//
---
----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________R
----01----____02____----03----____04____----05----____06____----07----____08____----09----____10____----11----____12____----13----____14____----15----____16____----17----____18____----19----____20____U
----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________L
01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
╔═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╗
║																	Features and their default values																║
╟═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╤═══════════════════════════════╣
║			  Action																												║		  Default Value			║
╟═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╬═══════════════════════════════╣
║																																	║	  CTRL + ALT + SHIFT + M	║
╟───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╬───────────────────────────────╣
║																																	║			 ALT + M			║
╟───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╬───────────────────────────────╣
║																																	║			CTRL + M			║
╟───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╬───────────────────────────────╣
║																																	║								║
╚═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╩═══════════════════════════════╝
----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________
----01----____02____----03----____04____----05----____06____----07----____08____----09----____10____----11----____12____----13----____14____----15----____16____----17----____18____----19----____20____
----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________



//
---
// Keybindings
---
//
---

----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________
----01----____02____----03----____04____----05----____06____----07----____08____----09----____10____----11----____12____----13----____14____----15----____16____----17----____18____----19----____20____
----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________
╔═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╗
║																			  Key Bindings																			║
╟═══════════════════════════════════════════════════════════════════════════╤═══════════════════════════╤═══════════════════════════╤═══════════════════════════════╣
║			  Action														║			Mac				║			Linux			║			Windows				║
╟═══════════════════════════════════════════════════════════════════════════╬═══════════════════════════╬═══════════════════════════╬═══════════════════════════════╣
║																			║		SUPER				║							║	  CTRL + ALT + SHIFT + M	║
╟───────────────────────────────────────────────────────────────────────────╬───────────────────────────╬───────────────────────────╬───────────────────────────────╣
║																			║		SUPER				║							║			 ALT + M			║
╟───────────────────────────────────────────────────────────────────────────╬───────────────────────────╬───────────────────────────╬───────────────────────────────╣
║																			║		SUPER				║							║			CTRL + M			║
╟───────────────────────────────────────────────────────────────────────────╬───────────────────────────╬───────────────────────────╬───────────────────────────────╣
║																			║							║							║								║
╚═══════════════════════════════════════════════════════════════════════════╩═══════════════════════════╩═══════════════════════════╩═══════════════════════════════╝




//
---
// Related Links
---
//
---
----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________
----01----____02____----03----____04____----05----____06____----07----____08____----09----____10____----11----____12____----13----____14____----15----____16____----17----____18____----19----____20____
----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________
╔═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╗
║														 			 Important Links																				║
╟═══════════════════════════════════╤═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╣
║			  						URL										║									Description											║
╟═══════════════════════════════════════════════════════════════════════════╬═══════════════════════════════════════════════════════════════════════════════════════╣
║	https://paypal.me/Acecool												║	Support Us and the Development Costs by Donating - Every little bit helps!			║
╟───────────────────────────────────────────────────────────────────────────╬───────────────────────────────────────────────────────────────────────────────────────╣
║	https://bitbucket.org/Acecool/AcecoolCodeMappingSystem/overview/		║	View our Repo																		║
╟───────────────────────────────────────────────────────────────────────────╬───────────────────────────────────────────────────────────────────────────────────────╣
║	https://bitbucket.org/Acecool/AcecoolCodeMappingSystem/src/				║	View our Code																		║
╟───────────────────────────────────────────────────────────────────────────╬───────────────────────────────────────────────────────────────────────────────────────╣
║	https://bitbucket.org/Acecool/AcecoolCodeMappingSystem/downloads/		║	View all available downloads														║
╟───────────────────────────────────────────────────────────────────────────╬───────────────────────────────────────────────────────────────────────────────────────╣
║	https://bitbucket.org/Acecool/AcecoolCodeMappingSystem/get/master.zip	║	Download the latest version - Direct Link											║
╟───────────────────────────────────────────────────────────────────────────╬───────────────────────────────────────────────────────────────────────────────────────╣
║	https://bitbucket.org/Acecool/AcecoolCodeMappingSystem/issues/			║	View all Issues and Feature Requests												║
╟───────────────────────────────────────────────────────────────────────────╬───────────────────────────────────────────────────────────────────────────────────────╣
║	https://bitbucket.org/Acecool/AcecoolCodeMappingSystem/issues/new/		║	Submit a Bug Report or Feature Request												║
╚═══════════════════════════════════════════════════════════════════════════╩═══════════════════════════════════════════════════════════════════════════════════════╝
----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________
----01----____02____----03----____04____----05----____06____----07----____08____----09----____10____----11----____12____----13----____14____----15----____16____----17----____18____----19----____20____
----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________----------__________




###
### Other...
###
##	Fading:			░ ▒ ▓
##	Blocks:			 █ ▄ ▌ ▐ ▀ ▬ ■
##	Single:			║║ ┤ ┐ └ ┴ ├ ┬ ─ ┼ ┘ ┌ ∟ Γ
##	Double:			 ╣ ║ ╗ ╝ ╚ ╔ ╩ ╦ ╠ ═ ╬
##	Both:			╡ ╢ ╖ ╕ ╜ ╛ ╞ ╟ ╧ ╨ ╤ ╥ ╙ ╘ ╒ ╓ ╫ ╪